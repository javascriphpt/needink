<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_WORKORDERENTRY';
$path_to_root = "..";

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/manufacturing.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/manufacturing/includes/manufacturing_db.inc");
include_once($path_to_root . "/manufacturing/includes/manufacturing_ui.inc");

include($path_to_root . "/manufacturing/includes/cart_class.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Work Order Entry"), false, false, "", $js);


check_db_has_manufacturable_items(_("There are no manufacturable items defined in the system."));

check_db_has_locations(("There are no inventory locations defined in the system."));

//---------------------------------------------------------------------------------------


if (isset($_GET['trans_no']))
{
	$selected_id_2 = $_GET['trans_no'];
}
elseif(isset($_POST['selected_id_2']))
{
	$selected_id_2 = $_POST['selected_id_2'];
}

//---------------------------------------------------------------------------------------

if (isset($_GET['AddedID']))
{
	$id = $_GET['AddedID'];
	$stype = ST_WORKORDER;

	display_notification_centered(_("The work order been added."));

    display_note(get_trans_view_str($stype, $id, _("View this Work Order")));

	if ($_GET['type'] != WO_ADVANCED)
	{
		include_once($path_to_root . "/reporting/includes/reporting.inc");
    	$ar = array('PARAM_0' => $id, 'PARAM_1' => $id, 'PARAM_2' => 0); 
    	display_note(print_link(_("Print this Work Order"), 409, $ar), 1);
    	$ar['PARAM_2'] = 1;
    	display_note(print_link(_("Email this Work Order"), 409, $ar), 1);
    	display_note(get_gl_view_str($stype, $id, _("View the GL Journal Entries for this Work Order")), 1);
    	$ar = array('PARAM_0' => $_GET['date'], 'PARAM_1' => $_GET['date'], 'PARAM_2' => $stype); 
    	display_note(print_link(_("Print the GL Journal Entries for this Work Order"), 702, $ar), 1);
	}
	
	safe_exit();
}

//---------------------------------------------------------------------------------------

if (isset($_GET['UpdatedID']))
{
	$id = $_GET['UpdatedID'];

	display_notification_centered(_("The work order been updated."));
	safe_exit();
}

//---------------------------------------------------------------------------------------

if (isset($_GET['DeletedID']))
{
	$id = $_GET['DeletedID'];

	display_notification_centered(_("Work order has been deleted."));
	safe_exit();
}

//---------------------------------------------------------------------------------------

if (isset($_GET['ClosedID']))
{
	$id = $_GET['ClosedID'];

	display_notification_centered(_("This work order has been closed. There can be no more issues against it.") . " #$id");
	safe_exit();
}

//---------------------------------------------------------------------------------------

function safe_exit()
{
	global $path_to_root;

	hyperlink_no_params("", _("Enter a new work order"));
	hyperlink_no_params("search_work_orders.php", _("Select an existing work order"));
	
	display_footer_exit();
}

//-------------------------------------------------------------------------------------
if (!isset($_POST['date_']))
{
	$_POST['date_'] = new_doc_date();
	if (!is_date_in_fiscalyear($_POST['date_']))
		$_POST['date_'] = end_fiscalyear();
}

function can_process()
{
	global $selected_id_2, $SysPrefs, $Refs;

	if (!isset($selected_id_2))
	{
    	if (!$Refs->is_valid($_POST['wo_ref']))
    	{
    		display_error(_("You must enter a reference."));
			set_focus('wo_ref');
    		return false;
    	}

    	if (!is_new_reference($_POST['wo_ref'], ST_WORKORDER))
    	{
    		display_error(_("The entered reference is already in use."));
			set_focus('wo_ref');
    		return false;
    	}
	}

	if (!check_num('quantity_2', 0))
	{
		display_error( _("The quantity entered is invalid or less than zero."));
		set_focus('quantity_2');
		return false;
	}

	if (!is_date($_POST['date_']))
	{
		display_error( _("The date entered is in an invalid format."));
		set_focus('date_');
		return false;
	}
	elseif (!is_date_in_fiscalyear($_POST['date_']))
	{
		display_error(_("The entered date is not in fiscal year."));
		set_focus('date_');
		return false;
	}
	// only check bom and quantites if quick assembly
	if (!($_POST['type'] == WO_ADVANCED))
	{
        if (!has_bom($_POST['stock_id']))
        {
        	display_error(_("The selected item to manufacture does not have a bom."));
			set_focus('stock_id');
        	return false;
        }

		if ($_POST['Labour'] == "")
			$_POST['Labour'] = price_format(0);
    	if (!check_num('Labour', 0))
    	{
    		display_error( _("The labour cost entered is invalid or less than zero."));
			set_focus('Labour');
    		return false;
    	}
		if ($_POST['Costs'] == "")
			$_POST['Costs'] = price_format(0);
    	if (!check_num('Costs', 0))
    	{
    		display_error( _("The cost entered is invalid or less than zero."));
			set_focus('Costs');
    		return false;
    	}

        if (!$SysPrefs->allow_negative_stock())
        {
        	if ($_POST['type'] == WO_ASSEMBLY)
        	{
        		// check bom if assembling
                $result = get_bom($_POST['stock_id']);

            	while ($bom_item = db_fetch($result))
            	{

            		if (has_stock_holding($bom_item["ResourceType"]))
            		{

                		$quantity_2 = $bom_item["quantity"] * input_num('quantity_2');

                        $qoh = get_qoh_on_date($bom_item["component"], $bom_item["loc_code"], $_POST['date_']);
                		if (-$quantity_2 + $qoh < 0)
                		{
                			display_error(_("The work order cannot be processed because there is an insufficient quantity for component:") .
                				" " . $bom_item["component"] . " - " .  $bom_item["description"] . ".  " . _("Location:") . " " . $bom_item["location_name"]);
							set_focus('quantity_2');
        					return false;
                		}
            		}
            	}
        	}
        	elseif ($_POST['type'] == WO_UNASSEMBLY)
        	{
        		// if unassembling, check item to unassemble
				$qoh = get_qoh_on_date($_POST['stock_id'], $_POST['StockLocation'], $_POST['date_']);
        		if (-input_num('quantity_2') + $qoh < 0)
        		{
        			display_error(_("The selected item cannot be unassembled because there is insufficient stock."));
					return false;
        		}
        	}
    	}
     }
     else
     {
    	if (!is_date($_POST['RequDate']))
    	{
			set_focus('RequDate');
    		display_error( _("The date entered is in an invalid format."));
    		return false;
		}
		//elseif (!is_date_in_fiscalyear($_POST['RequDate']))
		//{
		//	display_error(_("The entered date is not in fiscal year."));
		//	return false;
		//}
    	if (isset($selected_id_2))
    	{
    		$myrow = get_work_order($selected_id_2, true);

    		if ($_POST['units_issued'] > input_num('quantity_2'))
    		{
				set_focus('quantity_2');
    			display_error(_("The quantity cannot be changed to be less than the quantity already manufactured for this order."));
        		return false;
    		}
    	}
	}

	return true;
}

//-------------------------------------------------------------------------------------

if (isset($_POST['ADD_ITEM_2']) && can_process())
{
	if (!isset($_POST['cr_acc']))
		$_POST['cr_acc'] = "";
	if (!isset($_POST['cr_lab_acc']))
		$_POST['cr_lab_acc'] = "";
	$id = add_work_order($_POST['wo_ref'], $_POST['StockLocation'], input_num('quantity_2'),
		$_POST['stock_id'],  $_POST['type'], $_POST['date_'],
		$_POST['RequDate'], $_POST['memo_'], input_num('Costs'), $_POST['cr_acc'], input_num('Labour'), $_POST['cr_lab_acc']);

	new_doc_date($_POST['date_']);
	meta_forward($_SERVER['PHP_SELF'], "AddedID=$id&type=".$_POST['type']."&date=".$_POST['date_']);
}

//-------------------------------------------------------------------------------------

if (isset($_POST['UPDATE_ITEM_2']) && can_process())
{

	update_work_order($selected_id_2, $_POST['StockLocation'], input_num('quantity_2'),
		$_POST['stock_id'],  $_POST['date_'], $_POST['RequDate'], $_POST['memo_']);
	new_doc_date($_POST['date_']);
	meta_forward($_SERVER['PHP_SELF'], "UpdatedID=$selected_id_2");
}

//--------------------------------------------------------------------------------------

if (isset($_POST['delete_2']))
{
	//the link to delete a selected record was clicked instead of the submit button

	$cancel_delete = false;

	// can't delete it there are productions or issues
	if (work_order_has_productions($selected_id_2) ||
		work_order_has_issues($selected_id_2)	||
		work_order_has_payments($selected_id_2))
	{
		display_error(_("This work order cannot be deleted because it has already been processed."));
		$cancel_delete = true;
	}

	if ($cancel_delete == false)
	{ //ie not cancelled the delete as a result of above tests

		// delete the actual work order
		delete_work_order($selected_id_2);
		meta_forward($_SERVER['PHP_SELF'], "DeletedID=$selected_id_2");
	}
}

//-------------------------------------------------------------------------------------

if (isset($_POST['close_2']))
{

	// update the closed flag in the work order
	close_work_order($selected_id_2);
	meta_forward($_SERVER['PHP_SELF'], "ClosedID=$selected_id_2");
}

//-------------------------------------------------------------------------------------
if (get_post('_type_update')) 
{
  $Ajax->activate('_page_body');
}
//-------------------------------------------------------------------------------------

start_form();

start_table($table_style2);

$existing_comments = "";

$dec = 0;
if (isset($selected_id_2))
{
	$myrow = get_work_order($selected_id_2);

	if (strlen($myrow[0]) == 0)
	{
		echo _("The order number sent is not valid.");
		safe_exit();
	}

	// if it's a closed work order can't edit it
	if ($myrow["closed"] == 1)
	{
		echo "<center>";
		display_error(_("This work order is closed and cannot be edited."));
		safe_exit();
	}

	$_POST['wo_ref'] = $myrow["wo_ref"];
	$_POST['stock_id'] = $myrow["stock_id"];
	$_POST['quantity'] = qty_format($myrow["units_reqd"], $_POST['stock_id'], $dec);
	$_POST['StockLocation'] = $myrow["loc_code"];
	$_POST['released'] = $myrow["released"];
	$_POST['closed'] = $myrow["closed"];
	$_POST['type'] = $myrow["type"];
	$_POST['date_'] = sql2date($myrow["date_"]);
	$_POST['RequDate'] = sql2date($myrow["required_by"]);
	$_POST['released_date'] = sql2date($myrow["released_date"]);
	$_POST['memo_'] = "";
	$_POST['units_issued'] = $myrow["units_issued"];
	$_POST['Costs'] = price_format($myrow["additional_costs"]);

	$_POST['memo_'] = get_comments_string(ST_WORKORDER, $selected_id_2);

	hidden('wo_ref', $_POST['wo_ref']);
	hidden('units_issued', $_POST['units_issued']);
	hidden('released', $_POST['released']);
	hidden('released_date', $_POST['released_date']);
	hidden('selected_id_2',  $selected_id_2);
	hidden('old_qty', $myrow["units_reqd"]);
	hidden('old_stk_id', $myrow["stock_id"]);

	label_row(_("Reference:"), $_POST['wo_ref']);
	label_row(_("Type:"), $wo_types_array[$_POST['type']]);
	hidden('type', $myrow["type"]);
}
else
{
	$_POST['units_issued'] = $_POST['released'] = 0;
	ref_row(_("Reference:"), 'wo_ref', '', $Refs->get_next(ST_WORKORDER));

	wo_types_list_row(_("Type:"), 'type', null);
}

if (get_post('released'))
{
	hidden('stock_id', $_POST['stock_id']);
	hidden('StockLocation', $_POST['StockLocation']);
	hidden('type', $_POST['type']);

	label_row(_("Item:"), $myrow["StockItemName"]);
	label_row(_("Destination Location:"), $myrow["location_name"]);
}
else
{
	if($_SESSION["wa_current_user"]->can_access_all_item_categories == 0)
		stock_manufactured_items_list_row_user(_("Item:"), 'stock_id', null, false, true, $_SESSION["wa_current_user"]->default_item_categories);
	else
		stock_manufactured_items_list_row(_("Item:"), 'stock_id', null, false, true);
		
	if (list_updated('stock_id'))
		$Ajax->activate('quantity_2');

	locations_list_row(_("Destination Location:"), 'StockLocation', null);
}

if (!isset($_POST['quantity_2']))
	$_POST['quantity_2'] = qty_format(1, $_POST['stock_id'], $dec);
else
	$_POST['quantity_2'] = qty_format($_POST['quantity_2'], $_POST['stock_id'], $dec);
	

if (get_post('type') == WO_ADVANCED)
{
    qty_row(_("Quantity Required:"), 'quantity_2', null, null, null, $dec);
    if ($_POST['released'])
    	label_row(_("Quantity Manufactured:"), number_format($_POST['units_issued'], get_qty_dec($_POST['stock_id'])));
    date_row(_("Date") . ":", 'date_', '', true);
	date_row(_("Date Required By") . ":", 'RequDate', '', null, $SysPrefs->default_wo_required_by());
}
else
{
    qty_row(_("Quantity:"), 'quantity_2', null, null, null, $dec);
    date_row(_("Date") . ":", 'date_', '', true);
	hidden('RequDate', '');

	$sql = "SELECT DISTINCT account_code FROM ".TB_PREF."bank_accounts";
	$rs = db_query($sql,"could not get bank accounts");
	$r = db_fetch_row($rs);
	if (!isset($_POST['Labour']))
	{
		$_POST['Labour'] = price_format(0);
		$_POST['cr_lab_acc'] = $r[0];
	}
	amount_row($wo_cost_types[WO_LABOUR], 'Labour');
	gl_all_accounts_list_row(_("Credit Labour Account"), 'cr_lab_acc', null);
	if (!isset($_POST['Costs']))
	{
		$_POST['Costs'] = price_format(0);
		$_POST['cr_acc'] = $r[0];
	}
	amount_row($wo_cost_types[WO_OVERHEAD], 'Costs');
	gl_all_accounts_list_row(_("Credit Overhead Account"), 'cr_acc', null);
	
}

if (get_post('released'))
	label_row(_("Released On:"),$_POST['released_date']);

textarea_row(_("Memo:"), 'memo_', null, 40, 5);

end_table(1);

// <<<<<<< .mine
// if (isset($selected_id))
// {
	// echo "<table align=center><tr>";

	// submit_cells('UPDATE_ITEM', _("Update"), '', _('Save changes to work order'), 'default');
	// if (get_post('released'))
	// {
		// submit_cells('close', _("Close This Work Order"),'','',true);
	// }
	// submit_cells('delete', _("Delete This Work Order"),'','',true);

	// echo "</tr></table>";
// }
// else
// {
	// submit_center('ADD_ITEM', _("Add Workorder"), true, '', 'default');
// }

end_form();


include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/includes/manufacturing.inc");

check_db_has_bom_stock_items(_("There are no manufactured or kit items defined in the system."));

check_db_has_workcentres(_("There are no work centres defined in the system. BOMs require at least one work centre be defined."));

simple_page_mode(true);
$selected_component = $selected_id;
//--------------------------------------------------------------------------------------------------

//if (isset($_GET["NewItem"]))
//{
//	$_POST['stock_id'] = $_GET["NewItem"];
//}
if (isset($_GET['stock_id']))
// =======
// if (isset($selected_id_2))
// >>>>>>> .r79
{
	$_POST['stock_id'] = $_GET['stock_id'];
	$selected_parent =  $_GET['stock_id'];
}

// <<<<<<< .mine
/* selected_parent could come from a post or a get */
/*if (isset($_GET["selected_parent"]))
{
	$selected_parent = $_GET["selected_parent"];
}
else if (isset($_POST["selected_parent"]))
{
	$selected_parent = $_POST["selected_parent"];
}
*/
/* selected_component could also come from a post or a get */
/*if (isset($_GET["selected_component"]))
{
	$selected_component = $_GET["selected_component"];
}
else
{
	$selected_component = get_post("selected_component", -1);
}
*/

//--------------------------------------------------------------------------------------------------

function check_for_recursive_bom($ultimate_parent, $component_to_check)
{

	/* returns true ie 1 if the bom contains the parent part as a component
	ie the bom is recursive otherwise false ie 0 */

	$sql = "SELECT component FROM ".TB_PREF."bom WHERE parent=".db_escape($component_to_check);
	$result = db_query($sql,"could not check recursive bom");

	if ($result != 0)
// =======
	submit_cells('UPDATE_ITEM_2', _("Update"), '', _('Save changes to work order'), 'default');
	if (get_post('released'))
// >>>>>>> .r79
	{
// <<<<<<< .mine
		while ($myrow = db_fetch_row($result))
		{
			if ($myrow[0] == $ultimate_parent)
			{
				return 1;
			}

			if (check_for_recursive_bom($ultimate_parent, $myrow[0]))
			{
				return 1;
			}
		} //(while loop)
	} //end if $result is true

	return 0;

} //end of function check_for_recursive_bom

//--------------------------------------------------------------------------------------------------



function display_bom_items($selected_parent)
{
	global $table_style;

	$result = get_bom($selected_parent);
div_start('bom');
	start_table("$table_style width=60%");
	if (!(get_post('type') == WO_ADVANCED_UNASSEMBLY))
	$th = array(_("Code"), _("Description"), _("Location"),
		_("Work Centre"), _("Quantity"), _("Units"),'','');
	else
	$th = array(_("Code"), _("Description"), _("Location"),
		_("Work Centre"), _("Quantity"), _("Units"),'');
		
	table_header($th);

	$k = 0;
	
	//Temporarily stacks in a cart...
	$_SESSION['items'] = new items_cart;
	
	while ($myrow = db_fetch($result))
	{
		
		alt_table_row_color($k);
		$_SESSION['items']->code_id = $myrow["component"];
		$_SESSION['items']->description = $myrow["description"];
		$_SESSION['items']->location = $myrow["location_name"];
		$_SESSION['items']->work_centre = $myrow["WorkCentreDescription"];
		$_SESSION['items']->qty = $myrow["quantity"];
		$_SESSION['items']->units = $myrow["units"];

		label_cell($myrow["component"]);
		label_cell($myrow["description"]);
        label_cell($myrow["location_name"]);
        label_cell($myrow["WorkCentreDescription"]);
        qty_cell($myrow["quantity"], false, get_qty_dec($myrow["component"]));
        label_cell($myrow["units"]);
 		edit_button_cell("Edit".$myrow['id'], _("Edit"));
		if (!(get_post('type') == WO_ADVANCED_UNASSEMBLY))
 		delete_button_cell("Delete".$myrow['id'], _("Delete"));
        end_row();

	} //END WHILE LIST LOOP
	
	foreach($_SESSION['items'] as $key=>$val){
		display_error($key.' = '.$val);
	}
	// display_error('COUNT :: '.count($_SESSION['items']))	;
	end_table();
div_end();
}

//--------------------------------------------------------------------------------------------------

function on_submit($selected_parent, $selected_component=-1)
{
	if (!check_num('quantity', 0))
	{
		display_error(_("The quantity entered must be numeric and greater than zero."));
		set_focus('quantity');
		return;
// =======
		submit_cells('close_2', _("Close This Work Order"),'','',true);
// >>>>>>> .r79
	}
// <<<<<<< .mine
// =======
	submit_cells('delete_2', _("Delete This Work Order"),'','',true);
// >>>>>>> .r79

	if ($selected_component != -1)
	{

		$sql = "UPDATE ".TB_PREF."bom SET workcentre_added=".db_escape($_POST['workcentre_added'])
		 . ",loc_code=".db_escape($_POST['loc_code']) . ",
			quantity= " . input_num('quantity') . "
			WHERE parent=".db_escape($selected_parent) . "
			AND id=".db_escape($selected_component);
		check_db_error("Could not update this bom component", $sql);

		db_query($sql,"could not update bom");
		display_notification(_('Selected component has been updated'));
		$Mode = 'RESET';
	}
	else
	{

		/*Selected component is null cos no item selected on first time round
		so must be adding a record must be Submitting new entries in the new
		component form */

		//need to check not recursive bom component of itself!
		if (!check_for_recursive_bom($selected_parent, $_POST['component']))
		{

			/*Now check to see that the component is not already on the bom */
			$sql = "SELECT component FROM ".TB_PREF."bom
				WHERE parent=".db_escape($selected_parent)."
				AND component=".db_escape($_POST['component']) . "
				AND workcentre_added=".db_escape($_POST['workcentre_added']) . "
				AND loc_code=".db_escape($_POST['loc_code']);
			$result = db_query($sql,"check failed");

			if (db_num_rows($result) == 0)
			{
				$sql = "INSERT INTO ".TB_PREF."bom (parent, component, workcentre_added, loc_code, quantity)
					VALUES (".db_escape($selected_parent).", ".db_escape($_POST['component']) . ","
					.db_escape($_POST['workcentre_added']) . ", ".db_escape($_POST['loc_code']) . ", "
					. input_num('quantity') . ")";

				db_query($sql,"check failed");
				display_notification(_("A new component part has been added to the bill of material for this item."));
				$Mode = 'RESET';
			}
			else
			{
				/*The component must already be on the bom */
				display_error(_("The selected component is already on this bom. You can modify it's quantity but it cannot appear more than once on the same bom."));
			}

		} //end of if its not a recursive bom
		else
		{
			display_error(_("The selected component is a parent of the current item. Recursive BOMs are not allowed."));
		}
	}
}

//--------------------------------------------------------------------------------------------------

if ($Mode == 'Delete')
{
// <<<<<<< .mine
	$sql = "DELETE FROM ".TB_PREF."bom WHERE id=".db_escape($selected_id);
	db_query($sql,"Could not delete this bom components");

	display_notification(_("The component item has been deleted from this bom"));
	$Mode = 'RESET';
// =======
	submit_center('ADD_ITEM_2', _("Add Workorder"), true, '', 'default');
// >>>>>>> .r79
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	unset($_POST['quantity']);
}

//--------------------------------------------------------------------------------------------------

start_form();

start_form(false, true);
start_table("class='tablestyle_noborder'");
// stock_manufactured_items_list_row(_("Select a manufacturable item:"), 'stock_id', null, false, true);
if (list_updated('stock_id'))
	$Ajax->activate('_page_body');
end_table();
br();

end_form();
// <<<<<<< .mine
//--------------------------------------------------------------------------------------------------

if (get_post('stock_id') != '')
{ //Parent Item selected so display bom or edit component
	$selected_parent = $_POST['stock_id'];
	if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM')
		on_submit($selected_parent, $selected_id);
	//--------------------------------------------------------------------------------------

start_form();
	display_bom_items($selected_parent);
	//--------------------------------------------------------------------------------------
	echo '<br>';

	start_table($table_style2);

	if ($selected_id != -1)
	{
 		if ($Mode == 'Edit') {
			//editing a selected component from the link to the line item
			$sql = "SELECT ".TB_PREF."bom.*,".TB_PREF."stock_master.description FROM "
				.TB_PREF."bom,".TB_PREF."stock_master
				WHERE id=".db_escape($selected_id)."
				AND ".TB_PREF."stock_master.stock_id=".TB_PREF."bom.component";

			$result = db_query($sql, "could not get bom");
			$myrow = db_fetch($result);

			$_POST['loc_code'] = $myrow["loc_code"];
			$_POST['component'] = $myrow["component"]; // by Tom Moulton
			$_POST['workcentre_added']  = $myrow["workcentre_added"];
			$_POST['quantity'] = number_format2($myrow["quantity"], get_qty_dec($myrow["component"]));
		label_row(_("Component:"), $myrow["component"] . " - " . $myrow["description"]);
		}
		hidden('selected_id', $selected_id);
	}
	else
	{
		start_row();
		label_cell(_("Component:"));

		echo "<td>";
		echo stock_component_items_list('component', $selected_parent, null, false, true);
		if (get_post('_component_update')) 
		{
			$Ajax->activate('quantity');
		}
		echo "</td>";
		end_row();
	}
	hidden('stock_id', $selected_parent);

	locations_list_row(_("Location to Draw From:"), 'loc_code', null);
	workcenter_list_row(_("Work Centre Added:"), 'workcentre_added', null);
	$dec = get_qty_dec(get_post('component'));
	$_POST['quantity'] = number_format2(input_num('quantity',1), $dec);
	qty_row(_("Quantity:"), 'quantity', null, null, null, $dec);

	// submit_add_or_update_center($selected_id == -1, '', 'both');
	end_table(1);
	
	if (!(get_post('type') == WO_ADVANCED_UNASSEMBLY)){
	submit_center_first('add_item', 'Add Item Component');
	submit_center_last('add_entry', 'Add Entry');
	}else
	submit_center('add_entry', 'Add Entry');
	
	end_form();
}

// =======


if (($_POST['type'] == WO_ADVANCED))
{

	// manual bom
	// ----------------------------------------------------------------------------------

	simple_page_mode(true);
	$selected_component = $selected_id;
	//--------------------------------------------------------------------------------------------------

	if (isset($_GET['stock_id']))
	{
		$_POST['stock_id'] = $_GET['stock_id'];
		$selected_parent =  $_GET['stock_id'];
	}

	//--------------------------------------------------------------------------------------------------

	function check_for_recursive_bom($ultimate_parent, $component_to_check)
	{

		/* returns true ie 1 if the bom contains the parent part as a component
		ie the bom is recursive otherwise false ie 0 */

		$sql = "SELECT component FROM ".TB_PREF."bom WHERE parent=".db_escape($component_to_check);
		$result = db_query($sql,"could not check recursive bom");

		if ($result != 0)
		{
			while ($myrow = db_fetch_row($result))
			{
				if ($myrow[0] == $ultimate_parent)
				{
					return 1;
				}

				if (check_for_recursive_bom($ultimate_parent, $myrow[0]))
				{
					return 1;
				}
			} //(while loop)
		} //end if $result is true

		return 0;

	} //end of function check_for_recursive_bom

	//--------------------------------------------------------------------------------------------------

	function display_bom_items($selected_parent)
	{
		global $table_style;

		$result = get_bom($selected_parent);
	div_start('bom');
		start_table("$table_style width=60%");
		$th = array(_("Code"), _("Description"), _("Location"),
			_("Work Centre"), _("Quantity"), _("Units"),'','');
		table_header($th);

		$k = 0;
		while ($myrow = db_fetch($result))
		{

			alt_table_row_color($k);

			label_cell($myrow["component"]);
			label_cell($myrow["description"]);
			label_cell($myrow["location_name"]);
			label_cell($myrow["WorkCentreDescription"]);
			qty_cell($myrow["quantity"], false, get_qty_dec($myrow["component"]));
			label_cell($myrow["units"]);
			edit_button_cell("Edit".$myrow['id'], _("Edit"));
			delete_button_cell("Delete".$myrow['id'], _("Delete"));
			end_row();

		} //END WHILE LIST LOOP
		end_table();
	div_end();
	}

	//--------------------------------------------------------------------------------------------------

	function on_submit($selected_parent, $selected_component=-1)
	{
		if (!check_num('quantity', 0))
		{
			display_error(_("The quantity entered must be numeric and greater than zero."));
			set_focus('quantity');
			return;
		}

		if ($selected_component != -1)
		{

			$sql = "UPDATE ".TB_PREF."bom SET workcentre_added=".db_escape($_POST['workcentre_added'])
			 . ",loc_code=".db_escape($_POST['loc_code']) . ",
				quantity= " . input_num('quantity') . "
				WHERE parent=".db_escape($selected_parent) . "
				AND id=".db_escape($selected_component);
			check_db_error("Could not update this bom component", $sql);

			db_query($sql,"could not update bom");
			display_notification(_('Selected component has been updated'));
			$Mode = 'RESET';
		}
		else
		{

			/*Selected component is null cos no item selected on first time round
			so must be adding a record must be Submitting new entries in the new
			component form */

			//need to check not recursive bom component of itself!
			if (!check_for_recursive_bom($selected_parent, $_POST['component']))
			{

				/*Now check to see that the component is not already on the bom */
				$sql = "SELECT component FROM ".TB_PREF."bom
					WHERE parent=".db_escape($selected_parent)."
					AND component=".db_escape($_POST['component']) . "
					AND workcentre_added=".db_escape($_POST['workcentre_added']) . "
					AND loc_code=".db_escape($_POST['loc_code']);
				$result = db_query($sql,"check failed");

				if (db_num_rows($result) == 0)
				{
					$sql = "INSERT INTO ".TB_PREF."bom (parent, component, workcentre_added, loc_code, quantity)
						VALUES (".db_escape($selected_parent).", ".db_escape($_POST['component']) . ","
						.db_escape($_POST['workcentre_added']) . ", ".db_escape($_POST['loc_code']) . ", "
						. input_num('quantity') . ")";

					db_query($sql,"check failed");
					display_notification(_("A new component part has been added to the bill of material for this item."));
					$Mode = 'RESET';
				}
				else
				{
					/*The component must already be on the bom */
					display_error(_("The selected component is already on this bom. You can modify it's quantity but it cannot appear more than once on the same bom."));
				}

			} //end of if its not a recursive bom
			else
			{
				display_error(_("The selected component is a parent of the current item. Recursive BOMs are not allowed."));
			}
		}
	}

	//--------------------------------------------------------------------------------------------------

	if ($Mode == 'Delete')
	{
		$sql = "DELETE FROM ".TB_PREF."bom WHERE id=".db_escape($selected_id);
		db_query($sql,"Could not delete this bom components");

		display_notification(_("The component item has been deleted from this bom"));
		$Mode = 'RESET';
	}

	if ($Mode == 'RESET')
	{
		$selected_id = -1;
		unset($_POST['quantity']);
	}

	// function handle_add_new_item()
	// {
			// display_error('zxczxc');
		// foreach($_SESSION['PO'] as $order_item){
			// // if()
		// }
	// }
	
	//--------------------------------------------------------------------------------------------------

	if (list_updated('stock_id'))
		$Ajax->activate('_page_body');
	//--------------------------------------------------------------------------------------------------

	if (get_post('stock_id') != '')
	{ //Parent Item selected so display bom or edit component
		$selected_parent = $_POST['stock_id'];
		if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM')
			// if (($_POST['type'] != WO_ADVANCED_UNASSEMBLY))
			on_submit($selected_parent, $selected_id);
			else
			handle_add_new_item();
		//--------------------------------------------------------------------------------------

	start_form();
		echo '<br>';
		display_bom_items($selected_parent);
		//--------------------------------------------------------------------------------------
		echo '<br>';

		start_table($table_style2);

		if ($selected_id != -1)
		{
			if ($Mode == 'Edit') {
				//editing a selected component from the link to the line item
				$sql = "SELECT ".TB_PREF."bom.*,".TB_PREF."stock_master.description FROM "
					.TB_PREF."bom,".TB_PREF."stock_master
					WHERE id=".db_escape($selected_id)."
					AND ".TB_PREF."stock_master.stock_id=".TB_PREF."bom.component";

				$result = db_query($sql, "could not get bom");
				$myrow = db_fetch($result);

				$_POST['loc_code'] = $myrow["loc_code"];
				$_POST['component'] = $myrow["component"]; // by Tom Moulton
				$_POST['workcentre_added']  = $myrow["workcentre_added"];
				$_POST['quantity'] = number_format2($myrow["quantity"], get_qty_dec($myrow["component"]));
			label_row(_("Component:"), $myrow["component"] . " - " . $myrow["description"]);
			}
			hidden('selected_id', $selected_id);
		}
		else
		{
			if (($_POST['type'] != WO_ADVANCED_UNASSEMBLY))
			display_error('zxczxc');
			start_row();
			label_cell(_("Component:"));

			echo "<td>";
			echo stock_component_items_list('component', $selected_parent, null, false, true);
			if (get_post('_component_update')) 
			{
				$Ajax->activate('quantity');
			}
			echo "</td>";
			end_row();
		}
		hidden('stock_id', $selected_parent);

		locations_list_row(_("Location to Draw From:"), 'loc_code', null);
		workcenter_list_row(_("Work Centre Added:"), 'workcentre_added', null);
		$dec = get_qty_dec(get_post('component'));
		$_POST['quantity'] = number_format2(input_num('quantity',1), $dec);
		qty_row(_("Quantity:"), 'quantity', null, null, null, $dec);

		end_table(1);
		submit_add_or_update_center($selected_id == '', '', 'both');
		end_form();
	}

	// ----------------------------------------------------------------------------------
}	// if advanced assembly



function copy_from_cart(){
	$_POST['_component_edit'] = $_SESSION['items']->code_id;
	$_POST['loc_code'] = $_SESSION['items']->location;
	$_POST['workcentre_added'] = $_SESSION['items']->work_centre;
	$_POST['quantity'] = $_SESSION['items']->qty;
	$_POST['units'] = $_SESSION['items']->units;
}

function copy_to_cart(){
	$_SESSION['items']->code_id = $_POST['_component_edit'];
	$_SESSION['items']->location = $_POST['loc_code'];
	$_SESSION['items']->work_centre = $_POST['work_centre_added'];
	$_SESSION['items']->qty = $_POST['qty'];
	$_SESSION['items']->units = $_POST['units'];
}

if (($_POST['type'] == WO_ADVANCED_UNASSEMBLY))
{
	// display_error('asdasd');
	if ($Mode == 'Edit') {
		submit_add_or_update_center($selected_id == '-1', '', 'both');
	}
} // if advanced unassembly


// >>>>>>> .r79
end_page();

?>