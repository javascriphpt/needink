<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_TRANSFERPRODUCTIONPERSONNEL';
$path_to_root = "..";
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/manufacturing.inc");

include_once($path_to_root . "/manufacturing/includes/manufacturing_db.inc");
include_once($path_to_root . "/manufacturing/includes/manufacturing_ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Transfer Production Personnel"), false, false, "", $js);

if (isset($_GET["trans_no"]))
{
	$selected_id = $_GET["trans_no"];
}
elseif (isset($_POST["selected_id"]))
{
	$selected_id = $_POST["selected_id"];
}
else
{
	display_note("This page must be called with a work order reference");
	exit;
}

//------------------------------------------------------------------------------------
if (isset($_POST['release']))
{
	$err = false;
	
	$count = count($_POST['check']);
	$x=1;
	$item_categories_def = '';
	foreach($_POST['check'] as $key=>$val){
		if($count == $x)
			$item_categories_def .= $key;
		else
			$item_categories_def .= $key.",";
			
		$x++;
	}
	if ($item_categories_def == '')
	{
		display_error(_("Please choose who will assemble the work order."));
		set_focus('check');
		$err = true;	
	}
		
	if($err == false)
	{
		$update_sql = "UPDATE ".TB_PREF."workorders 
			SET production_personnel=".db_escape($item_categories_def)." 
			WHERE id=".db_escape($selected_id);
		db_query($update_sql, "could not be updated production personnel of workorder");
		
		display_notification(_("The work order has been transfered successfully."));

		display_note(get_trans_view_str(ST_WORKORDER, $selected_id, _("View this Work Order")));

		hyperlink_no_params("search_work_orders.php", _("Select another &work order"));

		$Ajax->activate('_page_body');
		end_page();
		exit;
	}
}

//------------------------------------------------------------------------------------

start_form();

$myrow = get_work_order($selected_id);

start_table($table_style2);

label_row(_("Work Order #:"), $myrow["form_type_no"]);

$sql_production = "SELECT production_personnel_code,production_personnel_name FROM ".TB_PREF."production_personnel WHERE inactive = 0";
$res_production = db_query($sql_production);
while($data_production = db_fetch($res_production)){
	check_row("<font color=red>".$data_production['production_personnel_name']."</font>",'check['.$data_production['production_personnel_code'].']');
}

end_table(1);

submit_center('release', _("Release Work Order"), true, '', 'default');

hidden('selected_id', $selected_id);

end_form();

end_page();

?>