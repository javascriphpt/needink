<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/manufacturing/includes/wo_ui.inc");

//--------------------------------------------------------------------------------

function add_to_order(&$order, $new_item, $new_item_qty, $standard_cost, $comment, $loc_code_bom, $work_centre_bom)
{
    if ($order->find_cart_item($new_item))
         display_error(_("For Part :") . $new_item . " " . "This item is already on this order.  You can change the quantity ordered of the existing line if necessary.");
	else
         $order->add_to_cart (count($order->line_items), $new_item, $new_item_qty, $standard_cost, "", $comment, $loc_code_bom, $work_centre_bom);
}

//--------------------------------------------------------------------------------

function display_order_header(&$order)
{
	global $table_style2, $Refs;

	start_outer_table("width=70% $table_style2"); // outer table
	table_section(1);
	
	if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
		locations_list_cells_user(_("Location:"), 'StockLocation', null, false, true, $_SESSION["wa_current_user"]->default_location);
	else
		locations_list_row(_("Location:"), 'StockLocation', null);
		
	ref_row(_("Reference:"), 'ref', '', $Refs->get_next(ST_INVADJUST));

	table_section(2, "33%");

    date_row(_("Date:"), 'AdjDate', '', true);

	table_section(3, "33%");

    movement_types_list_row(_("Detail:"), 'type', null);

    if (!isset($_POST['Increase']))
    	$_POST['Increase'] = 1;
    yesno_list_row(_("Type:"), 'Increase', $_POST['Increase'],
    	_("Positive Adjustment"), _("Negative Adjustment"));

	end_outer_table(1); // outer table
}

//---------------------------------------------------------------------------------

function display_adjustment_items($title, &$order)
{
	global $table_style, $path_to_root;
	display_heading($title);
    div_start('items_table');
	start_table("$table_style width=50%");
	$th = array(_("Code"), _("Description"), _("Location"), _("QOH"), _("Work Centre"), _("Quantity"), _("Units"), "");
	if ( count($order->line_items)) $th[] = '';

	table_header($th);
	$k = 0;  //row colour counter

	$id = find_submit('Edit_un');
	foreach ($order->line_items as $line_no=>$stock_item)
	{

		if ($id != $line_no)
		{
    		alt_table_row_color($k);
			
			$qoh = get_qoh_on_date($stock_item->stock_id, $stock_item->loc_code_bom);

			view_stock_status_cell($stock_item->stock_id);
			label_cell($stock_item->item_description);
			label_cell(get_location_name($stock_item->loc_code_bom));
			qty_cell($qoh, false, get_qty_dec($stock_item->stock_id));
			label_cell(get_workcentre_name($stock_item->work_centre_bom));
    		qty_cell($stock_item->quantity, false, get_qty_dec($stock_item->stock_id));
			label_cell($stock_item->units);

			edit_button_cell("Edit_un$line_no", _("Edit"),
				_('Edit document line'));
			delete_button_cell("Delete_un$line_no", _("Delete"),
				_('Remove line from document'));
			end_row();
		}
		else
		{
			adjustment_edit_item_controls($order, $line_no);
		}
	}

	if ($id == -1)
		adjustment_edit_item_controls($order);

    end_table();
	div_end();
}

//---------------------------------------------------------------------------------

function adjustment_edit_item_controls(&$order, $line_no=-1)
{
	global $Ajax;
	start_row();

	$dec2 = 0;
	$id = find_submit('Edit_un');
	if ($line_no != -1 && $line_no == $id)
	{
		$_POST['stock_id_un'] = $order->line_items[$id]->stock_id;
		$_POST['qty'] = qty_format($order->line_items[$id]->quantity, 
			$order->line_items[$id]->stock_id, $dec);
		$_POST['comment'] = $order->line_items[$id]->comment;
		$_POST['loc_code_bom'] = $order->line_items[$id]->loc_code_bom;
		$_POST['work_centre_bom'] = $order->line_items[$id]->work_centre_bom;
		$units = $order->line_items[$id]->units;

		hidden('stock_id_un', $_POST['stock_id_un']);
		label_cell($_POST['stock_id_un']);
		label_cell($order->line_items[$id]->item_description, 'nowrap');
	    $Ajax->activate('items_table');
	}
	else
	{
    	stock_costable_items_list_cells(null, 'stock_id_un', null, false, true);
		if (list_updated('stock_id_un')) {
			    $Ajax->activate('qty');
			    $Ajax->activate('qoh');
		}

    	$item_info = get_item_edit_info($_POST['stock_id_un']);
		$units = $item_info["units"];
		$dec = $item_info['decimals'];
   		$_POST['qty'] = number_format2(0, $dec);		
		$_POST['comment'] = '';
		$qoh = 0;
	}

	locations_list_cells('', 'loc_code_bom', '', false, true);
	
	if (get_post('loc_code_bom')) 
	{
		$Ajax->activate('qoh');
		$qoh = get_qoh_on_date($_POST['stock_id_un'], $_POST['loc_code_bom']);
	}
	
	label_cell($qoh, '', 'qoh');
	
	workcenter_list_cells('', 'work_centre_bom');
	
	qty_cells(null, 'qty', $_POST['qty'], null, null, $dec);
	label_cell($units, '', 'units');

	if ($id != -1)
	{
		button_cell('UpdateItem_un', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
		button_cell('CancelItemChanges_un', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
		hidden('LineNo', $line_no);
 		set_focus('qty');
	}
	else
	{
		submit_cells('AddItem_un', _("Add Item"), "colspan=2",
		    _('Add new item to document'), true);
	}

	end_row();
}


//---------------------------------------------------------------------------------

function adjustment_options_controls()
{
	  echo "<br>";
	  start_table();

	  textarea_row(_("Memo"), 'memo_', null, 50, 3);

	  end_table(1);
}


//---------------------------------------------------------------------------------

?>