<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_WORKORDERENTRY';
$path_to_root = "..";

include_once($path_to_root . "/manufacturing/includes/unassembly_class.inc");
include_once($path_to_root . "/manufacturing/includes/unassembly_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

include_once($path_to_root . "/manufacturing/includes/wo_class.inc");
include_once($path_to_root . "/manufacturing/includes/wo_ui.inc");
include_once($path_to_root . "/inventory/includes/inventory_db.inc");

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/manufacturing.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/manufacturing/includes/manufacturing_db.inc");
include_once($path_to_root . "/manufacturing/includes/manufacturing_ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Work Order Entry"), false, false, "", $js);

simple_page_mode(true);

check_db_has_manufacturable_items(_("There are no manufacturable items defined in the system."));

check_db_has_locations(("There are no inventory locations defined in the system."));

//---------------------------------------------------------------------------------------

if (isset($_GET['trans_no']))
{
	$selected_id_2 = $_GET['trans_no'];
}
elseif(isset($_POST['selected_id_2']))
{
	$selected_id_2 = $_POST['selected_id_2'];
}

//---------------------------------------------------------------------------------------

if (isset($_GET['AddedID']))
{
	$id = $_GET['AddedID'];
	$stype = ST_WORKORDER;

	display_notification_centered(_("The work order been added."));

    display_note(get_trans_view_str($stype, $id, _("View this Work Order")));

	if ($_GET['type'] != WO_ADVANCED)
	{
		include_once($path_to_root . "/reporting/includes/reporting.inc");
    	$ar = array('PARAM_0' => $id, 'PARAM_1' => $id, 'PARAM_2' => 0); 
    	display_note(print_link(_("Print this Work Order"), 409, $ar), 1);
    	$ar['PARAM_2'] = 1;
    	display_note(print_link(_("Email this Work Order"), 409, $ar), 1);
    	display_note(get_gl_view_str($stype, $id, _("View the GL Journal Entries for this Work Order")), 1);
    	$ar = array('PARAM_0' => $_GET['date'], 'PARAM_1' => $_GET['date'], 'PARAM_2' => $stype); 
    	display_note(print_link(_("Print the GL Journal Entries for this Work Order"), 702, $ar), 1);
	}
	
	safe_exit();
}

//---------------------------------------------------------------------------------------

if (isset($_GET['UpdatedID']))
{
	$id = $_GET['UpdatedID'];

	display_notification_centered(_("The work order been updated."));
	safe_exit();
}

//---------------------------------------------------------------------------------------

if (isset($_GET['DeletedID']))
{
	$id = $_GET['DeletedID'];

	display_notification_centered(_("Work order has been deleted."));
	safe_exit();
}

//---------------------------------------------------------------------------------------

if (isset($_GET['ClosedID']))
{
	$id = $_GET['ClosedID'];

	display_notification_centered(_("This work order has been closed. There can be no more issues against it.") . " #$id");
	safe_exit();
}

//---------------------------------------------------------------------------------------

function safe_exit()
{
	global $path_to_root;

	hyperlink_no_params("", _("Enter a new work order"));
	hyperlink_no_params("search_work_orders.php", _("Select an existing work order"));
	
	display_footer_exit();
}

//-------------------------------------------------------------------------------------
if (!isset($_POST['date_']))
{
	$_POST['date_'] = new_doc_date();
	if (!is_date_in_fiscalyear($_POST['date_']))
		$_POST['date_'] = end_fiscalyear();
}

function can_process()
{
	global $selected_id_2, $SysPrefs, $Refs;

	if (!isset($selected_id_2))
	{
    	if (!$Refs->is_valid($_POST['wo_ref']))
    	{
    		display_error(_("You must enter a reference."));
			set_focus('wo_ref');
    		return false;
    	}

    	if (!is_new_reference($_POST['wo_ref'], ST_WORKORDER))
    	{
    		display_error(_("The entered reference is already in use."));
			set_focus('wo_ref');
    		return false;
    	}
	}

	if (!check_num('quantity_2', 0))
	{
		display_error( _("The quantity entered is invalid or less than zero."));
		set_focus('quantity_2');
		return false;
	}

	if (!is_date($_POST['date_']))
	{
		display_error( _("The date entered is in an invalid format."));
		set_focus('date_');
		return false;
	}
	elseif (!is_date_in_fiscalyear($_POST['date_']))
	{
		display_error(_("The entered date is not in fiscal year."));
		set_focus('date_');
		return false;
	}
	// only check bom and quantites if quick assembly
	if (!($_POST['type'] == WO_ADVANCED))
	{
        if (!has_bom($_POST['stock_id_2']))
        {
        	display_error(_("The selected item to manufacture does not have a bom."));
			set_focus('stock_id_2');
        	return false;
        }

		if ($_POST['Labour'] == "")
			$_POST['Labour'] = price_format(0);
    	if (!check_num('Labour', 0))
    	{
    		display_error( _("The labour cost entered is invalid or less than zero."));
			set_focus('Labour');
    		return false;
    	}
		if ($_POST['Costs'] == "")
			$_POST['Costs'] = price_format(0);
    	if (!check_num('Costs', 0))
    	{
    		display_error( _("The cost entered is invalid or less than zero."));
			set_focus('Costs');
    		return false;
    	}

        if (!$SysPrefs->allow_negative_stock())
        {
        	if ($_POST['type'] == WO_ASSEMBLY)
        	{
        		// check bom if assembling
                $result = get_bom($_POST['stock_id_2']);

            	while ($bom_item = db_fetch($result))
            	{

            		if (has_stock_holding($bom_item["ResourceType"]))
            		{

                		$quantity_2 = $bom_item["quantity"] * input_num('quantity_2');

                        $qoh = get_qoh_on_date($bom_item["component"], $bom_item["loc_code"], $_POST['date_']);
                		if (-$quantity_2 + $qoh < 0)
                		{
                			display_error(_("The work order cannot be processed because there is an insufficient quantity for component:") .
                				" " . $bom_item["component"] . " - " .  $bom_item["description"] . ".  " . _("Location:") . " " . $bom_item["location_name"]);
							set_focus('quantity_2');
        					return false;
                		}
            		}
            	}
        	}
        	elseif ($_POST['type'] == WO_UNASSEMBLY)
        	{
        		// if unassembling, check item to unassemble
				$qoh = get_qoh_on_date($_POST['stock_id_2'], $_POST['StockLocation'], $_POST['date_']);
        		if (-input_num('quantity_2') + $qoh < 0)
        		{
        			display_error(_("The selected item cannot be unassembled because there is insufficient stock."));
					return false;
        		}
        	}
    	}
     }
     else
     {
    	if (!is_date($_POST['RequDate']))
    	{
			set_focus('RequDate');
    		display_error( _("The date entered is in an invalid format."));
    		return false;
		}
		//elseif (!is_date_in_fiscalyear($_POST['RequDate']))
		//{
		//	display_error(_("The entered date is not in fiscal year."));
		//	return false;
		//}
    	if (isset($selected_id_2))
    	{
    		$myrow = get_work_order($selected_id_2, true);

    		if ($_POST['units_issued'] > input_num('quantity_2'))
    		{
				set_focus('quantity_2');
    			display_error(_("The quantity cannot be changed to be less than the quantity already manufactured for this order."));
        		return false;
    		}
    	}
	}

	return true;
}

//-------------------------------------------------------------------------------------

if (isset($_POST['ADD_ITEM_2']) && can_process())
{
	// if (!isset($_POST['cr_acc']))
		// $_POST['cr_acc'] = "";
	// if (!isset($_POST['cr_lab_acc']))
		// $_POST['cr_lab_acc'] = "";
	// $id = add_work_order($_POST['wo_ref'], $_POST['StockLocation'], input_num('quantity_2'),
		// $_POST['stock_id_2'],  $_POST['type'], $_POST['date_'],
		// $_POST['RequDate'], $_POST['memo_'], input_num('Costs'), $_POST['cr_acc'], input_num('Labour'), $_POST['cr_lab_acc']);

	// new_doc_date($_POST['date_']);
	// meta_forward($_SERVER['PHP_SELF'], "AddedID=$id&type=".$_POST['type']."&date=".$_POST['date_']);
	
	print_r($_SESSION['PO']);
	
	foreach ($_SESSION['PO']->line_items as $po_line)
    {
		display_error($po_line->stock_id);
	}
	
	unset ($_SESSION['PO']);
}

//-------------------------------------------------------------------------------------

if (isset($_POST['UPDATE_ITEM_2']) && can_process())
{

	update_work_order($selected_id_2, $_POST['StockLocation'], input_num('quantity_2'),
		$_POST['stock_id_2'],  $_POST['date_'], $_POST['RequDate'], $_POST['memo_']);
	new_doc_date($_POST['date_']);
	meta_forward($_SERVER['PHP_SELF'], "UpdatedID=$selected_id_2");
}

//--------------------------------------------------------------------------------------

if (isset($_POST['delete_2']))
{
	//the link to delete a selected record was clicked instead of the submit button

	$cancel_delete = false;

	// can't delete it there are productions or issues
	if (work_order_has_productions($selected_id_2) ||
		work_order_has_issues($selected_id_2)	||
		work_order_has_payments($selected_id_2))
	{
		display_error(_("This work order cannot be deleted because it has already been processed."));
		$cancel_delete = true;
	}

	if ($cancel_delete == false)
	{ //ie not cancelled the delete as a result of above tests

		// delete the actual work order
		delete_work_order($selected_id_2);
		meta_forward($_SERVER['PHP_SELF'], "DeletedID=$selected_id_2");
	}
}

//-------------------------------------------------------------------------------------

if (isset($_POST['close_2']))
{

	// update the closed flag in the work order
	close_work_order($selected_id_2);
	meta_forward($_SERVER['PHP_SELF'], "ClosedID=$selected_id_2");
}

//-------------------------------------------------------------------------------------
if (get_post('_type_update')) 
{
  $Ajax->activate('_page_body');
}
//-------------------------------------------------------------------------------------

start_form();

start_table($table_style2);

$existing_comments = "";

$dec = 0;
if (isset($selected_id_2))
{
	$myrow = get_work_order($selected_id_2);

	if (strlen($myrow[0]) == 0)
	{
		echo _("The order number sent is not valid.");
		safe_exit();
	}

	// if it's a closed work order can't edit it
	if ($myrow["closed"] == 1)
	{
		echo "<center>";
		display_error(_("This work order is closed and cannot be edited."));
		safe_exit();
	}

	$_POST['wo_ref'] = $myrow["wo_ref"];
	$_POST['stock_id_2'] = $myrow["stock_id"];
	$_POST['quantity_2'] = qty_format($myrow["units_reqd"], $_POST['stock_id_2'], $dec);
	$_POST['StockLocation'] = $myrow["loc_code"];
	$_POST['released'] = $myrow["released"];
	$_POST['closed'] = $myrow["closed"];
	$_POST['type'] = $myrow["type"];
	$_POST['date_'] = sql2date($myrow["date_"]);
	$_POST['RequDate'] = sql2date($myrow["required_by"]);
	$_POST['released_date'] = sql2date($myrow["released_date"]);
	$_POST['memo_'] = "";
	$_POST['units_issued'] = $myrow["units_issued"];
	$_POST['Costs'] = price_format($myrow["additional_costs"]);

	$_POST['memo_'] = get_comments_string(ST_WORKORDER, $selected_id_2);

	hidden('wo_ref', $_POST['wo_ref']);
	hidden('units_issued', $_POST['units_issued']);
	hidden('released', $_POST['released']);
	hidden('released_date', $_POST['released_date']);
	hidden('selected_id_2',  $selected_id_2);
	hidden('old_qty', $myrow["units_reqd"]);
	hidden('old_stk_id', $myrow["stock_id"]);

	label_row(_("Reference:"), $_POST['wo_ref']);
	label_row(_("Type:"), $wo_types_array[$_POST['type']]);
	hidden('type', $myrow["type"]);
}
else
{
	$_POST['units_issued'] = $_POST['released'] = 0;
	ref_row(_("Reference:"), 'wo_ref', '', $Refs->get_next(ST_WORKORDER));

	//wo_types_list_row(_("Type:"), 'type', null);
	label_row(_("Item: "), 4);
	hidden('type', 4);
}

if (get_post('released'))
{
	hidden('stock_id_2', $_POST['stock_id_2']);
	hidden('StockLocation', $_POST['StockLocation']);
	hidden('type', $_POST['type']);

	label_row(_("Item:"), $myrow["StockItemName"]);
	label_row(_("Destination Location:"), $myrow["location_name"]);
}
else
{	
	if($_SESSION["wa_current_user"]->can_access_all_item_categories == 0)
		stock_manufactured_items_list_row_user(_("Item:"), 'stock_id_2', null, false, true, $_SESSION["wa_current_user"]->default_item_categories);
	else
		stock_manufactured_items_list_row(_("Item:"), 'stock_id_2', null, false, true);
		
	if (list_updated('stock_id_2'))
	{
		$Ajax->activate('quantity_2');
		
		// if (($_POST['type'] == WO_ADVANCED_UNASSEMBLY))
			// line_start_focus();
	}

	locations_list_row(_("Destination Location:"), 'StockLocation', null);
}

if (!isset($_POST['quantity_2']))
	$_POST['quantity_2'] = qty_format(1, $_POST['stock_id_2'], $dec);
else
	$_POST['quantity_2'] = qty_format($_POST['quantity_2'], $_POST['stock_id_2'], $dec);
	

if (get_post('type') == WO_ADVANCED)
{
    qty_row(_("Quantity Required:"), 'quantity_2', null, null, null, $dec);
    if ($_POST['released'])
    	label_row(_("Quantity Manufactured:"), number_format($_POST['units_issued'], get_qty_dec($_POST['stock_id_2'])));
    date_row(_("Date") . ":", 'date_', '', true);
	date_row(_("Date Required By") . ":", 'RequDate', '', null, $SysPrefs->default_wo_required_by());
}
else
{
    qty_row(_("Quantity:"), 'quantity_2', null, null, null, $dec);
    date_row(_("Date") . ":", 'date_', '', true);
	hidden('RequDate', '');

	$sql = "SELECT DISTINCT account_code FROM ".TB_PREF."bank_accounts";
	$rs = db_query($sql,"could not get bank accounts");
	$r = db_fetch_row($rs);
	if (!isset($_POST['Labour']))
	{
		$_POST['Labour'] = price_format(0);
		$_POST['cr_lab_acc'] = $r[0];
	}
	amount_row($wo_cost_types[WO_LABOUR], 'Labour');
	gl_all_accounts_list_row(_("Credit Labour Account"), 'cr_lab_acc', null);
	if (!isset($_POST['Costs']))
	{
		$_POST['Costs'] = price_format(0);
		$_POST['cr_acc'] = $r[0];
	}
	amount_row($wo_cost_types[WO_OVERHEAD], 'Costs');
	gl_all_accounts_list_row(_("Credit Overhead Account"), 'cr_acc', null);
	
}

if (get_post('released'))
	label_row(_("Released On:"),$_POST['released_date']);

textarea_row(_("Memo:"), 'memo_', null, 40, 5);

end_table(1);

if (isset($selected_id_2))
{
	echo "<table align=center><tr>";

	submit_cells('UPDATE_ITEM_2', _("Update"), '', _('Save changes to work order'), 'default');
	if (get_post('released'))
	{
		submit_cells('close_2', _("Close This Work Order"),'','',true);
	}
	submit_cells('delete_2', _("Delete This Work Order"),'','',true);

	echo "</tr></table>";
}
else
{
	submit_center('ADD_ITEM_2', _("Add Workorder"), true, '', 'default');
}


//--------------------------------------------------------------------------------------------------

function copy_from_cart()
{
	$_POST['supplier_id'] = $_SESSION['PO']->supplier_id;
	$_POST['OrderDate'] = $_SESSION['PO']->orig_order_date;
    $_POST['Requisition'] = $_SESSION['PO']->requisition_no;
    $_POST['ref'] = $_SESSION['PO']->reference;
	$_POST['Comments'] = $_SESSION['PO']->Comments;
    $_POST['StkLocation'] = $_SESSION['PO']->Location;
    $_POST['delivery_address'] = $_SESSION['PO']->delivery_address;
}

function copy_to_cart()
{
	$_SESSION['PO']->supplier_id = $_POST['supplier_id'];
	$_SESSION['PO']->orig_order_date = $_POST['OrderDate'];
	$_SESSION['PO']->reference = $_POST['ref'];
	$_SESSION['PO']->requisition_no = $_POST['Requisition'];
	$_SESSION['PO']->Comments = $_POST['Comments'];
	$_SESSION['PO']->Location = $_POST['StkLocation'];
	$_SESSION['PO']->delivery_address = $_POST['delivery_address'];
}
//--------------------------------------------------------------------------------------------------

function line_start_focus() {
  global 	$Ajax;

  $Ajax->activate('items_table');
  set_focus('_stock_id_edit');
}
//--------------------------------------------------------------------------------------------------

function unset_form_variables() {
	unset($_POST['stock_id']);
    unset($_POST['qty']);
    unset($_POST['price']);
    unset($_POST['req_del_date']);
}

//---------------------------------------------------------------------------------------------------

function handle_delete_item($line_no)
{
	if($_SESSION['PO']->some_already_received($line_no) == 0)
	{
		$_SESSION['PO']->remove_from_order($line_no);
		unset_form_variables();
	} 
	else 
	{
		display_error(_("This item cannot be deleted because some of it has already been received."));
	}	
    line_start_focus();
}

//---------------------------------------------------------------------------------------------------

function handle_cancel_po()
{
	global $path_to_root;
	
	//need to check that not already dispatched or invoiced by the supplier
	if(($_SESSION['PO']->order_no != 0) && 
		$_SESSION['PO']->any_already_received() == 1)
	{
		display_error(_("This order cannot be cancelled because some of it has already been received.") 
			. "<br>" . _("The line item quantities may be modified to quantities more than already received. prices cannot be altered for lines that have already been received and quantities cannot be reduced below the quantity already received."));
		return;
	}
	
	if($_SESSION['PO']->order_no != 0)
	{
		delete_po($_SESSION['PO']->order_no);
	} else {
		unset($_SESSION['PO']);
		meta_forward($path_to_root.'/index.php','application=AP');
	}

	$_SESSION['PO']->clear_items();
	$_SESSION['PO'] = new purch_order;

	display_notification(_("This purchase order has been cancelled."));

	hyperlink_params($path_to_root . "/purchasing/po_entry_items.php", _("Enter a new purchase order"), "NewOrder=Yes");
	echo "<br>";

	end_page();
	exit;
}

//---------------------------------------------------------------------------------------------------

function check_data()
{
	$dec = get_qty_dec($_POST['stock_id']);
	$min = 1 / pow(10, $dec);
    if (!check_num('qty',$min))
    {
    	$min = number_format2($min, $dec);
	   	display_error(_("The quantity of the order item must be numeric and not less than ").$min);
		set_focus('qty');
	   	return false;
    }

    if (!check_num('price', 0))
    {
	   	display_error(_("The price entered must be numeric and not less than zero."));
		set_focus('price');
	   	return false;	   
    }
    if (!is_date($_POST['req_del_date'])){
    		display_error(_("The date entered is in an invalid format."));
		set_focus('req_del_date');
   		return false;    	 
    }
     
    return true;	
}

//---------------------------------------------------------------------------------------------------

function handle_update_item()
{
	$allow_update = check_data(); 

	if ($allow_update)
	{
		if ($_SESSION['PO']->line_items[$_POST['line_no']]->qty_inv > input_num('qty') ||
			$_SESSION['PO']->line_items[$_POST['line_no']]->qty_received > input_num('qty'))
		{
			display_error(_("You are attempting to make the quantity ordered a quantity less than has already been invoiced or received.  This is prohibited.") .
				"<br>" . _("The quantity received can only be modified by entering a negative receipt and the quantity invoiced can only be reduced by entering a credit note against this item."));
			set_focus('qty');
			return;
		}
	
		$_SESSION['PO']->update_order_item($_POST['line_no'], input_num('qty'), input_num('price'),
  			$_POST['req_del_date']);
		unset_form_variables();
	}	
    line_start_focus();
}

//---------------------------------------------------------------------------------------------------

function handle_add_new_item()
{
	$allow_update = check_data();
	
	if ($allow_update == true)
	{ 
		if (count($_SESSION['PO']->line_items) > 0)
		{
		    foreach ($_SESSION['PO']->line_items as $order_item) 
		    {

    			/* do a loop round the items on the order to see that the item
    			is not already on this order */
   			    if (($order_item->stock_id == $_POST['stock_id']) && 
   			    	($order_item->Deleted == false)) 
   			    {
				  	$allow_update = false;
				  	display_error(_("The selected item is already on this order."));
			    }
		    } /* end of the foreach loop to look for pre-existing items of the same code */
		}

		if ($allow_update == true)
		{
		   	$sql = "SELECT description, units, mb_flag
				FROM ".TB_PREF."stock_master WHERE stock_id = ".db_escape($_POST['stock_id']);

		    $result = db_query($sql,"The stock details for " . $_POST['stock_id'] . " could not be retrieved");

		    if (db_num_rows($result) == 0)
		    {
				$allow_update = false;
		    }		    

			if ($allow_update)
		   	{
				$myrow = db_fetch($result);
				$_SESSION['PO']->add_to_order ($_POST['line_no'], $_POST['stock_id'], input_num('qty'), 
					$myrow["description"], input_num('price'), $myrow["units"],
					$_POST['req_del_date'], 0, 0);

				unset_form_variables();
				$_POST['stock_id']	= "";
	   		} 
	   		else 
	   		{
			     display_error(_("The selected item does not exist or it is a kit part and therefore cannot be purchased."));
		   	}

		} /* end of if not already on the order and allow input was true*/
    }
	line_start_focus();
}

//---------------------------------------------------------------------------------------------------

function can_commit()
{
	global $Refs;

	if (!get_post('supplier_id')) 
	{
		display_error(_("There is no supplier selected."));
		set_focus('supplier_id');
		return false;
	} 
	
	if (!is_date($_POST['OrderDate'])) 
	{
		display_error(_("The entered order date is invalid."));
		set_focus('OrderDate');
		return false;
	} 
	
	if (!$_SESSION['PO']->order_no) 
	{
    	if (!$Refs->is_valid(get_post('ref'))) 
    	{
    		display_error(_("There is no reference entered for this purchase order."));
			set_focus('ref');
    		return false;
    	} 
    	
    	if (!is_new_reference(get_post('ref'), ST_PURCHORDER)) 
    	{
    		display_error(_("The entered reference is already in use."));
			set_focus('ref');
    		return false;
    	}
	}
	
	if (get_post('delivery_address') == '')
	{
		display_error(_("There is no delivery address specified."));
		set_focus('delivery_address');
		return false;
	} 
	
	if (get_post('StkLocation') == '')
	{
		display_error(_("There is no location specified to move any items into."));
		set_focus('StkLocation');
		return false;
	} 
	
	if ($_SESSION['PO']->order_has_items() == false)
	{
     	display_error (_("The order cannot be placed because there are no lines entered on this order."));
     	return false;
	}
		
	return true;
}

//---------------------------------------------------------------------------------------------------

function handle_commit_order()
{

	if (can_commit())
	{
		copy_to_cart();

		if ($_SESSION['PO']->order_no == 0)
		{ 
			
			/*its a new order to be inserted */
			$order_no = add_po($_SESSION['PO']);
			new_doc_date($_SESSION['PO']->orig_order_date); 
			unset($_SESSION['PO']);
			 
        	meta_forward($_SERVER['PHP_SELF'], "AddedID=$order_no");	

		} 
		else 
		{ 

			/*its an existing order need to update the old order info */
			$order_no = update_po($_SESSION['PO']);
			
			unset($_SESSION['PO']);
			
        	meta_forward($_SERVER['PHP_SELF'], "AddedID=$order_no&Updated=1");	
		}
	}	
}
//---------------------------------------------------------------------------------------------------
$id = find_submit('Delete');
if ($id != -1)
	handle_delete_item($id);

if (isset($_POST['Commit']))
{
	handle_commit_order();
}
if (isset($_POST['UpdateLine']))
	handle_update_item();

if (isset($_POST['EnterLine']))
	handle_add_new_item();

if (isset($_POST['CancelOrder'])) 
	handle_cancel_po();

if (isset($_POST['CancelUpdate']))
	unset_form_variables();

if (isset($_GET['ModifyOrderNumber']) && $_GET['ModifyOrderNumber'] != "")
{
	create_new_po();
	
	$_SESSION['PO']->order_no = $_GET['ModifyOrderNumber'];	

	/*read in all the selected order into the Items cart  */
	read_po($_SESSION['PO']->order_no, $_SESSION['PO']);
	
	copy_from_cart();
}

if (isset($_POST['CancelUpdate']) || isset($_POST['UpdateLine'])) {
	line_start_focus();
}

if (isset($_GET['NewOrder']))
{
	create_new_po();
	
	if(isset($_POST['stock_id_2']))
	{
	
		$result = get_bom($_POST['stock_id_2']);
		while ($myrow = db_fetch($result))
		{
			// add_to_order($_SESSION['adj_items'], $myrow["component"], 
			  // $myrow["quantity"], 0, '', $myrow["loc_code"], $myrow["workcentre_added"]);
			// line_start_focus();
			
			$_SESSION['PO']->add_to_order ($_SESSION['PO']->lines_on_order, $myrow["component"], $myrow["quantity"], 
				"cza", 0, "units_",
				"0000-00-00", 0, 0);

		} //END WHILE LIST LOOP
	}
}
//---------------------------------------------------------------------------------------------------

echo "<br>";

display_po_items($_SESSION['PO']);

//---------------------------------------------------------------------------------------------------


end_form();
end_page();