<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_WORKORDERENTRY';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");

page(_($help_context = "Bottling"));

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/inventory/includes/db/items_units_db.inc");
// include_once($path_to_root . "/includes/banking.inc");

simple_page_mode(false);

//---------------------------------------------------------------------------------------------

function check_data()
{
	// if(strlen($_post['amount']) == 0){
		// display_error( _("Enter a value."));
		// set_focus('amount');
		// return false;	
	// }
	
	if (!check_num('freight_cost',0)) {
			display_error(_("The shipping cost entered is expected to be numeric."));
			set_focus('freight_cost');
			return false;
		}
	
	return true;
}

if($_POST['update']){
	// if(check_data())
	{
		$update = "UPDATE ".TB_PREF."bottling 
						SET val2 = ".db_escape($_POST['rate'])." 
						WHERE id = 1";
						// display_error($update);
		db_query($update);
		
		display_notification('Updated conversion rate successfully.');
	}
}

//---------------------------------------------------------------------------------------------

function get_bottling(){
	$sql = "SELECT *
			FROM ".TB_PREF."bottling";
	$sql = db_query($sql);
	return $sql;
}

function display_bottling()
{
	global $table_style;
	
	start_table($table_style);
	
	$sql = get_bottling();
	
	$th = array('From','To');
	table_header($th);	
	
	while($data = db_fetch($sql)){
		label_cell($data["val1"].' '.get_unit_name($data["from_units"]));
		label_cell($data["val2"].' '.get_unit_name($data["to_units"]));
		// edit_button_cell("Edit".$myrow["curr_abrev"], _("Edit"));
	}
}

//---------------------------------------------------------------------------------------------

function display_bottling_update($selected_id)
{
	global $table_style2, $Mode;
	
	start_table($table_style2);

	$rate = get_bottling();
	$data = db_fetch($rate);
	echo "<br>";
	$th = array('Conversion Rate');
	table_header($th,'colspan=3');	
	
	label_cell($data["val1"].' '.get_unit_name($data["from_units"])._(" = "));
	$_POST['rate']=price_format($data['val2']);
	amount_cells(null, 'rate');
	label_cell(get_unit_name($data['to_units']));
	
	end_table(1);

	submit_center('update', 'Update');
}

start_form();
display_bottling();

display_bottling_update($selected_id);
end_form();
//---------------------------------------------------------------------------------------------

end_page();

?>
