<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PRODUCTION_PERSONNEL';
$path_to_root = "../..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Production Personnel"));

include($path_to_root . "/includes/ui.inc");

simple_page_mode(true);
//------------------------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM' || $Mode=='UPDATE_ITEM') 
{

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	if (strlen($_POST['production_personnel_name']) == 0)
	{
		$input_error = 1;
		display_error(_("The sales person name cannot be empty."));
		set_focus('production_personnel_name');
	}
	if ($input_error != 1)
	{
    	if ($selected_id != -1) 
    	{
    		/*selected_id could also exist if submit had not been clicked this code would not run in this case cos submit is false of course  see the delete code below*/

    		$sql = "UPDATE ".TB_PREF."production_personnel SET production_personnel_name=".db_escape($_POST['production_personnel_name']) . ",
    			production_personnel_phone=".db_escape($_POST['production_personnel_phone']) . ",
    			production_personnel_fax=".db_escape($_POST['production_personnel_fax']) . ",
    			production_personnel_email=".db_escape($_POST['production_personnel_email']) . "
    			WHERE production_personnel_code = ".db_escape($selected_id);
    	}
    	else
    	{
    		/*Selected group is null cos no item selected on first time round so must be adding a record must be submitting new entries in the new Sales-person form */
    		$sql = "INSERT INTO ".TB_PREF."production_personnel (production_personnel_name, production_personnel_phone, production_personnel_fax, 
				production_personnel_email)
    			VALUES (".db_escape($_POST['production_personnel_name']) . ", "
				  .db_escape($_POST['production_personnel_phone']) . ", "
				  .db_escape($_POST['production_personnel_fax']) . ", "
				  .db_escape($_POST['production_personnel_email']) . ")";
    	}

    	//run the sql from either of the above possibilites
    	db_query($sql,"The insert or update of the production personnel failed");
    	if ($selected_id != -1) 
			display_notification(_('Selected production personnel data have been updated'));
		else
			display_notification(_('New production personnel data have been added'));
		$Mode = 'RESET';
	}
}
if ($Mode == 'Delete')
{
	//the link to delete a selected record was clicked instead of the submit button

	$sql="DELETE FROM ".TB_PREF."production_personnel WHERE production_personnel_code=".db_escape($selected_id);
	db_query($sql,"The production personnel could not be deleted");
	display_notification(_('Selected production personnel data have been deleted'));

	$Mode = 'RESET';
}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$sav = get_post('show_inactive');
	unset($_POST);
	$_POST['show_inactive'] = $sav;
}
//------------------------------------------------------------------------------------------------

$sql = "SELECT * FROM ".TB_PREF."production_personnel";
if (!check_value('show_inactive')) $sql .= " WHERE !inactive";
$result = db_query($sql,"could not get sales persons");

start_form();
start_table("$table_style width=60%");
$th = array(_("Name"), _("Phone"), _("Fax"), _("Email"), "", "");
inactive_control_column($th);
table_header($th);

$k = 0;

while ($myrow = db_fetch($result))
{

	alt_table_row_color($k);

    label_cell($myrow["production_personnel_name"]);
   	label_cell($myrow["production_personnel_phone"]);
   	label_cell($myrow["production_personnel_fax"]);
	email_cell($myrow["production_personnel_email"]);
	inactive_control_cell($myrow["production_personnel_code"], $myrow["inactive"],
		'production_personnel', 'production_personnel_code');
 	edit_button_cell("Edit".$myrow["production_personnel_code"], _("Edit"));
 	delete_button_cell("Delete".$myrow["production_personnel_code"], _("Delete"));
  	end_row();

} //END WHILE LIST LOOP

inactive_control_row($th);
end_table();
echo '<br>';

//------------------------------------------------------------------------------------------------

$_POST['production_personnel_email'] = "";
if ($selected_id != -1) 
{
 	if ($Mode == 'Edit') {
		//editing an existing Sales-person
		$sql = "SELECT *  FROM ".TB_PREF."production_personnel WHERE production_personnel_code=".db_escape($selected_id);

		$result = db_query($sql,"could not get sales person");
		$myrow = db_fetch($result);

		$_POST['production_personnel_name'] = $myrow["production_personnel_name"];
		$_POST['production_personnel_phone'] = $myrow["production_personnel_phone"];
		$_POST['production_personnel_fax'] = $myrow["production_personnel_fax"];
		$_POST['production_personnel_email'] = $myrow["production_personnel_email"];
	}
	hidden('selected_id', $selected_id);
}

start_table($table_style2);

text_row_ex(_("Sales person name:"), 'production_personnel_name', 30);
text_row_ex(_("Telephone number:"), 'production_personnel_phone', 20);
text_row_ex(_("Fax number:"), 'production_personnel_fax', 20);
email_row_ex(_("E-mail:"), 'production_personnel_email', 40);
end_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');

end_form();

end_page();

?>
