<?php
$path_to_root="../..";

$page_security = 'SA_APVVOUCHER';
include_once($path_to_root . "/modules/checkprint/includes/batch_cv_cart.inc");
include($path_to_root . "/includes/session.inc");

include($path_to_root . "/purchasing/includes/purchasing_ui.inc");
include($path_to_root . "/modules/checkprint/includes/check_accounts_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_("Cheque Number Issue"), false, false, "", $js);

if (isset($_POST['type']))
	$_GET['type_id'] = $_POST['type'];
if (isset($_POST['trans_no']))
	$_GET['trans_no'] = $_POST['trans_no'];	
if (!isset($_POST['num_cheque']))
	$_POST['num_cheque']=1;	
if(isset($_POST['batchIssueCV'])){
	if (isset($_POST['check'])){
		if(count($_POST['check'])==1){
			meta_forward("$path_to_root/modules/checkprint/check_issue.php", "type_id=45&trans_no=".$_POST['check'][0]);
			end_page();
		}else{
			unset($_SESSION['cart']);
			$_SESSION['cart']=new items_cart();
			session_register($_SESSION['cart']);
			foreach($_POST['check'] as $key=>$value){
				$_SESSION['cart']->add_to_cart($_SESSION['cart']->counter,$value);
			}
		}
	}else{
		display_error(_("No Transaction Selected"), true);
		end_page();
		exit;
	}
}
/*
if(isset($_POST['batchIssueCV'])){
	$_SESSION['batch_check']=$_POST['check'];
}
	*/
	
//----------------------------------------------------------------------------------------

check_db_has_suppliers(_("There are no suppliers defined in the system."));

check_db_has_bank_accounts(_("There are no bank accounts defined in the system."));

check_db_has_bank_trans_types(_("There are no bank payment types defined in the system."));

//---------------------------------------------------------------------------------------------------------------

if (isset($_GET['IssuedID']))
{
	$check_no = $_GET['IssuedID'];

    echo "<center>";
    display_notification_centered(_("Cheque Number has been assigned."));

	//display_note(print_document_link($invoice_no, _("Print This Cheque"), true, 10));

    hyperlink_params("$path_to_root/modules/checkprint/check_list.php", _("Issue Another Cheque"), "");


	display_footer_exit();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------
function allocate_cv_to_invoice($amount,  $lastCV, $trans_no_to)
{
	$sql = "INSERT INTO ".TB_PREF."supp_allocations (amt,date_alloc,trans_no_from,trans_type_from,trans_no_to,trans_type_to)
				VALUES($amount,curdate(),".db_escape($lastCV).",50,$trans_no_to,20)";
	
	db_query($sql,"failed to insert allocations");
	
	$sql = "UPDATE ".TB_PREF."supp_trans
				SET alloc = alloc+$amount
				WHERE trans_no = $trans_no_to
				AND type = 20";
	db_query($sql,"failed to update allocation");
				
}


function check_sum(){
	$x=0;
	$sub=0;
	do{
		$sub+=round(input_num('amount'.$x),2);
		$x++;
	}while($x<$_POST['num_cheque']);
	
	$total=input_num('total_cheque');
	
	$returnMe=true;
	
	if($sub<$total){
		display_error('Cheques total amount must match Debit : Short by '.number_format2($total-$sub,2));
		$returnMe=false;
	}
	if($sub>$total){
		display_error('Cheques total amount must match Debit : Off by '.number_format2($sub-$total,2));
		$returnMe=false;
	}
		
	return $returnMe;
}


function check_reference_ok($type, $trans_no, $reference,$x) {

	// Check Posts of Cheque No is not already taken.
	if (!references::is_valid($reference))
    {
		display_error(_("You must enter a cheque number."));
		set_focus('next_reference'.$x);
		return false;
	}

	if (!is_new_cheque($type, $trans_no, $reference))
	{
		display_error(_("The entered cheque number is already in use."));
		set_focus('next_reference'.$x);
		return false;
	}

	return true;

}

function save_last_cheque($bank_ref, $check_ref)
{
	$next = references::increment($check_ref);
	save_next_check_reference($bank_ref, $next);
}

if (isset($_POST['skip'])){
	if($_POST['num_cheque'] < 1){
		global $Ajax;
		display_error('Invalid Number of Checks : Reset to 1');
		$_POST['num_cheque']=1;
		$Ajax->activate('num_cheque');
	}else{
		$Ajax->activate('info');	
		//$Ajax->deactivate('info');	
	}
}

//redo checking
//
if (isset($_POST['POST_CHECK'])) {

	$x=0;
	
	do{
		foreach($_SESSION['cart']->line_items as $key=>$value){
			if (!check_reference_ok(45, $value->apv_no, $_POST['next_reference'.$x],$x))
				return;
		}
		$x++;
	}while($x<$_POST['num_cheque']);

	if(!check_sum())
		return;
	 //Also has DB constraints to not allow duplicates Cheque Refs
	
	// $lastCV=references::get_next(50);
	// references::save_last($lastCV, 50);
	
	$lastCV = $Refs->get_next(50);		
	$Refs->save_last($lastCV, 50);
	
	$_POST['type'] = 50;
	$totalAmount=0;
	$apvs='';
	foreach($_SESSION['cart']->line_items as $key=>$value){
		if(strlen($apvs)>0)
			$apvs.=',';
		$apvs.=$value->apv_no;
		//die(display_error($apvs));
		$_POST['trans_no']=$value->apv_no;
		// $sql='UPDATE '.TB_PREF.'apv_header SET alloc = '. $value->amount .', cv = '.$lastCV.' WHERE id = "'.$_POST['trans_no'].'"';
		// db_query($sql,"Error Update -> header");
		$sql = "SELECT * FROM ".TB_PREF."apv_header WHERE id = '".$_POST['trans_no']."'";
		$sql = db_fetch(db_query($sql));
		
		$value->account = $sql['voucher_account'];
		
		
		// change
		$sql = "SELECT a.*, b.alloc
				from 0_gl_trans a
				JOIN 0_apv_header b ON b.id = a.type_no and b.id = '".$_POST['trans_no']."'
				WHERE a.type= 45 and a.account = '".$value->account."'";
		//$sql="select SUM(amount) FROM ".TB_PREF."apv_details WHERE apv_no='".$_POST['trans_no']."'AND account = '9000000'";
		$result=db_query($sql,"Cannot get APV_DETAILS");
		$amount=db_fetch($result);
		$value->amount=abs($amount['amount'])-$amount['alloc'];
		
		// $value->sl=$amount['sl'];
		// $value->dimension_id=$amount['dimension_id'];
		// $value->dimension2_id=$amount['dimension2_id'];
		// $value->dimension3_id=$amount['dimension3_id'];
		// $value->dimension4_id=$amount['dimension4_id'];
		// $value->dimension5_id=$amount['dimension5_id'];
		$totalAmount+=$amount[0];
	}
		//die(display_error($apvs));
		
		$sql="select * FROM ".TB_PREF."apv_header WHERE id='".$_POST['trans_no']."'";
		$result=db_query($sql,"Cannot get APV_HEADER");
		$row=db_fetch($result);
		
		switch($row['cust_supp']){
			case 0://misc;
				$curr='PHP';
			break;
			case 2:
				$curr=get_customer_currency($row['cust_supp_id']);
			break;
			case 3:
				$curr=get_supplier_currency($row['cust_supp_id']);
			break;
		}
		$x=0;
		do{
			$postamount=0;
			$postamount=round(input_num('amount'.$x),2);
			
			//2009
			//$sql='INSERT INTO '.TB_PREF.'check_voucher(cv_no,cv_date,cheque_no,apv_no,amount) VALUES('.$lastCV.','.db_escape(date2sql($_POST['payment_date'])).','.$_POST['next_reference'.$x].',"'.$row['ref'].'",'.$postamount.')';
			$sql='INSERT INTO '.TB_PREF.'check_voucher(cv_no,cv_date,cheque_no,apv_no,amount,memo_) VALUES('.db_escape($lastCV).','.db_escape(date2sql($_POST['date_cheque'.$x])).','.$_POST['next_reference'.$x].',"'.$apvs.'",'.$postamount.',"'.$_POST['memo_'].'")';
			db_query($sql,"Error:check voucher");	
			
			$cv_id=db_insert_id();
			
			//add_bank_trans(50,$lastCV,$_POST['bank_account'],$_POST['trans_no'],$_POST['date_cheque'.$x],3,-$postamount,$row['cust_supp'],$row['cust_supp_id'],$curr,"Error");
			$bank_trans_id=add_bank_trans(50,$cv_id,$_POST['bank_account'],$lastCV,$_POST['date_cheque'.$x],-$postamount,$row['cust_supp'],$row['cust_supp_id'],$curr,"Error");
			
			
			////////////////////////////Add voucher account
			//add_gl_trans(50,$cv_id,$_POST['date_cheque'.$x],$row['voucher_account'],'','','',$postamount,$curr,$row['cust_supp'],$row['cust_supp_id']);
			foreach($_SESSION['cart']->line_items as $key=>$value){
				add_gl_trans(50,$cv_id,$_POST['date_cheque'.$x],$value->account,$value->dimension_id,$value->dimension2_id,'',abs($value->amount),$curr,$row['cust_supp'],$row['cust_supp_id'],"",0);	//,$value->dimension3_id,$value->dimension4_id,$value->dimension5_id,$value->sl);
				//display_error("gl cv=>". $value->apv_no ."->". $value->amount);
				
				$sql='UPDATE '.TB_PREF.'apv_header SET alloc = alloc+'.abs($value->amount).' WHERE id = "'.$value->apv_no.'"';
				db_query($sql,"Error Update -> header");
				
			}
			add_gl_trans(50,$cv_id,$_POST['date_cheque'.$x],$_POST['bank_account'],'','','',-$postamount,$curr,$row['cust_supp'],$row['cust_supp_id']);
			
			issue_check_number(50, $lastCV, $_POST['next_reference'.$x],$x);
			
			$x++;
			
			
		}while($x<$_POST['num_cheque']);
		
		// if ($row['invoice_no'] != 0)
		// {
			// //2009
			// //$supp_trans_no = add_supp_trans(50, $row['cust_supp_id'], $_POST['payment_date'], '', $lastCV, $row['ref'],	$amount, 0, 0, '', 0);
			// $supp_trans_no = add_supp_trans(50, $row['cust_supp_id'], Today(), '', $lastCV, $row['ref'],	$value->amount/*$amount*/, 0, 0, '', 0);
			// allocate_cv_to_invoice(abs($value->amount/*$amount*/), $supp_trans_no, $row['invoice_no']);
		// }
		
		foreach($_SESSION['cart']->line_items as $key=>$value){
			
			$sql1="select * FROM ".TB_PREF."apv_header WHERE id='". $value->apv_no ."'";
			$result1=db_query($sql1,"Cannot get APV_HEADER");
			$row1=db_fetch($result1);
			
			if($row1['cust_supp'] == 3)
			{
				$supp_trans_no = add_supp_trans_cv(50, $row1['cust_supp_id'], Today(), '', $lastCV, get_reference_no($value->apv_no, ST_APVVOUCHER)/*$row['ref']*/,	$value->amount/*$amount*/, 0, 0, '', 0, abs($value->amount));
				allocate_cv_to_invoice(abs($value->amount/*$amount*/), $supp_trans_no, $row1['invoice_no']);
			}	
		}
		
		//$_POST['type'] = 50;
		//$_POST['trans_no'] = $lastCV;
		
		unset($_SESSION['cart']);
		
		$issue_id=$cv_id;
		add_reference(50, $cv_id, $lastCV);
	meta_forward($_SERVER['PHP_SELF'], "IssuedID=$issue_id");

}

//--------------------------------------------------------------------------------------------------

/*
if (is_issued_check($_GET['type_id'], $_GET['trans_no'])) {

	display_error(_("This payment has already been issued a cheque number"), true);
	end_page();
	exit;

}
*/

start_table($table_style);

$th = array(_("Type"), _("#"), _("Payee"),
		_("Date Created"), _("Amount"),"");
	
table_header($th);
$k = 0;
$globalAmount=0;
$cart = $_SESSION['cart'];
foreach($cart->line_items as $key=>$value){
	$paymentline = get_paymentline(45,$value->apv_no);

	//if($paymentline['ref']!='' && $_GET['type_id']==45)
	$paymentline['trans_no']=$paymentline['ref'];
		
	alt_table_row_color($k);

	if ($paymentline["tran_date"] != '')
		$date = sql2date($paymentline["tran_date"]);
	else $date = sql2date($paymentline["trans_date"]);

	label_cell($systypes_array[$paymentline["type"]]);

	if($paymentline["type"]==45){
		//label_cell($paymentline["ref"]);
		label_cell(get_apv_view_str($paymentline['id'], $paymentline['ref']));
	}else{	
		label_cell(get_trans_view_str($paymentline["type"],$paymentline["trans_no"]));
		label_cell(get_trans_view_str($paymentline["type"],$paymentline["trans_no"], $paymentline["reference"]));
	}
	
	
	if($paymentline['type']==45){
		label_cell(payment_person_name($paymentline['cust_supp'],$paymentline['cust_supp_id'], false));
		$date=sql2date($paymentline['tran_date']);
		//$sql='SELECT SUM(amount) FROM '.TB_PREF.'apv_details WHERE apv_no = '.$paymentline['trans_no'].' AND amount>=0';
		$sql="SELECT amount FROM ".TB_PREF."apv_details WHERE apv_no= '". $paymentline['id']."' AND account = ". $paymentline['voucher_account'];
		$result=db_query($sql);
		$row=db_fetch($result);
		$paymentline["amount"]=-$row[0];
	}
	$value->amount=$paymentline["amount"]-$paymentline["alloc"];
	$TotalAmount = $paymentline["amount"]-$paymentline["alloc"];
	$globalAmount += $TotalAmount;
	label_cell($date);
		if ($TotalAmount >= 0)
			if($paymentline['type']!=45)
			label_cell("");
			amount_cell(abs($TotalAmount));
		if ($TotalAmount < 0)
			label_cell("");
	if($paymentline['type']==45){
		label_cell(get_apv_view_str($paymentline['id'], 'GL'));
	}else{
		label_cell(get_gl_view_str($paymentline["type"], $paymentline["trans_no"]));
	}
	end_row();
}
end_table();

$TotalAmount=$globalAmount;

echo '<br/>';

start_form();
	start_table($table_style2);
			text_cells('No. of Cheque','num_cheque',$_POST['num_cheque'],5);
			date_cells('Starting from','start_');
			//submit_cells('Show',_("Show"),'','', true);
			submit_cells('Create','Create','','',true);
			hidden('skip',true);
			hidden('type',45) ;
			hidden('bank_account',$_POST['bank_account']);			
	end_table(1);
end_form();

//------------------------------------------------------------------------------------
if(isset($_POST['num_cheque'])){

	start_form();
		div_start('info');
		start_table($table_style2);
		foreach($_POST['check'] as $value){
			hidden('check[]',$value);	
		}
		hidden('total_cheque',abs($TotalAmount));
		// Get the Next Cheque Reference.
		//enter here --> APV
	
		
			//bank_accounts_list_row( $payment ? _("From:") : _("To:"), 'bank_account', 0, true);
			check_bank_accounts_list_row( $payment ? _("From:") : _("To:"), 'bank_account', null, true);
			//date_cells('Payment Date:','payment_date');
			$amount = abs($TotalAmount) / $_POST['num_cheque'];
			
			$excess=0;
			if(($pos=strpos($amount,'.'))!==false){
			//display_error($amount);				
				$excess=substr($amount,$pos,strlen($amount))*$_POST['num_cheque'];
				$amount = substr_replace($amount,'0',$pos+1);
			}
			
			hidden('type',$_GET['type_id']) ;
			hidden('trans_no',$_GET['trans_no']);
			hidden('num_cheque',$_POST['num_cheque']);			
			
			if($_POST['bank_account'] != 0){
				$check_ref = get_next_check_reference($paymentline["type"], $paymentline["trans_no"], $_POST['bank_account']);
				
				end_table(1);
				echo '<br/>';
				start_table($table_style2);
				
				$th = array(("Cheque #"), _("Amount"), _("Date"));
				table_header($th);
				
				$x=0;
				do{
					if($x+1==$_POST['num_cheque'])
						$amount+=$excess;
					
					start_row();
					text_cells('', 'next_reference'.$x, $check_ref+$x,20);
					//text_cells('', 'amount'.$x, $amount,20);
					amount_cells('', 'amount'.$x,price_format($amount),null,null,null);
					if($x>0)
						$date=date('m/d/Y',strtotime($_POST['start_'].' + '.$x.' months'));
					else
						$date=date('m/d/Y',strtotime($_POST['start_']));
					date_cells_cv('','date_cheque'.$x,null,$date);
					$x++;
					end_row();
				
				}while($x<$_POST['num_cheque']);
			}
				textarea_row(_("Memo"), 'memo_', null, 50, 3);
				end_table(1);
				
				if($_POST['bank_account']!=0)
				submit_center('POST_CHECK', _("Issue Cheque"), true, '', true);
		
	div_end();
	end_form();
}

end_page();

?>
