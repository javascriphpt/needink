<?php

$page_security = 5;
// ----------------------------------------------------------------
// $ Revision:	1.0 $
// Creator:	Tu Nguyen
// date_:	2008-08-04
// Title:	Print CPA Cheques (Canadian Pre-printed Standard)
// ----------------------------------------------------------------

$path_to_root="../..";
$page_security = 'SA_APVVOUCHER';

include($path_to_root . "/includes/session.inc");
include($path_to_root . "/modules/checkprint/includes/check_accounts_db.inc");
include_once($path_to_root . "/purchasing/includes/db/suppalloc_db.inc");
$path_to_root="../../";
define('K_PATH_FONTS', "../../reporting/fonts/");
include_once($path_to_root . "reporting/includes/pdf_report.inc");
include_once ($path_to_root . "includes/db/comments_db.inc");

require('Numbers/Words.php');

// Get Cheque Number to display

/*
$cheque->row = $cheque->pageHeight - $cheque->topMargin;

$cheque->row -= 47;
//------------------------ col - row - spec's ------------
$col0 				= 45;		// the first column
$col1 				= 248;
$col3 				= 480;
$col4 				= 380;
$col5 				= 455;
$col6 				= 145;
$col_right = $cheque->pageWidth - $cheque->rightMargin;
$width_col0 = $col_right - $col0;
$width_col1 = $col_right - $col1;
$width_col3 = $col_right - $col3;
$width_col4 = $col_right - $col4;
$width_col5 = $col_right - $col5;
$width_normal 		= 200;

$row_address		= $cheque->pageHeight - 177;
$row_first_stub 	= $row_address - 149; 	// first spec. for the payment
$row_second_stub 	= $row_address - 380; 	// second spec. for the payment

$gap 				= 15; 	// space between the spaced date and spec
//-------------------------------------------------------
$cheque->TextWrap($col4, $cheque->row, $width_normal, $cheque->title);
$cheque->TextWrap($col4, $cheque->row, $width_col4, $checkinput, "right");

$cheque->NewLine();

$cheque->TextWrap($col4, $cheque->row, 50, "DATE");

$cdate = sql2checkdate($receipt['tran_date']);
$ddate = "DDMMYYYY";

for ($i = 0, $col = $col5; $i < 8; $col += $gap, $i++)
	$cheque->TextWrap($col, $cheque->row, $gap - 1, $cdate[$i]);

$cheque->lineHeight = 12;
$cheque->NewLine();
for ($i = 0, $col = $col5; $i < 8; $col += $gap, $i++)
	$cheque->TextWrap($col, $cheque->row, $gap - 1, $ddate[$i]);

$cheque->lineHeight = 22;
$cheque->NewLine();

$sampleamount = number_format(-$receipt['BankAmount'], 2, '.', '');

$amounttotal = reformat_num($nw->toWords(floor($sampleamount)));
$amountfrac = intval(($sampleamount - floor($sampleamount)) * 1000);
$amountfrac = number_format2($amountfrac/10);

setlocale(LC_MONETARY, $cheque->l['a_meta_language']);
$themoney = (($show_currencies) ? 'US' : '').'Php**'.number_format($sampleamount, 2, '.', ',');

$amount = "**".ucwords($amounttotal) . ' pesos';
if ($amountfrac != 0)
{
	$amount .= ' and ' . reformat_num($nw->toWords(floor($amountfrac))).' centavos';
}
//." and ".(($amountfrac < 10) ? '0' : '');
$cheque->TextWrap($col0, $cheque->row, $width_col0, $amount);

$cheque->TextWrap($col0, $cheque->row, $width_col0, $themoney, "right");
$cheque->lineHeight = 13;
$cheque->NewLine();
if ($show_currencies)
	$cheque->TextWrap($col0, $cheque->row, $width_col0, "Amount in United States Dollars");

$cheque->row = $row_address;
$cheque->lineHeight = 12;
$cheque->TextWrapLines($col0, $width_col0, $receipt['supplier_name'] . "\n" .$receipt['SupplierAddress']);

$dotadd = 40;

$cheque->lineHeight = 16;

for ($i = 0; $i < 2; $i++)
{
	// First Stub
	$cheque->row = ($i == 0 ? $row_first_stub : $row_second_stub);

	$cheque->TextWrap($col0, $cheque->row, $width_col0, $receipt['supplier_name']);
	$cheque->TextWrap($col1, $cheque->row, $width_col1, sql2date($receipt['tran_date']));
	$cheque->TextWrap($col3, $cheque->row, 93, $checkinput);

	$cheque->NewLine();

	// Get allocations (shows supplier references on invoices and its amount) (Two columns).
	if ($i == 0)
		$supplierrefs = get_allocatable_to_supp_transactions($receipt['supplier_id'], $receipt['trans_no'],22);

	$totalallocated = 0;
	$totallines = 0;
	if ($i == 1)
		db_seek($supplierrefs, 0);

	while ($alloc_row = db_fetch($supplierrefs))
	{
		$theamout = number_format($alloc_row['amt'], 2, '.', ',');

		$cheque->TextWrap($col0, $cheque->row, 100, $alloc_row['supp_reference'] . " " . str_repeat('.',$dotadd));
		$tcol = $col6;
		$cheque->TextWrap($tcol, $cheque->row, 50, number_format($alloc_row['amt'], 2), "right");
		if ($show_currencies)
			$cheque->TextWrap($tcol += 55, $cheque->row, 50, $receipt['SupplierCurrCode']);
		if ($alloc_row['Total'] != $alloc_row['amt'])
			$cheque->TextWrap($tcol += 55, $cheque->row, $width_normal, " (Left to allocate ".
				number_format($alloc_row['Total'] - $alloc_row['amt'], 2, '.', '.').")");
		$totalallocated += $alloc_row['amt'];
		$totallines++;
		$cheque->lineHeight = 12;
		$cheque->NewLine();
	}
	$cheque->lineHeight = 16;
	if ($i == 0)
	{
		if ($totallines > 16)
		{
				$errorpdf = new FrontReport("ERROR", "cheque.pdf", user_pagesize(), 10);
				$errorpdf->Font();
				$errorpdf->TextWrap($col0, $errorpdf->row, $width_col0, "Error: Currently cannot allocate more than 16 invoices per payment.");
				$errorpdf->End();
				return;
		}

		// Get allocations that are left
		$allocationleft = $sampleamount - $totalallocated;

		//if (!$supplierrefs || db_num_rows($supplierrefs) == 0 || $allocationleft > 0)
		if (!$supplierrefs || db_num_rows($supplierrefs) == 0)
		{
			$errorpdf = new FrontReport("ERROR", "cheque.pdf", user_pagesize(), 10);
			$errorpdf->Font();
			$errorpdf->TextWrapLines($col0, $width_col0, "Error: Payment allocations missing / incomplete for this payment.\n" .
				number_format($allocationleft, 2, '.', ',') . " left to allocate.");
			$errorpdf->End();
			return;
		}
	}
}
$cheque->End();
*/

/*$company_currency = get_company_currency();

$show_currencies = false;
$show_both_amounts = false;

if (($receipt['bank_curr_code'] != $company_currency) || ($receipt['SupplierCurrCode'] != $company_currency))
	$show_currencies = true;

if ($receipt['bank_curr_code'] != $receipt['SupplierCurrCode'])
{
	$show_currencies = true;
}
*/
// now the print

function reformat_num($totalword) {
	$search = array('-one','-two','-three','-four','-five','-six','-seven','-eight','-nine');
	$replace = array(' One',' Two',' Three',' Four',' Five',' Six', ' Seven', ' Eight',' Nine');
	return str_replace($search,$replace,$totalword);
}

function sql2checkdate($date_)
{
	global $date_system;

	//for MySQL dates are in the format YYYY-mm-dd
	if ($date_ == null || strlen($date_) == 0)
		return "";

	if (strpos($date_, "/"))
	{ // In MySQL it could be either / or -
		list($year, $month, $day) = explode("/", $date_);
	}
	elseif (strpos ($date_, "-"))
	{
		list($year, $month, $day) = explode("-", $date_);
	}

	if (strlen($day) > 4)
	{ 
		$day = substr($day, 0, 2);
	}
	if ($date_system == 1)
		list($year, $month, $day) = gregorian_to_jalali($year, $month, $day);
	elseif ($date_system == 2)
		list($year, $month, $day) = gregorian_to_islamic($year, $month, $day);

	return $day.$month.$year;
}

function get_check_trans_details($checkinput, $check_bank)
{
	$sql = "SELECT bank_trans_id, bank_trans_type, check_ref FROM ".TB_PREF."check_trans
			WHERE cheque_bank_id = $check_bank
			AND check_ref = ". db_escape($checkinput);
	$res = db_query($sql,'failed to get check trans type');

	$row = db_fetch($res);
	
	return $row;
	
}

function get_check_trans_no($id)
{
	$sql = "SELECT trans_no FROM ".TB_PREF."bank_trans
			WHERE id = $id";
	$res = db_query($sql,'failed to get bank trans trans_no');
	$row = db_fetch($res);
	
	return $row[0];
	
}

function get_apv_ref($trans_no, $type)
{
	$sql = "SELECT reference FROM ".TB_PREF."refs WHERE type=".db_escape($type)." AND id=".db_escape($trans_no);

	$result = db_query($sql, "could not get reference no");

	$row = db_fetch_row($result);

	return $row[0];
}

function get_apv_memo($apv_no)
{
	$sql = "SELECT explanation FROM ".TB_PREF."apv_header WHERE id = ".db_escape($apv_no);

	$result = db_query($sql, "could not get memo");

	$row = db_fetch_row($result);

	return $row[0];
}

function get_cv_footer($cat_id)
{
	$sql = "SELECT * FROM ".TB_PREF."cv_forms WHERE category_id = ".db_escape($cat_id)." ORDER BY type_id";

	$result = db_query($sql, "could not get signatories");

	$signatories="";
	
	while($row = db_fetch($result))
	{
		$signatories .= $row['name']." ";
	}
	
	return $signatories;
}

/*
==============================================
=== CHECK
==============================================
*/

//$input = $_POST['PARAM_0'];
$check_bank = $_POST['PARAM_1'];
$nw = new Numbers_Words();

$input = explode(',',$_POST['PARAM_0']);
//$cheque->TextWrap(60,60, 70, $input);
$cheque = new FrontReport("CHEQUE NO.", "cheque.pdf", 'letter', 10);
$trigger=false;

foreach($input as $checkinput){
	$trans_type = get_check_trans_details($checkinput, $check_bank);
	if($trigger==false){
		$trigger=true;
	}else{
		$cheque->addPage();
	}
		
	// Grab Cheque Information
	$receipt = get_trans_from_check($checkinput, $check_bank, $trans_type[1],$trans_type[0]); 
	$payee = '';
	$cheque->Font();
	$cheque->lineHeight = 16;

	if ($trans_type[1]==22)
	{
		$to = $receipt['supplier_name'];
		$cheque->TextWrap(500, 746, 70, sql2date($receipt['tran_date']),'left');
		$cheque->TextWrap(90, 723, 300, $to,'left');
		
		//------------------------------------------------------------------------------------------------------------------------------------
		
		if($receipt['BankAmount'] < 0) $receipt['BankAmount'] *= -1;
		$sampleamount = number_format($receipt['BankAmount'], 2, '.', '');

		$payee = $to;
		
		$amounttotal = reformat_num($nw->toWords(floor($sampleamount)));
		$amountfrac = intval(($sampleamount - floor($sampleamount)) * 1000);
		$amountfrac = number_format2($amountfrac/10);

		setlocale(LC_MONETARY, $cheque->l['a_meta_language']);
		$themoney = number_format2($sampleamount, 2);

		$amount = "**".ucwords($amounttotal) . ' pesos';
		if ($amountfrac != 0)
		{
			$amount .= ' and ' . ucwords(reformat_num($nw->toWords(floor($amountfrac))).' centavos');
		}
		$amount .= ' only**';
		//------------------------------------------------------------------------------------------------------------------------------------
		if(strlen($amount)>90)
		$cheque->fontSize=8;
		$cheque->TextWrap(500, 723, 150, $themoney);
		$cheque->TextWrap(90, 700, 460, $amount);

	}
	else
	{
		// $to = payment_person_types::person_name($receipt["person_type_id"],$receipt["person_id"]);
		$to = payment_person_name($receipt["person_type_id"],$receipt["person_id"], false);
		$cheque->TextWrap(500, 746, 70, sql2date($receipt['trans_date']),'left');
		$cheque->TextWrap(90, 723, 300, $to,'left');
		
		//------------------------------------------------------------------------------------------------------------------------------------
		$sampleamount = number_format(-$receipt['amount'], 2, '.', '');

		$amounttotal = reformat_num($nw->toWords(floor($sampleamount)));
		$amountfrac = intval(($sampleamount - floor($sampleamount)) * 1000);
		$amountfrac = number_format2($amountfrac/10);

		setlocale(LC_MONETARY, $cheque->l['a_meta_language']);
		$themoney = number_format2($sampleamount, 2);

		$amount = "**".ucwords($amounttotal) . ' pesos';
		if ($amountfrac != 0)
		{
			$amount .= ' and ' . ucwords(reformat_num($nw->toWords(floor($amountfrac))).' centavos');
		}
		$amount .= ' only';
		
		//------------------------------------------------------------------------------------------------------------------------------------
//die($amount);
		if(strlen($amount)>90)
		$cheque->fontSize=8;
		$cheque->TextWrap(500, 723, 150, $themoney,'left');
		//$cheque->TextWrap(90, 700, 500, $amount);
		$cheque->TextWrap(90, 700, 460, $amount,'left');
	}	
}

$checkAmount=$amount;
$cheque->addPage();
/*
==============================================
=== CHECK VOUCHER
==============================================
*/
//$checkinput = $_POST['PARAM_0'];
//$check_bank = $_POST['PARAM_1'];
//$nw = new Numbers_Words();

//$trans_type = get_check_trans_details($checkinput, $check_bank);

//Grab Cheque Information
//$receipt = get_trans_from_check($checkinput, $check_bank, $trans_type[1],$trans_type[0]); 

//cheque = new FrontReport("CHEQUE NO.", "cheque.pdf", 'letter', 10);

$YPos = 723;

	$company=get_company_prefs();

	if($trans_type[1]==22)
	{
		$filename = '../../printouts/checkvoucherprimepeak.jpg';
		$cheque->AddImage($filename, 0, 0, $cheque->pageWidth, $cheque->pageHeight);
		$cheque->Font();
		$cheque->lineHeight = 16;	
		$payment_for='';
		$count = 1;
		$supp_ref = get_supp_reference($receipt["trans_no"]);
		while($row = db_fetch($supp_ref))	
		{
			if ($count==1)
			$payment_for .= $row[0];
			else
			$payment_for .= ','.$row[0] ;
			$count++;
		}
		
		$payment_for = 'Supplier Invoice # '. $payment_for;
		
		$leftover = $cheque->TextWrap(90, $YPos-$n2d, 400, $payment_for);
		$YPos2 = $YPos-$n2d;
		while ($leftover)
		{
			$YPos2 -= 14;
			$leftover = $cheque->TextWrap(90, $YPos2-$n2d, 400, $leftover);	
		}
		$cheque->TextWrap(490, $YPos-$n2d, 70, $themoney,'right');
		$YPos = $YPos2;
		
	}
	else if($trans_type[1] == 50)
	{
		//=========================

		//$filename = '../../printouts/checkvoucherprimepeakorig.jpg';
		//$filename = '../../printouts/checkvoucherprimepeakpayee.jpg';

		//if 2 copies set to 0
		//$counter_x = 0;
		//$diff=384;
		$diff=384;
		$counter_x = 1;
		$trigger=false;
		$YPos+=3;
		while($counter_x++ != 2){
			if($trigger==false){
				//$filename = '../../printouts/checkvoucherprimepeakorig.jpg';
				$filename = '../../printouts/checkvoucherprimepeak.jpg';
				$trigger=true;
			}else{
				$cheque->addPage();
				$filename = '../../printouts/checkvoucherprimepeakpayee.jpg';
			}
			$cheque->AddImage($filename, 0, 0, $cheque->pageWidth, $cheque->pageHeight);
			$cheque->Font();
			$cheque->lineHeight = 16;
			$details_sql = 'SELECT a.cv_no, a.cv_date, a.cheque_no, a.apv_no
						FROM `'.TB_PREF.'check_voucher` a
						JOIN `'.TB_PREF.'check_trans` b
						ON   b.check_ref = a.cheque_no
						WHERE bank_trans_type = ' . $trans_type[1] . '
						AND b.check_ref = \''.$checkinput.'\';';
		
			$res = db_query($details_sql);
			$me_row = db_fetch($res);
			
			
			$ref_num = $me_row['cv_no'];
			$date = $me_row['cv_date'];
			$check_num = $me_row['cheque_no'];
			$apv_num = $me_row['apv_no'];
			
			
			$money_sql = 'SELECT account, amount 
				FROM `'.TB_PREF.'gl_trans`
				WHERE type = ' . $trans_type[1] . '
				AND type_no = \''.$ref_num.'\' ;';
				
			$money_sql='SELECT a.account, a.amount FROM `'.TB_PREF.'gl_trans` a JOIN '.TB_PREF.'bank_trans b ON a.type_no=b.trans_no WHERE a.type ='.$trans_type[1].' AND b.ref = '.$ref_num ;
			
			//$cheque->TextWrapLines2(100,100, 500, $details_sql);
				
			$res2 = db_query($money_sql);
			
			$supp_ref = get_supp_reference($receipt["trans_no"]);
			$cheque->TextWrap(40,752,$cheque->pageWidth,$company['coy_name'],'left');
			$cheque->TextWrap(40,752-$diff,$cheque->pageWidth,$company['coy_name'],'left');
			
			//$cheque->TextWrap(90, $YPos, 400, payment_person_types::person_name($receipt["person_type_id"],$receipt["person_id"]),'left');
			//$cheque->TextWrap(90, $YPos-$diff, 400, payment_person_types::person_name($receipt["person_type_id"],$receipt["person_id"]),'left');
			$cheque->TextWrap(90, $YPos, 400, payment_person_name($receipt["person_type_id"],$receipt["person_id"], false),'left');
			$cheque->TextWrap(90, $YPos-$diff, 400, payment_person_name($receipt["person_type_id"],$receipt["person_id"], false),'left');
			
			$cheque->TextWrap(305, $YPos+18, 400, $ref_num,'left');
			$cheque->TextWrap(305, $YPos+18-$diff, 400, $ref_num,'left');
			
			$cheque->TextWrap(480, $YPos+18, 400, date('F d, Y', strtotime($date)),'left');
			$cheque->TextWrap(480, $YPos+18-$diff, 400, date('F d, Y', strtotime($date)),'left');
			
			$cheque->TextWrap(453, $YPos, 400, $check_num,'left');
			$cheque->TextWrap(453, $YPos-$diff, 400, $check_num,'left');
			
			$cheque->TextWrap(40, $YPos-16, 90, 'Payment for APV : ','left');
			$cheque->TextWrap(40, $YPos-16-$diff, 90, 'Payment for APV : ','left');
			
			if(get_apv_memo($apv_num) == '')
				$memo = '';
			else
				$memo = " - ".get_apv_memo($apv_num);
				
				
			$apv_no_ = explode(",", $apv_num);
			
			$apv_no__='';
		    foreach($apv_no_ as $val)
		    {
				//display_error("discount_ => ".$stock_item->qty_dispatched * $stock_item->price * ($val / 100));
				$line_total = $line_total * (1 - ($val/100));
				
				if($val == "")
					$apv_no__='';
				else
					$apv_no__ .= get_apv_ref($val, 45).", ";
		    }
				
			$cheque->TextWrapLines2(125, $YPos-16, 450,str_replace(',',', ',$apv_no__/*$apv_num*/).$memo,'left');
			$cheque->TextWrapLines2(125, $YPos-16-$diff, 450,str_replace(',',', ',$apv_no__).$memo,'left');
			
			/*
			$cheque->TextWrap(453, $YPos-14, 400, $apv_num);
			$cheque->TextWrap(453, $YPos-14-$diff, 400, $apv_num);
			*/
			$YPos2 = $YPos-20;
			$YPos2 -= 15;
			$debit_credit = array(380,473);
			$ypos3 = 0;
			
			$count=0;
			
			while($row2 = db_fetch($res2)){
			
				if($count==24){
					$YPos = 726;
					$cheque->addPage();
					$cheque->AddImage($filename, 0, 0, $cheque->pageWidth, $cheque->pageHeight);
					$cheque->Font();
					$cheque->lineHeight = 16;
					
					$cheque->TextWrap(90, $YPos-6, 400, payment_person_types::person_name($receipt["person_type_id"],$receipt["person_id"]));
					$cheque->TextWrap(90, $YPos-6-$diff, 400, payment_person_types::person_name($receipt["person_type_id"],$receipt["person_id"]));
					
					$cheque->TextWrap(305, $YPos+18, 400, $ref_num);
					$cheque->TextWrap(305, $YPos+18-$diff, 400, $ref_num);
					
					$cheque->TextWrap(480, $YPos+18, 400, date('F d, Y', strtotime($date)));
					$cheque->TextWrap(480, $YPos+18-$diff, 400, date('F d, Y', strtotime($date)));
					
					$cheque->TextWrap(453, $YPos, 400, $check_num);
					$cheque->TextWrap(453, $YPos-$diff, 400, $check_num);
					
					$cheque->TextWrap(453, $YPos-14, 400, $apv_num);
					$cheque->TextWrap(453, $YPos-14-$diff, 400, $apv_num);
					
					$YPos2 = $YPos;
					$YPos2 -= 15;
					$debit_credit = array(380,473);
					$ypos3 = 0;					
					$count=0;
				}
				
				$account = $row2['account'];
				$amount = $row2['amount'];
				
				if($ypos3 > 0) $YPos2 = $ypos3;
				else $YPos2 -=14;
				
				$ypos3 = 0;
				
				$acct_name = get_gl_account_name($account);
				$cheque->TextWrap(37, $YPos2-19, 300, $account,'left');
				$cheque->TextWrap(37, $YPos2-19-$diff, 300, $account,'left');
				
				$ypos3 = $cheque->TextWrapLines2(145, $YPos2-19, 220, $acct_name);
				$cheque->TextWrapLines2(145, $YPos2-19-$diff, 220, $acct_name);
				
				if($amount > 0){
					$cheque->TextWrap($debit_credit[0], $YPos2-19, 100, number_format2($amount,2), 'right');
					$cheque->TextWrap($debit_credit[0], $YPos2-19-$diff, 100, number_format2($amount,2), 'right');
				}else{
					$cheque->TextWrap($debit_credit[1], $YPos2-19, 100, number_format2($amount*-1,2), 'right');
					$cheque->TextWrap($debit_credit[1], $YPos2-19-$diff, 100, number_format2($amount*-1,2), 'right');
				}
				$count++;
			}
		}
		
		
		$ypos3 = $cheque->TextWrapLines2(145, $YPos2-40, 220, '**Payment of account');
		$cheque->TextWrapLines2(145, $YPos2-40-$diff, 220, '**Payment of account');
		
		$cheque->font('bold');
		$ypos3 = $cheque->TextWrapLines2(145, $YPos2-55, 220, 'In Words');
		$cheque->TextWrapLines2(145, $YPos2-55-$diff, 220, 'In Words');
		$cheque->font();
		$cheque->fontSize=8;
		$ypos3 = $cheque->TextWrapLines2(145, $YPos2-65, 220, $checkAmount);
		$cheque->TextWrapLines2(145, $YPos2-65-$diff, 220, $checkAmount);
		
		/*payment_person_types::person_name($receipt["person_type_id"],$receipt["person_id"])
		$YPos2 = $YPos;
		while ($leftover)
		{
			$YPos2 -= 14;
			$leftover = $cheque->TextWrap(90, $YPos2-$n2d, 400, $leftover);	
		}
		$cheque->TextWrap(490, $YPos-$n2d, 70, $themoney,'right');
		$YPos = $YPos2-$n2d;
		*/
		
		
		//------------ footer ------------//
				
		// // prepared
		// $ypos3 = $cheque->TextWrapLines2(57, $YPos2-225, 280, get_cv_footer(1));
		// $cheque->TextWrapLines2(57, $YPos2-225-$diff, 220, get_cv_footer(1));
		
		// // checked
		// $ypos3 = $cheque->TextWrapLines2(187, $YPos2-225, 220, get_cv_footer(2));
		// $cheque->TextWrapLines2(187, $YPos2-225-$diff, 220, get_cv_footer(2));
		
		// // approved
		// $ypos3 = $cheque->TextWrapLines2(327, $YPos2-225, 220, get_cv_footer(3));
		// $cheque->TextWrapLines2(327, $YPos2-225-$diff, 220, get_cv_footer(3));
		
		// // received
		// $ypos3 = $cheque->TextWrapLines2(457, $YPos2-225, 220, get_cv_footer(4));
		// $cheque->TextWrapLines2(457, $YPos2-225-$diff, 220, get_cv_footer(4));
		
		// // prepared
		// $ypos3 = $cheque->TextWrapLines2(57, 433, 280, get_cv_footer(1));
		// $cheque->TextWrapLines2(57, 49, 220, get_cv_footer(1));
		
		// // checked
		// $ypos3 = $cheque->TextWrapLines2(187, 433, 220, get_cv_footer(2));
		// $cheque->TextWrapLines2(187, 49, 220, get_cv_footer(2));
		
		// // approved
		// $ypos3 = $cheque->TextWrapLines2(327, 433, 220, get_cv_footer(3));
		// $cheque->TextWrapLines2(327, 49, 220, get_cv_footer(3));
		
		// // received
		// $ypos3 = $cheque->TextWrapLines2(457, 433, 220, get_cv_footer(4));
		// $cheque->TextWrapLines2(457, 49, 220, get_cv_footer(4));
		
		//------------ footer ------------//
		
		
		//=========================
	} else{
	
		if($trans_type[1]!=1){
			$comments = get_comments($trans_type[1],get_check_trans_no($trans_type[0]));
			
			$comment = db_fetch($comments);

			$leftover = $cheque->TextWrap(90, $YPos-$n2d, 400, $comment['memo_']);
			$YPos2 = $YPos;
			while ($leftover)
			{
				$YPos2 -= 14;
				$leftover = $cheque->TextWrap(90, $YPos2-$n2d, 400, $leftover);	
			}
			$cheque->TextWrap(490, $YPos-$n2d, 70, $themoney,'right');
			$YPos = $YPos2-$n2d;
		}
	}
	
/*	$cheque->TextWrap(490, ($top-200), 70, $themoney,'right');
	
	$cheque->TextWrap(295, (654-232)-$n2d, 70, $trans_type['check_ref'], 'center');
	$YPos=654;
	$n2d=375;	
*/

$cheque->End();
?>