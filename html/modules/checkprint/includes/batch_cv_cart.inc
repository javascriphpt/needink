<?php

class items_cart
{
	var $line_items;
	var $counter;

	function items_cart()
	{
		$this->clear_items();
		$this->line_items=array();
	}

	// --------------- line item functions

	function add_to_cart($line_no,$apv_no)
	{
		$this->line_items[$line_no]=new line_item($apv_no);
		$this->counter++;
	}

	function find_cart_item($apv_no)
	{
		foreach($this->line_items as $line_no=>$line) {
			if ($line->apv_no == $apv_no)
				return $this->line_items[$line_no];
		}
		return null;
	}

	function remove_from_cart($line_no)
	{
		unset($this->line_items[$line_no]);
	}

	function count_items()
	{
		return count($this->line_items);
	}
	
	function get_total(){
		$total=0;
		foreach($this->line_items as $line_no=>$line){
			$total+=$line->amount;
		}
		return $total;
	}
	
	// ------------ common functions

	function clear_items()
	{
    	unset($this->line_items);
		$this->line_items = array();
		$this->counter=0;
	}
}

//--------------------------------------------------------------------------------------------

class line_item
{
	var $apv_no;
	var $amount;
	var $account;
	var $dimension_id;
	var $dimension2_id;
	var $dimension3_id;
	var $dimension4_id;
	var $dimension5_id;
	var $sl;

	function line_item($apv)
	{
		$this->apv_no=$apv;
	}
}

//---------------------------------------------------------------------------------------
?>
