<?php
$path_to_root="../..";

$page_security = 'SA_APVVOUCHER';

include($path_to_root . "/includes/session.inc");

include($path_to_root . "/purchasing/includes/purchasing_ui.inc");
include($path_to_root . "/modules/checkprint/includes/check_accounts_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");


$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_("Cheque Number Issue"), false, false, "", $js);

if (isset($_POST['type']))
	$_GET['type_id'] = $_POST['type'];
if (isset($_POST['trans_no']))
	$_GET['trans_no'] = $_POST['trans_no'];	
if (!isset($_POST['num_cheque']))
	$_POST['num_cheque']=1;
	
	
//----------------------------------------------------------------------------------------

check_db_has_suppliers(_("There are no suppliers defined in the system."));

check_db_has_bank_accounts(_("There are no bank accounts defined in the system."));

check_db_has_bank_trans_types(_("There are no bank payment types defined in the system."));

//---------------------------------------------------------------------------------------------------------------

if (isset($_GET['IssuedID']))
{
	$check_no = $_GET['IssuedID'];

    echo "<center>";
    display_notification_centered(_("Cheque Number has been assigned."));

	//display_note(print_document_link($invoice_no, _("Print This Cheque"), true, 10));

    hyperlink_params("$path_to_root/modules/checkprint/check_list.php", _("Issue Another Cheque"), "");


	display_footer_exit();
}

// ----------------------------------------------------------------------------------------------------------------------------------------------------------
function allocate_cv_to_invoice($amount,  $lastCV, $trans_no_to)
{
	$sql = "INSERT INTO ".TB_PREF."supp_allocations (amt,date_alloc,trans_no_from,trans_type_from,trans_no_to,trans_type_to)
				VALUES($amount,curdate(),".db_escape($lastCV).",50,$trans_no_to,20)";
	
	db_query($sql,"failed to insert allocations");
	
	$sql = "UPDATE ".TB_PREF."supp_trans
				SET alloc = alloc+$amount
				WHERE trans_no = $trans_no_to
				AND type = 20";
	db_query($sql,"failed to update allocation");
	
	
			
}


function check_sum(){
	$x=0;
	$sub=0;
	do{
		$sub+=round(input_num('amount'.$x),2);
		$x++;
	}while($x<$_POST['num_cheque']);
	
	$total=input_num('total_cheque');
	
	$returnMe=true;
	
	// if($sub<$total){
		// display_error('Cheques total amount must match Debit : Short by '.number_format2($total-$sub,2));
		// $returnMe=false;
	// }
	if($sub>$total){
		display_error('Cheques total amount must match Debit : Off by '.number_format2($sub-$total,2));
		$returnMe=false;
	}
		
	return $returnMe;
}


function check_reference_ok($type, $trans_no, $reference,$x) {

	// Check Posts of Cheque No is not already taken.
	if (!references::is_valid($reference))
    {
		display_error(_("You must enter a cheque number."));
		set_focus('next_reference'.$x);
		return false;
	}

	if (!is_new_cheque($type, $trans_no, $reference))
	{
		display_error(_("The entered cheque number is already in use."));
		set_focus('next_reference'.$x);
		return false;
	}

	return true;

}

function save_last_cheque($bank_ref, $check_ref)
{
	$next = references::increment($check_ref);
	save_next_check_reference($bank_ref, $next);
}

if (isset($_POST['skip'])){
	if($_POST['num_cheque'] < 1){
		global $Ajax;
		display_error('Invalid Number of Checks : Reset to 1');
		$_POST['num_cheque']=1;
		$Ajax->activate('num_cheque');
	}else{
		$Ajax->activate('info');	
		//$Ajax->deactivate('info');	
	}
}

//redo checking
//
if (isset($_POST['POST_CHECK'])) {

	$x=0;
	global $Refs;
	
	do{
		if (!check_reference_ok($_POST['type'], $_POST['trans_no'], $_POST['next_reference'.$x],$x))
			return;
		$x++;
	}while($x<$_POST['num_cheque']);

	if(!check_sum())
		return;
	 //Also has DB constraints to not allow duplicates Cheque Refs
	
	if($_POST['type']==45){
	
		$sql="select * FROM ".TB_PREF."apv_header WHERE id='".$_POST['trans_no']."'";
		$result=db_query($sql,"Cannot get APV_HEADER");
		$row=db_fetch($result);
		
		$sql="select SUM(amount) FROM ".TB_PREF."apv_details WHERE apv_no='".$_POST['trans_no']."' AND account = '".$row['voucher_account']."'";
		$result=db_query($sql,"Cannot get APV_DETAILS");
		$amount=db_fetch($result);
		$amount=$amount[0];
		
		$sql = "SELECT a.*
				FROM ".TB_PREF."gl_trans a
				JOIN ".TB_PREF."apv_header b ON b.id = a.type_no 
				AND b.id = ".db_escape($_POST['trans_no'])."
				WHERE a.type= 45 AND a.account = '".$row['voucher_account']."'";
		//die(display_error($sql));
		//die(display_error($_POST['bank_account']));
		$result=db_query($sql,"Cannot get GL TRANS");
		$slgl=db_fetch($result);
	
		switch($row['cust_supp']){
			case 0://misc;
				$curr='PHP';
			break;
			case 2:
				$curr=get_customer_currency($row['cust_supp_id']);
			break;
			case 3:
				$curr=get_supplier_currency($row['cust_supp_id']);
			break;
		}
		// $lastCV=references::get_next(50);
		// references::save_last($lastCV, 50);
		
		$lastCV = $Refs->get_next(50);		
		$Refs->save_last($lastCV, 50);
		
		$_POST['type'] = 50;
		$x=0;
		do{
			$postamount=0;
			$postamount=round(input_num('amount'.$x),2);
			
			//2009
			$sql='INSERT INTO '.TB_PREF.'check_voucher(cv_no,cv_date,cheque_no,apv_no,amount,memo_) VALUES('.db_escape($lastCV).','.db_escape(date2sql($_POST['date_cheque'.$x])).','.db_escape($_POST['next_reference'.$x]).',"'.$row['id'].'",'.$postamount.',"'.$_POST['memo_'].'")';

			//$sql='INSERT INTO '.TB_PREF.'check_voucher(cv_no,cv_date,cheque_no,apv_no,amount) VALUES('.$lastCV.','.db_escape(date2sql($_POST['payment_date'])).','.$_POST['next_reference'.$x].',"'.$row['ref'].'",'.$postamount.')';
			db_query($sql,"Error:check voucher");	
			
			$cv_id=db_insert_id();
			
			//add_bank_trans(50,$lastCV,$_POST['bank_account'],$_POST['trans_no'],$_POST['date_cheque'.$x],3,-$postamount,$row['cust_supp'],$row['cust_supp_id'],$curr,"Error");
			$bank_trans_id=add_bank_trans(50,$cv_id,$_POST['bank_account'],$lastCV,$_POST['date_cheque'.$x],-$postamount,$row['cust_supp'],$row['cust_supp_id'],$curr,"Error");
			
			add_gl_trans(50,$cv_id,$_POST['date_cheque'.$x],$row['voucher_account'],$slgl['dimension_id'],$slgl['dimension2_id'],'',$postamount,$curr,$row['cust_supp'],$row['cust_supp_id'],"",0);
			add_gl_trans(50,$cv_id,$_POST['date_cheque'.$x],$_POST['bank_account'],'','','',-$postamount,$curr,$row['cust_supp'],$row['cust_supp_id']);
			
			issue_check_number($_POST['type'], $lastCV, $_POST['next_reference'.$x],$x);
			
			$x++;
			
			$sql='UPDATE '.TB_PREF.'apv_header SET alloc = alloc+'.$postamount.' WHERE id = "'.$_POST['trans_no'].'"';
			db_query($sql,"Error Update -> header");
		}while($x<$_POST['num_cheque']);
		
		//add_bank_trans(50,$lastCV,$_POST['bank_account'],$_POST['trans_no'],Today(),3,$amount,$row['cust_supp'],$row['cust_supp_id'],$curr,"Error");		
		
		//add_gl_trans(50,$lastCV,Today(),$row['voucher_account'],'','','',-$amount,$curr,$row['cust_supp'],$row['cust_supp_id']);
		//add_gl_trans(50,$lastCV,Today(),$_POST['bank_account'],'','','',$amount,$curr,$row['cust_supp'],$row['cust_supp_id']);
		//insetrt to CV
		
		//$sql='INSERT INTO '.TB_PREF.'check_voucher(cv_no,cv_date,cheque_no,apv_no,amount) VALUES('.$lastCV.',curdate(),'.$_POST['next_reference'].','.$row['ref'].','.$amount.')';
		//db_query($sql,"Error:check voucher");	
		
		add_reference(50, $cv_id, $lastCV);
		
		$sql='UPDATE '.TB_PREF.'apv_header SET cv = '.db_escape($lastCV).' WHERE id = "'.$_POST['trans_no'].'"';
		db_query($sql,"Error Update -> header");
		
		if ($row['invoice_no'] != 0)
		{
			//2009
			//$supp_trans_no = add_supp_trans(50, $row['cust_supp_id'], $_POST['payment_date'], '', $lastCV, $row['ref'],	$amount, 0, 0, '', 0);
			$supp_trans_no = add_supp_trans_cv(50, $row['cust_supp_id'], Today(), '', $lastCV, $row['ref'],	-$postamount, 0, 0, '', 0, $postamount);
			allocate_cv_to_invoice(abs($postamount), $supp_trans_no, $row['invoice_no']);
		}
		
		$_POST['type'] = 50;
		$_POST['trans_no'] = $lastCV;		
		
	}
	else{
		issue_check_number($_POST['type'], $_POST['trans_no'], $_POST['next_reference0']);
	}
	//meta_forward($_SERVER['PHP_SELF'], "IssuedID=$issue_id");
	meta_forward($_SERVER['PHP_SELF'], "IssuedID=$lastCV");

}

//--------------------------------------------------------------------------------------------------

if (isset($_GET['type_id']) && isset($_GET['trans_no']))
{
	// Check if already issued

	//----------------------------
	if (is_issued_check($_GET['type_id'], $_GET['trans_no'])) {

		display_error(_("This payment has already been issued a cheque number"), true);
   	 	end_page();
    	exit;

	}
	
	$paymentline = get_paymentline($_GET['type_id'],$_GET['trans_no']);
		
	if($paymentline['ref']!='' && $_GET['type_id']==45)
		$paymentline['trans_no']=$paymentline['ref'];
		
	start_table($table_style);
	
	if ($paymentline["type"] == 22){
		$th = array(("Type"), _("#"), _("Reference"), _("Supplier"),
		_("Supplier's Reference"), _("Date"), //_("Currency"),
		_("Debit"), _("Credit"), "");
	}else if($paymentline["type"]==45){
	$th = array(_("Type"), _("#"), _("Payee"),
			_("Date Created"), _("Amount"),"");
	}
	else
		$th = array(("Type"), _("#"), _("Reference"), _("Payee"),
		_("Date"), //_("Currency"),
		_("Debit"), _("Credit"), "");

	table_header($th);

	$k = 0;
	alt_table_row_color($k);
	
	if ($paymentline["tran_date"] != '')
		$date = sql2date($paymentline["tran_date"]);
	else $date = sql2date($paymentline["trans_date"]);
	
	label_cell($systypes_array[$paymentline["type"]]);
	
	if($paymentline["type"]==45){
		//label_cell($paymentline["ref"]);
		label_cell(get_apv_view_str($paymentline['id'], $paymentline['ref']));
	}else{	
		label_cell(get_trans_view_str($paymentline["type"],$paymentline["trans_no"]));
		label_cell(get_trans_view_str($paymentline["type"],$paymentline["trans_no"], $paymentline["reference"]));
	}
	
	if ($paymentline["type"] == 22)
	{		
		label_cell($paymentline["supp_name"]);
		
		$supp_refs='';
		$count = 1;
		$supp_ref = get_supp_reference($paymentline["trans_no"]);
		while($row = db_fetch($supp_ref))	
		{
			if ($count==1)
			$supp_refs .= $row[0];
			else
			$supp_refs .= ','.$row[0] ;
			$count++;
		}
		label_cell($supp_refs);
		
		$TotalAmount = $paymentline["TotalAmount"];
		$alloc_ = 0;
		
	}
	else
	{
		if($paymentline['type']==45){
			label_cell(payment_person_name($paymentline['cust_supp'],$paymentline['cust_supp_id'], false));
			$date=sql2date($paymentline['tran_date']);
			//$sql='SELECT SUM(amount) FROM '.TB_PREF.'apv_details WHERE apv_no = '.$paymentline['trans_no'].' AND amount>=0';
			$sql="SELECT amount FROM ".TB_PREF."apv_details WHERE apv_no= '". $paymentline['id']."' AND account = ". $paymentline['voucher_account'];
			$result=db_query($sql);
			$row=db_fetch($result);
			$paymentline["amount"]=-$row[0];
		}else{
			label_cell(payment_person_types::person_name($paymentline["person_type_id"],$paymentline["person_id"]));
		}
		
		$TotalAmount = $paymentline["amount"]-$paymentline["alloc"];
		$alloc_ = $paymentline["alloc"];
	}
	label_cell($date);
		if ($TotalAmount >= 0)
			if($paymentline['type']!=45)
			label_cell("");
			amount_cell(abs($TotalAmount));
		if ($TotalAmount < 0)
			label_cell("");
	if($paymentline['type']==45){
		label_cell(get_apv_view_str($paymentline['ref'], 'GL'));
	}else{
		label_cell(get_gl_view_str($paymentline["type"], $paymentline["trans_no"]));
	}
	end_row();

	end_table();

	echo '<br/>';
	
	if($paymentline['type']==45){
	start_form();
		start_table($table_style);
				text_cells('No. of Cheque','num_cheque',$_POST['num_cheque'],5);
				date_cells('Starting from','start_');
				//submit_cells('Show',_("Show"),'','', true);
				submit_cells('Create','Create','','',true);
				hidden('skip',true);
				hidden('type',$_GET['type_id']) ;
				hidden('trans_no',$_GET['trans_no']);			
				hidden('bank_account',$_POST['bank_account']);			
		end_table(1);
	end_form();
	}
	
	if(isset($_POST['num_cheque'])){
	
		start_form();
			div_start('info');
			start_table($table_style2);
			
			hidden('total_cheque',abs($TotalAmount));
			// Get the Next Cheque Reference.
			//enter here --> APV
			
			if($paymentline['type']!=45){
				//$check_ref = get_next_check_reference($paymentline["type"], $paymentline["trans_no"], $paymentline['bank_act']);
				
				hidden('trans_no', $paymentline["trans_no"]);
				hidden('type', $paymentline["type"]);
				hidden('num_cheque',$_POST['num_cheque']);
				
					text_cells('Cheque # for this payment', 'next_reference0', $check_ref,20);
					hidden('amount0',abs($TotalAmount));

				end_table(1);
				
				submit_center('POST_CHECK', _("Issue Cheque"), true, '', true);
			}else{
			
				check_bank_accounts_list_row( $payment ? _("From:") : _("To:"), 'bank_account', null, true);
				//date_cells('Payment Date:','payment_date');
				$amount = abs($TotalAmount) / $_POST['num_cheque'];
				
				$excess=0;
				if(($pos=strpos($amount,'.'))!==false){
				//display_error($amount);				
					$excess=substr($amount,$pos,strlen($amount))*$_POST['num_cheque'];
					$amount = substr_replace($amount,'0',$pos+1);
				}
				
				hidden('type',$_GET['type_id']);
				hidden('trans_no',$_GET['trans_no']);
				hidden('num_cheque',$_POST['num_cheque']);			
				
				if($_POST['bank_account'] != 0){
					$check_ref = get_next_check_reference($paymentline["type"], $paymentline["trans_no"], $_POST['bank_account']);
					
					end_table(1);
					echo '<br/>';
					start_table($table_style2);
					
					$th = array(("Cheque #"), _("Amount"), _("Date"));
					table_header($th);
					
					$x=0;
					do{
						if($x+1==$_POST['num_cheque'])
							$amount+=$excess;
						
						start_row();
						text_cells('', 'next_reference'.$x, $check_ref+$x,20);
						//text_cells('', 'amount'.$x, $amount,20);
						amount_cells('', 'amount'.$x,price_format($amount),null,null,null);
						if($x>0)
							$date=date('m/d/Y',strtotime($_POST['start_'].' + '.$x.' months'));
						else
							$date=date('m/d/Y',strtotime($_POST['start_']));
						
						date_cells_cv('','date_cheque'.$x,null,$date);
						$x++;
						//echo "<td>&nbsp;</td>";
						end_row();
					}while($x<$_POST['num_cheque']);
				}
					textarea_row(_("Memo"), 'memo_', null, 50, 3);
					end_table(1);
					
					if($_POST['bank_account']!=0)
					submit_center('POST_CHECK', _("Issue Cheque"), true, '', true);
			}
		div_end();
		end_form();
	}
	
	end_page();

} else {
	display_error(_("No Payment has been Selected"), true);
    end_page();
    exit;
}

?>
