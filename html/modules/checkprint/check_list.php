<?php

$page_security = 'SA_APVVOUCHER';
$path_to_root="../..";
include($path_to_root . "/includes/session.inc");
include($path_to_root . "/purchasing/includes/purchasing_ui.inc");
include($path_to_root . "/modules/checkprint/includes/check_accounts_db.inc");
//include_once($path_to_root . "/reporting/includes/reporting.inc");
include($path_to_root . "/modules/checkprint/includes/check_pdf.inc");


$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_("Check Issue / Print"), false, false, "", $js);


if (isset($_GET['FromDate'])){
	$_POST['TransAfterDate'] = $_GET['FromDate'];
}
if (isset($_GET['ToDate'])){
	$_POST['TransToDate'] = $_GET['ToDate'];
}

//------------------------------------------------------------------------------------------------

start_form(false, true);

start_table("class='tablestyle_noborder'");
start_row();

ref_cells(_("Cheque #:"), 'check_no', '',null, '', true);
echo "<td> || </td>";
ref_cells(_("APV #:"), 'apv', '',null, '', true);
echo "<td> || </td>";
ref_cells(_("CV #:"), 'cv', '',null, '', true);

date_cells(_("From:"), 'TransAfterDate', '', null, -30);
date_cells(_("To:"), 'TransToDate');

submit_cells('Refresh Inquiry', _("Search"),'',_('Refresh Inquiry'), true);

end_row();
end_table();

end_form();


//------------------------------------------------------------------------------------------------

function get_transactions()
{
	global $db;

    $date_after = date2sql($_POST['TransAfterDate']);
    $date_to = date2sql($_POST['TransToDate']);
	$apv = $_POST['apv'];
	$check_no = $_POST['check_no'];
	$cv = $_POST['cv'];
	//display_error($cv);
    // Sherifoz 22.06.03 Also get the description
	/*
    $sql = "SELECT ".TB_PREF."supp_trans.type, ".TB_PREF."supp_trans.trans_no,
    	".TB_PREF."supp_trans.tran_date, ".TB_PREF."supp_trans.reference, ".TB_PREF."supp_trans.supp_reference,
    	(".TB_PREF."supp_trans.ov_amount + ".TB_PREF."supp_trans.ov_gst  + ".TB_PREF."supp_trans.ov_discount) AS TotalAmount, ".TB_PREF."supp_trans.alloc AS Allocated,
		((".TB_PREF."supp_trans.type = 20 OR ".TB_PREF."supp_trans.type = 21) AND ".TB_PREF."supp_trans.due_date < '" . date2sql(Today()) . "') AS OverDue,
    	(ABS(".TB_PREF."supp_trans.ov_amount + ".TB_PREF."supp_trans.ov_gst  + ".TB_PREF."supp_trans.ov_discount - ".TB_PREF."supp_trans.alloc) <= 0.005) AS Settled,
		".TB_PREF."suppliers.curr_code, ".TB_PREF."suppliers.supp_name, ".TB_PREF."supp_trans.due_date
    	FROM ".TB_PREF."supp_trans, ".TB_PREF."suppliers
    	WHERE ".TB_PREF."suppliers.supplier_id = ".TB_PREF."supp_trans.supplier_id
     	AND ".TB_PREF."supp_trans.tran_date >= '$date_after'
    	AND ".TB_PREF."supp_trans.tran_date <= '$date_to'";
		$sql .= " AND ".TB_PREF."supp_trans.type = 22";
    $sql .= " ORDER BY ".TB_PREF."supp_trans.tran_date";
	*/
   $sql = "SELECT ".TB_PREF."bank_trans.*,".TB_PREF."check_voucher.apv_no,".TB_PREF."check_voucher.cv_no 
			FROM ".TB_PREF."bank_trans, ".TB_PREF."check_voucher, ".TB_PREF."bank_accounts
			WHERE ".TB_PREF."check_voucher.cv_no NOT IN (SELECT id FROM ".TB_PREF."voided WHERE type=50) 
			AND ".TB_PREF."bank_trans.type IN (50,1)
			AND ".TB_PREF."check_voucher.cv_no = ".TB_PREF."bank_trans.ref 
			AND ".TB_PREF."check_voucher.amount != 0 ";
	
	if($apv!='')
	{
		//$sql .= " AND ".TB_PREF."check_voucher.apv_no = ".db_escape($apv);
		$sql .= " AND ( SELECT ref FROM ".TB_PREF."apv_header WHERE id = ".TB_PREF."check_voucher.apv_no ) LIKE ".db_escape($apv."%");
	}else if($check_no!='')
	$sql .= " AND ".TB_PREF."check_voucher.cheque_no = ".db_escape($check_no);
	else if($cv!='')
	$sql .= " AND ".TB_PREF."check_voucher.cv_no = ".db_escape($cv);
	else 
	$sql .= "AND trans_date >= '$date_after'
			 AND trans_date <= '$date_to'";
	$sql .= " GROUP BY ".TB_PREF."bank_trans.id
			ORDER BY trans_date,".TB_PREF."bank_trans.id";
			
	return db_query($sql,"No supplier transactions were returned");
}

function get_apv(){
	$date_after = date2sql($_POST['TransAfterDate']);
    $date_to = date2sql($_POST['TransToDate']);	
	$sql="SELECT a.*,SUM(b.amount) as 'TotalAmount' 
		  FROM ".TB_PREF."apv_header a,".TB_PREF."apv_details b 
		  WHERE a.ref NOT IN (SELECT id FROM ".TB_PREF."voided WHERE type=45) 
		  AND b.apv_no = a.id 
		  AND b.amount>=0 
		  AND tran_date >= '$date_after' AND tran_date <= '$date_to' 
		  GROUP BY a.id 
		  ORDER BY tran_date,id";		
	return db_query($sql,"No Accounts Payable Voucher returned");
}

function get_payment_transactions(){
	$date_after = date2sql($_POST['TransAfterDate']);
    $date_to = date2sql($_POST['TransToDate']);	
    $sql = "SELECT ".TB_PREF."bank_trans.*,".TB_PREF."check_voucher.payment_no 
			FROM ".TB_PREF."bank_trans, ".TB_PREF."check_voucher, ".TB_PREF."bank_accounts
			WHERE ".TB_PREF."check_voucher.cv_no NOT IN (SELECT id FROM ".TB_PREF."voided WHERE type=1)
			AND trans_date >= '$date_after'
			AND trans_date <= '$date_to'
			AND ".TB_PREF."bank_trans.type IN (50,1)
			AND 0_bank_trans.trans_no = 0_check_voucher.payment_no
			GROUP BY ".TB_PREF."bank_trans.id
			ORDER BY trans_date,".TB_PREF."bank_trans.id";
	return db_query($sql,"No bank transactions were returned");
}

/* not needed anymore */
/*
function get_cv_apv($cv_no){
	$date_after = date2sql($_POST['TransAfterDate']);
    $date_to = date2sql($_POST['TransToDate']);	
	$sql='SELECT cv_no,apv_no FROM '.TB_PREF.'check_voucher WHERE cv_no = '.$cv_no;
	$result=db_query($sql,"Invalid CV Number, contact enjo!");
	$row=db_fetch($result);
	return $row;
}
*/

//------------------------------------------------------------------------------------------------

//------------------------------------------------------------------------------------------------


if(get_post('Refresh Inquiry'))
{
	$Ajax->activate('trans_tbl');
	$Ajax->activate('trans_pay_tbl');
	$Ajax->activate('apv_tbl');
	$Ajax->activate('totals_tbl');
}
//------------------------------------------------------------------------------------------------

/*show a table of the transactions returned by the sql */
$result = get_payment_transactions();
div_start('trans_pay_tbl');

if (db_num_rows($result) == 0)
{
	display_note(_("There are no Bank Payments to display for the given dates."), 1, 1);
} else{


start_table("$table_style width=80%");
$th = array((""), _("Cheque # Issued"), _("Type"), _("#"), _("Reference"), _("Payee"),
		_("Date Paid"),
//		_("Currency"),
		_("Debit"), _("Credit"), "");

 table_header($th);

 $j = 1;
 $k = 0; //row colour counter

 while ($myrow = db_fetch($result))
 {

	$date = sql2date($myrow["trans_date"]);
	//$date = sql2date($myrow["trans_date"]);

	// Check if a check is issued for this trans no
	if (!get_voided_entry($myrow["type"], $myrow["trans_no"]))
	{
	if (is_issued_check($myrow["type"],$myrow["trans_no"])) {
		$group=show_check_ref($myrow["type"],$myrow["trans_no"],db_escape($myrow["id"]/*$myrow["ref"]*/));
		$check_print = "$path_to_root/modules/checkprint/check_print.php?" . SID . "type_id=" . $myrow["type"] . "&amp;trans_no=" . $myrow["trans_no"];
		
		$string='';
		while(current($group)){
			if(key($group)>0)
				$string.=',';
			$string.=$group[key($group)][0];
			next($group);
		}
		label_cell(print_document_cheque_link($string, _("Print"), true, $group[0][1]));
		label_cell($group[0][0]);
 	} else {
		$check_issue = "$path_to_root/modules/checkprint/check_issue.php?" . SID . "type_id=" . $myrow["type"] . "&amp;trans_no=" . $myrow["trans_no"];
		label_cell("");
		label_cell("<a href=$check_issue>" . _("Issue Cheque No") . "</a>");
	}
	label_cell(systypes::name($myrow["type"]));
	if($myrow['type']!=50){
		label_cell(get_trans_view_str($myrow["type"],$myrow["trans_no"]));
		label_cell(get_trans_view_str($myrow["type"],$myrow["trans_no"], $myrow["ref"]));
	}else{
		//label_cell(get_cv_view_str($myrow['trans_no']));
		label_cell(get_cv_view_str($myrow[4]));
		label_cell(get_apv_view_str($myrow['apv_no'], 'APV '.$myrow['apv_no']));
	}
	
	//label_cell($myrow["supp_name"]);
	label_cell(payment_person_types::person_name($myrow["person_type_id"],$myrow["person_id"]));
	label_cell($date);
   //label_cell($myrow["curr_code"]);
   /*if ($myrow["TotalAmount"] >= 0)
    	label_cell("");
	amount_cell(abs($myrow["TotalAmount"]));
	if ($myrow["TotalAmount"] < 0) 
       */
	if ($myrow["amount"] >= 0)
    	label_cell("");
	amount_cell(abs($myrow["amount"]));
	if ($myrow["amount"] < 0)
		label_cell("");

	label_cell(get_gl_view_str($myrow["type"], $myrow["trans_no"]));
	
	end_row();

	$j++;
	If ($j == 12)
	{
		$j=1;
		table_header($th);
	}
 //end of page full new headings if
	}
 }
 //end of while loop

 end_table(1);

}
div_end();

$result = get_transactions();
div_start('trans_tbl');

if (db_num_rows($result) == 0)
{
	display_note(_("There are no Check Vouchers to display for the given dates."), 1, 1);
} else{


start_table("$table_style width=80%");
$th = array((""), _("Cheque # Issued"), _("Type"), _("#"), _("Reference"), _("Payee"),
		_("Date Paid"),
//		_("Currency"),
		_("Debit"), _("Credit"), "");

 table_header($th);

 $j = 1;
 $k = 0; //row colour counter

 while ($myrow = db_fetch($result))
 {

	$date = sql2date($myrow["trans_date"]);
	//$date = sql2date($myrow["trans_date"]);

	// Check if a check is issued for this trans no
	if (!get_voided_entry($myrow["type"], $myrow["trans_no"]))
	{
	if (is_issued_check($myrow["type"],$myrow["trans_no"])) {
		$group=show_check_ref($myrow["type"],$myrow["trans_no"],$myrow["id"]/*$myrow["ref"]*/);
		$check_print = "$path_to_root/modules/checkprint/check_print.php?" . SID . "type_id=" . $myrow["type"] . "&amp;trans_no=" . $myrow["trans_no"];
		
		$string='';
		while(current($group)){
			if(key($group)>0)
				$string.=',';
			$string.=$group[key($group)][0];
			next($group);
		}
		label_cell(print_document_cheque_link($string, _("Print"), true, $group[0][1]));
		label_cell($group[0][0]);
 	} else {
		$check_issue = "$path_to_root/modules/checkprint/check_issue.php?" . SID . "type_id=" . $myrow["type"] . "&amp;trans_no=" . $myrow["trans_no"];
		label_cell("");
		label_cell("<a href=$check_issue>" . _("Issue Cheque No") . "</a>");
	}
	label_cell($systypes_array[$myrow["type"]]);
	if($myrow['type']!=50){
		label_cell(get_trans_view_str($myrow["type"],$myrow["trans_no"]));
		label_cell(get_trans_view_str($myrow["type"],$myrow["trans_no"], $myrow["reference"]));
	}else{
		//label_cell(get_cv_view_str($myrow['trans_no']));
		label_cell(get_gl_view_str($myrow["type"], $myrow["trans_no"],$myrow['cv_no']));
		
		$apvs=explode(',',$myrow['apv_no']);
		$count=count($apvs);
		$aaa='';
		foreach($apvs as $key=>$value){
			$trans_no = $value;
			$ref_ = get_reference_no($trans_no, 45);
			if(strlen($aaa)>0)
				$aaa.=',';
			$aaa.=$ref_;
		}
		label_cell(get_apv_view_str($myrow['apv_no'], 'APV '.$aaa));
	}
	
	//label_cell($myrow["supp_name"]);
	label_cell(payment_person_name($myrow["person_type_id"],$myrow["person_id"], false));
	label_cell($date);
   //label_cell($myrow["curr_code"]);
   /*if ($myrow["TotalAmount"] >= 0)
    	label_cell("");
	amount_cell(abs($myrow["TotalAmount"]));
	if ($myrow["TotalAmount"] < 0) 
       */
	if ($myrow["amount"] >= 0)
    	label_cell("");
	amount_cell(abs($myrow["amount"]));
	if ($myrow["amount"] < 0)
		label_cell("");

	label_cell(get_gl_view_str($myrow["type"], $myrow["trans_no"]));
	
	end_row();

	$j++;
	If ($j == 12)
	{
		$j=1;
		table_header($th);
	}
 //end of page full new headings if
	}
 }
 //end of while loop

 end_table(1);

}
div_end();

$result = get_apv();

div_start('apv_tbl');
print_hidden_cheque_script();
if (db_num_rows($result) == 0)
{
	display_note(_("There are no APV's to display for the given dates."), 1, 1);
} else
{

$apv = $_POST['apv'];
$cv_ = $_POST['cv_'];
$cv  = $_POST['cv'];
if($apv=='' && $cv_=='' && $cv==''){
start_form(false,false,'batch_check_issue.php');
start_table("class='tablestyle_noborder'");
start_row();

//submit_cells('Refresh Inquiry', _("Search"),'',_('Refresh Inquiry'), true);
submit_cells('batchIssueCV','Issue Batch Check Voucher',false);
end_row();
end_table();

start_table("$table_style width=80%");
$th = array((""), '', _("Type"), _("#"), _("Payee"),
		_("Date Created"), _("Amount"),"");

 table_header($th);

 $j = 1;
 $k = 0; //row colour counter

 while ($myrow = db_fetch($result))
 {
	if($myrow["TotalAmount"] != $myrow["alloc"])
	{
		$date = sql2date($myrow["tran_date"]);

		// Check if a check is issued for this trans no
		
		//$check_issue = "$path_to_root/modules/checkprint/check_issue.php?" . SID . "type_id=" . 45 . "&amp;trans_no=" . $myrow['ref'];
		
		if (is_issued_check(45,$myrow['id'])) {
			list($check_reference, $check_act) = show_check_ref(45,$myrow['id']);
			$check_print = "$path_to_root/modules/checkprint/check_print.php?" . SID . "type_id=" . 45 . "&amp;trans_no=" . $myrow['id'];
			label_cell(print_document_cheque_link($check_reference, _("Print"), true, $check_act));
			label_cell($check_reference);
		} 
		else if ($myrow["type"] == '')
		{	
			//$check_issue = "$path_to_root/gl/gl_cv.php?" . SID . "trans_no=" . $myrow['ref'];
			//$check_issue = "$path_to_root/modules/checkprint/check_issue.php?type_id=45&trans_no=" . $myrow['ref'];
			$check_issue = "$path_to_root/modules/checkprint/check_issue.php?" . SID . "type_id=" . 45 . "&amp;trans_no=" . $myrow['id'];
			label_cell("");
			label_cell("<a href=$check_issue>" . _("Issue Cheque No") . "</a>");
		}
		else {
			$check_issue = "$path_to_root/modules/checkprint/check_issue.php?" . SID . "type_id=" . 45 . "&amp;trans_no=" . $myrow['id'];
			echo "<td><input type=checkbox name=check[] value='".$myrow['id']."'></td>";
			label_cell("<a href=$check_issue>" . _("Issue Check Voucher") . "</a>");
		}

		label_cell($systypes_array[ST_APVVOUCHER]);
		
		//label_cell(get_apv_view_str($myrow['ref'], $myrow['id']));
		label_cell(get_apv_view_str($myrow['id'], $myrow['ref']));
		
		//label_cell(get_payee($myrow['cust_supp'],$myrow['cust_supp_id']));
		if($myrow['cust_supp'] == PT_QUICKENTRY)
			label_cell($myrow['cust_supp_id']);
		else
			label_cell(payment_person_name($myrow['cust_supp'],$myrow['cust_supp_id'], false));
		
		label_cell($date);
		
		//label_cell($myrow["curr_code"]);
		
		amount_cell(abs($myrow["TotalAmount"]-$myrow["alloc"]));
		if ($myrow["TotalAmount"] < 0)
			label_cell("");
		$modify=$path_to_root.'/gl/gl_apv.php?ModifyOrderNumber='.$myrow['id'];
		label_cell("<a href=$modify>" . _("Edit") . "</a>");
		//amount_cell(get_apv_amount($myrow['ref']));
		//label_cell(get_gl_view_str(45, $myrow['ref']));

		end_row();

		$j++;
		If ($j == 12)
		{
			$j=1;
			table_header($th);
		}
		//end of page full new headings if
	}
 }
 //end of while loop

 end_table(1);

}
end_form();
}
div_end();
end_page();
?>
