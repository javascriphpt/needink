<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLREP';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	GL Accounts Transactions
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

print_GL_transactions();

//----------------------------------------------------------------------------------------------------

function get_sales_ref_no_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_sales_ref_no_OR_PR_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location, form_type
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_so_form_cat_name_report($cat_id)
{
	$sql = "SELECT name FROM ".TB_PREF."sales_form_category 
			WHERE id=".db_escape($cat_id);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

function get_check_number($type, $type_no)
{
	$sql = "SELECT chk_number FROM ".TB_PREF."cheque_details 
			WHERE bank_trans_id=".db_escape($type_no)."
			AND type=".db_escape($type);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

function get_branch_tin($type, $trans_no)
{
	$sql = "SELECT c.gst_no 
			FROM ".TB_PREF."gl_trans a, ".TB_PREF."debtor_trans b, ".TB_PREF."cust_branch c
			WHERE a.type = b.type
			AND a.type_no = b.trans_no
			AND b.branch_code = c.branch_code
			AND a.type=".db_escape($type)."
			AND a.type_no=".db_escape($trans_no);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

function get_reference_no_in_bank_trans($type, $trans_no)
{
	$sql = "SELECT reference_no
			FROM ".TB_PREF."bank_trans
			WHERE type=".db_escape($type)."
			AND trans_no=".db_escape($trans_no);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

function print_GL_transactions()
{
	global $path_to_root, $systypes_array;

	$dim = get_company_pref('use_dimension');
	$dimension = $dimension2 = 0;

	$from = $_POST['PARAM_0'];
	$to = $_POST['PARAM_1'];
	$fromacc = $_POST['PARAM_2'];
	$toacc = $_POST['PARAM_3'];
	if ($dim == 2)
	{
		$dimension = $_POST['PARAM_4'];
		$dimension2 = $_POST['PARAM_5'];
		$comments = $_POST['PARAM_6'];
		$destination = $_POST['PARAM_7'];
	}
	else if ($dim == 1)
	{
		$dimension = $_POST['PARAM_4'];
		$comments = $_POST['PARAM_5'];
		$destination = $_POST['PARAM_6'];
	}
	else
	{
		$comments = $_POST['PARAM_4'];
		$destination = $_POST['PARAM_5'];
	}
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$rep = new FrontReport(_('GL Account Transactions'), "GLAccountTransactions", user_pagesize(), 6.5);
	$dec = user_price_dec();

  //$cols = array(0, 80, 100, 150, 210, 280, 340, 400, 450, 510, 570);
	$cols = array(0, 50, 60, 110, 160, 210, 260, 330, 400, 450, 500, 555);
	//------------0--1---2---3----4----5----6----7----8----9----10-------
	//-----------------------dim1-dim2-----------------------------------
	//-----------------------dim1----------------------------------------
	//-------------------------------------------------------------------
	$aligns = array('left', 'left', 'left',	'left',	'left',	'left',	'left',	'right', 'right', 'right', 'right');

	// if ($dim == 2)
		$headers = array(_('Type'),	_(' '), _('#'),	_('Date'), _('Reference#'), _('TIN #'),
			_('Person/Item'), _('Debit'),	_('Credit'), _('Balance'), _('Check #'));
	// elseif ($dim == 1)
		// $headers = array(_('Type'),	_(' '), _('#'),	_('Date'), _('Dimension'), "", _('Person/Item'),
			// _('Debit'),	_('Credit'), _('Balance'), _('Check #'));
	// else
		// $headers = array(_('Type'),	_(' '), _('#'),	_('Date'), "", "", _('Person/Item'),
			// _('Debit'),	_('Credit'), _('Balance'), _('Check #'));

	if ($dim == 2)
	{
    	$params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to),
    				    2 => array('text' => _('Accounts'),'from' => $fromacc,'to' => $toacc),
                    	3 => array('text' => _('Dimension')." 1", 'from' => get_dimension_string($dimension),
                            'to' => ''),
                    	4 => array('text' => _('Dimension')." 2", 'from' => get_dimension_string($dimension2),
                            'to' => ''));
    }
    else if ($dim == 1)
    {
    	$params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to),
    				    2 => array('text' => _('Accounts'),'from' => $fromacc,'to' => $toacc),
                    	3 => array('text' => _('Dimension'), 'from' => get_dimension_string($dimension),
                            'to' => ''));
    }
    else
    {
    	$params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to),
    				    2 => array('text' => _('Accounts'),'from' => $fromacc,'to' => $toacc));
    }

	$rep->Font();
	$rep->Info($params, $cols, $headers, $aligns);
	$rep->Header();

	$accounts = get_gl_accounts($fromacc, $toacc);

	while ($account=db_fetch($accounts))
	{
		if (is_account_balancesheet($account["account_code"]))
			$begin = "";
		else
		{
			$begin = begin_fiscalyear();
			if (date1_greater_date2($begin, $from))
				$begin = $from;
			$begin = add_days($begin, -1);
		}
		$prev_balance = get_gl_balance_from_to($begin, $from, $account["account_code"], $dimension, $dimension2);

		$trans = get_gl_transactions($from, $to, -1, $account['account_code'], $dimension, $dimension2);
		$rows = db_num_rows($trans);
		if ($prev_balance == 0.0 && $rows == 0)
			continue;
		$rep->Font('bold');
		$rep->TextCol(0, 4,	$account['account_code'] . " " . $account['account_name'], -2);
		$rep->TextCol(4, 6, _('Opening Balance'));
		if ($prev_balance > 0.0)
			$rep->AmountCol(7, 8, abs($prev_balance), $dec);
		else
			$rep->AmountCol(8, 9, abs($prev_balance), $dec);
		$rep->Font();
		$total = $prev_balance;
		$rep->NewLine(2);
		if ($rows > 0)
		{
			while ($myrow=db_fetch($trans))
			{
				$total += $myrow['amount'];

				$rep->TextCol(0, 1, $systypes_array[$myrow["type"]], -2);
				$reference = get_reference($myrow["type"], $myrow["type_no"]);
				//$rep->TextCol(1, 2, $reference);
				//$rep->TextCol(2, 3,	$myrow['type_no'], -2);
				
				if($myrow["type"] == ST_CUSTDELIVERY || $myrow["type"] == ST_SALESINVOICE || $myrow["type"] == ST_CUSTCREDIT)
				{
					$ref_no = get_sales_ref_no_report($myrow["type_no"], $myrow["type"]);
					$rep->TextCol(2, 3,	$ref_no["location"]."-".$ref_no["form_type_no"], -2);
				}
				else if($myrow["type"] == ST_CUSTPAYMENT)
				{
					$ref_no = get_sales_ref_no_OR_PR_report($myrow["type_no"], $myrow["type"]);
					$rep->TextCol(2, 3,	$ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name_report($ref_no["form_type"]), -2);
				}
				else
					$rep->TextCol(2, 3, get_reference($myrow['type'], $myrow['type_no']));
				
				$rep->DateCol(3, 4,	$myrow["tran_date"], true);
				$rep->TextCol(4, 5,	get_reference_no_in_bank_trans($myrow['type'], $myrow['type_no']));
				// if ($dim >= 1)
					// $rep->TextCol(4, 5,	get_dimension_string($myrow['dimension_id']));
				// if ($dim > 1)
					// $rep->TextCol(5, 6,	get_dimension_string($myrow['dimension2_id']));
				if($myrow["person_type_id"] == PT_SUPPLIER)
					$rep->TextCol(5, 6,	get_supplier_tin($myrow["person_id"]));
				else if($myrow["person_type_id"] == PT_CUSTOMER)
					$rep->TextCol(5, 6,	get_branch_tin($myrow["type"], $myrow["type_no"]));
				else
					$rep->TextCol(5, 6,	'');
					
				$rep->TextCol(6, 7,	payment_person_name($myrow["person_type_id"],$myrow["person_id"], false));
				if ($myrow['amount'] > 0.0)
					$rep->AmountCol(7, 8, abs($myrow['amount']), $dec);
				else
					$rep->AmountCol(8, 9, abs($myrow['amount']), $dec);
				$rep->TextCol(9, 10, number_format2($total, $dec));
				
				if($myrow["type"] == ST_BANKPAYMENT || $myrow["type"] == ST_CUSTPAYMENT || $myrow["type"] == ST_SUPPAYMENT 
					|| $myrow["type"] == ST_BANKDEPOSIT || $myrow["type"] == ST_BANKTRANSFER)
				{
					$rep->TextCol(10, 11, get_check_number($myrow["type"], $myrow["type_no"]));
				}
				
				$rep->NewLine();
				if ($rep->row < $rep->bottomMargin + $rep->lineHeight)
				{
					$rep->Line($rep->row - 2);
					$rep->Header();
				}
			}
			$rep->NewLine();
		}
		$rep->Font('bold');
		$rep->TextCol(4, 6,	_("Ending Balance"));
		if ($total > 0.0)
			$rep->AmountCol(7, 8, abs($total), $dec);
		else
			$rep->AmountCol(8, 9, abs($total), $dec);
		$rep->Font();
		$rep->Line($rep->row - $rep->lineHeight + 4);
		$rep->NewLine(2, 1);
	}
	$rep->End();
}

?>