<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_MANUFTRANSVIEW';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/includes/ui/ui_lists.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
$output=print_work_order_inquiry();

header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=WorkOrderInquiry.xls");
header("Pragma: no-cache");
header("Expires: 0");	

echo $output;

function get_transactions($OrderNumber, $StockLocation, $type, $OverdueOnly, $OpenOnly, $SelectedStockItem, $outstanding_only)
{
	$sql = "SELECT
			workorder.id,	
			workorder.type,
			location.location_name,
			item.description,
			workorder.units_reqd,
			workorder.units_issued,
			workorder.date_,
			workorder.required_by,
			workorder.released_date,
			workorder.closed,
			workorder.released,
			workorder.stock_id,
			unit.decimals,
			workorder.wo_ref,
			workorder.form_type_no,
			workorder.released
			FROM ".TB_PREF."workorders as workorder,"
				.TB_PREF."stock_master as item,"
				.TB_PREF."item_units as unit,"
				.TB_PREF."locations as location
			WHERE workorder.stock_id=item.stock_id 
				AND workorder.loc_code=location.loc_code
				AND item.units=unit.abbr";

		if ($OpenOnly != 0 || $outstanding_only != 0)
			$sql .= " AND workorder.closed=0";
		
		if ($StockLocation != 0)
			$sql .= " AND workorder.loc_code=".db_escape($StockLocation);
		
		if ($type != 0)
			$sql .= " AND workorder.type=".db_escape($type);
		
		if ($OrderNumber != 0)
			$sql .= " AND workorder.form_type_no LIKE ".db_escape('%'.$OrderNumber.'%');
		
		if ($SelectedStockItem != 0)
			$sql .= " AND workorder.stock_id=".db_escape($SelectedStockItem);
		
		if ($OverdueOnly != 0)
		{
			$Today = date2sql(Today());

			$sql .= " AND workorder.required_by < '$Today' ";
		}

    return db_query($sql,"No transactions were returned");
}

function get_wo_comments_string($type, $type_no)
{
	$sql = "SELECT * FROM ".TB_PREF."comments WHERE type="
		.db_escape($type)." AND id=".db_escape($type_no);

	$result = db_query($sql, "could not query comments transaction table");

	$str_return = "";
	while ($comment = db_fetch($result))
	{
		if (strlen($str_return))
			$str_return = $str_return . " \n";
		$str_return = $str_return . $comment["memo_"];
	}
	
	return $str_return;
}

//----------------------------------------------------------------------------------------------------

function print_work_order_inquiry()
{
    global $path_to_root, $SysPrefs, $wo_types_array;

	include_once($path_to_root . "/reporting/includes/pdf_report.inc");
	
	$OrderNumber = $_GET['OrderNumber'];
	$StockLocation = $_GET['StockLocation'];
	$type = $_GET['type'];
	$OverdueOnly = $_GET['OverdueOnly'];
	$OpenOnly = $_GET['OpenOnly'];
	$SelectedStockItem = $_GET['SelectedStockItem'];
	$outstanding_only = $_GET['outstanding_only'];
	$output = $_GET['output'];
		
	if ($output == 0)
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/excel_report.inc");

	
	$cols = array(0, 50, 100, 250, 470, 510, 590, 660, 730);

	$headers = array(_('#'), _('Type'), _('Location'), _('Item'), _('Required'), _('Manufactured'), _('Date'), _('Required By'));

	$aligns = array('left',	'left',	'left',	'left',	'right', 'right', 'right', 'right');

    $params =   array( 	0 => '');

    $rep = new FrontReport(_('Work Order Inquiry'), "WorkOrderInquiry", user_pagesize(), 9, 'L');

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	$res = get_transactions($OrderNumber, $StockLocation, $type, $OverdueOnly, $OpenOnly, $SelectedStockItem, $outstanding_only);
	
	ob_start();
	?>
		<html>
			<head>
				<style>
					.title{
						font-weight:bold;
						font-size:18px;
					}
					td{
						border:solid thin black;
					}
					.total{
						font-weight:bold;
					}
					.header{
						font-style:italic;
						font-weight:bold;
					}
				</style>
			</head>
			<body>
	<?php
	
	echo '<span class="title">Work Order Inquiry</span>';
	echo '<br>';
	echo '<br>';
		$headers = array(_('#'), _('Type'), _('Location'), _('Item'), _('Assigned Person'), _('Required'), _('Manufactured'), _('Date'), _('Required By'));
		
	start_table();
	start_row();
	foreach($headers as $value){
		echo "<td class='header'>".$value."</td>";
	}
	end_row();


	while ($myrow = db_fetch($res))
	{
		start_row();
		echo "<td>".$myrow["form_type_no"]."</td>";
		echo "<td>".$wo_types_array[$myrow["type"]]."</td>";
		echo "<td>".$myrow["location_name"]."</td>";
		echo "<td>".$myrow["stock_id"]." - ".$myrow["description"]."</td>";
		echo "<td>".get_wo_comments_string(ST_WORKORDER, $myrow['id'])."</td>";
		echo "<td>".$myrow["units_reqd"]."</td>";
		echo "<td>".$myrow["units_issued"]."</td>";
		echo "<td>".$myrow["date_"]."</td>";
		echo "<td>".$myrow["required_by"]."</td>";
	}

	return ob_get_clean();
}

?>