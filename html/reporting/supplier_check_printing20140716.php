<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPCHECKDETAILSINQ';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/includes/ui/ui_lists.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();

$output=print_supplier_check_printing();

header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=SupplierCheckPrinting.xls");
header("Pragma: no-cache");
header("Expires: 0");	

echo $output;


//----------------------------------------------------------------------------------------------------

function print_supplier_check_printing()
{
    global $path_to_root, $systypes_array;
	
	$supplier = $_GET['supplier'];
	$from = $_GET['from'];
	$to = $_GET['to'];
	
	include_once($path_to_root . "/reporting/includes/excel_report.inc");
	
	$sql = "SELECT
	".TB_PREF."supp_trans.trans_no,
	".TB_PREF."suppliers.supp_name,
	".TB_PREF."cheque_details.chk_number,
	".TB_PREF."cheque_details.chk_date,
	ABS(".TB_PREF."bank_trans.amount) as total_amount,
	".TB_PREF."supp_trans.type,
	".TB_PREF."supp_trans.reference,
	".TB_PREF."supp_trans.tran_date,
	".TB_PREF."suppliers.supp_ref
	FROM
	".TB_PREF."cheque_details ,
	".TB_PREF."supp_trans ,
	".TB_PREF."suppliers ,
	".TB_PREF."bank_trans
	WHERE
	".TB_PREF."bank_trans.type = 22 AND ".TB_PREF."supp_trans.type = 22 AND
	".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
	".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type AND
	".TB_PREF."supp_trans.reference =  ".TB_PREF."bank_trans.ref AND
	".TB_PREF."supp_trans.supplier_id =  ".TB_PREF."suppliers.supplier_id AND 
	".TB_PREF."cheque_details.chk_date >= '$from' AND	
	".TB_PREF."cheque_details.chk_date <= '$to' ";	

	if ($supplier != 0)
		$sql .= " AND ".TB_PREF."supp_trans.supplier_id = ".db_escape($supplier);
	
	$result = db_query($sql, "Transactions could not be retrieved");
	
	ob_start();
		?>
			<html>
				<head>
					<style>
						.title{
							font-weight:bold;
							font-size:18px;
						}
						td{
							border:solid thin black;
						}
						.total{
							font-weight:bold;
						}
						.header{
							font-style:italic;
							font-weight:bold;
						}
					</style>
				</head>
				<body>
		<?php
		
				// echo '<span class="title">Supplier Check Printing</span>';
				// echo '<br>';
				// echo '<br>';
				$headers = array('Prefix', 'Batch No.', 'Voucher No.', 'Voucher Date', 'Payee Code', 'Payee Name', 'Check Date', 'Check Amount', 'Reference No.', 'Invoice No.', 'Invoice Date', 'Invoice Amount', 'DM/CM Amount', 'Tax Amount', 'Amount Paid', 'Purpose of Check', 'Tax Code', 'Type of Check');
					
				start_table();
				start_row();
				foreach($headers as $value){
					echo "<td class='header'>".$value."</td>";
				}
				end_row();

				while ($myrow = db_fetch($result))
				{
					start_row();
					echo "<td>C</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>".$myrow['reference']."</td>";
					echo "<td>".$myrow['tran_date']."</td>";
					echo "<td>".$myrow['supp_ref']."</td>";
					echo "<td>".$myrow['supp_name']."</td>";
					echo "<td>".$myrow['chk_date']."</td>";
					echo "<td>".number_format2($myrow['total_amount'], 2)."</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					
					$alloc_sql = "SELECT trans.trans_no, trans.supp_reference, trans.reference, trans.tran_date,
							(trans.ov_amount + trans.ov_gst) as total, alloc.amt as alloc_amount
						FROM ".TB_PREF."supp_allocations alloc,
							".TB_PREF."supp_trans trans
						WHERE trans.trans_no = alloc.trans_no_to
						AND trans.type = alloc.trans_type_to
						AND alloc.trans_type_from = ".db_escape($myrow['type'])."
						AND  alloc.trans_no_from = ".db_escape($myrow['trans_no']);
					$alloc_result = db_query($alloc_sql, "Transactions could not be retrieved");
					
					while ($row = db_fetch($alloc_result))
					{
						start_row();
						echo "<td>V</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>".$row['supp_reference']."</td>";
						echo "<td>".$row['reference']."</td>";
						echo "<td>".$row['tran_date']."</td>";
						echo "<td>".number_format2($row['total'], 2)."</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>".number_format2($row['alloc_amount'], 2)."</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>";
						echo "<td>&nbsp;</td>";
					}
				}
								
			return ob_get_clean();
}

?>