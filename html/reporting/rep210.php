<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

$page_security = $_POST['PARAM_0'] == $_POST['PARAM_1'] ?
	'SA_SUPPTRANSVIEW' : 'SA_SUPPBULKREP';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Purchase Orders
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");

//----------------------------------------------------------------------------------------------------

print_remittances();

//----------------------------------------------------------------------------------------------------
function get_remittance($type, $trans_no)
{
   	$sql = "SELECT ".TB_PREF."supp_trans.*, 
   		(".TB_PREF."supp_trans.ov_amount+".TB_PREF."supp_trans.ov_gst+".TB_PREF."supp_trans.ov_discount+".TB_PREF."supp_trans.ewt) AS Total, 
   		".TB_PREF."suppliers.supp_name,  ".TB_PREF."suppliers.supp_account_no,
   		".TB_PREF."suppliers.curr_code, ".TB_PREF."suppliers.payment_terms, ".TB_PREF."suppliers.gst_no AS tax_id, 
   		".TB_PREF."suppliers.email, ".TB_PREF."suppliers.address, ".TB_PREF."suppliers.contact
		FROM ".TB_PREF."supp_trans, ".TB_PREF."suppliers
		WHERE ".TB_PREF."supp_trans.supplier_id = ".TB_PREF."suppliers.supplier_id
		AND ".TB_PREF."supp_trans.type = ".db_escape($type)."
		AND ".TB_PREF."supp_trans.trans_no = ".db_escape($trans_no);
   	$result = db_query($sql, "The remittance cannot be retrieved");
   	if (db_num_rows($result) == 0)
   		return false;
    return db_fetch($result);
}

function get_allocations_for_remittance($supplier_id, $type, $trans_no)
{
	$sql = get_alloc_supp_sql("amt, supp_reference, trans.alloc", "trans.trans_no = alloc.trans_no_to
		AND trans.type = alloc.trans_type_to
		AND alloc.trans_no_from=".db_escape($trans_no)."
		AND alloc.trans_type_from=".db_escape($type)."
		AND trans.supplier_id=".db_escape($supplier_id),
		TB_PREF."supp_allocations as alloc");
	$sql .= " ORDER BY trans_no";
	return db_query($sql, "Cannot retreive alloc to transactions");
}

function get_chk_details($trans_no, $type)
{
	$sql = "SELECT
			".TB_PREF."cheque_details.id,
			".TB_PREF."cheque_details.bank_trans_id,
			".TB_PREF."cheque_details.bank,
			".TB_PREF."cheque_details.branch,
			".TB_PREF."cheque_details.chk_number,
			".TB_PREF."cheque_details.chk_date,
			".TB_PREF."bank_trans.amount,
			".TB_PREF."suppliers.supp_name,
			".TB_PREF."supp_trans.tran_date,
			".TB_PREF."supp_trans.trans_no
			FROM
			".TB_PREF."cheque_details ,
			".TB_PREF."supp_trans ,
			".TB_PREF."suppliers ,
			".TB_PREF."bank_trans
			WHERE
			".TB_PREF."bank_trans.trans_no = $trans_no AND ".TB_PREF."bank_trans.type = ".$type." AND ".TB_PREF."supp_trans.type = ".$type." AND
			".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
			".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type AND
			".TB_PREF."supp_trans.reference =  ".TB_PREF."bank_trans.ref AND
			".TB_PREF."supp_trans.supplier_id =  ".TB_PREF."suppliers.supplier_id ";
	
	
	$result = db_query($sql,"Failed to retrieve cheque details -> view_receipt");
	$row = db_fetch($result);
	return $row;

}

function print_remittances()
{
	global $path_to_root, $systypes_array;

	include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$from = $_POST['PARAM_0'];
	$to = $_POST['PARAM_1'];
	$currency = $_POST['PARAM_2'];
	$email = $_POST['PARAM_3'];
	$comments = $_POST['PARAM_4'];

	if ($from == null)
		$from = 0;
	if ($to == null)
		$to = 0;
	$dec = user_price_dec();

 	$fno = explode("-", $from);
	$tno = explode("-", $to);

	$cols = array(4, 85, 150, 225, 275, 360, 450, 515);

	// $headers in doctext.inc
	$aligns = array('left',	'left',	'left', 'left', 'right', 'right', 'right');

	$params = array('comments' => $comments);

	$cur = get_company_Pref('curr_default');

	if ($email == 0)
	{
		$rep = new FrontReport(_('REMITTANCE'), "RemittanceBulk", user_pagesize());
		$rep->currency = $cur;
		$rep->Font();
		$rep->Info($params, $cols, null, $aligns);
	}

	for ($i = $fno[0]; $i <= $tno[0]; $i++)
	{
		if ($fno[0] == $tno[0])
			$types = array($fno[1]);
		else
			$types = array(ST_BANKPAYMENT, ST_SUPPAYMENT, ST_SUPPCREDIT);
		foreach ($types as $j)
		{
			$myrow = get_remittance($j, $i);
			if (!$myrow)
				continue;			
			$baccount = get_default_bank_account($myrow['curr_code']);
			$params['bankaccount'] = $baccount['id'];

			if ($email == 1)
			{
				$rep = new FrontReport("", "", user_pagesize());
				$rep->currency = $cur;
				$rep->Font();
				$rep->title = _('REMITTANCE');
				$rep->filename = "Remittance" . $i . ".pdf";
				$rep->Info($params, $cols, null, $aligns);
			}
			else
				$rep->title = _('REMITTANCE');
			$rep->Header2($myrow, null, $myrow, $baccount, ST_SUPPAYMENT);
			$result = get_allocations_for_remittance($myrow['supplier_id'], $myrow['type'], $myrow['trans_no']);

			$linetype = true;
			$doctype = ST_SUPPAYMENT;
			if ($rep->currency != $myrow['curr_code'])
			{
				include($path_to_root . "/reporting/includes/doctext2.inc");
			}
			else
			{
				include($path_to_root . "/reporting/includes/doctext.inc");
			}

			$total_allocated = 0;
			$rep->TextCol(0, 4,	$doc_Towards, -2);
			$rep->NewLine(2);
			
			while ($myrow2=db_fetch($result))
			{
				$rep->TextCol(0, 1,	$systypes_array[$myrow2['type']], -2);
				$rep->TextCol(1, 2,	$myrow2['supp_reference'], -2);
				$rep->TextCol(2, 3,	sql2date($myrow2['tran_date']), -2);
				$rep->TextCol(3, 4,	sql2date($myrow2['due_date']), -2);
				$rep->AmountCol(4, 5, $myrow2['Total'], $dec, -2);
				$rep->AmountCol(5, 6, $myrow2['Total'] - $myrow2['alloc'], $dec, -2);
				$rep->AmountCol(6, 7, $myrow2['amt'], $dec, -2);

				$total_allocated += $myrow2['amt'];
				$rep->NewLine(1);
				if ($rep->row < $rep->bottomMargin + (15 * $rep->lineHeight))
					$rep->Header2($myrow, null, $myrow, $baccount, ST_SUPPAYMENT);
			}
			
			// ////////////// GL breakdown //////////////
			
			// $sql001 = "SELECT gl.*, cm.account_name, IF(ISNULL(refs.reference), '', refs.reference) AS reference FROM "
				// .TB_PREF."gl_trans as gl
				// LEFT JOIN ".TB_PREF."chart_master as cm ON gl.account = cm.account_code
				// LEFT JOIN ".TB_PREF."refs as refs ON (gl.type=refs.type AND gl.type_no=refs.id)"
				// ." WHERE gl.type= ".db_escape($myrow['type']) 
				// ." AND gl.type_no = ".db_escape($myrow['trans_no'])
				// ." ORDER BY amount DESC, counter";
			// $result001 = db_query($sql001,"could not get transactions");
			
			// $rep->Font('bold');		
			// $rep->NewLine();	
			// $rep->TextCol(0, 4,	"*** GL Breakdown ***", -2);
			// $rep->NewLine();	
			// $rep->TextCol(1, 2,	"Account", -2);
			// $rep->TextCol(3, 4,	"Dimension", -2);
			// $rep->TextCol(4, 5,	"Debit", -2);
			// $rep->TextCol(5, 6,	"Credit", -2);
			// $rep->TextCol(6, 7,	"Memo", -2);
			// $rep->NewLine();	
			// $rep->Font('');
			
			// $dim = get_company_pref('use_dimension');
			
			// while ($myrow001=db_fetch($result001))
			// {
				// $rep->TextCol(0, 3,	$myrow001['account']." - ".$myrow001['account_name'], -2);
				
				// if ($dim >= 1)
					// $rep->TextCol(3, 4,	get_dimension_string($myrow['dimension_id'], true), -2);
								
				// if ($myrow001['amount'] >= 0)
					// $rep->AmountCol(4, 5,	$myrow001['amount'], $dec, -2);
				// else
					// $rep->AmountCol(5, 6,	abs($myrow001['amount']), $dec, -2);
				
				// $rep->TextCol(6, 7,	$myrow001['memo_'], -2);
				
				// $rep->NewLine(1);				
			// }
			
			// ////////////// GL breakdown //////////////
			
			////////////// Check Details //////////////
			
			$myrow002 = get_chk_details($myrow['trans_no'], $myrow['type']);
			
			if($myrow002['amount'] != 0)
			{
				$rep->Font('bold');		
				$rep->NewLine();	
				$rep->TextCol(0, 4,	"*** Cheque Details ***", -2);
				$rep->NewLine();	
				$rep->TextCol(0, 2,	"Bank", -2);
				$rep->TextCol(2, 3,	"Branch", -2);
				$rep->TextCol(3, 4,	"Cheque #", -2);
				$rep->TextCol(4, 5,	"Cheque Date", -2);
				$rep->TextCol(5, 6,	"Amount", -2);
				$rep->NewLine();	
				$rep->Font('');
				
				$rep->TextCol(0, 2,	$myrow002['bank'], -2);
				$rep->TextCol(2, 3,	$myrow002['branch'], -2);
				$rep->TextCol(3, 4,	$myrow002['chk_number'], -2);
				$rep->TextCol(4, 5,	sql2date($myrow002['chk_date']), -2);
				$rep->AmountCol(5, 6, abs($myrow002['amount']), $dec, -2);
			}
			
			////////////// Check Details //////////////

			$rep->row = $rep->bottomMargin + (15 * $rep->lineHeight);

			$rep->TextCol(3, 6, $doc_Total_Allocated, -2);
			$rep->AmountCol(6, 7, $total_allocated, $dec, -2);
			$rep->NewLine();
			$rep->TextCol(3, 6, $doc_Left_To_Allocate, -2);
			$myrow['Total'] *= -1;
			$rep->AmountCol(6, 7, $myrow['Total'] - $total_allocated, $dec, -2);
			
			$rep->NewLine();
			$rep->Font('bold');
			$rep->TextCol(3, 6, "EWT", - 2);
			$myrow['ewt'] *= -1;
			$rep->AmountCol(6, 7, $myrow['ewt'], $dec, -2);
			
			$rep->NewLine();
			$rep->Font('bold');
			$rep->TextCol(3, 6, $doc_Total_Payment, - 2);
			$rep->AmountCol(6, 7, $myrow['Total'], $dec, -2);
			$words = price_in_words($myrow['Total'], ST_SUPPAYMENT);
			if ($words != "")
			{
				$rep->NewLine(2);
				$rep->TextCol(1, 7, $myrow['curr_code'] . ": " . $words, - 2);
			}	
			$rep->Font();
			if ($email == 1)
			{
				$myrow['contact_email'] = $myrow['email'];
				$myrow['DebtorName'] = $myrow['supp_name'];
				if ($myrow['contact'] != '') $myrow['DebtorName'] = $myrow['contact'];
				$rep->End($email, $doc_Order_no . " " . $myrow['reference'], $myrow);
			}
			
			$rep->LineTo(140,50,20,50);
			$rep->LineTo(290,50,170,50);
			$rep->LineTo(440,50,320,50);
			$rep->LineTo(590,50,470,50);
			
			$rep->Font('bold');
			$rep->row = $rep->row - 130;
			$rep->TextWrap(55, $rep->row, $width, 'Prepared By');
			$rep->TextWrap(205, $rep->row, $width, 'Checked By');
			$rep->TextWrap(355, $rep->row, $width, 'Approved By');
			$rep->TextWrap(505, $rep->row, $width, 'Received By');
		}	
	}

	if ($email == 0)
		$rep->End();
}

?>