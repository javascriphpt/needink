<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_sales_report();

function getTransactions($location, $from, $to)
{
	$from = date2sql($from);
	$to = date2sql($to);
	
	$sql = "SELECT ".TB_PREF."debtor_trans.*,
		(".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + 
		".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt)
		AS TotalAmount, ".TB_PREF."debtor_trans.alloc AS Allocated
    	FROM ".TB_PREF."debtor_trans
    	WHERE ".TB_PREF."debtor_trans.tran_date >= '$from'
		AND ".TB_PREF."debtor_trans.tran_date <= '$to'
		AND ".TB_PREF."debtor_trans.location = ".db_escape($location)."
		AND ".TB_PREF."debtor_trans.type = 13
    	ORDER BY ".TB_PREF."debtor_trans.form_type_no, ".TB_PREF."debtor_trans.tran_date";
	
	return db_query($sql,"No transactions were returned");

}

function get_sales_ref_no_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_so_ref_no_report($order_no, $type)
{
	$sql = "SELECT so_form_type_no, from_stk_loc
			FROM ".TB_PREF."sales_orders 
			WHERE order_no = ".db_escape($order_no)."
			AND trans_type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_salesman_name_report($branch_code)
{
	$sql = "SELECT salesman_name FROM ".TB_PREF."salesman, ".TB_PREF."cust_branch
			WHERE ".TB_PREF."salesman.salesman_code = ".TB_PREF."cust_branch.salesman
			AND ".TB_PREF."cust_branch.branch_code=".db_escape($branch_code);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

//----------------------------------------------------------------------------------------------------

function print_sales_report()
{
    global $path_to_root, $systypes_array;

    $from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $location = $_POST['PARAM_2'];
    $comments = $_POST['PARAM_3'];
	$destination = $_POST['PARAM_4'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$cols = array(0, 90, 150, 350, 480, 620);

	$headers = array(_('DR #'), _('Date'), _("Customer Name"), _('Salesperson'), _('Amount'));

	$aligns = array('left',	'left',	'left',	'left',	'left');
	
	if ($location == ALL_TEXT)
		$location = 'all';
	if ($location == 'all')
		$loc = _('All');
	else
		$loc = get_location_name($location);
		
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to),
    				    2 => array('text' => _('Location'), 'from' => $loc, 'to' => ''));

    $rep = new FrontReport(_('Delivery Receipt Report'), "DeliveryReceiptReport", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$sql = "SELECT DISTINCT loc.loc_code, loc.location_name 
			FROM ".TB_PREF."locations loc, ".TB_PREF."debtor_trans trans
			WHERE loc.loc_code = trans.location
			AND trans.type = 13
			AND trans.tran_date >= '".date2sql($from)."'
			AND trans.tran_date <= '".date2sql($to)."'";
	if ($location != 'all')
		$sql .= "AND loc.loc_code=".db_escape($location);
	$sql .= " ORDER BY loc.location_name";
	$result = db_query($sql, "The location could not be retrieved");

	$loc_ = '';
	$grand_total = 0;
	while ($myrow = db_fetch($result))
	{
		if ($loc_ != $myrow['location_name'])
		{
			$rep->NewLine();
			$rep->fontSize += 2;
			$rep->TextCol(0, 4, $myrow['location_name']);
			$rep->fontSize -= 2;
			$rep->NewLine();
		}
		
		$total = 0;
		$res = getTransactions($myrow['loc_code'], $from, $to);
		while ($row = db_fetch($res))
		{
			$ref_no = get_sales_ref_no_report($row["trans_no"], $row["type"]);
			
			$rep->TextCol(0, 1, $ref_no["location"]."-".$ref_no["form_type_no"]);
			$rep->TextCol(1, 2, $row['tran_date']);
			$rep->TextCol(2, 3, get_customer_name($row['debtor_no']));
			$rep->TextCol(3, 4, get_salesman_name_report($row['branch_code']));
			$rep->TextCol(4, 5, $row['TotalAmount'] != 0 ? number_format2($row['TotalAmount'],2) : "");
			$rep->NewLine();
			
			$total += $row['TotalAmount'];
		}
		
		$rep->Font('bold');	
		$rep->Line($rep->row + 6);
		$rep->row -= 6;
		$rep->TextCol(0, 2,	_('Total '));
		$rep->TextCol(4, 5, number_format2($total,2));
		$rep->NewLine();
		$rep->Font('');	
		
		$grand_total += $total;
	}
	
	$rep->Font('bold');	
	$rep->NewLine();
	$rep->Line($rep->row + 6);
	$rep->row -= 6;
	$rep->TextCol(0, 4,	_('GrandTotal '));
	$rep->TextCol(4, 5, number_format2($grand_total,2));
	$rep->NewLine();
	$rep->Font('');	
	
    $rep->End();
}

?>