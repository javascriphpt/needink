<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPPAYMREP';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Payment Report
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

print_payables_report();

function getTransactions($supplier, $from, $to)
{
	$from = date2sql($from);
	$to = date2sql($to);
	$dec = user_price_dec();

	$sql = "SELECT ".TB_PREF."supp_trans.supp_reference,
			".TB_PREF."supp_trans.tran_date,
			".TB_PREF."supp_trans.due_date,
			".TB_PREF."supp_trans.trans_no,
			".TB_PREF."supp_trans.reference,
			".TB_PREF."supp_trans.type,
			(ABS(".TB_PREF."supp_trans.ov_amount) + ABS(".TB_PREF."supp_trans.ov_gst) + ABS(".TB_PREF."supp_trans.ewt) - ".TB_PREF."supp_trans.alloc) AS Balance,
			(ABS(".TB_PREF."supp_trans.ov_amount) + ABS(".TB_PREF."supp_trans.ov_gst) + ABS(".TB_PREF."supp_trans.ewt) ) AS TranTotal
		FROM ".TB_PREF."supp_trans
		WHERE ".TB_PREF."supp_trans.supplier_id = '" . $supplier . "'
		AND ROUND(ABS(".TB_PREF."supp_trans.ov_amount),$dec) + ROUND(ABS(".TB_PREF."supp_trans.ov_gst),$dec) + ROUND(ABS(".TB_PREF."supp_trans.ewt),$dec) - 
		ROUND(".TB_PREF."supp_trans.alloc,$dec) != 0
		AND ".TB_PREF."supp_trans.tran_date >= '$from' 
		AND ".TB_PREF."supp_trans.tran_date <= '$to' 
		AND (".TB_PREF."supp_trans.type = ".ST_SUPPINVOICE." OR ".TB_PREF."supp_trans.type = ".ST_BANKDEPOSIT.")

		ORDER BY ".TB_PREF."supp_trans.type,
			".TB_PREF."supp_trans.trans_no";

    return db_query($sql, "No transactions were returned");
}

//----------------------------------------------------------------------------------------------------

function print_payables_report()
{
    global $path_to_root, $systypes_array;

    $from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $fromsupp = $_POST['PARAM_2'];
    $comments = $_POST['PARAM_3'];
	$destination = $_POST['PARAM_4'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	if ($fromsupp == ALL_NUMERIC)
		$supp = _('All');
	else
		$supp = get_supplier_name($fromsupp);

    $dec = user_price_dec();

	$cols = array(0, 90, 140, 250, 290, 320, 385, 450,	515);

	$headers = array(_('Trans Type'), _('#'), _("Supplier's Ref"), _('Date'), '', '',
		'Amount', _(''), _(' '));

	$aligns = array('left',	'left',	'left',	'left',	'right', 'right', 'right', 'right');

    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to),
    				    2 => array('text' => _('Supplier'), 'from' => $supp, 'to' => ''));

    $rep = new FrontReport(_('Payables Report'), "PayablesReport", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$total = array();
	$grandtotal = array(0,0);

	$sql = "SELECT supplier_id, supp_name AS name, curr_code, ".TB_PREF."payment_terms.terms FROM ".TB_PREF."suppliers, ".TB_PREF."payment_terms
		WHERE ";
	if ($fromsupp != ALL_NUMERIC)
		$sql .= "supplier_id=".db_escape($fromsupp)." AND ";
	$sql .= "".TB_PREF."suppliers.payment_terms = ".TB_PREF."payment_terms.terms_indicator
		ORDER BY supp_name";
	$result = db_query($sql, "The customers could not be retrieved");

	$grandtotal = 0;
	while ($myrow=db_fetch($result))
	{
		$rep->fontSize += 2;
		$rep->TextCol(0, 7, $myrow['name'] . " - " . $myrow['terms'] . " - " . $myrow['curr_code']);
		$rep->fontSize -= 2;
		$rep->NewLine(1, 2);
		
		$res = getTransactions($myrow['supplier_id'], $from, $to);
		if (db_num_rows($res)==0)
			continue;
		$rep->Line($rep->row + 4);
		
		$total = 0;
		while ($trans=db_fetch($res))
		{
			
			$rep->NewLine(1, 2);
			$rep->TextCol(0, 1, $systypes_array[$trans['type']]);
			$rep->TextCol(1, 2,	$trans['reference']);
			$rep->TextCol(2, 3,	$trans['supp_reference']);
			$rep->DateCol(3, 4,	$trans['tran_date'], true);
			$rep->AmountCol(6, 7, $trans['TranTotal'], $dec);
			$total += $trans['TranTotal'];
			
		}
		$rep->Line($rep->row - 8);
		$rep->NewLine(2);
		$rep->TextCol(0, 3,	_('Total'));
		$rep->AmountCol(6, 7, $total, $dec);
    	$rep->Line($rep->row  - 4);
    	$rep->NewLine(2);
		
		$grandtotal += $total;
	}
	
	$rep->fontSize += 2;
	$rep->TextCol(0, 3,	_('Grand Total'));
	$rep->fontSize -= 2;
	$rep->AmountCol(6, 7, $grandtotal, $dec);
	$rep->Line($rep->row  - 4);
	$rep->NewLine();
	
    $rep->End();
}

?>