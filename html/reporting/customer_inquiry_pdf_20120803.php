<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SALESTRANSVIEW';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/includes/ui/ui_lists.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_customer_balances();

function get_open_balance($debtorno, $to, $convert)
{
	$to = date2sql($to);

    $sql = "SELECT SUM(IF(".TB_PREF."debtor_trans.type = ".ST_SALESINVOICE.", (".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + 
    	".TB_PREF."debtor_trans.ov_freight + ".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt)";
    if ($convert)
    	$sql .= " * rate";
    $sql .= ", 0)) AS charges,
    	SUM(IF(".TB_PREF."debtor_trans.type <> ".ST_SALESINVOICE.", (".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + 
    	".TB_PREF."debtor_trans.ov_freight + ".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt)";
    if ($convert)
    	$sql .= " * rate";
    $sql .= " * -1, 0)) AS credits,
		SUM(".TB_PREF."debtor_trans.alloc";
	if ($convert)
		$sql .= " * rate";
	$sql .= ") AS Allocated,
		SUM(IF(".TB_PREF."debtor_trans.type = ".ST_SALESINVOICE.", (".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + 
    	".TB_PREF."debtor_trans.ov_freight + ".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt - ".TB_PREF."debtor_trans.alloc)";
    if ($convert)
    	$sql .= " * rate";
    $sql .= ", 
    	((".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + 
    	".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt) * -1 + ".TB_PREF."debtor_trans.alloc)";
    if ($convert)
    	$sql .= " * rate";
    $sql .= ")) AS OutStanding
		FROM ".TB_PREF."debtor_trans
    	WHERE ".TB_PREF."debtor_trans.tran_date < '$to'
		AND ".TB_PREF."debtor_trans.debtor_no = ".db_escape($debtorno)."
		AND ".TB_PREF."debtor_trans.type <> ".ST_CUSTDELIVERY." GROUP BY debtor_no";

    $result = db_query($sql,"No transactions were returned");
    return db_fetch($result);
}

function get_transactions($debtorno, $from, $to)
{
	$from = date2sql($from);
	$to = date2sql($to);

    $sql = "SELECT ".TB_PREF."debtor_trans.*,
		(".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + 
		".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt)
		AS TotalAmount, ".TB_PREF."debtor_trans.alloc AS Allocated,
		((".TB_PREF."debtor_trans.type = ".ST_SALESINVOICE.")
		AND ".TB_PREF."debtor_trans.due_date < '$to') AS OverDue
    	FROM ".TB_PREF."debtor_trans
    	WHERE ".TB_PREF."debtor_trans.tran_date >= '$from'
		AND ".TB_PREF."debtor_trans.tran_date <= '$to'
		AND ".TB_PREF."debtor_trans.debtor_no = ".db_escape($debtorno)."
		AND ".TB_PREF."debtor_trans.type <> ".ST_CUSTDELIVERY."
    	ORDER BY ".TB_PREF."debtor_trans.tran_date";

    return db_query($sql,"No transactions were returned");
}

function get_sales_ref_no_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_sales_ref_no_OR_PR_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location, form_type
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_so_form_cat_name_report($cat_id)
{
	$sql = "SELECT name FROM ".TB_PREF."sales_form_category 
			WHERE id=".db_escape($cat_id);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

function get_cust_po($order_no)
{
	$sql = "SELECT customer_ref FROM ".TB_PREF."sales_orders 
		WHERE trans_type = ".ST_SALESORDER."
		AND order_no = ".db_escape($order_no);

	$result = db_query($sql,"could not retreive customer po #");

	$myrow = db_fetch_row($result);	
	return $myrow[0];
}

function dr_view($trans_no)
{
	$x=1;
	$dr_text = "";
	foreach(get_parent_trans(ST_SALESINVOICE,$trans_no) as $id=>$val)
	{	
		$ref_no = get_sales_ref_no($val, ST_CUSTDELIVERY);
		if($x == 1)
			$dr_text .= $ref_no["location"]."-".$ref_no["form_type_no"];
		else
			$dr_text .= ", ".$ref_no["location"]."-".$ref_no["form_type_no"];
			
		$x++;
	}

	return $dr_text;
}

//----------------------------------------------------------------------------------------------------

function print_customer_balances()
{
    global $path_to_root, $systypes_array;
	
	$customer = $_GET['customer'];
	$from = $_GET['from'];
	$to = $_GET['to'];
	$type = $_GET['type'];
	$output = $_GET['output'];
		
	if ($output == 0)
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/excel_report.inc");

	if ($customer == '')
		$cust = _('All');
	else
		$cust = get_customer_name($customer);
    $dec = user_price_dec();

	$cols = array(0, 70, 140, 210, 270, 340, 385, 435, 550, 655, 705, 750);

	$headers = array(_('Type'), _('#'), _('Order'), _('Customer PO #'), _('DR #'), _('Date'), _('Due Date'), _('Customer'), _('Branch'), _('Debit'), _('Credit'));

	$aligns = array('left',	'left',	'left',	'left',	'left', 'left',	'left', 'left', 'left', 'left', 'left');

    $params =   array( 	0 => '',
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to),
    				    2 => array('text' => _('Customer'), 'from' => $cust, 'to' => ''));

    $rep = new FrontReport(_('Customer Inquiry'), "CustomerInquriy", user_pagesize(), 7, 'L');

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	
	$sql = "SELECT 
  		trans.type, 
		trans.trans_no, 
		trans.order_, 
		trans.tran_date, 
		trans.due_date, 
		debtor.name, 
		branch.br_name,
		debtor.curr_code,
		(trans.ov_amount + trans.ov_gst + trans.ov_freight 
			+ trans.ov_freight_tax + trans.ov_discount + trans.ewt)	AS TotalAmount, "; 
   	if ($type != 0)
		$sql .= "@bal := @bal+(trans.ov_amount + trans.ov_gst + trans.ov_freight + trans.ov_freight_tax + trans.ov_discount + trans.ewt), ";

		$sql .= "trans.alloc AS Allocated,
		((trans.type = ".ST_SALESINVOICE.")
			AND trans.due_date < '" . date2sql(Today()) . "') AS OverDue, trans.reference, trans.form_type
		FROM "
			.TB_PREF."debtor_trans as trans, "
			.TB_PREF."debtors_master as debtor, "
			.TB_PREF."cust_branch as branch
		WHERE debtor.debtor_no = trans.debtor_no
			AND trans.tran_date >= '$from'
			AND trans.tran_date <= '$to'
			AND trans.branch_code = branch.branch_code";
			
	if ($customer != 0)
   		$sql .= " AND trans.debtor_no = ".db_escape($customer);

   	if ($type != 0)
   	{
   		if ($type == '1')
   		{
   			$sql .= " AND (trans.type = ".ST_SALESINVOICE." OR trans.type = ".ST_BANKPAYMENT.") ";
   		}
   		elseif ($type == '2')
   		{
   			$sql .= " AND (trans.type = ".ST_SALESINVOICE.") ";
   		}
   		elseif ($type == '3')
   		{
			$sql .= " AND (trans.type = " . ST_CUSTPAYMENT 
					." OR trans.type = ".ST_BANKDEPOSIT.") ";
   		}
   		elseif ($type == '4')
   		{
			$sql .= " AND trans.type = ".ST_CUSTCREDIT." ";
   		}
   		elseif ($type == '5')
   		{
			$sql .= " AND trans.type = ".ST_CUSTDELIVERY." ";
   		}

    	if ($type == '2')
    	{
    		$today =  date2sql(Today());
    		$sql .= " AND trans.due_date < '$today'
				AND (trans.ov_amount + trans.ov_gst + trans.ov_freight_tax + 
				trans.ov_freight + trans.ov_discount + trans.ewt - trans.alloc > 0) ";
    	}
   	}
	
	$result = db_query($sql, "Transactions could not be retrieved");

	while ($myrow = db_fetch($result))
	{
		$ref_no = get_sales_ref_no($myrow["trans_no"], $myrow["type"]);
		$so_no = get_so_ref_no($myrow["order_"], ST_SALESORDER);
		
		$rep->TextCol(0, 1, $systypes_array[$myrow["type"]]);
		if($myrow["type"] != 12)
			$rep->TextCol(1, 2, $ref_no["location"]."-".$ref_no["form_type_no"]);
		else
			$rep->TextCol(1, 2, $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($myrow["form_type"]));
			
		$rep->TextCol(2, 3, $myrow["type"] != 12 ? $so_no["from_stk_loc"]."-".$so_no["so_form_type_no"] : "");
		
		if($myrow["type"] == 10)
		{
			$rep->TextCol(3, 4, get_cust_po($myrow["order_"]));
			$rep->TextCol(4, 5, dr_view($myrow["trans_no"]));
		}
		else
		{
			$rep->TextCol(3, 4, "");
			$rep->TextCol(4, 5, "");
		}	
		$rep->TextCol(5, 6, sql2date($myrow["tran_date"]));
		$rep->TextCol(6, 7, $myrow["type"] == ST_SALESINVOICE ? sql2date($myrow["due_date"]) : "");
		$rep->TextCol(7, 8, $myrow["name"]);
		$rep->TextCol(8, 9, $myrow["br_name"]);
		
		if($myrow["type"] == 13 || $myrow["type"] == 10) {
			$rep->AmountCol(9, 10, $myrow["TotalAmount"], $dec);
			$rep->TextCol(10, 11, "");
		} else {
			$rep->TextCol(9, 10, "");
			$rep->AmountCol(10, 11, $myrow["TotalAmount"], $dec);
		}
			
		$rep->NewLine();
	}

    $rep->End();
}

?>