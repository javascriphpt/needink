<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPINQUIRY';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/includes/ui/ui_lists.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();

$output=print_sup_inquiry();

header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=SupportProcessInquiry.xls");
header("Pragma: no-cache");
header("Expires: 0");	

echo $output;

//----------------------------------------------------------------------------------------------------

function print_sup_inquiry()
{
    global $path_to_root, $systypes_array;
	
	$sup_no = $_GET['sup_no'];
	$company_name = $_GET['company_name'];
	$system_unit = $_GET['system_unit'];
	$serial_no = $_GET['serial_no'];
	$assigned_tsg = $_GET['assigned_tsg'];
	$conformed = $_GET['conformed'];
	$from = $_GET['from'];
	$to = $_GET['to'];
	$type = $_GET['type'];
	$output = $_GET['output'];
		
	if ($output == 0)
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/excel_report.inc");

	$dec = user_price_dec();

	$cols = array(0, 90, 155, 215, 270, 340, 460, 580, 620, 675, 735);

	$headers = array(_('SUP No.'), _('Company Name'), _('System / Unit'), _('Serial No.'), _('Assigned TSG'), _('Conformed'), _('Compledted Date'));

	$aligns = array('left',	'left',	'left',	'left',	'left', 'left', 'left', 'left', 'right', 'right');

    $params =   array( 	0 => '',
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to));

    $rep = new FrontReport(_('Support Process Inquiry'), "SupportProcessInquiry", user_pagesize(), 9, 'L');

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	
	$sql = "SELECT sup.sup_no,
				sup.company_name,
				sup.system_unit,
				sup.serial_no,
				sup.assigned_tsg,
				sup.conformed, 
				completed_date, sup.id
			FROM ".TB_PREF."sup as sup, ".TB_PREF."sup_type as type
			WHERE sup.type = type.id 
			AND sup.type = ".db_escape($type)."
			AND sup.completed_date >= ".db_escape($from)."
			AND sup.completed_date <= ".db_escape($to)." ";

	if ($sup_no != '')
		$sql .= " AND sup.sup_no LIKE ".db_escape("%".$sup_no."%");
		
	if ($company_name != '')
		$sql .= " AND sup.company_name LIKE ".db_escape("%".$company_name."%");
		
	if ($system_unit != '')
		$sql .= " AND sup.system_unit LIKE ".db_escape("%".$system_unit."%");
		
	if ($serial_no != '')
		$sql .= " AND sup.serial_no LIKE ".db_escape("%".$serial_no."%");
		
	if ($assigned_tsg != '')
		$sql .= " AND sup.assigned_tsg LIKE ".db_escape("%".$assigned_tsg."%");
		
	if ($conformed != '')
		$sql .= " AND sup.conformed LIKE ".db_escape("%".$conformed."%");
		
	$sql .= " ORDER BY sup.sup_no ASC, sup.company_name";
	
	$result = db_query($sql, "Transactions could not be retrieved");
	
	ob_start();
		?>
			<html>
				<head>
					<style>
						.title{
							font-weight:bold;
							font-size:18px;
						}
						td{
							border:solid thin black;
						}
						.total{
							font-weight:bold;
						}
						.header{
							font-style:italic;
							font-weight:bold;
						}
					</style>
				</head>
				<body>
		<?php
		
				echo '<span class="title">Support Process Inquiry</span>';
				echo '<br>';
				echo '<br>';
					$headers = array('SUP No.', 'Company Name', 'System / Unit', 'Serial No.', 'Assigned TSG', 'Conformed', 'Completed Date');
					
				start_table();
				start_row();
				foreach($headers as $value){
					echo "<td class='header'>".$value."</td>";
				}
				end_row();

				while ($myrow = db_fetch($result))
				{
									
					start_row();
					echo "<td>".$myrow["sup_no"]."</td>";					
					echo "<td>".$myrow["company_name"]."</td>";					
					echo "<td>".$myrow["system_unit"]."</td>";					
					echo "<td>".$myrow["serial_no"]."</td>";					
					echo "<td>".$myrow["assigned_tsg"]."</td>";					
					echo "<td>".$myrow["conformed"]."</td>";					
					echo "<td>".sql2date($myrow["completed_date"])."</td>";					
					
				}
											
			return ob_get_clean();
}

?>