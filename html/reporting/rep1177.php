<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Aged Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

//print_aged_customer_analysis();
$output=print_aged_customer_analysis();

header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=AgedCustomerAnalysis.xls");
header("Pragma: no-cache");
header("Expires: 0");	

echo $output;

function get_invoices($customer_id, $to, $branch_code, $location)
{
	$todate = date2sql($to);
	$PastDueDays1 = get_company_pref('past_due_days');
	$PastDueDays2 = 2 * $PastDueDays1;

	// Revomed allocated from sql
    $value = "(".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + "
		.TB_PREF."debtor_trans.ov_freight + ".TB_PREF."debtor_trans.ov_freight_tax + "
		.TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt)";
	$due = "IF (".TB_PREF."debtor_trans.type=".ST_SALESINVOICE.",".TB_PREF."debtor_trans.due_date,".TB_PREF."debtor_trans.tran_date)";
	$sql = "SELECT ".TB_PREF."debtor_trans.type, ".TB_PREF."debtor_trans.reference,
		".TB_PREF."debtor_trans.tran_date, ".TB_PREF."debtor_trans.trans_no,
		$value as Balance, ".TB_PREF."debtor_trans.trans_link,
		IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= 0,$value,0) AS Due,
		IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= $PastDueDays1,$value,0) AS Overdue1,
		IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= $PastDueDays2,$value,0) AS Overdue2

		FROM ".TB_PREF."debtors_master,
			".TB_PREF."payment_terms,
			".TB_PREF."debtor_trans

		WHERE ".TB_PREF."debtors_master.payment_terms = ".TB_PREF."payment_terms.terms_indicator
			AND ".TB_PREF."debtors_master.debtor_no = ".TB_PREF."debtor_trans.debtor_no
			AND ".TB_PREF."debtor_trans.debtor_no = $customer_id 
			AND ".TB_PREF."debtor_trans.location = ".db_escape($location)."
			AND ".TB_PREF."debtor_trans.tran_date <= '$todate'
			AND ".TB_PREF."debtor_trans.type <> 13
			AND ABS(".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + ".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt) > 0.004 ";

	
	if ($branch_code != '')
		$sql .= " AND ".TB_PREF."debtor_trans.branch_code = ".db_escape($branch_code)." ";
		
    $sql .= " ORDER BY ".TB_PREF."debtor_trans.tran_date";

	return db_query($sql, "The customer details could not be retrieved");
}

function get_customer_details_report($customer_id, $to=null, $branch_code,  $location)
{

	if ($to == null)
		$todate = date("Y-m-d");
	else
		$todate = date2sql($to);
	$past1 = get_company_pref('past_due_days');
	$past2 = 2 * $past1;
	// removed - debtor_trans.alloc from all summations

    $value = "IF(".TB_PREF."debtor_trans.type=11 OR ".TB_PREF."debtor_trans.type=12 OR ".TB_PREF."debtor_trans.type=2,
	-1, 1) *".
      "(".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + "
		.TB_PREF."debtor_trans.ov_freight + ".TB_PREF."debtor_trans.ov_freight_tax + "
		.TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt)";
	$due = "IF (".TB_PREF."debtor_trans.type=10,".TB_PREF."debtor_trans.due_date,".TB_PREF."debtor_trans.tran_date)";
    $sql = "SELECT ".TB_PREF."debtors_master.name, ".TB_PREF."debtors_master.curr_code, ".TB_PREF."payment_terms.terms,
		".TB_PREF."debtors_master.credit_limit, ".TB_PREF."credit_status.dissallow_invoices, ".TB_PREF."credit_status.reason_description,

		Sum(".$value.") AS Balance, ".TB_PREF."debtor_trans.trans_link,
		
		SUM((".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + "
		.TB_PREF."debtor_trans.ov_freight + ".TB_PREF."debtor_trans.ov_freight_tax + "
		.TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt)) as total_amount,

		Sum(IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= 0,$value,0)) AS Due,
		Sum(IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= $past1,$value,0)) AS Overdue1,
		Sum(IF ((TO_DAYS('$todate') - TO_DAYS($due)) >= $past2,$value,0)) AS Overdue2

		FROM ".TB_PREF."debtors_master,
			 ".TB_PREF."payment_terms,
			 ".TB_PREF."credit_status,
			 ".TB_PREF."debtor_trans

		WHERE
			 ".TB_PREF."debtors_master.payment_terms = ".TB_PREF."payment_terms.terms_indicator
			 AND ".TB_PREF."debtors_master.credit_status = ".TB_PREF."credit_status.id
			 AND ".TB_PREF."debtors_master.debtor_no = ".db_escape($customer_id)."
			 AND ".TB_PREF."debtor_trans.location = ".db_escape($location)."
			 AND ".TB_PREF."debtor_trans.tran_date <= '$todate'
			 AND ".TB_PREF."debtor_trans.type <> 13
			 AND ".TB_PREF."debtors_master.debtor_no = ".TB_PREF."debtor_trans.debtor_no  AND ( (".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + 
		".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt) - ".TB_PREF."debtor_trans.alloc) > 0 ";
		
	if ($branch_code != '')
		$sql .= " AND ".TB_PREF."debtor_trans.branch_code = ".db_escape($branch_code)." ";

	$sql .= " GROUP BY
			  ".TB_PREF."debtors_master.name,
			  ".TB_PREF."payment_terms.terms,
			  ".TB_PREF."payment_terms.days_before_due,
			  ".TB_PREF."payment_terms.day_in_following_month,
			  ".TB_PREF."debtors_master.credit_limit,
			  ".TB_PREF."credit_status.dissallow_invoices,
			  ".TB_PREF."credit_status.reason_description";
    $result = db_query($sql,"The customer details could not be retrieved");

    if (db_num_rows($result) == 0)
    {

    	/*Because there is no balance - so just retrieve the header information about the customer - the choice is do one query to get the balance and transactions for those customers who have a balance and two queries for those who don't have a balance OR always do two queries - I opted for the former */

    	$nil_balance = true;

    	$sql = "SELECT ".TB_PREF."debtors_master.name, ".TB_PREF."debtors_master.curr_code, ".TB_PREF."debtors_master.debtor_no,  ".TB_PREF."payment_terms.terms,
    		".TB_PREF."debtors_master.credit_limit, ".TB_PREF."credit_status.dissallow_invoices, ".TB_PREF."credit_status.reason_description
    		FROM ".TB_PREF."debtors_master,
    		     ".TB_PREF."payment_terms,
    		     ".TB_PREF."credit_status

    		WHERE
    		     ".TB_PREF."debtors_master.payment_terms = ".TB_PREF."payment_terms.terms_indicator
    		     AND ".TB_PREF."debtors_master.credit_status = ".TB_PREF."credit_status.id
    		     AND ".TB_PREF."debtors_master.debtor_no = ".db_escape($customer_id);

    	$result = db_query($sql,"The customer details could not be retrieved");

    }
    else
    {
    	$nil_balance = false;
    }

    $customer_record = db_fetch($result);

    if ($nil_balance == true)
    {
		// if($customer_record['type'] == 13 && $customer_record['trans_link'] != 0)
			// continue;
			
    	$customer_record["Balance"] = 0;
    	$customer_record["Due"] = 0;
    	$customer_record["Overdue1"] = 0;
    	$customer_record["Overdue2"] = 0;
    }

    return $customer_record;

}

function get_sales_ref_no_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_sales_ref_no_OR_PR_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location, form_type
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_so_form_cat_name_report($cat_id)
{
	$sql = "SELECT name FROM ".TB_PREF."sales_form_category 
			WHERE id=".db_escape($cat_id);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

//----------------------------------------------------------------------------------------------------

function print_aged_customer_analysis()
{
    global $comp_path, $path_to_root, $systypes_array;

    $to = $_GET['PARAM_0'];
    $fromcust = $_GET['PARAM_1'];
	$branch = $_GET['PARAM_2'];
    $currency = $_GET['PARAM_3'];
	$summaryOnly = $_GET['PARAM_4'];
	$destination = $_GET['PARAM_5'];
	//$paid_unpaid = $_GET['PARAM_6'];
	$location = $_GET['PARAM_7'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");
	
	if ($fromcust == '')
		$from = _('All');
	else
		$from = get_customer_name($fromcust);
    $dec = user_price_dec();
	
	if ($location == '')
		$loc = _('All');
	else
		$loc = get_location_name($location);

	if ($summaryOnly == 1)
		$summary = _('Summary Only');
	else
		$summary = _('Detailed Report');
	if ($currency == '')
	{
		$convert = true;
		$currency = _('Balances in Home Currency');
	}
	else
		$convert = false;

	$PastDueDays1 = get_company_pref('past_due_days');
	$PastDueDays2 = 2 * $PastDueDays1;
	$nowdue = "1-" . $PastDueDays1 . " " . _('Days');
	$pastdue1 = $PastDueDays1 + 1 . "-" . $PastDueDays2 . " " . _('Days');
	$pastdue2 = _('Over') . " " . $PastDueDays2 . " " . _('Days');

	$cols = array(0, 90, 155, 215,	270, 340, 405, 470,	535);
	$headers = array(_('Customer'),	'',	'',	_('Current'), $nowdue, $pastdue1, $pastdue2,
		_('Total Balance'));

	$aligns = array('left',	'left',	'left',	'right', 'right', 'right', 'right',	'right');

    $params =   array( 	0 => $comments,
    					1 => array('text' => _('End Date'), 'from' => $to, 'to' => ''),
    				    2 => array('text' => _('Customer'),	'from' => $from, 'to' => ''),
    				    3 => array('text' => _('Currency'), 'from' => $currency, 'to' => ''),
                    	4 => array('text' => _('Type'),		'from' => $summary,'to' => ''),
						5 => array('text' => _('Location'), 'from' => $loc, 'to' => ''));

	if ($convert)
		$headers[2] = _('Currency');
    $rep = new FrontReport(_('Aged Customer Analysis'), "AgedCustomerAnalysis", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	ob_start();
	?>
		<html>
			<head>
				<style>
					.title{
						font-weight:bold;
						font-size:18px;
					}
					td{
						border:solid thin black;
					}
					.total{
						font-weight:bold;
					}
					.header{
						font-style:italic;
						font-weight:bold;
					}
				</style>
			</head>
			<body>
	<?php
	
	echo '<span class="title">Aged Customer Analysis</span>';
	echo '<br>';
	echo '<br>';
		$headers = array(_('Customer'),	'',	'',	_('Current'), $nowdue, $pastdue1, $pastdue2, _('Total Balance'));
		
	$sql3 = "SELECT DISTINCT loc.loc_code, loc.location_name
			FROM ".TB_PREF."locations loc, ".TB_PREF."debtor_trans trans
			WHERE loc.loc_code = trans.location
			AND trans.type = type <> ".ST_CUSTDELIVERY."
			AND trans.tran_date >= '".date2sql($from)."'
			AND trans.tran_date <= '".date2sql($to)."'";
	if ($location != '')
		$sql3 .= "AND loc.loc_code=".db_escape($location);
	if ($fromcust != '')
		$sql3 .= " AND trans.debtor_no=".db_escape($fromcust);
	$sql3 .= " ORDER BY loc.location_name";
	
	$result3 = db_query($sql3, "The location could not be retrieved");
	
	start_table();
	
	$loc_ = '';
	$total = array(0,0,0,0, 0);
	
	while ($myrow3 = db_fetch($result3))
	{
		if ($loc_ != $myrow3['location_name'])
		{
			start_row();
			echo "<td class='header' colspan=8>".$myrow3['location_name']."</td>";
			end_row();
		}
		
		$sql = "SELECT DISTINCT ".TB_PREF."debtors_master.debtor_no, ".TB_PREF."debtors_master.name, ".TB_PREF."debtors_master.curr_code 
				FROM ".TB_PREF."debtors_master, ".TB_PREF."debtor_trans
				WHERE ".TB_PREF."debtors_master.debtor_no = ".TB_PREF."debtor_trans.debtor_no
				AND ".TB_PREF."debtor_trans.tran_date >= '".date2sql($from)."'
				AND ".TB_PREF."debtor_trans.tran_date <= '".date2sql($to)."'
				AND ".TB_PREF."debtor_trans.location=".db_escape($myrow3['loc_code']);
		if ($fromcust != '')
			$sql .= " AND ".TB_PREF."debtor_trans.debtor_no=".db_escape($fromcust);
		$sql .= " ORDER BY ".TB_PREF."debtors_master.name";
		$result = db_query($sql, "The customers could not be retrieved");
		
		while ($myrow=db_fetch($result))
		{
			if (!$convert && $currency != $myrow['curr_code'])
				continue;
				
			start_row();
			foreach($headers as $value){
				echo "<td class='header'>".$value."</td>";
			}
			end_row();
				
			start_row();
			
			echo "<td colspan=2>".$myrow['name']."</td>";		
			if ($convert)
			{
				$rate = get_exchange_rate_from_home_currency($myrow['curr_code'], $to);
				echo "<td>".$myrow['curr_code']."</td>";	
			}
			else
			{
				$rate = 1.0;
				echo "<td>&nbsp;</td>";
			}
			
			$custrec = get_customer_details_report($myrow['debtor_no'], $to, $branch, $myrow3['loc_code']);
			foreach ($custrec as $i => $value)
				$custrec[$i] *= $rate;
			
			// if($custrec["type"] == 13 && $custrec["trans_link"] != 0)
			// {
				// $total[0] += ($custrec["Balance"] - $custrec["Due"]-$custrec["total_amount"]);
				// $total[1] += ($custrec["Due"]-$custrec["Overdue1"]-$custrec["total_amount"]);
				// $total[2] += ($custrec["Overdue1"]-$custrec["Overdue2"]-$custrec["total_amount"]);
				// $total[3] += $custrec["Overdue2"]-$custrec["total_amount"];
				// $total[4] += $custrec["Balance"]-$custrec["total_amount"];
			// }
			// else
			// {
				$total[0] += ($custrec["Balance"] - $custrec["Due"]);
				$total[1] += ($custrec["Due"]-$custrec["Overdue1"]);
				$total[2] += ($custrec["Overdue1"]-$custrec["Overdue2"]);
				$total[3] += $custrec["Overdue2"];
				$total[4] += $custrec["Balance"];
			//}
			$str = array($custrec["Balance"] - $custrec["Due"],
				$custrec["Due"]-$custrec["Overdue1"],
				$custrec["Overdue1"]-$custrec["Overdue2"],
				$custrec["Overdue2"],
				$custrec["Balance"]);
			for ($i = 0; $i < count($str); $i++)
			{
				// if($custrec["type"] == 13 && $custrec["trans_link"] != 0)
					// echo "<td>".$str[$i]-$custrec["total_amount"]."</td>";	
				// else
					echo "<td>".$str[$i]."</td>";	
			}
			
			end_row();
			
			if (!$summaryOnly)
			{
				$res = get_invoices($myrow['debtor_no'], $to, $branch, $myrow3['loc_code']);
				if (db_num_rows($res)==0)
					continue;
			
				while ($trans=db_fetch($res))
				{
					// if($trans['type'] == 13 && $trans['trans_link'] != 0)
						// continue;
						
					start_row();
					
					if($trans['type'] != 12)
					{
						$ref_no = get_sales_ref_no_report($trans['trans_no'], $trans['type']);
						$ref = $ref_no["location"]."-".$ref_no["form_type_no"];
					}
					else
					{
						$ref_no = get_sales_ref_no_OR_PR_report($trans['trans_no'], $trans['type']);
						$ref = $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name_report($ref_no["form_type"]);
					}
				
					
					echo "<td>".$systypes_array[$trans['type']]."</td>";	
					echo "<td>".$ref."</td>";	
					echo "<td>".$trans['tran_date']."</td>";	
					if ($trans['type'] == ST_CUSTCREDIT || $trans['type'] == ST_CUSTPAYMENT || $trans['type'] == ST_BANKDEPOSIT)
					{
						$trans['Balance'] *= -1;
						$trans['Due'] *= -1;
						$trans['Overdue1'] *= -1;
						$trans['Overdue2'] *= -1;
					}
					foreach ($trans as $i => $value)
						$trans[$i] *= $rate;
					$str = array($trans["Balance"] - $trans["Due"],
						$trans["Due"]-$trans["Overdue1"],
						$trans["Overdue1"]-$trans["Overdue2"],
						$trans["Overdue2"],
						$trans["Balance"]);
					for ($i = 0; $i < count($str); $i++)
						echo "<td>".$str[$i]."</td>";	
						
					end_row();
				}
				start_row(); echo "<td class='header' colspan=8>&nbsp;</td>"; end_row();
			}
			
		}
		
	}
	
	if ($summaryOnly)
	{
    	start_row(); echo "<td class='header' colspan=8>&nbsp;</td>"; end_row();
	}
	
	echo "<td colspan=3>"._('Grand Total')."</td>";	
	
	for ($i = 0; $i < count($total); $i++)
	{
		echo "<td>".$total[$i]."</td>";	
	}
   	start_row(); echo "<td class='header' colspan=8>&nbsp;</td>"; end_row();
		
	return ob_get_clean();
}

?>