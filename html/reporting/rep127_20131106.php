<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

$page_security = $_POST['PARAM_0'] == $_POST['PARAM_1'] ?
	'SA_SALESTRANSVIEW' : 'SA_SALESBULKREP';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Purchase Orders
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/includes/ui/ui_view.inc");

//----------------------------------------------------------------------------------------------------

print_receipts();

//----------------------------------------------------------------------------------------------------
function get_receipt($type, $trans_no)
{
    $sql = "SELECT ".TB_PREF."debtor_trans.*,
				(".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + 
				".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt) AS Total, 
   				".TB_PREF."debtors_master.name AS DebtorName,  ".TB_PREF."debtors_master.debtor_ref,
   				".TB_PREF."debtors_master.curr_code, ".TB_PREF."debtors_master.payment_terms, ".TB_PREF."debtors_master.tax_id AS tax_id, 
   				".TB_PREF."debtors_master.email, ".TB_PREF."debtors_master.address
    			FROM ".TB_PREF."debtor_trans, ".TB_PREF."debtors_master 
				WHERE ".TB_PREF."debtor_trans.debtor_no = ".TB_PREF."debtors_master.debtor_no
				AND ".TB_PREF."debtor_trans.type = ".db_escape($type)."
				AND ".TB_PREF."debtor_trans.trans_no = ".db_escape($trans_no);
   	$result = db_query($sql, "The remittance cannot be retrieved");
   	if (db_num_rows($result) == 0)
   		return false;
    return db_fetch($result);
}

function get_allocations_for_receipt($debtor_id, $type, $trans_no)
{
	$sql = get_alloc_trans_sql("amt, trans.reference, trans.alloc", "trans.trans_no = alloc.trans_no_to
		AND trans.type = alloc.trans_type_to
		AND alloc.trans_no_from=$trans_no
		AND alloc.trans_type_from=$type
		AND trans.debtor_no=".db_escape($debtor_id),
		TB_PREF."cust_allocations as alloc");
	$sql .= " ORDER BY trans_no";
	return db_query($sql, "Cannot retreive alloc to transactions");
}

function get_sales_ref_no_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_sales_ref_no_OR_PR_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location, form_type
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_so_form_cat_name_report($cat_id)
{
	$sql = "SELECT name FROM ".TB_PREF."sales_form_category 
			WHERE id=".db_escape($cat_id);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

function get_chk_details($trans_no, $type)
{
	$sql = "SELECT
			".TB_PREF."cheque_details.id,
			".TB_PREF."cheque_details.bank_trans_id,
			".TB_PREF."cheque_details.bank,
			".TB_PREF."cheque_details.branch,
			".TB_PREF."cheque_details.chk_number,
			".TB_PREF."cheque_details.chk_date,
			".TB_PREF."bank_trans.amount,
			".TB_PREF."debtors_master.name,
			".TB_PREF."debtor_trans.tran_date,
			".TB_PREF."debtor_trans.trans_no
			FROM
			".TB_PREF."cheque_details ,
			".TB_PREF."debtor_trans ,
			".TB_PREF."debtors_master ,
			".TB_PREF."bank_trans
			WHERE
			".TB_PREF."bank_trans.trans_no = $trans_no AND ".TB_PREF."bank_trans.type = ".$type." AND ".TB_PREF."debtor_trans.type = ".$type." AND
			".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
			".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type AND
			".TB_PREF."debtor_trans.reference =  ".TB_PREF."bank_trans.ref AND
			".TB_PREF."debtor_trans.debtor_no =  ".TB_PREF."debtors_master.debtor_no ";
	
	$result = db_query($sql,"Failed to retrieve cheque details -> view_receipt");
	$row = db_fetch($result);
	return $row;

}

function _y($y)
{
	return 792 - $y;
}

function convert_number($number) 
{ 
    if (($number < 0) || ($number > 999999999)) 
    { 
    throw new Exception("Number is out of range");
    } 

    $Gn = floor($number / 1000000);  /* Millions (giga) */ 
    $number -= $Gn * 1000000; 
    $kn = floor($number / 1000);     /* Thousands (kilo) */ 
    $number -= $kn * 1000; 
    $Hn = floor($number / 100);      /* Hundreds (hecto) */ 
    $number -= $Hn * 100; 
    $Dn = floor($number / 10);       /* Tens (deca) */ 
    $n = $number % 10;               /* Ones */ 

    $res = ""; 

    if ($Gn) 
    { 
        $res .= convert_number($Gn) . " Million"; 
    } 

    if ($kn) 
    { 
        $res .= (empty($res) ? "" : " ") . 
            convert_number($kn) . " Thousand"; 
    } 

    if ($Hn) 
    { 
        $res .= (empty($res) ? "" : " ") . 
            convert_number($Hn) . " Hundred"; 
    } 

    $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", 
        "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", 
        "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", 
        "Nineteen"); 
    $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", 
        "Seventy", "Eigthy", "Ninety"); 

    if ($Dn || $n) 
    { 
        if (!empty($res)) 
        { 
            $res .= " and "; 
        } 

        if ($Dn < 2) 
        { 
            $res .= $ones[$Dn * 10 + $n]; 
        } 
        else 
        { 
            $res .= $tens[$Dn]; 

            if ($n) 
            { 
                $res .= "-" . $ones[$n]; 
            } 
        } 
    } 

    if (empty($res)) 
    { 
        $res = "zero"; 
    } 

    return $res; 
}

function print_receipts()
{
	global $path_to_root, $systypes_array;

	include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$from = $_POST['PARAM_0'];
	// $to = $_POST['PARAM_1'];
	// $currency = $_POST['PARAM_2'];
	$comments = $_POST['PARAM_3'];

	if ($from == null)
		$from = 0;
	$dec = user_price_dec();

 	$fno = explode("-", $from);

	$cols = array(4, 85, 150, 225, 275, 360, 450, 515);

	// $headers in doctext.inc
	$aligns = array('left',	'left',	'left', 'left', 'right', 'right', 'right');

	$params = array('comments' => $comments);

	$cur = get_company_Pref('curr_default');

	$rep = new FrontReport(_('RECEIPT'), "ReceiptBulk", user_pagesize());
	$rep->currency = $cur;
	$rep->Font();
	$rep->Info($params, $cols, null, $aligns);
	
	$rep->AddImage($path_to_root . "/printouts/collection_receipt_2.JPG", 0, 396, 612, 396);
		
	$myrow = get_receipt($fno[1], $fno[0]);
	if (!$myrow)
		continue;			
	$baccount = get_default_bank_account($myrow['curr_code']);
	$params['bankaccount'] = $baccount['id'];
	
	$rep->TextWrap(470,_y(120),508-390,sql2date($myrow['tran_date']),'left');
	$rep->TextWrap(120,_y(150),240,$myrow['DebtorName'],'left');
	$rep->TextWrap(410,_y(150),240,$myrow['tax_id'],'left');
	$rep->TextWrap(105,_y(170),335,$myrow['address'],'left');
	
	$whole = floor($myrow['Total']);
	$cents = substr(number_format2($myrow['Total'],2),-2);
	
	$rep->TextWrap(27,_y(210),297,convert_number($whole).' pesos'.($cents > 0 ? ' and '.convert_number($cents) .' centavo/s': '').' only','left');
	$rep->TextWrap(380,_y(210),110,number_format2($myrow['Total'],2),'left');
	$rep->TextWrap(85,_y(230),475,get_comments_string($fno[1], $fno[0]),'left');
	
	$rep->End();
}

?>