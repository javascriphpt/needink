<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
//print_customer_balances();

$output=print_customer_balances();

header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=CustomerBalances.xls");
header("Pragma: no-cache");
header("Expires: 0");	

echo $output;

function get_open_balance($debtorno, $to, $convert, $branch_code, $location)
{
	$to = date2sql($to);

    $sql = "SELECT SUM(IF(".TB_PREF."debtor_trans.type = ".ST_SALESINVOICE.", (".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + 
    	".TB_PREF."debtor_trans.ov_freight + ".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt)";
    if ($convert)
    	$sql .= " * rate";
    $sql .= ", 0)) AS charges,
    	SUM(IF(".TB_PREF."debtor_trans.type <> ".ST_SALESINVOICE.", (".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + 
    	".TB_PREF."debtor_trans.ov_freight + ".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt)";
    if ($convert)
    	$sql .= " * rate";
    $sql .= " * -1, 0)) AS credits,
		SUM(".TB_PREF."debtor_trans.alloc";
	if ($convert)
		$sql .= " * rate";
	$sql .= ") AS Allocated,
		SUM(IF(".TB_PREF."debtor_trans.type = ".ST_SALESINVOICE.", (".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + 
    	".TB_PREF."debtor_trans.ov_freight + ".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt - ".TB_PREF."debtor_trans.alloc)";
    if ($convert)
    	$sql .= " * rate";
    $sql .= ", 
    	((".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + 
    	".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt) * -1 + ".TB_PREF."debtor_trans.alloc)";
    if ($convert)
    	$sql .= " * rate";
    $sql .= ")) AS OutStanding
		FROM ".TB_PREF."debtor_trans
    	WHERE ".TB_PREF."debtor_trans.tran_date < '$to'
		AND ".TB_PREF."debtor_trans.debtor_no = ".db_escape($debtorno)."
		AND ( (".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + 
    	".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt) - ".TB_PREF."debtor_trans.alloc) > 0
		AND ".TB_PREF."debtor_trans.type <> ".ST_CUSTDELIVERY." 
		AND ".TB_PREF."debtor_trans.location = ".db_escape($location)." ";
		
	// if($paid_unpaid == 1)
		// $sql .= " AND ".TB_PREF."debtor_trans.type <> ".ST_CUSTPAYMENT." AND ".TB_PREF."debtor_trans.type <> ".ST_CUSTCREDIT."";
				
	if ($branch_code != '')
		$sql .= " AND ".TB_PREF."debtor_trans.branch_code = ".db_escape($branch_code)." ";
		
	$sql .= " GROUP BY debtor_no";

    $result = db_query($sql,"No transactions were returned");
    return db_fetch($result);
}

function get_transactions($debtorno, $from, $to, $branch_code, $location)
{
	$from = date2sql($from);
	$to = date2sql($to);

    $sql = "SELECT ".TB_PREF."debtor_trans.*,
		(".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + 
		".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt)
		AS TotalAmount, ".TB_PREF."debtor_trans.alloc AS Allocated,
		((".TB_PREF."debtor_trans.type = ".ST_SALESINVOICE.")
		AND ".TB_PREF."debtor_trans.due_date < '$to') AS OverDue
    	FROM ".TB_PREF."debtor_trans
    	WHERE ".TB_PREF."debtor_trans.tran_date >= '$from'
		AND ".TB_PREF."debtor_trans.tran_date <= '$to'
		AND ".TB_PREF."debtor_trans.debtor_no = ".db_escape($debtorno)."
		AND ( (".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + 
		".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt) - ".TB_PREF."debtor_trans.alloc) > 0
		AND ".TB_PREF."debtor_trans.type <> ".ST_CUSTDELIVERY." 
		AND ".TB_PREF."debtor_trans.location = ".db_escape($location)." ";
	
	// if($paid_unpaid == 1)
		// $sql .= " AND ".TB_PREF."debtor_trans.type <> ".ST_CUSTPAYMENT." AND ".TB_PREF."debtor_trans.type <> ".ST_CUSTCREDIT."";
			
	if ($branch_code != '')
		$sql .= " AND ".TB_PREF."debtor_trans.branch_code = ".db_escape($branch_code)." ";
		
    $sql .= " ORDER BY ".TB_PREF."debtor_trans.tran_date";

    return db_query($sql,"No transactions were returned");
}

function get_sales_ref_no_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_sales_ref_no_OR_PR_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location, form_type
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_so_form_cat_name_report($cat_id)
{
	$sql = "SELECT name FROM ".TB_PREF."sales_form_category 
			WHERE id=".db_escape($cat_id);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

//----------------------------------------------------------------------------------------------------

function print_customer_balances()
{
    global $path_to_root, $systypes_array;

    $from = $_GET['PARAM_0'];
    $to = $_GET['PARAM_1'];
    $fromcust = $_GET['PARAM_2'];
    $branch = $_GET['PARAM_3'];
    $currency = $_GET['PARAM_4'];
	$destination = $_GET['PARAM_5'];
	//$paid_unpaid = $_GET['PARAM_6'];
	$location = $_GET['PARAM_7'];
	if ($destination == 0)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	if ($fromcust == '')
		$cust = _('All');
	else
		$cust = get_customer_name($fromcust);
    $dec = user_price_dec();

	if ($currency == '')
	{
		$convert = true;
		$currency = _('Balances in Home Currency');
	}
	else
		$convert = false;
		
	if ($location == '')
		$loc = _('All');
	else
		$loc = get_location_name($location);
		
	$cols = array(0, 90, 155, 210,	265, 335, 400, 465,	530);

	$headers = array(_('Trans Type'), _('#'), _('Date'), _('Due Date'), _('Charges'), _('Credits'),
		_('Allocated'), 	_('Outstanding'));

	$aligns = array('left',	'left',	'left',	'left',	'right', 'right', 'right', 'right');

    $params =   array( 	0 => '',
    				    1 => array('text' => _('Period'), 'from' => $from, 		'to' => $to),
    				    2 => array('text' => _('Customer'), 'from' => $cust,   	'to' => ''),
    				    3 => array('text' => _('Location'), 'from' => $loc, 'to' => ''));

    $rep = new FrontReport(_('Customer Balances'), "CustomerBalances", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	ob_start();
	?>
		<html>
			<head>
				<style>
					.title{
						font-weight:bold;
						font-size:18px;
					}
					td{
						border:solid thin black;
					}
					.total{
						font-weight:bold;
					}
					.header{
						font-style:italic;
						font-weight:bold;
					}
				</style>
			</head>
			<body>
	<?php
	
	echo '<span class="title">Customer Balances</span>';
	echo '<br>';
	echo '<br>';
		$headers =array(_('Trans Type'), _('#'), _('Date'), _('Due Date'), _('Charges'), _('Credits'), _('Allocated'), 	_('Outstanding'));
		
	$sql3 = "SELECT DISTINCT loc.loc_code, loc.location_name
			FROM ".TB_PREF."locations loc, ".TB_PREF."debtor_trans trans
			WHERE loc.loc_code = trans.location
			AND trans.type = type <> ".ST_CUSTDELIVERY."
			AND ( (trans.ov_amount + trans.ov_gst + trans.ov_freight + trans.ov_freight_tax + trans.ov_discount + trans.ewt) - trans.alloc) > 0
			AND trans.tran_date >= '".date2sql($from)."'
			AND trans.tran_date <= '".date2sql($to)."'";
	if ($location != '')
		$sql3 .= "AND loc.loc_code=".db_escape($location);
	if ($fromcust != '')
		$sql3 .= " AND trans.debtor_no=".db_escape($fromcust);
		$sql3 .= " ORDER BY loc.location_name";
	
	$result3 = db_query($sql3, "The location could not be retrieved");

	start_table();
	
	$loc_ = '';
	$grandtotal = array(0,0,0,0);
	while ($myrow3 = db_fetch($result3))
	{	
		if ($loc_ != $myrow3['location_name'])
		{
			start_row();
			echo "<td class='header' colspan=8>".$myrow3['location_name']."</td>";
			end_row();
			//start_row(); echo "<td class='header' colspan=8>&nbsp;</td>"; end_row();
		}
		
		$sql = "SELECT DISTINCT ".TB_PREF."debtors_master.debtor_no, ".TB_PREF."debtors_master.name, ".TB_PREF."debtors_master.curr_code 
				FROM ".TB_PREF."debtors_master, ".TB_PREF."debtor_trans 
				WHERE ".TB_PREF."debtors_master.debtor_no = ".TB_PREF."debtor_trans.debtor_no
				AND ".TB_PREF."debtor_trans.tran_date >= '".date2sql($from)."'
				AND ".TB_PREF."debtor_trans.tran_date <= '".date2sql($to)."'
				AND ".TB_PREF."debtor_trans.type <> ".ST_CUSTDELIVERY."
				AND ( (".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + 
		".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt) - ".TB_PREF."debtor_trans.alloc) > 0
				AND ".TB_PREF."debtor_trans.location=".db_escape($myrow3['loc_code']);
		if ($fromcust != '')
			$sql .= " AND ".TB_PREF."debtor_trans.debtor_no=".db_escape($fromcust);
				//AND ".TB_PREF."debtor_trans.debtor_no=".db_escape($myrow3['debtor_no']);
		$sql .= " ORDER BY ".TB_PREF."debtors_master.name";
		$result = db_query($sql, "The customers could not be retrieved");
		
		while ($myrow = db_fetch($result))
		{
			if (!$convert && $currency != $myrow['curr_code'])
				continue;
			
			start_row();
			echo "<td colspan=3>".$myrow['name']."</td>";
			
			$bal = get_open_balance($myrow['debtor_no'], $from, $convert, $branch, $myrow3['loc_code']);
			$init[0] = $init[1] = 0.0;
			echo "<td>"._("Open Balance")."</td>";	
			$init[0] = round2(abs($bal['charges']), $dec);
			echo "<td>".$init[0]."</td>";	
			$init[1] = round2(Abs($bal['credits']), $dec);
			echo "<td>".$init[1]."</td>";	
			$init[2] = round2($bal['Allocated'], $dec);
			echo "<td>".$init[2]."</td>";	
			$init[3] = round2($bal['OutStanding'], $dec);;
			echo "<td>".$init[3]."</td>";	
			$total = array(0,0,0,0);
			for ($i = 0; $i < 4; $i++)
			{
				$total[$i] += $init[$i];
				$grandtotal[$i] += $init[$i];
			}			
			end_row();
			
			start_row();
			foreach($headers as $value){
				echo "<td class='header'>".$value."</td>";
			}
			end_row();
						
			$res = get_transactions($myrow['debtor_no'], $from, $to, $branch, $myrow3['loc_code']);
			if (db_num_rows($res)==0)
				continue;
			$rep->Line($rep->row + 4);
			while ($trans = db_fetch($res))
			{
				if($trans['type'] == 13 && $trans['trans_link'] != 0)
					continue;
			
				if($trans['type'] != 12)
				{
					$ref_no = get_sales_ref_no_report($trans['trans_no'], $trans['type']);
					$ref = $ref_no["location"]."-".$ref_no["form_type_no"];
				}
				else
				{
					$ref_no = get_sales_ref_no_OR_PR_report($trans['trans_no'], $trans['type']);
					$ref = $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name_report($ref_no["form_type"]);
				}
				
				start_row();	
				echo "<td>".$systypes_array[$trans['type']]."</td>";	
				echo "<td>".$ref."</td>";	
				echo "<td>".$trans['tran_date']."</td>";	
				if ($trans['type'] == ST_SALESINVOICE)
					echo "<td>".$trans['due_date']."</td>";	
				else
					echo "<td>&nbsp;</td>";	
				$item[0] = $item[1] = 0.0;
				if ($convert)
					$rate = $trans['rate'];
				else
					$rate = 1.0;
				if ($trans['type'] == ST_CUSTCREDIT || $trans['type'] == ST_CUSTPAYMENT || $trans['type'] == ST_BANKDEPOSIT)
					$trans['TotalAmount'] *= -1;
				if ($trans['TotalAmount'] > 0.0)
				{
					$item[0] = round2(abs($trans['TotalAmount']) * $rate, $dec);
					echo "<td>".$item[0]."</td>";	
					echo "<td>&nbsp;</td>";	
				}
				else
				{
					$item[1] = round2(Abs($trans['TotalAmount']) * $rate, $dec);
					echo "<td>&nbsp;</td>";	
					echo "<td>".$item[1]."</td>";	
				}
				$item[2] = round2($trans['Allocated'] * $rate, $dec);
				echo "<td>".$item[2]."</td>";	
				/*
				if ($trans['type'] == 10)
					$item[3] = ($trans['TotalAmount'] - $trans['Allocated']) * $rate;
				else
					$item[3] = ($trans['TotalAmount'] + $trans['Allocated']) * $rate;
				*/
				if ($trans['type'] == ST_SALESINVOICE || $trans['type'] == ST_BANKPAYMENT)
					$item[3] = $item[0] + $item[1] - $item[2];
				else	
					$item[3] = $item[0] - $item[1] + $item[2];
				echo "<td>".$item[3]."</td>";	
				for ($i = 0; $i < 4; $i++)
				{
					$total[$i] += $item[$i];
					$grandtotal[$i] += $item[$i];
				}
				end_row();
			}
			
			start_row();
			echo "<td colspan=4>"._('Total')."</td>";	
			for ($i = 0; $i < 4; $i++)
				echo "<td>".$total[$i]."</td>";	
			end_row();
			start_row(); echo "<td class='header' colspan=8>&nbsp;</td>"; end_row();
			
		}
	}
	
	start_row();
	echo "<td colspan=4>"._('Grand Total')."</td>";	
	for ($i = 0; $i < 4; $i++)
		echo "<td>".$grandtotal[$i]."</td>";	
	end_row();
			
	return ob_get_clean();
}

?>