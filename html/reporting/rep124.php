<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_sales_report();

function getTransactions($debtorno, $from, $to, $location)
{
	$sql = "SELECT trans.debtor_no, trans.branch_code, trans.trans_no, trans.type, trans.tran_date, 
				line.stock_id, line.description, line.quantity
		FROM ".TB_PREF."debtor_trans as trans, ".TB_PREF."debtor_trans_details as line, ".TB_PREF."stock_moves as moves
		WHERE trans.type = ".ST_CUSTDELIVERY."
		AND line.debtor_trans_type = ".ST_CUSTDELIVERY."
		AND trans.trans_no = line.debtor_trans_no
		AND trans.type = line.debtor_trans_type
		AND moves.trans_no = trans.trans_no
		AND moves.type = trans.type
		AND moves.stock_id = line.stock_id
		AND trans.tran_date >= '".date2sql($from)."'
		AND trans.tran_date <= '".date2sql($to)."' ";
	
	if ($location != 'all')
		$sql .= " AND moves.loc_code = ".db_escape($location);
		
	$sql .= " AND trans.debtor_no = ".db_escape($debtorno)."";
//display_error(db_num_rows(db_query($sql)));
//display_error($sql);
	return db_query($sql,"No transactions were returned");

}

function get_sales_ref_no_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

//----------------------------------------------------------------------------------------------------

function print_sales_report()
{
    global $path_to_root, $systypes_array;

	$from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $fromcust = $_POST['PARAM_2'];
    $location = $_POST['PARAM_3'];
    $comments = $_POST['PARAM_4'];
	$destination = $_POST['PARAM_5'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");
		
	if ($fromcust == ALL_NUMERIC)
		$cust = _('All');
	else
		$cust = get_customer_name($fromcust);
		
	if ($location == ALL_TEXT)
		$location = 'all';
	if ($location == 'all')
		$loc = _('All');
	else
		$loc = get_location_name($location);

	$cols = array(0, 90, 150, 250, 350, 450, 505, 620);

	$headers = array(_('Customer Name'), _('Branch'), _("Item"), _('Qty'), _('DR No.'), _('Date Delivered'), _('INV No.'));

	$aligns = array('left',	'left',	'left',	'left',	'left',	'left',	'left');
		
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Customer'), 'from' => $cust,   	'to' => ''),
						2 => array('text' => _('Location'), 'from' => $loc, 'to' => ''));

    $rep = new FrontReport(_('Summary of Optimum Toner Purchases Report'), "SummaryOfOptimumTonerPurchasesReport", user_pagesize(), 8);

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$sql = "SELECT DISTINCT ".TB_PREF."debtor_trans.debtor_no, name, curr_code 
			FROM ".TB_PREF."debtors_master, ".TB_PREF."debtor_trans, ".TB_PREF."stock_moves 
			WHERE ".TB_PREF."debtors_master.debtor_no = ".TB_PREF."debtor_trans.debtor_no 
			AND ".TB_PREF."debtor_trans.trans_no =  ".TB_PREF."stock_moves.trans_no
			AND ".TB_PREF."debtor_trans.type =  ".TB_PREF."stock_moves.type
			AND ".TB_PREF."debtor_trans.location =  ".TB_PREF."stock_moves.loc_code
			AND ".TB_PREF."debtor_trans.tran_date >= '".date2sql($from)."'
			AND ".TB_PREF."debtor_trans.tran_date <= '".date2sql($to)."'";
	if ($fromcust != ALL_NUMERIC)
		$sql .= " AND ".TB_PREF."debtor_trans.debtor_no=".db_escape($fromcust);
	if ($location != 'all')
		$sql .= " AND ".TB_PREF."stock_moves.loc_code = ".db_escape($location);
	$sql .= " ORDER BY name";
	$result = db_query($sql, "The customers could not be retrieved");
	
	while ($myrow = db_fetch($result))
	{
		set_time_limit(0);
		
		$rep->fontSize += 2;
		$rep->TextCol(0, 4, $myrow['name']);
		$rep->fontSize -= 2;
		$rep->NewLine();
		
		$res = getTransactions($myrow['debtor_no'], $from, $to, $location);
		// if (db_num_rows($res)==0)
			// continue;
		if (db_num_rows($res)!=0)
		{
			while ($row = db_fetch($res))
			{
				set_time_limit(0);
				
				foreach(get_parent_trans($row["type"], $row["trans_no"]) as $id=>$val)
				{
					$inv_no = $val;
				}

				$ref_no = get_sales_ref_no_report($row["trans_no"], $row["type"]);
				$ref_no2 = get_sales_ref_no_report($inv_no, ST_SALESINVOICE);
				
				$rep->TextCol(0, 1, get_customer_name($row['debtor_no']));
				$rep->TextCol(1, 2, get_branch_name($row['branch_code']));
				$rep->TextCol(2, 3, $row['description']);
				$rep->TextCol(3, 4, $row['quantity']);
				$rep->TextCol(4, 5, $ref_no["location"]."-".$ref_no["form_type_no"]);
				$rep->TextCol(5, 6, sql2date($row['tran_date']));
				$rep->TextCol(6, 7, $inv_no == '' ? '' : $ref_no2["location"]."-".$ref_no2["form_type_no"]);
				$rep->NewLine();
				
			}
				
			$rep->NewLine();
		}
	}
	
    $rep->End();
}

?>