<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPINQUIRY';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/includes/ui/ui_lists.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_sup_inquiry();

//----------------------------------------------------------------------------------------------------

function print_sup_inquiry()
{
    global $path_to_root, $systypes_array;
	
	$sup_no = $_GET['sup_no'];
	$company_name = $_GET['company_name'];
	$system_unit = $_GET['system_unit'];
	$serial_no = $_GET['serial_no'];
	$assigned_tsg = $_GET['assigned_tsg'];
	$conformed = $_GET['conformed'];
	$from = $_GET['from'];
	$to = $_GET['to'];
	$type = $_GET['type'];
	$output = $_GET['output'];
		
	if ($output == 0)
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/excel_report.inc");

    $dec = user_price_dec();

	$cols = array(0, 55, 275, 370, 490, 570, 670, 770, 815);

	$headers = array(_('SUP No.'), _('Company Name'), _('System / Unit'), _('Serial No.'), _('Assigned TSG'), _('Conformed'), _('Completed Date'));

	$aligns = array('left', 'left', 'left', 'left', 'left', 'left', 'left');

    $params =   array( 	0 => '',
    				    1 => array('text' => _('Period'), 'from' => sql2date($from), 'to' => sql2date($to)));

    $rep = new FrontReport(_('Support Process Inquiry'), "SupportProcessInquiry", user_pagesize(), 9, 'L');

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	$sql = "SELECT sup.sup_no,
				sup.company_name,
				sup.system_unit,
				sup.serial_no,
				sup.assigned_tsg,
				sup.conformed, 
				completed_date, sup.id
			FROM ".TB_PREF."sup as sup, ".TB_PREF."sup_type as type
			WHERE sup.type = type.id 
			AND sup.type = ".db_escape($type)."
			AND sup.completed_date >= ".db_escape($from)."
			AND sup.completed_date <= ".db_escape($to)." ";

	if ($sup_no != '')
		$sql .= " AND sup.sup_no LIKE ".db_escape("%".$sup_no."%");
		
	if ($company_name != '')
		$sql .= " AND sup.company_name LIKE ".db_escape("%".$company_name."%");
		
	if ($system_unit != '')
		$sql .= " AND sup.system_unit LIKE ".db_escape("%".$system_unit."%");
		
	if ($serial_no != '')
		$sql .= " AND sup.serial_no LIKE ".db_escape("%".$serial_no."%");
		
	if ($assigned_tsg != '')
		$sql .= " AND sup.assigned_tsg LIKE ".db_escape("%".$assigned_tsg."%");
		
	if ($conformed != '')
		$sql .= " AND sup.conformed LIKE ".db_escape("%".$conformed."%");
		
	$sql .= " ORDER BY sup.sup_no ASC, sup.company_name";
	
	$result = db_query($sql, "Transactions could not be retrieved");

	while ($myrow = db_fetch($result))
	{
			
		$rep->TextCol(0, 1, $myrow["sup_no"]);
		$rep->TextCol(1, 2, $myrow["company_name"]);
		$rep->TextCol(2, 3, $myrow["system_unit"]);
		$rep->TextCol(3, 4, $myrow["serial_no"]);
		$rep->TextCol(4, 5, $myrow["assigned_tsg"]);
		$rep->TextCol(5, 6, $myrow["conformed"]);
		$rep->TextCol(6, 7, sql2date($myrow["completed_date"]));
		
		$rep->NewLine();
	}

    $rep->End();
}

?>