<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui/ui_lists.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_sales_report();

function get_trans_details($branch_code)
{
	$sql = "SELECT MAX(trans.trans_no) trans_no, MAX(trans.tran_date) tran_date
			FROM ".TB_PREF."debtor_trans trans, ".TB_PREF."cust_branch c
			WHERE trans.branch_code=c.branch_code
			AND trans.type=13
			AND c.branch_code = ".db_escape($branch_code)."
			GROUP BY c.branch_code";
	
	$result = db_query($sql,"could not retreive transaction");

	return db_fetch_row($result);	
}

function get_info($trans_no, $type)
{
	$sql = "SELECT CONCAT(trans.location, '-', trans.form_type_no) as ref, 
				(trans.ov_amount + trans.ov_gst + trans.ov_freight + trans.ov_freight_tax + trans.ov_discount) as total, c.salesman, c.br_name
			FROM ".TB_PREF."debtor_trans trans, ".TB_PREF."cust_branch c
			WHERE trans.branch_code=c.branch_code
			AND trans.type=".db_escape($type)."
			AND trans.trans_no = ".db_escape($trans_no);
	
	$result = db_query($sql,"could not retreive transaction");

	return db_fetch_row($result);	
}

//----------------------------------------------------------------------------------------------------

function print_sales_report()
{
    global $path_to_root, $systypes_array;

	$from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $location = $_POST['PARAM_2'];
    $comments = $_POST['PARAM_3'];
	$destination = $_POST['PARAM_4'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");
				
	if ($location == ALL_TEXT)
		$location = 'all';
	if ($location == 'all')
		$loc = _('All');
	else
		$loc = get_location_name($location);

	$cols = array(0, 250, 340, 420, 475, 620);

	$headers = array(_('Company'), _('Date of Last Purchase'), _("DR #"), _('DR Amount'), _('Sales Personnel'));

	$aligns = array('left',	'left',	'left',	'left',	'left',	'left');
		
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Location'), 'from' => $loc, 'to' => ''));

    $rep = new FrontReport(_('Report for Company without Orders'), "ReportForCompanyWithoutOrders", user_pagesize(), 8);

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	$subquery = "SELECT DISTINCT branch_code
				FROM ".TB_PREF."debtor_trans 
				WHERE type=13
				AND tran_date >= ".db_escape(date2sql($from))."
				AND	tran_date <= ".db_escape(date2sql($to));
	if ($location != 'all')
		$subquery .= " AND location = ".db_escape($location);
						
	
	$sql = "SELECT a.branch_code, a.br_name, b.name
			FROM ".TB_PREF."cust_branch a
			JOIN ".TB_PREF."debtors_master b ON a.debtor_no = b.debtor_no
			WHERE a.branch_code NOT IN	
				(
					$subquery
				) 
			AND a.inactive != 1 ";
	if ($location != 'all')
		$sql .= " AND a.default_location = ".db_escape($location);	
	$sql .= " ORDER BY a.br_name";
	$result = db_query($sql, "transactions could not be retrieve");

	$customer = "";
	while($myrow = db_fetch($result))
	{
		set_time_limit(0);
		
		if($customer != $myrow['name'])
		{
			$rep->NewLine();
			$rep->fontSize += 2;
			$rep->TextCol(0, 3, $myrow['name']);
			$rep->fontSize -= 2;
			$customer = $myrow['name'];
			$rep->NewLine();
		}
		
		$details = get_trans_details($myrow['branch_code']);
		$info = get_info($details[0], ST_CUSTDELIVERY);
		
		$rep->TextCol(0, 1, $myrow['name']." / ".$myrow['br_name']);
		$rep->TextCol(1, 2, sql2date($details[1]));
		$rep->TextCol(2, 3, $info[0]);	
		$rep->TextCol(3, 4, number_format2($info[1], 2));
		$rep->TextCol(4, 5, get_salesman_name($info[2]));
		
		$rep->NewLine();
	}
	
    $rep->End();
}

?>