<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPLIERANALYTIC';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Outstanding GRNs Report
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/includes/db/references_db.inc");

//----------------------------------------------------------------------------------------------------

print_outstanding_GRN();

function getTransactions($fromsupp, $from, $to)
{
	$from = date2sql($from);
	$to = date2sql($to);	

	$sql = "SELECT ".TB_PREF."grn_batch.id,
			".TB_PREF."purch_order_details.order_no,
			".TB_PREF."grn_batch.reference,
			".TB_PREF."grn_batch.loc_code,
			".TB_PREF."grn_batch.supplier_id,
			".TB_PREF."suppliers.supp_name,
			".TB_PREF."grn_items.item_code,
			".TB_PREF."grn_items.description,
			qty_recd,
			quantity_inv,
			std_cost_unit,
			act_price,
			unit_price,
			quantity_ordered,
			quantity_received,
			comments
		FROM ".TB_PREF."grn_items,
			".TB_PREF."grn_batch,
			".TB_PREF."purch_order_details,
			".TB_PREF."purch_orders,
			".TB_PREF."suppliers
		WHERE ".TB_PREF."grn_batch.supplier_id=".TB_PREF."suppliers.supplier_id
		AND ".TB_PREF."grn_batch.id = ".TB_PREF."grn_items.grn_batch_id
		AND ".TB_PREF."grn_items.po_detail_item = ".TB_PREF."purch_order_details.po_detail_item
		AND ".TB_PREF."purch_orders.order_no = ".TB_PREF."purch_order_details.order_no
		AND ".TB_PREF."grn_batch.delivery_date >= '$from'
		AND ".TB_PREF."grn_batch.delivery_date <= '$to' ";

	if ($fromsupp != ALL_NUMERIC)
		$sql .= "AND ".TB_PREF."grn_batch.supplier_id =".db_escape($fromsupp)." ";
	$sql .= "ORDER BY ".TB_PREF."grn_batch.supplier_id,
			".TB_PREF."grn_batch.id";

    return db_query($sql, "No transactions were returned");
}

//----------------------------------------------------------------------------------------------------

function print_outstanding_GRN()
{
    global $path_to_root;

    $from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $fromsupp = $_POST['PARAM_2'];
    $comments = $_POST['PARAM_3'];
	$destination = $_POST['PARAM_4'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	if ($fromsupp == ALL_NUMERIC)
		$supp = _('All');
	else
		$supp = get_supplier_name($fromsupp);
    $dec = user_price_dec();

	$cols = array(0, 40, 80, 190,	250, 320, 385, 470,	535, 730);

	$headers = array(_('GRN'), _('Order'), _('Item') . '/' . _('Description'), _('PO Qty'), _('Qty Recd'), _('Balance'),
		_('ISN'), _('AIC'), _('Memo'));

	$aligns = array('left',	'left',	'left',	'right', 'right', 'right', 'right', 'right', 'right');

    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Supplier'), 'from' => $supp, 'to' => ''));

    $rep = new FrontReport(_('Outstanding GRNs Report'), "OutstandingGRN", user_pagesize(), 9, 'L');

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$Tot_Val=0;
	$Supplier = '';
	$SuppTot_Val=0;
	$res = getTransactions($fromsupp, $from, $to);

	While ($GRNs = db_fetch($res))
	{
		$dec2 = get_qty_dec($GRNs['item_code']);
		if ($Supplier != $GRNs['supplier_id'])
		{
			if ($Supplier != '')
			{
				$rep->NewLine(2);
				$rep->TextCol(0, 5, _('Total'));
				$rep->AmountCol(5, 6, $SuppTot_Val, $dec);
				$rep->Line($rep->row - 2);
				$rep->NewLine(3);
				$SuppTot_Val = 0;
			}
			$rep->TextCol(0, 6, $GRNs['supp_name']);
			$Supplier = $GRNs['supplier_id'];
		}
		
		$isn=get_isn($GRNs['id'], ST_SUPPRECEIVE, $GRNs['item_code'], $GRNs['loc_code']);
		$aic=get_aic_out($GRNs['id'], ST_SUPPRECEIVE, $GRNs['item_code'], $GRNs['loc_code']);
		
		$rep->NewLine();
		$rep->TextCol(0, 1, $GRNs['reference']);	//$GRNs['id']);
		$rep->TextCol(1, 2, get_reference(ST_PURCHORDER, $GRNs['order_no']));
		$rep->TextCol(2, 3, $GRNs['item_code'] . '-' . $GRNs['description']);
		$rep->AmountCol(3, 4, $GRNs['quantity_ordered'], $dec2);
		$rep->AmountCol(4, 5, $GRNs['quantity_received'], $dec2);
		$QtyOstg = $GRNs['quantity_ordered'] - $GRNs['quantity_received'];
		$rep->AmountCol(5, 6, $QtyOstg, $dec2);
		
		if ($destination)
		{	//excel
			$rep->TextCol(6, 7, $isn);
			$rep->TextCol(7, 8, $aic);
		}
		else
		{	//pdf
			// if($isn == '' && $aic == '')
			// {
				// $rep->TextCol(6, 7, '');
				// $rep->TextCol(7, 8, '');
			// }
			// else
			// {
				// $rep->TextColLines(6, 7, $isn);
			// }
			
			// $oldrow = $rep->row;

			$rowno = $rep->row;
			if($aic)
			$rep->TextColLines(7, 8, $aic);
			$rep->row = $rowno;
			if($isn)
			$rep->TextColLines(6, 7, $isn);
			
			// $newrow = $rep->row;
			// $rep->row = $oldrow;
		}
		
		$rep->TextCol(8, 9, $GRNs['comments']);
			
		$Tot_Val += $QtyOstg;
		$SuppTot_Val += $QtyOstg;

		$rep->NewLine(0, 1);
	}
	if ($Supplier != '')
	{
		$rep->NewLine();
		$rep->TextCol(0, 5, _('Total'));
		$rep->AmountCol(5, 6, $SuppTot_Val, $dec);
		$rep->Line($rep->row - 2);
		$rep->NewLine(3);
		$SuppTot_Val = 0;
	}
	$rep->NewLine(2);
	$rep->TextCol(0, 5, _('Grand Total'));
	$rep->AmountCol(5, 6, $Tot_Val, $dec);
	$rep->Line($rep->row - 2);
	$rep->NewLine();
    $rep->End();
}

?>