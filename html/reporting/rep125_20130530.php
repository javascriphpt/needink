<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_sales_report();

function get_trans_details($trans_no, $debtor_no)
{
	$sql = "SELECT cust.name, trans.tran_date, CONCAT(trans.location, '-', trans.form_type_no) as ref, 
				(trans.ov_amount + trans.ov_gst + trans.ov_freight + trans.ov_freight_tax + trans.ov_discount) as total, c.salesman
		FROM ".TB_PREF."debtor_trans as trans, ".TB_PREF."debtors_master as cust, ".TB_PREF."cust_branch c
		WHERE trans.type = ".ST_CUSTDELIVERY."
		AND trans.debtor_no = cust.debtor_no
		AND trans.branch_code = c.branch_code
		AND cust.debtor_no = c.debtor_no
		AND trans.trans_no = ".db_escape($trans_no)."
		AND cust.debtor_no = ".db_escape($debtor_no);
	
	$result = db_query($sql,"could not retreive transaction");

	return db_fetch_row($result);	
}

//----------------------------------------------------------------------------------------------------

function print_sales_report()
{
    global $path_to_root, $systypes_array;

	$from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $location = $_POST['PARAM_2'];
    $comments = $_POST['PARAM_3'];
	$destination = $_POST['PARAM_4'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");
				
	if ($location == ALL_TEXT)
		$location = 'all';
	if ($location == 'all')
		$loc = _('All');
	else
		$loc = get_location_name($location);

	$cols = array(0, 250, 340, 420, 475, 620);

	$headers = array(_('Company'), _('Date of Last Purchase'), _("DR #"), _('DR Amount'), _('Sales Personnel'));

	$aligns = array('left',	'left',	'left',	'left',	'left',	'left');
		
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Location'), 'from' => $loc, 'to' => ''));

    $rep = new FrontReport(_('Report for Company without Orders'), "ReportForCompanyWithoutOrders", user_pagesize(), 8);

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	$sql = "SELECT distinct a.debtor_no,
					(
						SELECT MAX(trans_no)
						FROM ".TB_PREF."debtor_trans
						WHERE debtor_no=a.debtor_no
						AND type=13
					) as trans_no,
					(
						SELECT MAX(tran_date)
						FROM ".TB_PREF."debtor_trans
						WHERE debtor_no=a.debtor_no
						AND type=13
					) as transaction_date
				FROM ".TB_PREF."debtor_trans a
				WHERE a.type=13
				AND a.tran_date >= '". date2sql($from) . "'
				AND a.tran_date <= '". date2sql($to). "'";
				
		if ($location != 'all')
			$sql .= " AND a.location = ".db_escape($location);
						
		$sql .= " GROUP BY a.debtor_no
				ORDER BY transaction_date";

	$result = db_query($sql, "transactions could not be retrieve");
	
	$dr_text = '';
	while($myrow = db_fetch($result))
	{
		set_time_limit(0);
		
		$details = get_trans_details($myrow['trans_no'], $myrow['debtor_no']);
		
		$rep->TextCol(0, 1, $details[0]);
		$rep->TextCol(1, 2, sql2date($details[1]));
		$rep->TextCol(2, 3, $details[2]);
		$rep->TextCol(3, 4, number_format2($details[3], 2));
		$rep->TextCol(4, 5, get_salesman_name($details[4]));
		
		$rep->NewLine();
	}
	
    $rep->End();
}

?>