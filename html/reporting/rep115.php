<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SALESTRANSVIEW';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_no_po_report();

function get_no_po($startdate, $enddate, $customer, $disabled)
{
	$sql = "SELECT MAX(a.trans_no), b.name, MAX(a.tran_date) as last_po_date,
				   (SELECT SUM(ov_amount) FROM 0_debtor_trans WHERE trans_no=MAX(a.trans_no) AND type=10) as amount
			FROM ".TB_PREF."debtor_trans a, ".TB_PREF."debtors_master b, ".TB_PREF."stock_moves c, ".TB_PREF."cust_branch d 
			WHERE a.type=10
			AND a.debtor_no = b.debtor_no
			AND a.trans_no = c.trans_no
			AND a.branch_code = d.branch_code
			AND b.debtor_no = d.debtor_no
			AND d.disable_trans = ".$disabled."
			AND a.tran_date >= '". date2sql($startdate) . "'
			AND a.tran_date <= '". date2sql($enddate). "'";
			
		if ($customer != -1)
		{
	$sql .= " AND b.debtor_no = ". $customer ." ";
		}
		
	$sql .= " GROUP BY b.debtor_no
			ORDER BY b.name";
			
	return db_query($sql,"No transactions were returned");
}

//----------------------------------------------------------------------------------------------------

function print_no_po_report()
{
    global $path_to_root, $systypes_array;

    $from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $customer = $_POST['PARAM_2'];
    $comments = $_POST['PARAM_3'];
	$destination = $_POST['PARAM_4'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$cols = array(10, 150, 350, 540);

	$headers = array(_('Customer Name'), _('Last PO Date'), _('Amount'));

	$aligns = array('left',	'right', 'right');
	
	if ($customer == ALL_NUMERIC)
		$cust = _('All');
	else
		$cust = get_customer_name($fromcust);
		
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to));

    $rep = new FrontReport(_('Ageing per Sales Customer'), "AgeingPerSalesCustomer", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$sql = "SELECT distinct b.debtor_no, b.name,
				(
					SELECT MAX(tran_date)
					FROM ".TB_PREF."debtor_trans
					WHERE debtor_no=b.debtor_no
					AND type=10
				) as last_po_date
			FROM ".TB_PREF."debtor_trans a, ".TB_PREF."debtors_master b, ".TB_PREF."cust_branch c
			WHERE a.type=10
			AND a.debtor_no != b.debtor_no
			AND a.branch_code != c.branch_code
			AND b.debtor_no = c.debtor_no
			AND a.tran_date >= '". date2sql($from) . "'
			AND a.tran_date <= '". date2sql($to). "'";
			
	if ($customer != -1)
		$sql .= " AND b.debtor_no = ". $customer ." ";
	
	$sql .= " GROUP BY b.debtor_no
		ORDER BY b.name";
	
	$res = db_query($sql, "The customers could not be retrieved");
	
	while ($trans=db_fetch($res)) 
	{
		$rep->TextCol(0, 1,	$trans['name']);
		$rep->TextCol(1, 2,	$trans['last_po_date']);
		$rep->NewLine();			
	}
	
    $rep->End();
}

?>