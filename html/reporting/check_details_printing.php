<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPCHECKDETAILSINQ';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/includes/ui/ui_lists.inc");
include_once($path_to_root . "/includes/banking.inc");
include_once($path_to_root . "/includes/ui.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();

$output=print_check_details_printing();

header("Content-type: application/x-msdownload");
header("Content-Disposition: attachment; filename=CheckDetailsPrinting.xls");
header("Pragma: no-cache");
header("Expires: 0");	

echo $output;


//----------------------------------------------------------------------------------------------------

function get_supplier_ref($supplier_id)
{
	$sql = "SELECT supp_ref AS name FROM ".TB_PREF."suppliers WHERE supplier_id=".db_escape($supplier_id);

	$result = db_query($sql, "could not get supplier");

	$row = db_fetch_row($result);

	return $row[0];
}

//----------------------------------------------------------------------------------------------------

function print_check_details_printing()
{
    global $path_to_root, $systypes_array;
	
	$from = $_GET['from'];
	$to = $_GET['to'];
	
	include_once($path_to_root . "/reporting/includes/excel_report.inc");
	
	$sql = "SELECT
		".TB_PREF."bank_trans.trans_no,	
		".TB_PREF."bank_trans.person_id,	
		".TB_PREF."cheque_details.chk_number,
		".TB_PREF."cheque_details.chk_date,
		ABS(".TB_PREF."bank_trans.amount) as total_amount,
		".TB_PREF."bank_trans.type,
		".TB_PREF."bank_trans.ref,
		".TB_PREF."bank_trans.person_type_id,
		".TB_PREF."bank_trans.trans_date
		FROM
		".TB_PREF."cheque_details ,
		".TB_PREF."bank_trans
		WHERE
		".TB_PREF."bank_trans.type IN (1, 22) AND
		".TB_PREF."cheque_details.type IN (1, 22) AND
		".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
		".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type AND
		".TB_PREF."cheque_details.chk_date >= '$from' AND	
		".TB_PREF."cheque_details.chk_date <= '$to' ";
	
	$result = db_query($sql, "Transactions could not be retrieved");
	
	ob_start();
		?>
			<html>
				<head>
					<style>
						.title{
							font-weight:bold;
							font-size:18px;
						}
						td{
							border:solid thin black;
						}
						.total{
							font-weight:bold;
						}
						.header{
							font-style:italic;
							font-weight:bold;
						}
					</style>
				</head>
				<body>
		<?php
		
				// echo '<span class="title">Check Details Printing</span>';
				// echo '<br>';
				// echo '<br>';
				$headers = array('Prefix', 'Batch No.', 'Voucher No.', 'Voucher Date', 'Payee Code', 'Payee Name', 'Check Date', 'Check Amount', 'Reference No.', 'Invoice No.', 'Invoice Date', 'Invoice Amount', 'DM/CM Amount', 'Tax Amount', 'Amount Paid', 'Purpose of Check', 'Tax Code', 'Type of Check');
					
				start_table();
				start_row();
				foreach($headers as $value){
					echo "<td class='header'>".$value."</td>";
				}
				end_row();
				
				while ($myrow = db_fetch($result))
				{
					start_row();
					echo "<td>C</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>".$myrow['ref']."</td>";
					echo "<td>".$myrow['trans_date']."</td>";
					if($myrow['type'] == 1)
					{
						echo "<td>".payment_person_name($myrow['person_type_id'], $myrow['person_id'])."</td>";
						echo "<td>".payment_person_name($myrow['person_type_id'], $myrow['person_id'])."</td>";
					}
					else
					{
						echo "<td>".get_supplier_ref($myrow['person_id'])."</td>";
						echo "<td>".get_supplier_name($myrow['person_id'])."</td>";
					}
					echo "<td>".$myrow['chk_date']."</td>";
					echo "<td>".number_format2($myrow['total_amount'], 2)."</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					echo "<td>&nbsp;</td>";
					
					if($myrow['type'] == 22)
					{
						$alloc_sql = "SELECT trans.trans_no, trans.supp_reference, trans.reference, trans.tran_date,
								(trans.ov_amount + trans.ov_gst) as total, alloc.amt as alloc_amount
							FROM ".TB_PREF."supp_allocations alloc,
								".TB_PREF."supp_trans trans
							WHERE trans.trans_no = alloc.trans_no_to
							AND trans.type = alloc.trans_type_to
							AND alloc.trans_type_from = ".db_escape($myrow['type'])."
							AND  alloc.trans_no_from = ".db_escape($myrow['trans_no']);
						$alloc_result = db_query($alloc_sql, "Transactions could not be retrieved");
						
						while ($row = db_fetch($alloc_result))
						{
							start_row();
							echo "<td>V</td>";
							echo "<td>&nbsp;</td>";
							echo "<td>&nbsp;</td>";
							echo "<td>&nbsp;</td>";
							echo "<td>&nbsp;</td>";
							echo "<td>&nbsp;</td>";
							echo "<td>&nbsp;</td>";
							echo "<td>&nbsp;</td>";
							echo "<td>".$row['supp_reference']."</td>";
							echo "<td>".$row['reference']."</td>";
							echo "<td>".$row['tran_date']."</td>";
							echo "<td>".number_format2($row['total'], 2)."</td>";
							echo "<td>&nbsp;</td>";
							echo "<td>&nbsp;</td>";
							echo "<td>".number_format2($row['alloc_amount'], 2)."</td>";
							echo "<td>&nbsp;</td>";
							echo "<td>&nbsp;</td>";
							echo "<td>&nbsp;</td>";
						}
					}
				}
								
			return ob_get_clean();
}

?>