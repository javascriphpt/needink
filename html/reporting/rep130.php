<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_customer_since();

function print_customer_since()
{
    global $path_to_root, $systypes_array;

    $date_added = $_POST['PARAM_0'];
    $comments = $_POST['PARAM_1'];
	$destination = $_POST['PARAM_2'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$cols = array(0, 300, 450);

	$headers = array(_('Name'), _('Date Added'));

	$aligns = array('left',	'left');

    $params =   array( 	0 => '');

    $rep = new FrontReport(_('Customer Since'), "CustomerSince", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$sql = "SELECT name, date_added
		FROM ".TB_PREF."debtors_master 
		WHERE date_added = ".db_escape(date2sql($date_added));
	$result = db_query($sql, "The customer since could not be retrieved");

	while ($myrow = db_fetch($result))
	{
		set_time_limit(0);
		
		$rep->TextCol(0, 1, $myrow['name']);
		$rep->TextCol(1, 2, sql2date($myrow['date_added']));
			
		$rep->NewLine();
	}
	
	$rep->Line($rep->row - 4);
	$rep->NewLine();
	
	$rep->End();
}

?>