<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui/ui_lists.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_new_customer_first_purchase();


//----------------------------------------------------------------------------------------------------

function print_new_customer_first_purchase()
{
    global $path_to_root, $systypes_array;

	$from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $comments = $_POST['PARAM_2'];
	$destination = $_POST['PARAM_3'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");
			
	$cols = array(0, 100, 250, 340, 440, 495, 640);

	$headers = array(_('Company'), _('Branch'), _('Date'), _("DR #"), _('Amount'), _('Sales Personnel'));

	$aligns = array('left',	'left',	'left',	'left',	'left',	'left');
		
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to));

    $rep = new FrontReport(_('New Customer First Purchase'), "NewCustomerFirstPurchase", user_pagesize(), 8);

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	$sql = "SELECT DISTINCT b.branch_code, b.br_name, c.name, a.tran_date,
				CONCAT(a.location, '-', a.form_type_no) as ref, b.salesman,
				(a.ov_amount + a.ov_gst + a.ov_freight + a.ov_freight_tax + a.ov_discount) as total
			FROM ".TB_PREF."debtor_trans a
			INNER JOIN 0_cust_branch b ON a.branch_code = b.branch_code
			INNER JOIN 0_debtors_master c ON a.debtor_no = c.debtor_no AND b.debtor_no = c.debtor_no
			WHERE b.date_added >= ".db_escape(date2sql($from))."
			AND b.date_added <= ".db_escape(date2sql($to))."
			AND a.type = 13
			ORDER BY c.name, a.tran_date";
	$result = db_query($sql, "transactions could not be retrieve");

	while($myrow = db_fetch($result))
	{
		set_time_limit(0);
		
		$rep->TextCol(0, 1, $myrow['name']);
		$rep->TextCol(1, 2, $myrow['br_name']);
		$rep->TextCol(2, 3, sql2date($myrow['tran_date']));
		$rep->TextCol(3, 4, $myrow['ref']);	
		$rep->TextCol(4, 5, number_format2($myrow['total'], 2));
		$rep->TextCol(5, 6, get_salesman_name($myrow['salesman']));
		
		$rep->NewLine();
	}
		
    $rep->End();
}

?>