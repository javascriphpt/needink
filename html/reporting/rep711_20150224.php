<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PDCREP';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Tax Report
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//------------------------------------------------------------------

// trial_inquiry_controls();
print_pdc_report();

//----------------------------------------------------------------------------------------------------

function print_pdc_report()
{
    global $path_to_root, $systypes_array;

    $from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $comments = $_POST['PARAM_2'];
	$destination = $_POST['PARAM_3'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$cols = array(0, 70, 130, 270, 360, 480, 530);

	$headers = array(_('Voucher #'), _('Date'), _("Payee"), _("Check Date"), _('Check #'), _('Amount'));

	$aligns = array('left',	'left',	'left',	'left',	'left',	'right');
			
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to));

    $rep = new FrontReport(_('PDC Report'), "PDCReport", user_pagesize(), 9, 'P');

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	$sql = "SELECT a.trans_no, a.trans_date, a.ref, a.amount, a.person_type_id, a.person_id, b.*
			FROM 0_bank_trans a
			JOIN 0_cheque_details b ON a.trans_no = b.bank_trans_id AND a.type = b.type
			WHERE a.type = 1
			AND a.trans_date >= ".db_escape(date2sql($from))."
			AND a.trans_date <= ".db_escape(date2sql($to));
	$result = db_query($sql, "The pdc could not be retrieved");
	
	$total = 0;
	while ($row = db_fetch($result))
	{	
		$rep->TextCol(0, 1, $row['ref']);
		$rep->TextCol(1, 2, sql2date($row['trans_date']));
		$rep->TextCol(2, 3, payment_person_name($row['person_type_id'], $row['person_id']));
		$rep->TextCol(3, 4, sql2date($row['chk_date']));
		$rep->TextCol(4, 5, $row['chk_number']);
		$rep->TextCol(5, 6, number_format2(abs($row['amount']),user_price_dec()));
		$rep->NewLine();
		
		$total += abs($row['amount']);
	}
	
	$rep->Font('bold');	
	$rep->NewLine();
	$rep->Line($rep->row + 6);
	$rep->row -= 6;
	$rep->TextCol(0, 2,	_('Total '));
	$rep->TextCol(5, 6, number_format2($total,2));
	$rep->NewLine();
	$rep->Font('');	
	
    $rep->End();
}

?>