<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_collection_report();

function getTransactions($from, $to)
{
	$from = date2sql($from);
	$to = date2sql($to);
	
	$sql = "SELECT ".TB_PREF."supp_trans.*
    	FROM ".TB_PREF."supp_trans
    	WHERE ".TB_PREF."supp_trans.tran_date >= '$from'
		AND ".TB_PREF."supp_trans.ewt < 0
		AND ".TB_PREF."supp_trans.tran_date <= '$to'
		AND ".TB_PREF."supp_trans.type = 22
    	ORDER BY ".TB_PREF."supp_trans.reference, ".TB_PREF."supp_trans.tran_date";
	
	return db_query($sql,"No transactions were returned");

}

function get_chk_date($trans_no)
{
	$sql = "SELECT ".TB_PREF."cheque_details.chk_date
			FROM
			".TB_PREF."cheque_details ,
			".TB_PREF."supp_trans ,
			".TB_PREF."suppliers ,
			".TB_PREF."bank_trans
			WHERE
			".TB_PREF."bank_trans.trans_no = $trans_no AND ".TB_PREF."bank_trans.type = 22 AND ".TB_PREF."supp_trans.type = 22 AND
			".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
			".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type AND
			".TB_PREF."supp_trans.reference =  ".TB_PREF."bank_trans.ref AND
			".TB_PREF."supp_trans.supplier_id =  ".TB_PREF."suppliers.supplier_id ";
	
	$result = db_query($sql,"Failed to retrieve cheque details -> view_receipt");
	$row = db_fetch($result);
	
	if(db_num_rows($result) == 0)
		return '';
	else
		return $row[0];

}

//----------------------------------------------------------------------------------------------------

function print_collection_report()
{
    global $path_to_root, $systypes_array;

    $from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $comments = $_POST['PARAM_2'];
	$destination = $_POST['PARAM_3'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$cols = array(0, 90, 160, 330, 440, 500, 550);

	$headers = array(_('OR #'), _('Date'), _("Supplier's Name"), "", _('Check Date'), _('Amount'));

	$aligns = array('left',	'left',	'left',	'left',	'left',	'right');
	
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to));

    $rep = new FrontReport(_('EWT Report'), "EWTReportSupplier", user_pagesize(), 9);

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$total = 0;
	$res = getTransactions($from, $to);
	while ($row = db_fetch($res))
	{
		$rep->TextCol(0, 1, $row['reference']);
		$rep->TextCol(1, 2, $row['tran_date']);
		$rep->TextCol(2, 4, get_supplier_name($row['supplier_id']));
		$rep->TextCol(4, 5, get_chk_date($row['trans_no']));
		$rep->TextCol(5, 6, $row['ewt'] < 0 ? number_format2($row['ewt'] * -1,2) : "");
		$rep->NewLine();
		
		$total += $row['ewt'];
	}
	
	$rep->Font('bold');	
	$rep->Line($rep->row + 6);
	$rep->row -= 6;
	$rep->TextCol(0, 4,	_('Total '));
	$rep->TextCol(5, 6, number_format2($total * -1,2));
	$rep->NewLine();
	$rep->Font('');	
	
    $rep->End();
}

?>