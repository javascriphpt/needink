<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_inactive_customer_branch();

function print_inactive_customer_branch()
{
    global $path_to_root, $systypes_array;

    $comments = $_POST['PARAM_0'];
	$destination = $_POST['PARAM_1'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$cols = array(0, 90, 155, 215, 270, 340, 460, 580, 620, 675, 735);

	$headers = array(_('Short Name'), _('Name'), _('Address'), _('Contact'), _('Salesperson'), _('Area'), _('Phone #'), _('Fax #'), _('Email'), _('Tax Group'));

	$aligns = array('left',	'left',	'left',	'left',	'left', 'left', 'left', 'left', 'left');

    $params =   array( 	0 => '');

    $rep = new FrontReport(_('Inactive Customer Branch List'), "InacticeCustomerBranchList", user_pagesize(), 9, 'L');

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$sql = "SELECT b.*, s.salesman_name, a.description, t.name AS tax_group_name
		FROM ".TB_PREF."cust_branch b, "
			.TB_PREF."debtors_master c, "
			.TB_PREF."areas a, "
			.TB_PREF."salesman s, "
			.TB_PREF."tax_groups t
		WHERE b.debtor_no=c.debtor_no
		AND b.tax_group_id=t.id
		AND b.area=a.area_code
		AND b.salesman=s.salesman_code
		AND b.inactive = 1";
	$result = db_query($sql, "The inactive customer branch could not be retrieved");

	while ($myrow = db_fetch($result))
	{
		set_time_limit(0);
		
		$rep->TextCol(0, 1, $myrow['branch_ref']);
		$rep->TextCol(1, 2, $myrow['br_name']);
		$rep->TextCol(2, 3, $myrow['br_address']);
		$rep->TextCol(3, 4, $myrow['contact_name']);
		$rep->TextCol(4, 5, $myrow['salesman_name']);
		$rep->TextCol(5, 6, $myrow['description']);
		$rep->TextCol(6, 7, $myrow['phone']." ".$myrow['phone2']);
		$rep->TextCol(7, 8, $myrow['fax']);
		$rep->TextCol(8, 9, $myrow['email']);
		$rep->TextCol(9, 10, $myrow['tax_group_name']);
			
		$rep->NewLine();
	}
	
	$rep->Line($rep->row - 4);
	$rep->NewLine();
	
	$rep->End();
}

?>