<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_delivery_receipt_and_collection_report();

function get_total_amount($from, $to, $salesman, $type)
{
	$from = date2sql($from);
	$to = date2sql($to);
	
	$sql = "SELECT a.type, c.salesman_name, a.location, 
					SUM(ov_amount+ov_discount+ov_freight+ov_freight_tax+ov_gst+ewt) as total, 
					SUM(alloc) as alloc
			FROM ".TB_PREF."debtor_trans a
			JOIN ".TB_PREF."cust_branch b ON a.branch_code=b.branch_code
			JOIN ".TB_PREF."salesman c ON b.salesman=c.salesman_code
			WHERE a.type =  ".db_escape($type)."
			AND a.tran_date >= '$from'
			AND a.tran_date <= '$to'
			AND c.salesman_code = ".db_escape($salesman)."
			GROUP BY a.type, c.salesman_code
			ORDER BY c.salesman_code, a.location";
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[3];
}

//----------------------------------------------------------------------------------------------------

function print_delivery_receipt_and_collection_report()
{
    global $path_to_root, $systypes_array;

    $from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $comments = $_POST['PARAM_2'];
	$destination = $_POST['PARAM_3'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$cols = array(0, 90, 250, 350);

	$headers = array(_(""), _(""), _('Delivery Receipt'), _('Collection'));

	$aligns = array('left',	'left',	'left',	'left');
			
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to));

    $rep = new FrontReport(_('Delivery Receipt and Collection Report'), "DeliveryReceiptAndCollectionReport", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$sql = "SELECT salesman_code, salesman_name 
			FROM ".TB_PREF."salesman
			ORDER BY salesman_name";
	$result = db_query($sql, "The salesperson could not be retrieved");

	while ($myrow = db_fetch($result))
	{
		$rep->TextCol(0, 2, $myrow["salesman_name"]);
		$rep->TextCol(2, 3, number_format2(get_total_amount($from, $to, $myrow['salesman_code'], 13), 2));
		$rep->TextCol(3, 4, number_format2(get_total_amount($from, $to, $myrow['salesman_code'], 12), 2));
		$rep->NewLine();
	}
	
    $rep->End();
}

?>