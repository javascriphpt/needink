<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_MANUFTRANSVIEW';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Bill Of Material
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/inventory/includes/db/items_db.inc");

//----------------------------------------------------------------------------------------------------

print_finished_workorders();

function getTransactions($from, $to, $type, $items)
{
	$from = date2sql($from);
	$to = date2sql($to);
	
	if($type == WO_ADVANCED)
	{
		$sql = "SELECT ".TB_PREF."workorders.form_type_no as reference, 
						SUM(".TB_PREF."wo_manufacture.quantity) as qty, 
						".TB_PREF."workorders.id as trans_no, ".TB_PREF."workorders.type,
						".TB_PREF."workorders.stock_id, ".TB_PREF."stock_master.description
				FROM ".TB_PREF."workorders, ".TB_PREF."wo_manufacture, ".TB_PREF."stock_master
				WHERE ".TB_PREF."workorders.id = ".TB_PREF."wo_manufacture.workorder_id
				AND ".TB_PREF."stock_master.stock_id=".TB_PREF."workorders.stock_id
				AND ".TB_PREF."workorders.type = ".$type."
				AND ".TB_PREF."wo_manufacture.date_ >= '$from'
				AND ".TB_PREF."wo_manufacture.date_ <= '$to' ";
		if($items != '')
			$sql .= " AND ".TB_PREF."workorders.stock_id = ".db_escape($items);
		
		$sql .= " GROUP BY ".TB_PREF."workorders.id
				ORDER BY ".TB_PREF."workorders.form_type_no"; 	
	}
	else
	{
		$sql = "SELECT ".TB_PREF."workorders.form_type_no as reference, 
						SUM(".TB_PREF."wo_manufacture.quantity) as qty, 
						".TB_PREF."workorders.id as trans_no, ".TB_PREF."workorders.type,
						".TB_PREF."wo_requirements.stock_id, ".TB_PREF."stock_master.description
				FROM ".TB_PREF."workorders, ".TB_PREF."wo_manufacture, ".TB_PREF."stock_master, ".TB_PREF."wo_requirements
				WHERE ".TB_PREF."workorders.id = ".TB_PREF."wo_manufacture.workorder_id
				AND ".TB_PREF."workorders.id = ".TB_PREF."wo_requirements.workorder_id
				AND ".TB_PREF."stock_master.stock_id=".TB_PREF."wo_requirements.stock_id
				AND ".TB_PREF."workorders.type = ".$type."
				AND ".TB_PREF."wo_manufacture.date_ >= '$from'
				AND ".TB_PREF."wo_manufacture.date_ <= '$to' ";
		if($items != '')
			$sql .= " AND ".TB_PREF."wo_requirements.stock_id = ".db_escape($items);
		
		$sql .= " GROUP BY ".TB_PREF."workorders.id
				ORDER BY ".TB_PREF."workorders.form_type_no"; 
	}
		
    return db_query($sql,"No transactions were returned");

}

function getTransactions_summary($from, $to, $type, $items)
{
	$from = date2sql($from);
	$to = date2sql($to);
	
	if($type == WO_ADVANCED)
	{
		$sql = "SELECT ".TB_PREF."workorders.form_type_no as reference, 
						SUM(".TB_PREF."wo_manufacture.quantity) as qty, 
						".TB_PREF."workorders.id as trans_no, ".TB_PREF."workorders.type,
						".TB_PREF."workorders.stock_id, ".TB_PREF."stock_master.description
				FROM ".TB_PREF."workorders, ".TB_PREF."wo_manufacture, ".TB_PREF."stock_master
				WHERE ".TB_PREF."workorders.id = ".TB_PREF."wo_manufacture.workorder_id
				AND ".TB_PREF."stock_master.stock_id=".TB_PREF."workorders.stock_id
				AND ".TB_PREF."workorders.type = ".$type."
				AND ".TB_PREF."wo_manufacture.date_ >= '$from'
				AND ".TB_PREF."wo_manufacture.date_ <= '$to' ";
		if($items != '')
			$sql .= " AND ".TB_PREF."workorders.stock_id = ".db_escape($items);
		
		$sql .= " GROUP BY ".TB_PREF."workorders.stock_id
				ORDER BY ".TB_PREF."workorders.form_type_no"; 	
	}
	else
	{
		$sql = "SELECT ".TB_PREF."workorders.form_type_no as reference, 
						SUM(".TB_PREF."wo_manufacture.quantity) as qty, 
						".TB_PREF."workorders.id as trans_no, ".TB_PREF."workorders.type,
						".TB_PREF."wo_requirements.stock_id, ".TB_PREF."stock_master.description
				FROM ".TB_PREF."workorders, ".TB_PREF."wo_manufacture, ".TB_PREF."stock_master, ".TB_PREF."wo_requirements
				WHERE ".TB_PREF."workorders.id = ".TB_PREF."wo_manufacture.workorder_id
				AND ".TB_PREF."workorders.id = ".TB_PREF."wo_requirements.workorder_id
				AND ".TB_PREF."stock_master.stock_id=".TB_PREF."wo_requirements.stock_id
				AND ".TB_PREF."workorders.type = ".$type."
				AND ".TB_PREF."wo_manufacture.date_ >= '$from'
				AND ".TB_PREF."wo_manufacture.date_ <= '$to' ";
		if($items != '')
			$sql .= " AND ".TB_PREF."wo_requirements.stock_id = ".db_escape($items);
		
		$sql .= " GROUP BY ".TB_PREF."wo_requirements.stock_id
				ORDER BY ".TB_PREF."workorders.form_type_no"; 
	}
		
    return db_query($sql,"No transactions were returned");

}

function get_wo_comments_string($type, $type_no)
{
	$sql = "SELECT * FROM ".TB_PREF."comments WHERE type="
		.db_escape($type)." AND id=".db_escape($type_no);

	$result = db_query($sql, "could not query comments transaction table");

	$str_return = "";
	while ($comment = db_fetch($result))
	{
		if (strlen($str_return))
			$str_return = $str_return . " \n";
		$str_return = $str_return . $comment["memo_"];
	}
	
	return $str_return;
}

function get_stock_description($type_no)
{
	$sql = "SELECT CONCAT(".TB_PREF."wo_requirements.stock_id, ' - ', ".TB_PREF."stock_master.description) as description 
			FROM ".TB_PREF."wo_requirements, ".TB_PREF."stock_master 
			WHERE ".TB_PREF."wo_requirements.workorder_id=".db_escape($type_no)."
			AND ".TB_PREF."stock_master.stock_id=".TB_PREF."wo_requirements.stock_id";

	$result = db_query($sql, "could not query wo_requirements transaction table");

	$str_return = "";
	while ($row = db_fetch($result))
	{
		if (strlen($str_return))
			$str_return = $str_return . " \n";
		$str_return = $str_return . $row["description"];
	}
	
	return $str_return;
}

function get_item_description($stock_id)
{
	$sql = "SELECT description FROM ".TB_PREF."stock_master WHERE stock_id=".db_escape($stock_id);
	$result = db_query($sql, "could not retrieve item name");

	$row = db_fetch_row($result);
	return $row[0];
}

//----------------------------------------------------------------------------------------------------

function print_finished_workorders()
{
    global $path_to_root, $wo_types_array;

	$from = $_POST['PARAM_0'];
	$to = $_POST['PARAM_1'];
    $type = $_POST['PARAM_2'];
    $items = $_POST['PARAM_3'];
	$summaryOnly = $_POST['PARAM_4'];
    $comments = $_POST['PARAM_5'];
	$destination = $_POST['PARAM_6'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$type_name = $wo_types_array[$type];
	
	if($items == '')
		$item_name = "All Items";
	else
		$item_name = get_item_description($items);
		
	if ($summaryOnly == 1)
		$summary = _('Summary Only');
	else
		$summary = _('Detailed Report');
	
	$cols = array(0, 70, 130, 320, 550);
	$headers = array(_('JO #'), _('Qty'), _('Assigned Person'), _('Model'));
	$aligns = array('left',	'left',	'left',	'left');
		
    $params =   array( 	0 => $comments,
						1 => array('text' => _('Period'),'from' => $from, 'to' => $to),
						2 => array('text' => _('Type'), 'from' => $type_name, 'to' => ''),
						3 => array('text' => _('Item(s)'), 'from' => $item_name, 'to' => ''),
						4 => array('text' => _('Report Type'), 'from' => $summary,'to' => ''));

    $rep = new FrontReport(_('Finished Work Orders'), "FinishedWorkOrders", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	if (!$summaryOnly)
		$res = getTransactions($from, $to, $type, $items);
	else
		$res = getTransactions_summary($from, $to, $type, $items);
	
	$voided = "";
	while ($trans=db_fetch($res))
	{
		set_time_limit(0);
		
		if (get_voided_entry(ST_WORKORDER, $trans['trans_no']) === true)
			$voided = " - Voided";
	
		$rep->TextCol(0, 1, $summaryOnly == 0 ? $trans['reference'] : '');
		$rep->TextCol(1, 2, $trans['qty']);
		$rep->TextCol(2, 3, $summaryOnly == 0 ? get_wo_comments_string(ST_WORKORDER, $trans['trans_no']) .$voided : '');
		$rep->TextCol(3, 4, $trans['stock_id']." - ".$trans['description']);
			
		$rep->NewLine();
	}
	
	$rep->Line($rep->row - 4);
	$rep->NewLine();
	
	
    $rep->End();
}

?>