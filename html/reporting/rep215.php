<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPPAYMREP';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Payment Report
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/ui/ui_lists.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

print_payables_report();

function getTransactions($supplier, $from, $to)
{
	$from = date2sql($from);
	$to = date2sql($to);
	$dec = user_price_dec();

	$sql = "SELECT ".TB_PREF."supp_trans.supp_reference,
			".TB_PREF."supp_trans.tran_date,
			".TB_PREF."supp_trans.due_date,
			".TB_PREF."supp_trans.trans_no,
			".TB_PREF."supp_trans.reference,
			".TB_PREF."supp_trans.type,
			".TB_PREF."supp_trans.ov_amount,
			".TB_PREF."supp_trans.ov_gst,
			(ABS(".TB_PREF."supp_trans.ov_amount) + ABS(".TB_PREF."supp_trans.ov_gst) + ABS(".TB_PREF."supp_trans.ewt) - ".TB_PREF."supp_trans.alloc) AS Balance,
			(ABS(".TB_PREF."supp_trans.ov_amount) + ABS(".TB_PREF."supp_trans.ov_gst) + ABS(".TB_PREF."supp_trans.ewt) ) AS TranTotal
		FROM ".TB_PREF."supp_trans
		WHERE ".TB_PREF."supp_trans.supplier_id = '" . $supplier . "'
		AND ROUND(ABS(".TB_PREF."supp_trans.ov_amount),$dec) + ROUND(ABS(".TB_PREF."supp_trans.ov_gst),$dec) + ROUND(ABS(".TB_PREF."supp_trans.ewt),$dec) - 
		ROUND(".TB_PREF."supp_trans.alloc,$dec) != 0
		AND ".TB_PREF."supp_trans.tran_date >= '$from' 
		AND ".TB_PREF."supp_trans.tran_date <= '$to' 
		AND (".TB_PREF."supp_trans.type = ".ST_SUPPINVOICE." OR ".TB_PREF."supp_trans.type = ".ST_BANKDEPOSIT.")

		ORDER BY ".TB_PREF."supp_trans.type,
			".TB_PREF."supp_trans.trans_no";

    return db_query($sql, "No transactions were returned");
}

function getRrNo($trans_no, $type)
{
	$from = date2sql($from);
	$to = date2sql($to);
	$dec = user_price_dec();

	$sql = "SELECT c.grn_batch_id, b.*
		FROM ".TB_PREF."supp_trans AS a
		JOIN ".TB_PREF."supp_invoice_items AS b on a.trans_no = b.supp_trans_no AND a.type = b.supp_trans_type
		JOIN ".TB_PREF."grn_items AS c on b.grn_item_id = c.id
		WHERE a.type = ".db_escape($type)." 
		AND a.trans_no = ".db_escape($trans_no)."
		ORDER BY b.id desc LIMIT 1";

    $res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

//----------------------------------------------------------------------------------------------------

function print_payables_report()
{
    global $path_to_root, $systypes_array;

    $from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $fromsupp = $_POST['PARAM_2'];
    $comments = $_POST['PARAM_3'];
	$destination = $_POST['PARAM_4'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	if ($fromsupp == ALL_NUMERIC)
		$supp = _('All');
	else
		$supp = get_supplier_name($fromsupp);

    $dec = user_price_dec();

	$cols = array(0, 80, 160, 240, 290, 370, 450, 505);

	$headers = array(_('TIN#'), _('#'), _("Supplier's Ref"), _("RR #"), _('Date'), _('Sub-total'), _('Vat'), _('Total Amount'));

	$aligns = array('left',	'left',	'left',	'left',	'left', 'left', 'left', 'left');

    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to),
    				    2 => array('text' => _('Supplier'), 'from' => $supp, 'to' => ''));

    $rep = new FrontReport(_('Monthly Summary of Purchases Report'), "MonthlySummaryOfPurchasesReport", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$total = array();
	$grandtotal = array(0,0);

	$sql = "SELECT supplier_id, supp_name AS name, curr_code, gst_no, ".TB_PREF."payment_terms.terms FROM ".TB_PREF."suppliers, ".TB_PREF."payment_terms
		WHERE ";
	if ($fromsupp != ALL_NUMERIC)
		$sql .= "supplier_id=".db_escape($fromsupp)." AND ";
	$sql .= "".TB_PREF."suppliers.payment_terms = ".TB_PREF."payment_terms.terms_indicator
		ORDER BY supp_name";
	$result = db_query($sql, "The customers could not be retrieved");

	$grandtotal = 0;
	while ($myrow=db_fetch($result))
	{
		$rep->fontSize += 2;
		$rep->TextCol(0, 7, $myrow['name']);
		$rep->fontSize -= 2;
		$rep->NewLine(1, 2);
		
		$res = getTransactions($myrow['supplier_id'], $from, $to);
		if (db_num_rows($res)==0)
			continue;
		$rep->Line($rep->row + 4);
		
		$total = 0;
		while ($trans=db_fetch($res))
		{
			$rr_no = getRrNo($trans['trans_no'], $trans['type']);
			
			$rep->NewLine(1, 2);
			$rep->TextCol(0, 1,	$myrow['gst_no']);
			$rep->TextCol(1, 2,	$trans['reference']);
			$rep->TextCol(2, 3,	$trans['supp_reference']);
			$rep->TextCol(3, 4,	get_reference_no($rr_no, ST_SUPPRECEIVE));
			$rep->DateCol(4, 5,	$trans['tran_date'], true);
			$rep->AmountCol(5, 6, $trans['ov_amount'], $dec);
			$rep->AmountCol(6, 7, $trans['ov_gst'], $dec);
			$rep->AmountCol(7, 8, $trans['TranTotal'], $dec);
			$total += $trans['TranTotal'];
			
		}
		$rep->Line($rep->row - 8);
		$rep->NewLine(2);
		$rep->TextCol(0, 3,	_('Total'));
		$rep->AmountCol(7, 8, $total, $dec);
    	$rep->Line($rep->row  - 4);
    	$rep->NewLine(2);
		
		$grandtotal += $total;
	}
	
	$rep->fontSize += 2;
	$rep->TextCol(0, 3,	_('Grand Total'));
	$rep->fontSize -= 2;
	$rep->AmountCol(7, 8, $grandtotal, $dec);
	$rep->Line($rep->row  - 4);
	$rep->NewLine();
	
    $rep->End();
}

?>