<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_UNUSEDREF';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_unused_ref_report();

function getTransactions($location, $from, $to)
{
	$from = date2sql($from);
	$to = date2sql($to);
	
	$sql = "SELECT ".TB_PREF."debtor_trans.*,
		(".TB_PREF."debtor_trans.ov_amount + ".TB_PREF."debtor_trans.ov_gst + ".TB_PREF."debtor_trans.ov_freight + 
		".TB_PREF."debtor_trans.ov_freight_tax + ".TB_PREF."debtor_trans.ov_discount + ".TB_PREF."debtor_trans.ewt)
		AS TotalAmount, ".TB_PREF."debtor_trans.alloc AS Allocated
    	FROM ".TB_PREF."debtor_trans
    	WHERE ".TB_PREF."debtor_trans.tran_date >= '$from'
		AND ".TB_PREF."debtor_trans.tran_date <= '$to'
		AND ".TB_PREF."debtor_trans.location = ".db_escape($location)."
		AND ".TB_PREF."debtor_trans.type = 12
    	ORDER BY ".TB_PREF."debtor_trans.tran_date";
	
	return db_query($sql,"No transactions were returned");

}

function get_sales_ref_no_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_sales_ref_no_OR_PR_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location, form_type
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_so_form_cat_name_report($cat_id)
{
	$sql = "SELECT name FROM ".TB_PREF."sales_form_category 
			WHERE id=".db_escape($cat_id);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

//----------------------------------------------------------------------------------------------------

function print_unused_ref_report()
{
    global $path_to_root, $systypes_array;

    $type = $_POST['PARAM_0'];
    $location = $_POST['PARAM_1'];
    $comments = $_POST['PARAM_2'];
	$destination = $_POST['PARAM_3'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$cols = array(0, 70, 150, 300, 370, 430, 530);

	$headers = array(_('From'), _(''), _("To"));

	$aligns = array('left',	'left',	'left',	'left',	'right', 'right');
	
	if ($location == ALL_TEXT)
		$location = 'all';
	if ($location == 'all')
		$loc = _('All');
	else
		$loc = get_location_name($location);
		
	if($type == 0)
	{
		$type_name = 'Invoice';
		$type_ref = 10;
	}
	else if($type == 1)
	{
		$type_name = 'OR';
		$type_ref = 12;
		$type_ref_cat = 2;
	}
	else
	{
		$type_name = 'PR';
		$type_ref = 12;
		$type_ref_cat = 1;
	}
		
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Type'), 'from' => $type_name, 'to' => ''),
    				    2 => array('text' => _('Location'), 'from' => $loc, 'to' => ''));

    $rep = new FrontReport(_('Unused Reference # Report'), "UnusedRefReport", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	$sql = "SELECT loc_code, location_name 
			FROM ".TB_PREF."locations ";
	if ($location != 'all')
		$sql .= " WHERE loc_code=".db_escape($location);
	$sql .= " ORDER BY location_name";
	$result = db_query($sql, "The location could not be retrieved");

	$loc_ = '';	
	while ($myrow = db_fetch($result))
	{
		if ($loc_ != $myrow['location_name'])
		{
			$rep->NewLine();
			$rep->fontSize += 2;
			$rep->TextCol(0, 2, $myrow['location_name']);
			$rep->fontSize -= 2;
			$rep->NewLine();
		}
		
		$sql = "SELECT 
			  CAST(a.reference AS SIGNED)+1 AS 'Missing From', 
			  MIN(CAST(b.reference AS SIGNED)) - 1 AS 'To' 
			FROM 0_refs_so_forms AS a, 0_refs_so_forms AS b 
			WHERE CAST(a.reference AS SIGNED) < CAST(b.reference AS SIGNED)
			AND a.type = ".db_escape($type_ref)."
			AND b.type = ".db_escape($type_ref)."
			AND a.loc_code = ".db_escape($myrow['loc_code'])."
			AND b.loc_code = ".db_escape($myrow['loc_code'])." ";
		
		if ($type != 0)
			$sql .= " AND a.cat_id=".db_escape($type_ref_cat)." AND b.cat_id=".db_escape($type_ref_cat);	
	
		$sql .= " GROUP BY CAST(a.reference AS SIGNED)
			HAVING `Missing From` < MIN(CAST(b.reference AS SIGNED)) ";
		
		$res = db_query($sql, "unused ref # could not be retrieved");
		while ($row = db_fetch($res))
		{
			$rep->TextCol(0, 1, $row['Missing From']);
			$rep->TextCol(2, 3, $row['To']);
			$rep->NewLine();
			
			$total += $row['TotalAmount'];
		}
	}
	
	
    $rep->End();
}

?>