<?php

//$page_security = 2;
$page_security = 'SA_MANUFTRANSVIEW';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Stock Check
// ----------------------------------------------------------------
$path_to_root="../";

include_once($path_to_root . "includes/session.inc");
include_once($path_to_root . "includes/date_functions.inc");
include_once($path_to_root . "includes/data_checks.inc");


//----------------------------------------------------------------------------------------------------

print_workorder();

//----------------------------------------------------------------------------------------------------

function print_workorder()
{
    global $comp_path, $path_to_root, $pic_height, $pic_width;

    include_once($path_to_root . "reporting/includes/pdf_report.inc");

    $trans_no = $_GET['trans_no'];
	
    $rep = new FrontReport(_('Work Order'), "PrintWorkOrder.pdf", user_pagesize());
	
	$rep->fontSize += 2;
    $rep->Font();
    $rep->Info($params, '', '', '');
    $rep->Header();
	
	$myrow = get_work_order($trans_no);
	
	$rep->Font('bold');
	$rep->TextWrap(30,$rep->row, 280, 'Work Order # : ');
	$rep->Font('');
	$rep->TextWrap(30+90,$rep->row, 190, $myrow["id"]);
	
	$rep->NewLine(1.3);
	
	$rep->Font('bold');
	$rep->TextWrap(30,$rep->row, 280, 'Item : ');
	$rep->Font('');
	$rep->TextWrap(30+40,$rep->row, 500, $myrow["stock_id"]." - ".$myrow["StockItemName"]);
	
	$rep->NewLine(1.3);
	
	$rep->Font('bold');
	$rep->TextWrap(30,$rep->row, 280, 'Location : ');
	$rep->Font('');
	$rep->TextWrap(30+60,$rep->row, 400, $myrow["location_name"]);
	
	$rep->NewLine(1.3);
	
	$rep->Font('bold');
	$rep->TextWrap(30,$rep->row, 280, 'Date : ');
	$rep->Font('');
	$rep->TextWrap(30+40,$rep->row, 190,sql2date($myrow["date_"]));
	
	$rep->Font('bold');
	$rep->TextWrap(30+350,$rep->row, 210, 'Required By : ');
	$rep->Font('');
	$rep->TextWrap(30+350+80,$rep->row, 210, sql2date($myrow["required_by"]));
	
	$rep->NewLine(1.3);
	
	$rep->Font('bold');
	$rep->TextWrap(30,$rep->row, 280, 'Quantity Required : ');
	$rep->Font('');
	$rep->TextWrap(30+110,$rep->row, 190, $myrow["units_reqd"]);
	
	$rep->Font('bold');
	$rep->TextWrap(30+350,$rep->row, 210, 'Manufactured : ');
	$rep->Font('');
	$rep->TextWrap(30+350+90,$rep->row, 210, $myrow["units_issued"]);
	
	$rep->fontSize -= 2;
	
	$rep->NewLine(1.3);
	$rep->Line($rep->row-8, 2);
	$rep->NewLine(2.3);
	
	$rep->Font('bold');
	$rep->TextWrap(30,$rep->row, 280, 'Component');
	$rep->TextWrap(30+160,$rep->row, 280, 'From Location ');
	$rep->TextWrap(30+150+140,$rep->row, 280, 'Work Centre');
	$rep->TextWrap(30+150+130+90,$rep->row, 280, 'Unit Quantity');
	$rep->TextWrap(30+150+130+70+100,$rep->row, 280, 'Unit Issued');
	$rep->TextWrap(30+150+130+70+100+70,$rep->row, 280, 'On Hand');
	$rep->Font('');
	
	$result = get_wo_requirements($trans_no);
	
	
	while ($row = db_fetch($result))
    {
		$qoh = 0;
		$show_qoh = true;
		$date = Today();
		
		if ($show_qoh)
			$qoh = get_qoh_on_date($row["stock_id"], $row["loc_code"], $date);
	
		$rep->NewLine(1);
		$rep->TextWrap(30,$rep->row, 150, $row["stock_id"]. " - " . $row["description"]);
		$rep->TextWrap(30+160,$rep->row, 100, $row["location_name"]);
		$rep->TextWrap(30+150+140,$rep->row, 100, $row["WorkCentreDescription"]);
		$rep->TextWrap(30+150+130+90,$rep->row, 100, $row["units_req"]);
		$rep->TextWrap(30+150+130+70+100,$rep->row, 100, $row["units_issued"]);
		$rep->TextWrap(30+150+130+70+100+70,$rep->row, 100, $qoh);
	}
			
    $rep->End();
}

?>