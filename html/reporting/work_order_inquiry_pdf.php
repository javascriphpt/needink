<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_MANUFTRANSVIEW';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/includes/ui/ui_lists.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_work_order_inquiry();

function get_transactions($OrderNumber, $StockLocation, $type, $OverdueOnly, $OpenOnly, $SelectedStockItem, $outstanding_only)
{
	$sql = "SELECT
			workorder.id,	
			workorder.type,
			location.location_name,
			item.description,
			workorder.units_reqd,
			workorder.units_issued,
			workorder.date_,
			workorder.required_by,
			workorder.released_date,
			workorder.closed,
			workorder.released,
			workorder.stock_id,
			unit.decimals,
			workorder.wo_ref,
			workorder.form_type_no,
			workorder.released
			FROM ".TB_PREF."workorders as workorder,"
				.TB_PREF."stock_master as item,"
				.TB_PREF."item_units as unit,"
				.TB_PREF."locations as location
			WHERE workorder.stock_id=item.stock_id 
				AND workorder.loc_code=location.loc_code
				AND item.units=unit.abbr";

		if ($OpenOnly != 0 || $outstanding_only != 0)
			$sql .= " AND workorder.closed=0";
		
		if ($StockLocation != 0)
			$sql .= " AND workorder.loc_code=".db_escape($StockLocation);
		
		if ($type != 0)
			$sql .= " AND workorder.type=".db_escape($type);
		
		if ($OrderNumber != 0)
			$sql .= " AND workorder.form_type_no LIKE ".db_escape('%'.$OrderNumber.'%');
		
		if ($SelectedStockItem != 0)
			$sql .= " AND workorder.stock_id=".db_escape($SelectedStockItem);
		
		if ($OverdueOnly != 0)
		{
			$Today = date2sql(Today());

			$sql .= " AND workorder.required_by < '$Today' ";
		}

    return db_query($sql,"No transactions were returned");
}

function get_wo_comments_string($type, $type_no)
{
	$sql = "SELECT * FROM ".TB_PREF."comments WHERE type="
		.db_escape($type)." AND id=".db_escape($type_no);

	$result = db_query($sql, "could not query comments transaction table");

	$str_return = "";
	while ($comment = db_fetch($result))
	{
		if (strlen($str_return))
			$str_return = $str_return . " \n";
		$str_return = $str_return . $comment["memo_"];
	}
	
	return $str_return;
}

//----------------------------------------------------------------------------------------------------

function print_work_order_inquiry()
{
    global $path_to_root, $SysPrefs, $wo_types_array;

	include_once($path_to_root . "/reporting/includes/pdf_report.inc");
	
	$OrderNumber = $_GET['OrderNumber'];
	$StockLocation = $_GET['StockLocation'];
	$type = $_GET['type'];
	$OverdueOnly = $_GET['OverdueOnly'];
	$OpenOnly = $_GET['OpenOnly'];
	$SelectedStockItem = $_GET['SelectedStockItem'];
	$outstanding_only = $_GET['outstanding_only'];
	$output = $_GET['output'];
		
	if ($output == 0)
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/excel_report.inc");

	
	$cols = array(0, 35, 80, 205, 400, 470, 510, 590, 660, 730, 770);

	$headers = array(_('#'), _('Type'), _('Location'), _('Item'), _('Assigned Person'), _('Required'), _('Manufactured'), _('Date'), _('Required By'));

	$aligns = array('left',	'left',	'left',	'left',	'left',	'right', 'right', 'right', 'right');

    $params =   array( 	0 => '');

    $rep = new FrontReport(_('Work Order Inquiry'), "WorkOrderInquiry", user_pagesize(), 7.5, 'L');

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	$res = get_transactions($OrderNumber, $StockLocation, $type, $OverdueOnly, $OpenOnly, $SelectedStockItem, $outstanding_only);
	
	while ($myrow = db_fetch($res))
	{
		$rep->TextCol(0, 1, $myrow["form_type_no"]);
		$rep->TextCol(1, 2, $wo_types_array[$myrow["type"]]);
		$rep->TextCol(2, 3, $myrow["location_name"]);
		$rep->TextCol(3, 4, $myrow["stock_id"]." - ".$myrow["description"]);
		$rep->TextCol(4, 5, get_wo_comments_string(ST_WORKORDER, $myrow['id']));
		$rep->TextCol(5, 6, $myrow["units_reqd"]);
		$rep->TextCol(6, 7, $myrow["units_issued"]);
		$rep->TextCol(7, 8, $myrow["date_"]);
		$rep->TextCol(8, 9, $myrow["required_by"]);
		
		$rep->NewLine();
	}

    $rep->End();
}

?>