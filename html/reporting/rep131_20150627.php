<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_sup_list();

function print_sup_list()
{
    global $path_to_root, $systypes_array;

    $sup_type = $_POST['PARAM_0'];
    $degree_of_service = $_POST['PARAM_1'];
    $comments = $_POST['PARAM_2'];
	$destination = $_POST['PARAM_3'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$cols = array(0, 90, 300, 500);

	$headers = array(_('SUP No.'), _('Company Name'), _('System/Unit'));

	$aligns = array('left',	'left',	'left');
	
	if($degree_of_service == 1)
		$sup_type_text = 'In-house';
	else
		$sup_type_text = 'Field';
	
	if($degree_of_service == 1)
		$degree_of_service_text = 'LEVEL 1';
	else if($degree_of_service == 2)
		$degree_of_service_text = 'LEVEL 2';
	else
		$degree_of_service_text = 'LEVEL 3';

    $params = array( 	0 => $comments,
    				    1 => array('text' => _('Type'), 'from' => $sup_type_text, 'to' => ''),
    				    2 => array('text' => _('Degree of Service'), 'from' => $degree_of_service_text, 'to' => ''));

    $rep = new FrontReport(_('SUP List'), "SUPList", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$sql = "SELECT sup.sup_no,
			sup.company_name,
			sup.system_unit,
			sup.serial_no,
			sup.assigned_tsg,
			sup.conformed, 
			completed_date, sup.id
		FROM ".TB_PREF."sup as sup
		WHERE sup.degree_of_service=".db_escape($degree_of_service)."
		AND type=".db_escape($sup_type);
	$result = db_query($sql, "The sup could not be retrieved");
display_error($sql);
	while ($myrow = db_fetch($result))
	{
		set_time_limit(0);
		
		$rep->TextCol(0, 1, $myrow['sup_no']);
		$rep->TextCol(1, 2, $myrow['company_name']);
		$rep->TextCol(2, 3, $myrow['system_unit']);
			
		$rep->NewLine();
	}
	
	$rep->Line($rep->row - 4);
	$rep->NewLine();
	
	$rep->End();
}

?>