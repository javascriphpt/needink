<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTPAYMREP';

// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Customer Balances
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

// trial_inquiry_controls();
print_sales_report();

function getTransactions($location, $from, $to, $fromcust)
{
	// all printers only
	$sql = "SELECT trans.debtor_no, trans.branch_code, trans.trans_no, trans.type, trans.tran_date, 
			isn.stock_id, isn.isn, stock.description
		FROM ".TB_PREF."debtor_trans as trans, ".TB_PREF."isn_trans as isn, 
			".TB_PREF."stock_master as stock, ".TB_PREF."stock_category as category
		WHERE trans.type = ".ST_CUSTDELIVERY."
		AND trans.trans_no = isn.trans_no
		AND trans.type = isn.trans_type
		AND isn.stock_id = stock.stock_id
		AND stock.category_id = category.category_id
		AND category.category_id = 2
		AND isn.isn != '' ";
	
	if ($fromcust != ALL_NUMERIC)
		$sql .= " AND trans.debtor_no=".db_escape($fromcust);
	
	$sql .= " AND trans.tran_date >= '".date2sql($from)."'
		AND trans.tran_date <= '".date2sql($to)."'
		AND trans.location = ".db_escape($location)."";

	return db_query($sql,"No transactions were returned");

}

function get_sales_ref_no_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_aic_out_report($trans_no, $trans_type, $stock_id, $loc_code){
	$sql = "SELECT aic FROM ".TB_PREF."isn_trans a, ".TB_PREF."isn_gen b
		WHERE a.trans_no = ".$trans_no." AND a.trans_type=".$trans_type." AND a.isn=b.isn
		AND a.stock_id=".db_escape($stock_id)." AND a.loc_code = ".db_escape($loc_code);

	$result = db_query($sql,"could not retreive name for branch" . $branch_id);

	$myrow = db_fetch_row($result);	
	return $myrow[0];
}

//----------------------------------------------------------------------------------------------------

function print_sales_report()
{
    global $path_to_root, $systypes_array;

    $from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
	$fromcust = $_POST['PARAM_2'];
    $location = $_POST['PARAM_3'];
    $comments = $_POST['PARAM_4'];
	$destination = $_POST['PARAM_5'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$cols = array(0, 90, 150, 250, 350, 450, 505, 620);

	$headers = array(_('Customer Name'), _('Branch'), _("Printer Model"), _('AIC No.'), _('Serial No.'), _('DR No.'), _('Date Delivered'));

	$aligns = array('left',	'left',	'left',	'left',	'left',	'left',	'left');
	
	if ($fromcust == ALL_NUMERIC)
		$cust = _('All');
	else
		$cust = get_customer_name($fromcust);
	
	if ($location == ALL_TEXT)
		$location = 'all';
	if ($location == 'all')
		$loc = _('All');
	else
		$loc = get_location_name($location);
		
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to),
    				    2 => array('text' => _('Location'), 'from' => $loc, 'to' => ''));

    $rep = new FrontReport(_('Service Units Deployed Report'), "ServiceUnitsDeployedReport", user_pagesize(), 8);

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();

	$sql = "SELECT DISTINCT loc.loc_code, loc.location_name 
			FROM ".TB_PREF."locations loc, ".TB_PREF."isn_trans trans, ".TB_PREF."debtor_trans debtor
			WHERE loc.loc_code = trans.loc_code
			AND trans.trans_type = 13
			AND trans.trans_type = debtor.type
			AND trans.trans_no = debtor.trans_no
			AND trans.date >= '".date2sql($from)."'
			AND trans.date <= '".date2sql($to)."'";
	if ($location != 'all')
		$sql .= "AND trans.loc_code=".db_escape($location);
	if ($fromcust != ALL_NUMERIC)
		$sql .= " AND debtor.debtor_no=".db_escape($fromcust);
	$sql .= " ORDER BY loc.location_name";
	$result = db_query($sql, "The location could not be retrieved");

	$loc_ = '';
	while ($myrow = db_fetch($result))
	{
		if ($loc_ != $myrow['location_name'])
		{
			$rep->NewLine();
			$rep->fontSize += 2;
			$rep->TextCol(0, 4, $myrow['location_name']);
			$rep->fontSize -= 2;
			$rep->NewLine();
		}
		
		$res = getTransactions($myrow['loc_code'], $from, $to, $fromcust);
		while ($row = db_fetch($res))
		{
			set_time_limit(0);
			$ref_no = get_sales_ref_no_report($row["trans_no"], $row["type"]);
			$aic = get_aic_out_report($row["trans_no"], $row["type"], $row['stock_id'], $myrow['loc_code']);
			
			if($aic == '' || $aic == NULL)
				continue;
			
			$rep->TextCol(0, 1, get_customer_name($row['debtor_no']));
			$rep->TextCol(1, 2, get_branch_name($row['branch_code']));
			$rep->TextCol(2, 3, $row['description']);
			$rep->TextCol(3, 4, $aic);
			$rep->TextCol(4, 5, $row['isn']);
			$rep->TextCol(5, 6, $ref_no["location"]."-".$ref_no["form_type_no"]);
			$rep->TextCol(6, 7, sql2date($row['tran_date']));
			$rep->NewLine();
			
		}
				
		$rep->NewLine();
	}
	
    $rep->End();
}

?>