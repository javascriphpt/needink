<?php

$page_security = 'SA_APVVOUCHER';

$path_to_root="../";

include_once($path_to_root . "includes/session.inc");
include_once($path_to_root . "includes/current_user.inc");
include_once($path_to_root . "includes/date_functions.inc");
include_once($path_to_root . "includes/data_checks.inc");
include_once($path_to_root . "gl/includes/gl_db.inc");
include_once($path_to_root . "/dimensions/includes/dimensions_db.inc");

//----------------------------------------------------------------------------------------------------
global $path_to_root;

// include_once($path_to_root . "reporting/includes/pdf_report.inc");
// $rep = new FrontReport(_('APV'), "APV.pdf", 'letter');
print_apv();

//createAPV($rep);
function print_apv()
{
	global $path_to_root;

	include_once($path_to_root . "reporting/includes/pdf_report.inc");

	$rep = new FrontReport(_('APV'), "APV.pdf", 'letter');

	$from = $_POST['PARAM_0']; //this is the APV number
	//die($from);
	$key=md5('trigger');
	
	for($x=0;$x<=1;$x++)
	{
	
		if ($x == 1)
			$minus = $rep->pageHeight/2 - 20;
		else
			$minus = 0;
			
	$yBase=534;
	$yBase_=764;
	$diff=384;
	$nextLine=10;
	
	$ymargin = 590;
	$xmargin = 20;
	
	$result=get_apv_details($from);
	$deb_tot = 0;
	$cre_tot = 0;
	
	// $filename = '../printouts/APV.jpg';
		// $rep->AddImage($filename, 0, 0, $rep->pageWidth, $rep->pageHeight);
	//left
	// $rep->LineTo(20,20+2+400-30-400+30,20, $yBase_);
	// $rep->LineTo($ymargin,20+2+400-30-400+30,$ymargin, $yBase_);
	// $rep->LineTo(590,764,20,764);
	// $rep->LineTo(590,422-30+20+31,20,422-30+20+31);
	
	$rep->LineTo($ymargin,405 - $minus,$ymargin, $yBase_ - $minus);
	$rep->LineTo($xmargin,405 - $minus,$xmargin, $yBase_ - $minus);
	$rep->LineTo($ymargin,$yBase_ - $minus,$xmargin,$yBase_ - $minus);
	$rep->LineTo($ymargin,422-30+20+31-23-15 - $minus,20,422-30+20+31-23-15 - $minus);
	
	//Header...
	$rep->LineTo($ymargin,740+9 - $minus,302,740+9 - $minus);
	$rep->LineTo($ymargin,744-10 - $minus,20,744-10 - $minus);
	$rep->LineTo(302,734 - $minus,302, $yBase_ - $minus);
	//Payee...
	$rep->LineTo($ymargin,744-10-20+5+1-4 - $minus,20,744-10-20+5+1-4 - $minus);
	//Memo...
	$rep->LineTo($ymargin,744-10-20-40 - $minus,20,744-10-20-40 - $minus);
	//Thead...
	$rep->LineTo($ymargin,744-10-20-40-20 - $minus,20,744-10-20-40-20 - $minus);
	//Column lines...
	$cols = array(240,320,400,500);
	foreach($cols as $val)
	$rep->LineTo($val,440+20 - $minus,$val, $yBase_-90 - $minus);
	//End line...
	$rep->LineTo($ymargin,460 - $minus,20,460 - $minus);
	//Footer lines...
	$rep->LineTo($ymargin-50,400+40 - $minus,$ymargin-170,400+40 - $minus);
	$rep->LineTo($ymargin-50-175,400+40 - $minus,$ymargin-170-175,400+40 - $minus);
	$rep->LineTo($ymargin-50-350,400+40 - $minus,$ymargin-170-350,400+40 - $minus);
	
	//Company preferences.
	$company=get_company_prefs();
	$rep->TextWrap(30,745 - $minus,$rep->pageWidth,$company['coy_name'],'left');
	// $rep->TextWrap(30,745-$diff,$rep->pageWidth,$company['coy_name'],'left');
	
	$rep->font('bold');
	$rep->fontSize=11.5;
	$rep->TextWrap(315,751 - $minus,$rep->pageWidth,STRTOUPPER('Account Payable Voucher'),'left');
	$rep->font();
	$rep->fontSize=10;
	$rep->TextWrap(307,737 - $minus,$rep->pageWidth,'#','left');
	$rep->TextWrap(437,737 - $minus,$rep->pageWidth,STRTOUPPER('Date : '),'left');
	$rep->TextWrap(30,720 - $minus,$rep->pageWidth,STRTOUPPER('Payee :'),'left');
	$rep->TextWrap(30,700 - $minus,$rep->pageWidth,STRTOUPPER('Details : '),'left');
	
	// $thead = array('Account',
						 // 'Dimension 1',
						 // 'Dimension 2',
						 // 'Debit',
						 // 'Credit'
						// );
						
	$rep->TextWrap(100,660 - $minus,$rep->pageWidth,STRTOUPPER('Account'),'left');
	$rep->TextWrap(250-2,660 - $minus,$rep->pageWidth,STRTOUPPER('Dimension 1'),'left');
	$rep->TextWrap(320+6,660 - $minus,$rep->pageWidth,STRTOUPPER('Dimension 2'),'left');
	$rep->TextWrap(420+12,660 - $minus,$rep->pageWidth,STRTOUPPER('Debit'),'left');
	$rep->TextWrap(530,660 - $minus,$rep->pageWidth,STRTOUPPER('Credit'),'right');
	$rep->TextWrap(530,660 - $minus,$rep->pageWidth,STRTOUPPER('Credit'),'right');
	
	$rep->TextWrap(100,425 - $minus,$rep->pageWidth,STRTOLOWER('prepared by'),'left');
	$rep->TextWrap(280,425 - $minus,$rep->pageWidth,STRTOLOWER('checked by'),'left');
	$rep->TextWrap(460,425 - $minus,$rep->pageWidth,STRTOLOWER('approved by'),'left');
	
	//APV Header
	$from = $_POST['PARAM_0'];
	$data=get_apv_header($from);
	
		$rep->TextWrap(320,737 - $minus,$rep->pageWidth,$data['ref'],'left');
		if($data['cust_supp']==3)
		$rep->TextWrap(360,737 - $minus,$rep->pageWidth,'INV # '.get_apv_header_get_ref($data['id']),'left');
		else
		$rep->TextWrap(360,737 - $minus,$rep->pageWidth,'INV # '.$data['invoice_no'],'left');
		// $rep->TextWrap(310,744-$diff,$rep->pageWidth,$data['ref'],'left');
		
		$rep->TextWrap(470,737 - $minus,$rep->pageWidth,sql2date($data['tran_date']),'left');
		// $rep->TextWrap(470,744-$diff,$rep->pageWidth,sql2date($data['tran_date']),'left');
		
		// if($data['cust_supp']!=0){
		if($data['cust_supp']==3){
		$sql_ = "SELECT supp_name FROM ".TB_PREF."suppliers WHERE supplier_id = ".db_escape($data['cust_supp_id']);
		// display_error($sql_);
		$result_=db_query($sql_,"No Supplier matches Supplier ID: ".$data['cust_supp_id']);
		$row_=db_fetch($result_);
		$supp=$row_[0];
		$rep->TextWrap(100,721 - $minus,$rep->pageWidth,$supp,'left');
		}
		elseif($data['cust_supp']==2){
		$sql_ = "SELECT name FROM ".TB_PREF."debtors_master WHERE debtor_no = ".db_escape($data['cust_supp_id']);
		// display_error($sql_);
		$result_=db_query($sql_,"No Customer matches Customer ID: ".$data['cust_supp_id']);
		$row_=db_fetch($result_);
		$supp=$row_[0];
		$rep->TextWrap(100,721 - $minus,$rep->pageWidth,$supp,'left');
		}
		
		else{
		$rep->TextWrap(100,721 - $minus,$rep->pageWidth,$data['cust_supp_id'],'left');
		
		}
		// else
		// $supp= $data['cust_supp_id'];
		// $rep->TextWrap(100,721 - $minus,$rep->pageWidth,$data['cust_supp_id'],'left');
		// $rep->TextWrap(100,721-$diff,$rep->pageWidth,$supp,'left');
		
	
	//APV Accounts
	$result=get_apv_details($from);
	$deb_tot = 0;
	$cre_tot = 0;
	$base = 634 - $minus;
	while($row = db_fetch($result)){
		$rep->TextWrap(30,$base,190,$row['account'].' - '.get_gl_account_name($row['account']),'left');
		// $rep->TextWrap(30,$base-$diff,190,$row['account'].' - '.get_gl_account_name($row['account']),'left');
		
		$rep->TextWrap(250,$base,$rep->pageWidth,get_dimension_string($row['dimension_id']),'left');
		// $rep->TextWrap(250,$base-$diff,$rep->pageWidth,get_dimension_string($row['dimension_id']),'left');
		$rep->TextWrap(350,$base,$rep->pageWidth,get_dimension_string($row['dimension2_id']),'left');
		// $rep->TextWrap(350,$base-$diff,$rep->pageWidth,get_dimension_string($row['dimension2_id']),'left');
		
		$add = 100;
		$amount = number_format2($row['amount'],2);
		if($amount>0){
			$rep->TextWrap(450,$base,$rep->pageWidth,$amount,'left');
			// $rep->TextWrap(450,$base-$diff,$rep->pageWidth,$amount,'left');
		}else{
			$rep->TextWrap(540,$base,$rep->pageWidth,number_format2(abs($row['amount']),2),'left');
			// $rep->TextWrap(540,$base-$diff,$rep->pageWidth,$amount,'left');
		}
		$base = $base - 15;
		// $base = $base -400;
	}
	}
		
	$rep->End();
}

?>