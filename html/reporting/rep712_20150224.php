<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PDCREP';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Tax Report
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//------------------------------------------------------------------

// trial_inquiry_controls();
print_income_and_expense_report();

//----------------------------------------------------------------------------------------------------

function get_total_amount($from, $to, $salesman, $type)
{
	$from = date2sql($from);
	$to = date2sql($to);
	
	$sql = "SELECT a.type, c.salesman_name, a.location, 
					SUM(ov_amount+ov_discount+ov_freight+ov_freight_tax+ov_gst+ewt) as total, 
					SUM(alloc) as alloc
			FROM ".TB_PREF."debtor_trans a
			JOIN ".TB_PREF."cust_branch b ON a.branch_code=b.branch_code
			JOIN ".TB_PREF."salesman c ON b.salesman=c.salesman_code
			WHERE a.type =  ".db_escape($type)."
			AND a.tran_date >= '$from'
			AND a.tran_date <= '$to'
			AND c.salesman_code = ".db_escape($salesman)."
			GROUP BY a.type, c.salesman_code
			ORDER BY c.salesman_code, a.location";
	$res = db_query($sql);
	$row = db_fetch($res);
	
	return $$row[3];
}

function get_check_number($trans_no, $type)
{
	$sql = "SELECT ".TB_PREF."cheque_details.chk_number
			FROM ".TB_PREF."cheque_details
			WHERE ".TB_PREF."cheque_details.bank_trans_id = $trans_no 
			AND ".TB_PREF."cheque_details.type = $type ";
	
	$result = db_query($sql,"Failed to retrieve check_number");
	$row = db_fetch($result);
	
	if(db_num_rows($result) == 0)
		return '';
	else
		return $row[0];

}

function get_bank_ref($trans_no, $type)
{
	$sql = "SELECT ".TB_PREF."bank_trans.ref
			FROM ".TB_PREF."bank_trans
			WHERE ".TB_PREF."bank_trans.trans_no = $trans_no 
			AND ".TB_PREF."bank_trans.type = $type ";
	
	$result = db_query($sql,"Failed to retrieve bank_ref");
	$row = db_fetch($result);
	
	return $row[0];

}

function get_sales_ref_no_OR_PR_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location, form_type
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_so_form_cat_name_report($cat_id)
{
	$sql = "SELECT name FROM ".TB_PREF."sales_form_category 
			WHERE id=".db_escape($cat_id);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

function print_income_and_expense_report()
{
    global $path_to_root, $systypes_array;

    $from = $_POST['PARAM_0'];
    $to = $_POST['PARAM_1'];
    $comments = $_POST['PARAM_2'];
	$destination = $_POST['PARAM_3'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$cols = array(0, 70, 190, 290, 390, 490);

	$headers = array(_('Date'), _('Sales'), _("Purchases"), _("Check #"), _('Balance'));

	$aligns = array('left',	'left',	'left',	'left',	'left');
			
    $params =   array( 	0 => $comments,
    				    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to));

    $rep = new FrontReport(_('Income & Expense Report'), "IncomeAndExpenseReport", user_pagesize(), 9, 'P');

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	$from = date2sql($from);
	$to = date2sql($to);
	$total = 0;
	$total2 = 0;
	
	$sql = "SELECT trans_no, type, tran_date, ov_amount+ov_discount+ov_freight+ov_freight_tax+ov_gst+ewt as total
			FROM ".TB_PREF."debtor_trans 
			WHERE type IN (1, 12)
			AND tran_date >= '$from'
			AND tran_date <= '$to'
			ORDER BY tran_date, trans_no";
	$result = db_query($sql, "The salesperson could not be retrieved");

	while ($myrow = db_fetch($result))
	{
		set_time_limit(0);
		
		if($myrow["type"] == 12)
		{
			$ref_no = get_sales_ref_no_OR_PR_report($myrow["trans_no"], $myrow["type"]);
			$ref = $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name_report($ref_no["form_type"]);
		}
		else
			$ref = get_bank_ref($myrow["trans_no"], $myrow["type"]);
		
		$rep->TextCol(0, 1, $myrow["tran_date"]);
		$rep->TextCol(1, 2, $ref);
		$rep->TextCol(2, 3, '');
		$rep->TextCol(3, 4, get_check_number($myrow["trans_no"], $myrow["type"]));
		$rep->TextCol(4, 5, number_format2($myrow["total"], 2));
		
		$total += $myrow["total"];
		
		$rep->NewLine();
	}
	
	$rep->Font('bold');	
	$rep->NewLine();
	$rep->Line($rep->row + 6);
	$rep->row -= 6;
	$rep->TextCol(0, 2,	_('Total '));
	$rep->TextCol(4, 5, number_format2($total,2));
	$rep->row -= 12;
	$rep->NewLine();
	$rep->Font('');	
	
	// $sql2 = "SELECT a.trans_no, a.type, a.reference, a.tran_date, a.ov_amount+a.ov_discount+a.ov_gst+a.ewt as total
			// FROM ".TB_PREF."supp_trans a, ".TB_PREF."bank_trans b
			// WHERE a.trans_no = b.trans_no
			// AND a.type = b.type
			// AND a.type IN (1, 22)
			// AND b.type IN (1, 22)
			// AND a.tran_date >= '$from'
			// AND a.tran_date <= '$to'
			// ORDER BY a.tran_date, a.trans_no";
	$sql2 = "SELECT a.trans_no, a.person_id, ABS(a.amount) as total, a.type, a.ref, a.person_type_id, a.trans_date
			FROM ".TB_PREF."bank_trans a
			WHERE a.type IN (1, 22)
			AND a.trans_date >= '$from'
			AND a.trans_date <= '$to'
			ORDER BY a.trans_date, a.trans_no";
	$result2 = db_query($sql2, "The salesperson could not be retrieved");

	while ($myrow2 = db_fetch($result2))
	{
		set_time_limit(0);
				
		$rep->TextCol(0, 1, $myrow2["trans_date"]);
		$rep->TextCol(1, 2, '');
		$rep->TextCol(2, 3, $myrow2["ref"]);
		$rep->TextCol(3, 4, get_check_number($myrow2["trans_no"], $myrow2["type"]));
		$rep->TextCol(4, 5, number_format2(abs($myrow2["total"]), 2));
		
		$total2 += abs($myrow2["total"]);
		
		$rep->NewLine();
	}
	
	$rep->Font('bold');	
	$rep->NewLine();
	$rep->Line($rep->row + 6);
	$rep->row -= 6;
	$rep->TextCol(0, 2,	_('Total '));
	$rep->TextCol(4, 5, number_format2($total2,2));
	$rep->row -= 12;
	$rep->NewLine();
	$rep->Font('');	
	
    $rep->End();
}

?>