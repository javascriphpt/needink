<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_ITEMSVALREP';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Inventory Valuation
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/inventory/includes/db/items_category_db.inc");

//----------------------------------------------------------------------------------------------------

print_inventory_valuation_report();

function getTransactions($category, $location, $from, $to)
{
	$from = date2sql($from);
	$to = date2sql($to);
	
	$sql = "SELECT ".TB_PREF."stock_master.category_id,
			".TB_PREF."stock_category.description AS cat_description,
			".TB_PREF."stock_master.stock_id,
			".TB_PREF."stock_master.description, ".TB_PREF."stock_master.inactive,
			".TB_PREF."stock_moves.loc_code,
			SUM(".TB_PREF."stock_moves.qty) AS QtyOnHand,
			".TB_PREF."stock_master.material_cost + ".TB_PREF."stock_master.labour_cost + ".TB_PREF."stock_master.overhead_cost AS UnitCost,
			SUM(".TB_PREF."stock_moves.qty) *(".TB_PREF."stock_master.material_cost + ".TB_PREF."stock_master.labour_cost + ".TB_PREF."stock_master.overhead_cost) AS ItemTotal
		FROM ".TB_PREF."stock_master,
			".TB_PREF."stock_category,
			".TB_PREF."stock_moves
		WHERE ".TB_PREF."stock_master.stock_id=".TB_PREF."stock_moves.stock_id
		AND ".TB_PREF."stock_master.category_id=".TB_PREF."stock_category.category_id
		AND ".TB_PREF."stock_moves.tran_date >= '$from'
		AND ".TB_PREF."stock_moves.tran_date <= '$to'
		GROUP BY ".TB_PREF."stock_master.category_id,
			".TB_PREF."stock_category.description, ";
		if ($location != 'all')
			$sql .= TB_PREF."stock_moves.loc_code, ";
		$sql .= "UnitCost,
			".TB_PREF."stock_master.stock_id,
			".TB_PREF."stock_master.description
		HAVING SUM(".TB_PREF."stock_moves.qty) != 0";
		if ($category != 0)
			$sql .= " AND ".TB_PREF."stock_master.category_id = ".db_escape($category);
		if ($location != 'all')
			$sql .= " AND ".TB_PREF."stock_moves.loc_code = ".db_escape($location);
		$sql .= " ORDER BY ".TB_PREF."stock_master.category_id,
			".TB_PREF."stock_master.stock_id";

    return db_query($sql,"No transactions were returned");
}

//----------------------------------------------------------------------------------------------------

function print_inventory_valuation_report()
{
    global $path_to_root;

    $to = $_POST['PARAM_0'];
	$category = $_POST['PARAM_1'];
    $location = $_POST['PARAM_2'];
    $comments = $_POST['PARAM_3'];
	$destination = $_POST['PARAM_4'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

    $dec = user_price_dec();

	if ($location == ALL_TEXT)
		$location = 'all';
	if ($location == 'all')
		$loc = _('All');
	else
		$loc = get_location_name($location);
		
	if ($category == ALL_NUMERIC)
		$category = 0;
	if ($category == 0)
		$cat = _('All');
	else
		$cat = get_category_name($category);

	$cols = array(0, 100, 250, 350, 450,	515);

	$headers = array(_('Item'), 'Description', _(''), _('Reorder Level'), _('QOH'));

	$aligns = array('left',	'left',	'right', 'right', 'right');

    $params =   array( 	0 => $comments,
						1 => array('text' => _('Period'), 'from' => $to, 'to' => ''),
						2 => array('text' => _('Category'), 'from' => $cat, 'to' => ''),
    				    3 => array('text' => _('Location'), 'from' => $loc, 'to' => ''));

    $rep = new FrontReport(_('Re-Order Level Report'), "ReOrderLevelReport", user_pagesize());

    $rep->Font();
    $rep->Info($params, $cols, $headers, $aligns);
    $rep->Header();
	
	$sql = "SELECT DISTINCT loc.loc_code, loc.location_name
			FROM ".TB_PREF."locations loc";
	if ($location != 'all')
		$sql .= " WHERE loc.loc_code=".db_escape($location);
	$sql .= " ORDER BY loc.location_name";
	$result = db_query($sql, "The location could not be retrieved");
	
	$loc_ = '';
	$grand_total = 0;
	while ($myrow = db_fetch($result))
	{
		$sql2 = "SELECT trans.stock_id, stock.description, SUM(trans.qty) as total_qty, loc.reorder_level
				FROM ".TB_PREF."stock_master stock, ".TB_PREF."stock_moves trans, ".TB_PREF."loc_stock loc
				WHERE stock.stock_id = trans.stock_id
				AND loc.loc_code = trans.loc_code
				AND trans.stock_id = loc.stock_id
				AND loc.reorder_level  > 0
				AND trans.tran_date <= '".date2sql($to)."'
				AND trans.loc_code=".db_escape($myrow['loc_code'])."";
		
		if ($category != 0)
			$sql2 .= " AND stock.category_id = ".db_escape($category);
		
		$sql2 .= " GROUP BY trans.stock_id 
				HAVING SUM(trans.qty) <= loc.reorder_level";
		$result2 = db_query($sql2, "The location could not be retrieved");
	
		if(db_num_rows($result2) >= 1)
		{
			if ($loc_ != $myrow['location_name'])
			{
				$rep->NewLine(2);
				$rep->fontSize += 4;
				$rep->TextCol(0, 4, $myrow['location_name']);
				$rep->fontSize -= 4;
				$rep->NewLine(2);
			}

			$total = $total1 = 0;
			while($row = db_fetch($result2))
			{
				$rep->TextCol(0, 1, $row['stock_id']);
				$rep->TextCol(1, 3, $row['description']);
				$rep->AmountCol(3, 4, $row['reorder_level'], $dec);
				$rep->AmountCol(4, 5, $row['total_qty'], $dec);
				
				$total += $row['reorder_level'];
				$total1 += $row['total_qty'];
				$rep->NewLine();
			}
			
			$rep->NewLine();
			$rep->TextCol(0, 4, _('Total'));
			$rep->AmountCol(3, 4, $total, $dec);
			$rep->AmountCol(4, 5, $total1, $dec);
			$rep->Line($rep->row  - 4);
			$rep->NewLine();
		}
	}	
	
    $rep->End();
}

?>