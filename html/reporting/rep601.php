<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_BANKREP';
// ----------------------------------------------------------------
// $ Revision:	2.0 $
// Creator:	Joe Hunt
// date_:	2005-05-19
// Title:	Bank Accounts Transactions
// ----------------------------------------------------------------
$path_to_root="..";

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

//----------------------------------------------------------------------------------------------------

print_bank_transactions();

//----------------------------------------------------------------------------------------------------

function get_bank_balance_to($to, $account)
{
	$to = date2sql($to);
	$sql = "SELECT SUM(amount) FROM ".TB_PREF."bank_trans WHERE bank_act='$account'
	AND trans_date < '$to'";
	$result = db_query($sql, "The starting balance on hand could not be calculated");
	$row = db_fetch_row($result);
	return $row[0];
}

function get_bank_transactions($from, $to, $account)
{
	$from = date2sql($from);
	$to = date2sql($to);
	$sql = "SELECT ".TB_PREF."bank_trans.*, ".TB_PREF."bank_accounts.account_type  
		FROM ".TB_PREF."bank_trans	JOIN ".TB_PREF."bank_accounts ON ".TB_PREF."bank_trans.bank_act = ".TB_PREF."bank_accounts.id
		WHERE ".TB_PREF."bank_trans.bank_act = '$account'
		AND trans_date >= '$from'
		AND trans_date <= '$to'
		ORDER BY trans_date,".TB_PREF."bank_trans.id";

	return db_query($sql,"The transactions for '$account' could not be retrieved");
}

function get_sales_ref_no_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_sales_ref_no_OR_PR_report($trans_no, $type)
{
	$sql = "SELECT form_type_no, location, form_type
			FROM ".TB_PREF."debtor_trans 
			WHERE trans_no = ".db_escape($trans_no)."
			AND type = ".db_escape($type);

	$result = db_query($sql, "could not retrieve ref no!!");

	return db_fetch($result);
}

function get_so_form_cat_name_report($cat_id)
{
	$sql = "SELECT name FROM ".TB_PREF."sales_form_category 
			WHERE id=".db_escape($cat_id);
	$res = db_query($sql);
	$row = db_fetch($res);
	return $row[0];
}

function get_chk_details($trans_no, $type)
{
	$sql = "SELECT ".TB_PREF."cheque_details.chk_number
			FROM
			".TB_PREF."cheque_details ,
			".TB_PREF."bank_trans
			WHERE
			".TB_PREF."bank_trans.trans_no = $trans_no AND 
			".TB_PREF."bank_trans.type = $type AND
			".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
			".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type ";
	
	//display_error($sql);
	$result = db_query($sql,"Failed to retrieve cheque details");
	$row = db_fetch($result);
	return $row;

}

function get_cv_chk_details($trans_no)
{
	$sql = "SELECT cheque_no
			FROM ".TB_PREF."check_voucher
			WHERE id = $trans_no";
	
	//display_error($sql);
	$result = db_query($sql,"Failed to retrieve cheque details --> cv");
	$row = db_fetch($result);
	return $row;

}

function get_chk_details_row_count($trans_no, $type)
{
	$sql = "SELECT ".TB_PREF."cheque_details.chk_number
			FROM
			".TB_PREF."cheque_details ,
			".TB_PREF."bank_trans
			WHERE
			".TB_PREF."bank_trans.trans_no = $trans_no AND 
			".TB_PREF."bank_trans.type = $type AND
			".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
			".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type ";
	
	//display_error($sql);
	$result = db_query($sql,"Failed to retrieve cheque details");
	return db_num_rows($result);

}

function print_bank_transactions()
{
	global $path_to_root, $systypes_array;

	$acc = $_POST['PARAM_0'];
	$from = $_POST['PARAM_1'];
	$to = $_POST['PARAM_2'];
	$comments = $_POST['PARAM_3'];
	$destination = $_POST['PARAM_4'];
	if ($destination)
		include_once($path_to_root . "/reporting/includes/excel_report.inc");
	else
		include_once($path_to_root . "/reporting/includes/pdf_report.inc");

	$rep = new FrontReport(_('Bank Statement'), "BankStatement", user_pagesize());
	$dec = user_price_dec();

	$cols = array(0, 90, 145, 200, 255, 380, 430, 490, 550);

	$aligns = array('left',	'left',	'left',	'left',	'left',	'right', 'right', 'right');

	$headers = array(_('Type'),	_('#'),	_('Check #'), _('Date'), _('Person/Item'),
		_('Debit'),	_('Credit'), _('Balance'));

	$account = get_bank_account($acc);
	$act = $account['bank_account_name']." - ".$account['bank_curr_code']." - ".$account['bank_account_number'];
   	$params =   array( 	0 => $comments,
	    1 => array('text' => _('Period'), 'from' => $from, 'to' => $to),
	    2 => array('text' => _('Bank Account'),'from' => $act,'to' => ''));

	$rep->Font();
	$rep->Info($params, $cols, $headers, $aligns);
	$rep->Header();


	$prev_balance = get_bank_balance_to($from, $account["id"]);

	$trans = get_bank_transactions($from, $to, $account['id']);

	$rows = db_num_rows($trans);
	if ($prev_balance != 0.0 || $rows != 0)
	{
		$rep->Font('bold');
		$rep->TextCol(0, 3,	$act);
		$rep->TextCol(3, 5, _('Opening Balance'));
		if ($prev_balance > 0.0)
			$rep->AmountCol(5, 6, abs($prev_balance), $dec);
		else
			$rep->AmountCol(6, 7, abs($prev_balance), $dec);
		$rep->Font();
		$total = $prev_balance;
		$rep->NewLine(2);
		if ($rows > 0)
		{
			// Keep a running total as we loop through
			// the transactions.
			$total_debit = $total_credit = 0;			
			
			while ($myrow=db_fetch($trans))
			{
				$total += $myrow['amount'];

				$rep->TextCol(0, 1, $systypes_array[$myrow["type"]]);
				//$rep->TextCol(1, 2,	$myrow['trans_no']);
				
				if($myrow["type"] == ST_CUSTDELIVERY || $myrow["type"] == ST_SALESINVOICE || $myrow["type"] == ST_CUSTCREDIT)
				{
					$ref_no = get_sales_ref_no_report($myrow["trans_no"], $myrow["type"]);
					$rep->TextCol(1, 2,	$ref_no["location"]."-".$ref_no["form_type_no"]);
				}
				else if($myrow["type"] == ST_CUSTPAYMENT)
				{
					$ref_no = get_sales_ref_no_OR_PR_report($myrow["trans_no"], $myrow["type"]);
					$rep->TextCol(1, 2,	$ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name_report($ref_no["form_type"]));
				}
				else
					$rep->TextCol(1, 2,	$myrow['ref']);
					
				//$rep->TextCol(2, 3,	$myrow['ref']);
				
				if($myrow["type"] == ST_CUSTPAYMENT || $myrow["type"] == ST_SUPPAYMENT)
				{	
					if($myrow["account_type"] == 1 && get_chk_details_row_count($myrow["trans_no"], $myrow["type"]) == 1)	
					{
						$row = get_chk_details($myrow["trans_no"], $myrow["type"]);
						$rep->TextCol(2, 3,	$row[0]);
					}
					else
						$rep->TextCol(2, 3,	'');
				}
				else if($myrow["type"] == ST_CVVVOUCHER)
				{
					$row2 = get_cv_chk_details($myrow["trans_no"]);
					$rep->TextCol(2, 3,	$row2[0]);
				}
				else if($myrow["type"] == ST_BANKPAYMENT || $myrow["type"] == ST_BANKDEPOSIT)
				{
					if($myrow["account_type"] == 1 && get_chk_details_row_count($myrow["trans_no"], $myrow["type"]) == 1)	
					{
						$row = get_chk_details($myrow["trans_no"], $myrow["type"]);
						$rep->TextCol(2, 3,	$row[0]);
					}
					else
						$rep->TextCol(2, 3,	'');
				}
				else
					$rep->TextCol(2, 3,	'');
				
				$rep->DateCol(3, 4,	$myrow["trans_date"], true);
				$rep->TextCol(4, 5,	payment_person_name($myrow["person_type_id"],$myrow["person_id"], false));
				if ($myrow['amount'] > 0.0)
				{
					$rep->AmountCol(5, 6, abs($myrow['amount']), $dec);
					$total_debit += abs($myrow['amount']);
				}
				else
				{
					$rep->AmountCol(6, 7, abs($myrow['amount']), $dec);
					$total_credit += abs($myrow['amount']);
				}
				$rep->AmountCol(7, 8, $total, $dec);
				$rep->NewLine();
				if ($rep->row < $rep->bottomMargin + $rep->lineHeight)
				{
					$rep->Line($rep->row - 2);
					$rep->Header();
				}
			}
			$rep->NewLine();
		}
		
		// Print totals for the debit and credit columns.
		$rep->TextCol(3, 5, _("Total Debit / Credit"));
		$rep->AmountCol(5, 6, $total_debit, $dec);
		$rep->AmountCol(6, 7, $total_credit, $dec);
		$rep->NewLine(2);

		$rep->Font('bold');
		$rep->TextCol(3, 5,	_("Ending Balance"));
		if ($total > 0.0)
			$rep->AmountCol(5, 6, abs($total), $dec);
		else
			$rep->AmountCol(6, 7, abs($total), $dec);
		$rep->Font();
		$rep->Line($rep->row - $rep->lineHeight + 4);
		$rep->NewLine(2, 1);
		
		// Print the difference between starting and ending balances.
		$net_change = ($total - $prev_balance); 
		$rep->TextCol(3, 5, _("Net Change"));
		if ($total > 0.0)
			$rep->AmountCol(5, 6, $net_change, $dec, 0, 0, 0, 0, null, 1, True);
		else
			$rep->AmountCol(6, 7, $net_change, $dec, 0, 0, 0, 0, null, 1, True);
	}
	$rep->End();
}

?>