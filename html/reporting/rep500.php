<?php

$page_security = 'SA_SUPPTRANSVIEW';

$path_to_root="../";

include_once($path_to_root . "includes/session.inc");
include_once($path_to_root . "includes/current_user.inc");
include_once($path_to_root . "includes/date_functions.inc");
include_once($path_to_root . "includes/data_checks.inc");
include_once($path_to_root . "gl/includes/gl_db.inc");
include_once($path_to_root . "/dimensions/includes/dimensions_db.inc");

//----------------------------------------------------------------------------------------------------
global $path_to_root;


print_apv();

function print_apv()
{
	global $path_to_root;

	include_once($path_to_root . "reporting/includes/pdf_report.inc");

	$rep = new FrontReport(_('APV'), "APV.pdf", 'letter');

	$type = $_POST['PARAM_0']; 
	$trans_no = $_POST['PARAM_1']; 
	//die($from);
	$key=md5('trigger');
	
	for($x=0;$x<=1;$x++)
	{
	
		if ($x == 1)
			$minus = $rep->pageHeight/2 - 20;
		else
			$minus = 0;
			
		$yBase=534;
		$yBase_=764;
		$diff=384;
		$nextLine=10;
		
		$ymargin = 590;
		$xmargin = 20;
		
		// $result=get_apv_details($from);
		$deb_tot = 0;
		$cre_tot = 0;
		
		// $filename = '../printouts/APV.jpg';
			// $rep->AddImage($filename, 0, 0, $rep->pageWidth, $rep->pageHeight);
		//left
		// $rep->LineTo(20,20+2+400-30-400+30,20, $yBase_);
		// $rep->LineTo($ymargin,20+2+400-30-400+30,$ymargin, $yBase_);
		// $rep->LineTo(590,764,20,764);
		// $rep->LineTo(590,422-30+20+31,20,422-30+20+31);
		
		$rep->LineTo($ymargin,405 - $minus,$ymargin, $yBase_ - $minus);
		$rep->LineTo($xmargin,405 - $minus,$xmargin, $yBase_ - $minus);
		$rep->LineTo($ymargin,$yBase_ - $minus,$xmargin,$yBase_ - $minus);
		$rep->LineTo($ymargin,422-30+20+31-23-15 - $minus,20,422-30+20+31-23-15 - $minus);
		
		//Header...
		$rep->LineTo($ymargin,740+9 - $minus,302,740+9 - $minus);
		$rep->LineTo($ymargin,744-10 - $minus,20,744-10 - $minus);
		$rep->LineTo(302,734 - $minus,302, $yBase_ - $minus);
		//Payee...
		$rep->LineTo($ymargin,744-10-20+5+1-4 - $minus,20,744-10-20+5+1-4 - $minus);
		//Memo...
		$rep->LineTo($ymargin,744-10-20-40 - $minus,20,744-10-20-40 - $minus);
		//Thead...
		$rep->LineTo($ymargin,744-10-20-40-20 - $minus,20,744-10-20-40-20 - $minus);
		//Column lines...
		$cols = array(240,320,400,500);
		foreach($cols as $val)
		$rep->LineTo($val,440+20 - $minus,$val, $yBase_-90 - $minus);
		//End line...
		$rep->LineTo($ymargin,460 - $minus,20,460 - $minus);
		//Footer lines...
		$rep->LineTo($ymargin-50,400+40 - $minus,$ymargin-170,400+40 - $minus);
		$rep->LineTo($ymargin-50-175,400+40 - $minus,$ymargin-170-175,400+40 - $minus);
		$rep->LineTo($ymargin-50-350,400+40 - $minus,$ymargin-170-350,400+40 - $minus);
		
		//Company preferences.
		$company=get_company_prefs();
		$rep->TextWrap(30,745 - $minus,$rep->pageWidth,$company['coy_name'],'left');
		// $rep->TextWrap(30,745-$diff,$rep->pageWidth,$company['coy_name'],'left');
		
		$rep->font('bold');
		$rep->fontSize=11.5;
		$rep->TextWrap(315,751 - $minus,$rep->pageWidth,STRTOUPPER('Account Payable Voucher'),'left');
		$rep->font();
		$rep->fontSize=10;
		$rep->TextWrap(307,737 - $minus,$rep->pageWidth,'#','left');
		$rep->TextWrap(437,737 - $minus,$rep->pageWidth,STRTOUPPER('Date : '),'left');
		$rep->TextWrap(30,720 - $minus,$rep->pageWidth,STRTOUPPER('Payee :'),'left');
		$rep->TextWrap(30,700 - $minus,$rep->pageWidth,STRTOUPPER('Details : '),'left');
		
		$rep->TextWrap(100,660 - $minus,$rep->pageWidth,STRTOUPPER('Account'),'left');
		$rep->TextWrap(250-2,660 - $minus,$rep->pageWidth,STRTOUPPER('Dimension 1'),'left');
		$rep->TextWrap(320+6,660 - $minus,$rep->pageWidth,STRTOUPPER('Dimension 2'),'left');
		$rep->TextWrap(420+12,660 - $minus,$rep->pageWidth,STRTOUPPER('Debit'),'left');
		$rep->TextWrap(530,660 - $minus,$rep->pageWidth,STRTOUPPER('Credit'),'left');
		
		$rep->TextWrap(100,425 - $minus,$rep->pageWidth,STRTOLOWER('prepared by'),'left');
		$rep->TextWrap(280,425 - $minus,$rep->pageWidth,STRTOLOWER('checked by'),'left');
		$rep->TextWrap(460,425 - $minus,$rep->pageWidth,STRTOLOWER('approved by'),'left');
		
		$sql = "SELECT gl.*, cm.account_name, IF(ISNULL(refs.reference), '', refs.reference) AS reference FROM "
				.TB_PREF."gl_trans as gl
				LEFT JOIN ".TB_PREF."chart_master as cm ON gl.account = cm.account_code
				LEFT JOIN ".TB_PREF."refs as refs ON (gl.type=refs.type AND gl.type_no=refs.id)" ." 
				WHERE gl.type= ".db_escape($type) ." 
				AND gl.type_no = ".db_escape($trans_no) ." 
				ORDER BY amount DESC, counter";
		$result = db_query($sql,"could not get transactions");
		
		$base = 634 - $minus;
		$counter = 1;
		while ($myrow = db_fetch($result)) 
		{
		
			$rep->TextWrap(320,737 - $minus,$rep->pageWidth,$myrow["reference"],'left');
			$rep->TextWrap(360,737 - $minus,$rep->pageWidth,'INV # '.$myrow["reference"],'left');
			$rep->TextWrap(470,737 - $minus,$rep->pageWidth,sql2date($myrow["tran_date"]),'left');
			
			$rep->TextWrap(100,721 - $minus,$rep->pageWidth,get_supplier_name($myrow["person_id"]),'left');
			
			$comments = get_comments($myrow["type"], $myrow["type_no"]);
			while ($comment = db_fetch($comments))
			{
				$rep->TextWrap(100,699 - $minus,$rep->pageWidth,$comment["memo_"],'left');
			}
		
			$deb_tot = 0;
			$cre_tot = 0;
			
			$rep->TextWrap(30,$base,190,$myrow['account'].' - '.$myrow['account_name'],'left');
			$rep->TextWrap(250,$base,$rep->pageWidth,get_dimension_string($myrow['dimension_id']),'left');
			$rep->TextWrap(350,$base,$rep->pageWidth,get_dimension_string($myrow['dimension2_id']),'left');
			
			$add = 100;
			$amount = number_format2($myrow['amount'],2);
			if($amount>0)
				$rep->TextWrap(450,$base,$rep->pageWidth,$amount,'left');
			else
				$rep->TextWrap(530,$base,$rep->pageWidth,number_format2(abs($myrow['amount']),2),'left');
			
			$base = $base - 15;
			
			$counter++;
			
			// if($counter == 3)
			// {
				// $counter = 1;
				// $rep->NewPage();
				// if ($x == 1)
					// $minus = $rep->pageHeight/2 - 20;
				// else
					// $minus = 0;
			
				// $yBase=534;
				// $yBase_=764;
				// $diff=384;
				// $nextLine=10;
				
				// $ymargin = 590;
				// $xmargin = 20;
				
				// // $result=get_apv_details($from);
				// $deb_tot = 0;
				// $cre_tot = 0;
				
				// // $filename = '../printouts/APV.jpg';
					// // $rep->AddImage($filename, 0, 0, $rep->pageWidth, $rep->pageHeight);
				// //left
				// // $rep->LineTo(20,20+2+400-30-400+30,20, $yBase_);
				// // $rep->LineTo($ymargin,20+2+400-30-400+30,$ymargin, $yBase_);
				// // $rep->LineTo(590,764,20,764);
				// // $rep->LineTo(590,422-30+20+31,20,422-30+20+31);
				
				// $rep->LineTo($ymargin,405 - $minus,$ymargin, $yBase_ - $minus);
				// $rep->LineTo($xmargin,405 - $minus,$xmargin, $yBase_ - $minus);
				// $rep->LineTo($ymargin,$yBase_ - $minus,$xmargin,$yBase_ - $minus);
				// $rep->LineTo($ymargin,422-30+20+31-23-15 - $minus,20,422-30+20+31-23-15 - $minus);
				
				// //Header...
				// $rep->LineTo($ymargin,740+9 - $minus,302,740+9 - $minus);
				// $rep->LineTo($ymargin,744-10 - $minus,20,744-10 - $minus);
				// $rep->LineTo(302,734 - $minus,302, $yBase_ - $minus);
				// //Payee...
				// $rep->LineTo($ymargin,744-10-20+5+1-4 - $minus,20,744-10-20+5+1-4 - $minus);
				// //Memo...
				// $rep->LineTo($ymargin,744-10-20-40 - $minus,20,744-10-20-40 - $minus);
				// //Thead...
				// $rep->LineTo($ymargin,744-10-20-40-20 - $minus,20,744-10-20-40-20 - $minus);
				// //Column lines...
				// $cols = array(240,320,400,500);
				// foreach($cols as $val)
				// $rep->LineTo($val,440+20 - $minus,$val, $yBase_-90 - $minus);
				// //End line...
				// $rep->LineTo($ymargin,460 - $minus,20,460 - $minus);
				// //Footer lines...
				// $rep->LineTo($ymargin-50,400+40 - $minus,$ymargin-170,400+40 - $minus);
				// $rep->LineTo($ymargin-50-175,400+40 - $minus,$ymargin-170-175,400+40 - $minus);
				// $rep->LineTo($ymargin-50-350,400+40 - $minus,$ymargin-170-350,400+40 - $minus);
				
				// //Company preferences.
				// $company=get_company_prefs();
				// $rep->TextWrap(30,745 - $minus,$rep->pageWidth,$company['coy_name'],'left');
				// // $rep->TextWrap(30,745-$diff,$rep->pageWidth,$company['coy_name'],'left');
				
				// $rep->font('bold');
				// $rep->fontSize=11.5;
				// $rep->TextWrap(315,751 - $minus,$rep->pageWidth,STRTOUPPER('Account Payable Voucher'),'left');
				// $rep->font();
				// $rep->fontSize=10;
				// $rep->TextWrap(307,737 - $minus,$rep->pageWidth,'#','left');
				// $rep->TextWrap(437,737 - $minus,$rep->pageWidth,STRTOUPPER('Date : '),'left');
				// $rep->TextWrap(30,720 - $minus,$rep->pageWidth,STRTOUPPER('Payee :'),'left');
				// $rep->TextWrap(30,700 - $minus,$rep->pageWidth,STRTOUPPER('Details : '),'left');
				
				// $rep->TextWrap(100,660 - $minus,$rep->pageWidth,STRTOUPPER('Account'),'left');
				// $rep->TextWrap(250-2,660 - $minus,$rep->pageWidth,STRTOUPPER('Dimension 1'),'left');
				// $rep->TextWrap(320+6,660 - $minus,$rep->pageWidth,STRTOUPPER('Dimension 2'),'left');
				// $rep->TextWrap(420+12,660 - $minus,$rep->pageWidth,STRTOUPPER('Debit'),'left');
				// $rep->TextWrap(530,660 - $minus,$rep->pageWidth,STRTOUPPER('Credit'),'left');
				
				// $rep->TextWrap(100,425 - $minus,$rep->pageWidth,STRTOLOWER('prepared by'),'left');
				// $rep->TextWrap(280,425 - $minus,$rep->pageWidth,STRTOLOWER('checked by'),'left');
				// $rep->TextWrap(460,425 - $minus,$rep->pageWidth,STRTOLOWER('approved by'),'left');
				
				// $base = 634 - $minus;
			// }
		}	//while
	}
		
	$rep->End();
}

?>