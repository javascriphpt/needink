ALTER TABLE `0_users` ADD COLUMN `default_item_categories`  int(11) NOT NULL default '0' AFTER `can_access`;
ALTER TABLE `0_users` ADD COLUMN `can_access_all_item_categories`  int(11) NOT NULL default '0' AFTER `default_item_categories`;
ALTER TABLE `0_users` ADD COLUMN `item_categories_def`  varchar(100) NOT NULL default '' AFTER `can_access_all_item_categories`;