-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.47


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema ARIA310
--

CREATE DATABASE IF NOT EXISTS ARIA310;
USE ARIA310;

--
-- Definition of table `ARIA310`.`0_areas`
--

DROP TABLE IF EXISTS `ARIA310`.`0_areas`;
CREATE TABLE  `ARIA310`.`0_areas` (
  `area_code` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(60) NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`area_code`),
  UNIQUE KEY `description` (`description`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_areas`
--

/*!40000 ALTER TABLE `0_areas` DISABLE KEYS */;
LOCK TABLES `0_areas` WRITE;
INSERT INTO `ARIA310`.`0_areas` VALUES  (1,'Global',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_areas` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_attachments`
--

DROP TABLE IF EXISTS `ARIA310`.`0_attachments`;
CREATE TABLE  `ARIA310`.`0_attachments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(60) NOT NULL DEFAULT '',
  `type_no` int(11) NOT NULL DEFAULT '0',
  `trans_no` int(11) NOT NULL DEFAULT '0',
  `unique_name` varchar(60) NOT NULL DEFAULT '',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `filename` varchar(60) NOT NULL DEFAULT '',
  `filesize` int(11) NOT NULL DEFAULT '0',
  `filetype` varchar(60) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `type_no` (`type_no`,`trans_no`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_attachments`
--

/*!40000 ALTER TABLE `0_attachments` DISABLE KEYS */;
LOCK TABLES `0_attachments` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_attachments` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_audit_trail`
--

DROP TABLE IF EXISTS `ARIA310`.`0_audit_trail`;
CREATE TABLE  `ARIA310`.`0_audit_trail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(6) unsigned NOT NULL DEFAULT '0',
  `trans_no` int(11) unsigned NOT NULL DEFAULT '0',
  `user` smallint(6) unsigned NOT NULL DEFAULT '0',
  `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `description` varchar(60) DEFAULT NULL,
  `fiscal_year` int(11) NOT NULL,
  `gl_date` date NOT NULL DEFAULT '0000-00-00',
  `gl_seq` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fiscal_year` (`fiscal_year`,`gl_seq`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_audit_trail`
--

/*!40000 ALTER TABLE `0_audit_trail` DISABLE KEYS */;
LOCK TABLES `0_audit_trail` WRITE;
INSERT INTO `ARIA310`.`0_audit_trail` VALUES  (1,17,1,1,'2010-06-09 14:20:29','',3,'2010-06-09',0),
 (2,1,1,1,'2011-01-18 17:12:57','',3,'2010-12-31',NULL),
 (3,1,1,1,'2011-01-18 17:12:57','',3,'2010-12-31',0),
 (4,2,1,1,'2011-01-18 17:15:33','',3,'2010-12-31',NULL),
 (5,2,1,1,'2011-01-18 17:15:33','',3,'2010-12-31',0),
 (6,30,1,1,'2011-01-25 19:01:53','',3,'2010-12-31',0),
 (7,13,1,1,'2011-01-25 19:02:23','',3,'2010-12-31',0),
 (8,10,1,1,'2011-01-25 19:02:44','',3,'2010-12-31',0),
 (9,30,2,1,'2011-02-28 08:57:21','',3,'2010-12-31',0),
 (10,13,2,1,'2011-02-28 08:57:21','',3,'2010-12-31',0),
 (11,10,2,1,'2011-02-28 08:57:21','',3,'2010-12-31',0),
 (12,12,1,1,'2011-02-28 08:57:21','',3,'2010-12-31',0),
 (13,17,2,1,'2011-03-14 15:58:08','',4,'2011-03-14',0),
 (14,30,3,1,'2011-03-14 15:58:26','',4,'2010-12-31',0),
 (15,30,4,1,'2011-03-14 15:58:59','',4,'2011-03-14',0),
 (16,13,3,1,'2011-03-14 16:00:20','',4,'2011-03-14',0),
 (17,13,4,1,'2011-03-14 16:01:02','',4,'2011-03-14',0),
 (18,26,1,1,'2011-03-14 16:05:19','',4,'2011-03-14',NULL),
 (19,26,1,1,'2011-03-14 16:05:19','Released.',4,'2011-03-14',0),
 (20,28,1,1,'2011-03-14 16:05:41','',4,'2011-03-14',0),
 (21,29,1,1,'2011-03-14 16:07:48','Production.',4,'2011-03-14',0),
 (22,29,2,1,'2011-03-14 16:09:18','Production.',4,'2011-03-14',0),
 (23,18,1,1,'2011-03-14 16:11:55','',4,'2011-03-14',0),
 (24,25,1,1,'2011-03-14 16:11:59','',4,'2011-03-14',0),
 (25,29,3,1,'2011-03-14 16:12:19','Production.',4,'2011-03-14',0),
 (26,32,1,1,'2011-03-18 12:11:44','',4,'2011-03-18',0),
 (27,18,2,1,'2011-03-24 18:29:06','',4,'2011-03-24',0),
 (28,25,2,1,'2011-03-24 18:29:11','',4,'2011-03-24',0),
 (29,20,1,1,'2011-03-24 18:29:33','',4,'2011-03-24',0),
 (30,18,3,1,'2011-03-26 09:25:48','',4,'2011-03-26',0),
 (31,25,3,1,'2011-03-26 09:27:48','',4,'2011-03-26',0),
 (32,20,2,1,'2011-03-26 09:29:59','',4,'2011-03-26',0),
 (33,32,2,1,'2011-03-26 09:33:43','',4,'2011-03-26',0),
 (34,30,5,1,'2011-03-26 09:35:11','',4,'2011-03-26',0),
 (35,13,5,1,'2011-03-26 09:37:21','',4,'2011-03-26',0),
 (36,10,3,1,'2011-03-26 09:43:08','',4,'2011-03-26',0),
 (37,26,2,1,'2011-03-26 09:55:39','',4,'2011-03-26',NULL),
 (38,26,2,1,'2011-03-26 09:55:39','Released.',4,'2011-03-26',0),
 (39,30,6,1,'2011-03-26 09:57:02','',4,'2011-03-26',0),
 (40,28,2,1,'2011-03-26 10:00:41','',4,'2011-03-26',0),
 (41,29,4,1,'2011-03-26 10:02:34','Production.',4,'2011-03-26',0),
 (42,26,3,1,'2011-03-26 10:18:43','',4,'2011-03-26',NULL),
 (43,26,3,1,'2011-03-26 10:18:43','Released.',4,'2011-03-26',0),
 (44,18,4,1,'2011-04-12 10:40:25','',4,'2011-04-12',0),
 (45,25,4,1,'2011-04-12 10:41:19','',4,'2011-04-12',0),
 (46,28,3,1,'2011-04-12 11:18:32','',4,'2011-04-12',0),
 (47,29,5,1,'2011-04-12 11:19:04','Production.',4,'2011-04-12',0),
 (48,28,4,1,'2011-04-12 11:19:22','',4,'2011-04-12',0),
 (49,29,6,1,'2011-04-12 11:19:39','Production.',4,'2011-04-12',0),
 (50,28,5,1,'2011-04-12 11:23:37','',4,'2011-04-12',0),
 (51,18,5,1,'2011-04-12 14:29:17','',4,'2011-04-12',0),
 (52,25,5,1,'2011-04-12 14:35:17','',4,'2011-04-12',0),
 (53,32,3,1,'2011-04-12 14:41:15','',4,'2011-04-12',0),
 (54,30,7,1,'2011-04-12 14:45:20','',4,'2011-04-12',0),
 (55,30,8,1,'2011-04-12 14:52:11','',4,'2011-04-12',0),
 (56,13,6,1,'2011-04-12 14:53:09','',4,'2011-04-12',0),
 (57,10,4,1,'2011-04-12 14:53:39','',4,'2011-04-12',0),
 (58,26,4,1,'2011-04-12 15:01:04','',4,'2011-04-12',NULL),
 (59,26,4,1,'2011-04-12 15:01:04','Released.',4,'2011-04-12',0),
 (60,28,6,1,'2011-04-12 15:01:59','',4,'2011-04-12',0),
 (61,29,7,1,'2011-04-12 15:02:55','Production.',4,'2011-04-12',0),
 (62,30,9,1,'2011-04-12 15:34:26','',4,'2011-04-12',0),
 (63,13,7,1,'2011-04-12 15:35:26','',4,'2011-04-12',0),
 (64,18,6,1,'2011-04-12 15:38:33','',4,'2011-04-12',0),
 (65,25,6,1,'2011-04-12 15:38:51','',4,'2011-04-12',0),
 (66,30,10,1,'2011-05-06 11:18:44','',4,'2011-05-06',0),
 (67,13,8,1,'2011-05-06 11:18:52','',4,'2011-05-06',0),
 (68,30,11,1,'2011-05-06 11:19:31','',4,'2011-05-06',0),
 (69,13,9,1,'2011-05-06 11:19:35','',4,'2011-05-06',0),
 (70,10,5,1,'2011-05-06 11:21:08','',4,'2011-05-06',0),
 (71,18,7,1,'2011-05-19 15:25:01','',4,'2011-05-19',0),
 (72,25,7,1,'2011-05-19 15:28:29','',4,'2011-05-19',0),
 (73,20,3,1,'2011-05-19 15:34:41','',4,'2011-05-19',0),
 (74,16,1,1,'2011-05-19 15:37:51','',4,'2011-05-19',0),
 (75,30,12,1,'2011-05-19 15:41:15','',4,'2011-05-19',0),
 (76,13,10,1,'2011-05-19 15:42:03','',4,'2011-05-19',0),
 (77,10,6,1,'2011-05-19 15:43:48','',4,'2011-05-19',0),
 (78,12,2,1,'2011-05-19 15:45:58','',4,'2011-05-19',0),
 (79,0,1,1,'2011-05-25 13:20:37','',4,'2011-05-25',0),
 (80,18,8,1,'2011-06-04 11:27:01','',4,'2011-06-04',0),
 (81,25,8,1,'2011-06-04 11:28:13','',4,'2011-06-04',0),
 (82,20,4,1,'2011-06-04 11:29:40','',4,'2011-06-04',0),
 (83,12,3,1,'2011-06-04 11:35:25','',4,'2011-07-29',0),
 (84,16,2,1,'2011-06-04 11:46:55','',4,'2011-06-04',0),
 (85,30,13,1,'2011-06-28 17:49:46','',4,'2011-06-28',0),
 (86,13,11,1,'2011-06-28 17:49:46','',4,'2011-06-28',0),
 (87,18,9,1,'2011-06-28 17:54:27','',4,'2011-06-28',0),
 (88,25,9,1,'2011-06-28 17:54:35','',4,'2011-06-28',0),
 (89,20,5,1,'2011-06-28 17:55:44','',4,'2011-06-28',0),
 (90,18,10,1,'2011-07-06 11:12:37','',4,'2011-07-06',0),
 (91,25,10,1,'2011-07-06 11:12:43','',4,'2011-07-06',0),
 (92,20,6,1,'2011-07-06 11:13:55','',4,'2011-07-06',0),
 (93,26,5,1,'2011-07-06 11:15:14','',4,'2011-07-06',NULL),
 (94,26,5,1,'2011-07-06 11:15:14','Released.',4,'2011-07-06',0),
 (95,28,7,1,'2011-07-06 11:15:58','',4,'2011-07-06',0),
 (96,28,8,1,'2011-07-06 11:16:29','',4,'2011-07-06',0),
 (97,18,11,1,'2011-07-07 16:57:09','',4,'2011-07-07',0),
 (98,25,11,1,'2011-07-07 16:57:14','',4,'2011-07-07',0),
 (99,18,12,1,'2011-07-07 17:31:54','',4,'2011-07-07',0),
 (100,18,13,1,'2011-07-07 17:32:20','',4,'2011-07-07',0),
 (101,25,12,1,'2011-07-07 17:32:47','',4,'2011-07-07',0),
 (102,16,3,1,'2011-07-07 17:37:15','',4,'2011-07-07',0),
 (103,30,14,1,'2011-07-07 17:43:15','',4,'2011-07-07',0),
 (104,13,12,1,'2011-07-07 17:43:20','',4,'2011-07-07',0),
 (105,30,15,1,'2011-07-07 18:08:53','',4,'2011-07-07',0),
 (106,13,13,1,'2011-07-07 18:10:27','',4,'2011-07-07',0),
 (107,13,14,1,'2011-07-07 18:11:13','',4,'2011-07-07',0),
 (108,22,1,1,'2011-07-07 18:14:02','',4,'2011-07-07',0),
 (109,18,14,1,'2011-07-07 18:16:59','',4,'2011-07-07',0),
 (110,25,13,1,'2011-07-07 18:17:04','',4,'2011-07-07',0),
 (111,20,7,1,'2011-07-07 18:17:16','',4,'2011-07-07',0),
 (112,22,2,1,'2011-07-07 18:17:59','',4,'2011-07-07',0),
 (113,22,3,1,'2011-07-07 18:23:06','',4,'2011-07-07',0),
 (114,22,4,1,'2011-07-07 18:23:40','',4,'2011-07-07',0),
 (115,26,6,1,'2011-07-07 18:25:49','',4,'2011-07-07',NULL),
 (116,26,6,1,'2011-07-07 18:25:49','Released.',4,'2011-07-07',0),
 (117,28,9,1,'2011-07-07 18:26:10','',4,'2011-07-07',0),
 (118,26,7,1,'2011-07-07 18:43:36','',4,'2011-07-07',NULL),
 (119,26,7,1,'2011-07-07 18:43:36','Released.',4,'2011-07-07',0),
 (120,29,8,1,'2011-07-07 18:44:11','Production.',4,'2011-07-07',0),
 (121,18,15,1,'2011-07-08 10:47:00','',4,'2011-07-08',0),
 (122,25,14,1,'2011-07-08 10:48:02','',4,'2011-07-08',0),
 (123,30,16,1,'2011-07-12 16:41:22','',4,'2011-07-12',0),
 (124,13,15,1,'2011-07-12 16:41:40','',4,'2011-07-12',0),
 (125,18,16,1,'2011-07-14 10:21:51','',4,'2011-07-14',0),
 (126,25,15,1,'2011-07-14 10:21:55','',4,'2011-07-14',0),
 (127,20,8,1,'2011-07-14 10:22:17','',4,'2011-07-14',0),
 (128,22,5,1,'2011-07-14 10:22:43','',4,'2011-07-14',0),
 (129,17,3,1,'2011-07-14 11:05:05','',4,'2011-07-14',0),
 (130,25,16,1,'2011-07-22 13:50:18','',4,'2011-07-22',0),
 (131,20,9,1,'2011-07-22 13:52:05','',4,'2011-07-22',0),
 (132,22,6,1,'2011-07-22 13:53:13','',4,'2011-07-22',0),
 (133,1,2,1,'2011-07-22 13:54:11','',4,'2011-07-22',0),
 (134,18,17,1,'2011-08-02 10:50:10','',4,'2011-08-02',0),
 (135,25,17,1,'2011-08-02 10:53:13','',4,'2011-08-02',0),
 (136,20,10,1,'2011-08-02 10:56:15','',4,'2011-08-02',0),
 (137,22,7,1,'2011-08-02 11:02:45','',4,'2011-08-02',0),
 (138,22,8,1,'2011-08-02 11:03:16','',4,'2011-08-02',0),
 (139,30,17,1,'2011-08-02 11:15:22','',4,'2011-08-02',0),
 (140,13,16,1,'2011-08-02 11:15:27','',4,'2011-08-02',0),
 (141,30,18,1,'2011-08-12 16:32:54','',4,'2011-08-12',0),
 (142,13,17,1,'2011-08-12 16:32:54','',4,'2011-08-12',0),
 (143,16,4,1,'2011-08-12 16:44:46','',4,'2011-08-12',0),
 (144,16,5,1,'2011-08-12 16:47:10','',4,'2011-08-12',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_audit_trail` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_bank_accounts`
--

DROP TABLE IF EXISTS `ARIA310`.`0_bank_accounts`;
CREATE TABLE  `ARIA310`.`0_bank_accounts` (
  `account_code` varchar(11) NOT NULL DEFAULT '',
  `account_type` smallint(6) NOT NULL DEFAULT '0',
  `bank_account_name` varchar(60) NOT NULL DEFAULT '',
  `bank_account_number` varchar(100) NOT NULL DEFAULT '',
  `bank_name` varchar(60) NOT NULL DEFAULT '',
  `bank_address` tinytext,
  `bank_curr_code` char(3) NOT NULL DEFAULT '',
  `dflt_curr_act` tinyint(1) NOT NULL DEFAULT '0',
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `last_reconciled_date` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ending_reconcile_balance` double NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `bank_account_name` (`bank_account_name`),
  KEY `bank_account_number` (`bank_account_number`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_bank_accounts`
--

/*!40000 ALTER TABLE `0_bank_accounts` DISABLE KEYS */;
LOCK TABLES `0_bank_accounts` WRITE;
INSERT INTO `ARIA310`.`0_bank_accounts` VALUES  ('1065',3,'Petty Cash account','N/A','N/A','','USD',0,2,'0000-00-00 00:00:00',0,0),
 ('1060',1,'Metrobank ','123465656534343','Metrobank','address','PHP',1,3,'2011-07-14 00:00:00',4000000,0),
 ('1065',2,'Cashier','1','Cashier','','PHP',0,4,'2011-07-07 00:00:00',45000000,0),
 ('10000',1,'UCPB','','','','PHP',0,5,'0000-00-00 00:00:00',0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_bank_accounts` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_bank_trans`
--

DROP TABLE IF EXISTS `ARIA310`.`0_bank_trans`;
CREATE TABLE  `ARIA310`.`0_bank_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(6) DEFAULT NULL,
  `trans_no` int(11) DEFAULT NULL,
  `bank_act` varchar(11) DEFAULT NULL,
  `ref` varchar(40) DEFAULT NULL,
  `trans_date` date NOT NULL DEFAULT '0000-00-00',
  `amount` double DEFAULT NULL,
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  `person_type_id` int(11) NOT NULL DEFAULT '0',
  `person_id` tinyblob,
  `reconciled` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bank_act` (`bank_act`,`ref`),
  KEY `type` (`type`,`trans_no`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_bank_trans`
--

/*!40000 ALTER TABLE `0_bank_trans` DISABLE KEYS */;
LOCK TABLES `0_bank_trans` WRITE;
INSERT INTO `ARIA310`.`0_bank_trans` VALUES  (1,1,1,'2','1','2010-12-31',-10000,0,0,3,0x31,NULL),
 (2,2,1,'2','1','2010-12-31',100000,0,0,2,0x34,NULL),
 (3,12,1,'2','1','2010-12-31',8572.1352301028,0,0,2,0x34,NULL),
 (4,26,1,'2','','2011-03-14',-22.558250605534,0,0,1,0x30,NULL),
 (5,26,2,'2','','2011-03-26',-115.15764076815,0,0,1,0x30,NULL),
 (6,26,2,'2','','2011-04-12',-34.5472922304,0,0,1,0x31,NULL),
 (7,26,4,'2','','2011-04-12',-34.5472922304,0,0,1,0x31,NULL),
 (8,12,2,'4','2','2011-05-19',38000,0,0,2,0x34,'2011-07-07'),
 (9,0,1,'2','1','2011-05-25',1151.57640768,0,0,0,'',NULL),
 (10,12,3,'4','3','2011-07-29',15000,0,0,2,0x34,'2011-07-07'),
 (11,26,5,'2','','2011-07-06',-11.5157640768,0,0,1,0x30,NULL),
 (12,22,1,'4','1','2011-07-07',-1000000,0,0,3,0x33,NULL),
 (13,22,2,'4','2','2011-07-07',-28000,0,0,3,0x33,NULL),
 (14,22,3,'4','3','2011-07-07',-600000,0,0,3,0x33,NULL),
 (15,22,4,'3','4','2011-07-07',-600000,0,0,3,0x33,'2011-07-14'),
 (16,26,6,'2','','2011-07-07',-1151.57640768,0,0,1,0x31,NULL),
 (17,22,5,'3','5','2011-07-14',-5712000,0,0,3,0x31,'2011-07-14'),
 (18,22,6,'3','6','2011-07-22',-560000,0,0,3,0x31,NULL),
 (19,1,2,'3','2','2011-07-22',-55000,0,0,0,0x746573742064697362757273656D656E74,NULL),
 (20,22,7,'4','7','2011-08-02',-1120000,0,0,3,0x31,NULL),
 (21,22,8,'3','8','2011-08-02',-2940000,0,0,3,0x31,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_bank_trans` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_bom`
--

DROP TABLE IF EXISTS `ARIA310`.`0_bom`;
CREATE TABLE  `ARIA310`.`0_bom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent` char(20) NOT NULL DEFAULT '',
  `component` char(20) NOT NULL DEFAULT '',
  `workcentre_added` int(11) NOT NULL DEFAULT '0',
  `loc_code` char(5) NOT NULL DEFAULT '',
  `quantity` double NOT NULL DEFAULT '1',
  PRIMARY KEY (`parent`,`component`,`workcentre_added`,`loc_code`),
  KEY `component` (`component`),
  KEY `id` (`id`),
  KEY `loc_code` (`loc_code`),
  KEY `parent` (`parent`,`loc_code`),
  KEY `Parent_2` (`parent`),
  KEY `workcentre_added` (`workcentre_added`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_bom`
--

/*!40000 ALTER TABLE `0_bom` DISABLE KEYS */;
LOCK TABLES `0_bom` WRITE;
INSERT INTO `ARIA310`.`0_bom` VALUES  (1,'qqq111','1000-100-104',1,'DEF',1),
 (2,'qqq111','1000-100-105',1,'DEF',1),
 (3,'ITEM00001','1000-100-104',1,'DEF',1),
 (4,'ITEM00001','1000-100-105',1,'DEF',1),
 (5,'ITEM00001','1000-100-106',1,'DEF',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_bom` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_budget_trans`
--

DROP TABLE IF EXISTS `ARIA310`.`0_budget_trans`;
CREATE TABLE  `ARIA310`.`0_budget_trans` (
  `counter` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(6) NOT NULL DEFAULT '0',
  `type_no` bigint(16) NOT NULL DEFAULT '1',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `account` varchar(11) NOT NULL DEFAULT '',
  `memo_` tinytext NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `dimension_id` int(11) DEFAULT '0',
  `dimension2_id` int(11) DEFAULT '0',
  `person_type_id` int(11) DEFAULT NULL,
  `person_id` tinyblob,
  PRIMARY KEY (`counter`),
  KEY `Type_and_Number` (`type`,`type_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_budget_trans`
--

/*!40000 ALTER TABLE `0_budget_trans` DISABLE KEYS */;
LOCK TABLES `0_budget_trans` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_budget_trans` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_chart_class`
--

DROP TABLE IF EXISTS `ARIA310`.`0_chart_class`;
CREATE TABLE  `ARIA310`.`0_chart_class` (
  `cid` int(11) NOT NULL DEFAULT '0',
  `class_name` varchar(60) NOT NULL DEFAULT '',
  `ctype` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_chart_class`
--

/*!40000 ALTER TABLE `0_chart_class` DISABLE KEYS */;
LOCK TABLES `0_chart_class` WRITE;
INSERT INTO `ARIA310`.`0_chart_class` VALUES  (1,'Assets',1,0),
 (2,'Liabilities',2,0),
 (3,'Income',4,0),
 (4,'Costs',6,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_chart_class` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_chart_master`
--

DROP TABLE IF EXISTS `ARIA310`.`0_chart_master`;
CREATE TABLE  `ARIA310`.`0_chart_master` (
  `account_code` varchar(11) NOT NULL DEFAULT '',
  `account_code2` varchar(11) DEFAULT '',
  `account_name` varchar(60) NOT NULL DEFAULT '',
  `account_type` int(11) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`account_code`),
  KEY `account_code` (`account_code`),
  KEY `account_name` (`account_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_chart_master`
--

/*!40000 ALTER TABLE `0_chart_master` DISABLE KEYS */;
LOCK TABLES `0_chart_master` WRITE;
INSERT INTO `ARIA310`.`0_chart_master` VALUES  ('1060','','Checking Account',1,0),
 ('1065','','Petty Cash',1,0),
 ('1200','','Accounts Receivables',1,0),
 ('1205','','Allowance for doubtful accounts',1,0),
 ('1510','','Inventory',2,0),
 ('1520','','Stocks of Raw Materials',2,0),
 ('1530','','Stocks of Work In Progress',2,0),
 ('1540','','Stocks of Finsihed Goods',2,0),
 ('1550','','Goods Received Clearing account',2,0),
 ('1820','','Office Furniture &amp; Equipment',3,0),
 ('1825','','Accum. Amort. -Furn. &amp; Equip.',3,0),
 ('1840','','Vehicle',3,0),
 ('1845','','Accum. Amort. -Vehicle',3,0),
 ('2100','','Accounts Payable',4,0),
 ('2110','','Accrued Income Tax - Federal',4,0),
 ('2120','','Accrued Income Tax - State',4,0),
 ('2130','','Accrued Franchise Tax',4,0),
 ('2140','','Accrued Real &amp; Personal Prop Tax',4,0),
 ('2145','','VAT input Tax',1,0),
 ('2150','','VAT Output Tax',4,0),
 ('2160','','Accrued Use Tax Payable',4,0),
 ('2210','','Accrued Wages',4,0),
 ('2220','','Accrued Comp Time',4,0),
 ('2230','','Accrued Holiday Pay',4,0),
 ('2240','','Accrued Vacation Pay',4,0),
 ('2310','','Accr. Benefits - 401K',4,0),
 ('2320','','Accr. Benefits - Stock Purchase',4,0),
 ('2330','','Accr. Benefits - Med, Den',4,0),
 ('2340','','Accr. Benefits - Payroll Taxes',4,0),
 ('2350','','Accr. Benefits - Credit Union',4,0),
 ('2360','','Accr. Benefits - Savings Bond',4,0),
 ('2370','','Accr. Benefits - Garnish',4,0),
 ('2380','','Accr. Benefits - Charity Cont.',4,0),
 ('2620','','Bank Loans',5,0),
 ('2680','','Loans from Shareholders',5,0),
 ('3350','','Common Shares',6,0),
 ('3590','','Retained Earnings - prior years',7,0),
 ('4010','','Sales',8,0),
 ('4430','','Shipping &amp; Handling',9,0),
 ('4440','','Interest',9,0),
 ('4450','','Foreign Exchange Gain',9,0),
 ('4500','','Prompt Payment Discounts',9,0),
 ('4510','','Discounts Given',9,0),
 ('5010','','Cost of Goods Sold - Retail',10,0),
 ('5020','','Material Usage Varaiance',10,0),
 ('5030','','Consumable Materials',10,0),
 ('5040','','Purchase price Variance',10,0),
 ('5050','','Purchases of materials',10,0),
 ('5060','','Discounts Received',10,0),
 ('5100','','Freight',10,0),
 ('5410','','Wages &amp; Salaries',11,0),
 ('5420','','Wages - Overtime',11,0),
 ('5430','','Benefits - Comp Time',11,0),
 ('5440','','Benefits - Payroll Taxes',11,0),
 ('5450','','Benefits - Workers Comp',11,0),
 ('5460','','Benefits - Pension',11,0),
 ('5470','','Benefits - General Benefits',11,0),
 ('5510','','Inc Tax Exp - Federal',11,0),
 ('5520','','Inc Tax Exp - State',11,0),
 ('5530','','Taxes - Real Estate',11,0),
 ('5540','','Taxes - Personal Property',11,0),
 ('5550','','Taxes - Franchise',11,0),
 ('5560','','Taxes - Foreign Withholding',11,0),
 ('5610','','Accounting &amp; Legal',12,0),
 ('5615','','Advertising &amp; Promotions',12,0),
 ('5620','','Bad Debts',12,0),
 ('5660','','Amortization Expense',12,0),
 ('5685','','Insurance',12,0),
 ('5690','','Interest &amp; Bank Charges',12,0),
 ('5700','','Office Supplies',12,0),
 ('5760','','Rent',12,0),
 ('5765','','Repair &amp; Maintenance',12,0),
 ('5780','','Telephone',12,0),
 ('5785','','Travel &amp; Entertainment',12,0),
 ('5790','','Utilities',12,0),
 ('5795','','Registrations',12,0),
 ('5800','','Licenses',12,0),
 ('5810','','Foreign Exchange Loss',12,0),
 ('9990','','Year Profit/Loss',12,0),
 ('2500','','Expanded Witholding Tax Payables - Services',4,0),
 ('2501','','Expanded Witholding Tax Payables - Goods',4,0),
 ('2052','','Expanded Witholding Tax Receivables',1,0),
 ('10000','','CASH IN BANK - UCPB',1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_chart_master` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_chart_types`
--

DROP TABLE IF EXISTS `ARIA310`.`0_chart_types`;
CREATE TABLE  `ARIA310`.`0_chart_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '',
  `class_id` tinyint(1) NOT NULL DEFAULT '0',
  `parent` int(11) NOT NULL DEFAULT '-1',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_chart_types`
--

/*!40000 ALTER TABLE `0_chart_types` DISABLE KEYS */;
LOCK TABLES `0_chart_types` WRITE;
INSERT INTO `ARIA310`.`0_chart_types` VALUES  (1,'Current Assets',1,-1,0),
 (2,'Inventory Assets',1,-1,0),
 (3,'Capital Assets',1,-1,0),
 (4,'Current Liabilities',2,-1,0),
 (5,'Long Term Liabilities',2,-1,0),
 (6,'Share Capital',2,-1,0),
 (7,'Retained Earnings',2,-1,0),
 (8,'Sales Revenue',3,-1,0),
 (9,'Other Revenue',3,-1,0),
 (10,'Cost of Goods Sold',4,-1,0),
 (11,'Payroll Expenses',4,-1,0),
 (12,'General &amp; Administrative expenses',4,-1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_chart_types` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_check_account`
--

DROP TABLE IF EXISTS `ARIA310`.`0_check_account`;
CREATE TABLE  `ARIA310`.`0_check_account` (
  `account_id` int(11) NOT NULL AUTO_INCREMENT,
  `bank_ref` varchar(11) COLLATE utf8_unicode_ci NOT NULL,
  `next_reference` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`account_id`) USING BTREE,
  UNIQUE KEY `bank_ref` (`bank_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ARIA310`.`0_check_account`
--

/*!40000 ALTER TABLE `0_check_account` DISABLE KEYS */;
LOCK TABLES `0_check_account` WRITE;
INSERT INTO `ARIA310`.`0_check_account` VALUES  (1,'1060','100000006'),
 (2,'10000','9876543210');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_check_account` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_check_trans`
--

DROP TABLE IF EXISTS `ARIA310`.`0_check_trans`;
CREATE TABLE  `ARIA310`.`0_check_trans` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `check_ref` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `bank_trans_id` int(11) NOT NULL,
  `cheque_bank_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `cheque_ref` (`check_ref`,`cheque_bank_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `ARIA310`.`0_check_trans`
--

/*!40000 ALTER TABLE `0_check_trans` DISABLE KEYS */;
LOCK TABLES `0_check_trans` WRITE;
INSERT INTO `ARIA310`.`0_check_trans` VALUES  (1,'100000001',8,1),
 (2,'100000002',12,1),
 (3,'100000003',17,1),
 (4,'100000004',18,1),
 (5,'100000005',21,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_check_trans` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_comments`
--

DROP TABLE IF EXISTS `ARIA310`.`0_comments`;
CREATE TABLE  `ARIA310`.`0_comments` (
  `type` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '0',
  `date_` date DEFAULT '0000-00-00',
  `memo_` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_comments`
--

/*!40000 ALTER TABLE `0_comments` DISABLE KEYS */;
LOCK TABLES `0_comments` WRITE;
INSERT INTO `ARIA310`.`0_comments` VALUES  (17,1,'2010-06-09','\n'),
 (12,1,'2010-12-31','Cash invoice 2'),
 (13,5,'2011-03-26','Sales Quotation # 2'),
 (10,3,'2011-03-26','Sales Quotation # 2'),
 (28,5,'2011-04-12','this is an additional item'),
 (13,6,'2011-04-12','Sales Quotation # 3'),
 (10,4,'2011-04-12','Sales Quotation # 3'),
 (26,4,'2011-04-12','Release to work center'),
 (0,2,'2011-07-14','Accruals for 15000'),
 (0,3,'2011-08-14','Accruals for 15000'),
 (0,4,'2011-09-14','Accruals for 15000'),
 (0,5,'2011-10-14','Accruals for 15000'),
 (0,6,'2011-11-14','Accruals for 15000'),
 (16,4,'2011-08-12','Transfer item');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_comments` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_company`
--

DROP TABLE IF EXISTS `ARIA310`.`0_company`;
CREATE TABLE  `ARIA310`.`0_company` (
  `coy_code` int(11) NOT NULL DEFAULT '1',
  `coy_name` varchar(60) NOT NULL DEFAULT '',
  `gst_no` varchar(25) NOT NULL DEFAULT '',
  `coy_no` varchar(25) NOT NULL DEFAULT '0',
  `tax_prd` int(11) NOT NULL DEFAULT '1',
  `tax_last` int(11) NOT NULL DEFAULT '1',
  `postal_address` tinytext NOT NULL,
  `phone` varchar(30) NOT NULL DEFAULT '',
  `fax` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `coy_logo` varchar(100) NOT NULL DEFAULT '',
  `domicile` varchar(55) NOT NULL DEFAULT '',
  `curr_default` char(3) NOT NULL DEFAULT '',
  `debtors_act` varchar(11) NOT NULL DEFAULT '',
  `pyt_discount_act` varchar(11) NOT NULL DEFAULT '',
  `creditors_act` varchar(11) NOT NULL DEFAULT '',
  `bank_charge_act` varchar(11) NOT NULL DEFAULT '',
  `exchange_diff_act` varchar(11) NOT NULL DEFAULT '',
  `profit_loss_year_act` varchar(11) NOT NULL DEFAULT '',
  `retained_earnings_act` varchar(11) NOT NULL DEFAULT '',
  `freight_act` varchar(11) NOT NULL DEFAULT '',
  `default_sales_act` varchar(11) NOT NULL DEFAULT '',
  `default_sales_discount_act` varchar(11) NOT NULL DEFAULT '',
  `default_prompt_payment_act` varchar(11) NOT NULL DEFAULT '',
  `default_inventory_act` varchar(11) NOT NULL DEFAULT '',
  `default_cogs_act` varchar(11) NOT NULL DEFAULT '',
  `default_adj_act` varchar(11) NOT NULL DEFAULT '',
  `default_inv_sales_act` varchar(11) NOT NULL DEFAULT '',
  `default_assembly_act` varchar(11) NOT NULL DEFAULT '',
  `payroll_act` varchar(11) NOT NULL DEFAULT '',
  `allow_negative_stock` tinyint(1) NOT NULL DEFAULT '0',
  `po_over_receive` int(11) NOT NULL DEFAULT '10',
  `po_over_charge` int(11) NOT NULL DEFAULT '10',
  `default_credit_limit` int(11) NOT NULL DEFAULT '1000',
  `default_workorder_required` int(11) NOT NULL DEFAULT '20',
  `default_dim_required` int(11) NOT NULL DEFAULT '20',
  `past_due_days` int(11) NOT NULL DEFAULT '30',
  `use_dimension` tinyint(1) DEFAULT '0',
  `f_year` int(11) NOT NULL DEFAULT '1',
  `no_item_list` tinyint(1) NOT NULL DEFAULT '0',
  `no_customer_list` tinyint(1) NOT NULL DEFAULT '0',
  `no_supplier_list` tinyint(1) NOT NULL DEFAULT '0',
  `base_sales` int(11) NOT NULL DEFAULT '-1',
  `foreign_codes` tinyint(1) NOT NULL DEFAULT '0',
  `accumulate_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `legal_text` tinytext NOT NULL,
  `default_delivery_required` smallint(6) NOT NULL DEFAULT '1',
  `version_id` varchar(11) NOT NULL DEFAULT '',
  `time_zone` tinyint(1) NOT NULL DEFAULT '0',
  `add_pct` int(5) NOT NULL DEFAULT '-1',
  `round_to` int(5) NOT NULL DEFAULT '1',
  `login_tout` smallint(6) NOT NULL DEFAULT '600',
  PRIMARY KEY (`coy_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_company`
--

/*!40000 ALTER TABLE `0_company` DISABLE KEYS */;
LOCK TABLES `0_company` WRITE;
INSERT INTO `ARIA310`.`0_company` VALUES  (1,'IntelliTech Business Solutions Inc','','+632 63 7187',1,1,'Prestige Tower, F. Ortigas Jr Ave., Ortigas Center, Pasig City.','+632 6317187','+632 635 5661','inquire@intellitech.com.ph','','','PHP','1200','5060','2100','5690','4450','9990','3590','4430','4010','4510','4500','1510','5010','5040','4010','1530','5000',0,10,10,1000,20,20,30,2,4,0,0,0,1,0,0,'',1,'2.2',0,-1,1,600);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_company` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_credit_status`
--

DROP TABLE IF EXISTS `ARIA310`.`0_credit_status`;
CREATE TABLE  `ARIA310`.`0_credit_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason_description` char(100) NOT NULL DEFAULT '',
  `dissallow_invoices` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `reason_description` (`reason_description`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_credit_status`
--

/*!40000 ALTER TABLE `0_credit_status` DISABLE KEYS */;
LOCK TABLES `0_credit_status` WRITE;
INSERT INTO `ARIA310`.`0_credit_status` VALUES  (1,'Good History',0,0),
 (3,'No more work until payment received',1,0),
 (4,'In liquidation',1,0),
 (5,'Prepaid',1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_credit_status` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_currencies`
--

DROP TABLE IF EXISTS `ARIA310`.`0_currencies`;
CREATE TABLE  `ARIA310`.`0_currencies` (
  `currency` varchar(60) NOT NULL DEFAULT '',
  `curr_abrev` char(3) NOT NULL DEFAULT '',
  `curr_symbol` varchar(10) NOT NULL DEFAULT '',
  `country` varchar(100) NOT NULL DEFAULT '',
  `hundreds_name` varchar(15) NOT NULL DEFAULT '',
  `auto_update` tinyint(1) NOT NULL DEFAULT '1',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`curr_abrev`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_currencies`
--

/*!40000 ALTER TABLE `0_currencies` DISABLE KEYS */;
LOCK TABLES `0_currencies` WRITE;
INSERT INTO `ARIA310`.`0_currencies` VALUES  ('US Dollars','USD','$','United States','Cents',1,0),
 ('Phlippine Peso','PHP','PHP','Philippines','Hundreds',1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_currencies` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_cust_allocations`
--

DROP TABLE IF EXISTS `ARIA310`.`0_cust_allocations`;
CREATE TABLE  `ARIA310`.`0_cust_allocations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amt` double unsigned DEFAULT NULL,
  `date_alloc` date NOT NULL DEFAULT '0000-00-00',
  `trans_no_from` int(11) DEFAULT NULL,
  `trans_type_from` int(11) DEFAULT NULL,
  `trans_no_to` int(11) DEFAULT NULL,
  `trans_type_to` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_cust_allocations`
--

/*!40000 ALTER TABLE `0_cust_allocations` DISABLE KEYS */;
LOCK TABLES `0_cust_allocations` WRITE;
INSERT INTO `ARIA310`.`0_cust_allocations` VALUES  (1,380000,'2011-02-28',1,12,2,10),
 (2,18000,'2011-05-19',2,12,5,10),
 (3,20000,'2011-05-19',2,12,6,10),
 (4,15000,'2011-06-04',3,12,6,10);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_cust_allocations` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_cust_branch`
--

DROP TABLE IF EXISTS `ARIA310`.`0_cust_branch`;
CREATE TABLE  `ARIA310`.`0_cust_branch` (
  `branch_code` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_no` int(11) NOT NULL DEFAULT '0',
  `br_name` varchar(60) NOT NULL DEFAULT '',
  `branch_ref` varchar(30) NOT NULL DEFAULT '',
  `br_address` tinytext NOT NULL,
  `area` int(11) DEFAULT NULL,
  `salesman` int(11) NOT NULL DEFAULT '0',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `phone2` varchar(30) NOT NULL DEFAULT '',
  `fax` varchar(30) NOT NULL DEFAULT '',
  `contact_name` varchar(60) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `default_location` varchar(5) NOT NULL DEFAULT '',
  `tax_group_id` int(11) DEFAULT NULL,
  `sales_account` varchar(11) DEFAULT NULL,
  `sales_discount_account` varchar(11) DEFAULT NULL,
  `receivables_account` varchar(11) DEFAULT NULL,
  `payment_discount_account` varchar(11) DEFAULT NULL,
  `default_ship_via` int(11) NOT NULL DEFAULT '1',
  `disable_trans` tinyint(4) NOT NULL DEFAULT '0',
  `br_post_address` tinytext NOT NULL,
  `group_no` int(11) NOT NULL DEFAULT '0',
  `notes` tinytext NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`branch_code`,`debtor_no`),
  KEY `branch_code` (`branch_code`),
  KEY `br_name` (`br_name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_cust_branch`
--

/*!40000 ALTER TABLE `0_cust_branch` DISABLE KEYS */;
LOCK TABLES `0_cust_branch` WRITE;
INSERT INTO `ARIA310`.`0_cust_branch` VALUES  (1,1,'MICROMATIC INDUSTRIES INC','MMI','PARANAQUE CITY',1,1,'','','','Main Branch','@','DEF',1,'','4510','1200','4500',1,0,'PARANAQUE CITY',0,'',0),
 (2,2,'PRIME PEAK PROPERTIES LTD INC','3PLI','ORTIGAS CENTER, PASIG',1,1,'','','','Main Branch','@','DEF',1,'','4510','1200','4500',1,0,'ORTIGAS CENTER, PASIG',0,'',0),
 (3,3,'STAINLESS STEEL INTERNATIONAL SALES INC','SSIS','PARANAQUE CITY',1,1,'','','','Main Branch','@','DEF',1,'','4510','1200','4500',1,0,'PARANAQUE CITY',0,'',0),
 (4,4,'BON INDUSTRIAL SALES INC','BON','QUEZON CITY',1,1,'','','','Main Branch','@','DEF',1,'','4510','1200','4500',1,0,'QUEZON CITY',0,'',0),
 (5,5,'FARMACIA SRA. DEL PILAR','DEL PILAR','ILOCOS SUR',1,1,'','','','Main Branch','@','DEF',1,'','4510','1200','4500',1,0,'ILOCOS SUR',0,'',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_cust_branch` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_debtor_trans`
--

DROP TABLE IF EXISTS `ARIA310`.`0_debtor_trans`;
CREATE TABLE  `ARIA310`.`0_debtor_trans` (
  `trans_no` int(11) unsigned NOT NULL DEFAULT '0',
  `type` smallint(6) unsigned NOT NULL DEFAULT '0',
  `version` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `debtor_no` int(11) unsigned DEFAULT NULL,
  `branch_code` int(11) NOT NULL DEFAULT '-1',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `due_date` date NOT NULL DEFAULT '0000-00-00',
  `reference` varchar(60) NOT NULL DEFAULT '',
  `tpe` int(11) NOT NULL DEFAULT '0',
  `order_` int(11) NOT NULL DEFAULT '0',
  `ov_amount` double NOT NULL DEFAULT '0',
  `ov_gst` double NOT NULL DEFAULT '0',
  `ov_freight` double NOT NULL DEFAULT '0',
  `ov_freight_tax` double NOT NULL DEFAULT '0',
  `ov_discount` double NOT NULL DEFAULT '0',
  `alloc` double NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '1',
  `ship_via` int(11) DEFAULT NULL,
  `trans_link` int(11) NOT NULL DEFAULT '0',
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`trans_no`,`type`),
  KEY `debtor_no` (`debtor_no`,`branch_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_debtor_trans`
--

/*!40000 ALTER TABLE `0_debtor_trans` DISABLE KEYS */;
LOCK TABLES `0_debtor_trans` WRITE;
INSERT INTO `ARIA310`.`0_debtor_trans` VALUES  (1,2,0,4,4,'2010-12-31','0000-00-00','1',0,0,4432967.8638941,0,0,0,0,0,1,0,0,0,0),
 (1,10,0,2,2,'2010-12-31','2011-01-01','1',1,1,53000,0,0,0,0,0,1,1,1,0,0),
 (1,12,0,4,4,'2010-12-31','0000-00-00','1',0,0,380000,0,0,0,0,380000,1,0,0,0,0),
 (1,13,1,2,2,'2010-12-31','2011-01-01','1',1,1,53000,0,0,0,0,0,1,1,1,0,0),
 (2,10,0,4,4,'2010-12-31','2010-12-31','2',1,2,380000,0,0,0,0,380000,1,1,2,0,0),
 (2,12,0,4,4,'2011-05-19','0000-00-00','2',0,0,38000,0,0,0,0,38000,1,0,0,0,0),
 (2,13,1,4,4,'2010-12-31','2010-12-31','auto',1,2,380000,0,0,0,0,0,1,1,2,0,0),
 (3,10,0,2,2,'2011-03-26','2011-03-27','3',1,5,54000,0,0,0,0,0,1,1,5,0,0),
 (3,12,0,4,4,'2011-07-29','0000-00-00','3',0,0,15000,0,0,0,0,15000,1,0,0,0,0),
 (3,13,0,2,2,'2011-03-14','2011-03-15','2',1,4,180000,0,0,0,0,0,1,1,0,0,0),
 (4,10,0,2,2,'2011-04-12','2011-04-13','4',1,8,453500,0,0,0,0,0,1,1,6,0,0),
 (4,13,0,2,2,'2011-03-14','2011-03-15','3',1,4,180000,0,0,0,0,0,1,1,0,0,0),
 (5,10,0,4,4,'2011-05-06','2011-05-07','5',1,10,18000,0,0,0,0,18000,1,1,8,0,0),
 (5,13,1,2,2,'2011-03-26','2011-03-26','4',1,5,54000,0,0,0,0,0,1,1,3,0,0),
 (6,10,0,4,4,'2011-05-19','2011-05-20','6',1,12,35000,0,0,0,0,35000,1,1,10,0,0),
 (6,13,1,2,2,'2011-04-12','2011-04-12','5',1,8,453500,0,0,0,0,0,1,1,4,0,0),
 (7,13,0,2,2,'2011-04-12','2011-04-13','6',1,9,665000,0,0,0,0,0,1,1,0,0,0),
 (8,13,1,4,4,'2011-05-06','2011-05-06','7',1,10,18000,0,0,0,0,0,1,1,5,0,0),
 (9,13,0,4,4,'2011-05-06','2011-05-07','8',1,11,36000,0,0,0,0,0,1,1,0,0,0),
 (10,13,1,4,4,'2011-05-19','2011-05-19','9',1,12,35000,0,0,0,0,0,1,1,6,0,0),
 (11,13,0,4,4,'2011-06-28','2011-06-28','10',1,13,1800000,0,0,0,0,0,1,1,0,0,0),
 (12,13,0,4,4,'2011-07-07','2011-07-08','11',1,14,18000,0,0,0,0,0,1,1,0,0,0),
 (13,13,0,4,4,'2011-07-07','2011-07-07','12',1,15,36000,0,0,0,0,0,1,1,0,0,0),
 (14,13,0,4,4,'2011-07-07','2011-07-07','13',1,15,54000,0,0,0,0,0,1,1,0,0,0),
 (15,13,0,4,4,'2011-07-12','2011-07-12','14',1,16,24750,0,0,0,0,0,1,1,0,0,0),
 (16,13,0,4,4,'2011-08-02','2011-08-02','15',1,17,67600,0,0,0,0,0,1,1,0,0,0),
 (17,13,0,4,4,'2011-08-12','2011-08-12','16',1,18,1750000,0,0,0,0,0,1,1,0,0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_debtor_trans` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_debtor_trans_details`
--

DROP TABLE IF EXISTS `ARIA310`.`0_debtor_trans_details`;
CREATE TABLE  `ARIA310`.`0_debtor_trans_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `debtor_trans_no` int(11) DEFAULT NULL,
  `debtor_trans_type` int(11) DEFAULT NULL,
  `stock_id` varchar(20) NOT NULL DEFAULT '',
  `description` tinytext,
  `unit_price` double NOT NULL DEFAULT '0',
  `unit_tax` double NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `discount_percent` double NOT NULL DEFAULT '0',
  `standard_cost` double NOT NULL DEFAULT '0',
  `qty_done` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_debtor_trans_details`
--

/*!40000 ALTER TABLE `0_debtor_trans_details` DISABLE KEYS */;
LOCK TABLES `0_debtor_trans_details` WRITE;
INSERT INTO `ARIA310`.`0_debtor_trans_details` VALUES  (1,1,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,1,0,0,1),
 (2,1,13,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',35000,3750,1,0,0,1),
 (3,1,10,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,1,0,0,0),
 (4,1,10,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',35000,3750,1,0,0,0),
 (5,2,13,'1000-100-100','ARIA ERP SYSTEM',380000,40714.2857,1,0,0,1),
 (6,2,10,'1000-100-100','ARIA ERP SYSTEM',380000,40714.2857,1,0,0,0),
 (7,3,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,10,0,9861.93,0),
 (8,4,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,10,0,9861.93,0),
 (9,5,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,3,0,9864.68,3),
 (10,3,10,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,3,0,9864.68,0),
 (11,6,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,5,0,9871.14,5),
 (12,6,13,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',35000,3750,10,0,9166.67,10),
 (13,6,13,'1000-100-105','CASH DRAWER',6750,723.2143,2,0,4960.71,2),
 (14,4,10,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,5,0,9871.14,0),
 (15,4,10,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',35000,3750,10,0,9166.67,0),
 (16,4,10,'1000-100-105','CASH DRAWER',6750,723.2143,2,0,4960.71,0),
 (17,7,13,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',35000,3750,19,0,9166.67,0),
 (18,8,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,1,0,9871.14,1),
 (19,9,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,2,0,9871.14,0),
 (20,5,10,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,1,0,9871.14,0),
 (21,10,13,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',35000,3750,1,0,15000,1),
 (22,6,10,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',35000,3750,1,0,15000,0),
 (23,11,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,100,0,9882.47,0),
 (24,12,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,1,0,9912.14,0),
 (25,13,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,2,0,9912.14,0),
 (26,14,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,3,0,9912.14,0),
 (27,15,13,'1000-100-104','EPSON POS PRINTER',18000,1928.5714,1,0,9583.27,0),
 (28,15,13,'1000-100-105','CASH DRAWER',6750,723.2143,1,0,4960.71,0),
 (29,15,13,'1000-100-107','BIOMETRIC FINGER SCANNING HARDWARE',18500,1982.1429,0,0,3500,0),
 (30,16,13,'SRV00001','Service',650,69.6429,104,0,0,0),
 (31,17,13,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',35000,3750,50,0,15000,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_debtor_trans_details` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_debtors_master`
--

DROP TABLE IF EXISTS `ARIA310`.`0_debtors_master`;
CREATE TABLE  `ARIA310`.`0_debtors_master` (
  `debtor_no` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `debtor_ref` varchar(30) NOT NULL,
  `address` tinytext,
  `email` varchar(100) NOT NULL DEFAULT '',
  `tax_id` varchar(55) NOT NULL DEFAULT '',
  `curr_code` char(3) NOT NULL DEFAULT '',
  `sales_type` int(11) NOT NULL DEFAULT '1',
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  `credit_status` int(11) NOT NULL DEFAULT '0',
  `payment_terms` int(11) DEFAULT NULL,
  `discount` double NOT NULL DEFAULT '0',
  `pymt_discount` double NOT NULL DEFAULT '0',
  `credit_limit` float NOT NULL DEFAULT '1000',
  `notes` tinytext NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`debtor_no`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_debtors_master`
--

/*!40000 ALTER TABLE `0_debtors_master` DISABLE KEYS */;
LOCK TABLES `0_debtors_master` WRITE;
INSERT INTO `ARIA310`.`0_debtors_master` VALUES  (1,'MICROMATIC INDUSTRIES INC','MMI','PARANAQUE CITY','@','','PHP',1,0,0,1,4,0,0,1000,'',0),
 (2,'PRIME PEAK PROPERTIES LTD INC','3PLI','ORTIGAS CENTER, PASIG','@','','PHP',1,0,0,3,4,0,0,1000,'',0),
 (3,'STAINLESS STEEL INTERNATIONAL SALES INC','SSIS','PARANAQUE CITY','@','','PHP',1,0,0,1,4,0,0,1000,'',0),
 (4,'BON INDUSTRIAL SALES INC','BON','QUEZON CITY','@','','PHP',1,0,0,1,4,0,0,1000,'',0),
 (5,'FARMACIA SRA. DEL PILAR','DEL PILAR','ILOCOS SUR','@','','PHP',1,0,0,1,4,0,0,1000,'',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_debtors_master` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_dimensions`
--

DROP TABLE IF EXISTS `ARIA310`.`0_dimensions`;
CREATE TABLE  `ARIA310`.`0_dimensions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference` varchar(60) NOT NULL DEFAULT '',
  `name` varchar(60) NOT NULL DEFAULT '',
  `type_` tinyint(1) NOT NULL DEFAULT '1',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  `due_date` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `reference` (`reference`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_dimensions`
--

/*!40000 ALTER TABLE `0_dimensions` DISABLE KEYS */;
LOCK TABLES `0_dimensions` WRITE;
INSERT INTO `ARIA310`.`0_dimensions` VALUES  (1,'1','Vehicles',1,0,'2011-06-04','2011-06-24'),
 (2,'2','Trucks',2,0,'2011-06-04','2011-06-24'),
 (3,'3','Cars',2,0,'2011-06-04','2011-06-24'),
 (4,'4','BRANCH 1',1,0,'2011-07-14','2011-08-03'),
 (5,'5','BRANCH 2',1,0,'2011-07-14','2011-08-03'),
 (6,'6','BRANCH 4',1,0,'2011-07-14','2011-08-03'),
 (7,'7','Project 1',1,0,'2011-08-02','2011-08-22'),
 (8,'8','Project 2',1,0,'2011-08-02','2011-08-22'),
 (9,'9','Project 1 - Unilab',2,0,'2011-08-02','2011-08-22'),
 (10,'10','Project 1 - Test',2,0,'2011-08-02','2011-08-22');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_dimensions` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_exchange_rates`
--

DROP TABLE IF EXISTS `ARIA310`.`0_exchange_rates`;
CREATE TABLE  `ARIA310`.`0_exchange_rates` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curr_code` char(3) NOT NULL DEFAULT '',
  `rate_buy` double NOT NULL DEFAULT '0',
  `rate_sell` double NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `curr_code` (`curr_code`,`date_`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_exchange_rates`
--

/*!40000 ALTER TABLE `0_exchange_rates` DISABLE KEYS */;
LOCK TABLES `0_exchange_rates` WRITE;
INSERT INTO `ARIA310`.`0_exchange_rates` VALUES  (1,'USD',46.6630380171,46.6630380171,'2010-06-09'),
 (2,'USD',43.009764093792,43.009764093792,'2010-10-26'),
 (3,'USD',43.101681460634,43.101681460634,'2010-10-28'),
 (4,'USD',44.036784641313,44.036784641313,'2010-11-23'),
 (5,'USD',44.329678638941,44.329678638941,'2010-11-29'),
 (6,'USD',43.418742921857,43.418742921857,'2011-03-25'),
 (7,'USD',42.6388693573,42.6388693573,'2011-07-22'),
 (8,'USD',41.9347901492,41.9347901492,'2011-08-02');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_exchange_rates` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_fiscal_year`
--

DROP TABLE IF EXISTS `ARIA310`.`0_fiscal_year`;
CREATE TABLE  `ARIA310`.`0_fiscal_year` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `begin` date DEFAULT '0000-00-00',
  `end` date DEFAULT '0000-00-00',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_fiscal_year`
--

/*!40000 ALTER TABLE `0_fiscal_year` DISABLE KEYS */;
LOCK TABLES `0_fiscal_year` WRITE;
INSERT INTO `ARIA310`.`0_fiscal_year` VALUES  (1,'2008-01-01','2008-12-31',1),
 (2,'2009-01-01','2009-12-31',1),
 (3,'2010-01-01','2010-12-31',0),
 (4,'2011-01-01','2011-12-31',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_fiscal_year` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_gl_trans`
--

DROP TABLE IF EXISTS `ARIA310`.`0_gl_trans`;
CREATE TABLE  `ARIA310`.`0_gl_trans` (
  `counter` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(6) NOT NULL DEFAULT '0',
  `type_no` bigint(16) NOT NULL DEFAULT '1',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `account` varchar(11) NOT NULL DEFAULT '',
  `memo_` tinytext NOT NULL,
  `amount` double NOT NULL DEFAULT '0',
  `dimension_id` int(11) NOT NULL DEFAULT '0',
  `dimension2_id` int(11) NOT NULL DEFAULT '0',
  `person_type_id` int(11) DEFAULT NULL,
  `person_id` tinyblob,
  PRIMARY KEY (`counter`),
  KEY `Type_and_Number` (`type`,`type_no`)
) ENGINE=InnoDB AUTO_INCREMENT=186 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_gl_trans`
--

/*!40000 ALTER TABLE `0_gl_trans` DISABLE KEYS */;
LOCK TABLES `0_gl_trans` WRITE;
INSERT INTO `ARIA310`.`0_gl_trans` VALUES  (1,1,1,'2010-12-31','2100','this is a test',443296.79,0,0,3,0x31),
 (2,1,1,'2010-12-31','1065','',-443296.79,0,0,3,0x31),
 (3,2,1,'2010-12-31','1200','',-4432967.86,0,0,2,0x34),
 (4,2,1,'2010-12-31','1065','',4432967.86,0,0,2,0x34),
 (5,10,1,'2010-12-31','4010','',-16071.43,0,0,2,0x32),
 (6,10,1,'2010-12-31','4010','',-31250,0,0,2,0x32),
 (7,10,1,'2010-12-31','1200','',53000,0,0,2,0x32),
 (8,10,1,'2010-12-31','2150','',-5678.57,0,0,2,0x32),
 (9,10,2,'2010-12-31','4010','',-339285.71,0,0,2,0x34),
 (10,10,2,'2010-12-31','1200','',380000,0,0,2,0x34),
 (11,10,2,'2010-12-31','2150','',-40714.29,0,0,2,0x34),
 (12,12,1,'2010-12-31','1065','',380000,0,0,2,0x34),
 (13,12,1,'2010-12-31','1200','',-380000,0,0,2,0x34),
 (14,17,2,'2011-03-14','5040','',-10000000,0,0,NULL,NULL),
 (15,17,2,'2011-03-14','1510','',10000000,0,0,NULL,NULL),
 (16,13,3,'2011-03-14','5010','',98619.3,0,0,2,0x32),
 (17,13,3,'2011-03-14','1510','',-98619.3,0,0,2,0x32),
 (18,13,4,'2011-03-14','5010','',98619.3,0,0,2,0x32),
 (19,13,4,'2011-03-14','1510','',-98619.3,0,0,2,0x32),
 (20,26,1,'2011-03-14','1510','',-49309.65,0,0,NULL,NULL),
 (21,26,1,'2011-03-14','1510','',-4930.97,0,0,NULL,NULL),
 (22,26,1,'2011-03-14','1520','',54240.62,0,0,NULL,NULL),
 (23,26,1,'2011-03-14','1065','Labour Cost',-1000,0,0,1,0x30),
 (24,26,1,'2011-03-14','1520','Labour Cost',1000,0,0,1,0x30),
 (25,26,1,'2011-03-14','1510','',-19723.86,0,0,NULL,NULL),
 (26,26,1,'2011-03-14','1510','',-1972.39,0,0,NULL,NULL),
 (27,26,1,'2011-03-14','1520','',21696.25,0,0,NULL,NULL),
 (28,26,1,'2011-03-14','1510','',-29585.79,0,0,NULL,NULL),
 (29,26,1,'2011-03-14','1510','',-14880.96,0,0,NULL,NULL),
 (30,26,1,'2011-03-14','1510','',-2958.58,0,0,NULL,NULL),
 (31,26,1,'2011-03-14','1520','',47425.33,0,0,NULL,NULL),
 (32,20,1,'2011-03-24','2100','',-5712000,0,0,3,0x31),
 (33,20,1,'2011-03-24','1510','',5000000,0,0,3,0x31),
 (34,20,1,'2011-03-24','1510','',100000,0,0,3,0x31),
 (35,20,1,'2011-03-24','2145','',612000,0,0,3,0x31),
 (36,20,2,'2011-03-26','2100','',-392000,0,0,3,0x31),
 (37,20,2,'2011-03-26','1510','',100000,0,0,3,0x31),
 (38,20,2,'2011-03-26','1510','',50000,0,0,3,0x31),
 (39,20,2,'2011-03-26','1510','',150000,0,0,3,0x31),
 (40,20,2,'2011-03-26','1510','',50000,0,0,3,0x31),
 (41,20,2,'2011-03-26','2145','',42000,0,0,3,0x31),
 (42,13,5,'2011-03-26','5010','',29594.04,0,0,2,0x32),
 (43,13,5,'2011-03-26','1510','',-29594.04,0,0,2,0x32),
 (44,10,3,'2011-03-26','4010','',-48214.29,0,0,2,0x32),
 (45,10,3,'2011-03-26','1200','',54000,0,0,2,0x32),
 (46,10,3,'2011-03-26','2150','',-5785.71,0,0,2,0x32),
 (47,26,2,'2011-03-26','1510','',-29594.04,0,0,NULL,NULL),
 (48,26,2,'2011-03-26','1510','',-14882.13,0,0,NULL,NULL),
 (49,26,2,'2011-03-26','1510','',-18750,0,0,NULL,NULL),
 (50,26,2,'2011-03-26','1510','',63226.17,0,0,NULL,NULL),
 (51,26,2,'2011-03-26','1065','Labour Cost',-5000,0,0,1,0x30),
 (52,26,2,'2011-03-26','1530','Labour Cost',5000,0,0,1,0x30),
 (53,26,2,'2011-04-12','1510','',-9871.14,0,0,NULL,NULL),
 (54,26,2,'2011-04-12','1510','',-4960.71,0,0,NULL,NULL),
 (55,26,2,'2011-04-12','1510','',-6250,0,0,NULL,NULL),
 (56,26,2,'2011-04-12','1510','',21081.85,0,0,NULL,NULL),
 (57,26,2,'2011-04-12','1065','Overhead Cost',-1500,0,0,1,0x31),
 (58,26,2,'2011-04-12','1530','Overhead Cost',1500,0,0,1,0x31),
 (59,13,6,'2011-04-12','5010','',49355.7,0,0,2,0x32),
 (60,13,6,'2011-04-12','1510','',-49355.7,0,0,2,0x32),
 (61,13,6,'2011-04-12','5010','',91666.7,0,0,2,0x32),
 (62,13,6,'2011-04-12','1510','',-91666.7,0,0,2,0x32),
 (63,13,6,'2011-04-12','5010','',9921.42,0,0,2,0x32),
 (64,13,6,'2011-04-12','1510','',-9921.42,0,0,2,0x32),
 (65,10,4,'2011-04-12','4010','',-80357.14,0,0,2,0x32),
 (66,10,4,'2011-04-12','4010','',-312500,0,0,2,0x32),
 (67,10,4,'2011-04-12','4010','',-12053.57,0,0,2,0x32),
 (68,10,4,'2011-04-12','1200','',453500,0,0,2,0x32),
 (69,10,4,'2011-04-12','2150','',-48589.29,0,0,2,0x32),
 (70,26,4,'2011-04-12','1510','',-9871.14,0,0,NULL,NULL),
 (71,26,4,'2011-04-12','1510','',-4960.71,0,0,NULL,NULL),
 (72,26,4,'2011-04-12','1510','',-9166.67,0,0,NULL,NULL),
 (73,26,4,'2011-04-12','1510','',23998.52,0,0,NULL,NULL),
 (74,26,4,'2011-04-12','1065','Overhead Cost',-1500,0,0,1,0x31),
 (75,26,4,'2011-04-12','1530','Overhead Cost',1500,0,0,1,0x31),
 (76,13,7,'2011-04-12','5010','',174166.73,0,0,2,0x32),
 (77,13,7,'2011-04-12','1510','',-174166.73,0,0,2,0x32),
 (78,13,8,'2011-05-06','5010','',9871.14,0,0,2,0x34),
 (79,13,8,'2011-05-06','1510','',-9871.14,0,0,2,0x34),
 (80,13,9,'2011-05-06','5010','',19742.28,0,0,2,0x34),
 (81,13,9,'2011-05-06','1510','',-19742.28,0,0,2,0x34),
 (82,10,5,'2011-05-06','4010','',-16071.43,0,0,2,0x34),
 (83,10,5,'2011-05-06','1200','',18000,0,0,2,0x34),
 (84,10,5,'2011-05-06','2150','',-1928.57,0,0,2,0x34),
 (85,20,3,'2011-05-19','2100','',-2744000,0,0,3,0x31),
 (86,20,3,'2011-05-19','1510','',1500000,0,0,3,0x31),
 (87,20,3,'2011-05-19','1510','',500000,0,0,3,0x31),
 (88,20,3,'2011-05-19','1510','',150000,0,0,3,0x31),
 (89,20,3,'2011-05-19','5010','Cost diff.',169166.57,0,0,NULL,NULL),
 (90,20,3,'2011-05-19','1510','Cost diff.',-169166.57,0,0,NULL,NULL),
 (91,20,3,'2011-05-19','1510','',300000,0,0,3,0x31),
 (92,20,3,'2011-05-19','2145','',294000,0,0,3,0x31),
 (93,13,10,'2011-05-19','5010','',15000,0,0,2,0x34),
 (94,13,10,'2011-05-19','1510','',-15000,0,0,2,0x34),
 (95,10,6,'2011-05-19','4010','',-31250,0,0,2,0x34),
 (96,10,6,'2011-05-19','1200','',35000,0,0,2,0x34),
 (97,10,6,'2011-05-19','2150','',-3750,0,0,2,0x34),
 (98,12,2,'2011-05-19','1065','',38000,0,0,2,0x34),
 (99,12,2,'2011-05-19','1200','',-38000,0,0,2,0x34),
 (100,0,1,'2011-05-25','10000','',-50000,0,0,NULL,NULL),
 (101,0,1,'2011-05-25','1065','',50000,0,0,NULL,NULL),
 (102,20,4,'2011-06-04','2100','',-1120000,0,0,3,0x31),
 (103,20,4,'2011-06-04','1510','',1000000,0,0,3,0x31),
 (104,20,4,'2011-06-04','2145','',120000,0,0,3,0x31),
 (105,12,3,'2011-07-29','1065','',15000,0,0,2,0x34),
 (106,12,3,'2011-07-29','1200','',-15000,0,0,2,0x34),
 (107,13,11,'2011-06-28','5010','',988247,0,0,2,0x34),
 (108,13,11,'2011-06-28','1510','',-988247,0,0,2,0x34),
 (109,20,5,'2011-06-28','2100','',-1120000,0,0,3,0x31),
 (110,20,5,'2011-06-28','1510','',1000000,0,0,3,0x31),
 (111,20,5,'2011-06-28','5010','Cost diff.',1034,0,0,NULL,NULL),
 (112,20,5,'2011-06-28','1510','Cost diff.',-1034,0,0,NULL,NULL),
 (113,20,5,'2011-06-28','2145','',120000,0,0,3,0x31),
 (114,20,6,'2011-07-06','2100','',-1120000,0,0,3,0x31),
 (115,20,6,'2011-07-06','1510','',1000000,0,0,3,0x31),
 (116,20,6,'2011-07-06','2145','',120000,0,0,3,0x31),
 (117,26,5,'2011-07-06','1065','Labour Cost',-500,0,0,1,0x30),
 (118,26,5,'2011-07-06','1840','Labour Cost',500,0,0,1,0x30),
 (119,13,12,'2011-07-07','5010','',9912.14,0,0,2,0x34),
 (120,13,12,'2011-07-07','1510','',-9912.14,0,0,2,0x34),
 (121,13,13,'2011-07-07','5010','',19824.28,0,0,2,0x34),
 (122,13,13,'2011-07-07','1510','',-19824.28,0,0,2,0x34),
 (123,13,14,'2011-07-07','5010','',29736.42,0,0,2,0x34),
 (124,13,14,'2011-07-07','1510','',-29736.42,0,0,2,0x34),
 (125,22,1,'2011-07-07','2100','',1000000,0,0,3,0x33),
 (126,22,1,'2011-07-07','1065','',-1000000,0,0,3,0x33),
 (127,20,7,'2011-07-07','2100','',-28000,0,0,3,0x33),
 (128,20,7,'2011-07-07','1510','',25000,0,0,3,0x33),
 (129,20,7,'2011-07-07','5010','Cost diff.',-1973.22,0,0,NULL,NULL),
 (130,20,7,'2011-07-07','1510','Cost diff.',1973.22,0,0,NULL,NULL),
 (131,20,7,'2011-07-07','2145','',3000,0,0,3,0x33),
 (132,22,2,'2011-07-07','2100','',28000,0,0,3,0x33),
 (133,22,2,'2011-07-07','1065','',-28000,0,0,3,0x33),
 (134,22,3,'2011-07-07','2100','',600000,0,0,3,0x33),
 (135,22,3,'2011-07-07','1065','',-600000,0,0,3,0x33),
 (136,22,4,'2011-07-07','2100','',600000,0,0,3,0x33),
 (137,22,4,'2011-07-07','1060','',-600000,0,0,3,0x33),
 (138,26,6,'2011-07-07','1065','Overhead Cost',-50000,0,0,1,0x31),
 (139,26,6,'2011-07-07','1530','Overhead Cost',50000,0,0,1,0x31),
 (140,26,7,'2011-07-07','1510','',-95832.7,0,0,NULL,NULL),
 (141,26,7,'2011-07-07','1510','',-49607.1,0,0,NULL,NULL),
 (142,26,7,'2011-07-07','1510','',-150000,0,0,NULL,NULL),
 (143,26,7,'2011-07-07','1510','',295439.8,0,0,NULL,NULL),
 (144,13,15,'2011-07-12','5010','',9583.27,0,0,2,0x34),
 (145,13,15,'2011-07-12','1510','',-9583.27,0,0,2,0x34),
 (146,13,15,'2011-07-12','5010','',4960.71,0,0,2,0x34),
 (147,13,15,'2011-07-12','1510','',-4960.71,0,0,2,0x34),
 (148,20,8,'2011-07-14','2100','',-2940000,0,0,3,0x31),
 (149,20,8,'2011-07-14','1510','',1000000,0,0,3,0x31),
 (150,20,8,'2011-07-14','5010','Cost diff.',191.94,0,0,NULL,NULL),
 (151,20,8,'2011-07-14','1510','Cost diff.',-191.94,0,0,NULL,NULL),
 (152,20,8,'2011-07-14','1510','',500000,0,0,3,0x31),
 (153,20,8,'2011-07-14','1510','',125000,0,0,3,0x31),
 (154,20,8,'2011-07-14','1510','',1000000,0,0,3,0x31),
 (155,20,8,'2011-07-14','2145','',315000,0,0,3,0x31),
 (156,22,5,'2011-07-14','2100','',5712000,0,0,3,0x31),
 (157,22,5,'2011-07-14','1060','',-5712000,0,0,3,0x31),
 (158,0,2,'2011-07-14','2620','2',-3000,0,0,NULL,NULL),
 (159,0,2,'2011-07-14','1840','2',3000,5,0,NULL,NULL),
 (160,0,3,'2011-08-14','2620','3',-3000,0,0,NULL,NULL),
 (161,0,3,'2011-08-14','1840','3',3000,5,0,NULL,NULL),
 (162,0,4,'2011-09-14','2620','4',-3000,0,0,NULL,NULL),
 (163,0,4,'2011-09-14','1840','4',3000,5,0,NULL,NULL),
 (164,0,5,'2011-10-14','2620','5',-3000,0,0,NULL,NULL),
 (165,0,5,'2011-10-14','1840','5',3000,5,0,NULL,NULL),
 (166,0,6,'2011-11-14','2620','6',-3000,0,0,NULL,NULL),
 (167,0,6,'2011-11-14','1840','6',3000,5,0,NULL,NULL),
 (168,17,3,'2011-07-14','5040','',48053.45,0,0,NULL,NULL),
 (169,17,3,'2011-07-14','1510','',-48053.45,0,0,NULL,NULL),
 (170,20,9,'2011-07-22','2100','',-560000,0,0,3,0x31),
 (171,20,9,'2011-07-22','1510','',500000,0,0,3,0x31),
 (172,20,9,'2011-07-22','2145','',60000,0,0,3,0x31),
 (173,22,6,'2011-07-22','2100','',560000,0,0,3,0x31),
 (174,22,6,'2011-07-22','1060','',-560000,0,0,3,0x31),
 (175,1,2,'2011-07-22','5685','',55000,0,0,0,0x746573742064697362757273656D656E74),
 (176,1,2,'2011-07-22','1060','',-55000,0,0,0,0x746573742064697362757273656D656E74),
 (177,20,10,'2011-08-02','2100','',-1120000,0,0,3,0x31),
 (178,20,10,'2011-08-02','1510','',1000000,0,0,3,0x31),
 (179,20,10,'2011-08-02','2145','',120000,0,0,3,0x31),
 (180,22,7,'2011-08-02','2100','',1120000,0,0,3,0x31),
 (181,22,7,'2011-08-02','1065','',-1120000,0,0,3,0x31),
 (182,22,8,'2011-08-02','2100','',2940000,0,0,3,0x31),
 (183,22,8,'2011-08-02','1060','',-2940000,0,0,3,0x31),
 (184,13,17,'2011-08-12','5010','',750000,0,0,2,0x34),
 (185,13,17,'2011-08-12','1510','',-750000,0,0,2,0x34);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_gl_trans` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_grn_batch`
--

DROP TABLE IF EXISTS `ARIA310`.`0_grn_batch`;
CREATE TABLE  `ARIA310`.`0_grn_batch` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `purch_order_no` int(11) DEFAULT NULL,
  `reference` varchar(60) NOT NULL DEFAULT '',
  `delivery_date` date NOT NULL DEFAULT '0000-00-00',
  `loc_code` varchar(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_grn_batch`
--

/*!40000 ALTER TABLE `0_grn_batch` DISABLE KEYS */;
LOCK TABLES `0_grn_batch` WRITE;
INSERT INTO `ARIA310`.`0_grn_batch` VALUES  (1,1,1,'1','2011-03-14','DEF'),
 (2,1,2,'2','2011-03-24','DEF'),
 (3,1,3,'3','2011-03-26','DEF'),
 (4,1,4,'4','2011-04-12','DEF'),
 (5,1,5,'5','2011-04-12','DEF'),
 (6,1,6,'6','2011-04-12','DEF'),
 (7,1,7,'7','2011-05-19','DEF'),
 (8,1,8,'8','2011-06-04','DEF'),
 (9,1,9,'9','2011-06-28','DEF'),
 (10,1,10,'10','2011-07-06','DEF'),
 (11,1,11,'11','2011-07-07','LOC1'),
 (12,1,13,'12','2011-07-07','DEF'),
 (13,3,14,'13','2011-07-07','DEF'),
 (14,1,15,'14','2011-07-08','DEF'),
 (15,1,16,'15','2011-07-14','DEF'),
 (16,1,13,'16','2011-07-22','DEF'),
 (17,1,17,'17','2011-08-02','DEF');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_grn_batch` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_grn_items`
--

DROP TABLE IF EXISTS `ARIA310`.`0_grn_items`;
CREATE TABLE  `ARIA310`.`0_grn_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grn_batch_id` int(11) DEFAULT NULL,
  `po_detail_item` int(11) NOT NULL DEFAULT '0',
  `item_code` varchar(20) NOT NULL DEFAULT '',
  `description` tinytext,
  `qty_recd` double NOT NULL DEFAULT '0',
  `quantity_inv` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_grn_items`
--

/*!40000 ALTER TABLE `0_grn_items` DISABLE KEYS */;
LOCK TABLES `0_grn_items` WRITE;
INSERT INTO `ARIA310`.`0_grn_items` VALUES  (1,1,1,'1000-100-105','CASH DRAWER',1000,1000),
 (2,2,2,'1000-100-104','EPSON POS PRINTER',10,10),
 (3,3,3,'1000-100-104','EPSON POS PRINTER',10,10),
 (4,3,4,'1000-100-105','CASH DRAWER',10,10),
 (5,3,5,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',10,10),
 (6,3,6,'1000-100-107','BIOMETRIC FINGER SCANNING HARDWARE',10,10),
 (7,4,7,'1000-100-104','EPSON POS PRINTER',50,50),
 (8,5,8,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',10,10),
 (9,6,9,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',20,20),
 (10,7,10,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',100,100),
 (11,8,11,'1000-100-104','EPSON POS PRINTER',100,100),
 (12,9,12,'1000-100-104','EPSON POS PRINTER',100,100),
 (13,10,13,'1000-100-104','EPSON POS PRINTER',100,100),
 (14,11,14,'1000-100-104','EPSON POS PRINTER',100,100),
 (15,12,16,'1000-100-104','EPSON POS PRINTER',50,50),
 (16,13,17,'1000-100-104','EPSON POS PRINTER',50,50),
 (17,14,18,'1000-100-107','BIOMETRIC FINGER SCANNING HARDWARE',25,25),
 (18,15,19,'1000-100-104','EPSON POS PRINTER',100,100),
 (19,16,16,'1000-100-104','EPSON POS PRINTER',50,50),
 (20,17,20,'1000-100-104','EPSON POS PRINTER',100,100);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_grn_items` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_groups`
--

DROP TABLE IF EXISTS `ARIA310`.`0_groups`;
CREATE TABLE  `ARIA310`.`0_groups` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(60) NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `description` (`description`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_groups`
--

/*!40000 ALTER TABLE `0_groups` DISABLE KEYS */;
LOCK TABLES `0_groups` WRITE;
INSERT INTO `ARIA310`.`0_groups` VALUES  (1,'Small',0),
 (2,'Medium',0),
 (3,'Large',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_groups` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_item_codes`
--

DROP TABLE IF EXISTS `ARIA310`.`0_item_codes`;
CREATE TABLE  `ARIA310`.`0_item_codes` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `item_code` varchar(20) NOT NULL,
  `stock_id` varchar(20) NOT NULL,
  `description` varchar(200) NOT NULL DEFAULT '',
  `category_id` smallint(6) unsigned NOT NULL,
  `quantity` double NOT NULL DEFAULT '1',
  `is_foreign` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `stock_id` (`stock_id`,`item_code`)
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_item_codes`
--

/*!40000 ALTER TABLE `0_item_codes` DISABLE KEYS */;
LOCK TABLES `0_item_codes` WRITE;
INSERT INTO `ARIA310`.`0_item_codes` VALUES  (1,'1000-100-100','1000-100-100','ARIA ERP SYSTEM',3,1,0,0),
 (2,'1000-100-101','1000-100-101','EZRA POS SYSTEM',3,1,0,0),
 (3,'1000-100-102','1000-100-102','KAIA HRIS SYSTEM',3,1,0,0),
 (4,'1000-100-103','1000-100-103','ERITHEIA PAYROLL SYSTEM',3,1,0,0),
 (5,'1000-100-104','1000-100-104','EPSON POS PRINTER',1,1,0,0),
 (6,'1000-100-105','1000-100-105','CASH DRAWER',1,1,0,0),
 (7,'1000-100-106','1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',1,1,0,0),
 (8,'1000-100-107','1000-100-107','BIOMETRIC FINGER SCANNING HARDWARE',1,1,0,0),
 (9,'1000-100-108','1000-100-108','DELL POWEREDGE 2950 RACKSERVER',1,1,0,0),
 (10,'1000-100-109','1000-100-109','DELL LATITUDE NOTEBOOK',1,1,0,0),
 (11,'qqq111','qqq111','fsdfsdf',3,1,0,0),
 (12,'QQQ211','QQQ211','rei alison',3,1,0,0),
 (13,'qqq333','qqq333','rei alison',3,1,0,0),
 (14,'ITEM00001','ITEM00001','Manufactured Item',1,1,0,0),
 (15,'123','1000-100-105','test',1,1000,0,0),
 (16,'123','1000-100-106','test',1,1,0,0),
 (17,'123','1000-100-107','test',1,1,0,0),
 (18,'sample','1000-100-104','printer',1,1,0,0),
 (19,'sample','1000-100-105','printer',1,1,0,0),
 (20,'sample','1000-100-106','printer',1,1,0,0),
 (21,'SRV00001','SRV00001','Service',4,1,0,0),
 (22,'SRV00002','SRV00002','Service Two',4,1,0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_item_codes` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_item_tax_type_exemptions`
--

DROP TABLE IF EXISTS `ARIA310`.`0_item_tax_type_exemptions`;
CREATE TABLE  `ARIA310`.`0_item_tax_type_exemptions` (
  `item_tax_type_id` int(11) NOT NULL DEFAULT '0',
  `tax_type_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`item_tax_type_id`,`tax_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_item_tax_type_exemptions`
--

/*!40000 ALTER TABLE `0_item_tax_type_exemptions` DISABLE KEYS */;
LOCK TABLES `0_item_tax_type_exemptions` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_item_tax_type_exemptions` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_item_tax_types`
--

DROP TABLE IF EXISTS `ARIA310`.`0_item_tax_types`;
CREATE TABLE  `ARIA310`.`0_item_tax_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '',
  `exempt` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_item_tax_types`
--

/*!40000 ALTER TABLE `0_item_tax_types` DISABLE KEYS */;
LOCK TABLES `0_item_tax_types` WRITE;
INSERT INTO `ARIA310`.`0_item_tax_types` VALUES  (1,'VATABLE',0,0),
 (2,'NON VAT',1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_item_tax_types` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_item_units`
--

DROP TABLE IF EXISTS `ARIA310`.`0_item_units`;
CREATE TABLE  `ARIA310`.`0_item_units` (
  `abbr` varchar(20) NOT NULL,
  `name` varchar(40) NOT NULL,
  `decimals` tinyint(2) NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`abbr`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_item_units`
--

/*!40000 ALTER TABLE `0_item_units` DISABLE KEYS */;
LOCK TABLES `0_item_units` WRITE;
INSERT INTO `ARIA310`.`0_item_units` VALUES  ('PC','PIECES',0,0),
 ('LOT','LOT',-1,0),
 ('KG','Kilograms',2,0),
 ('HRS','MAN HOURS',2,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_item_units` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_loc_stock`
--

DROP TABLE IF EXISTS `ARIA310`.`0_loc_stock`;
CREATE TABLE  `ARIA310`.`0_loc_stock` (
  `loc_code` char(5) NOT NULL DEFAULT '',
  `stock_id` char(20) NOT NULL DEFAULT '',
  `reorder_level` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`loc_code`,`stock_id`),
  KEY `stock_id` (`stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_loc_stock`
--

/*!40000 ALTER TABLE `0_loc_stock` DISABLE KEYS */;
LOCK TABLES `0_loc_stock` WRITE;
INSERT INTO `ARIA310`.`0_loc_stock` VALUES  ('DEF','1000-100-100',0),
 ('DEF','1000-100-101',0),
 ('DEF','1000-100-102',0),
 ('DEF','1000-100-103',0),
 ('DEF','1000-100-104',0),
 ('DEF','1000-100-105',0),
 ('DEF','1000-100-106',0),
 ('DEF','1000-100-107',0),
 ('DEF','1000-100-108',0),
 ('DEF','1000-100-109',0),
 ('DEF','ITEM00001',0),
 ('DEF','qqq111',0),
 ('DEF','QQQ211',0),
 ('DEF','qqq333',0),
 ('DEF','SRV00001',0),
 ('DEF','SRV00002',0),
 ('LOC1','1000-100-100',0),
 ('LOC1','1000-100-101',0),
 ('LOC1','1000-100-102',0),
 ('LOC1','1000-100-103',0),
 ('LOC1','1000-100-104',0),
 ('LOC1','1000-100-105',0),
 ('LOC1','1000-100-106',0),
 ('LOC1','1000-100-107',0),
 ('LOC1','1000-100-108',0),
 ('LOC1','1000-100-109',0),
 ('LOC1','ITEM00001',0),
 ('LOC1','qqq111',0),
 ('LOC1','QQQ211',0),
 ('LOC1','qqq333',0),
 ('LOC1','SRV00001',0),
 ('LOC1','SRV00002',0),
 ('LOC2','1000-100-100',0),
 ('LOC2','1000-100-101',0),
 ('LOC2','1000-100-102',0),
 ('LOC2','1000-100-103',0),
 ('LOC2','1000-100-104',0),
 ('LOC2','1000-100-105',0),
 ('LOC2','1000-100-106',0),
 ('LOC2','1000-100-107',0),
 ('LOC2','1000-100-108',0),
 ('LOC2','1000-100-109',0),
 ('LOC2','ITEM00001',0),
 ('LOC2','qqq111',0),
 ('LOC2','QQQ211',0),
 ('LOC2','qqq333',0),
 ('LOC2','SRV00001',0),
 ('LOC2','SRV00002',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_loc_stock` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_locations`
--

DROP TABLE IF EXISTS `ARIA310`.`0_locations`;
CREATE TABLE  `ARIA310`.`0_locations` (
  `loc_code` varchar(5) NOT NULL DEFAULT '',
  `location_name` varchar(60) NOT NULL DEFAULT '',
  `delivery_address` tinytext NOT NULL,
  `phone` varchar(30) NOT NULL DEFAULT '',
  `phone2` varchar(30) NOT NULL DEFAULT '',
  `fax` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `contact` varchar(30) NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`loc_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_locations`
--

/*!40000 ALTER TABLE `0_locations` DISABLE KEYS */;
LOCK TABLES `0_locations` WRITE;
INSERT INTO `ARIA310`.`0_locations` VALUES  ('DEF','DEFAULT','N/A','','','','','',0),
 ('LOC1','RETAIL STORE','Retail Store','','','','','Contact Person',0),
 ('LOC2','LOCATION TWO','','','','','','',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_locations` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_movement_types`
--

DROP TABLE IF EXISTS `ARIA310`.`0_movement_types`;
CREATE TABLE  `ARIA310`.`0_movement_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_movement_types`
--

/*!40000 ALTER TABLE `0_movement_types` DISABLE KEYS */;
LOCK TABLES `0_movement_types` WRITE;
INSERT INTO `ARIA310`.`0_movement_types` VALUES  (1,'Adjustment',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_movement_types` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_payment_terms`
--

DROP TABLE IF EXISTS `ARIA310`.`0_payment_terms`;
CREATE TABLE  `ARIA310`.`0_payment_terms` (
  `terms_indicator` int(11) NOT NULL AUTO_INCREMENT,
  `terms` char(80) NOT NULL DEFAULT '',
  `days_before_due` smallint(6) NOT NULL DEFAULT '0',
  `day_in_following_month` smallint(6) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`terms_indicator`),
  UNIQUE KEY `terms` (`terms`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_payment_terms`
--

/*!40000 ALTER TABLE `0_payment_terms` DISABLE KEYS */;
LOCK TABLES `0_payment_terms` WRITE;
INSERT INTO `ARIA310`.`0_payment_terms` VALUES  (1,'Due 15th Of the Following Month',0,17,0),
 (2,'Due By End Of The Following Month',0,30,0),
 (3,'Payment due within 10 days',10,0,0),
 (4,'Cash Only',1,0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_payment_terms` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_prices`
--

DROP TABLE IF EXISTS `ARIA310`.`0_prices`;
CREATE TABLE  `ARIA310`.`0_prices` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` varchar(20) NOT NULL DEFAULT '',
  `sales_type_id` int(11) NOT NULL DEFAULT '0',
  `curr_abrev` char(3) NOT NULL DEFAULT '',
  `price` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `price` (`stock_id`,`sales_type_id`,`curr_abrev`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_prices`
--

/*!40000 ALTER TABLE `0_prices` DISABLE KEYS */;
LOCK TABLES `0_prices` WRITE;
INSERT INTO `ARIA310`.`0_prices` VALUES  (1,'1000-100-104',1,'PHP',18000),
 (2,'1000-100-105',1,'PHP',6750),
 (3,'1000-100-106',1,'PHP',35000),
 (4,'1000-100-107',1,'PHP',18500),
 (5,'1000-100-108',1,'PHP',550000),
 (6,'1000-100-109',1,'PHP',87500),
 (7,'1000-100-100',1,'PHP',380000),
 (8,'1000-100-101',1,'PHP',150000),
 (9,'1000-100-102',1,'PHP',380000),
 (10,'1000-100-103',1,'PHP',100000),
 (11,'SRV00002',1,'PHP',650);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_prices` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_print_profiles`
--

DROP TABLE IF EXISTS `ARIA310`.`0_print_profiles`;
CREATE TABLE  `ARIA310`.`0_print_profiles` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `profile` varchar(30) NOT NULL,
  `report` varchar(5) DEFAULT NULL,
  `printer` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile` (`profile`,`report`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_print_profiles`
--

/*!40000 ALTER TABLE `0_print_profiles` DISABLE KEYS */;
LOCK TABLES `0_print_profiles` WRITE;
INSERT INTO `ARIA310`.`0_print_profiles` VALUES  (1,'Out of office','',0),
 (2,'Sales Department','',0),
 (3,'Central','',2),
 (4,'Sales Department','104',2),
 (5,'Sales Department','105',2),
 (6,'Sales Department','107',2),
 (7,'Sales Department','109',2),
 (8,'Sales Department','110',2),
 (9,'Sales Department','201',2);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_print_profiles` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_printers`
--

DROP TABLE IF EXISTS `ARIA310`.`0_printers`;
CREATE TABLE  `ARIA310`.`0_printers` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(60) NOT NULL,
  `queue` varchar(20) NOT NULL,
  `host` varchar(40) NOT NULL,
  `port` smallint(11) unsigned NOT NULL,
  `timeout` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_printers`
--

/*!40000 ALTER TABLE `0_printers` DISABLE KEYS */;
LOCK TABLES `0_printers` WRITE;
INSERT INTO `ARIA310`.`0_printers` VALUES  (1,'QL500','Label printer','QL500','server',127,20),
 (2,'Samsung','Main network printer','scx4521F','server',515,5),
 (3,'Local','Local print server at user IP','lp','',515,10);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_printers` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_purch_data`
--

DROP TABLE IF EXISTS `ARIA310`.`0_purch_data`;
CREATE TABLE  `ARIA310`.`0_purch_data` (
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `stock_id` char(20) NOT NULL DEFAULT '',
  `price` double NOT NULL DEFAULT '0',
  `suppliers_uom` char(50) NOT NULL DEFAULT '',
  `conversion_factor` double NOT NULL DEFAULT '1',
  `supplier_description` char(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`supplier_id`,`stock_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_purch_data`
--

/*!40000 ALTER TABLE `0_purch_data` DISABLE KEYS */;
LOCK TABLES `0_purch_data` WRITE;
INSERT INTO `ARIA310`.`0_purch_data` VALUES  (2,'1000-100-104',15000,'PC',1,'EPTMU110'),
 (2,'1000-100-105',5000,'PC',1,'1002012'),
 (2,'1000-100-106',28000,'PC',1,'PS90182'),
 (3,'1000-100-107',15000,'PC',1,'123998'),
 (1,'1000-100-108',450000,'LOT',1,'DELL1239221'),
 (1,'1000-100-109',77500,'PC',1,'DEL1099232'),
 (1,'1000-100-105',5000,'',1,'CASH DRAWER'),
 (1,'1000-100-104',10000,'',1,'EPSON POS PRINTER'),
 (1,'1000-100-106',15000,'',1,'POSIFLEX TOUCH SCREEN MONITOR'),
 (1,'1000-100-107',5000,'',1,'BIOMETRIC FINGER SCANNING HARDWARE'),
 (3,'1000-100-104',500,'',1,'EPSON POS PRINTER');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_purch_data` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_purch_order_details`
--

DROP TABLE IF EXISTS `ARIA310`.`0_purch_order_details`;
CREATE TABLE  `ARIA310`.`0_purch_order_details` (
  `po_detail_item` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` int(11) NOT NULL DEFAULT '0',
  `item_code` varchar(20) NOT NULL DEFAULT '',
  `description` tinytext,
  `delivery_date` date NOT NULL DEFAULT '0000-00-00',
  `qty_invoiced` double NOT NULL DEFAULT '0',
  `unit_price` double NOT NULL DEFAULT '0',
  `act_price` double NOT NULL DEFAULT '0',
  `std_cost_unit` double NOT NULL DEFAULT '0',
  `quantity_ordered` double NOT NULL DEFAULT '0',
  `quantity_received` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`po_detail_item`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_purch_order_details`
--

/*!40000 ALTER TABLE `0_purch_order_details` DISABLE KEYS */;
LOCK TABLES `0_purch_order_details` WRITE;
INSERT INTO `ARIA310`.`0_purch_order_details` VALUES  (1,1,'1000-100-105','CASH DRAWER','2011-03-24',1000,5000,5000,4960.32,1000,1000),
 (2,2,'1000-100-104','EPSON POS PRINTER','2011-04-03',10,10000,10000,9863.32,10,10),
 (3,3,'1000-100-104','EPSON POS PRINTER','2011-04-05',10,10000,10000,9864.68,10,10),
 (4,3,'1000-100-105','CASH DRAWER','2011-04-05',10,5000,5000,4960.71,10,10),
 (5,3,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR','2011-04-05',10,15000,15000,6250,10,10),
 (6,3,'1000-100-107','BIOMETRIC FINGER SCANNING HARDWARE','2011-04-05',10,5000,5000,2000,10,10),
 (7,4,'1000-100-104','EPSON POS PRINTER','2011-04-22',50,10000,10000,9871.14,50,50),
 (8,5,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR','2011-04-22',10,15000,15000,9166.67,10,10),
 (9,6,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR','2011-04-22',20,15000,15000,15000,50,20),
 (10,7,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR','2011-05-29',100,15000,15000,15000,100,100),
 (11,8,'1000-100-104','EPSON POS PRINTER','2011-06-14',100,10000,10000,9882.47,100,100),
 (12,9,'1000-100-104','EPSON POS PRINTER','2011-07-08',100,10000,10000,9892.81,100,100),
 (13,10,'1000-100-104','EPSON POS PRINTER','2011-07-16',100,10000,10000,9901.48,100,100),
 (14,11,'1000-100-104','EPSON POS PRINTER','2011-07-17',100,10000,10000,9908.85,100,100),
 (15,12,'1000-100-104','EPSON POS PRINTER','2011-07-17',0,10000,0,0,1,0),
 (16,13,'1000-100-104','EPSON POS PRINTER','2011-07-17',100,10000,10000,9912.14,100,100),
 (17,14,'1000-100-104','EPSON POS PRINTER','2011-07-17',50,500,500,9583.27,50,50),
 (18,15,'1000-100-107','BIOMETRIC FINGER SCANNING HARDWARE','2011-07-18',25,5000,5000,3500,25,25),
 (19,16,'1000-100-104','EPSON POS PRINTER','2011-07-24',100,10000,10000,9610.69,100,100),
 (20,17,'1000-100-104','EPSON POS PRINTER','2011-08-12',100,10000,10000,9645.76,100,100);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_purch_order_details` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_purch_orders`
--

DROP TABLE IF EXISTS `ARIA310`.`0_purch_orders`;
CREATE TABLE  `ARIA310`.`0_purch_orders` (
  `order_no` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL DEFAULT '0',
  `comments` tinytext,
  `ord_date` date NOT NULL DEFAULT '0000-00-00',
  `reference` tinytext NOT NULL,
  `requisition_no` tinytext,
  `into_stock_location` varchar(5) NOT NULL DEFAULT '',
  `delivery_address` tinytext NOT NULL,
  PRIMARY KEY (`order_no`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_purch_orders`
--

/*!40000 ALTER TABLE `0_purch_orders` DISABLE KEYS */;
LOCK TABLES `0_purch_orders` WRITE;
INSERT INTO `ARIA310`.`0_purch_orders` VALUES  (1,1,'','2011-03-14','1','','DEF','N/A'),
 (2,1,'','2011-03-24','2','','DEF','N/A'),
 (3,1,'','2011-03-26','3','','DEF','N/A'),
 (4,1,'','2011-04-12','4','','DEF','N/A'),
 (5,1,'','2011-04-12','5','','DEF','N/A'),
 (6,1,'','2011-04-12','6','','DEF','N/A'),
 (7,1,'','2011-05-19','7','','DEF','N/A'),
 (8,1,'','2011-06-04','8','','DEF','N/A'),
 (9,1,'','2011-06-28','9','','DEF','N/A'),
 (10,1,'','2011-07-06','10','','DEF','N/A'),
 (11,1,'','2011-07-07','11','','LOC1','Retail Store'),
 (12,1,'','2011-07-07','12','','DEF','N/A'),
 (13,1,'','2011-07-07','13','','DEF','N/A'),
 (14,3,'','2011-07-07','14','','DEF','N/A'),
 (15,1,'','2011-07-08','15','','DEF','N/A'),
 (16,1,'','2011-07-14','16','','DEF','N/A'),
 (17,1,'','2011-08-02','17','','DEF','N/A');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_purch_orders` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_quick_entries`
--

DROP TABLE IF EXISTS `ARIA310`.`0_quick_entries`;
CREATE TABLE  `ARIA310`.`0_quick_entries` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `description` varchar(60) NOT NULL,
  `base_amount` double NOT NULL DEFAULT '0',
  `base_desc` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `description` (`description`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_quick_entries`
--

/*!40000 ALTER TABLE `0_quick_entries` DISABLE KEYS */;
LOCK TABLES `0_quick_entries` WRITE;
INSERT INTO `ARIA310`.`0_quick_entries` VALUES  (1,1,'Maintenance',0,'Amount'),
 (2,4,'Phone',0,'Amount'),
 (3,2,'Cash Sales',0,'Amount'),
 (4,4,'EWT',0.01,'Base Amount');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_quick_entries` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_quick_entry_lines`
--

DROP TABLE IF EXISTS `ARIA310`.`0_quick_entry_lines`;
CREATE TABLE  `ARIA310`.`0_quick_entry_lines` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `qid` smallint(6) unsigned NOT NULL,
  `amount` double DEFAULT '0',
  `action` varchar(2) NOT NULL,
  `dest_id` varchar(11) NOT NULL,
  `dimension_id` smallint(6) unsigned DEFAULT NULL,
  `dimension2_id` smallint(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `qid` (`qid`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_quick_entry_lines`
--

/*!40000 ALTER TABLE `0_quick_entry_lines` DISABLE KEYS */;
LOCK TABLES `0_quick_entry_lines` WRITE;
INSERT INTO `ARIA310`.`0_quick_entry_lines` VALUES  (1,1,0,'t-','1',0,0),
 (2,2,0,'t-','1',0,0),
 (3,3,0,'t-','1',0,0),
 (4,3,0,'=','4010',0,0),
 (5,1,0,'=','5765',0,0),
 (6,2,0,'=','5780',0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_quick_entry_lines` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_recurrent_invoices`
--

DROP TABLE IF EXISTS `ARIA310`.`0_recurrent_invoices`;
CREATE TABLE  `ARIA310`.`0_recurrent_invoices` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(60) NOT NULL DEFAULT '',
  `order_no` int(11) unsigned NOT NULL,
  `debtor_no` int(11) unsigned DEFAULT NULL,
  `group_no` smallint(6) unsigned DEFAULT NULL,
  `days` int(11) NOT NULL DEFAULT '0',
  `monthly` int(11) NOT NULL DEFAULT '0',
  `begin` date NOT NULL DEFAULT '0000-00-00',
  `end` date NOT NULL DEFAULT '0000-00-00',
  `last_sent` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_recurrent_invoices`
--

/*!40000 ALTER TABLE `0_recurrent_invoices` DISABLE KEYS */;
LOCK TABLES `0_recurrent_invoices` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_recurrent_invoices` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_refs`
--

DROP TABLE IF EXISTS `ARIA310`.`0_refs`;
CREATE TABLE  `ARIA310`.`0_refs` (
  `id` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `reference` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_refs`
--

/*!40000 ALTER TABLE `0_refs` DISABLE KEYS */;
LOCK TABLES `0_refs` WRITE;
INSERT INTO `ARIA310`.`0_refs` VALUES  (1,0,'1'),
 (1,1,'1'),
 (1,2,'1'),
 (1,10,'1'),
 (1,12,'1'),
 (1,13,'1'),
 (1,16,'1'),
 (1,17,'1'),
 (1,18,'1'),
 (1,20,'1'),
 (1,22,'1'),
 (1,25,'1'),
 (1,26,'1'),
 (1,28,'1'),
 (1,29,'1'),
 (1,30,'1'),
 (1,32,'1'),
 (1,40,'1'),
 (2,0,'2'),
 (2,1,'2'),
 (2,10,'2'),
 (2,12,'2'),
 (2,13,'auto'),
 (2,16,'2'),
 (2,17,'2'),
 (2,18,'2'),
 (2,20,'2'),
 (2,22,'2'),
 (2,25,'2'),
 (2,26,'2'),
 (2,28,'2'),
 (2,29,'2'),
 (2,30,'auto'),
 (2,32,'2'),
 (2,40,'2'),
 (3,0,'3'),
 (3,10,'3'),
 (3,12,'3'),
 (3,13,'2'),
 (3,16,'3'),
 (3,17,'3'),
 (3,18,'3'),
 (3,20,'3'),
 (3,22,'3'),
 (3,25,'3'),
 (3,26,'3'),
 (3,28,'3'),
 (3,29,'3'),
 (3,30,'2'),
 (3,32,'3'),
 (3,40,'3'),
 (4,0,'4'),
 (4,10,'4'),
 (4,13,'3'),
 (4,16,'4'),
 (4,18,'4'),
 (4,20,'4'),
 (4,22,'4'),
 (4,25,'4'),
 (4,26,'4'),
 (4,28,'4'),
 (4,29,'4'),
 (4,30,'3'),
 (4,40,'4'),
 (5,0,'5'),
 (5,10,'5'),
 (5,13,'4'),
 (5,16,'5'),
 (5,18,'5'),
 (5,20,'5'),
 (5,22,'5'),
 (5,25,'5'),
 (5,26,'5'),
 (5,28,'5'),
 (5,29,'5'),
 (5,30,'4'),
 (5,40,'5'),
 (6,0,'6'),
 (6,10,'6'),
 (6,13,'5'),
 (6,18,'6'),
 (6,20,'6'),
 (6,22,'6'),
 (6,25,'6'),
 (6,26,'6'),
 (6,28,'6'),
 (6,29,'6'),
 (6,30,'5'),
 (6,40,'6'),
 (7,13,'6'),
 (7,18,'7'),
 (7,20,'7'),
 (7,22,'7'),
 (7,25,'7'),
 (7,26,'7'),
 (7,28,'7'),
 (7,29,'7'),
 (7,30,'6'),
 (7,40,'7'),
 (8,13,'7'),
 (8,18,'8'),
 (8,20,'8'),
 (8,22,'8'),
 (8,25,'8'),
 (8,28,'8'),
 (8,29,'8'),
 (8,30,'7'),
 (8,40,'8'),
 (9,13,'8'),
 (9,18,'9'),
 (9,20,'9'),
 (9,25,'9'),
 (9,28,'9'),
 (9,30,'8'),
 (9,40,'9'),
 (10,13,'9'),
 (10,18,'10'),
 (10,20,'10'),
 (10,25,'10'),
 (10,30,'9'),
 (10,40,'10'),
 (11,13,'10'),
 (11,18,'11'),
 (11,25,'11'),
 (11,30,'10'),
 (12,13,'11'),
 (12,18,'12'),
 (12,25,'12'),
 (12,30,'11'),
 (13,13,'12'),
 (13,18,'13'),
 (13,25,'13'),
 (13,30,'auto'),
 (14,13,'13'),
 (14,18,'14'),
 (14,25,'14'),
 (14,30,'12'),
 (15,13,'14'),
 (15,18,'15'),
 (15,25,'15'),
 (15,30,'13'),
 (16,13,'15'),
 (16,18,'16'),
 (16,25,'16'),
 (16,30,'14'),
 (17,13,'16'),
 (17,18,'17'),
 (17,25,'17'),
 (17,30,'15'),
 (18,30,'auto');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_refs` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_sales_order_details`
--

DROP TABLE IF EXISTS `ARIA310`.`0_sales_order_details`;
CREATE TABLE  `ARIA310`.`0_sales_order_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_no` int(11) NOT NULL DEFAULT '0',
  `trans_type` smallint(6) NOT NULL DEFAULT '30',
  `stk_code` varchar(20) NOT NULL DEFAULT '',
  `description` tinytext,
  `qty_sent` double NOT NULL DEFAULT '0',
  `unit_price` double NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `discount_percent` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_sales_order_details`
--

/*!40000 ALTER TABLE `0_sales_order_details` DISABLE KEYS */;
LOCK TABLES `0_sales_order_details` WRITE;
INSERT INTO `ARIA310`.`0_sales_order_details` VALUES  (1,1,30,'1000-100-104','EPSON POS PRINTER',1,18000,1,0),
 (2,1,30,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',1,35000,1,0),
 (3,2,30,'1000-100-100','ARIA ERP SYSTEM',1,380000,1,0),
 (4,3,30,'1000-100-104','EPSON POS PRINTER',0,18000,100,0),
 (5,4,30,'1000-100-104','EPSON POS PRINTER',20,18000,100,0),
 (6,1,32,'1000-100-105','CASH DRAWER',0,6750,1,0),
 (7,2,32,'1000-100-104','EPSON POS PRINTER',0,18000,3,0),
 (8,5,30,'1000-100-104','EPSON POS PRINTER',3,18000,3,0),
 (9,6,30,'ITEM00001','Manufactured Item',0,500000,1,0),
 (10,3,32,'1000-100-104','EPSON POS PRINTER',0,18000,5,0),
 (11,3,32,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',0,35000,10,0),
 (12,7,30,'1000-100-104','EPSON POS PRINTER',0,18000,5,0),
 (13,7,30,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',0,35000,10,0),
 (14,8,30,'1000-100-104','EPSON POS PRINTER',5,18000,5,0),
 (15,8,30,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',10,35000,10,0),
 (16,8,30,'1000-100-105','CASH DRAWER',2,6750,2,0),
 (17,9,30,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',19,35000,25,0),
 (18,10,30,'1000-100-104','EPSON POS PRINTER',1,18000,1,0),
 (19,11,30,'1000-100-104','EPSON POS PRINTER',2,18000,2,0),
 (20,12,30,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',1,35000,1,0),
 (21,13,30,'1000-100-104','EPSON POS PRINTER',100,18000,100,0),
 (22,14,30,'1000-100-104','EPSON POS PRINTER',1,18000,1,0),
 (23,15,30,'1000-100-104','EPSON POS PRINTER',5,18000,10,0),
 (24,16,30,'1000-100-104','EPSON POS PRINTER',1,18000,1,0),
 (25,16,30,'1000-100-105','CASH DRAWER',1,6750,1,0),
 (26,16,30,'1000-100-107','BIOMETRIC FINGER SCANNING HARDWARE',0,18500,0,0),
 (27,17,30,'SRV00001','Service',104,650,104,0),
 (28,18,30,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',50,35000,50,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_sales_order_details` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_sales_orders`
--

DROP TABLE IF EXISTS `ARIA310`.`0_sales_orders`;
CREATE TABLE  `ARIA310`.`0_sales_orders` (
  `order_no` int(11) NOT NULL,
  `trans_type` smallint(6) NOT NULL DEFAULT '30',
  `version` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `type` tinyint(1) NOT NULL DEFAULT '0',
  `debtor_no` int(11) NOT NULL DEFAULT '0',
  `branch_code` int(11) NOT NULL DEFAULT '0',
  `reference` varchar(100) NOT NULL DEFAULT '',
  `customer_ref` tinytext NOT NULL,
  `comments` tinytext,
  `ord_date` date NOT NULL DEFAULT '0000-00-00',
  `order_type` int(11) NOT NULL DEFAULT '0',
  `ship_via` int(11) NOT NULL DEFAULT '0',
  `delivery_address` tinytext NOT NULL,
  `contact_phone` varchar(30) DEFAULT NULL,
  `contact_email` varchar(100) DEFAULT NULL,
  `deliver_to` tinytext NOT NULL,
  `freight_cost` double NOT NULL DEFAULT '0',
  `from_stk_loc` varchar(5) NOT NULL DEFAULT '',
  `delivery_date` date NOT NULL DEFAULT '0000-00-00',
  `is_consign` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`trans_type`,`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_sales_orders`
--

/*!40000 ALTER TABLE `0_sales_orders` DISABLE KEYS */;
LOCK TABLES `0_sales_orders` WRITE;
INSERT INTO `ARIA310`.`0_sales_orders` VALUES  (1,30,1,0,2,2,'1','','','2010-12-31',1,1,'ORTIGAS CENTER, PASIG','','','PRIME PEAK PROPERTIES LTD INC',0,'DEF','2011-01-01',0),
 (2,30,1,0,4,4,'auto','','','2010-12-31',1,1,'','','','',0,'LOC1','2010-12-31',0),
 (3,30,0,0,2,2,'2','','','2010-12-31',1,1,'ORTIGAS CENTER, PASIG','','','PRIME PEAK PROPERTIES LTD INC',0,'DEF','2011-01-01',0),
 (4,30,2,0,2,2,'3','','','2011-03-14',1,1,'ORTIGAS CENTER, PASIG','','','PRIME PEAK PROPERTIES LTD INC',0,'DEF','2011-03-15',0),
 (5,30,1,0,2,2,'4','','Sales Quotation # 2','2011-03-26',1,1,'ORTIGAS CENTER, PASIG','','','PRIME PEAK PROPERTIES LTD INC',0,'DEF','2011-03-26',0),
 (6,30,0,0,2,2,'5','','','2011-03-26',1,1,'ORTIGAS CENTER, PASIG','','','PRIME PEAK PROPERTIES LTD INC',0,'DEF','2011-03-27',0),
 (7,30,0,0,2,2,'6','','Sales Quotation # 3','2011-04-12',1,1,'ORTIGAS CENTER, PASIG','','','PRIME PEAK PROPERTIES LTD INC',0,'LOC1','2011-04-12',0),
 (8,30,1,0,2,2,'7','','Sales Quotation # 3','2011-04-12',1,1,'ORTIGAS CENTER, PASIG','','','PRIME PEAK PROPERTIES LTD INC',0,'DEF','2011-04-12',0),
 (9,30,1,0,2,2,'8','','','2011-04-12',1,1,'ORTIGAS CENTER, PASIG','','','PRIME PEAK PROPERTIES LTD INC',0,'DEF','2011-04-13',0),
 (10,30,1,0,4,4,'9','','','2011-05-06',1,1,'QUEZON CITY','','','BON INDUSTRIAL SALES INC',0,'DEF','2011-05-06',0),
 (11,30,1,0,4,4,'10','','','2011-05-06',1,1,'QUEZON CITY','','','BON INDUSTRIAL SALES INC',0,'DEF','2011-05-07',0),
 (12,30,1,0,4,4,'11','','','2011-05-19',1,1,'QUEZON CITY','','','BON INDUSTRIAL SALES INC',0,'DEF','2011-05-19',0),
 (13,30,1,0,4,4,'auto','','','2011-06-28',1,1,'QUEZON CITY','','','BON INDUSTRIAL SALES INC',0,'DEF','2011-06-28',0),
 (14,30,1,0,4,4,'12','','','2011-07-07',1,1,'QUEZON CITY','','','BON INDUSTRIAL SALES INC',0,'DEF','2011-07-08',0),
 (15,30,2,0,4,4,'13','','','2011-07-07',1,1,'QUEZON CITY','','','BON INDUSTRIAL SALES INC',0,'DEF','2011-07-07',0),
 (16,30,1,0,4,4,'14','','','2011-07-12',1,1,'QUEZON CITY','','','BON INDUSTRIAL SALES INC',0,'DEF','2011-07-12',0),
 (17,30,1,0,4,4,'15','','','2011-08-02',1,1,'QUEZON CITY','','','BON INDUSTRIAL SALES INC',0,'DEF','2011-08-02',0),
 (18,30,1,0,4,4,'auto','','','2011-08-12',1,1,'QUEZON CITY','','','BON INDUSTRIAL SALES INC',0,'DEF','2011-08-12',0),
 (1,32,0,0,2,2,'1','','','2011-03-18',1,1,'ORTIGAS CENTER, PASIG','','','PRIME PEAK PROPERTIES LTD INC',0,'DEF','2011-03-19',0),
 (2,32,0,0,2,2,'2','','','2011-03-26',1,1,'ORTIGAS CENTER, PASIG','','','PRIME PEAK PROPERTIES LTD INC',0,'DEF','2011-03-27',0),
 (3,32,0,0,2,2,'3','','','2011-04-12',1,1,'ORTIGAS CENTER, PASIG','','','PRIME PEAK PROPERTIES LTD INC',0,'DEF','2011-04-13',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_sales_orders` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_sales_pos`
--

DROP TABLE IF EXISTS `ARIA310`.`0_sales_pos`;
CREATE TABLE  `ARIA310`.`0_sales_pos` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `pos_name` varchar(30) NOT NULL,
  `cash_sale` tinyint(1) NOT NULL,
  `credit_sale` tinyint(1) NOT NULL,
  `pos_location` varchar(5) NOT NULL,
  `pos_account` smallint(6) unsigned NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pos_name` (`pos_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_sales_pos`
--

/*!40000 ALTER TABLE `0_sales_pos` DISABLE KEYS */;
LOCK TABLES `0_sales_pos` WRITE;
INSERT INTO `ARIA310`.`0_sales_pos` VALUES  (1,'Default',1,1,'LOC1',2,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_sales_pos` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_sales_types`
--

DROP TABLE IF EXISTS `ARIA310`.`0_sales_types`;
CREATE TABLE  `ARIA310`.`0_sales_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sales_type` char(50) NOT NULL DEFAULT '',
  `tax_included` int(1) NOT NULL DEFAULT '0',
  `factor` double NOT NULL DEFAULT '1',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `sales_type` (`sales_type`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_sales_types`
--

/*!40000 ALTER TABLE `0_sales_types` DISABLE KEYS */;
LOCK TABLES `0_sales_types` WRITE;
INSERT INTO `ARIA310`.`0_sales_types` VALUES  (1,'Retail',1,1,0),
 (2,'Wholesale',0,0.7,0),
 (3,'SM',0,1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_sales_types` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_salesman`
--

DROP TABLE IF EXISTS `ARIA310`.`0_salesman`;
CREATE TABLE  `ARIA310`.`0_salesman` (
  `salesman_code` int(11) NOT NULL AUTO_INCREMENT,
  `salesman_name` char(60) NOT NULL DEFAULT '',
  `salesman_phone` char(30) NOT NULL DEFAULT '',
  `salesman_fax` char(30) NOT NULL DEFAULT '',
  `salesman_email` varchar(100) NOT NULL DEFAULT '',
  `provision` double NOT NULL DEFAULT '0',
  `break_pt` double NOT NULL DEFAULT '0',
  `provision2` double NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`salesman_code`),
  UNIQUE KEY `salesman_name` (`salesman_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_salesman`
--

/*!40000 ALTER TABLE `0_salesman` DISABLE KEYS */;
LOCK TABLES `0_salesman` WRITE;
INSERT INTO `ARIA310`.`0_salesman` VALUES  (1,'Sales Person','','','',5,1000,4,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_salesman` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_security_roles`
--

DROP TABLE IF EXISTS `ARIA310`.`0_security_roles`;
CREATE TABLE  `ARIA310`.`0_security_roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(30) NOT NULL,
  `description` varchar(50) DEFAULT NULL,
  `sections` text,
  `areas` text,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `role` (`role`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_security_roles`
--

/*!40000 ALTER TABLE `0_security_roles` DISABLE KEYS */;
LOCK TABLES `0_security_roles` WRITE;
INSERT INTO `ARIA310`.`0_security_roles` VALUES  (1,'Inquiries','Inquiries','768;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15872;16128','257;258;259;260;513;514;515;516;517;518;519;520;521;522;523;524;525;773;774;2822;3073;3075;3076;3077;3329;3330;3331;3332;3333;3334;3335;5377;5633;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8450;8451;10497;10753;11009;11010;11012;13313;13315;15617;15618;15619;15620;15621;15622;15623;15624;15625;15626;15873;15882;16129;16130;16131;16132',0),
 (2,'System Administrator','System Administrator','256;512;768;131840;459520;2816;3072;3328;5376;5632;136704;5888;7936;8192;8448;10496;10752;11008;13056;13312;15616;15872;343552;16128','257;258;259;260;513;514;515;516;517;518;519;520;521;522;523;524;525;769;770;771;772;773;774;131940;459620;2817;2818;2819;2820;2821;2822;2823;3073;3074;3082;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5635;5636;5637;5641;5638;5639;5640;136805;5889;5890;5891;7937;7938;7939;7940;8193;8194;8195;8196;8197;8449;8450;8451;10497;10753;10754;10755;10756;10757;11009;11010;11011;11012;13057;13313;13314;13315;15617;15618;15619;15620;15621;15622;15623;15624;15628;15625;15626;15627;15873;15874;15875;15876;15877;15878;15879;15880;15883;15881;15882;343652;16129;16130;16131;16132',0),
 (3,'Salesman','Salesman','768;3072;5632;8192;15872','773;774;3073;3075;3081;5633;8194;15873',0),
 (4,'Stock Manager','Stock Manager','2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15872;16128','2818;2822;3073;3076;3077;3329;3330;3330;3330;3331;3331;3332;3333;3334;3335;5633;5640;5889;5890;5891;8193;8194;8450;8451;10753;11009;11010;11012;13313;13315;15882;16129;16130;16131;16132',0),
 (5,'Production Manager','Production Manager','512;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128','521;523;524;2818;2819;2820;2821;2822;2823;3073;3074;3076;3077;3078;3079;3080;3081;3329;3330;3330;3330;3331;3331;3332;3333;3334;3335;5633;5640;5640;5889;5890;5891;8193;8194;8196;8197;8450;8451;10753;10755;11009;11010;11012;13313;13315;15617;15619;15620;15621;15624;15624;15876;15877;15880;15882;16129;16130;16131;16132',0),
 (6,'Purchase Officer','Purchase Officer','512;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128','521;523;524;2818;2819;2820;2821;2822;2823;3073;3074;3076;3077;3078;3079;3080;3081;3329;3330;3330;3330;3331;3331;3332;3333;3334;3335;5377;5633;5635;5640;5640;5889;5890;5891;8193;8194;8196;8197;8449;8450;8451;10753;10755;11009;11010;11012;13313;13315;15617;15619;15620;15621;15624;15624;15876;15877;15880;15882;16129;16130;16131;16132',0),
 (7,'AR Officer','AR Officer','512;768;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128','521;523;524;771;773;774;2818;2819;2820;2821;2822;2823;3073;3073;3074;3075;3076;3077;3078;3079;3080;3081;3081;3329;3330;3330;3330;3331;3331;3332;3333;3334;3335;5633;5633;5634;5637;5638;5639;5640;5640;5889;5890;5891;8193;8194;8194;8196;8197;8450;8451;10753;10755;11009;11010;11012;13313;13315;15617;15619;15620;15621;15624;15624;15873;15876;15877;15878;15880;15882;16129;16130;16131;16132',0),
 (8,'AP Officer','AP Officer','512;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128','257;258;259;260;521;523;524;769;770;771;772;773;774;2818;2819;2820;2821;2822;2823;3073;3074;3082;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5635;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8196;8197;8449;8450;8451;10497;10753;10755;11009;11010;11012;13057;13313;13315;15617;15619;15620;15621;15624;15876;15877;15880;15882;16129;16130;16131;16132',0),
 (9,'Accountant','New Accountant','512;768;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128','257;258;259;260;521;523;524;771;772;773;774;2818;2819;2820;2821;2822;2823;3073;3074;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5635;5637;5638;5639;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8196;8197;8449;8450;8451;10497;10753;10755;11009;11010;11012;13313;13315;15617;15618;15619;15620;15621;15624;15873;15876;15877;15878;15880;15882;16129;16130;16131;16132',0),
 (10,'Sub Admin','Sub Admin','512;768;2816;3072;3328;5376;5632;5888;8192;8448;13312;15616;15872;16128','257;258;259;260;7937;7938;7939;7940;10497;10753;10754;10755;10756;10757;11009;11010;11011;11012;13057;521;523;524;771;772;773;774;2818;2819;2820;2821;2822;2823;3073;3074;3082;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5635;5637;5638;5639;5640;5889;5890;5891;8193;8194;8196;8197;8449;8450;8451;13313;13315;15617;15619;15620;15621;15624;15873;15874;15876;15877;15878;15879;15880;15882;16129;16130;16131;16132',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_security_roles` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_shippers`
--

DROP TABLE IF EXISTS `ARIA310`.`0_shippers`;
CREATE TABLE  `ARIA310`.`0_shippers` (
  `shipper_id` int(11) NOT NULL AUTO_INCREMENT,
  `shipper_name` varchar(60) NOT NULL DEFAULT '',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `phone2` varchar(30) NOT NULL DEFAULT '',
  `contact` tinytext NOT NULL,
  `address` tinytext NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`shipper_id`),
  UNIQUE KEY `name` (`shipper_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_shippers`
--

/*!40000 ALTER TABLE `0_shippers` DISABLE KEYS */;
LOCK TABLES `0_shippers` WRITE;
INSERT INTO `ARIA310`.`0_shippers` VALUES  (1,'Default','','','','',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_shippers` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_sql_trail`
--

DROP TABLE IF EXISTS `ARIA310`.`0_sql_trail`;
CREATE TABLE  `ARIA310`.`0_sql_trail` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sql` text NOT NULL,
  `result` tinyint(1) NOT NULL,
  `msg` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_sql_trail`
--

/*!40000 ALTER TABLE `0_sql_trail` DISABLE KEYS */;
LOCK TABLES `0_sql_trail` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_sql_trail` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_stock_category`
--

DROP TABLE IF EXISTS `ARIA310`.`0_stock_category`;
CREATE TABLE  `ARIA310`.`0_stock_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(60) NOT NULL DEFAULT '',
  `dflt_tax_type` int(11) NOT NULL DEFAULT '1',
  `dflt_units` varchar(20) NOT NULL DEFAULT 'each',
  `dflt_mb_flag` char(1) NOT NULL DEFAULT 'B',
  `dflt_sales_act` varchar(11) NOT NULL DEFAULT '',
  `dflt_cogs_act` varchar(11) NOT NULL DEFAULT '',
  `dflt_inventory_act` varchar(11) NOT NULL DEFAULT '',
  `dflt_adjustment_act` varchar(11) NOT NULL DEFAULT '',
  `dflt_assembly_act` varchar(11) NOT NULL DEFAULT '',
  `dflt_dim1` int(11) DEFAULT NULL,
  `dflt_dim2` int(11) DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `dflt_no_sale` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`category_id`),
  UNIQUE KEY `description` (`description`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_stock_category`
--

/*!40000 ALTER TABLE `0_stock_category` DISABLE KEYS */;
LOCK TABLES `0_stock_category` WRITE;
INSERT INTO `ARIA310`.`0_stock_category` VALUES  (1,'Components',1,'each','B','4010','5010','1510','5040','1530',0,0,0,0),
 (3,'Systems',1,'each','M','4010','5010','1510','5040','1530',0,0,0,0),
 (4,'Services',1,'hrs','D','4010','5010','1510','5040','1530',0,0,0,0),
 (5,'Asset',1,'LOT','M','1840','5010','1840','5040','1530',0,0,0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_stock_category` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_stock_master`
--

DROP TABLE IF EXISTS `ARIA310`.`0_stock_master`;
CREATE TABLE  `ARIA310`.`0_stock_master` (
  `stock_id` varchar(20) NOT NULL DEFAULT '',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `tax_type_id` int(11) NOT NULL DEFAULT '0',
  `description` varchar(200) NOT NULL DEFAULT '',
  `long_description` tinytext NOT NULL,
  `units` varchar(20) NOT NULL DEFAULT 'each',
  `mb_flag` char(1) NOT NULL DEFAULT 'B',
  `sales_account` varchar(11) NOT NULL DEFAULT '',
  `cogs_account` varchar(11) NOT NULL DEFAULT '',
  `inventory_account` varchar(11) NOT NULL DEFAULT '',
  `adjustment_account` varchar(11) NOT NULL DEFAULT '',
  `assembly_account` varchar(11) NOT NULL DEFAULT '',
  `dimension_id` int(11) DEFAULT NULL,
  `dimension2_id` int(11) DEFAULT NULL,
  `actual_cost` double NOT NULL DEFAULT '0',
  `last_cost` double NOT NULL DEFAULT '0',
  `material_cost` double NOT NULL DEFAULT '0',
  `labour_cost` double NOT NULL DEFAULT '0',
  `overhead_cost` double NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  `no_sale` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_stock_master`
--

/*!40000 ALTER TABLE `0_stock_master` DISABLE KEYS */;
LOCK TABLES `0_stock_master` WRITE;
INSERT INTO `ARIA310`.`0_stock_master` VALUES  ('1000-100-100',3,1,'ARIA ERP SYSTEM','','LOT','D','4010','5010','1510','5040','1530',0,0,0,0,0,0,0,0,0),
 ('1000-100-101',3,1,'EZRA POS SYSTEM','','LOT','D','4010','5010','1510','5040','1530',0,0,0,0,0,0,0,0,0),
 ('1000-100-102',3,1,'KAIA HRIS SYSTEM','','LOT','D','4010','5010','1510','5040','1530',0,0,0,0,0,0,0,0,0),
 ('1000-100-103',3,1,'ERITHEIA PAYROLL SYSTEM','','LOT','D','4010','5010','1510','5040','1530',0,0,0,0,0,0,0,0,0),
 ('1000-100-104',1,1,'EPSON POS PRINTER','','PC','B','4010','5010','1510','5040','1530',0,0,0,0,9645.76,0,0,0,0),
 ('1000-100-105',1,1,'CASH DRAWER','','PC','B','4010','5010','1510','5040','1530',0,0,0,0,4960.71,0,0,0,0),
 ('1000-100-106',1,1,'POSIFLEX TOUCH SCREEN MONITOR','','PC','B','4010','5010','1510','5040','1530',0,0,0,0,15000,0,0,0,0),
 ('1000-100-107',1,1,'BIOMETRIC FINGER SCANNING HARDWARE','','PC','B','4010','5010','1510','5040','1530',0,0,0,0,3500,0,0,0,0),
 ('1000-100-108',1,1,'DELL POWEREDGE 2950 RACKSERVER','','LOT','B','4010','5010','1510','5040','1530',0,0,0,0,0,0,0,0,0),
 ('1000-100-109',1,1,'DELL LATITUDE NOTEBOOK','','PC','B','4010','5010','1510','5040','1530',0,0,0,0,0,0,0,0,0),
 ('ITEM00001',1,1,'Manufactured Item','test','LOT','M','4010','5010','1510','5040','1530',0,0,0,0,27972.34,66.67,0,0,0),
 ('qqq111',3,1,'fsdfsdf','sdfsdfsdf','LOT','M','4430','5020','1520','5050','1540',0,0,0,0,1563.82,50,0,0,1),
 ('QQQ211',3,1,'rei alison','dfgdfgdfg','LOT','M','4430','5020','1520','5050','1540',0,0,0,0,0,0,0,0,1),
 ('qqq333',3,1,'rei alison','gbdffgdfgf','LOT','M','4430','5020','1520','5050','1540',0,0,0,0,0,0,0,0,0),
 ('SRV00001',4,1,'Service','Service','KG','D','4010','5010','1510','5040','1530',7,10,0,0,0,0,0,0,0),
 ('SRV00002',4,1,'Service Two','','HRS','D','4010','5010','1510','5040','1530',7,9,0,0,0,0,0,0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_stock_master` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_stock_moves`
--

DROP TABLE IF EXISTS `ARIA310`.`0_stock_moves`;
CREATE TABLE  `ARIA310`.`0_stock_moves` (
  `trans_id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_no` int(11) NOT NULL DEFAULT '0',
  `stock_id` char(20) NOT NULL DEFAULT '',
  `type` smallint(6) NOT NULL DEFAULT '0',
  `loc_code` char(5) NOT NULL DEFAULT '',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `person_id` int(11) DEFAULT NULL,
  `price` double NOT NULL DEFAULT '0',
  `reference` char(40) NOT NULL DEFAULT '',
  `qty` double NOT NULL DEFAULT '1',
  `discount_percent` double NOT NULL DEFAULT '0',
  `standard_cost` double NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`trans_id`),
  KEY `type` (`type`,`trans_no`)
) ENGINE=InnoDB AUTO_INCREMENT=90 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_stock_moves`
--

/*!40000 ALTER TABLE `0_stock_moves` DISABLE KEYS */;
LOCK TABLES `0_stock_moves` WRITE;
INSERT INTO `ARIA310`.`0_stock_moves` VALUES  (1,1,'1000-100-104',17,'DEF','2010-06-09',1,0,'1',15,0,0,1),
 (2,1,'1000-100-105',17,'DEF','2010-06-09',1,0,'1',15,0,0,1),
 (3,1,'1000-100-106',17,'DEF','2010-06-09',1,0,'1',15,0,0,1),
 (4,1,'1000-100-107',17,'DEF','2010-06-09',1,0,'1',15,0,0,1),
 (5,1,'1000-100-108',17,'DEF','2010-06-09',1,0,'1',5,0,0,1),
 (6,1,'1000-100-109',17,'DEF','2010-06-09',1,0,'1',15,0,0,1),
 (7,1,'1000-100-104',13,'DEF','2010-12-31',0,18000,'1',-1,0,0,1),
 (8,1,'1000-100-106',13,'DEF','2010-12-31',0,35000,'1',-1,0,0,1),
 (9,2,'1000-100-104',17,'DEF','2011-03-14',1,0,'2',1000,0,10000,1),
 (10,3,'1000-100-104',13,'DEF','2011-03-14',0,18000,'2',-10,0,9861.93,1),
 (11,4,'1000-100-104',13,'DEF','2011-03-14',0,18000,'3',-10,0,9861.93,1),
 (12,1,'1000-100-104',28,'DEF','2011-03-14',0,0,'',-1,0,0,1),
 (13,1,'1000-100-104',29,'DEF','2011-03-14',0,0,'',-5,0,0,1),
 (14,1,'1000-100-105',29,'DEF','2011-03-14',0,0,'',-5,0,0,1),
 (15,1,'qqq111',29,'DEF','2011-03-14',0,0,'',5,0,0,1),
 (16,2,'1000-100-104',29,'DEF','2011-03-14',0,0,'',-2,0,0,1),
 (17,2,'1000-100-105',29,'DEF','2011-03-14',0,0,'',-2,0,0,1),
 (18,2,'qqq111',29,'DEF','2011-03-14',0,0,'',2,0,0,1),
 (19,1,'1000-100-105',25,'DEF','2011-03-14',1,5000,'',1000,0,4960.32,1),
 (20,3,'1000-100-104',29,'DEF','2011-03-14',0,0,'',-3,0,0,1),
 (21,3,'1000-100-105',29,'DEF','2011-03-14',0,0,'',-3,0,0,1),
 (22,3,'qqq111',29,'DEF','2011-03-14',0,0,'',3,0,0,1),
 (23,2,'1000-100-104',25,'DEF','2011-03-24',1,10000,'',10,0,9863.32,1),
 (24,3,'1000-100-104',25,'DEF','2011-03-26',1,10000,'',10,0,9864.68,1),
 (25,3,'1000-100-105',25,'DEF','2011-03-26',1,5000,'',10,0,4960.71,1),
 (26,3,'1000-100-106',25,'DEF','2011-03-26',1,15000,'',10,0,6250,1),
 (27,3,'1000-100-107',25,'DEF','2011-03-26',1,5000,'',10,0,2000,1),
 (28,5,'1000-100-104',13,'DEF','2011-03-26',0,18000,'4',-3,0,9864.68,1),
 (29,4,'1000-100-104',29,'DEF','2011-03-26',0,0,'',-3,0,0,1),
 (30,4,'1000-100-105',29,'DEF','2011-03-26',0,0,'',-3,0,0,1),
 (31,4,'1000-100-106',29,'DEF','2011-03-26',0,0,'',-3,0,0,1),
 (32,4,'ITEM00001',29,'DEF','2011-03-26',0,0,'',3,0,0,1),
 (33,4,'1000-100-104',25,'DEF','2011-04-12',1,10000,'',50,0,9871.14,1),
 (34,5,'1000-100-104',29,'DEF','2011-04-12',0,0,'',0,0,0,1),
 (35,5,'1000-100-105',29,'DEF','2011-04-12',0,0,'',0,0,0,1),
 (36,5,'1000-100-106',29,'DEF','2011-04-12',0,0,'',0,0,0,1),
 (37,5,'ITEM00001',29,'DEF','2011-04-12',0,0,'',0,0,0,1),
 (38,6,'1000-100-104',29,'DEF','2011-04-12',0,0,'',-1,0,0,1),
 (39,6,'1000-100-105',29,'DEF','2011-04-12',0,0,'',-1,0,0,1),
 (40,6,'1000-100-106',29,'DEF','2011-04-12',0,0,'',-1,0,0,1),
 (41,6,'ITEM00001',29,'DEF','2011-04-12',0,0,'',1,0,0,1),
 (42,5,'1000-100-108',28,'DEF','2011-04-12',0,0,'this is an additional item',-1,0,0,1),
 (43,5,'1000-100-106',25,'DEF','2011-04-12',1,15000,'',10,0,15000,1),
 (44,6,'1000-100-104',13,'DEF','2011-04-12',0,18000,'5',-5,0,9871.14,1),
 (45,6,'1000-100-106',13,'DEF','2011-04-12',0,35000,'5',-10,0,15000,1),
 (46,6,'1000-100-105',13,'DEF','2011-04-12',0,6750,'5',-2,0,4960.71,1),
 (47,7,'1000-100-104',29,'DEF','2011-04-12',0,0,'',-1,0,0,1),
 (48,7,'1000-100-105',29,'DEF','2011-04-12',0,0,'',-1,0,0,1),
 (49,7,'1000-100-106',29,'DEF','2011-04-12',0,0,'',-1,0,0,1),
 (50,7,'ITEM00001',29,'DEF','2011-04-12',0,0,'',1,0,0,1),
 (51,7,'1000-100-106',13,'DEF','2011-04-12',0,35000,'6',-19,0,15000,1),
 (52,6,'1000-100-106',25,'DEF','2011-04-12',1,15000,'',20,0,15000,1),
 (53,8,'1000-100-104',13,'DEF','2011-05-06',0,18000,'7',-1,0,9871.14,1),
 (54,9,'1000-100-104',13,'DEF','2011-05-06',0,18000,'8',-2,0,9871.14,1),
 (55,7,'1000-100-106',25,'DEF','2011-05-19',1,15000,'',100,0,15000,1),
 (56,1,'1000-100-106',16,'DEF','2011-05-19',1,0,'1',-50,0,0,1),
 (57,1,'1000-100-106',16,'LOC2','2011-05-19',1,0,'1',50,0,0,1),
 (58,10,'1000-100-106',13,'DEF','2011-05-19',0,35000,'9',-1,0,15000,1),
 (59,8,'1000-100-104',25,'DEF','2011-06-04',1,10000,'',100,0,9882.47,1),
 (60,2,'1000-100-104',16,'DEF','2011-06-04',1,0,'2',-100,0,0,1),
 (61,2,'1000-100-104',16,'LOC2','2011-06-04',1,0,'2',100,0,0,1),
 (62,11,'1000-100-104',13,'DEF','2011-06-28',0,18000,'10',-100,0,9892.81,1),
 (63,9,'1000-100-104',25,'DEF','2011-06-28',1,10000,'',100,0,9892.81,1),
 (64,10,'1000-100-104',25,'DEF','2011-07-06',1,10000,'',100,0,9901.48,1),
 (65,7,'1000-100-109',28,'DEF','2011-07-06',0,0,'',-1,0,0,1),
 (66,11,'1000-100-104',25,'LOC1','2011-07-07',1,10000,'',100,0,9610.69,1),
 (67,12,'1000-100-104',25,'DEF','2011-07-07',1,10000,'',50,0,9610.69,1),
 (68,3,'1000-100-104',16,'DEF','2011-07-07',1,0,'3',-500,0,0,1),
 (69,3,'1000-100-104',16,'LOC2','2011-07-07',1,0,'3',500,0,0,1),
 (70,12,'1000-100-104',13,'DEF','2011-07-07',0,18000,'11',-1,0,9610.69,1),
 (71,13,'1000-100-104',13,'DEF','2011-07-07',0,18000,'12',-2,0,9610.69,1),
 (72,14,'1000-100-104',13,'DEF','2011-07-07',0,18000,'13',-3,0,9610.69,1),
 (73,13,'1000-100-104',25,'DEF','2011-07-07',3,500,'',50,0,9583.27,1),
 (74,8,'1000-100-104',29,'DEF','2011-07-07',0,0,'',-10,0,0,1),
 (75,8,'1000-100-105',29,'DEF','2011-07-07',0,0,'',-10,0,0,1),
 (76,8,'1000-100-106',29,'DEF','2011-07-07',0,0,'',-10,0,0,1),
 (77,8,'ITEM00001',29,'DEF','2011-07-07',0,0,'',10,0,0,1),
 (78,14,'1000-100-107',25,'DEF','2011-07-08',1,5000,'',25,0,3500,1),
 (79,15,'1000-100-104',13,'DEF','2011-07-12',0,18000,'14',-1,0,9610.69,1),
 (80,15,'1000-100-105',13,'DEF','2011-07-12',0,6750,'14',-1,0,4960.71,1),
 (81,15,'1000-100-104',25,'DEF','2011-07-14',1,10000,'',100,0,9610.69,1),
 (82,3,'1000-100-104',17,'DEF','2011-07-14',1,0,'3',-5,0,9610.69,1),
 (83,16,'1000-100-104',25,'DEF','2011-07-22',1,10000,'',50,0,9623.13,1),
 (84,17,'1000-100-104',25,'DEF','2011-08-02',1,10000,'',100,0,9645.76,1),
 (85,17,'1000-100-106',13,'DEF','2011-08-12',0,35000,'16',-50,0,15000,1),
 (86,4,'1000-100-104',16,'DEF','2011-08-12',1,0,'4',-4,0,0,1),
 (87,4,'1000-100-104',16,'LOC1','2011-08-12',1,0,'4',4,0,0,1),
 (88,5,'1000-100-109',16,'DEF','2011-08-12',1,0,'5',-4,0,0,1),
 (89,5,'1000-100-109',16,'LOC1','2011-08-12',1,0,'5',4,0,0,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_stock_moves` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_supp_allocations`
--

DROP TABLE IF EXISTS `ARIA310`.`0_supp_allocations`;
CREATE TABLE  `ARIA310`.`0_supp_allocations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `amt` double unsigned DEFAULT NULL,
  `date_alloc` date NOT NULL DEFAULT '0000-00-00',
  `trans_no_from` int(11) DEFAULT NULL,
  `trans_type_from` int(11) DEFAULT NULL,
  `trans_no_to` int(11) DEFAULT NULL,
  `trans_type_to` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_supp_allocations`
--

/*!40000 ALTER TABLE `0_supp_allocations` DISABLE KEYS */;
LOCK TABLES `0_supp_allocations` WRITE;
INSERT INTO `ARIA310`.`0_supp_allocations` VALUES  (1,28000,'2011-07-07',2,22,7,20),
 (2,5712000,'2011-07-14',5,22,1,20),
 (3,560000,'2011-07-22',6,22,9,20),
 (4,1120000,'2011-08-02',7,22,10,20),
 (5,2940000,'2011-08-02',8,22,8,20);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_supp_allocations` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_supp_invoice_items`
--

DROP TABLE IF EXISTS `ARIA310`.`0_supp_invoice_items`;
CREATE TABLE  `ARIA310`.`0_supp_invoice_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supp_trans_no` int(11) DEFAULT NULL,
  `supp_trans_type` int(11) DEFAULT NULL,
  `gl_code` varchar(11) NOT NULL DEFAULT '0',
  `grn_item_id` int(11) DEFAULT NULL,
  `po_detail_item_id` int(11) DEFAULT NULL,
  `stock_id` varchar(20) NOT NULL DEFAULT '',
  `description` tinytext,
  `quantity` double NOT NULL DEFAULT '0',
  `unit_price` double NOT NULL DEFAULT '0',
  `unit_tax` double NOT NULL DEFAULT '0',
  `memo_` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_supp_invoice_items`
--

/*!40000 ALTER TABLE `0_supp_invoice_items` DISABLE KEYS */;
LOCK TABLES `0_supp_invoice_items` WRITE;
INSERT INTO `ARIA310`.`0_supp_invoice_items` VALUES  (1,1,20,'0',1,1,'1000-100-105','CASH DRAWER',1000,5000,600,''),
 (2,1,20,'0',2,2,'1000-100-104','EPSON POS PRINTER',10,10000,1200,''),
 (3,2,20,'0',3,3,'1000-100-104','EPSON POS PRINTER',10,10000,1200,''),
 (4,2,20,'0',4,4,'1000-100-105','CASH DRAWER',10,5000,600,''),
 (5,2,20,'0',5,5,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',10,15000,1800,''),
 (6,2,20,'0',6,6,'1000-100-107','BIOMETRIC FINGER SCANNING HARDWARE',10,5000,600,''),
 (7,3,20,'0',10,10,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',100,15000,1800,''),
 (8,3,20,'0',7,7,'1000-100-104','EPSON POS PRINTER',50,10000,1200,''),
 (9,3,20,'0',8,8,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',10,15000,1800,''),
 (10,3,20,'0',9,9,'1000-100-106','POSIFLEX TOUCH SCREEN MONITOR',20,15000,1800,''),
 (11,4,20,'0',11,11,'1000-100-104','EPSON POS PRINTER',100,10000,1200,''),
 (12,5,20,'0',12,12,'1000-100-104','EPSON POS PRINTER',100,10000,1200,''),
 (13,6,20,'0',13,13,'1000-100-104','EPSON POS PRINTER',100,10000,1200,''),
 (14,7,20,'0',16,17,'1000-100-104','EPSON POS PRINTER',50,500,60,''),
 (15,8,20,'0',14,14,'1000-100-104','EPSON POS PRINTER',100,10000,1200,''),
 (16,8,20,'0',15,16,'1000-100-104','EPSON POS PRINTER',50,10000,1200,''),
 (17,8,20,'0',17,18,'1000-100-107','BIOMETRIC FINGER SCANNING HARDWARE',25,5000,600,''),
 (18,8,20,'0',18,19,'1000-100-104','EPSON POS PRINTER',100,10000,1200,''),
 (19,9,20,'0',19,16,'1000-100-104','EPSON POS PRINTER',50,10000,1200,''),
 (20,10,20,'0',20,20,'1000-100-104','EPSON POS PRINTER',100,10000,1200,'');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_supp_invoice_items` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_supp_trans`
--

DROP TABLE IF EXISTS `ARIA310`.`0_supp_trans`;
CREATE TABLE  `ARIA310`.`0_supp_trans` (
  `trans_no` int(11) unsigned NOT NULL DEFAULT '0',
  `type` smallint(6) unsigned NOT NULL DEFAULT '0',
  `supplier_id` int(11) unsigned DEFAULT NULL,
  `reference` tinytext NOT NULL,
  `supp_reference` varchar(60) NOT NULL DEFAULT '',
  `tran_date` date NOT NULL DEFAULT '0000-00-00',
  `due_date` date NOT NULL DEFAULT '0000-00-00',
  `ov_amount` double NOT NULL DEFAULT '0',
  `ov_discount` double NOT NULL DEFAULT '0',
  `ov_gst` double NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '1',
  `alloc` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`trans_no`,`type`),
  KEY `supplier_id` (`supplier_id`),
  KEY `SupplierID_2` (`supplier_id`,`supp_reference`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_supp_trans`
--

/*!40000 ALTER TABLE `0_supp_trans` DISABLE KEYS */;
LOCK TABLES `0_supp_trans` WRITE;
INSERT INTO `ARIA310`.`0_supp_trans` VALUES  (1,1,1,'1','','2010-12-31','0000-00-00',-443296.78638941,0,0,1,0),
 (1,20,1,'1','7890','2011-03-24','2011-03-25',5100000,0,612000,1,5712000),
 (1,22,3,'1','','2011-07-07','2011-07-07',-1000000,0,0,1,0),
 (2,20,1,'2','9877765','2011-03-26','2011-03-27',350000,0,42000,1,0),
 (2,22,3,'2','','2011-07-07','2011-07-07',-28000,0,0,1,28000),
 (3,20,1,'3','98765','2011-05-19','2011-05-20',2450000,0,294000,1,0),
 (3,22,3,'3','','2011-07-07','2011-07-07',-600000,0,0,1,0),
 (4,20,1,'4','123456','2011-06-04','2011-06-05',1000000,0,120000,1,0),
 (4,22,3,'4','','2011-07-07','2011-07-07',-600000,0,0,1,0),
 (5,20,1,'5','12345','2011-06-28','2011-06-29',1000000,0,120000,1,0),
 (5,22,1,'5','','2011-07-14','2011-07-14',-5712000,0,0,1,5712000),
 (6,20,1,'6','7123456','2011-07-06','2011-07-07',1000000,0,120000,1,0),
 (6,22,1,'6','','2011-07-22','2011-07-22',-560000,0,0,1,560000),
 (7,20,3,'7','766544','2011-07-07','2011-07-08',25000,0,3000,1,28000),
 (7,22,1,'7','','2011-08-02','2011-08-02',-1120000,0,0,1,1120000),
 (8,20,1,'8','987666','2011-07-14','2011-07-15',2625000,0,315000,1,2940000),
 (8,22,1,'8','','2011-08-02','2011-08-02',-2940000,0,0,1,2940000),
 (9,20,1,'9','123456789','2011-07-22','2011-07-23',500000,0,60000,1,560000),
 (10,20,1,'10','98760998','2011-08-02','2011-08-03',1000000,0,120000,1,1120000);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_supp_trans` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_suppliers`
--

DROP TABLE IF EXISTS `ARIA310`.`0_suppliers`;
CREATE TABLE  `ARIA310`.`0_suppliers` (
  `supplier_id` int(11) NOT NULL AUTO_INCREMENT,
  `supp_name` varchar(60) NOT NULL DEFAULT '',
  `supp_ref` varchar(30) NOT NULL DEFAULT '',
  `address` tinytext NOT NULL,
  `supp_address` tinytext NOT NULL,
  `phone` varchar(30) NOT NULL DEFAULT '',
  `phone2` varchar(30) NOT NULL DEFAULT '',
  `fax` varchar(30) NOT NULL DEFAULT '',
  `gst_no` varchar(25) NOT NULL DEFAULT '',
  `contact` varchar(60) NOT NULL DEFAULT '',
  `supp_account_no` varchar(40) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `website` varchar(100) NOT NULL DEFAULT '',
  `bank_account` varchar(60) NOT NULL DEFAULT '',
  `curr_code` char(3) DEFAULT NULL,
  `payment_terms` int(11) DEFAULT NULL,
  `dimension_id` int(11) DEFAULT '0',
  `dimension2_id` int(11) DEFAULT '0',
  `tax_group_id` int(11) DEFAULT NULL,
  `credit_limit` double NOT NULL DEFAULT '0',
  `purchase_account` varchar(11) DEFAULT NULL,
  `payable_account` varchar(11) DEFAULT NULL,
  `payment_discount_account` varchar(11) DEFAULT NULL,
  `notes` tinytext NOT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`supplier_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_suppliers`
--

/*!40000 ALTER TABLE `0_suppliers` DISABLE KEYS */;
LOCK TABLES `0_suppliers` WRITE;
INSERT INTO `ARIA310`.`0_suppliers` VALUES  (1,'ACCENT MICRO TECHNOLOGIES INC','AMTI','ORTIGAS CENTER, PASIG CITY','ORTIGAS CENTER, PASIG CITY','123','123','123','','CONTACT PERSON','','@','','','PHP',4,0,0,1,0,'5010','2100','5060','',0),
 (2,'MICROGOLD TECHNOLOGIES INC','MICROGOLD','MAKATI CITY','MAKATI CITY','123','123','123','','CONTACT PERSON','','@','','','PHP',4,0,0,1,0,'5010','2100','5060','',0),
 (3,'ASI PHILIPPINES INC','ASI','MANDALUYONG CITY','MANDALUYONG CITY','123','123','123','','CONTACT PERSON','','123','@','','PHP',4,0,0,1,0,'5010','2100','5060','',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_suppliers` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_sys_types`
--

DROP TABLE IF EXISTS `ARIA310`.`0_sys_types`;
CREATE TABLE  `ARIA310`.`0_sys_types` (
  `type_id` smallint(6) NOT NULL DEFAULT '0',
  `type_no` int(11) NOT NULL DEFAULT '1',
  `next_reference` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_sys_types`
--

/*!40000 ALTER TABLE `0_sys_types` DISABLE KEYS */;
LOCK TABLES `0_sys_types` WRITE;
INSERT INTO `ARIA310`.`0_sys_types` VALUES  (0,17,'7'),
 (1,7,'3'),
 (2,4,'2'),
 (4,3,'1'),
 (10,16,'7'),
 (11,2,'1'),
 (12,6,'4'),
 (13,1,'17'),
 (16,2,'6'),
 (17,2,'4'),
 (18,1,'18'),
 (20,6,'11'),
 (21,1,'1'),
 (22,3,'9'),
 (25,1,'18'),
 (26,1,'8'),
 (28,1,'10'),
 (29,1,'9'),
 (30,0,'16'),
 (32,0,'4'),
 (35,1,'1'),
 (40,1,'11');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_sys_types` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_tag_associations`
--

DROP TABLE IF EXISTS `ARIA310`.`0_tag_associations`;
CREATE TABLE  `ARIA310`.`0_tag_associations` (
  `record_id` varchar(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  UNIQUE KEY `record_id` (`record_id`,`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_tag_associations`
--

/*!40000 ALTER TABLE `0_tag_associations` DISABLE KEYS */;
LOCK TABLES `0_tag_associations` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_tag_associations` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_tags`
--

DROP TABLE IF EXISTS `ARIA310`.`0_tags`;
CREATE TABLE  `ARIA310`.`0_tags` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` smallint(6) NOT NULL,
  `name` varchar(30) NOT NULL,
  `description` varchar(60) DEFAULT NULL,
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`,`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_tags`
--

/*!40000 ALTER TABLE `0_tags` DISABLE KEYS */;
LOCK TABLES `0_tags` WRITE;
INSERT INTO `ARIA310`.`0_tags` VALUES  (1,1,'test','test',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_tags` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_tax_group_items`
--

DROP TABLE IF EXISTS `ARIA310`.`0_tax_group_items`;
CREATE TABLE  `ARIA310`.`0_tax_group_items` (
  `tax_group_id` int(11) NOT NULL DEFAULT '0',
  `tax_type_id` int(11) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`tax_group_id`,`tax_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_tax_group_items`
--

/*!40000 ALTER TABLE `0_tax_group_items` DISABLE KEYS */;
LOCK TABLES `0_tax_group_items` WRITE;
INSERT INTO `ARIA310`.`0_tax_group_items` VALUES  (1,1,5),
 (3,1,12),
 (3,2,2);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_tax_group_items` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_tax_groups`
--

DROP TABLE IF EXISTS `ARIA310`.`0_tax_groups`;
CREATE TABLE  `ARIA310`.`0_tax_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL DEFAULT '',
  `tax_shipping` tinyint(1) NOT NULL DEFAULT '0',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_tax_groups`
--

/*!40000 ALTER TABLE `0_tax_groups` DISABLE KEYS */;
LOCK TABLES `0_tax_groups` WRITE;
INSERT INTO `ARIA310`.`0_tax_groups` VALUES  (1,'Tax',0,0),
 (2,'Tax Exempt',0,0),
 (3,'group 1',0,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_tax_groups` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_tax_types`
--

DROP TABLE IF EXISTS `ARIA310`.`0_tax_types`;
CREATE TABLE  `ARIA310`.`0_tax_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rate` double NOT NULL DEFAULT '0',
  `sales_gl_code` varchar(11) NOT NULL DEFAULT '',
  `purchasing_gl_code` varchar(11) NOT NULL DEFAULT '',
  `name` varchar(60) NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_tax_types`
--

/*!40000 ALTER TABLE `0_tax_types` DISABLE KEYS */;
LOCK TABLES `0_tax_types` WRITE;
INSERT INTO `ARIA310`.`0_tax_types` VALUES  (1,12,'2150','2145','VAT',0),
 (2,-2,'1060','2500','Expanded Withholding Tax - Services',0),
 (3,-1,'1065','2501','Expanded Withholding Tax - Goods',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_tax_types` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_trans_tax_details`
--

DROP TABLE IF EXISTS `ARIA310`.`0_trans_tax_details`;
CREATE TABLE  `ARIA310`.`0_trans_tax_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `trans_type` smallint(6) DEFAULT NULL,
  `trans_no` int(11) DEFAULT NULL,
  `tran_date` date NOT NULL,
  `tax_type_id` int(11) NOT NULL DEFAULT '0',
  `rate` double NOT NULL DEFAULT '0',
  `ex_rate` double NOT NULL DEFAULT '1',
  `included_in_price` tinyint(1) NOT NULL DEFAULT '0',
  `net_amount` double NOT NULL DEFAULT '0',
  `amount` double NOT NULL DEFAULT '0',
  `memo` tinytext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_trans_tax_details`
--

/*!40000 ALTER TABLE `0_trans_tax_details` DISABLE KEYS */;
LOCK TABLES `0_trans_tax_details` WRITE;
INSERT INTO `ARIA310`.`0_trans_tax_details` VALUES  (1,13,1,'2010-12-31',1,12,1,1,47321.4286,5678.571432,'1'),
 (2,10,1,'2010-12-31',1,12,1,1,47321.4286,5678.571432,'1'),
 (3,13,2,'2010-12-31',1,12,1,1,339285.7143,40714.285716,'auto'),
 (4,10,2,'2010-12-31',1,12,1,1,339285.7143,40714.285716,'2'),
 (5,13,3,'2011-03-14',1,12,1,1,160714.2857,19285.714284,'2'),
 (6,13,4,'2011-03-14',1,12,1,1,160714.2857,19285.714284,'3'),
 (7,20,1,'2011-03-24',1,12,1,0,5100000,612000,'7890'),
 (8,20,2,'2011-03-26',1,12,1,0,350000,42000,'9877765'),
 (9,13,5,'2011-03-26',1,12,1,1,48214.2857,5785.714284,'4'),
 (10,10,3,'2011-03-26',1,12,1,1,48214.2857,5785.714284,'3'),
 (11,13,6,'2011-04-12',1,12,1,1,404910.7143,48589.285716,'5'),
 (12,10,4,'2011-04-12',1,12,1,1,404910.7143,48589.285716,'4'),
 (13,13,7,'2011-04-12',1,12,1,1,593750,71250,'6'),
 (14,13,8,'2011-05-06',1,12,1,1,16071.4286,1928.571432,'7'),
 (15,13,9,'2011-05-06',1,12,1,1,32142.8571,3857.142852,'8'),
 (16,10,5,'2011-05-06',1,12,1,1,16071.4286,1928.571432,'5'),
 (17,20,3,'2011-05-19',1,12,1,0,2450000,294000,'98765'),
 (18,13,10,'2011-05-19',1,12,1,1,31250,3750,'9'),
 (19,10,6,'2011-05-19',1,12,1,1,31250,3750,'6'),
 (20,0,1,'2011-05-25',3,1,1,0,-5000000,-50000,''),
 (21,20,4,'2011-06-04',1,12,1,0,1000000,120000,'123456'),
 (22,13,11,'2011-06-28',1,12,1,1,1607142.8571,192857.142852,'10'),
 (23,20,5,'2011-06-28',1,12,1,0,1000000,120000,'12345'),
 (24,20,6,'2011-07-06',1,12,1,0,1000000,120000,'7123456'),
 (25,13,12,'2011-07-07',1,12,1,1,16071.4286,1928.571432,'11'),
 (26,13,13,'2011-07-07',1,12,1,1,32142.8571,3857.142852,'12'),
 (27,13,14,'2011-07-07',1,12,1,1,48214.2857,5785.714284,'13'),
 (28,20,7,'2011-07-07',1,12,1,0,25000,3000,'766544'),
 (29,13,15,'2011-07-12',1,12,1,1,22098.2143,2651.785716,'14'),
 (30,20,8,'2011-07-14',1,12,1,0,2625000,315000,'987666'),
 (31,20,9,'2011-07-22',1,12,1,0,500000,60000,'123456789'),
 (32,20,10,'2011-08-02',1,12,1,0,1000000,120000,'98760998'),
 (33,13,16,'2011-08-02',1,12,1,1,60357.1429,7242.857148,'15'),
 (34,13,17,'2011-08-12',1,12,1,1,1562500,187500,'16');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_trans_tax_details` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_useronline`
--

DROP TABLE IF EXISTS `ARIA310`.`0_useronline`;
CREATE TABLE  `ARIA310`.`0_useronline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `timestamp` int(15) NOT NULL DEFAULT '0',
  `ip` varchar(40) NOT NULL DEFAULT '',
  `file` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_useronline`
--

/*!40000 ALTER TABLE `0_useronline` DISABLE KEYS */;
LOCK TABLES `0_useronline` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_useronline` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_users`
--

DROP TABLE IF EXISTS `ARIA310`.`0_users`;
CREATE TABLE  `ARIA310`.`0_users` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `user_id` varchar(60) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `real_name` varchar(100) NOT NULL DEFAULT '',
  `role_id` int(11) NOT NULL DEFAULT '1',
  `phone` varchar(30) NOT NULL DEFAULT '',
  `email` varchar(100) DEFAULT NULL,
  `language` varchar(20) DEFAULT NULL,
  `date_format` tinyint(1) NOT NULL DEFAULT '0',
  `date_sep` tinyint(1) NOT NULL DEFAULT '0',
  `tho_sep` tinyint(1) NOT NULL DEFAULT '0',
  `dec_sep` tinyint(1) NOT NULL DEFAULT '0',
  `theme` varchar(20) NOT NULL DEFAULT 'default',
  `page_size` varchar(20) NOT NULL DEFAULT 'A4',
  `prices_dec` smallint(6) NOT NULL DEFAULT '2',
  `qty_dec` smallint(6) NOT NULL DEFAULT '2',
  `rates_dec` smallint(6) NOT NULL DEFAULT '4',
  `percent_dec` smallint(6) NOT NULL DEFAULT '1',
  `show_gl` tinyint(1) NOT NULL DEFAULT '1',
  `show_codes` tinyint(1) NOT NULL DEFAULT '0',
  `show_hints` tinyint(1) NOT NULL DEFAULT '0',
  `last_visit_date` datetime DEFAULT NULL,
  `query_size` tinyint(1) DEFAULT '10',
  `graphic_links` tinyint(1) DEFAULT '1',
  `pos` smallint(6) DEFAULT '1',
  `print_profile` varchar(30) NOT NULL DEFAULT '1',
  `rep_popup` tinyint(1) DEFAULT '1',
  `sticky_doc_date` tinyint(1) DEFAULT '0',
  `startup_tab` varchar(20) NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_users`
--

/*!40000 ALTER TABLE `0_users` DISABLE KEYS */;
LOCK TABLES `0_users` WRITE;
INSERT INTO `ARIA310`.`0_users` VALUES  (1,'admin','5f4dcc3b5aa765d61d8327deb882cf99','Administrator',2,'','adm@adm.com','en_GB',0,0,0,0,'elegant226','Letter',2,2,4,1,1,0,0,'2011-08-16 02:55:32',10,1,1,'',1,0,'orders',0),
 (2,'prodriguez','5f4dcc3b5aa765d61d8327deb882cf99','Policarpio Rodriguez',10,'','','en_GB',0,0,0,0,'elegant226','Letter',2,2,4,1,1,0,0,'2011-05-21 06:05:14',10,1,1,'',1,0,'orders',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_users` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_voided`
--

DROP TABLE IF EXISTS `ARIA310`.`0_voided`;
CREATE TABLE  `ARIA310`.`0_voided` (
  `type` int(11) NOT NULL DEFAULT '0',
  `id` int(11) NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  `memo_` tinytext NOT NULL,
  UNIQUE KEY `id` (`type`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_voided`
--

/*!40000 ALTER TABLE `0_voided` DISABLE KEYS */;
LOCK TABLES `0_voided` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_voided` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_wo_issue_items`
--

DROP TABLE IF EXISTS `ARIA310`.`0_wo_issue_items`;
CREATE TABLE  `ARIA310`.`0_wo_issue_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `stock_id` varchar(40) DEFAULT NULL,
  `issue_id` int(11) DEFAULT NULL,
  `qty_issued` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_wo_issue_items`
--

/*!40000 ALTER TABLE `0_wo_issue_items` DISABLE KEYS */;
LOCK TABLES `0_wo_issue_items` WRITE;
INSERT INTO `ARIA310`.`0_wo_issue_items` VALUES  (1,'1000-100-104',1,1),
 (2,'1000-100-108',5,1),
 (3,'1000-100-109',7,1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_wo_issue_items` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_wo_issues`
--

DROP TABLE IF EXISTS `ARIA310`.`0_wo_issues`;
CREATE TABLE  `ARIA310`.`0_wo_issues` (
  `issue_no` int(11) NOT NULL AUTO_INCREMENT,
  `workorder_id` int(11) NOT NULL DEFAULT '0',
  `reference` varchar(100) DEFAULT NULL,
  `issue_date` date DEFAULT NULL,
  `loc_code` varchar(5) DEFAULT NULL,
  `workcentre_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`issue_no`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_wo_issues`
--

/*!40000 ALTER TABLE `0_wo_issues` DISABLE KEYS */;
LOCK TABLES `0_wo_issues` WRITE;
INSERT INTO `ARIA310`.`0_wo_issues` VALUES  (1,1,'1','2011-03-14','DEF',1),
 (2,2,'2','2011-03-26','DEF',1),
 (3,3,'3','2011-04-12','DEF',1),
 (4,2,'4','2011-04-12','DEF',1),
 (5,2,'5','2011-04-12','DEF',1),
 (6,4,'6','2011-04-12','DEF',1),
 (7,5,'7','2011-07-06','DEF',1),
 (8,5,'8','2011-07-06','DEF',1),
 (9,6,'9','2011-07-07','DEF',1);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_wo_issues` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_wo_manufacture`
--

DROP TABLE IF EXISTS `ARIA310`.`0_wo_manufacture`;
CREATE TABLE  `ARIA310`.`0_wo_manufacture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reference` varchar(100) DEFAULT NULL,
  `workorder_id` int(11) NOT NULL DEFAULT '0',
  `quantity` double NOT NULL DEFAULT '0',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_wo_manufacture`
--

/*!40000 ALTER TABLE `0_wo_manufacture` DISABLE KEYS */;
LOCK TABLES `0_wo_manufacture` WRITE;
INSERT INTO `ARIA310`.`0_wo_manufacture` VALUES  (1,'1',1,5,'2011-03-14'),
 (2,'2',1,2,'2011-03-14'),
 (3,'3',1,3,'2011-03-14'),
 (4,'4',2,3,'2011-03-26'),
 (5,'5',3,0,'2011-04-12'),
 (6,'6',2,1,'2011-04-12'),
 (7,'7',4,1,'2011-04-12'),
 (8,'8',7,10,'2011-07-07');
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_wo_manufacture` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_wo_requirements`
--

DROP TABLE IF EXISTS `ARIA310`.`0_wo_requirements`;
CREATE TABLE  `ARIA310`.`0_wo_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `workorder_id` int(11) NOT NULL DEFAULT '0',
  `stock_id` char(20) NOT NULL DEFAULT '',
  `workcentre` int(11) NOT NULL DEFAULT '0',
  `units_req` double NOT NULL DEFAULT '1',
  `std_cost` double NOT NULL DEFAULT '0',
  `loc_code` char(5) NOT NULL DEFAULT '',
  `units_issued` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_wo_requirements`
--

/*!40000 ALTER TABLE `0_wo_requirements` DISABLE KEYS */;
LOCK TABLES `0_wo_requirements` WRITE;
INSERT INTO `ARIA310`.`0_wo_requirements` VALUES  (1,1,'1000-100-104',1,1,0,'DEF',10),
 (2,1,'1000-100-105',1,1,0,'DEF',10),
 (3,2,'1000-100-104',1,1,0,'DEF',4),
 (4,2,'1000-100-105',1,1,0,'DEF',4),
 (5,2,'1000-100-106',1,1,0,'DEF',4),
 (6,3,'1000-100-104',1,1,0,'DEF',0),
 (7,3,'1000-100-105',1,1,0,'DEF',0),
 (8,3,'1000-100-106',1,1,0,'DEF',0),
 (9,4,'1000-100-104',1,1,0,'DEF',1),
 (10,4,'1000-100-105',1,1,0,'DEF',1),
 (11,4,'1000-100-106',1,1,0,'DEF',1),
 (12,5,'1000-100-104',1,1,0,'DEF',0),
 (13,5,'1000-100-105',1,1,0,'DEF',0),
 (14,5,'1000-100-106',1,1,0,'DEF',0),
 (15,6,'1000-100-104',1,1,0,'DEF',0),
 (16,6,'1000-100-105',1,1,0,'DEF',0),
 (17,6,'1000-100-106',1,1,0,'DEF',0),
 (18,7,'1000-100-104',1,1,0,'DEF',10),
 (19,7,'1000-100-105',1,1,0,'DEF',10),
 (20,7,'1000-100-106',1,1,0,'DEF',10);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_wo_requirements` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_workcentres`
--

DROP TABLE IF EXISTS `ARIA310`.`0_workcentres`;
CREATE TABLE  `ARIA310`.`0_workcentres` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` char(40) NOT NULL DEFAULT '',
  `description` char(50) NOT NULL DEFAULT '',
  `inactive` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_workcentres`
--

/*!40000 ALTER TABLE `0_workcentres` DISABLE KEYS */;
LOCK TABLES `0_workcentres` WRITE;
INSERT INTO `ARIA310`.`0_workcentres` VALUES  (1,'DEFAULT','DEFAULT WORKCENTER',0),
 (2,'PLANT1','',0),
 (3,'PLANt2','',0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_workcentres` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`0_workorders`
--

DROP TABLE IF EXISTS `ARIA310`.`0_workorders`;
CREATE TABLE  `ARIA310`.`0_workorders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wo_ref` varchar(60) NOT NULL DEFAULT '',
  `loc_code` varchar(5) NOT NULL DEFAULT '',
  `units_reqd` double NOT NULL DEFAULT '1',
  `stock_id` varchar(20) NOT NULL DEFAULT '',
  `date_` date NOT NULL DEFAULT '0000-00-00',
  `type` tinyint(4) NOT NULL DEFAULT '0',
  `required_by` date NOT NULL DEFAULT '0000-00-00',
  `released_date` date NOT NULL DEFAULT '0000-00-00',
  `units_issued` double NOT NULL DEFAULT '0',
  `closed` tinyint(1) NOT NULL DEFAULT '0',
  `released` tinyint(1) NOT NULL DEFAULT '0',
  `additional_costs` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `wo_ref` (`wo_ref`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`0_workorders`
--

/*!40000 ALTER TABLE `0_workorders` DISABLE KEYS */;
LOCK TABLES `0_workorders` WRITE;
INSERT INTO `ARIA310`.`0_workorders` VALUES  (1,'1','DEF',10,'qqq111','2011-03-14',2,'2011-04-03','2011-03-14',10,1,1,0),
 (2,'2','DEF',5,'ITEM00001','2011-03-26',2,'2011-04-15','2011-03-26',4,0,1,0),
 (3,'3','DEF',0,'ITEM00001','2011-03-26',2,'2011-04-15','2011-03-26',0,1,1,0),
 (4,'4','DEF',100,'ITEM00001','2011-04-12',2,'2011-05-02','2011-04-12',1,0,1,0),
 (5,'5','DEF',0,'ITEM00001','2011-07-06',2,'2011-07-26','2011-07-06',0,0,1,0),
 (6,'6','DEF',1,'ITEM00001','2011-07-07',2,'2011-07-27','2011-07-07',0,0,1,0),
 (7,'7','DEF',10,'ITEM00001','2011-07-07',2,'2011-07-27','2011-07-07',10,1,1,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `0_workorders` ENABLE KEYS */;


--
-- Definition of table `ARIA310`.`xx_reports`
--

DROP TABLE IF EXISTS `ARIA310`.`xx_reports`;
CREATE TABLE  `ARIA310`.`xx_reports` (
  `id` varchar(10) NOT NULL DEFAULT '',
  `typ` varchar(10) NOT NULL DEFAULT '',
  `attrib` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ARIA310`.`xx_reports`
--

/*!40000 ALTER TABLE `xx_reports` DISABLE KEYS */;
LOCK TABLES `xx_reports` WRITE;
INSERT INTO `ARIA310`.`xx_reports` VALUES  ('3','item','String|PF|Helvetica|8|10l|300|10|printed at||||||||'),
 ('3','item','DB|DE|Helvetica|12|6l|100|0|typ||||'),
 ('3','item','Term|PF|Helvetica|8|20l|460|10|PageNo||||||||'),
 ('6','item','Line|PH|1|1|1|1|1'),
 ('3','item','String|PH|Helvetica|14|40l|200|0|Stored Report|||1|40'),
 ('3','item','DB|DE|Helvetica|12|40l|150|0|attrib||||||||'),
 ('3','item','String|GH|Helvetica-Bold|14|10l|200|0|New Group||||||||'),
 ('3','item','DB|GH|Helvetica-Bold|12|6l|285|0|id||||||||'),
 ('3','info','Reports|2007-10-01|Bauer|List of all stored Reports|portrait|a4|classgrid'),
 ('3','group','id|newpage'),
 ('F2','funct','atime|2007-09-21|Bauer|Actual Time|function atime() {return date(\"h:i a\");}'),
 ('F1','funct','RepDate|2007-09-26|Joe|Date|function RepDate() {return today().\" \".now();}'),
 ('3','item','Term|PF|Helvetica|8|20l|335|10|RepDate||||||||'),
 ('F4','funct','oldgroup|2007-09-26|bauer|Value of old group|function oldgroup($it){return $it->group_old;}'),
 ('F5','funct','Newgroup|2007-09-26|Hunt|New group value|function newgroup($it){return $it->group_new;}'),
 ('B1','block','block1|2007-09-27|Bauer|Block 1'),
 ('6','item','Line|GF|1|1|1|400|1'),
 ('2','item','DB|DE|Helvetica-Bold|12||100|600|attrib||||'),
 ('6','item','DB|DE|Helvetica|9|6l|50|0|account_code||||||||'),
 ('6','item','Term|GH|Helvetica|9|50l|50|0|Newgroup||||||||'),
 ('F6','funct','rec_count|2007-09-26|bauer|total number of records|function rec_count($it) {return $it->count;}'),
 ('6','item','String|RF|Helvetica|9|20l|50|0|Gran Total||||||||'),
 ('6','item','String|GF|Helvetica|9|20l|50|0|Sum:||||||||'),
 ('6','item','DB|DE|Helvetica|9|30l|300|0|name||||||||'),
 ('6','item','DB|DE|Helvetica|9|20l|400|0|class_name||||||||'),
 ('6','item','DB|DE|Helvetica|9|50l|100|0|account_name||||||||'),
 ('F7','funct','subcount|2007-09-26|bauer|total number of records of a group|function subcount($it) {return $it->subcount;}'),
 ('6','info','Accounts|2009-02-08|Hunt|Accounts List|portrait|a4|class'),
 ('6','select','select * from 0_chart_master,0_chart_types,0_chart_class where account_type=id and class_id=cid order by account_code'),
 ('2','info','single|2007-09-26|Bauer|Single page per Record|portrait|a4|single'),
 ('2','select','select * from xx_reports'),
 ('2','group','|nopage'),
 ('6','item','Line|GH|1|1|1|1|1'),
 ('6','item','Line|RH|1|1|1|1|1'),
 ('6','item','Line|RF|1|1|1|1|1'),
 ('6','item','Term|RH|Helvetica-Bold|16|50l|50|0|RepTitle||||||||'),
 ('F8','funct','RepTitle|2007-09-26|Joe|Report Title|function RepTitle($it) \r\n{ return $it->long_name; }'),
 ('6','item','String|RH|Helvetica|8|30l|50|-12|Print Out Date:||||||||'),
 ('6','item','Term|RH|Helvetica|8|30l|120|-12|RepDate||||||||'),
 ('6','item','String|RH|Helvetica|8|30l|50|-24|Fiscal Year:||||||||'),
 ('6','item','String|RH|Helvetica|8|30l|50|-36|Select:||||||||'),
 ('6','item','Term|GF|Helvetica|9|4r|200|0|subcount||||||||'),
 ('6','item','Term|RF|Helvetica|9|4r|200|0|rec_count||||||||'),
 ('6','item','String|PH|Helvetica-Bold|9|20l|50|0|Account||||||||'),
 ('6','item','String|PH|Helvetica-Bold|9|50l|100|0|Account Name||||||||'),
 ('6','item','String|PH|Helvetica-Bold|9|20l|300|0|Type||||||||'),
 ('6','item','String|PH|Helvetica-Bold|9|20l|400|0|Class||||||||'),
 ('6','item','Term|RH|Helvetica|9|50l|400|0|Company||||||||'),
 ('6','item','Term|RH|Helvetica|9|50l|400|-12|Username||||||||'),
 ('6','item','Term|RH|Helvetica|9|50l|400|-36|PageNo||||||||'),
 ('F9','funct','PageNo|2007-09-26|Joe|Page Number|function PageNo($it){return \"Page   \".$it->pdf->numPages;}'),
 ('B1','item','String|PH|Helvetica|7|20l|100|0|Stringitem||||||||'),
 ('3','select','select * from xx_reports order by id'),
 ('3','item','DB|PH|Helvetica|14|6l|360|0|id|||1|6||||'),
 ('6','item','Term|RH|Helvetica|8|50l|400|-24|Host||||||||'),
 ('F13','funct','Host|2007-09-26|Hunt|Host name|function Host(){return $_SERVER[\'SERVER_NAME\'];}'),
 ('6','group','name|nopage'),
 ('6','item','Term|RH|Helvetica|8|50l|120|-24|FiscalYear||||||||'),
 ('F12','funct','FiscalYear|2007-09-26|Hunt|Get current Fiscal Year|function FiscalYear(){$y=get_current_fiscalyear();return sql2date($y[\'begin\']) . \" - \" . sql2date($y[\'end\']);}'),
 ('F11','funct','Username|2007-09-26|Hunt|Get Username|function Username(){return $_SESSION[\"wa_current_user\"]->name;}'),
 ('F10','funct','Company|2007-09-26|Hunt|Company Name|function Company(){ return get_company_pref(\'coy_name\'); }');
UNLOCK TABLES;
/*!40000 ALTER TABLE `xx_reports` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
