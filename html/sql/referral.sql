CREATE TABLE `0_referral` (
`referral_code`  int(11) NOT NULL AUTO_INCREMENT ,
`referral_name`  char(60) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' ,
`referral_phone`  char(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' ,
`referral_address`  text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL ,
`referral_email`  varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '' ,
`percentage`  double NOT NULL DEFAULT 0 ,
`inactive`  tinyint(1) NOT NULL DEFAULT 0 ,
PRIMARY KEY (`referral_code`),
UNIQUE INDEX `salesman_name` USING BTREE (`referral_name`)
)
ENGINE=MyISAM
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
CHECKSUM=0
ROW_FORMAT=Dynamic
DELAY_KEY_WRITE=0
;

ALTER TABLE `0_cust_branch` ADD COLUMN `referral_id`  int(10) NOT NULL AFTER `dimension2_id`;