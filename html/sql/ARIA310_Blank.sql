-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.0.51b-community-nt


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema aria310
--

CREATE DATABASE IF NOT EXISTS aria310;
USE aria310;

--
-- Definition of table `0_areas`
--

DROP TABLE IF EXISTS `0_areas`;
CREATE TABLE `0_areas` (
  `area_code` int(11) NOT NULL auto_increment,
  `description` varchar(60) NOT NULL default '',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`area_code`),
  UNIQUE KEY `description` (`description`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_areas`
--

/*!40000 ALTER TABLE `0_areas` DISABLE KEYS */;
INSERT INTO `0_areas` (`area_code`,`description`,`inactive`) VALUES 
 (1,'Global',0);
/*!40000 ALTER TABLE `0_areas` ENABLE KEYS */;


--
-- Definition of table `0_attachments`
--

DROP TABLE IF EXISTS `0_attachments`;
CREATE TABLE `0_attachments` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `description` varchar(60) NOT NULL default '',
  `type_no` int(11) NOT NULL default '0',
  `trans_no` int(11) NOT NULL default '0',
  `unique_name` varchar(60) NOT NULL default '',
  `tran_date` date NOT NULL default '0000-00-00',
  `filename` varchar(60) NOT NULL default '',
  `filesize` int(11) NOT NULL default '0',
  `filetype` varchar(60) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `type_no` (`type_no`,`trans_no`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_attachments`
--

/*!40000 ALTER TABLE `0_attachments` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_attachments` ENABLE KEYS */;


--
-- Definition of table `0_audit_trail`
--

DROP TABLE IF EXISTS `0_audit_trail`;
CREATE TABLE `0_audit_trail` (
  `id` int(11) NOT NULL auto_increment,
  `type` smallint(6) unsigned NOT NULL default '0',
  `trans_no` int(11) unsigned NOT NULL default '0',
  `user` smallint(6) unsigned NOT NULL default '0',
  `stamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `description` varchar(60) default NULL,
  `fiscal_year` int(11) NOT NULL,
  `gl_date` date NOT NULL default '0000-00-00',
  `gl_seq` int(11) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `fiscal_year` (`fiscal_year`,`gl_seq`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_audit_trail`
--

/*!40000 ALTER TABLE `0_audit_trail` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_audit_trail` ENABLE KEYS */;


--
-- Definition of table `0_bank_accounts`
--

DROP TABLE IF EXISTS `0_bank_accounts`;
CREATE TABLE `0_bank_accounts` (
  `account_code` varchar(11) NOT NULL default '',
  `account_type` smallint(6) NOT NULL default '0',
  `bank_account_name` varchar(60) NOT NULL default '',
  `bank_account_number` varchar(100) NOT NULL default '',
  `bank_name` varchar(60) NOT NULL default '',
  `bank_address` tinytext,
  `bank_curr_code` char(3) NOT NULL default '',
  `dflt_curr_act` tinyint(1) NOT NULL default '0',
  `id` smallint(6) NOT NULL auto_increment,
  `last_reconciled_date` timestamp NOT NULL default '0000-00-00 00:00:00',
  `ending_reconcile_balance` double NOT NULL default '0',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `bank_account_name` (`bank_account_name`),
  KEY `bank_account_number` (`bank_account_number`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_bank_accounts`
--

/*!40000 ALTER TABLE `0_bank_accounts` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_bank_accounts` ENABLE KEYS */;


--
-- Definition of table `0_bank_trans`
--

DROP TABLE IF EXISTS `0_bank_trans`;
CREATE TABLE `0_bank_trans` (
  `id` int(11) NOT NULL auto_increment,
  `type` smallint(6) default NULL,
  `trans_no` int(11) default NULL,
  `bank_act` varchar(11) default NULL,
  `ref` varchar(40) default NULL,
  `trans_date` date NOT NULL default '0000-00-00',
  `amount` double default NULL,
  `dimension_id` int(11) NOT NULL default '0',
  `dimension2_id` int(11) NOT NULL default '0',
  `person_type_id` int(11) NOT NULL default '0',
  `person_id` tinyblob,
  `reconciled` date default NULL,
  PRIMARY KEY  (`id`),
  KEY `bank_act` (`bank_act`,`ref`),
  KEY `type` (`type`,`trans_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_bank_trans`
--

/*!40000 ALTER TABLE `0_bank_trans` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_bank_trans` ENABLE KEYS */;


--
-- Definition of table `0_bom`
--

DROP TABLE IF EXISTS `0_bom`;
CREATE TABLE `0_bom` (
  `id` int(11) NOT NULL auto_increment,
  `parent` char(20) NOT NULL default '',
  `component` char(20) NOT NULL default '',
  `workcentre_added` int(11) NOT NULL default '0',
  `loc_code` char(5) NOT NULL default '',
  `quantity` double NOT NULL default '1',
  PRIMARY KEY  (`parent`,`component`,`workcentre_added`,`loc_code`),
  KEY `component` (`component`),
  KEY `id` (`id`),
  KEY `loc_code` (`loc_code`),
  KEY `parent` (`parent`,`loc_code`),
  KEY `Parent_2` (`parent`),
  KEY `workcentre_added` (`workcentre_added`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_bom`
--

/*!40000 ALTER TABLE `0_bom` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_bom` ENABLE KEYS */;


--
-- Definition of table `0_budget_trans`
--

DROP TABLE IF EXISTS `0_budget_trans`;
CREATE TABLE `0_budget_trans` (
  `counter` int(11) NOT NULL auto_increment,
  `type` smallint(6) NOT NULL default '0',
  `type_no` bigint(16) NOT NULL default '1',
  `tran_date` date NOT NULL default '0000-00-00',
  `account` varchar(11) NOT NULL default '',
  `memo_` tinytext NOT NULL,
  `amount` double NOT NULL default '0',
  `dimension_id` int(11) default '0',
  `dimension2_id` int(11) default '0',
  `person_type_id` int(11) default NULL,
  `person_id` tinyblob,
  PRIMARY KEY  (`counter`),
  KEY `Type_and_Number` (`type`,`type_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_budget_trans`
--

/*!40000 ALTER TABLE `0_budget_trans` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_budget_trans` ENABLE KEYS */;


--
-- Definition of table `0_chart_class`
--

DROP TABLE IF EXISTS `0_chart_class`;
CREATE TABLE `0_chart_class` (
  `cid` int(11) NOT NULL default '0',
  `class_name` varchar(60) NOT NULL default '',
  `ctype` tinyint(1) NOT NULL default '0',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`cid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_chart_class`
--

/*!40000 ALTER TABLE `0_chart_class` DISABLE KEYS */;
INSERT INTO `0_chart_class` (`cid`,`class_name`,`ctype`,`inactive`) VALUES 
 (1,'Assets',1,0),
 (2,'Liabilities',2,0),
 (3,'Income',4,0),
 (4,'Costs',6,0);
/*!40000 ALTER TABLE `0_chart_class` ENABLE KEYS */;


--
-- Definition of table `0_chart_master`
--

DROP TABLE IF EXISTS `0_chart_master`;
CREATE TABLE `0_chart_master` (
  `account_code` varchar(11) NOT NULL default '',
  `account_code2` varchar(11) default '',
  `account_name` varchar(60) NOT NULL default '',
  `account_type` int(11) NOT NULL default '0',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`account_code`),
  KEY `account_code` (`account_code`),
  KEY `account_name` (`account_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_chart_master`
--

/*!40000 ALTER TABLE `0_chart_master` DISABLE KEYS */;
INSERT INTO `0_chart_master` (`account_code`,`account_code2`,`account_name`,`account_type`,`inactive`) VALUES 
 ('1060','','Checking Account',1,0),
 ('1065','','Petty Cash',1,0),
 ('1200','','Accounts Receivables',1,0),
 ('1205','','Allowance for doubtful accounts',1,0),
 ('1510','','Inventory',2,0),
 ('1520','','Stocks of Raw Materials',2,0),
 ('1530','','Stocks of Work In Progress',2,0),
 ('1540','','Stocks of Finsihed Goods',2,0),
 ('1550','','Goods Received Clearing account',2,0),
 ('1820','','Office Furniture &amp; Equipment',3,0),
 ('1825','','Accum. Amort. -Furn. &amp; Equip.',3,0),
 ('1840','','Vehicle',3,0),
 ('1845','','Accum. Amort. -Vehicle',3,0),
 ('2100','','Accounts Payable',4,0),
 ('2110','','Accrued Income Tax - Federal',4,0),
 ('2120','','Accrued Income Tax - State',4,0),
 ('2130','','Accrued Franchise Tax',4,0),
 ('2140','','Accrued Real &amp; Personal Prop Tax',4,0),
 ('2145','','VAT input Tax',1,0),
 ('2150','','VAT Output Tax',4,0),
 ('2160','','Accrued Use Tax Payable',4,0),
 ('2210','','Accrued Wages',4,0);
INSERT INTO `0_chart_master` (`account_code`,`account_code2`,`account_name`,`account_type`,`inactive`) VALUES 
 ('2220','','Accrued Comp Time',4,0),
 ('2230','','Accrued Holiday Pay',4,0),
 ('2240','','Accrued Vacation Pay',4,0),
 ('2310','','Accr. Benefits - 401K',4,0),
 ('2320','','Accr. Benefits - Stock Purchase',4,0),
 ('2330','','Accr. Benefits - Med, Den',4,0),
 ('2340','','Accr. Benefits - Payroll Taxes',4,0),
 ('2350','','Accr. Benefits - Credit Union',4,0),
 ('2360','','Accr. Benefits - Savings Bond',4,0),
 ('2370','','Accr. Benefits - Garnish',4,0),
 ('2380','','Accr. Benefits - Charity Cont.',4,0),
 ('2620','','Bank Loans',5,0),
 ('2680','','Loans from Shareholders',5,0),
 ('3350','','Common Shares',6,0),
 ('3590','','Retained Earnings - prior years',7,0),
 ('4010','','Sales',8,0),
 ('4430','','Shipping &amp; Handling',9,0),
 ('4440','','Interest',9,0),
 ('4450','','Foreign Exchange Gain',9,0),
 ('4500','','Prompt Payment Discounts',9,0),
 ('4510','','Discounts Given',9,0),
 ('5010','','Cost of Goods Sold - Retail',10,0);
INSERT INTO `0_chart_master` (`account_code`,`account_code2`,`account_name`,`account_type`,`inactive`) VALUES 
 ('5020','','Material Usage Varaiance',10,0),
 ('5030','','Consumable Materials',10,0),
 ('5040','','Purchase price Variance',10,0),
 ('5050','','Purchases of materials',10,0),
 ('5060','','Discounts Received',10,0),
 ('5100','','Freight',10,0),
 ('5410','','Wages &amp; Salaries',11,0),
 ('5420','','Wages - Overtime',11,0),
 ('5430','','Benefits - Comp Time',11,0),
 ('5440','','Benefits - Payroll Taxes',11,0),
 ('5450','','Benefits - Workers Comp',11,0),
 ('5460','','Benefits - Pension',11,0),
 ('5470','','Benefits - General Benefits',11,0),
 ('5510','','Inc Tax Exp - Federal',11,0),
 ('5520','','Inc Tax Exp - State',11,0),
 ('5530','','Taxes - Real Estate',11,0),
 ('5540','','Taxes - Personal Property',11,0),
 ('5550','','Taxes - Franchise',11,0),
 ('5560','','Taxes - Foreign Withholding',11,0),
 ('5610','','Accounting &amp; Legal',12,0),
 ('5615','','Advertising &amp; Promotions',12,0),
 ('5620','','Bad Debts',12,0);
INSERT INTO `0_chart_master` (`account_code`,`account_code2`,`account_name`,`account_type`,`inactive`) VALUES 
 ('5660','','Amortization Expense',12,0),
 ('5685','','Insurance',12,0),
 ('5690','','Interest &amp; Bank Charges',12,0),
 ('5700','','Office Supplies',12,0),
 ('5760','','Rent',12,0),
 ('5765','','Repair &amp; Maintenance',12,0),
 ('5780','','Telephone',12,0),
 ('5785','','Travel &amp; Entertainment',12,0),
 ('5790','','Utilities',12,0),
 ('5795','','Registrations',12,0),
 ('5800','','Licenses',12,0),
 ('5810','','Foreign Exchange Loss',12,0),
 ('9990','','Year Profit/Loss',12,0),
 ('2500','','Expanded Witholding Tax Payables - Services',4,0),
 ('2501','','Expanded Witholding Tax Payables - Goods',4,0),
 ('2052','','Expanded Witholding Tax Receivables',1,0),
 ('1000','','Cash in Bank',1,0);
/*!40000 ALTER TABLE `0_chart_master` ENABLE KEYS */;


--
-- Definition of table `0_chart_types`
--

DROP TABLE IF EXISTS `0_chart_types`;
CREATE TABLE `0_chart_types` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(60) NOT NULL default '',
  `class_id` tinyint(1) NOT NULL default '0',
  `parent` int(11) NOT NULL default '-1',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_chart_types`
--

/*!40000 ALTER TABLE `0_chart_types` DISABLE KEYS */;
INSERT INTO `0_chart_types` (`id`,`name`,`class_id`,`parent`,`inactive`) VALUES 
 (1,'Current Assets',1,-1,0),
 (2,'Inventory Assets',1,-1,0),
 (3,'Capital Assets',1,-1,0),
 (4,'Current Liabilities',2,-1,0),
 (5,'Long Term Liabilities',2,-1,0),
 (6,'Share Capital',2,-1,0),
 (7,'Retained Earnings',2,-1,0),
 (8,'Sales Revenue',3,-1,0),
 (9,'Other Revenue',3,-1,0),
 (10,'Cost of Goods Sold',4,-1,0),
 (11,'Payroll Expenses',4,-1,0),
 (12,'General &amp; Administrative expenses',4,-1,0);
/*!40000 ALTER TABLE `0_chart_types` ENABLE KEYS */;


--
-- Definition of table `0_check_account`
--

DROP TABLE IF EXISTS `0_check_account`;
CREATE TABLE `0_check_account` (
  `account_id` int(11) NOT NULL auto_increment,
  `bank_ref` varchar(11) collate utf8_unicode_ci NOT NULL,
  `next_reference` varchar(100) collate utf8_unicode_ci NOT NULL,
  PRIMARY KEY  USING BTREE (`account_id`),
  UNIQUE KEY `bank_ref` (`bank_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `0_check_account`
--

/*!40000 ALTER TABLE `0_check_account` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_check_account` ENABLE KEYS */;


--
-- Definition of table `0_check_trans`
--

DROP TABLE IF EXISTS `0_check_trans`;
CREATE TABLE `0_check_trans` (
  `id` int(11) NOT NULL auto_increment,
  `check_ref` varchar(100) collate utf8_unicode_ci NOT NULL,
  `bank_trans_id` int(11) NOT NULL,
  `cheque_bank_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `cheque_ref` USING BTREE (`check_ref`,`cheque_bank_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `0_check_trans`
--

/*!40000 ALTER TABLE `0_check_trans` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_check_trans` ENABLE KEYS */;


--
-- Definition of table `0_comments`
--

DROP TABLE IF EXISTS `0_comments`;
CREATE TABLE `0_comments` (
  `type` int(11) NOT NULL default '0',
  `id` int(11) NOT NULL default '0',
  `date_` date default '0000-00-00',
  `memo_` tinytext
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_comments`
--

/*!40000 ALTER TABLE `0_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_comments` ENABLE KEYS */;


--
-- Definition of table `0_company`
--

DROP TABLE IF EXISTS `0_company`;
CREATE TABLE `0_company` (
  `coy_code` int(11) NOT NULL default '1',
  `coy_name` varchar(60) NOT NULL default '',
  `gst_no` varchar(25) NOT NULL default '',
  `coy_no` varchar(25) NOT NULL default '0',
  `tax_prd` int(11) NOT NULL default '1',
  `tax_last` int(11) NOT NULL default '1',
  `postal_address` tinytext NOT NULL,
  `phone` varchar(30) NOT NULL default '',
  `fax` varchar(30) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `coy_logo` varchar(100) NOT NULL default '',
  `domicile` varchar(55) NOT NULL default '',
  `curr_default` char(3) NOT NULL default '',
  `debtors_act` varchar(11) NOT NULL default '',
  `pyt_discount_act` varchar(11) NOT NULL default '',
  `creditors_act` varchar(11) NOT NULL default '',
  `bank_charge_act` varchar(11) NOT NULL default '',
  `exchange_diff_act` varchar(11) NOT NULL default '',
  `profit_loss_year_act` varchar(11) NOT NULL default '',
  `retained_earnings_act` varchar(11) NOT NULL default '',
  `freight_act` varchar(11) NOT NULL default '',
  `default_sales_act` varchar(11) NOT NULL default '',
  `default_sales_discount_act` varchar(11) NOT NULL default '',
  `default_prompt_payment_act` varchar(11) NOT NULL default '',
  `default_inventory_act` varchar(11) NOT NULL default '',
  `default_cogs_act` varchar(11) NOT NULL default '',
  `default_adj_act` varchar(11) NOT NULL default '',
  `default_inv_sales_act` varchar(11) NOT NULL default '',
  `default_assembly_act` varchar(11) NOT NULL default '',
  `payroll_act` varchar(11) NOT NULL default '',
  `allow_negative_stock` tinyint(1) NOT NULL default '0',
  `po_over_receive` int(11) NOT NULL default '10',
  `po_over_charge` int(11) NOT NULL default '10',
  `default_credit_limit` int(11) NOT NULL default '1000',
  `default_workorder_required` int(11) NOT NULL default '20',
  `default_dim_required` int(11) NOT NULL default '20',
  `past_due_days` int(11) NOT NULL default '30',
  `use_dimension` tinyint(1) default '0',
  `f_year` int(11) NOT NULL default '1',
  `no_item_list` tinyint(1) NOT NULL default '0',
  `no_customer_list` tinyint(1) NOT NULL default '0',
  `no_supplier_list` tinyint(1) NOT NULL default '0',
  `base_sales` int(11) NOT NULL default '-1',
  `foreign_codes` tinyint(1) NOT NULL default '0',
  `accumulate_shipping` tinyint(1) NOT NULL default '0',
  `legal_text` tinytext NOT NULL,
  `default_delivery_required` smallint(6) NOT NULL default '1',
  `version_id` varchar(11) NOT NULL default '',
  `time_zone` tinyint(1) NOT NULL default '0',
  `add_pct` int(5) NOT NULL default '-1',
  `round_to` int(5) NOT NULL default '1',
  `login_tout` smallint(6) NOT NULL default '600',
  PRIMARY KEY  (`coy_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_company`
--

/*!40000 ALTER TABLE `0_company` DISABLE KEYS */;
INSERT INTO `0_company` (`coy_code`,`coy_name`,`gst_no`,`coy_no`,`tax_prd`,`tax_last`,`postal_address`,`phone`,`fax`,`email`,`coy_logo`,`domicile`,`curr_default`,`debtors_act`,`pyt_discount_act`,`creditors_act`,`bank_charge_act`,`exchange_diff_act`,`profit_loss_year_act`,`retained_earnings_act`,`freight_act`,`default_sales_act`,`default_sales_discount_act`,`default_prompt_payment_act`,`default_inventory_act`,`default_cogs_act`,`default_adj_act`,`default_inv_sales_act`,`default_assembly_act`,`payroll_act`,`allow_negative_stock`,`po_over_receive`,`po_over_charge`,`default_credit_limit`,`default_workorder_required`,`default_dim_required`,`past_due_days`,`use_dimension`,`f_year`,`no_item_list`,`no_customer_list`,`no_supplier_list`,`base_sales`,`foreign_codes`,`accumulate_shipping`,`legal_text`,`default_delivery_required`,`version_id`,`time_zone`,`add_pct`,`round_to`,`login_tout`) VALUES 
 (1,'Need Ink Sales &amp; Services','','',1,1,'288 P. Rodriguez St., Poblacion, Lapu Lapu City, Cebu, Philippines','+6332 495 6954','+6332 340 4954','','','','PHP','1200','5060','2100','5690','4450','9990','3590','4430','4010','4510','4500','1510','5010','5040','4010','1530','5000',0,10,10,1000,20,20,30,2,4,0,0,0,1,0,0,'',1,'2.2',0,-1,1,600);
/*!40000 ALTER TABLE `0_company` ENABLE KEYS */;


--
-- Definition of table `0_credit_status`
--

DROP TABLE IF EXISTS `0_credit_status`;
CREATE TABLE `0_credit_status` (
  `id` int(11) NOT NULL auto_increment,
  `reason_description` char(100) NOT NULL default '',
  `dissallow_invoices` tinyint(1) NOT NULL default '0',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `reason_description` (`reason_description`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_credit_status`
--

/*!40000 ALTER TABLE `0_credit_status` DISABLE KEYS */;
INSERT INTO `0_credit_status` (`id`,`reason_description`,`dissallow_invoices`,`inactive`) VALUES 
 (1,'Good History',0,0),
 (3,'No more work until payment received',1,0),
 (4,'In liquidation',1,0),
 (5,'Prepaid',1,0);
/*!40000 ALTER TABLE `0_credit_status` ENABLE KEYS */;


--
-- Definition of table `0_currencies`
--

DROP TABLE IF EXISTS `0_currencies`;
CREATE TABLE `0_currencies` (
  `currency` varchar(60) NOT NULL default '',
  `curr_abrev` char(3) NOT NULL default '',
  `curr_symbol` varchar(10) NOT NULL default '',
  `country` varchar(100) NOT NULL default '',
  `hundreds_name` varchar(15) NOT NULL default '',
  `auto_update` tinyint(1) NOT NULL default '1',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`curr_abrev`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_currencies`
--

/*!40000 ALTER TABLE `0_currencies` DISABLE KEYS */;
INSERT INTO `0_currencies` (`currency`,`curr_abrev`,`curr_symbol`,`country`,`hundreds_name`,`auto_update`,`inactive`) VALUES 
 ('US Dollars','USD','$','United States','Cents',1,0),
 ('Phlippine Peso','PHP','PHP','Philippines','Hundreds',1,0);
/*!40000 ALTER TABLE `0_currencies` ENABLE KEYS */;


--
-- Definition of table `0_cust_allocations`
--

DROP TABLE IF EXISTS `0_cust_allocations`;
CREATE TABLE `0_cust_allocations` (
  `id` int(11) NOT NULL auto_increment,
  `amt` double unsigned default NULL,
  `date_alloc` date NOT NULL default '0000-00-00',
  `trans_no_from` int(11) default NULL,
  `trans_type_from` int(11) default NULL,
  `trans_no_to` int(11) default NULL,
  `trans_type_to` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_cust_allocations`
--

/*!40000 ALTER TABLE `0_cust_allocations` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_cust_allocations` ENABLE KEYS */;


--
-- Definition of table `0_cust_branch`
--

DROP TABLE IF EXISTS `0_cust_branch`;
CREATE TABLE `0_cust_branch` (
  `branch_code` int(11) NOT NULL auto_increment,
  `debtor_no` int(11) NOT NULL default '0',
  `br_name` varchar(60) NOT NULL default '',
  `branch_ref` varchar(30) NOT NULL default '',
  `br_address` tinytext NOT NULL,
  `area` int(11) default NULL,
  `salesman` int(11) NOT NULL default '0',
  `phone` varchar(30) NOT NULL default '',
  `phone2` varchar(30) NOT NULL default '',
  `fax` varchar(30) NOT NULL default '',
  `contact_name` varchar(60) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `default_location` varchar(5) NOT NULL default '',
  `tax_group_id` int(11) default NULL,
  `sales_account` varchar(11) default NULL,
  `sales_discount_account` varchar(11) default NULL,
  `receivables_account` varchar(11) default NULL,
  `payment_discount_account` varchar(11) default NULL,
  `default_ship_via` int(11) NOT NULL default '1',
  `disable_trans` tinyint(4) NOT NULL default '0',
  `br_post_address` tinytext NOT NULL,
  `group_no` int(11) NOT NULL default '0',
  `notes` tinytext NOT NULL,
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`branch_code`,`debtor_no`),
  KEY `branch_code` (`branch_code`),
  KEY `br_name` (`br_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_cust_branch`
--

/*!40000 ALTER TABLE `0_cust_branch` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_cust_branch` ENABLE KEYS */;


--
-- Definition of table `0_debtor_trans`
--

DROP TABLE IF EXISTS `0_debtor_trans`;
CREATE TABLE `0_debtor_trans` (
  `trans_no` int(11) unsigned NOT NULL default '0',
  `type` smallint(6) unsigned NOT NULL default '0',
  `version` tinyint(1) unsigned NOT NULL default '0',
  `debtor_no` int(11) unsigned default NULL,
  `branch_code` int(11) NOT NULL default '-1',
  `tran_date` date NOT NULL default '0000-00-00',
  `due_date` date NOT NULL default '0000-00-00',
  `reference` varchar(60) NOT NULL default '',
  `tpe` int(11) NOT NULL default '0',
  `order_` int(11) NOT NULL default '0',
  `ov_amount` double NOT NULL default '0',
  `ov_gst` double NOT NULL default '0',
  `ov_freight` double NOT NULL default '0',
  `ov_freight_tax` double NOT NULL default '0',
  `ov_discount` double NOT NULL default '0',
  `alloc` double NOT NULL default '0',
  `rate` double NOT NULL default '1',
  `ship_via` int(11) default NULL,
  `trans_link` int(11) NOT NULL default '0',
  `dimension_id` int(11) NOT NULL default '0',
  `dimension2_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`trans_no`,`type`),
  KEY `debtor_no` (`debtor_no`,`branch_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_debtor_trans`
--

/*!40000 ALTER TABLE `0_debtor_trans` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_debtor_trans` ENABLE KEYS */;


--
-- Definition of table `0_debtor_trans_details`
--

DROP TABLE IF EXISTS `0_debtor_trans_details`;
CREATE TABLE `0_debtor_trans_details` (
  `id` int(11) NOT NULL auto_increment,
  `debtor_trans_no` int(11) default NULL,
  `debtor_trans_type` int(11) default NULL,
  `stock_id` varchar(20) NOT NULL default '',
  `description` tinytext,
  `unit_price` double NOT NULL default '0',
  `unit_tax` double NOT NULL default '0',
  `quantity` double NOT NULL default '0',
  `discount_percent` double NOT NULL default '0',
  `standard_cost` double NOT NULL default '0',
  `qty_done` double NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_debtor_trans_details`
--

/*!40000 ALTER TABLE `0_debtor_trans_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_debtor_trans_details` ENABLE KEYS */;


--
-- Definition of table `0_debtors_master`
--

DROP TABLE IF EXISTS `0_debtors_master`;
CREATE TABLE `0_debtors_master` (
  `debtor_no` int(11) NOT NULL auto_increment,
  `name` varchar(100) NOT NULL default '',
  `debtor_ref` varchar(30) NOT NULL,
  `address` tinytext,
  `email` varchar(100) NOT NULL default '',
  `tax_id` varchar(55) NOT NULL default '',
  `curr_code` char(3) NOT NULL default '',
  `sales_type` int(11) NOT NULL default '1',
  `dimension_id` int(11) NOT NULL default '0',
  `dimension2_id` int(11) NOT NULL default '0',
  `credit_status` int(11) NOT NULL default '0',
  `payment_terms` int(11) default NULL,
  `discount` double NOT NULL default '0',
  `pymt_discount` double NOT NULL default '0',
  `credit_limit` float NOT NULL default '1000',
  `notes` tinytext NOT NULL,
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`debtor_no`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_debtors_master`
--

/*!40000 ALTER TABLE `0_debtors_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_debtors_master` ENABLE KEYS */;


--
-- Definition of table `0_dimensions`
--

DROP TABLE IF EXISTS `0_dimensions`;
CREATE TABLE `0_dimensions` (
  `id` int(11) NOT NULL auto_increment,
  `reference` varchar(60) NOT NULL default '',
  `name` varchar(60) NOT NULL default '',
  `type_` tinyint(1) NOT NULL default '1',
  `closed` tinyint(1) NOT NULL default '0',
  `date_` date NOT NULL default '0000-00-00',
  `due_date` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `reference` (`reference`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_dimensions`
--

/*!40000 ALTER TABLE `0_dimensions` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_dimensions` ENABLE KEYS */;


--
-- Definition of table `0_exchange_rates`
--

DROP TABLE IF EXISTS `0_exchange_rates`;
CREATE TABLE `0_exchange_rates` (
  `id` int(11) NOT NULL auto_increment,
  `curr_code` char(3) NOT NULL default '',
  `rate_buy` double NOT NULL default '0',
  `rate_sell` double NOT NULL default '0',
  `date_` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `curr_code` (`curr_code`,`date_`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_exchange_rates`
--

/*!40000 ALTER TABLE `0_exchange_rates` DISABLE KEYS */;
INSERT INTO `0_exchange_rates` (`id`,`curr_code`,`rate_buy`,`rate_sell`,`date_`) VALUES 
 (1,'USD',46.6630380171,46.6630380171,'2010-06-09'),
 (2,'USD',43.009764093792,43.009764093792,'2010-10-26'),
 (3,'USD',43.101681460634,43.101681460634,'2010-10-28'),
 (4,'USD',44.036784641313,44.036784641313,'2010-11-23'),
 (5,'USD',44.329678638941,44.329678638941,'2010-11-29'),
 (6,'USD',43.418742921857,43.418742921857,'2011-03-25'),
 (7,'USD',42.6388693573,42.6388693573,'2011-07-22'),
 (8,'USD',41.9347901492,41.9347901492,'2011-08-02');
/*!40000 ALTER TABLE `0_exchange_rates` ENABLE KEYS */;


--
-- Definition of table `0_fiscal_year`
--

DROP TABLE IF EXISTS `0_fiscal_year`;
CREATE TABLE `0_fiscal_year` (
  `id` int(11) NOT NULL auto_increment,
  `begin` date default '0000-00-00',
  `end` date default '0000-00-00',
  `closed` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_fiscal_year`
--

/*!40000 ALTER TABLE `0_fiscal_year` DISABLE KEYS */;
INSERT INTO `0_fiscal_year` (`id`,`begin`,`end`,`closed`) VALUES 
 (1,'2008-01-01','2008-12-31',1),
 (2,'2009-01-01','2009-12-31',1),
 (3,'2010-01-01','2010-12-31',0),
 (4,'2011-01-01','2011-12-31',0);
/*!40000 ALTER TABLE `0_fiscal_year` ENABLE KEYS */;


--
-- Definition of table `0_gl_trans`
--

DROP TABLE IF EXISTS `0_gl_trans`;
CREATE TABLE `0_gl_trans` (
  `counter` int(11) NOT NULL auto_increment,
  `type` smallint(6) NOT NULL default '0',
  `type_no` bigint(16) NOT NULL default '1',
  `tran_date` date NOT NULL default '0000-00-00',
  `account` varchar(11) NOT NULL default '',
  `memo_` tinytext NOT NULL,
  `amount` double NOT NULL default '0',
  `dimension_id` int(11) NOT NULL default '0',
  `dimension2_id` int(11) NOT NULL default '0',
  `person_type_id` int(11) default NULL,
  `person_id` tinyblob,
  PRIMARY KEY  (`counter`),
  KEY `Type_and_Number` (`type`,`type_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_gl_trans`
--

/*!40000 ALTER TABLE `0_gl_trans` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_gl_trans` ENABLE KEYS */;


--
-- Definition of table `0_grn_batch`
--

DROP TABLE IF EXISTS `0_grn_batch`;
CREATE TABLE `0_grn_batch` (
  `id` int(11) NOT NULL auto_increment,
  `supplier_id` int(11) NOT NULL default '0',
  `purch_order_no` int(11) default NULL,
  `reference` varchar(60) NOT NULL default '',
  `delivery_date` date NOT NULL default '0000-00-00',
  `loc_code` varchar(5) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_grn_batch`
--

/*!40000 ALTER TABLE `0_grn_batch` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_grn_batch` ENABLE KEYS */;


--
-- Definition of table `0_grn_items`
--

DROP TABLE IF EXISTS `0_grn_items`;
CREATE TABLE `0_grn_items` (
  `id` int(11) NOT NULL auto_increment,
  `grn_batch_id` int(11) default NULL,
  `po_detail_item` int(11) NOT NULL default '0',
  `item_code` varchar(20) NOT NULL default '',
  `description` tinytext,
  `qty_recd` double NOT NULL default '0',
  `quantity_inv` double NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_grn_items`
--

/*!40000 ALTER TABLE `0_grn_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_grn_items` ENABLE KEYS */;


--
-- Definition of table `0_groups`
--

DROP TABLE IF EXISTS `0_groups`;
CREATE TABLE `0_groups` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `description` varchar(60) NOT NULL default '',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `description` (`description`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_groups`
--

/*!40000 ALTER TABLE `0_groups` DISABLE KEYS */;
INSERT INTO `0_groups` (`id`,`description`,`inactive`) VALUES 
 (1,'Small',0),
 (2,'Medium',0),
 (3,'Large',0);
/*!40000 ALTER TABLE `0_groups` ENABLE KEYS */;


--
-- Definition of table `0_item_codes`
--

DROP TABLE IF EXISTS `0_item_codes`;
CREATE TABLE `0_item_codes` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `item_code` varchar(20) NOT NULL,
  `stock_id` varchar(20) NOT NULL,
  `description` varchar(200) NOT NULL default '',
  `category_id` smallint(6) unsigned NOT NULL,
  `quantity` double NOT NULL default '1',
  `is_foreign` tinyint(1) NOT NULL default '0',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `stock_id` (`stock_id`,`item_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_item_codes`
--

/*!40000 ALTER TABLE `0_item_codes` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_item_codes` ENABLE KEYS */;


--
-- Definition of table `0_item_tax_type_exemptions`
--

DROP TABLE IF EXISTS `0_item_tax_type_exemptions`;
CREATE TABLE `0_item_tax_type_exemptions` (
  `item_tax_type_id` int(11) NOT NULL default '0',
  `tax_type_id` int(11) NOT NULL default '0',
  PRIMARY KEY  (`item_tax_type_id`,`tax_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_item_tax_type_exemptions`
--

/*!40000 ALTER TABLE `0_item_tax_type_exemptions` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_item_tax_type_exemptions` ENABLE KEYS */;


--
-- Definition of table `0_item_tax_types`
--

DROP TABLE IF EXISTS `0_item_tax_types`;
CREATE TABLE `0_item_tax_types` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(60) NOT NULL default '',
  `exempt` tinyint(1) NOT NULL default '0',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_item_tax_types`
--

/*!40000 ALTER TABLE `0_item_tax_types` DISABLE KEYS */;
INSERT INTO `0_item_tax_types` (`id`,`name`,`exempt`,`inactive`) VALUES 
 (1,'VATABLE',0,0),
 (2,'NON VAT',1,0);
/*!40000 ALTER TABLE `0_item_tax_types` ENABLE KEYS */;


--
-- Definition of table `0_item_units`
--

DROP TABLE IF EXISTS `0_item_units`;
CREATE TABLE `0_item_units` (
  `abbr` varchar(20) NOT NULL,
  `name` varchar(40) NOT NULL,
  `decimals` tinyint(2) NOT NULL,
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`abbr`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_item_units`
--

/*!40000 ALTER TABLE `0_item_units` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_item_units` ENABLE KEYS */;


--
-- Definition of table `0_loc_stock`
--

DROP TABLE IF EXISTS `0_loc_stock`;
CREATE TABLE `0_loc_stock` (
  `loc_code` char(5) NOT NULL default '',
  `stock_id` char(20) NOT NULL default '',
  `reorder_level` bigint(20) NOT NULL default '0',
  PRIMARY KEY  (`loc_code`,`stock_id`),
  KEY `stock_id` (`stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_loc_stock`
--

/*!40000 ALTER TABLE `0_loc_stock` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_loc_stock` ENABLE KEYS */;


--
-- Definition of table `0_locations`
--

DROP TABLE IF EXISTS `0_locations`;
CREATE TABLE `0_locations` (
  `loc_code` varchar(5) NOT NULL default '',
  `location_name` varchar(60) NOT NULL default '',
  `delivery_address` tinytext NOT NULL,
  `phone` varchar(30) NOT NULL default '',
  `phone2` varchar(30) NOT NULL default '',
  `fax` varchar(30) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `contact` varchar(30) NOT NULL default '',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`loc_code`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_locations`
--

/*!40000 ALTER TABLE `0_locations` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_locations` ENABLE KEYS */;


--
-- Definition of table `0_movement_types`
--

DROP TABLE IF EXISTS `0_movement_types`;
CREATE TABLE `0_movement_types` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(60) NOT NULL default '',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_movement_types`
--

/*!40000 ALTER TABLE `0_movement_types` DISABLE KEYS */;
INSERT INTO `0_movement_types` (`id`,`name`,`inactive`) VALUES 
 (1,'Adjustment',0);
/*!40000 ALTER TABLE `0_movement_types` ENABLE KEYS */;


--
-- Definition of table `0_payment_terms`
--

DROP TABLE IF EXISTS `0_payment_terms`;
CREATE TABLE `0_payment_terms` (
  `terms_indicator` int(11) NOT NULL auto_increment,
  `terms` char(80) NOT NULL default '',
  `days_before_due` smallint(6) NOT NULL default '0',
  `day_in_following_month` smallint(6) NOT NULL default '0',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`terms_indicator`),
  UNIQUE KEY `terms` (`terms`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_payment_terms`
--

/*!40000 ALTER TABLE `0_payment_terms` DISABLE KEYS */;
INSERT INTO `0_payment_terms` (`terms_indicator`,`terms`,`days_before_due`,`day_in_following_month`,`inactive`) VALUES 
 (1,'Due 15th Of the Following Month',0,17,0),
 (2,'Due By End Of The Following Month',0,30,0),
 (3,'Payment due within 10 days',10,0,0),
 (4,'Cash Only',1,0,0);
/*!40000 ALTER TABLE `0_payment_terms` ENABLE KEYS */;


--
-- Definition of table `0_prices`
--

DROP TABLE IF EXISTS `0_prices`;
CREATE TABLE `0_prices` (
  `id` int(11) NOT NULL auto_increment,
  `stock_id` varchar(20) NOT NULL default '',
  `sales_type_id` int(11) NOT NULL default '0',
  `curr_abrev` char(3) NOT NULL default '',
  `price` double NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `price` (`stock_id`,`sales_type_id`,`curr_abrev`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_prices`
--

/*!40000 ALTER TABLE `0_prices` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_prices` ENABLE KEYS */;


--
-- Definition of table `0_print_profiles`
--

DROP TABLE IF EXISTS `0_print_profiles`;
CREATE TABLE `0_print_profiles` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `profile` varchar(30) NOT NULL,
  `report` varchar(5) default NULL,
  `printer` tinyint(3) unsigned default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `profile` (`profile`,`report`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_print_profiles`
--

/*!40000 ALTER TABLE `0_print_profiles` DISABLE KEYS */;
INSERT INTO `0_print_profiles` (`id`,`profile`,`report`,`printer`) VALUES 
 (1,'Out of office','',0),
 (2,'Sales Department','',0),
 (3,'Central','',2),
 (4,'Sales Department','104',2),
 (5,'Sales Department','105',2),
 (6,'Sales Department','107',2),
 (7,'Sales Department','109',2),
 (8,'Sales Department','110',2),
 (9,'Sales Department','201',2);
/*!40000 ALTER TABLE `0_print_profiles` ENABLE KEYS */;


--
-- Definition of table `0_printers`
--

DROP TABLE IF EXISTS `0_printers`;
CREATE TABLE `0_printers` (
  `id` tinyint(3) unsigned NOT NULL auto_increment,
  `name` varchar(20) NOT NULL,
  `description` varchar(60) NOT NULL,
  `queue` varchar(20) NOT NULL,
  `host` varchar(40) NOT NULL,
  `port` smallint(11) unsigned NOT NULL,
  `timeout` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_printers`
--

/*!40000 ALTER TABLE `0_printers` DISABLE KEYS */;
INSERT INTO `0_printers` (`id`,`name`,`description`,`queue`,`host`,`port`,`timeout`) VALUES 
 (1,'QL500','Label printer','QL500','server',127,20),
 (2,'Samsung','Main network printer','scx4521F','server',515,5),
 (3,'Local','Local print server at user IP','lp','',515,10);
/*!40000 ALTER TABLE `0_printers` ENABLE KEYS */;


--
-- Definition of table `0_purch_data`
--

DROP TABLE IF EXISTS `0_purch_data`;
CREATE TABLE `0_purch_data` (
  `supplier_id` int(11) NOT NULL default '0',
  `stock_id` char(20) NOT NULL default '',
  `price` double NOT NULL default '0',
  `suppliers_uom` char(50) NOT NULL default '',
  `conversion_factor` double NOT NULL default '1',
  `supplier_description` char(50) NOT NULL default '',
  PRIMARY KEY  (`supplier_id`,`stock_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_purch_data`
--

/*!40000 ALTER TABLE `0_purch_data` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_purch_data` ENABLE KEYS */;


--
-- Definition of table `0_purch_order_details`
--

DROP TABLE IF EXISTS `0_purch_order_details`;
CREATE TABLE `0_purch_order_details` (
  `po_detail_item` int(11) NOT NULL auto_increment,
  `order_no` int(11) NOT NULL default '0',
  `item_code` varchar(20) NOT NULL default '',
  `description` tinytext,
  `delivery_date` date NOT NULL default '0000-00-00',
  `qty_invoiced` double NOT NULL default '0',
  `unit_price` double NOT NULL default '0',
  `act_price` double NOT NULL default '0',
  `std_cost_unit` double NOT NULL default '0',
  `quantity_ordered` double NOT NULL default '0',
  `quantity_received` double NOT NULL default '0',
  PRIMARY KEY  (`po_detail_item`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_purch_order_details`
--

/*!40000 ALTER TABLE `0_purch_order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_purch_order_details` ENABLE KEYS */;


--
-- Definition of table `0_purch_orders`
--

DROP TABLE IF EXISTS `0_purch_orders`;
CREATE TABLE `0_purch_orders` (
  `order_no` int(11) NOT NULL auto_increment,
  `supplier_id` int(11) NOT NULL default '0',
  `comments` tinytext,
  `ord_date` date NOT NULL default '0000-00-00',
  `reference` tinytext NOT NULL,
  `requisition_no` tinytext,
  `into_stock_location` varchar(5) NOT NULL default '',
  `delivery_address` tinytext NOT NULL,
  PRIMARY KEY  (`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_purch_orders`
--

/*!40000 ALTER TABLE `0_purch_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_purch_orders` ENABLE KEYS */;


--
-- Definition of table `0_quick_entries`
--

DROP TABLE IF EXISTS `0_quick_entries`;
CREATE TABLE `0_quick_entries` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `type` tinyint(1) NOT NULL default '0',
  `description` varchar(60) NOT NULL,
  `base_amount` double NOT NULL default '0',
  `base_desc` varchar(60) default NULL,
  PRIMARY KEY  (`id`),
  KEY `description` (`description`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_quick_entries`
--

/*!40000 ALTER TABLE `0_quick_entries` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_quick_entries` ENABLE KEYS */;


--
-- Definition of table `0_quick_entry_lines`
--

DROP TABLE IF EXISTS `0_quick_entry_lines`;
CREATE TABLE `0_quick_entry_lines` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `qid` smallint(6) unsigned NOT NULL,
  `amount` double default '0',
  `action` varchar(2) NOT NULL,
  `dest_id` varchar(11) NOT NULL,
  `dimension_id` smallint(6) unsigned default NULL,
  `dimension2_id` smallint(6) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `qid` (`qid`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_quick_entry_lines`
--

/*!40000 ALTER TABLE `0_quick_entry_lines` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_quick_entry_lines` ENABLE KEYS */;


--
-- Definition of table `0_recurrent_invoices`
--

DROP TABLE IF EXISTS `0_recurrent_invoices`;
CREATE TABLE `0_recurrent_invoices` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `description` varchar(60) NOT NULL default '',
  `order_no` int(11) unsigned NOT NULL,
  `debtor_no` int(11) unsigned default NULL,
  `group_no` smallint(6) unsigned default NULL,
  `days` int(11) NOT NULL default '0',
  `monthly` int(11) NOT NULL default '0',
  `begin` date NOT NULL default '0000-00-00',
  `end` date NOT NULL default '0000-00-00',
  `last_sent` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `description` (`description`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_recurrent_invoices`
--

/*!40000 ALTER TABLE `0_recurrent_invoices` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_recurrent_invoices` ENABLE KEYS */;


--
-- Definition of table `0_refs`
--

DROP TABLE IF EXISTS `0_refs`;
CREATE TABLE `0_refs` (
  `id` int(11) NOT NULL default '0',
  `type` int(11) NOT NULL default '0',
  `reference` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_refs`
--

/*!40000 ALTER TABLE `0_refs` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_refs` ENABLE KEYS */;


--
-- Definition of table `0_sales_order_details`
--

DROP TABLE IF EXISTS `0_sales_order_details`;
CREATE TABLE `0_sales_order_details` (
  `id` int(11) NOT NULL auto_increment,
  `order_no` int(11) NOT NULL default '0',
  `trans_type` smallint(6) NOT NULL default '30',
  `stk_code` varchar(20) NOT NULL default '',
  `description` tinytext,
  `qty_sent` double NOT NULL default '0',
  `unit_price` double NOT NULL default '0',
  `quantity` double NOT NULL default '0',
  `discount_percent` double NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_sales_order_details`
--

/*!40000 ALTER TABLE `0_sales_order_details` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_sales_order_details` ENABLE KEYS */;


--
-- Definition of table `0_sales_orders`
--

DROP TABLE IF EXISTS `0_sales_orders`;
CREATE TABLE `0_sales_orders` (
  `order_no` int(11) NOT NULL,
  `trans_type` smallint(6) NOT NULL default '30',
  `version` tinyint(1) unsigned NOT NULL default '0',
  `type` tinyint(1) NOT NULL default '0',
  `debtor_no` int(11) NOT NULL default '0',
  `branch_code` int(11) NOT NULL default '0',
  `reference` varchar(100) NOT NULL default '',
  `customer_ref` tinytext NOT NULL,
  `comments` tinytext,
  `ord_date` date NOT NULL default '0000-00-00',
  `order_type` int(11) NOT NULL default '0',
  `ship_via` int(11) NOT NULL default '0',
  `delivery_address` tinytext NOT NULL,
  `contact_phone` varchar(30) default NULL,
  `contact_email` varchar(100) default NULL,
  `deliver_to` tinytext NOT NULL,
  `freight_cost` double NOT NULL default '0',
  `from_stk_loc` varchar(5) NOT NULL default '',
  `delivery_date` date NOT NULL default '0000-00-00',
  `is_consign` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`trans_type`,`order_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_sales_orders`
--

/*!40000 ALTER TABLE `0_sales_orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_sales_orders` ENABLE KEYS */;


--
-- Definition of table `0_sales_pos`
--

DROP TABLE IF EXISTS `0_sales_pos`;
CREATE TABLE `0_sales_pos` (
  `id` smallint(6) unsigned NOT NULL auto_increment,
  `pos_name` varchar(30) NOT NULL,
  `cash_sale` tinyint(1) NOT NULL,
  `credit_sale` tinyint(1) NOT NULL,
  `pos_location` varchar(5) NOT NULL,
  `pos_account` smallint(6) unsigned NOT NULL,
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `pos_name` (`pos_name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_sales_pos`
--

/*!40000 ALTER TABLE `0_sales_pos` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_sales_pos` ENABLE KEYS */;


--
-- Definition of table `0_sales_types`
--

DROP TABLE IF EXISTS `0_sales_types`;
CREATE TABLE `0_sales_types` (
  `id` int(11) NOT NULL auto_increment,
  `sales_type` char(50) NOT NULL default '',
  `tax_included` int(1) NOT NULL default '0',
  `factor` double NOT NULL default '1',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `sales_type` (`sales_type`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_sales_types`
--

/*!40000 ALTER TABLE `0_sales_types` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_sales_types` ENABLE KEYS */;


--
-- Definition of table `0_salesman`
--

DROP TABLE IF EXISTS `0_salesman`;
CREATE TABLE `0_salesman` (
  `salesman_code` int(11) NOT NULL auto_increment,
  `salesman_name` char(60) NOT NULL default '',
  `salesman_phone` char(30) NOT NULL default '',
  `salesman_fax` char(30) NOT NULL default '',
  `salesman_email` varchar(100) NOT NULL default '',
  `provision` double NOT NULL default '0',
  `break_pt` double NOT NULL default '0',
  `provision2` double NOT NULL default '0',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`salesman_code`),
  UNIQUE KEY `salesman_name` (`salesman_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_salesman`
--

/*!40000 ALTER TABLE `0_salesman` DISABLE KEYS */;
INSERT INTO `0_salesman` (`salesman_code`,`salesman_name`,`salesman_phone`,`salesman_fax`,`salesman_email`,`provision`,`break_pt`,`provision2`,`inactive`) VALUES 
 (1,'Sales Person','','','',5,1000,4,0);
/*!40000 ALTER TABLE `0_salesman` ENABLE KEYS */;


--
-- Definition of table `0_security_roles`
--

DROP TABLE IF EXISTS `0_security_roles`;
CREATE TABLE `0_security_roles` (
  `id` int(11) NOT NULL auto_increment,
  `role` varchar(30) NOT NULL,
  `description` varchar(50) default NULL,
  `sections` text,
  `areas` text,
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `role` (`role`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_security_roles`
--

/*!40000 ALTER TABLE `0_security_roles` DISABLE KEYS */;
INSERT INTO `0_security_roles` (`id`,`role`,`description`,`sections`,`areas`,`inactive`) VALUES 
 (1,'Inquiries','Inquiries','768;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15872;16128','257;258;259;260;513;514;515;516;517;518;519;520;521;522;523;524;525;773;774;2822;3073;3075;3076;3077;3329;3330;3331;3332;3333;3334;3335;5377;5633;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8450;8451;10497;10753;11009;11010;11012;13313;13315;15617;15618;15619;15620;15621;15622;15623;15624;15625;15626;15873;15882;16129;16130;16131;16132',0),
 (2,'System Administrator','System Administrator','256;512;768;852736;2816;3072;3328;5376;5632;857600;5888;7936;8192;8448;10496;10752;11008;13056;13312;15616;15872;16128','257;258;259;260;513;514;515;516;517;518;519;520;521;522;523;524;525;769;770;771;772;773;774;852836;2817;2818;2819;2820;2821;2822;2823;3073;3074;3082;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5635;5636;5637;5641;5638;5639;5640;857701;5889;5890;5891;7937;7938;7939;7940;8193;8194;8195;8196;8197;8449;8450;8451;10497;10753;10754;10755;10756;10757;11009;11010;11011;11012;13057;13313;13314;13315;15617;15618;15619;15620;15621;15622;15623;15624;15628;15625;15626;15627;15873;15874;15875;15876;15877;15878;15879;15880;15883;15881;15882;16129;16130;16131;16132',0),
 (3,'Salesman','Salesman','768;3072;5632;8192;15872','773;774;3073;3075;3081;5633;8194;15873',0);
INSERT INTO `0_security_roles` (`id`,`role`,`description`,`sections`,`areas`,`inactive`) VALUES 
 (4,'Stock Manager','Stock Manager','2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15872;16128','2818;2822;3073;3076;3077;3329;3330;3330;3330;3331;3331;3332;3333;3334;3335;5633;5640;5889;5890;5891;8193;8194;8450;8451;10753;11009;11010;11012;13313;13315;15882;16129;16130;16131;16132',0),
 (5,'Production Manager','Production Manager','512;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128','521;523;524;2818;2819;2820;2821;2822;2823;3073;3074;3076;3077;3078;3079;3080;3081;3329;3330;3330;3330;3331;3331;3332;3333;3334;3335;5633;5640;5640;5889;5890;5891;8193;8194;8196;8197;8450;8451;10753;10755;11009;11010;11012;13313;13315;15617;15619;15620;15621;15624;15624;15876;15877;15880;15882;16129;16130;16131;16132',0),
 (6,'Purchase Officer','Purchase Officer','512;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128','521;523;524;2818;2819;2820;2821;2822;2823;3073;3074;3076;3077;3078;3079;3080;3081;3329;3330;3330;3330;3331;3331;3332;3333;3334;3335;5377;5633;5635;5640;5640;5889;5890;5891;8193;8194;8196;8197;8449;8450;8451;10753;10755;11009;11010;11012;13313;13315;15617;15619;15620;15621;15624;15624;15876;15877;15880;15882;16129;16130;16131;16132',0),
 (7,'AR Officer','AR Officer','512;768;2816;3072;3328;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128','521;523;524;771;773;774;2818;2819;2820;2821;2822;2823;3073;3073;3074;3075;3076;3077;3078;3079;3080;3081;3081;3329;3330;3330;3330;3331;3331;3332;3333;3334;3335;5633;5633;5634;5637;5638;5639;5640;5640;5889;5890;5891;8193;8194;8194;8196;8197;8450;8451;10753;10755;11009;11010;11012;13313;13315;15617;15619;15620;15621;15624;15624;15873;15876;15877;15878;15880;15882;16129;16130;16131;16132',0);
INSERT INTO `0_security_roles` (`id`,`role`,`description`,`sections`,`areas`,`inactive`) VALUES 
 (8,'AP Officer','AP Officer','512;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128','257;258;259;260;521;523;524;769;770;771;772;773;774;2818;2819;2820;2821;2822;2823;3073;3074;3082;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5635;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8196;8197;8449;8450;8451;10497;10753;10755;11009;11010;11012;13057;13313;13315;15617;15619;15620;15621;15624;15876;15877;15880;15882;16129;16130;16131;16132',0),
 (9,'Accountant','New Accountant','512;768;2816;3072;3328;5376;5632;5888;8192;8448;10752;11008;13312;15616;15872;16128','257;258;259;260;521;523;524;771;772;773;774;2818;2819;2820;2821;2822;2823;3073;3074;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5635;5637;5638;5639;5640;5889;5890;5891;7937;7938;7939;7940;8193;8194;8196;8197;8449;8450;8451;10497;10753;10755;11009;11010;11012;13313;13315;15617;15618;15619;15620;15621;15624;15873;15876;15877;15878;15880;15882;16129;16130;16131;16132',0),
 (10,'Sub Admin','Sub Admin','512;768;2816;3072;3328;5376;5632;5888;8192;8448;13312;15616;15872;16128','257;258;259;260;7937;7938;7939;7940;10497;10753;10754;10755;10756;10757;11009;11010;11011;11012;13057;521;523;524;771;772;773;774;2818;2819;2820;2821;2822;2823;3073;3074;3082;3075;3076;3077;3078;3079;3080;3081;3329;3330;3331;3332;3333;3334;3335;5377;5633;5634;5635;5637;5638;5639;5640;5889;5890;5891;8193;8194;8196;8197;8449;8450;8451;13313;13315;15617;15619;15620;15621;15624;15873;15874;15876;15877;15878;15879;15880;15882;16129;16130;16131;16132',0);
/*!40000 ALTER TABLE `0_security_roles` ENABLE KEYS */;


--
-- Definition of table `0_shippers`
--

DROP TABLE IF EXISTS `0_shippers`;
CREATE TABLE `0_shippers` (
  `shipper_id` int(11) NOT NULL auto_increment,
  `shipper_name` varchar(60) NOT NULL default '',
  `phone` varchar(30) NOT NULL default '',
  `phone2` varchar(30) NOT NULL default '',
  `contact` tinytext NOT NULL,
  `address` tinytext NOT NULL,
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`shipper_id`),
  UNIQUE KEY `name` (`shipper_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_shippers`
--

/*!40000 ALTER TABLE `0_shippers` DISABLE KEYS */;
INSERT INTO `0_shippers` (`shipper_id`,`shipper_name`,`phone`,`phone2`,`contact`,`address`,`inactive`) VALUES 
 (1,'Default','','','','',0);
/*!40000 ALTER TABLE `0_shippers` ENABLE KEYS */;


--
-- Definition of table `0_sql_trail`
--

DROP TABLE IF EXISTS `0_sql_trail`;
CREATE TABLE `0_sql_trail` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `sql` text NOT NULL,
  `result` tinyint(1) NOT NULL,
  `msg` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_sql_trail`
--

/*!40000 ALTER TABLE `0_sql_trail` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_sql_trail` ENABLE KEYS */;


--
-- Definition of table `0_stock_category`
--

DROP TABLE IF EXISTS `0_stock_category`;
CREATE TABLE `0_stock_category` (
  `category_id` int(11) NOT NULL auto_increment,
  `description` varchar(60) NOT NULL default '',
  `dflt_tax_type` int(11) NOT NULL default '1',
  `dflt_units` varchar(20) NOT NULL default 'each',
  `dflt_mb_flag` char(1) NOT NULL default 'B',
  `dflt_sales_act` varchar(11) NOT NULL default '',
  `dflt_cogs_act` varchar(11) NOT NULL default '',
  `dflt_inventory_act` varchar(11) NOT NULL default '',
  `dflt_adjustment_act` varchar(11) NOT NULL default '',
  `dflt_assembly_act` varchar(11) NOT NULL default '',
  `dflt_dim1` int(11) default NULL,
  `dflt_dim2` int(11) default NULL,
  `inactive` tinyint(1) NOT NULL default '0',
  `dflt_no_sale` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`category_id`),
  UNIQUE KEY `description` (`description`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_stock_category`
--

/*!40000 ALTER TABLE `0_stock_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_stock_category` ENABLE KEYS */;


--
-- Definition of table `0_stock_master`
--

DROP TABLE IF EXISTS `0_stock_master`;
CREATE TABLE `0_stock_master` (
  `stock_id` varchar(20) NOT NULL default '',
  `category_id` int(11) NOT NULL default '0',
  `tax_type_id` int(11) NOT NULL default '0',
  `description` varchar(200) NOT NULL default '',
  `long_description` tinytext NOT NULL,
  `units` varchar(20) NOT NULL default 'each',
  `mb_flag` char(1) NOT NULL default 'B',
  `sales_account` varchar(11) NOT NULL default '',
  `cogs_account` varchar(11) NOT NULL default '',
  `inventory_account` varchar(11) NOT NULL default '',
  `adjustment_account` varchar(11) NOT NULL default '',
  `assembly_account` varchar(11) NOT NULL default '',
  `dimension_id` int(11) default NULL,
  `dimension2_id` int(11) default NULL,
  `actual_cost` double NOT NULL default '0',
  `last_cost` double NOT NULL default '0',
  `material_cost` double NOT NULL default '0',
  `labour_cost` double NOT NULL default '0',
  `overhead_cost` double NOT NULL default '0',
  `inactive` tinyint(1) NOT NULL default '0',
  `no_sale` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`stock_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_stock_master`
--

/*!40000 ALTER TABLE `0_stock_master` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_stock_master` ENABLE KEYS */;


--
-- Definition of table `0_stock_moves`
--

DROP TABLE IF EXISTS `0_stock_moves`;
CREATE TABLE `0_stock_moves` (
  `trans_id` int(11) NOT NULL auto_increment,
  `trans_no` int(11) NOT NULL default '0',
  `stock_id` char(20) NOT NULL default '',
  `type` smallint(6) NOT NULL default '0',
  `loc_code` char(5) NOT NULL default '',
  `tran_date` date NOT NULL default '0000-00-00',
  `person_id` int(11) default NULL,
  `price` double NOT NULL default '0',
  `reference` char(40) NOT NULL default '',
  `qty` double NOT NULL default '1',
  `discount_percent` double NOT NULL default '0',
  `standard_cost` double NOT NULL default '0',
  `visible` tinyint(1) NOT NULL default '1',
  PRIMARY KEY  (`trans_id`),
  KEY `type` (`type`,`trans_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_stock_moves`
--

/*!40000 ALTER TABLE `0_stock_moves` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_stock_moves` ENABLE KEYS */;


--
-- Definition of table `0_supp_allocations`
--

DROP TABLE IF EXISTS `0_supp_allocations`;
CREATE TABLE `0_supp_allocations` (
  `id` int(11) NOT NULL auto_increment,
  `amt` double unsigned default NULL,
  `date_alloc` date NOT NULL default '0000-00-00',
  `trans_no_from` int(11) default NULL,
  `trans_type_from` int(11) default NULL,
  `trans_no_to` int(11) default NULL,
  `trans_type_to` int(11) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_supp_allocations`
--

/*!40000 ALTER TABLE `0_supp_allocations` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_supp_allocations` ENABLE KEYS */;


--
-- Definition of table `0_supp_invoice_items`
--

DROP TABLE IF EXISTS `0_supp_invoice_items`;
CREATE TABLE `0_supp_invoice_items` (
  `id` int(11) NOT NULL auto_increment,
  `supp_trans_no` int(11) default NULL,
  `supp_trans_type` int(11) default NULL,
  `gl_code` varchar(11) NOT NULL default '0',
  `grn_item_id` int(11) default NULL,
  `po_detail_item_id` int(11) default NULL,
  `stock_id` varchar(20) NOT NULL default '',
  `description` tinytext,
  `quantity` double NOT NULL default '0',
  `unit_price` double NOT NULL default '0',
  `unit_tax` double NOT NULL default '0',
  `memo_` tinytext,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_supp_invoice_items`
--

/*!40000 ALTER TABLE `0_supp_invoice_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_supp_invoice_items` ENABLE KEYS */;


--
-- Definition of table `0_supp_trans`
--

DROP TABLE IF EXISTS `0_supp_trans`;
CREATE TABLE `0_supp_trans` (
  `trans_no` int(11) unsigned NOT NULL default '0',
  `type` smallint(6) unsigned NOT NULL default '0',
  `supplier_id` int(11) unsigned default NULL,
  `reference` tinytext NOT NULL,
  `supp_reference` varchar(60) NOT NULL default '',
  `tran_date` date NOT NULL default '0000-00-00',
  `due_date` date NOT NULL default '0000-00-00',
  `ov_amount` double NOT NULL default '0',
  `ov_discount` double NOT NULL default '0',
  `ov_gst` double NOT NULL default '0',
  `rate` double NOT NULL default '1',
  `alloc` double NOT NULL default '0',
  PRIMARY KEY  (`trans_no`,`type`),
  KEY `supplier_id` (`supplier_id`),
  KEY `SupplierID_2` (`supplier_id`,`supp_reference`),
  KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_supp_trans`
--

/*!40000 ALTER TABLE `0_supp_trans` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_supp_trans` ENABLE KEYS */;


--
-- Definition of table `0_suppliers`
--

DROP TABLE IF EXISTS `0_suppliers`;
CREATE TABLE `0_suppliers` (
  `supplier_id` int(11) NOT NULL auto_increment,
  `supp_name` varchar(60) NOT NULL default '',
  `supp_ref` varchar(30) NOT NULL default '',
  `address` tinytext NOT NULL,
  `supp_address` tinytext NOT NULL,
  `phone` varchar(30) NOT NULL default '',
  `phone2` varchar(30) NOT NULL default '',
  `fax` varchar(30) NOT NULL default '',
  `gst_no` varchar(25) NOT NULL default '',
  `contact` varchar(60) NOT NULL default '',
  `supp_account_no` varchar(40) NOT NULL default '',
  `email` varchar(100) NOT NULL default '',
  `website` varchar(100) NOT NULL default '',
  `bank_account` varchar(60) NOT NULL default '',
  `curr_code` char(3) default NULL,
  `payment_terms` int(11) default NULL,
  `dimension_id` int(11) default '0',
  `dimension2_id` int(11) default '0',
  `tax_group_id` int(11) default NULL,
  `credit_limit` double NOT NULL default '0',
  `purchase_account` varchar(11) default NULL,
  `payable_account` varchar(11) default NULL,
  `payment_discount_account` varchar(11) default NULL,
  `notes` tinytext NOT NULL,
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`supplier_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_suppliers`
--

/*!40000 ALTER TABLE `0_suppliers` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_suppliers` ENABLE KEYS */;


--
-- Definition of table `0_sys_types`
--

DROP TABLE IF EXISTS `0_sys_types`;
CREATE TABLE `0_sys_types` (
  `type_id` smallint(6) NOT NULL default '0',
  `type_no` int(11) NOT NULL default '1',
  `next_reference` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_sys_types`
--

/*!40000 ALTER TABLE `0_sys_types` DISABLE KEYS */;
INSERT INTO `0_sys_types` (`type_id`,`type_no`,`next_reference`) VALUES 
 (0,17,'7'),
 (1,7,'3'),
 (2,4,'2'),
 (4,3,'1'),
 (10,16,'7'),
 (11,2,'1'),
 (12,6,'4'),
 (13,1,'17'),
 (16,2,'6'),
 (17,2,'4'),
 (18,1,'19'),
 (20,6,'12'),
 (21,1,'1'),
 (22,3,'13'),
 (25,1,'19'),
 (26,1,'8'),
 (28,1,'10'),
 (29,1,'9'),
 (30,0,'16'),
 (32,0,'4'),
 (35,1,'1'),
 (40,1,'11');
/*!40000 ALTER TABLE `0_sys_types` ENABLE KEYS */;


--
-- Definition of table `0_tag_associations`
--

DROP TABLE IF EXISTS `0_tag_associations`;
CREATE TABLE `0_tag_associations` (
  `record_id` varchar(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  UNIQUE KEY `record_id` (`record_id`,`tag_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_tag_associations`
--

/*!40000 ALTER TABLE `0_tag_associations` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_tag_associations` ENABLE KEYS */;


--
-- Definition of table `0_tags`
--

DROP TABLE IF EXISTS `0_tags`;
CREATE TABLE `0_tags` (
  `id` int(11) NOT NULL auto_increment,
  `type` smallint(6) NOT NULL,
  `name` varchar(30) NOT NULL,
  `description` varchar(60) default NULL,
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `type` (`type`,`name`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_tags`
--

/*!40000 ALTER TABLE `0_tags` DISABLE KEYS */;
INSERT INTO `0_tags` (`id`,`type`,`name`,`description`,`inactive`) VALUES 
 (1,1,'test','test',0);
/*!40000 ALTER TABLE `0_tags` ENABLE KEYS */;


--
-- Definition of table `0_tax_group_items`
--

DROP TABLE IF EXISTS `0_tax_group_items`;
CREATE TABLE `0_tax_group_items` (
  `tax_group_id` int(11) NOT NULL default '0',
  `tax_type_id` int(11) NOT NULL default '0',
  `rate` double NOT NULL default '0',
  PRIMARY KEY  (`tax_group_id`,`tax_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_tax_group_items`
--

/*!40000 ALTER TABLE `0_tax_group_items` DISABLE KEYS */;
INSERT INTO `0_tax_group_items` (`tax_group_id`,`tax_type_id`,`rate`) VALUES 
 (1,1,5),
 (3,1,12),
 (3,2,2);
/*!40000 ALTER TABLE `0_tax_group_items` ENABLE KEYS */;


--
-- Definition of table `0_tax_groups`
--

DROP TABLE IF EXISTS `0_tax_groups`;
CREATE TABLE `0_tax_groups` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(60) NOT NULL default '',
  `tax_shipping` tinyint(1) NOT NULL default '0',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_tax_groups`
--

/*!40000 ALTER TABLE `0_tax_groups` DISABLE KEYS */;
INSERT INTO `0_tax_groups` (`id`,`name`,`tax_shipping`,`inactive`) VALUES 
 (1,'Tax',0,0),
 (2,'Tax Exempt',0,0),
 (3,'group 1',0,0);
/*!40000 ALTER TABLE `0_tax_groups` ENABLE KEYS */;


--
-- Definition of table `0_tax_types`
--

DROP TABLE IF EXISTS `0_tax_types`;
CREATE TABLE `0_tax_types` (
  `id` int(11) NOT NULL auto_increment,
  `rate` double NOT NULL default '0',
  `sales_gl_code` varchar(11) NOT NULL default '',
  `purchasing_gl_code` varchar(11) NOT NULL default '',
  `name` varchar(60) NOT NULL default '',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_tax_types`
--

/*!40000 ALTER TABLE `0_tax_types` DISABLE KEYS */;
INSERT INTO `0_tax_types` (`id`,`rate`,`sales_gl_code`,`purchasing_gl_code`,`name`,`inactive`) VALUES 
 (1,12,'2150','2145','VAT',0),
 (2,-2,'1060','2500','Expanded Withholding Tax - Services',0),
 (3,-1,'1065','2501','Expanded Withholding Tax - Goods',0);
/*!40000 ALTER TABLE `0_tax_types` ENABLE KEYS */;


--
-- Definition of table `0_trans_tax_details`
--

DROP TABLE IF EXISTS `0_trans_tax_details`;
CREATE TABLE `0_trans_tax_details` (
  `id` int(11) NOT NULL auto_increment,
  `trans_type` smallint(6) default NULL,
  `trans_no` int(11) default NULL,
  `tran_date` date NOT NULL,
  `tax_type_id` int(11) NOT NULL default '0',
  `rate` double NOT NULL default '0',
  `ex_rate` double NOT NULL default '1',
  `included_in_price` tinyint(1) NOT NULL default '0',
  `net_amount` double NOT NULL default '0',
  `amount` double NOT NULL default '0',
  `memo` tinytext,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_trans_tax_details`
--

/*!40000 ALTER TABLE `0_trans_tax_details` DISABLE KEYS */;
INSERT INTO `0_trans_tax_details` (`id`,`trans_type`,`trans_no`,`tran_date`,`tax_type_id`,`rate`,`ex_rate`,`included_in_price`,`net_amount`,`amount`,`memo`) VALUES 
 (1,13,1,'2010-12-31',1,12,1,1,47321.4286,5678.571432,'1'),
 (2,10,1,'2010-12-31',1,12,1,1,47321.4286,5678.571432,'1'),
 (3,13,2,'2010-12-31',1,12,1,1,339285.7143,40714.285716,'auto'),
 (4,10,2,'2010-12-31',1,12,1,1,339285.7143,40714.285716,'2'),
 (5,13,3,'2011-03-14',1,12,1,1,160714.2857,19285.714284,'2'),
 (6,13,4,'2011-03-14',1,12,1,1,160714.2857,19285.714284,'3'),
 (7,20,1,'2011-03-24',1,12,1,0,5100000,612000,'7890'),
 (8,20,2,'2011-03-26',1,12,1,0,350000,42000,'9877765'),
 (9,13,5,'2011-03-26',1,12,1,1,48214.2857,5785.714284,'4'),
 (10,10,3,'2011-03-26',1,12,1,1,48214.2857,5785.714284,'3'),
 (11,13,6,'2011-04-12',1,12,1,1,404910.7143,48589.285716,'5'),
 (12,10,4,'2011-04-12',1,12,1,1,404910.7143,48589.285716,'4'),
 (13,13,7,'2011-04-12',1,12,1,1,593750,71250,'6'),
 (14,13,8,'2011-05-06',1,12,1,1,16071.4286,1928.571432,'7'),
 (15,13,9,'2011-05-06',1,12,1,1,32142.8571,3857.142852,'8');
INSERT INTO `0_trans_tax_details` (`id`,`trans_type`,`trans_no`,`tran_date`,`tax_type_id`,`rate`,`ex_rate`,`included_in_price`,`net_amount`,`amount`,`memo`) VALUES 
 (16,10,5,'2011-05-06',1,12,1,1,16071.4286,1928.571432,'5'),
 (17,20,3,'2011-05-19',1,12,1,0,2450000,294000,'98765'),
 (18,13,10,'2011-05-19',1,12,1,1,31250,3750,'9'),
 (19,10,6,'2011-05-19',1,12,1,1,31250,3750,'6'),
 (20,0,1,'2011-05-25',3,1,1,0,-5000000,-50000,''),
 (21,20,4,'2011-06-04',1,12,1,0,1000000,120000,'123456'),
 (22,13,11,'2011-06-28',1,12,1,1,1607142.8571,192857.142852,'10'),
 (23,20,5,'2011-06-28',1,12,1,0,1000000,120000,'12345'),
 (24,20,6,'2011-07-06',1,12,1,0,1000000,120000,'7123456'),
 (25,13,12,'2011-07-07',1,12,1,1,16071.4286,1928.571432,'11'),
 (26,13,13,'2011-07-07',1,12,1,1,32142.8571,3857.142852,'12'),
 (27,13,14,'2011-07-07',1,12,1,1,48214.2857,5785.714284,'13'),
 (28,20,7,'2011-07-07',1,12,1,0,25000,3000,'766544'),
 (29,13,15,'2011-07-12',1,12,1,1,22098.2143,2651.785716,'14'),
 (30,20,8,'2011-07-14',1,12,1,0,2625000,315000,'987666'),
 (31,20,9,'2011-07-22',1,12,1,0,500000,60000,'123456789');
INSERT INTO `0_trans_tax_details` (`id`,`trans_type`,`trans_no`,`tran_date`,`tax_type_id`,`rate`,`ex_rate`,`included_in_price`,`net_amount`,`amount`,`memo`) VALUES 
 (32,20,10,'2011-08-02',1,12,1,0,1000000,120000,'98760998'),
 (33,13,16,'2011-08-02',1,12,1,1,60357.1429,7242.857148,'15'),
 (34,13,17,'2011-08-12',1,12,1,1,1562500,187500,'16'),
 (35,20,11,'2011-08-20',1,12,1,0,1000000,120000,'5423345');
/*!40000 ALTER TABLE `0_trans_tax_details` ENABLE KEYS */;


--
-- Definition of table `0_useronline`
--

DROP TABLE IF EXISTS `0_useronline`;
CREATE TABLE `0_useronline` (
  `id` int(11) NOT NULL auto_increment,
  `timestamp` int(15) NOT NULL default '0',
  `ip` varchar(40) NOT NULL default '',
  `file` varchar(100) NOT NULL default '',
  PRIMARY KEY  (`id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_useronline`
--

/*!40000 ALTER TABLE `0_useronline` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_useronline` ENABLE KEYS */;


--
-- Definition of table `0_users`
--

DROP TABLE IF EXISTS `0_users`;
CREATE TABLE `0_users` (
  `id` smallint(6) NOT NULL auto_increment,
  `user_id` varchar(60) NOT NULL default '',
  `password` varchar(100) NOT NULL default '',
  `real_name` varchar(100) NOT NULL default '',
  `role_id` int(11) NOT NULL default '1',
  `phone` varchar(30) NOT NULL default '',
  `email` varchar(100) default NULL,
  `language` varchar(20) default NULL,
  `date_format` tinyint(1) NOT NULL default '0',
  `date_sep` tinyint(1) NOT NULL default '0',
  `tho_sep` tinyint(1) NOT NULL default '0',
  `dec_sep` tinyint(1) NOT NULL default '0',
  `theme` varchar(20) NOT NULL default 'default',
  `page_size` varchar(20) NOT NULL default 'A4',
  `prices_dec` smallint(6) NOT NULL default '2',
  `qty_dec` smallint(6) NOT NULL default '2',
  `rates_dec` smallint(6) NOT NULL default '4',
  `percent_dec` smallint(6) NOT NULL default '1',
  `show_gl` tinyint(1) NOT NULL default '1',
  `show_codes` tinyint(1) NOT NULL default '0',
  `show_hints` tinyint(1) NOT NULL default '0',
  `last_visit_date` datetime default NULL,
  `query_size` tinyint(1) default '10',
  `graphic_links` tinyint(1) default '1',
  `pos` smallint(6) default '1',
  `print_profile` varchar(30) NOT NULL default '1',
  `rep_popup` tinyint(1) default '1',
  `sticky_doc_date` tinyint(1) default '0',
  `startup_tab` varchar(20) NOT NULL default '',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_users`
--

/*!40000 ALTER TABLE `0_users` DISABLE KEYS */;
INSERT INTO `0_users` (`id`,`user_id`,`password`,`real_name`,`role_id`,`phone`,`email`,`language`,`date_format`,`date_sep`,`tho_sep`,`dec_sep`,`theme`,`page_size`,`prices_dec`,`qty_dec`,`rates_dec`,`percent_dec`,`show_gl`,`show_codes`,`show_hints`,`last_visit_date`,`query_size`,`graphic_links`,`pos`,`print_profile`,`rep_popup`,`sticky_doc_date`,`startup_tab`,`inactive`) VALUES 
 (1,'admin','5f4dcc3b5aa765d61d8327deb882cf99','Administrator',2,'','adm@adm.com','en_GB',0,0,0,0,'elegant226','Letter',2,2,4,1,1,0,0,'2011-08-28 01:55:22',10,1,1,'',1,0,'orders',0),
 (2,'prodriguez','5f4dcc3b5aa765d61d8327deb882cf99','Policarpio Rodriguez',10,'','','en_GB',0,0,0,0,'elegant226','Letter',2,2,4,1,1,0,0,'2011-05-21 06:05:14',10,1,1,'',1,0,'orders',0);
/*!40000 ALTER TABLE `0_users` ENABLE KEYS */;


--
-- Definition of table `0_voided`
--

DROP TABLE IF EXISTS `0_voided`;
CREATE TABLE `0_voided` (
  `type` int(11) NOT NULL default '0',
  `id` int(11) NOT NULL default '0',
  `date_` date NOT NULL default '0000-00-00',
  `memo_` tinytext NOT NULL,
  UNIQUE KEY `id` (`type`,`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_voided`
--

/*!40000 ALTER TABLE `0_voided` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_voided` ENABLE KEYS */;


--
-- Definition of table `0_wo_issue_items`
--

DROP TABLE IF EXISTS `0_wo_issue_items`;
CREATE TABLE `0_wo_issue_items` (
  `id` int(11) NOT NULL auto_increment,
  `stock_id` varchar(40) default NULL,
  `issue_id` int(11) default NULL,
  `qty_issued` double default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_wo_issue_items`
--

/*!40000 ALTER TABLE `0_wo_issue_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_wo_issue_items` ENABLE KEYS */;


--
-- Definition of table `0_wo_issues`
--

DROP TABLE IF EXISTS `0_wo_issues`;
CREATE TABLE `0_wo_issues` (
  `issue_no` int(11) NOT NULL auto_increment,
  `workorder_id` int(11) NOT NULL default '0',
  `reference` varchar(100) default NULL,
  `issue_date` date default NULL,
  `loc_code` varchar(5) default NULL,
  `workcentre_id` int(11) default NULL,
  PRIMARY KEY  (`issue_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_wo_issues`
--

/*!40000 ALTER TABLE `0_wo_issues` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_wo_issues` ENABLE KEYS */;


--
-- Definition of table `0_wo_manufacture`
--

DROP TABLE IF EXISTS `0_wo_manufacture`;
CREATE TABLE `0_wo_manufacture` (
  `id` int(11) NOT NULL auto_increment,
  `reference` varchar(100) default NULL,
  `workorder_id` int(11) NOT NULL default '0',
  `quantity` double NOT NULL default '0',
  `date_` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_wo_manufacture`
--

/*!40000 ALTER TABLE `0_wo_manufacture` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_wo_manufacture` ENABLE KEYS */;


--
-- Definition of table `0_wo_requirements`
--

DROP TABLE IF EXISTS `0_wo_requirements`;
CREATE TABLE `0_wo_requirements` (
  `id` int(11) NOT NULL auto_increment,
  `workorder_id` int(11) NOT NULL default '0',
  `stock_id` char(20) NOT NULL default '',
  `workcentre` int(11) NOT NULL default '0',
  `units_req` double NOT NULL default '1',
  `std_cost` double NOT NULL default '0',
  `loc_code` char(5) NOT NULL default '',
  `units_issued` double NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_wo_requirements`
--

/*!40000 ALTER TABLE `0_wo_requirements` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_wo_requirements` ENABLE KEYS */;


--
-- Definition of table `0_workcentres`
--

DROP TABLE IF EXISTS `0_workcentres`;
CREATE TABLE `0_workcentres` (
  `id` int(11) NOT NULL auto_increment,
  `name` char(40) NOT NULL default '',
  `description` char(50) NOT NULL default '',
  `inactive` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_workcentres`
--

/*!40000 ALTER TABLE `0_workcentres` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_workcentres` ENABLE KEYS */;


--
-- Definition of table `0_workorders`
--

DROP TABLE IF EXISTS `0_workorders`;
CREATE TABLE `0_workorders` (
  `id` int(11) NOT NULL auto_increment,
  `wo_ref` varchar(60) NOT NULL default '',
  `loc_code` varchar(5) NOT NULL default '',
  `units_reqd` double NOT NULL default '1',
  `stock_id` varchar(20) NOT NULL default '',
  `date_` date NOT NULL default '0000-00-00',
  `type` tinyint(4) NOT NULL default '0',
  `required_by` date NOT NULL default '0000-00-00',
  `released_date` date NOT NULL default '0000-00-00',
  `units_issued` double NOT NULL default '0',
  `closed` tinyint(1) NOT NULL default '0',
  `released` tinyint(1) NOT NULL default '0',
  `additional_costs` double NOT NULL default '0',
  PRIMARY KEY  (`id`),
  UNIQUE KEY `wo_ref` (`wo_ref`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `0_workorders`
--

/*!40000 ALTER TABLE `0_workorders` DISABLE KEYS */;
/*!40000 ALTER TABLE `0_workorders` ENABLE KEYS */;


--
-- Definition of table `xx_reports`
--

DROP TABLE IF EXISTS `xx_reports`;
CREATE TABLE `xx_reports` (
  `id` varchar(10) NOT NULL default '',
  `typ` varchar(10) NOT NULL default '',
  `attrib` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `xx_reports`
--

/*!40000 ALTER TABLE `xx_reports` DISABLE KEYS */;
INSERT INTO `xx_reports` (`id`,`typ`,`attrib`) VALUES 
 ('3','item','String|PF|Helvetica|8|10l|300|10|printed at||||||||'),
 ('3','item','DB|DE|Helvetica|12|6l|100|0|typ||||'),
 ('3','item','Term|PF|Helvetica|8|20l|460|10|PageNo||||||||'),
 ('6','item','Line|PH|1|1|1|1|1'),
 ('3','item','String|PH|Helvetica|14|40l|200|0|Stored Report|||1|40'),
 ('3','item','DB|DE|Helvetica|12|40l|150|0|attrib||||||||'),
 ('3','item','String|GH|Helvetica-Bold|14|10l|200|0|New Group||||||||'),
 ('3','item','DB|GH|Helvetica-Bold|12|6l|285|0|id||||||||'),
 ('3','info','Reports|2007-10-01|Bauer|List of all stored Reports|portrait|a4|classgrid'),
 ('3','group','id|newpage'),
 ('F2','funct','atime|2007-09-21|Bauer|Actual Time|function atime() {return date(\"h:i a\");}'),
 ('F1','funct','RepDate|2007-09-26|Joe|Date|function RepDate() {return today().\" \".now();}'),
 ('3','item','Term|PF|Helvetica|8|20l|335|10|RepDate||||||||'),
 ('F4','funct','oldgroup|2007-09-26|bauer|Value of old group|function oldgroup($it){return $it->group_old;}'),
 ('F5','funct','Newgroup|2007-09-26|Hunt|New group value|function newgroup($it){return $it->group_new;}');
INSERT INTO `xx_reports` (`id`,`typ`,`attrib`) VALUES 
 ('B1','block','block1|2007-09-27|Bauer|Block 1'),
 ('6','item','Line|GF|1|1|1|400|1'),
 ('2','item','DB|DE|Helvetica-Bold|12||100|600|attrib||||'),
 ('6','item','DB|DE|Helvetica|9|6l|50|0|account_code||||||||'),
 ('6','item','Term|GH|Helvetica|9|50l|50|0|Newgroup||||||||'),
 ('F6','funct','rec_count|2007-09-26|bauer|total number of records|function rec_count($it) {return $it->count;}'),
 ('6','item','String|RF|Helvetica|9|20l|50|0|Gran Total||||||||'),
 ('6','item','String|GF|Helvetica|9|20l|50|0|Sum:||||||||'),
 ('6','item','DB|DE|Helvetica|9|30l|300|0|name||||||||'),
 ('6','item','DB|DE|Helvetica|9|20l|400|0|class_name||||||||'),
 ('6','item','DB|DE|Helvetica|9|50l|100|0|account_name||||||||'),
 ('F7','funct','subcount|2007-09-26|bauer|total number of records of a group|function subcount($it) {return $it->subcount;}'),
 ('6','info','Accounts|2009-02-08|Hunt|Accounts List|portrait|a4|class'),
 ('6','select','select * from 0_chart_master,0_chart_types,0_chart_class where account_type=id and class_id=cid order by account_code'),
 ('2','info','single|2007-09-26|Bauer|Single page per Record|portrait|a4|single');
INSERT INTO `xx_reports` (`id`,`typ`,`attrib`) VALUES 
 ('2','select','select * from xx_reports'),
 ('2','group','|nopage'),
 ('6','item','Line|GH|1|1|1|1|1'),
 ('6','item','Line|RH|1|1|1|1|1'),
 ('6','item','Line|RF|1|1|1|1|1'),
 ('6','item','Term|RH|Helvetica-Bold|16|50l|50|0|RepTitle||||||||'),
 ('F8','funct','RepTitle|2007-09-26|Joe|Report Title|function RepTitle($it) \r\n{ return $it->long_name; }'),
 ('6','item','String|RH|Helvetica|8|30l|50|-12|Print Out Date:||||||||'),
 ('6','item','Term|RH|Helvetica|8|30l|120|-12|RepDate||||||||'),
 ('6','item','String|RH|Helvetica|8|30l|50|-24|Fiscal Year:||||||||'),
 ('6','item','String|RH|Helvetica|8|30l|50|-36|Select:||||||||'),
 ('6','item','Term|GF|Helvetica|9|4r|200|0|subcount||||||||'),
 ('6','item','Term|RF|Helvetica|9|4r|200|0|rec_count||||||||'),
 ('6','item','String|PH|Helvetica-Bold|9|20l|50|0|Account||||||||'),
 ('6','item','String|PH|Helvetica-Bold|9|50l|100|0|Account Name||||||||'),
 ('6','item','String|PH|Helvetica-Bold|9|20l|300|0|Type||||||||'),
 ('6','item','String|PH|Helvetica-Bold|9|20l|400|0|Class||||||||');
INSERT INTO `xx_reports` (`id`,`typ`,`attrib`) VALUES 
 ('6','item','Term|RH|Helvetica|9|50l|400|0|Company||||||||'),
 ('6','item','Term|RH|Helvetica|9|50l|400|-12|Username||||||||'),
 ('6','item','Term|RH|Helvetica|9|50l|400|-36|PageNo||||||||'),
 ('F9','funct','PageNo|2007-09-26|Joe|Page Number|function PageNo($it){return \"Page   \".$it->pdf->numPages;}'),
 ('B1','item','String|PH|Helvetica|7|20l|100|0|Stringitem||||||||'),
 ('3','select','select * from xx_reports order by id'),
 ('3','item','DB|PH|Helvetica|14|6l|360|0|id|||1|6||||'),
 ('6','item','Term|RH|Helvetica|8|50l|400|-24|Host||||||||'),
 ('F13','funct','Host|2007-09-26|Hunt|Host name|function Host(){return $_SERVER[\'SERVER_NAME\'];}'),
 ('6','group','name|nopage'),
 ('6','item','Term|RH|Helvetica|8|50l|120|-24|FiscalYear||||||||'),
 ('F12','funct','FiscalYear|2007-09-26|Hunt|Get current Fiscal Year|function FiscalYear(){$y=get_current_fiscalyear();return sql2date($y[\'begin\']) . \" - \" . sql2date($y[\'end\']);}'),
 ('F11','funct','Username|2007-09-26|Hunt|Get Username|function Username(){return $_SESSION[\"wa_current_user\"]->name;}'),
 ('F10','funct','Company|2007-09-26|Hunt|Company Name|function Company(){ return get_company_pref(\'coy_name\'); }');
/*!40000 ALTER TABLE `xx_reports` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
