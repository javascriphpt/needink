CREATE TABLE `0_deposit_allocations` (
`id`  int(11) NOT NULL AUTO_INCREMENT ,
`amt`  double UNSIGNED NULL DEFAULT NULL ,
`date_alloc`  date NOT NULL DEFAULT '0000-00-00' ,
`trans_no_from`  int(11) NULL DEFAULT NULL ,
`trans_type_from`  int(11) NULL DEFAULT NULL ,
`trans_no_to`  int(11) NULL DEFAULT NULL ,
`trans_type_to`  int(11) NULL DEFAULT NULL ,
PRIMARY KEY (`id`)
)
ENGINE=InnoDB
DEFAULT CHARACTER SET=latin1 COLLATE=latin1_swedish_ci
ROW_FORMAT=Compact
;