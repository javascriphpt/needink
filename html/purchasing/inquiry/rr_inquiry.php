<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPTRANSVIEW';
$path_to_root="../..";
include($path_to_root . "/includes/db_pager.inc");
include($path_to_root . "/includes/session.inc");

include($path_to_root . "/purchasing/includes/purchasing_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Search Receiving Report"), false, false, "", $js);

if (isset($_GET['order_number']))
{
	$order_number = $_GET['order_number'];
}

//-----------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';

	$Ajax->addDisable(true, 'OrdersAfterDate', $disable);
	$Ajax->addDisable(true, 'OrdersToDate', $disable);
	$Ajax->addDisable(true, 'StockLocation', $disable);
	$Ajax->addDisable(true, '_SelectStockFromList_edit', $disable);

	if ($disable) {
		$Ajax->addFocus(true, 'order_number');
	} else
		$Ajax->addFocus(true, 'OrdersAfterDate');

	$Ajax->activate('orders_tbl');
}
//---------------------------------------------------------------------------------------------

start_form();

start_table("class='tablestyle_noborder'");
start_row();
ref_cells(_("#:"), 'order_number', '',null, '', true);

date_cells(_("from:"), 'OrdersAfterDate', '', null, -30);
date_cells(_("to:"), 'OrdersToDate');

if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	locations_list_cells_user(_("into locatio:"), 'StockLocation', null, false, true, $_SESSION["wa_current_user"]->default_location);
else
	locations_list_cells(_("into location:"), 'StockLocation', null, true);

submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');
end_row();
end_table();
//---------------------------------------------------------------------------------------------
if (isset($_POST['order_number']))
{
	$order_number = $_POST['order_number'];
}

//---------------------------------------------------------------------------------------------
function trans_view($trans)
{
	return get_trans_view_str(ST_SUPPRECEIVE,$trans["id"],$trans["reference"]);
}

function void_link($row)
{
	if (get_voided_entry(ST_SUPPRECEIVE, $row['id']) === false)
		return pager_link(_("Void"), "/sales/void.php?trans_no=". $row['id']."&type=". ST_SUPPRECEIVE, ICON_REMOVE);
}

//---------------------------------------------------------------------------------------------

$sql = "SELECT 
	grn.id, 	
	supplier.supp_name, 
	location.location_name,
	grn.delivery_date, 
	Sum(line.unit_price*line.quantity_ordered) AS OrderValue,
	grn.loc_code,
	grn.reference
	FROM ".TB_PREF."purch_orders as porder, "
		.TB_PREF."purch_order_details as line, "
		.TB_PREF."suppliers as supplier, "
		.TB_PREF."locations as location, "
		.TB_PREF."grn_batch as grn
	WHERE porder.order_no = line.order_no
	AND porder.is_approve = 1
	AND porder.supplier_id = supplier.supplier_id
	AND location.loc_code = porder.into_stock_location 
	AND porder.order_no = grn.purch_order_no ";
	
if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	$sql .= " AND grn.loc_code = ".db_escape($_SESSION["wa_current_user"]->default_location);

if (isset($order_number) && $order_number != "")
{
	$sql .= "AND grn.reference LIKE ".db_escape('%'. $order_number . '%');
}
else
{

	$data_after = date2sql($_POST['OrdersAfterDate']);
	$date_before = date2sql($_POST['OrdersToDate']);

	$sql .= " AND grn.delivery_date >= '$data_after'";
	$sql .= " AND grn.delivery_date <= '$date_before'";

} //end not order number selected

$sql .= " GROUP BY grn.id";

$cols = array(
		_("#") => array('fun'=>'trans_view', 'ord'=>''), 
		_("Supplier") => array('ord'=>''),
		_("Location"),
		_("Delivery Date") => array('name'=>'ord_date', 'type'=>'date', 'ord'=>'desc'),
		array('insert'=>true, 'fun'=>'void_link')
);

if (get_post('StockLocation') != $all_items) {
	$cols[_("Location")] = 'skip';
}
//---------------------------------------------------------------------------------------------------

$table =& new_db_pager('orders_tbl', $sql, $cols);

$table->width = "80%";

display_db_pager($table);

end_form();
end_page();
?>
