<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PENDINGPO';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include($path_to_root . "/includes/session.inc");

include($path_to_root . "/purchasing/includes/purchasing_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);
if ($use_date_picker)
	$js .= get_js_date_picker();
	
page(_($help_context = "Pending Purchase Order"), false, false, "", $js);

//----------------------------------------------------------------------------------------
//simple_page_mode(true);

if (isset($_GET['order_number']))
{
	$_POST['order_number'] = $_GET['order_number'];
}

//--------------------------------------------------------------------------------------------------

function changeOrder($trans_no,$action){
	global $Ajax;
	global $Refs;
	
	$date_today = date("Y-m-d");
	
	$sql = "UPDATE ".TB_PREF."purch_orders
			SET is_approve = ".db_escape($action).",
				date_approve = ".db_escape($date_today).",
				approving_officer = ".db_escape($_SESSION["wa_current_user"]->username)."
			WHERE order_no = ".db_escape($trans_no);
	db_query($sql, "could not update purch order");
		
	display_notification_centered(_("Processed"));
	
	$Ajax->activate('orders_tbl');
}

$checkaction=find_submit('approve');
if($checkaction!=-1)
changeOrder($checkaction,1);

$checkaction=find_submit('disapprove');
if($checkaction!=-1)
changeOrder($checkaction,2);	

//--------------------------------------------------------------------------------------------------


if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';

	$Ajax->addDisable(true, 'OrdersAfterDate', $disable);
	$Ajax->addDisable(true, 'OrdersToDate', $disable);

	if ($disable) {
		$Ajax->addFocus(true, 'order_number');
	} else
		$Ajax->addFocus(true, 'OrdersAfterDate');

	$Ajax->activate('orders_tbl');
}

start_form();

start_table("class='tablestyle_noborder'");
start_row();

ref_cells(_("PO #:"), 'order_number', '',null, '', true);

if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	locations_list_cells_user(_("Deliver from Location:"), 'StockLocation', null, false, true, $_SESSION["wa_current_user"]->default_location);
else
	locations_list_cells(_("Location:"), 'StockLocation', null, true);

date_cells(_("From:"), 'OrdersAfterDate', '', null, -30);
date_cells(_("To:"), 'OrdersToDate', '', null, 1);

submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');

end_row();

end_table(1);

end_form();

//--------------------------------------------------------------------------------------------------
//	Orders inquiry table
//
if (isset($_POST['order_number']) && ($_POST['order_number'] != ""))
{
	$order_number = $_POST['order_number'];
}

$date_after = date2sql($_POST['OrdersAfterDate']);
$date_before = date2sql($_POST['OrdersToDate']);

//figure out the sql required from the inputs available
$sql = "SELECT 
	porder.order_no, 
	porder.reference,
	supplier.supp_name, 
	location.location_name,
	porder.requisition_no, 
	porder.ord_date,
	supplier.curr_code,
	Sum(line.unit_price*line.quantity_ordered) AS OrderValue,
	Sum(line.delivery_date < '". date2sql(Today()) ."'
	AND (line.quantity_ordered > line.quantity_received)) As OverDue,
	porder.is_approve
	FROM "
		.TB_PREF."purch_orders as porder, "
		.TB_PREF."purch_order_details as line, "
		.TB_PREF."suppliers as supplier, "
		.TB_PREF."locations as location
	WHERE porder.order_no = line.order_no
	AND porder.is_approve != 1
	AND porder.supplier_id = supplier.supplier_id
	AND location.loc_code = porder.into_stock_location
	AND (line.quantity_ordered > line.quantity_received) ";
	
if (isset($_POST['StockLocation']) && $_POST['StockLocation'] != ALL_TEXT)
	$sql .= " AND porder.into_stock_location = ".db_escape($_POST['StockLocation']);

if (isset($order_number) && $order_number != "")
{
	$sql .= "AND porder.reference LIKE ".db_escape('%'. $order_number . '%');
}
else
{
	$sql .= "  AND porder.ord_date >= '$date_after'";
	$sql .= "  AND porder.ord_date <= '$date_before'";
} //end not order number selected

$sql .= " GROUP BY porder.order_no";

$result = db_query($sql, "could not query stock moves");

//--------------------------------------------------------------------------------------------------

start_form();

div_start('orders_tbl');
start_table($table_style);
$th = array(_("PO #"), _("Supplier"), _("Order Date"), _("Amount"), _("Status"), "");

table_header($th);

$j = 1;
$k = 0; //row colour counter

while ($myrow = db_fetch($result))
{
	alt_table_row_color($k);
	
	$trandate = sql2date($myrow["ord_date"]);
	
	label_cell(get_trans_view_str(ST_PURCHORDER, $myrow["order_no"], $myrow["reference"]));
	label_cell($myrow["supp_name"]);
	label_cell($trandate);
	amount_cell($myrow["OrderValue"]);
		
	if($myrow["is_approve"]==0)
		label_cell("For Approval");
	else if($myrow["is_approve"]==2)
		label_cell("Rejected");
	
	label_cell(pager_link( _("Edit"),$path_to_root . "/po_entry_items.php?ModifyOrderNumber=" . $myrow['order_no'], ICON_EDIT));
	
	end_row();
	
	$j++;
	if ($j == 11)
	{
		$j = 1;
		table_header($th);
	}
}

end_table(1);
div_end();

end_form();

end_page();

?>
