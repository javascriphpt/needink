<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPCHECKDETAILSINQ';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include($path_to_root . "/includes/session.inc");

include($path_to_root . "/purchasing/includes/purchasing_ui.inc");
include($path_to_root . "/reporting/includes/reporting.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Check Details Inquiry"), false, false, "", $js);

if (isset($_GET['supplier_id'])){
	$_POST['supplier_id'] = $_GET['supplier_id'];
}

//------------------------------------------------------------------------------------------------

start_form();

if (!isset($_POST['supplier_id']))
	$_POST['supplier_id'] = get_global_supplier();

start_table("class='tablestyle_noborder'");
start_row();

ref_cells(_("Check #:"), 'check_num', '',null, '', true);

supplier_list_cells(_("Select a supplier:"), 'supplier_id', null, true);

date_cells(_("From:"), 'TransAfterDate', '', null, -30);
date_cells(_("To:"), 'TransToDate', '', null, 1);

submit_cells('RefreshInquiry', _("Search"),'',_('Refresh Inquiry'), 'default');
end_row();
end_table();

set_global_supplier($_POST['supplier_id']);

//------------------------------------------------------------------------------------------------

function trans_view($trans)
{
	return get_trans_view_str($trans["type"], $trans["trans_no"], $trans["reference"]);
}

//------------------------------------------------------------------------------------------------
$date_after = date2sql($_POST['TransAfterDate']);
$date_to = date2sql($_POST['TransToDate']);

$sql = "SELECT
	".TB_PREF."supp_trans.trans_no,
	".TB_PREF."suppliers.supp_name,
	".TB_PREF."cheque_details.chk_number,
	".TB_PREF."cheque_details.chk_date,
	ABS(".TB_PREF."bank_trans.amount),
	".TB_PREF."supp_trans.type
	FROM
	".TB_PREF."cheque_details ,
	".TB_PREF."supp_trans ,
	".TB_PREF."suppliers ,
	".TB_PREF."bank_trans
	WHERE
	".TB_PREF."bank_trans.type = 22 AND ".TB_PREF."supp_trans.type = 22 AND
	".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
	".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type AND
	".TB_PREF."supp_trans.reference =  ".TB_PREF."bank_trans.ref AND
	".TB_PREF."supp_trans.supplier_id =  ".TB_PREF."suppliers.supplier_id AND 
	".TB_PREF."cheque_details.chk_date >= '$date_after' AND	
	".TB_PREF."cheque_details.chk_date <= '$date_to' ";	

if ($_POST['supplier_id'] != ALL_TEXT)
	$sql .= " AND ".TB_PREF."supp_trans.supplier_id = ".db_escape($_POST['supplier_id']);
	
if (isset($_POST['check_num']) && $_POST['check_num'] != "")
	$sql .= " AND ".TB_PREF."cheque_details.chk_number LIKE ".db_escape("%".$_POST['check_num']."%");

//------------------------------------------------------------------------------------------------

$cols = array(
	_("#") => array('fun'=>'trans_view', 'ord'=>''),
	_("Supplier") => array('ord'=>''),
	_("Check #") => array('ord'=>''),
	_("Check Date") => array('name'=>'chk_date', 'type'=>'date', 'ord'=>'desc'),
	_("Amount") => array('type'=>'amount', 'ord'=>'')
	);
	
$table =& new_db_pager('trans_tbl', $sql, $cols);

$table->width = "75%";

display_db_pager($table);

end_form();
end_page();

?>
