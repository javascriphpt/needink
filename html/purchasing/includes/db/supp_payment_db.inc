<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
function add_supp_payment($supplier_id, $date_, $bank_account,
	$amount, $discount, $ref, $memo_, $rate=0, $charge=0, $ewt=0)
{
	global $Refs;

	begin_transaction();

   	$supplier_currency = get_supplier_currency($supplier_id);
    $bank_account_currency = get_bank_account_currency($bank_account);
	$bank_gl_account = get_bank_gl_account($bank_account);

	if ($rate == 0)
	{
		$supp_amount = exchange_from_to($amount, $bank_account_currency, $supplier_currency, $date_);
		$supp_discount = exchange_from_to($discount, $bank_account_currency, $supplier_currency, $date_);
		$supp_charge = exchange_from_to($charge, $bank_account_currency, $supplier_currency, $date_);
	}
	else
	{
		$supp_amount = round($amount / $rate, user_price_dec());
		$supp_discount = round($discount / $rate, user_price_dec());
		$supp_charge = round($charge / $rate, user_price_dec());
	}
	

	// it's a supplier payment
	$trans_type = ST_SUPPAYMENT;

	/* Create a supp_trans entry for the supplier payment */
	$payment_id = add_supp_trans($trans_type, $supplier_id, $date_, $date_,
		$ref, "", -$supp_amount, 0, -$supp_discount, "", $rate, -$ewt);

	// Now debit creditors account with payment + discount

	$total = 0;
    $supplier_accounts = get_supplier_accounts($supplier_id);
	$total += add_gl_trans_supplier($trans_type, $payment_id, $date_, $supplier_accounts["payable_account"], 0, 0,
		$ewt + $supp_amount + $supp_discount, $supplier_id, "", $rate);

	// Now credit discount received account with discounts
	if ($supp_discount != 0)
	{
		$total += add_gl_trans_supplier($trans_type, $payment_id, $date_, $supplier_accounts["payment_discount_account"], 0, 0,
			-$supp_discount, $supplier_id, "", $rate);
	}

	if ($supp_charge != 0)
	{
		$charge_act = get_company_pref('bank_charge_act');
		$total += add_gl_trans_supplier($trans_type, $payment_id, $date_, $charge_act, 0, 0,
			$supp_charge, $supplier_id, "", $rate);
	}

	if ($supp_amount != 0)
	{
		$total += add_gl_trans_supplier($trans_type, $payment_id, $date_, $bank_gl_account, 0, 0,
			-($supp_amount + $supp_charge), $supplier_id, "", $rate);
	}
	
	if ($ewt != 0)
	{
		$ewt_act = get_company_pref('default_sales_ewt_act');
		$total += add_gl_trans_supplier($trans_type, $payment_id, $date_, $ewt_act, 0, 0,
			-$ewt, $supplier_id, "", $rate);
	}

	/*Post a balance post if $total != 0 */
	add_gl_balance($trans_type, $payment_id, $date_, -$total, PT_SUPPLIER, $supplier_id);	

   /*now enter the bank_trans entry */
	add_bank_trans($trans_type, $payment_id, $bank_account, $ref,
		$date_, -($amount + $charge/*$supp_charge*/), PT_SUPPLIER,
		$supplier_id, $bank_account_currency,
		"Could not add the supplier payment bank transaction");
		
	if(get_bank_trans_type($bank_account) == 1 && $_POST['type_'] == 2)
	{
		add_check($payment_id, ST_SUPPAYMENT, $_POST['Bank'], $_POST['Branch'], $_POST['ChkNo'], $_POST['Cheque_Date']);	
		issue_check_number2(ST_SUPPAYMENT, $payment_id, $_POST['ChkNo']);
		
		// change gl tran_date = check_date
		$sql_check = "UPDATE ".TB_PREF."gl_trans SET tran_date = ".db_escape(date2sql($_POST['Cheque_Date']))."
			WHERE type=".db_escape($trans_type)." AND type_no=".db_escape($payment_id);

		db_query($sql_check, "could not update gl tran_date");
	}

	add_comments($trans_type, $payment_id, $date_, $memo_);

	$Refs->save($trans_type, $payment_id, $ref);
	
	commit_transaction();

	return $payment_id;
}

//------------------------------------------------------------------------------------------------

function add_check_bank2($check_account, $type, $bank_trans_id, $reference)
{
	$sql = "INSERT INTO ".TB_PREF."check_trans (check_ref, bank_trans_type, bank_trans_id, cheque_bank_id) VALUES (".db_escape($reference).", $type, $bank_trans_id, $check_account)";
	db_query($sql, "Cannot Add new Cheque");
	return db_insert_id();
}


function issue_check_number2($type, $trans_no, $reference,$x=null) {
	begin_transaction();
	
	//list($bank_trans_id, $account_id,) = get_trans_id_and_bank_account($type, $trans_no,$x);
	
	$sql = "SELECT bank_act, id
			FROM ".TB_PREF."bank_trans 
			WHERE type=$type 
			AND trans_no=$trans_no";
	$result = db_query($sql, "could not get bank trans id ");	
	$row = db_fetch_row($result);
	
	$account_id = get_bank_gl_account($row[0]);
	
	add_check_bank2($account_id, $type, $row[0], $reference);
	
	save_last_cheque2($account_id, $reference);
	
	commit_transaction();
}

function save_last_cheque2($bank_ref, $check_ref)
{
	$next = references::increment($check_ref);
	save_next_check_reference2($bank_ref, $next);
}

function save_next_check_reference2($check_account, $check_ref)
{
    $sql = "UPDATE ".TB_PREF."check_account SET next_reference=" . db_escape(trim($check_ref)) . " WHERE bank_ref=$check_account";

	db_query($sql, "The next transaction cheque ref could not be updated");
}

//------------------------------------------------------------------------------------------------

function void_supp_payment($type, $type_no)
{
	begin_transaction();

	void_bank_trans($type, $type_no, true);
	void_gl_trans($type, $type_no, true);
	void_supp_allocations($type, $type_no);
	void_supp_trans($type, $type_no);

	commit_transaction();
}


?>