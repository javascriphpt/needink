<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPTRANSVIEW';
$path_to_root = "..";
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/includes/db_pager.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/manufacturing.inc");
include_once($path_to_root . "/includes/data_checks.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);
if ($use_date_picker)
	$js .= get_js_date_picker();
	
page(_($help_context = "Close Purchase Order"), false, false, "", $js);

//----------------------------------------------------------------------------------------

$po_no = $_GET['PONumber'];

$sql = "SELECT line.*
		FROM " .TB_PREF."purch_orders as porder, " .TB_PREF."purch_order_details as line
		WHERE porder.order_no = line.order_no
		AND porder.order_no = ".db_escape($po_no);
$result = db_query($sql, "could not retrieve transaction");

while($row = db_fetch($result))
{
	$sql2 = "UPDATE " .TB_PREF."purch_order_details
			SET quantity_ordered = ".db_escape($row['quantity_received'])."
			WHERE po_detail_item = ".db_escape($row['po_detail_item']);
	db_query($sql2, "could not update purchase order details");	
}

display_notification(_("Purchase Order Closed"));
meta_forward($path_to_root . "/purchasing/inquiry/po_search.php");

end_page();

?>
