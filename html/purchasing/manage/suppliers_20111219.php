<?
$page_security = 'SA_SUPPLIER';
$path_to_root="../..";
include($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
page(_("Supplier"));
echo "<link type='text/css' href='css/smoothness/jquery-ui-1.7.1.custom.css' rel='stylesheet' />	
		<script type='text/javascript' src='js/jquery-1.3.2.min.js'></script>
		<script type='text/javascript' src='js/jquery-ui-1.7.1.custom.min.js'></script>";
	if($_GET['x']==1) display_notification_centered("New Supplier has been added.");
	else if($_GET['x']==2) display_notification_centered("Changes has been saved.");
	$country="";
	
$new_supplier = get_post('supplier_id') == ''; 
// display_error($new_supplier);
global $Ajax;
if(list_updated('supplier_id')){
	set_focus('tab-1');
	$new_supplier = false; 
	
	$Ajax->activate('tabs');
	$Ajax->activate('tab-1');
	$Ajax->activate('tab-2');
	$Ajax->activate('tab-3');

}	
	
function get_role_name($id){
$sql = "SELECT role FROM ".TB_PREF."security_roles WHERE id = ".$id;
$sql = db_query($sql);
$data = db_fetch($sql);

return $data['role'];
}

function empty_fields(){
	$_POST['supp_account_no'] = '';
	$_POST['supp_name'] = '';
	$_POST['supp_ref'] = '';
	$_POST['o_address'] = '';
	$_POST['w_address'] = '';
	$_POST['p_address'] = '';
	$_POST['website'] = '';
	$_POST['dti_no'] = '';
	$_POST['tin_no'] = '';

	$_POST['yr_operation'] =number_format(0);
	$_POST['capitalization'] = price_format(0);
	$_POST['yr_business'] =number_format(0);
	$_POST['total_sales'] = price_format(0);
	$_POST['employee_no'] = number_format(0);
	
	
	$_POST['bank_account'] = '';
	$_POST['credit_limit'] = price_format(0);
	
	$_POST['btype_id'] = 1;
	$_POST['bclass_id'] = 1;
	
	$_POST['cname'] = '';
	$_POST['cphone_no'] = '';

	$_POST['cfax_no'] = '';
	$_POST['cfax2_no'] = '';
	$_POST['email'] = '';
	
	$_POST['ref_name'] = '';
	$_POST['ref_person'] = '';
	$_POST['ref_person_tel'] = '';
	$_POST['ref_accnt_no'] = '';
}

if(isset($_POST['submit'])){

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	/*******************************************************
	Validation of input fields...************************************
	*******************************************************/

	if (strlen($_POST['supp_name']) == 0 || $_POST['supp_name'] == "") 
	{
		$input_error = 1;
		display_error(_("The supplier name must be entered."));
		set_focus('supp_name');
		die();
	}
	
	if (strlen($_POST['supp_ref']) == 0 || $_POST['supp_ref'] == "") 
	{
		$input_error = 1;
		display_error(_("The supplier short name must be entered."));
		set_focus('supp_ref');
		die();
	}
	
	/*******************************************************
	*******************************************************/
	
	// $comp = "SELECT count (*) as co
				  // FROM ".TB_PREF."suppliers 
				  // WHERE supp_account_no = ".db_escape($_POST['supp_account_no'])." 
				  // OR supp_ref = ".db_escape($_POST['supp_name'])." 
				  // OR supp_name = ".db_escape($_POST['supp_ref']);
	$comp = "SELECT count(*) as co 
				  FROM ".TB_PREF."suppliers 
				  WHERE supp_ref = ".db_escape($_POST['supp_ref'])." 
				  OR supp_name = ".db_escape($_POST['supp_name']);
	
	// display_error($comp);
	$comp= db_query($comp);
	$data = db_fetch($comp);
	$exist = $data['co'];
	// display_error($exist);
	if ($exist) 
	{
	// display_error('qweqweqwe');
		$input_error = 1;
		display_error(_("The entered supplier already exist. Please go back and enter different values."));
		// set_focus('supp_name');
	}
	// die();
	
	
	if ($input_error !=1 ){
		if($new_supplier){
		$sql = "INSERT INTO ".TB_PREF."suppliers (
						supp_account_no,
						supp_name,
						supp_ref,
						address,
						warehouse_address,
						plant_address,
						website,
						dti_registration_no,
						tin_no,
						btype_id,
						bclass_id,
						no_of_operation_yr,
						capitalization,
						no_of_business_yr,
						total_sales,
						no_of_employees,
						bank_account,
						credit_limit,
						curr_code,
						tax_group_id,
						payment_terms,
						payable_account,
						purchase_account,
						payment_discount_account,
						inactive
					) VALUES (";
					
			$sql.= db_escape($_POST['supp_account_no']).",";
			$sql.= db_escape($_POST['supp_name']).",";
			$sql.= db_escape($_POST['supp_ref']).",";
			$sql.= db_escape($_POST['o_address']).",";
			$sql.= db_escape($_POST['w_address']).",";
			$sql.= db_escape($_POST['p_address']).",";
			$sql.= db_escape($_POST['website']).",";
			$sql.= db_escape($_POST['dti_no']).",";
			$sql.= db_escape($_POST['tin_no']).",";
			$sql.= db_escape($_POST['btype_id']).",";
			$sql.= db_escape($_POST['bclass_id']).",'";
			$sql.= input_num('yr_operation',0)."','";
			$sql.= input_num('capitalization',0)."','";
			$sql.= input_num('yr_business',0)."','";
			$sql.= input_num('total_sales',0)."','";
			$sql.= input_num('employee_no',0)."',";
			$sql.= db_escape($_POST['bank_account']).",'";
			$sql.= input_num('credit_limit',0)."',";
			$sql.= db_escape($_POST['curr_code']). ", ";
			$sql.= db_escape($_POST['tax_group_id']). ", ";
			$sql.= db_escape($_POST['payment_terms']). ", ";
			$sql.= db_escape($_POST['payable_account']). ", ";
			$sql.= db_escape($_POST['purchase_account']). ", ";
			$sql.= db_escape($_POST['payment_discount_account']).", ";
			$sql.= db_escape($_POST['inactive']);
			$sql.=")";
			
			// display_error($sql);
			// $sql.=";";
			
			db_query($sql);
			display_notification('Successfully added new supplier.');
			
			$last = mysql_insert_id();
			$sql="UPDATE ".TB_PREF."contacts 
					 SET person_id = ".$last." 
					 WHERE person_id is null"; 
			db_query($sql);
			
			
			$Ajax->activate('tabs');
			empty_fields();
			// $contact_update = "UPDATE ".TB_PREF."contacts 
										// SET person_id ".db_escape().;
			// die();
		}
	
	}
	
	// display_error($sql);
	// die();
}

start_form();

// if(isset($_GET['supp_id']) || $_GET['supp_id']!='' || $_POST['supplier_id']!=$_GET['supp_id']){
 // $_GET['supp_id'] = $_POST['supplier_id'];
// // exit();
// }
echo "<center>";
// supplier_list_cells($label, $name, $selected_id=null, $all_option=false, 
	// $submit_on_change=false, $all=false, $editkey = false,$async=false)
supplier_list_cells(_("Select a supplier: "), 'supplier_id', null,
		  _('New supplier'), true, check_value('show_inactive'),false,true);
	check_cells(_("Show inactive:"), 'show_inactive', null, true);
echo "</center>";
	if (get_post('_show_inactive_update')) {
		$Ajax->activate('supplier_id');
		set_focus('supplier_id');
	}
if (isset($_GET['supplier_id'])) 
{
	$_POST['supplier_id'] = $_GET['supplier_id'];
}
if($new_supplier AND !isset($_POST['insert_contact'])){

	//Default value for fields...
	empty_fields();
	
}else{
	$sql = "SELECT * 
			  FROM ".TB_PREF."suppliers ";
	$sql.= " WHERE supplier_id = ".db_escape($_POST['supplier_id']);
	$sql = db_query($sql);
	$data = db_fetch($sql);
	
	$_POST['supp_account_no'] = $data['supp_account_no'];
	$_POST['supp_name'] = $data['supp_name'];
	$_POST['supp_ref'] = $data['supp_ref'];
	$_POST['o_address'] = $data['office_address'];
	$_POST['w_address'] = $data['warehouse_address'];
	$_POST['p_address'] = $data['plant_address'];
	$_POST['website'] = $data['website'];
	$_POST['dti_no'] = $data['dti_registration_no'];
	$_POST['tin_no'] = $data['tin_no'];
	$_POST['btype_id'] = $data['btype_id'];
	$_POST['bclass_id'] = $data['bclass_id'];
	$_POST['yr_operation'] = $data['no_of_operation_yr'];
	$_POST['capitalization'] = price_format($data['capitalization']);
	$_POST['yr_business'] = $data['no_of_business_yr'];
	$_POST['total_sales'] = price_format($data['total_sales']);
	$_POST['credit_limit'] = price_format($data['credit_limit']);
	$_POST['employee_no'] = $data['no_of_employees'];
		
	
	// if($_POST['supplier_id']!=$_GET['supp_id']){
	
		// $Ajax->activate('_page_body');
		// if($data['supplier_id'])
		// meta_forward($_SERVER['PHP_SELF'], 'supp_id='.$data['supplier_id']);
		// $_POST['supplier_id'] = 2;
		// $_POST['supplier_id']!=$_GET['supp_id'];
	// }
	// exit;
	
}
// end_form();
	
// echo "<br><form method=POST><div id='tabs' style='width:90%; margin:auto'>
echo "<br><div id='tabs' style='width:90%; margin:auto'>
        <ul>
            <li><a href='#tab-1'>General Information</a></li>
            <li><a href='#tab-2'>Contact Person</a></li>
			<li><a href='#tab-3' onclick=addbranch($_GET[deb_id])>Credit Reference</a></li>
	   </ul>";
	//		
	
/****************General Tab********************/		

div_start('tab-1');
echo "
<table>
	<tr>
		<td rowspan=2 valign=top>";

echo "
				<fieldset>
					<legend>General Information</legend>
					<table align=center width=100% bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=0>";
		
						text_row(_("Our Customer No:"), 'supp_account_no', null, 20, 30);
						text_row(_("Supplier Name:"), 'supp_name', null, 42, 40);
						text_row(_("Supplier Short Name:"), 'supp_ref', null, 30, 30);
						textarea_row(_("Office Address:"), 'o_address', null, 45, 5);
						textarea_row(_("Warehouse(s) Address:"), 'w_address', null, 45, 5);
						textarea_row(_("Plant(s) Address:"), 'p_address', null, 45, 5);
						link_row(_("Website:"), 'website', null, 40, 55);
						text_row(_("SEC / DTI Registration No.:"), 'dti_no', null, 35, 40);
						text_row(_("TIN No:"), 'tin_no', null, 35, 40);
						record_status_list_row(_("Supplier status:"), 'inactive');
					echo "</table>";
					echo "</fieldset>";

echo "</td>";
echo "<td valign=top width=70%>
				<fieldset>
					<legend>Other Information</legend>
					<table width=100% align=center bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=30>";

						business_types_list_row(_("Business Type:"), 'btype_id', $_POST['btype_id']);
						echo "<td>";
						hyperlink_params_td($path_to_root . "/sales/manage/business_type.php",
							'<b>'. (@$_REQUEST['popup'] ?  _("Select or &Add") : _("&Add or Edit ")).'</b>', 
							"".(@$_REQUEST['popup'] ? '&popup=1':''));
						end_row();

						business_class_types_list_row(_("Classification:"), 'bclass_id', $_POST['bclass_id']);
						echo "<td>";
						hyperlink_params_td($path_to_root . "/sales/manage/business_classification.php",
							'<b>'. (@$_REQUEST['popup'] ?  _("Select or &Add") : _("&Add or Edit ")).'</b>', 
							"".(@$_REQUEST['popup'] ? '&popup=1':''));
						end_row();
					
						amount_row(_("No. of Years in Operation"),'yr_operation', null,null,null,0);
						// amount_row(_("Capitalization:"), 'capitalization', null, 42, 40);
						amount_row(_("Capitalization:"), 'capitalization', null);
						// text_row(_("No. of Years in Business:"), 'b_operation', null, 42, 40);
						amount_row(_("No. of Years in Business:"), 'yr_business', null,null,null,0);
						// text_row(_("Total Sales (Previous Fiscal Year):"), 'total_sales', null, 42, 40);
						amount_row(_("Total Sales (Previous Fiscal Year):"), 'total_sales', null);
						amount_row(_("No. of Employees:"), 'employee_no', null,null,null,0);
		
					echo "</table>";
				echo "</fieldset>";

echo "</tr>";
echo "<tr>
	<td valign=top width=50%>
		<fieldset>
			<legend>Purchasing</legend>
			<table width=100% align=center bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=30>";

				text_row(_("Bank Name/Account:"), 'bank_account', null, 42, 40);
				// amount_row(_("Credit Limit:"), 'credit_limit', null);
				amount_row(_("Credit Limit::"), 'credit_limit', null,null,null,0);
				if (!$new_supplier) 
				{
					label_row(_("Supplier's Currency:"), $_POST['curr_code']);
					hidden('curr_code', $_POST['curr_code']);
				} 
				else 
				{
					currencies_list_row(_("Supplier's Currency:"), 'curr_code', null);
				}

				tax_groups_list_row(_("Tax Group:"), 'tax_group_id', null);

				payment_terms_list_row(_("Payment Terms:"), 'payment_terms', null);

			echo "</table>";
		echo "</fieldset>";
echo "<tr>
	<td valign=top width=50%>
		<fieldset>
			<legend>Accounts</legend>";
echo "			<table width=100% bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' >";

				gl_all_accounts_list_row(_("Accounts Payable Account:"), 'payable_account', $_POST['payable_account']);

				gl_all_accounts_list_row(_("Purchase Account:"), 'purchase_account', $_POST['purchase_account']);

				gl_all_accounts_list_row(_("Purchase Discount Account:"), 'payment_discount_account', $_POST['payment_discount_account']);

				// $dim = get_company_pref('use_dimension');
				// if ($dim >= 1)
				// {
					// table_section_title(_("Dimension"));

					// dimensions_list_row(_("Dimension")." 1:", 'dimension_id', null, true, " ", false, 1);
					// if ($dim > 1)
						// dimensions_list_row(_("Dimension")." 2:", 'dimension2_id', null, true, " ", false, 2);
				// }
				// if ($dim < 1)
					// hidden('dimension_id', 0);
				// if ($dim < 2)
					// hidden('dimension2_id', 0);

			echo "</table>";
		echo "</fieldset>";
echo "</table>";



div_end();


//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\//\\

function can_process(){
	$name=$_POST['cname'];
		$sql = "SELECT contact_name 
				  FROM ".TB_PREF."contacts 
				  WHERE contact_name = ".db_escape($name);
		// display_error($sql);
		$sql = db_query($sql);
		$count = db_num_rows($sql);
		
		if($count!=0){
			display_error('This contact person already exist.');
			return false;
		}elseif(empty($name)){
			display_error('Contact name is empty. Cannot process.');
			return false;
		}if($count==0 && !empty($name))
			return true;
}


if(!isset($_POST['supplier_id']))
display_error('asdasd');
$can_process = 0;


if(isset($_POST['insert_contact']))
{
	if(can_process()){
		$sql = "INSERT INTO 0_contacts(
			contact_name,
			phone_no,
			phone2_no,
			fax_no,
			fax2_no,
			email_address,
			role_id,
			person_id) VALUES( 
			".db_escape($_POST['cname']).",
			".db_escape($_POST['cphone_no']).",
			".db_escape($_POST['cphone2_no']).",
			".db_escape($_POST['cfax_no']).",
			".db_escape($_POST['cfax2_no']).",
			".db_escape($_POST['email']).",
			".db_escape($_POST['role']).",
			1
		)";
		db_query($sql,'fail');
		display_notification('Added new contact person.');
	}
	
	$_POST['cname'] = 
	$_POST['cphone_no'] = 	
	$_POST['cphone2_no'] = 	
	$_POST['cfax_no'] = 
	$_POST['cfax2_no'] = 
	$_POST['email'] =
	$_POST['role'] = '';
				
	$Ajax->activate('tabs');
	$Ajax->activate('tab-1');
	$Ajax->activate('tab-2');
	$Ajax->activate('tab-3');
}

$delete_id  = find_submit('Deletexxx');
if($delete_id != -1)
{
	$sql = "DELETE FROM 0_contacts WHERE id=".$delete_id;
	db_query($sql);
	display_notification('Successfully deleted a contact.');

	$Ajax->activate('tab-2');

}
div_start('tab-2');

$supp_id = $_POST['supplier_id'];
echo  "
<div id=contact></div><br>";
// //========================================================================
	echo "<table align=center  cellpadding=3 border=1 bordercolor='#cccccc' style='border-collapse: collapse'
			<tr>
				<th class=tableheader>Contact Name
				<th class=tableheader>Role
				<th class=tableheader colspan=2>Telephone No.
				<th class=tableheader colspan=2>Fax No.
				<th class=tableheader>Email Address
				<th class=tableheader>&nbsp;
			</tr>";
	$sql = "SELECT * 
			  FROM ".TB_PREF."contacts
			  WHERE person_id = 1";
	if($supp_id)
	$sql.= " AND person_id = ".db_escape($supp_id);
	else
	$sql.= " AND person_id IS NULL";
	//Supplier = 1
	//Customer = 2
	$sql.= " AND contact_type = 1";
	
	// display_error($sql);
	$sql = db_query($sql);
	if(db_num_rows($sql)==0){
		echo "<tr><td colspan=10 align=center>No existing contacts</td>";
	}
	else
	{
		// $role = "SELECT";
	
		while($a=db_fetch($sql))
		{
			start_row();
			echo "<td>".$a['contact_name']."</td>";
			echo "<td>".get_role_name($a['role_id'])."</td>";
			echo "<td>".(!empty($a['phone_no'])?$a['phone_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['phone2_no'])?$a['phone2_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['fax_no'])?$a['fax_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['fax2_no'])?$a['fax2_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['email_address'])?$a['email_address']:'<font color=red>n/a</font>')."</td>";
			
			submit_cells('Deletexxx'.$a['id'],'Delete','',false,true);
			
			end_row();
		}
	}
	echo "</table><br>";
	//========================================================================
echo "<br>";
echo "<center>	
		<fieldset align=center style='width:40%'><legend>Add Contact</legend>
<table width=100% align=center bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=30>";
	text_row(_("Contact Name:"), 'cname', null, 40, 40);
	text_row(_("Phone No.(s):"), 'cphone_no', null, 20, 20);
	text_row(_(""), 'cphone2_no', null, 20, 20);
	text_row(_("Fax No.(s):"), 'cfax_no', null, 20, 20);
	text_row(_(""), 'cfax2_no', null, 20, 20);
	email_row(_("Email Address:"), 'email', null, 35, 55);
	security_roles_list_cells(_("Role:"). "&nbsp;", 'role', null, false, false, check_value('show_inactive'));
	// foreach($_POST as $key=>$value)
		// display_error($key."=>".$value);
echo "</table>";
echo "<br>
	<div align=center>";

$idd = $_POST['supplier_id'];
// echo "			<input type=button value=Add class=x onclick=addcontact('$idd','insert')>";
submit_center('insert_contact','Add',true,false,true);
echo "	</div>
		</fieldset>
		</center>
		";
div_end();
		
		
/****************Credit Reference Tab********************/			
div_start('tab-3');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function can_process_reference(){
	$ref_name=$_POST['ref_name'];

	$sql = "SELECT name  
			  FROM ".TB_PREF."references 
			  WHERE name = ".db_escape($ref_name);
	// DISPLAY_ERROR($SQL);
		$sql = db_query($sql);
		$count = db_num_rows($sql);
		
		if($count!=0){
			display_error('This name already exist.');
			return false;
		}elseif(empty($ref_name)){
			display_error('Reference name is empty. Cannot process.');
			return false;
		}if($count==0 && !empty($ref_name))
			return true;
}

if(isset($_POST['insert_reference'])){
	if(can_process_reference()){
		$ref_name = $_POST['ref_name'];
		$ref_person =  $_POST['ref_person'];
		$ref_person_tel = $_POST['ref_person_tel'];
		$ref_accnt_no = $_POST['ref_accnt_no'];
		
		$supp_id  = $_POST['supplier_id'];
		// $type_id = $_POST['type_id'];
		$type_id = 1;
	
		if(!empty($ref_name))
		{
			$sql = "INSERT INTO 0_references(name,contact_person,contact_no,account_no,type_id,person_id) VALUES(
					 '$ref_name','$ref_person','$ref_person_tel','$ref_accnt_no','$type_id','$supp_id')";
			// display_error($sql);
			mysql_query($sql);
			// display_notification('Successfully added a new reference.');
		}
		
		$_POST['ref_name'] = '';
		$_POST['ref_person'] =  '';
		$_POST['ref_person_tel'] = '';
		$_POST['ref_accnt_no'] = '';
		display_notification('Added new reference.');
	}
	
	$Ajax->activate('tabs');
	$Ajax->activate('tab-1');
	$Ajax->activate('tab-2');
	$Ajax->activate('tab-3');
}

$delete_idd = find_submit('Delete_');
if($delete_idd != -1)
{
	$sql = "DELETE FROM ".TB_PREF."references WHERE id=".$delete_idd;
	display_error($sql);
	// db_query($sql);
	display_notification('Successfully deleted a reference.');

	$Ajax->activate('tab-3');

}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

echo "<br>";
echo "<table align=center cellpadding=3 border=1 bordercolor='#cccccc' style='border-collapse: collapse'
	<tr>
		<th class=tableheader>Name
		<th class=tableheader>Contact Person
		<th class=tableheader>Telephone No.
		<th class=tableheader>Account No.
		<th class=tableheader>&nbsp;
	</tr>";
	
	$sql = "SELECT * 
			  FROM 0_references ";
	if($supp_id)
		$sql.= " WHERE person_id = ".db_escape($supp_id);
	else
		$sql.= " WHERE person_id IS NULL";
			  
			  // display_error($sql);
	$sql = mysql_query($sql);
	
	if(mysql_num_rows($sql)==0){
		echo "<tr><td colspan=10 align=center>No existing Reference.</td>";
	}else{
		while($a=db_fetch($sql))
			{
				start_row();
				echo "<td>".$a['name']."</td>";
				echo "<td>".(!empty($a['contact_person'])?$a['contact_person']:'<font color=red>n/a</font>')."</td>";
				echo "<td>".(!empty($a['contact_no'])?$a['contact_no']:'<font color=red>n/a</font>')."</td>";
				echo "<td>".(!empty($a['account_no'])?$a['account_no']:'<font color=red>n/a</font>')."</td>";

				// echo "<td style='vertical-align: top;'><input type=button name=delBranch value=Remove class=x onclick=addbranch($_GET[id],'delete',$a[id])></td>";
				submit_cells('Delete_'.$a['id'],'Delete','',false,true);
				end_row();
			}
	}
			
echo "</table>";

echo  "<br>
		
		<br><center>
			<fieldset style='width:35%;' align=center>
				<legend>Add Credit Reference</legend>
				<table width=100% align=center bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=30>";
					
					text_row(_("Name:"), 'ref_name', null, 40, 40);
					text_row(_("Contact Person:"), 'ref_person', null, 40, 40);
					text_row(_("Telephone No."), 'ref_person_tel', null, 40, 40);
					text_row(_("Account No."), 'ref_accnt_no', null, 40, 40);
					
					// $idd = $_POST['supplier_id'];
echo "					
				</table>
				<br>";
				submit_center('insert_reference','Add',true,false,true);
				// <input type=button value=Add class=x id='_add_' onclick=addbranch('$idd','insert')>
			echo "</fieldset>
		</center>
		";
		
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		

div_end();
div_end();
echo "<br>";
display_error($new_supplier);
if($new_supplier)
{
display_error('saasa'.get_post('supplier_id'));
	submit_center_first('update', _("Update Supplier"), 
	  _('Update supplier data'),true);
	// submit_return('select', get_post('supplier_id'), _("Select this supplier and return to document entry."));
	// submit_center_last('delete', _("Delete Supplier"), 
	  // _('Delete supplier data if have been never used'), true);
}
else
{
	// submit_center('submit', _("Add New Supplier Details"), true, '', 'default');
	submit_center('submit', _("Add New Supplier Details"), true, '', true);
}
end_form();
end_page();
?>
<style>
	.x{
	font-family:verdana,arial,sans-serif;
	width:100px;
	border: 1px solid rgb(0, 102, 204); 
	padding: 1px 4px; 
	background-color: rgb(226, 226, 226); 
	font-size: 12px;
	}
	select
	{
		width:50%;
	}
	legend{
		font-size:14px;
		font-family:calibri;
		color:orange;
		font-weight:bold;
		}
</style>

<script>

	$(function(){
		// Tabs
		$('#tabs').tabs();
		
		//hover states on the static widgets
		$('#dialog_link, ul#icons li').hover(
			function() { $(this).addClass('ui-state-hover'); }, 
			function() { $(this).removeClass('ui-state-hover'); }
		);
	});

</script>

