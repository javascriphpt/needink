<?
	$path_to_root="../..";
	include($path_to_root . "/includes/session.inc");
	include_once($path_to_root . "/includes/ui.inc");

	$supp_id = $_GET['id'];
	$action = $_GET['action'];
	$contact_id = $_GET['contact_id'];
	$name=$_GET['cname'];
	$cphone=$_GET['cphone'];
	$cphone2=$_GET['cphone2'];
	$cfax=$_GET['cfax'];
	$cfax2=$_GET['cfax2'];
	$email=$_GET['email'];
	$line=$_GET['line_no'];
	
	function can_process(){
	$name=$_GET['cname'];
		$sql = "SELECT contact_name 
				  FROM ".TB_PREF."contacts 
				  WHERE contact_name = ".db_escape($name);
				  // display_error($sql);
		$query = db_query($sql);
		$count = db_num_rows($query);
		
		if($count!=0){
			display_error('This contact person already exist.');
			return false;
		}elseif(empty($name)){
			display_error('Contact name is empty. Cannot process.');
			return false;
		}if($count==0 && !empty($name))
			return true;
	}

	if($action=='insert'){
		if(can_process()){
			$sql = "INSERT INTO 0_contacts(
				contact_name,
				phone_no,
				phone2_no,
				fax_no,
				fax2_no,
				email_address) VALUES( 
				".db_escape($name).",
				".db_escape($cphone).",
				".db_escape($cphone2).",
				".db_escape($cfax).",
				".db_escape($cfax2).",
				".db_escape($email)."
			)";
			// display_error($sql);die();
			db_query($sql);
			display_notification('Added new contact person.');
		}
	}if($action=='delete'){
		$sql = "DELETE FROM 0_contacts WHERE id=".$line;
		db_query($sql);
		display_notification('Successfully deleted a contact.');
	}
	echo "<table align=center  cellpadding=3 border=1 bordercolor='#cccccc' style='border-collapse: collapse'
			<tr>
				<th class=tableheader>Contact Name
				<th class=tableheader colspan=2>Telephone No.
				<th class=tableheader colspan=2>Fax No.
				<th class=tableheader>Email Address
				<th class=tableheader>&nbsp;
			</tr>";
	$sql = "SELECT * FROM 0_contacts";
	if($supp_id)
	$sql.= " WHERE person_id = ".db_escape($supp_id);
	else
	$sql.= " WHERE person_id IS NULL";
	// display_error($sql);
	$sql = db_query($sql);
	if(db_num_rows($sql)==0){
		echo "<tr><td colspan=10 align=center>No existing Contacts</td>";
	}
	else
	{
		while($a=db_fetch($sql))
		{
			start_row();
			echo "<td>".$a['contact_name']."</td>";
			echo "<td>".(!empty($a['phone_no'])?$a['phone_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['phone2_no'])?$a['phone2_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['fax_no'])?$a['fax_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['fax2_no'])?$a['fax2_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['email_address'])?$a['email_address']:'<font color=red>n/a</font>')."</td>";
			
			// echo "<td><input type=button name=delBranch value=Delete class=x onclick=addcontact(".$(".$a['id'].")></td>";
			// echo "<td><input type=button name=delBranch value=Delete class=x onclick=addcontact('','delete')></td>";
			echo "<td><input type=button value=Remove class=x onclick=addcontact('','delete',".$a['id'].")></td>";
				
			end_row();
		}
	}
	echo "</table>";
?>
<!--
<style>
.x{
	font-family:verdana,arial,sans-serif;
	width:100px;
	border: 1px solid rgb(0, 102, 204); 
	padding: 1px 4px; 
	background-color: rgb(226, 226, 226); 
	font-size: 12px;
	}
</style>-->