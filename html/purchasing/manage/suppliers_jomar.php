<?
$page_security = 'SA_SUPPLIER';
$path_to_root="../..";
include($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
page(_("Supplier"));
echo "<link type='text/css' href='css/smoothness/jquery-ui-1.7.1.custom.css' rel='stylesheet' />	
		<script type='text/javascript' src='js/jquery-1.3.2.min.js'></script>
		<script type='text/javascript' src='js/jquery-ui-1.7.1.custom.min.js'></script>";

$new_supplier = get_post('supplier_id') == ''; 

global $Ajax;

start_form();

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo "<center>";
// supplier_list_cells($label, $name, $selected_id=null, $all_option=false, 
	// $submit_on_change=false, $all=false, $editkey = false,$async=false)
supplier_list_cells(_("Select a supplier: "), 'supplier_id', null,
		  _('New supplier'), true, check_value('show_inactive'),false,true);
	check_cells(_("Show inactive:"), 'show_inactive', null, true);
echo "</center>";


if(list_updated('supplier_id')){

	$Ajax->activate('controls');
	$Ajax->activate('tab-1');
	$Ajax->activate('tab-2');
	$Ajax->activate('tab-3');
}	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//tabs
echo "<br><div id='tabs' style='width:90%; margin:auto'>
        <ul>
            <li><a href='#tab-1'>General Information</a></li>	
            <li><a href='#tab-2'>Contact Person</a></li>
			<li><a href='#tab-3' >Credit Reference</a></li>";
echo "	   </ul>";

if(isset($_POST['update']))
	field_action('tab1_update');
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//functions

function field_action($select,$result=null){

		if(!empty($result))
		$data = db_fetch($result);
		if($select == 'tab1_data' || $select == 'tab1_clear'){
			if($select == 'tab1_data' )$_POST['supp_account_no'] = $data['supp_account_no']; else $_POST['supp_account_no'] = '';
			if($select == 'tab1_data' )$_POST['supp_name'] = $data['supp_name']; else $_POST['supp_name'] = '';
			if($select == 'tab1_data' )$_POST['supp_ref'] = $data['supp_ref']; else $_POST['supp_ref'] = '';
			if($select == 'tab1_data' )$_POST['o_address'] = $data['address']; else $_POST['o_address'] = '';
			if($select == 'tab1_data' )$_POST['w_address'] = $data['warehouse_address']; else $_POST['w_address'] = '';
			if($select == 'tab1_data' )$_POST['p_address'] = $data['plant_address']; else $_POST['p_address'] = '';
			if($select == 'tab1_data' )$_POST['website'] = $data['website']; else $_POST['website'] = '';
			if($select == 'tab1_data' )$_POST['dti_no'] = $data['dti_registration_no']; else $_POST['dti_no'] = '';
			if($select == 'tab1_data' )$_POST['tin_no'] = $data['tin_no']; else $_POST['tin_no'] = '';
			if($select == 'tab1_data' )$_POST['inactive'] = $data['inactive']; else $_POST['inactive'] = '';
			
			if($select == 'tab1_data' )$_POST['btype_id'] = $data['btype_id']; else $_POST['btype_id'] = '';
			if($select == 'tab1_data' )$_POST['bclass_id'] = $data['bclass_id']; else $_POST['bclass_id'] = '';
			if($select == 'tab1_data' )$_POST['yr_operation'] = number_format($data['no_of_operation_yr']); else $_POST['yr_operation'] = number_format(0);
			if($select == 'tab1_data' )$_POST['capitalization'] = price_format($data['capitalization']); else $_POST['capitalization'] = price_format(0);
			if($select == 'tab1_data' )$_POST['yr_business'] = number_format($data['no_of_business_yr']); else $_POST['yr_business'] = number_format(0);
			if($select == 'tab1_data' )$_POST['total_sales'] = price_format($data['total_sales']); else $_POST['total_sales'] = price_format(0);
			if($select == 'tab1_data' )$_POST['employee_no'] = number_format($data['no_of_employees']); else $_POST['employee_no'] = number_format(0);

			if($select == 'tab1_data' )$_POST['bank_account'] = $data['bank_account']; else $_POST['bank_account'] = '';
			if($select == 'tab1_data' )$_POST['credit_limit'] = price_format($data['credit_limit']); else $_POST['credit_limit'] = price_format(0);
			if($select == 'tab1_data' )$_POST['tax_group_id'] = $data['tax_group_id']; else $_POST['tax_group_id'] = '';
			if($select == 'tab1_data' )$_POST['payment_terms'] = $data['payment_terms']; else $_POST['payment_terms'] = '';
			
			if($select == 'tab1_data' )$_POST['payable_account'] = $data['payable_account']; else $_POST['payable_account'] = '';
			if($select == 'tab1_data' )$_POST['purchase_account'] = $data['purchase_account']; else $_POST['purchase_account'] = '';
			if($select == 'tab1_data' )$_POST['payment_discount_account'] = $data['payment_discount_account']; else $_POST['payment_discount_account'] = '';
		}
		if($select == 'tab1_update'){
			$sql = "UPDATE ".TB_PREF."suppliers 
						SET
						supp_name = ".db_escape($_POST['supp_name']).",
						supp_ref = ".db_escape($_POST['supp_ref']).",
						supp_account_no = ".db_escape($_POST['supp_account_no']).",
						address = ".db_escape($_POST['o_address']).",
						warehouse_address = ".db_escape($_POST['w_address']).",
						plant_address = ".db_escape($_POST['p_address']).",
						website = ".db_escape($_POST['website']).",
						dti_registration_no = ".db_escape($_POST['dti_no']).",
						tin_no = ".db_escape($_POST['tin_no']).",
						btype_id = ".db_escape($_POST['btype_id']).",
						bclass_id = ".db_escape($_POST['bclass_id']).",
						no_of_operation_yr = ".db_escape(input_num('yr_operation',0)).",
						total_sales = ".db_escape(input_num('total_sales',0)).",
						no_of_employees = ".db_escape(input_num('employee_no',0)).",
						bank_account = ".db_escape($_POST['bank_account']).",
						credit_limit = ".db_escape(input_num('credit_limit',0)).",
						tax_group_id = ".db_escape($_POST['tax_group_id']).",
						payment_terms = ".db_escape($_POST['payment_terms']).",
						capitalization = ".db_escape(input_num('capitalization',0)).",
						no_of_business_yr = ".db_escape(input_num('yr_business',0)).",
						payable_account = ".db_escape($_POST['payable_account']).",
						purchase_account = ".db_escape($_POST['purchase_account']).",
						payment_discount_account = ".db_escape($_POST['payment_discount_account']).",
						inactive = ".db_escape($_POST['inactive'])." 
						WHERE supplier_id = 
						".db_escape($_POST['supplier_id']);
				// display_error($sql);
			db_query($sql);
			display_notification('Successfully updated the supplier.');
		}
		
		// if($select == '')
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//events
// display_error($_POST['supplier_id']);

if($new_supplier){
	field_action('tab1_clear');
	// UNSET($_post);
}else{
	$sql = "SELECT * 
			  FROM ".TB_PREF."suppliers ";
	$sql.= " WHERE supplier_id = ".db_escape($_POST['supplier_id']);
	// display_error($sql);
	$result = db_query($sql);
	// $data = db_fetch($sql);
	
	field_action('tab1_data',$result);
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
div_start('tab-1');
	echo "
		<table>
			<tr>
				<td rowspan=2 valign=top>";

		echo "
						<fieldset>
							<legend>General Information</legend>
							<table>";
				
								text_row(_("Our Customer No:"), 'supp_account_no', null, 20, 30);
								text_row(_("Supplier Name:"), 'supp_name', null, 42, 40);
								text_row(_("Supplier Short Name:"), 'supp_ref', null, 30, 30);
								textarea_row(_("Office Address:"), 'o_address', null, 45, 5);
								textarea_row(_("Warehouse(s) Address:"), 'w_address', null, 45, 5);
								textarea_row(_("Plant(s) Address:"), 'p_address', null, 45, 5);
								link_row(_("Website:"), 'website', null, 40, 55);
								text_row(_("SEC / DTI Registration No.:"), 'dti_no', null, 35, 40);
								text_row(_("TIN No:"), 'tin_no', null, 35, 40);
								record_status_list_row(_("Supplier status:"), 'inactive');
							echo "</table>";
							echo "</fieldset>";

		echo "</td>";
		echo "<td valign=top width=70%>
						<fieldset>
							<legend>Other Information</legend>
							<table width=100% align=center bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=30>";

								business_types_list_row(_("Business Type:"), 'btype_id', $_POST['btype_id']);
								echo "<td>";
								hyperlink_params_td($path_to_root . "/sales/manage/business_type.php",
									'<b>'. (@$_REQUEST['popup'] ?  _("Select or &Add") : _("&Add or Edit ")).'</b>', 
									"".(@$_REQUEST['popup'] ? '&popup=1':''));
								end_row();

								business_class_types_list_row(_("Classification:"), 'bclass_id', $_POST['bclass_id']);
								echo "<td>";
								hyperlink_params_td($path_to_root . "/sales/manage/business_classification.php",
									'<b>'. (@$_REQUEST['popup'] ?  _("Select or &Add") : _("&Add or Edit ")).'</b>', 
									"".(@$_REQUEST['popup'] ? '&popup=1':''));
								end_row();
							
								amount_row(_("No. of Years in Operation"),'yr_operation', null,null,null,0);
								// amount_row(_("Capitalization:"), 'capitalization', null, 42, 40);
								amount_row(_("Capitalization:"), 'capitalization', null);
								// text_row(_("No. of Years in Business:"), 'b_operation', null, 42, 40);
								amount_row(_("No. of Years in Business:"), 'yr_business', null,null,null,0);
								// text_row(_("Total Sales (Previous Fiscal Year):"), 'total_sales', null, 42, 40);
								amount_row(_("Total Sales (Previous Fiscal Year):"), 'total_sales', null);
								amount_row(_("No. of Employees:"), 'employee_no', null,null,null,0);
				
							echo "</table>";
						echo "</fieldset>";

		echo "</tr>";
		echo "<tr>
			<td valign=top width=50%>
				<fieldset>
					<legend>Purchasing</legend>
					<table width=100% align=center bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=30>";

						text_row(_("Bank Name/Account:"), 'bank_account', null, 42, 40);
						// amount_row(_("Credit Limit:"), 'credit_limit', null);
						// amount_row(_("Credit Limit:"), 'credit_limit', null,null,null,0);
					amount_row(_("Credit Limit:"), 'credit_limit', null);

						if (!$new_supplier) 
						{
							label_row(_("Supplier's Currency:"), $_POST['curr_code']);
							hidden('curr_code', $_POST['curr_code']);
						} 
						else 
						{
							currencies_list_row(_("Supplier's Currency:"), 'curr_code', null);
						}

						tax_groups_list_row(_("Tax Group:"), 'tax_group_id', null);

						payment_terms_list_row(_("Payment Terms:"), 'payment_terms', null);

					echo "</table>";
				echo "</fieldset>";
		echo "<tr>
			<td valign=top width=50%>
				<fieldset>
					<legend>Accounts</legend>";
		echo "			<table width=100% bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' >";

						gl_all_accounts_list_row(_("Accounts Payable Account:"), 'payable_account', $_POST['payable_account']);

						gl_all_accounts_list_row(_("Purchase Account:"), 'purchase_account', $_POST['purchase_account']);

						gl_all_accounts_list_row(_("Purchase Discount Account:"), 'payment_discount_account', $_POST['payment_discount_account']);

						// $dim = get_company_pref('use_dimension');
						// if ($dim >= 1)
						// {
							// table_section_title(_("Dimension"));

							// dimensions_list_row(_("Dimension")." 1:", 'dimension_id', null, true, " ", false, 1);
							// if ($dim > 1)
								// dimensions_list_row(_("Dimension")." 2:", 'dimension2_id', null, true, " ", false, 2);
						// }
						// if ($dim < 1)
							// hidden('dimension_id', 0);
						// if ($dim < 2)
							// hidden('dimension2_id', 0);

					echo "</table>";
				echo "</fieldset>";
		echo "</table>";
div_end();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function get_role($id){
	$sql = 'SELECT role FROM '.TB_PREF.'security_roles WHERE id = '.db_escape($id);
	$sql = db_query($sql);
	$data = db_fetch($sql);
	return $data[0];
}

function can_process(){
	$name=$_POST['cname'];
		$sql = "SELECT contact_name 
				  FROM ".TB_PREF."contacts 
				  WHERE contact_name = ".db_escape($name).
				  " AND contact_type = 1";
		// display_error($sql);
		$sql = db_query($sql);
		$count = db_num_rows($sql);
		
		if($count!=0){
			display_error('This contact person already exist.');
			return false;
		}elseif(empty($name)){
			display_error('Contact name is empty. Cannot process.');
			return false;
		}if($count==0 && !empty($name))
			return true;
}

if(isset($_POST['insert_contact']))
{
	if(can_process()){
		$sql = "INSERT INTO 0_contacts(
			contact_name,
			phone_no,
			phone2_no,
			fax_no,
			fax2_no,
			email_address,
			person_id,
			contact_type,
			role_id
			) VALUES( 
			".db_escape($_POST['cname']).",
			".db_escape($_POST['cphone_no']).",
			".db_escape($_POST['cphone2_no']).",
			".db_escape($_POST['cfax_no']).",
			".db_escape($_POST['cfax2_no']).",
			".db_escape($_POST['email']).",
			".db_escape($_POST['supplier_id']).",
			1,
			".db_escape($_POST['role'])."
		)";
		// display_error($sql);
		db_query($sql,'fail');
		display_notification('Added new contact person.');
	
	}
	$_POST['cname'] = 
	$_POST['cphone_no'] = 	
	$_POST['cphone2_no'] = 	
	$_POST['cfax_no'] = 
	$_POST['cfax2_no'] = 
	$_POST['email'] =
	$_POST['role'] = '';
	
				
	$Ajax->activate('tabs');
	$Ajax->activate('tab-1');
	$Ajax->activate('tab-2');
	$Ajax->activate('tab-3');
}

$delete_id  = find_submit('Deletexxx');
if($delete_id != -1)
{
	$sql = "DELETE FROM 0_contacts WHERE id=".$delete_id;
	db_query($sql);
	display_notification('Successfully deleted a contact.');

	$Ajax->activate('tab-2');

}

div_start('tab-2');
if(!$new_supplier)
{
	$supp_id = $_POST['supplier_id'];
	start_table($table_style2);
	echo "
			<tr>
				<th class=tableheader>Contact Name
				<th class=tableheader>Role
				<th class=tableheader colspan=2>Telephone No.
				<th class=tableheader colspan=2>Fax No.
				<th class=tableheader>Email Address
				<th class=tableheader>&nbsp;
			</tr>";
	
	$sql = "SELECT * 
			  FROM ".TB_PREF."contacts
			  WHERE contact_type = 1";
	if($supp_id)
	$sql.= " AND person_id = ".db_escape($supp_id);
	else
	$sql.= " AND person_id IS NULL";
	//Supplier = 1
	//Customer = 2
	$sql.= " AND contact_type = 1";
	
	// display_error($sql);
	$sql = db_query($sql);
	if(db_num_rows($sql)==0){
		echo "<tr><td colspan=10 align=center>No existing contacts</td>";
	}
	else
	{
		// $role = "SELECT";
	
		while($a=db_fetch($sql))
		{
			start_row();
			echo "<td>".$a['contact_name']."</td>";
			echo "<td>".get_role($a['role_id'])."</td>";
			echo "<td>".(!empty($a['phone_no'])?$a['phone_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['phone2_no'])?$a['phone2_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['fax_no'])?$a['fax_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['fax2_no'])?$a['fax2_no']:'<font color=red>n/a</font>')."</td>";
			echo "<td>".(!empty($a['email_address'])?$a['email_address']:'<font color=red>n/a</font>')."</td>";
			submit_cells('Deletexxx'.$a['id'],'Delete','',false,true);
			
			end_row();
		}
	}
	end_table();
	echo "<br>";
	
			start_table($table_style);
				text_row(_("Contact Name:"), 'cname', null, 40, 40);
				text_row(_("Phone No.(s):"), 'cphone_no', null, 20, 20);
				text_row(_(""), 'cphone2_no', null, 20, 20);
				text_row(_("Fax No.(s):"), 'cfax_no', null, 20, 20);
				text_row(_(""), 'cfax2_no', null, 20, 20);
				email_row(_("Email Address:"), 'email', null, 35, 55);
				security_roles_list_cells(_("Role:"). "&nbsp;", 'role', null, false, false, check_value('show_inactive'));
			end_table();
	
	echo "<br>";
	submit_center('insert_contact','Add New Contact',true,false,true);
}else{
	display_warning('Please select supplier first to add or delete contacts and/or references.');
}
div_end();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
function can_process_reference(){
	$ref_name=$_POST['ref_name'];

	$sql = "SELECT name  
			  FROM ".TB_PREF."references 
			  WHERE name = ".db_escape($ref_name)." 
			  AND contact_type = 1
			  ";
	// DISPLAY_ERROR($SQL);
		$sql = db_query($sql);
		$count = db_num_rows($sql);
		
		if($count!=0){
			display_error('This name already exist.');
			return false;
		}elseif(empty($ref_name)){
			display_error('Reference name is empty. Cannot process.');
			return false;
		}if($count==0 && !empty($ref_name))
			return true;
}
		$supp_id  = $_POST['supplier_id'];

if(isset($_POST['insert_reference'])){
	if(can_process_reference()){
		$ref_name = $_POST['ref_name'];
		$ref_person =  $_POST['ref_person'];
		$ref_person_tel = $_POST['ref_person_tel'];
		$ref_accnt_no = $_POST['ref_accnt_no'];
		
		// $type_id = $_POST['type_id'];
		$type_id = 1;
	
		if(!empty($ref_name))
		{
			$sql = "INSERT INTO 0_references(name,contact_person,contact_no,account_no,person_id,contact_type) VALUES(
					 '$ref_name','$ref_person','$ref_person_tel','$ref_accnt_no','$supp_id','$type_id')";
			// display_error($sql);
			mysql_query($sql);
			display_notification('Successfully added a new reference.');
		}
		
		$_POST['ref_name'] = '';
		$_POST['ref_person'] =  '';
		$_POST['ref_person_tel'] = '';
		$_POST['ref_accnt_no'] = '';
	}
	
	$Ajax->activate('tab-3');
}

$delete_idd = find_submit('Delete_');
if($delete_idd != -1)
{
	$sql = "DELETE FROM ".TB_PREF."references WHERE id=".$delete_idd;
	// display_error($sql);
	db_query($sql);
	display_notification('Successfully deleted a reference.');

	$Ajax->activate('tab-3');

}
	
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
div_start('tab-3');
if(!$new_supplier)
{

echo "<br>";
echo "<table align=center cellpadding=3 border=1 bordercolor='#cccccc' style='border-collapse: collapse' ";
echo "	<tr>
		<th class=tableheader>Name
		<th class=tableheader>Contact Person
		<th class=tableheader>Telephone No.
		<th class=tableheader>Account No.
		<th class=tableheader>&nbsp;
	</tr>";
	
	$sql = "SELECT * 
			  FROM 0_references ";
	if($supp_id)
		$sql.= " WHERE person_id = ".db_escape($supp_id);
	else
		$sql.= " WHERE person_id IS NULL";
			  
			  // display_error($sql);
	$sql = mysql_query($sql);
	
	if(mysql_num_rows($sql)==0){
		echo "<tr><td colspan=10 align=center>No existing Reference.</td>";
	}else{
		while($a=db_fetch($sql))
			{
				start_row();
				echo "<td>".$a['name']."</td>";
				echo "<td>".(!empty($a['contact_person'])?$a['contact_person']:'<font color=red>n/a</font>')."</td>";
				echo "<td>".(!empty($a['contact_no'])?$a['contact_no']:'<font color=red>n/a</font>')."</td>";
				echo "<td>".(!empty($a['account_no'])?$a['account_no']:'<font color=red>n/a</font>')."</td>";

				// echo "<td style='vertical-align: top;'><input type=button name=delBranch value=Remove class=x onclick=addbranch($_GET[id],'delete',$a[id])></td>";
				submit_cells('Delete_'.$a['id'],'Delete','',false,true);
				end_row();
			}
	}
			
echo "</table>";

echo  "<br>
		
		<br><center>
			<fieldset style='width:35%;' align=center>
				<legend>Add Credit Reference</legend>
				<table width=100% align=center bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=30>";
					
					text_row(_("Name:"), 'ref_name', null, 40, 40);
					text_row(_("Contact Person:"), 'ref_person', null, 40, 40);
					text_row(_("Telephone No."), 'ref_person_tel', null, 40, 40);
					text_row(_("Account No."), 'ref_accnt_no', null, 40, 40);
					
					// $idd = $_POST['supplier_id'];
echo "					
				</table>
				<br>";
				submit_center('insert_reference','Add Credit Reference',true,false,true);
				// <input type=button value=Add class=x id='_add_' onclick=addbranch('$idd','insert')>
			echo "</fieldset>
		</center>
		";
	
	}
div_end();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo "</div>";
//button
div_start('controls');
echo "<br><br>";
if (!$new_supplier) 
{
	submit_center_first('update', _("Update Supplier"), 
	  _('Update supplier data'), @$_REQUEST['popup'] ? true : 'default');
	submit_return('select', get_post('supplier_id'), _("Select this supplier and return to document entry."));
	submit_center_last('delete', _("Delete Supplier"), 
	  _('Delete supplier data if have been never used'), true);
}
else 
{
	submit_center('submit', _("Add New Supplier Details"), true, '', 'default');
}
div_end();
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
end_form();
end_page();
?>
<style>
	.x{
	font-family:verdana,arial,sans-serif;
	width:100px;
	border: 1px solid rgb(0, 102, 204); 
	padding: 1px 4px; 
	background-color: rgb(226, 226, 226); 
	font-size: 12px;
	}
	select
	{
		width:50%;
	}
	legend{
		font-size:14px;
		font-family:calibri;
		color:orange;
		font-weight:bold;
		}
</style>

<script>

	$(function(){
		// Tabs
		$('#tabs').tabs();
		
		//hover states on the static widgets
		$('#dialog_link, ul#icons li').hover(
			function() { $(this).addClass('ui-state-hover'); }, 
			function() { $(this).removeClass('ui-state-hover'); }
		);
	});

</script>

