<?php

function add_isn($trans_no,$trans_type,$stock_id,$serial,$loc_code,$tran_date,$cost=0,$gross_price=0,$unit_price=0,$tpc=0,$ssn=""){
		$sql = "INSERT INTO ".TB_PREF."isn_gen(trans_no, trans_type, stock_id, isn, loc_code, month , year, cost, gross_price,
				unit_price, tpc,ssn) VALUES(
				".$trans_no.",".$trans_type.",".db_escape($stock_id).",".db_escape($serial).",".db_escape($loc_code).",
				".date("n",strtotime(date2sql($tran_date))).",".date("Y",strtotime(date2sql($tran_date))).",".$cost.",
				".$gross_price.",".$unit_price.",".$tpc.",".db_escape($ssn).")";
		db_query($sql);
}

function delete_isn_batch($trans_no, $trans_type){
		
	db_query("DELETE FROM ".TB_PREF."isn_gen WHERE trans_no = ".$trans_no." AND trans_type=".$trans_type);
	db_query("DELETE FROM ".TB_PREF."isn_trans WHERE trans_no = ".$trans_no." AND trans_type=".$trans_type);
		
}

function update_isn_status($stock_id,$loc_code, $serial,$is_available=0){
	$sql = "UPDATE ".TB_PREF."isn_gen SET  is_available=".$is_available."
			  WHERE stock_id = ".db_escape($stock_id)." AND loc_code = ".db_escape($loc_code)."
			 AND isn = ".db_escape($serial);
			
		db_query($sql);
}

function change_location($stock_id,$loc_code_old,$loc_code_new, $serial){
	$sql = "UPDATE ".TB_PREF."isn_gen SET loc_code = ".db_escape($loc_code_new)."
			 WHERE stock_id = ".db_escape($stock_id)." AND loc_code = ".db_escape($loc_code_old)."
			 AND isn = ".db_escape($serial);
	db_query($sql,"could not update location");
}

function get_isn($trans_no, $trans_type, $stock_id, $loc_code){
	$sql = "SELECT * FROM ".TB_PREF."isn_gen WHERE 
	trans_no = ".$trans_no." AND trans_type=".$trans_type."
	AND stock_id=".db_escape($stock_id)." AND loc_code = ".db_escape($loc_code);
	$sql=db_query($sql);
	
	while($a=db_fetch($sql)){
		$serial_arr[]=$a['isn'];
	}
	
	return implode("\n",$serial_arr);
}

function check_exist_isn($stock_id, $loc_code, $isn){
	$sql="SELECT * FROM ".TB_PREF."isn_gen WHERE is_available=1 AND stock_id=".db_escape($stock_id)
	." AND loc_code = ".db_escape($loc_code)." AND isn = ".db_escape($isn);
	return db_num_rows(db_query($sql));
}

function add_isn_trail($stock_id, $isn, $loc_code, $trans_no, $trans_type, $date,$move_type){
	
	$sql = "INSERT INTO ".TB_PREF."isn_trans(stock_id, isn, trans_no, trans_type, date, movement_type, loc_code) VALUES(
	".db_escape($stock_id).",".db_escape($isn).",".$trans_no.",".$trans_type.",".db_escape(date2sql($date)).",".$move_type.",".db_escape($loc_code)."
	)";
	
	db_query($sql,"Cannot insert ISN trail");

}

?>