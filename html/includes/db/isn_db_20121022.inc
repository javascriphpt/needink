<?php

function add_isn($trans_no,$trans_type,$stock_id,$serial,$loc_code,$tran_date,$cost=0,$gross_price=0,$unit_price=0,$tpc=0,$ssn=""){
		$sql = "INSERT INTO ".TB_PREF."isn_gen(trans_no, trans_type, stock_id, isn, loc_code, month , year, cost, gross_price,
				unit_price, tpc,ssn) VALUES(
				".$trans_no.",".$trans_type.",".db_escape($stock_id).",".db_escape($serial).",".db_escape($loc_code).",
				".date("n",strtotime(date2sql($tran_date))).",".date("Y",strtotime(date2sql($tran_date))).",".$cost.",
				".$gross_price.",".$unit_price.",".$tpc.",".db_escape($ssn).")";
		db_query($sql,"could not insert isn");
}

function delete_isn_batch($trans_no, $trans_type){
		
	db_query("DELETE FROM ".TB_PREF."isn_gen WHERE trans_no = ".$trans_no." AND trans_type=".$trans_type);
	db_query("DELETE FROM ".TB_PREF."isn_trans WHERE trans_no = ".$trans_no." AND trans_type=".$trans_type);
		
}

function update_isn_status($stock_id,$loc_code, $serial,$is_available=0){
	$sql = "UPDATE ".TB_PREF."isn_gen SET  is_available=".$is_available."
			  WHERE stock_id = ".db_escape($stock_id)." AND loc_code = ".db_escape($loc_code)."
			 AND isn = ".db_escape($serial);
			
		db_query($sql);
}

function change_location($stock_id,$loc_code_old,$loc_code_new, $serial){
	$sql = "UPDATE ".TB_PREF."isn_gen SET loc_code = ".db_escape($loc_code_new)."
			 WHERE stock_id = ".db_escape($stock_id)." AND loc_code = ".db_escape($loc_code_old)."
			 AND isn = ".db_escape($serial);
	db_query($sql,"could not update location");
}

function get_isn($trans_no, $trans_type, $stock_id, $loc_code){
	$sql = "SELECT * FROM ".TB_PREF."isn_gen WHERE 
	trans_no = ".$trans_no." AND trans_type=".$trans_type."
	AND stock_id=".db_escape($stock_id)." AND loc_code = ".db_escape($loc_code);
	$sql=db_query($sql);
	
	while($a=db_fetch($sql)){
		$serial_arr[]=$a['isn'];
	}
	
	return implode("\n",$serial_arr);
}

function get_isn_out($trans_no, $trans_type, $stock_id, $loc_code){
	$sql = "SELECT * FROM ".TB_PREF."isn_trans WHERE 
	trans_no = ".$trans_no." AND trans_type=".$trans_type."
	AND stock_id=".db_escape($stock_id)." AND loc_code = ".db_escape($loc_code);
	$sql=db_query($sql);
	
	while($a=db_fetch($sql)){
		$serial_arr[]=$a['isn'];
	}
	
	return implode("\n",$serial_arr);
}

function get_aic_out($trans_no, $trans_type, $stock_id, $loc_code){
	$sql = "SELECT a.*, b.aic FROM ".TB_PREF."isn_trans a, ".TB_PREF."isn_gen b WHERE 
	a.trans_no = ".$trans_no." AND a.trans_type=".$trans_type." AND a.isn=b.isn
	AND a.stock_id=".db_escape($stock_id)." AND a.loc_code = ".db_escape($loc_code);
	$sql=db_query($sql);
	
	while($a=db_fetch($sql)){
		$aic_arr[]=$a['aic'];
	}
	
	return implode("\n",$aic_arr);
}

function check_exist_isn($stock_id, $loc_code, $isn){
	$sql="SELECT * FROM ".TB_PREF."isn_gen WHERE is_available=1 AND stock_id=".db_escape($stock_id)
	." AND loc_code = ".db_escape($loc_code)." AND isn = ".db_escape($isn);
	return db_num_rows(db_query($sql));
}

function check_exist_isn_credit($stock_id, $loc_code, $isn){
	$sql="SELECT * FROM ".TB_PREF."isn_gen WHERE is_available=0 AND stock_id=".db_escape($stock_id)
	." AND loc_code = ".db_escape($loc_code)." AND isn = ".db_escape($isn);
	return db_num_rows(db_query($sql));
}

function add_isn_trail($stock_id, $isn, $loc_code, $trans_no, $trans_type, $date,$move_type){
	
	$sql = "INSERT INTO ".TB_PREF."isn_trans(stock_id, isn, trans_no, trans_type, date, movement_type, loc_code) VALUES(
	".db_escape($stock_id).",".db_escape($isn).",".$trans_no.",".$trans_type.",".db_escape(date2sql($date)).",".$move_type.",".db_escape($loc_code)."
	)";
	
	db_query($sql,"Cannot insert ISN trail");

}

function get_next_ref($loc_code){
	$result=db_fetch(db_query("SELECT concat(prefix,next_ref) FROM ".TB_PREF."pc_code WHERE loc_code=".db_escape($loc_code)));
	return $result[0];
}

function save_next_ref($loc_code,$serial){
	db_query("UPDATE ".TB_PREF."pc_code SET next_ref=replace(".db_escape($serial).",prefix,'') WHERE loc_code=".db_escape($loc_code),"could not update reference");
}

function wo_produce($trans_no, $trans_type,$stock_id,$qty,$loc_code,$tran_date){
	
		$serial = get_next_ref($loc_code);
//		display_error($serial);
	for($i=1;$i<=$qty;$i++){
		
		add_isn($trans_no,$trans_type,$stock_id,$serial,$loc_code,$tran_date);
		add_isn_trail($stock_id, $serial, $loc_code, $trans_no, $trans_type, $tran_date,1);
		$serial=increments($serial);
	//	display_error($serial);
	}
	save_next_ref($loc_code,$serial);
//	die();
}

function increments($reference) 
	{
        if (preg_match('/^(\D*?)(\d+)(.*)/', $reference, $result) == 1) 
        {
			list($all, $prefix, $number, $postfix) = $result;
			$dig_count = strlen($number); 
			$fmt = '%0' . $dig_count . 'd';
			$nextval =  sprintf($fmt, intval($number + 1));

			return $prefix.$nextval.$postfix;
        }
        else 
            return $reference;
	}

?>