<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_ITEMREQUISITION';
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/items_cart.inc");

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/includes/db/isn_db.inc");

include_once($path_to_root . "/inventory/includes/item_requisition_ui.inc");
include_once($path_to_root . "/inventory/includes/inventory_db.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/gl_ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Item Requisition"), false, false, "", $js);

//-----------------------------------------------------------------------------------------------

check_db_has_costable_items(_("There are no inventory items defined in the system (Purchased or manufactured items)."));

check_db_has_movement_types(_("There are no inventory movement types defined in the system. Please define at least one inventory adjustment type."));

//-----------------------------------------------------------------------------------------------

if (isset($_GET['AddedID'])) 
{
	$trans_no = $_GET['AddedID'];
	$trans_type = ST_REQUISITIONSLIP;

	display_notification_centered(_("Item requisition has been processed"));
	display_note(get_trans_view_str($trans_type, $trans_no, _("&View this requisition")));

	hyperlink_no_params($_SERVER['PHP_SELF'], _("Enter &Another Item Requisition"));

	display_footer_exit();
}
//--------------------------------------------------------------------------------------------------

function line_start_focus() {
  global 	$Ajax;

  $Ajax->activate('items_table');
  $_POST['serial']="";
  set_focus('_stock_id_edit');
}
//-----------------------------------------------------------------------------------------------

function handle_new_order()
{
	if (isset($_SESSION['transfer_items']))
	{
		//$_SESSION['transfer_items']->clear_items();
		unset ($_SESSION['transfer_items']);
	}
	
    //session_register("transfer_items");

	$_SESSION['transfer_items'] = new items_cart(ST_REQUISITIONSLIP);
	$_POST['AdjDate'] = new_doc_date();
	if (!is_date_in_fiscalyear($_POST['AdjDate']))
		$_POST['AdjDate'] = end_fiscalyear();
	$_SESSION['transfer_items']->tran_date = $_POST['AdjDate'];	
}

//-----------------------------------------------------------------------------------------------

if (isset($_POST['Process']))
{
	global $Refs;

	$tr = &$_SESSION['transfer_items'];
	$input_error = 0;

	if (count($tr->line_items) == 0)	{
		display_error(_("You must enter at least one non empty item line."));
		set_focus('stock_id');
		return false;
	}
	if (!$Refs->is_valid($_POST['ref'])) 
	{
		display_error(_("You must enter a reference."));
		set_focus('ref');
		$input_error = 1;
	} 
	elseif (!is_new_reference($_POST['ref'], ST_REQUISITIONSLIP)) 
	{
		display_error(_("The entered reference is already in use."));
		set_focus('ref');
		$input_error = 1;
	} 
	elseif (!is_date($_POST['AdjDate'])) 
	{
		display_error(_("The entered date for the adjustment is invalid."));
		set_focus('AdjDate');
		$input_error = 1;
	} 
	elseif (!is_date_in_fiscalyear($_POST['AdjDate'])) 
	{
		display_error(_("The entered date is not in fiscal year."));
		set_focus('AdjDate');
		$input_error = 1;
	} 
	elseif ($_POST['FromStockLocation'] == ALL_TEXT)
	{
		display_error(_("The locations to transfer from should not be empty."));
		set_focus('FromStockLocation');
		$input_error = 1;
	} 
	elseif ($_POST['ToStockLocation'] == ALL_TEXT)
	{
		display_error(_("The locations to transfer to should not be empty."));
		set_focus('ToStockLocation');
		$input_error = 1;
	} 
	elseif ($_POST['FromStockLocation'] == $_POST['ToStockLocation'])
	{
		display_error(_("The locations to transfer from and to must be different."));
		set_focus('FromStockLocation');
		$input_error = 1;
	} 
	else 
	{
		$failed_item = $tr->check_qoh($_POST['FromStockLocation'], $_POST['AdjDate'], true);
		if ($failed_item >= 0) 
		{
			$line = $tr->line_items[$failed_item];
        	display_error(_("The quantity entered is greater than the available quantity for this item at the source location :") .
        		" " . $line->stock_id . " - " .  $line->item_description);
        	echo "<br>";
			$_POST['Edit'.$failed_item] = 1; // enter edit mode
			$input_error = 1;
		}
	}
	
	if (abs($_SESSION['transfer_items']->gl_items_total()) > 0.0001)
	{
		display_error(_("The journal must balance (debits equal to credits) before it can be processed."));
		set_focus('code_id');
		$input_error = 1;
	}

	if ($input_error == 1)
		unset($_POST['Process']);
}

//-------------------------------------------------------------------------------

if (isset($_POST['Process']))
{
	
	$trans_no = add_stock_requisition($_SESSION['transfer_items']->line_items,
		$_POST['FromStockLocation'], $_POST['ToStockLocation'],
		$_POST['AdjDate'], $_POST['type'], $_POST['ref'], $_POST['memo_']);
	
	foreach($_SESSION['transfer_items']->gl_items as $journal_item)
	{	
		add_gl_trans(ST_REQUISITIONSLIP, $trans_no, $_POST['AdjDate'], $journal_item->code_id,
				$journal_item->dimension_id, $journal_item->dimension2_id,
				$journal_item->reference, $journal_item->amount);
	}

	new_doc_date($_POST['AdjDate']);
	$_SESSION['transfer_items']->clear_items();
	unset($_SESSION['transfer_items']);

   	meta_forward($_SERVER['PHP_SELF'], "AddedID=$trans_no");
} /*end of process credit note */

//-----------------------------------------------------------------------------------------------

function check_item_data()
{
	if (!check_num('qty', 0))
	{
		display_error(_("The quantity entered must be a positive number."));
		set_focus('qty');
		return false;
	}
	
	if (isset($_POST['dimension_id']) && $_POST['dimension_id'] != 0 && dimension_is_closed($_POST['dimension_id'])) 
	{
		display_error(_("Dimension is closed."));
		set_focus('dimension_id');
		return false;
	}

	if (isset($_POST['dimension2_id']) && $_POST['dimension2_id'] != 0 && dimension_is_closed($_POST['dimension2_id'])) 
	{
		display_error(_("Dimension is closed."));
		set_focus('dimension2_id');
		return false;
	}

	if (strlen($_POST['AmountDebit']) && !check_num('AmountDebit', 0)) 
	{
    		display_error(_("The debit amount entered is not a valid number or is less than zero."));
		set_focus('AmountDebit');
    		return false;
  	} elseif (strlen($_POST['AmountCredit']) && !check_num('AmountCredit', 0))
	{
    		display_error(_("The credit amount entered is not a valid number or is less than zero."));
		set_focus('AmountCredit');
    		return false;
  	}
	
	if (!is_tax_gl_unique(get_post('code_id'))) {
   		display_error(_("Cannot post to GL account used by more than one tax type."));
		set_focus('code_id');
   		return false;
	}
	
   	return true;
}

//-----------------------------------------------------------------------------------------------

function handle_update_item()
{
    if($_POST['UpdateItem'] != "" && check_item_data())
    {
		$stock_det=get_item($_POST['stock_id']);
		$id = $_POST['LineNo'];
    	if (!isset($_POST['std_cost']))
    		$_POST['std_cost'] = $_SESSION['transfer_items']->line_items[$id]->standard_cost;
			$serial_complete=1;
		if($stock_det['serialize']){
		
				$serials=explode("\n",$_POST['serial']);
					$serials=array_unique($serials);
					$serial_count=0;
							foreach($serials as $s)
							if($s){ 
								$serial_count++; 
								if(!check_exist_isn($_POST['stock_id'], $_POST['FromStockLocation'],$s)){
										$serial_complete = 2;
									}
							}
				$serials=implode("\n",$serials);
				
				//$_POST['serial'] = $serials;

				if (input_num('qty') > $serial_count)
				{
					$serial_complete = 0;
				}
				}
		if(!$serial_complete)
			display_error(_("Please complete the serial number entry. Duplicate values will be automatically removed."));
		else if($serial_complete==2)
			display_error(_("One or more entered serial number is invalid."));
		else
    	$_SESSION['transfer_items']->update_cart_item($id, input_num('qty'), $_POST['std_cost'],$serials);
    }
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------

function handle_delete_item($id)
{
	$_SESSION['transfer_items']->remove_from_cart($id);
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------

function handle_new_item()
{
	if (!check_item_data())
		return;
	if (!isset($_POST['std_cost']))
   		$_POST['std_cost'] = 0;
	add_to_order($_SESSION['transfer_items'], $_POST['stock_id'], input_num('qty'), $_POST['std_cost']);
	
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------
$id = find_submit('Delete');
if ($id != -1)
	handle_delete_item($id);
	
if (isset($_POST['AddItem']))
	handle_new_item();

if (isset($_POST['UpdateItem']))
	handle_update_item();

if (isset($_POST['CancelItemChanges'])) {
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------

function line_start_focus_gl() {
  global 	$Ajax;

  $Ajax->activate('gl_table');
  set_focus('_code_id_edit2');
}	

function handle_update_item_gl()
{
	if($_POST['UpdateItem_gl'] != "" && check_item_data())
    {
    	if (input_num('AmountDebit') > 0)
    		$amount = input_num('AmountDebit');
    	else
    		$amount = -input_num('AmountCredit');

    	$_SESSION['transfer_items']->update_gl_item($_POST['Index'], $_POST['code_id'], 
    	    $_POST['dimension_id'], $_POST['dimension2_id'], $amount, $_POST['LineMemo']);
    }
	line_start_focus_gl();
}

function handle_new_item_gl()
{
	if (!check_item_data())
		return;

	if (input_num('AmountDebit') > 0)
		$amount = input_num('AmountDebit');
	else
		$amount = -input_num('AmountCredit');
	
	$_SESSION['transfer_items']->add_gl_item($_POST['code_id'], $_POST['dimension_id'],
		$_POST['dimension2_id'], $amount, $_POST['LineMemo']);
	line_start_focus_gl();
}

function handle_delete_item_gl($id)
{
	$_SESSION['transfer_items']->remove_gl_item($id);
	line_start_focus_gl();
}

$id2 = find_submit('Delete2');

if ($id2 != -1)
	handle_delete_item_gl($id2);

if (isset($_POST['AddItem_gl']))
	handle_new_item_gl();

if (isset($_POST['UpdateItem_gl']))
	handle_update_item_gl();

if (isset($_POST['CancelItemChanges_gl']))
	line_start_focus_gl();
	
	
//-----------------------------------------------------------------------------------------------

if (isset($_GET['NewTransfer']) || !isset($_SESSION['transfer_items']))
{
	handle_new_order();
}

//-----------------------------------------------------------------------------------------------
start_form();

display_order_header($_SESSION['transfer_items']);

start_table("$table_style width=70%", 10);
start_row();
echo "<td>";
display_transfer_items(_("Items"), $_SESSION['transfer_items']);

echo "<br>";
display_gl_items(_("GL Items for this Requisition"), $_SESSION['transfer_items']);


transfer_options_controls();
echo "</td>";
end_row();
end_table(1);

submit_center_first('Update', _("Update"), '', null);
submit_center_last('Process', _("Process Requisition"), '',  'default');

end_form();
end_page();

?>
