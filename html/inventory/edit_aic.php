<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_AICINQUIRY';
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/items_cart.inc");
include_once($path_to_root . "/includes/db/isn_db.inc");

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/inventory/includes/item_adjustments_ui.inc");
include_once($path_to_root . "/inventory/includes/inventory_db.inc");
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "ISN"), false, false, "", $js);

//---------------------------------------------------------------------------------------------------------------

function changeOrder($id,$action){
	
	global $Ajax;
	$input_error = 0;

	// if (strlen($_POST['new_aic']) == 0 && $_POST['update'] == 0) 
	// {
		// $input_error = 1;
		// display_error(_("AIC cannot be empty."));
		// set_focus('new_aic');
	// }
	
	if ($input_error != 1)
	{
		$sql = "UPDATE ".TB_PREF."isn_gen
				SET aic = ".db_escape($_POST['new_aic'])."
				WHERE isn = ".db_escape($_POST['isn'])."	
				AND stock_id = ".db_escape($_POST['stock_id'])."
				AND loc_code = ".db_escape($_POST['loc_code']);		
		db_query($sql, "could not update isn gen");
				
		display_notification(_("AIC Updated"));
		$Ajax->activate('orders_tbl');
		meta_forward("../inventory/inquiry/aic_inquiry.php");
	}
	
}

$checkaction=find_submit('Update');
if($checkaction!=-1)
changeOrder($checkaction,1);

//---------------------------------------------------------------------------------------------------------------

start_form();
div_start('orders_tbl');

if(isset($_GET['isn']))
	$_POST['isn'] = $_GET['isn'];
	
if(isset($_GET['stock_id']))
	$_POST['stock_id'] = $_GET['stock_id'];
	
if(isset($_GET['loc_code']))
	$_POST['loc_code'] = $_GET['loc_code'];

hidden('isn', $_POST['isn']);
		
$sql = "SELECT gen.isn,
		gen.stock_id,
		stock.description,
		aic, gen.inactive,
		gen.loc_code
FROM ".TB_PREF."isn_gen as gen, ".TB_PREF."stock_master as stock
WHERE gen.stock_id = stock.stock_id 
AND gen.isn = ".db_escape($_POST['isn'])."	
AND gen.stock_id = ".db_escape($_POST['stock_id'])."
AND gen.loc_code = ".db_escape($_POST['loc_code']);	
		
$result = db_query($sql, "could not get isn");
$myrow = db_fetch($result);

$_POST['inactive'] = $myrow['inactive'];	

start_table($table_style2);

	label_row(_("ISN:"), $myrow['isn'], "class='tableheader2'");
	label_row(_("Item Code:"), $myrow['stock_id'], "class='tableheader2'");
	label_row(_("Description:"), $myrow['description'], "class='tableheader2'");
	
	text_row(_("AIC:"), 'new_aic', $myrow['aic'], 42, 40, null, "class='tableheader2'");
	
	hidden('stock_id', $myrow['stock_id']);
	hidden('loc_code', $myrow['loc_code']);

end_table(1);	

// if($myrow['aic'] == null)
	submit_center('Update'.$myrow['id'], _("Update"), true, false, 'update');

div_end();

end_form();

//--------------------------------------------------------------------------------------------------

end_page();
?>

