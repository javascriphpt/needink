<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_AICINQUIRY';
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/aic_items_cart.inc");

include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/db/isn_db.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/inventory/includes/inventory_db.inc");
include_once($path_to_root . "/inventory/includes/aic_ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "ISN"), false, false, "", $js);

//---------------------------------------------------------------------------------------------------------------

function changeOrder($id,$action){
	
	global $Ajax;
	$input_error = 0;

	// if (strlen($_POST['new_aic']) == 0 && $_POST['update'] == 0) 
	// {
		// $input_error = 1;
		// display_error(_("AIC cannot be empty."));
		// set_focus('new_aic');
	// }
	
	if ($input_error != 1)
	{
		if($_POST['gl_trans'] == 0)
		{
			global $Refs;
						
			$cart = &$_SESSION['journal_items'];
			$cart->reference = $Refs->get_next(ST_JOURNAL);
			$cart->memo_ = "ITEM:".$_POST['stock_id']." ISN:".$_POST['isn']." AIC:".$_POST['new_aic'];
			$cart->tran_date = Today();
			$cart->trans_type = ST_JOURNAL;

			$trans_no = write_journal_entries($cart, check_value('Reverse'));

			$cart->clear_items();
			unset($_SESSION['journal_items']);
			
		}
		else
			$trans_no = 0;
	
		$sql = "UPDATE ".TB_PREF."isn_gen
				SET aic = ".db_escape($_POST['new_aic']).", gl_trans = ".db_escape($trans_no)."
				WHERE isn = ".db_escape($_POST['isn'])."	
				AND stock_id = ".db_escape($_POST['stock_id'])."
				AND loc_code = ".db_escape($_POST['loc_code']);		
		db_query($sql, "could not update isn gen");
				
		display_notification(_("AIC Updated"));
		$Ajax->activate('orders_tbl');
		meta_forward("../inventory/inquiry/aic_inquiry.php");
	}
	
}

$checkaction=find_submit('Update');
if($checkaction!=-1)
changeOrder($checkaction,1);

//---------------------------------------------------------------------------------------------------------------

start_form();
div_start('orders_tbl');

if(isset($_GET['isn']))
	$_POST['isn'] = $_GET['isn'];
	
if(isset($_GET['stock_id']))
	$_POST['stock_id'] = $_GET['stock_id'];
	
if(isset($_GET['loc_code']))
	$_POST['loc_code'] = $_GET['loc_code'];

hidden('isn', $_POST['isn']);
		
$sql = "SELECT gen.isn,
		gen.stock_id,
		stock.description,
		aic, gen.inactive,
		gen.loc_code,
		gen.gl_trans
FROM ".TB_PREF."isn_gen as gen, ".TB_PREF."stock_master as stock
WHERE gen.stock_id = stock.stock_id 
AND gen.isn = ".db_escape($_POST['isn'])."	
AND gen.stock_id = ".db_escape($_POST['stock_id'])."
AND gen.loc_code = ".db_escape($_POST['loc_code']);	
		
$result = db_query($sql, "could not get isn");
$myrow = db_fetch($result);

$_POST['inactive'] = $myrow['inactive'];	

//--------------------------------------------------------------------------------------------------\

function line_start_focus() {
  global 	$Ajax;

  $Ajax->activate('items_table');
  set_focus('_code_id_edit');
}

function handle_new_order()
{
	if (isset($_SESSION['journal_items']))
	{
		unset ($_SESSION['journal_items']);
	}
	$_SESSION['journal_items'] = new items_cart(ST_JOURNAL);
}

function check_item_data()
{
	if (isset($_POST['dimension_id']) && $_POST['dimension_id'] != 0 && dimension_is_closed($_POST['dimension_id'])) 
	{
		display_error(_("Dimension is closed."));
		set_focus('dimension_id');
		return false;
	}

	if (isset($_POST['dimension2_id']) && $_POST['dimension2_id'] != 0 && dimension_is_closed($_POST['dimension2_id'])) 
	{
		display_error(_("Dimension is closed."));
		set_focus('dimension2_id');
		return false;
	}

	if (!(input_num('AmountDebit')!=0 ^ input_num('AmountCredit')!=0) )
	{
		display_error(_("You must enter either a debit amount or a credit amount."));
		set_focus('AmountDebit');
    		return false;
  	}

	if (strlen($_POST['AmountDebit']) && !check_num('AmountDebit', 0)) 
	{
    		display_error(_("The debit amount entered is not a valid number or is less than zero."));
		set_focus('AmountDebit');
    		return false;
  	} elseif (strlen($_POST['AmountCredit']) && !check_num('AmountCredit', 0))
	{
    		display_error(_("The credit amount entered is not a valid number or is less than zero."));
		set_focus('AmountCredit');
    		return false;
  	}
	
	if (!is_tax_gl_unique(get_post('code_id'))) {
   		display_error(_("Cannot post to GL account used by more than one tax type."));
		set_focus('code_id');
   		return false;
	}

	if (!$_SESSION["wa_current_user"]->can_access('SA_BANKJOURNAL') && is_bank_account($_POST['code_id'])) 
	{
		display_error(_("You cannot make a journal entry for a bank account. Please use one of the banking functions for bank transactions."));
		set_focus('code_id');
		return false;
	}

   	return true;
}

function handle_update_item()
{
    if($_POST['UpdateItem'] != "" && check_item_data())
    {
    	if (input_num('AmountDebit') > 0)
    		$amount = input_num('AmountDebit');
    	else
    		$amount = -input_num('AmountCredit');

    	$_SESSION['journal_items']->update_gl_item($_POST['Index'], $_POST['code_id'], 
    	    $_POST['dimension_id'], $_POST['dimension2_id'], $amount, $_POST['LineMemo']);
    }
	line_start_focus();
}

function handle_delete_item($id)
{
	$_SESSION['journal_items']->remove_gl_item($id);
	line_start_focus();
}

function handle_new_item()
{
	if (!check_item_data())
		return;

	if (input_num('AmountDebit') > 0)
		$amount = input_num('AmountDebit');
	else
		$amount = -input_num('AmountCredit');
	
	$_SESSION['journal_items']->add_gl_item($_POST['code_id'], $_POST['dimension_id'],
		$_POST['dimension2_id'], $amount, $_POST['LineMemo']);
	line_start_focus();
}

$id = find_submit('Delete');
if ($id != -1)
	handle_delete_item($id);

if (isset($_POST['AddItem'])) 
	handle_new_item();

if (isset($_POST['UpdateItem'])) 
	handle_update_item();
	
if (isset($_POST['CancelItemChanges']))
	line_start_focus();
	
if (isset($_GET['New']) || !isset($_SESSION['journal_items']))
	handle_new_order();

//--------------------------------------------------------------------------------------------------

start_table($table_style2);

	label_row(_("ISN:"), $myrow['isn'], "class='tableheader2'");
	label_row(_("Item Code:"), $myrow['stock_id'], "class='tableheader2'");
	label_row(_("Description:"), $myrow['description'], "class='tableheader2'");
	
	text_row(_("AIC:"), 'new_aic', $myrow['aic'], 42, 40, null, "class='tableheader2'");
	
	hidden('stock_id', $myrow['stock_id']);
	hidden('loc_code', $myrow['loc_code']);
	hidden('gl_trans', $myrow['gl_trans']);
	
end_table(1);	

if($myrow['gl_trans'] == 0)
{
	start_table("$table_style2 width=90%", 10);
	start_row();
	echo "<td>";
	display_gl_items(_("GL Items for this AIC"), $_SESSION['journal_items']);
	echo "</td>";
	end_row();
	end_table(2);
}
else
{
	/*show a table of the transactions returned by the sql */
	$dim = get_company_pref('use_dimension');

	if ($dim == 2)
		$th = array(_("Account Code"), _("Account Name"), _("Dimension")." 1", _("Dimension")." 2",
			_("Debit"), _("Credit"), _("Memo"));
	else if ($dim == 1)
		$th = array(_("Account Code"), _("Account Name"), _("Dimension"),
			_("Debit"), _("Credit"), _("Memo"));
	else		
		$th = array(_("Account Code"), _("Account Name"),
			_("Debit"), _("Credit"), _("Memo"));
	$k = 0; //row colour counter
	$heading_shown = false;
	
	br();
	display_heading("GL Items for this AIC");
	
	start_table("$table_style width=50%");
    table_header($th);	
	
	$sql001 = "SELECT gl.*, cm.account_name, IF(ISNULL(refs.reference), '', refs.reference) AS reference FROM "
		.TB_PREF."gl_trans as gl
		LEFT JOIN ".TB_PREF."chart_master as cm ON gl.account = cm.account_code
		LEFT JOIN ".TB_PREF."refs as refs ON (gl.type=refs.type AND gl.type_no=refs.id)"
		." WHERE gl.type= ".db_escape(ST_JOURNAL) 
		." AND gl.type_no = ".db_escape($myrow['gl_trans'])
		." ORDER BY amount DESC, counter";
	$res = db_query($sql001,"could not get transactions");		

	while ($row = db_fetch($res)) 
	{
		if ($row['amount'] == 0) continue;
	
		alt_table_row_color($k);
		
		label_cell($row['account']);
		label_cell($row['account_name']);
		if ($dim >= 1)
			label_cell(get_dimension_string($row['dimension_id'], true));
		if ($dim > 1)
			label_cell(get_dimension_string($row['dimension2_id'], true));

		display_debit_or_credit_cells($row['amount']);
		label_cell($row['memo_']);
		end_row();

	}
	
	comments_display_row(ST_JOURNAL, $myrow['gl_trans']);
	
	end_table(3);
}

// if($myrow['aic'] == null)
	submit_center('Update', _("Update"), true, false, 'update');

div_end();

end_form();

//--------------------------------------------------------------------------------------------------

end_page();
?>

