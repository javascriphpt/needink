<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/ui/items_cart.inc");

//--------------------------------------------------------------------------------

function add_to_order(&$order, $new_item, $new_item_qty, $standard_cost)
{
    if ($order->find_cart_item($new_item))
         display_error(_("For Part :") . $new_item . " " . "This item is already on this order.  You can change the quantity ordered of the existing line if necessary.");
	else{
		$serial_complete=1;
			$stock_det=get_item($new_item);
			if($stock_det['serialize']){
			$serials=explode("\n",$_POST['serial']);
			$serials=array_unique($serials);
			$serial_count=0;
					foreach($serials as $s)
					if($s){ 
						$serial_count++; 
						if(!check_exist_isn($new_item, $_POST['StockLocation'],$s) && !$_POST['Increase']){
								$serial_complete = 2;
							}
					}
			$serials=implode("\n",$serials);
			
			//$_POST['serial'] = $serials;

			if ($new_item_qty > $serial_count)
			{
				$serial_complete = 0;
			
			}
			}	
					
				
			if(!$serial_complete)
				display_error(_("Please complete the serial number entry. Duplicate values will be automatically removed."));
			else if($serial_complete==2)
				display_error(_("One or more entered serial number is invalid."));
			else
			$order->add_to_cart (count($order->line_items), $new_item, $new_item_qty, $standard_cost,null,$serials);
	 }
	// else
         // $order->add_to_cart (count($order->line_items), $new_item, $new_item_qty, $standard_cost);
}

//--------------------------------------------------------------------------------

function display_order_header(&$order)
{
	global $table_style2, $Refs;

	start_outer_table("width=70% $table_style2"); // outer table
	table_section(1);
	
	if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
		locations_list_cells_user(_("Location:"), 'StockLocation', null, false, true, $_SESSION["wa_current_user"]->default_location);
	else
		locations_list_row(_("Location:"), 'StockLocation', null);
		
	ref_row(_("Inventory Adjustment #:"), 'ref', '', $Refs->get_next(ST_INVADJUST));

	table_section(2, "33%");

    date_row(_("Date:"), 'AdjDate', '', true);

	table_section(3, "33%");

    movement_types_list_row(_("Detail:"), 'type', null);

    if (!isset($_POST['Increase']))
    	$_POST['Increase'] = 1;
    yesno_list_row(_("Type:"), 'Increase', $_POST['Increase'],
    	_("Positive Adjustment"), _("Negative Adjustment"));

	end_outer_table(1); // outer table
}

//---------------------------------------------------------------------------------

function display_adjustment_items($title, &$order)
{
	global $table_style, $path_to_root;

	display_heading($title);
    div_start('items_table');
	start_table("$table_style width=80%");
	$th = array(_("Item Code"), _("Item Description"), _("Quantity"),"Serial No.",
		_("Unit"), _("Unit Cost"), _("Total"), "");
	if ( count($order->line_items)) $th[] = '';

	table_header($th);
	$total = 0;
	$k = 0;  //row colour counter

	$id = find_submit('Edit');
	foreach ($order->line_items as $line_no=>$stock_item)
	{

		$total += ($stock_item->standard_cost * $stock_item->quantity);

		if ($id != $line_no)
		{
    		alt_table_row_color($k);

			view_stock_status_cell($stock_item->stock_id);
			label_cell($stock_item->item_description);
    		qty_cell($stock_item->quantity, false, get_qty_dec($stock_item->stock_id));
			label_cell($stock_item->serial);
			label_cell($stock_item->units);
			amount_decimal_cell($stock_item->standard_cost);
			amount_cell($stock_item->standard_cost * $stock_item->quantity);

			edit_button_cell("Edit$line_no", _("Edit"),
				_('Edit document line'));
			delete_button_cell("Delete$line_no", _("Delete"),
				_('Remove line from document'));
			end_row();
		}
		else
		{
			adjustment_edit_item_controls($order, $line_no);
		}
	}

	if ($id == -1)
		adjustment_edit_item_controls($order);

	label_row(_("Total"), number_format2($total,user_price_dec()), "align=right colspan=6", "align=right", 2);

    end_table();
	div_end();
}

//---------------------------------------------------------------------------------

function adjustment_edit_item_controls(&$order, $line_no=-1)
{
	global $Ajax;
	start_row();

	$dec2 = 0;
	$id = find_submit('Edit');
	if ($line_no != -1 && $line_no == $id)
	{
		$_POST['stock_id'] = $order->line_items[$id]->stock_id;
		$_POST['qty'] = qty_format($order->line_items[$id]->quantity, 
			$order->line_items[$id]->stock_id, $dec);
		//$_POST['std_cost'] = price_format($order->line_items[$id]->standard_cost);
		$_POST['std_cost'] = price_decimal_format($order->line_items[$id]->standard_cost, $dec2);
		$_POST['units'] = $order->line_items[$id]->units;
		$_POST['serial'] = $order->line_items[$id]->serial;

		hidden('stock_id', $_POST['stock_id']);
		label_cell($_POST['stock_id']);
		label_cell($order->line_items[$id]->item_description, 'nowrap');
	    $Ajax->activate('items_table');
	}
	else
	{
		if($_SESSION["wa_current_user"]->can_access_all_item_categories == 0)
			stock_costable_items_list_cells_user(null, 'stock_id', null, false, true, $_SESSION["wa_current_user"]->default_item_categories);
		else
			stock_costable_items_list_cells(null, 'stock_id', null, false, true);
			
		if (list_updated('stock_id')) {
			    $Ajax->activate('units');
			    $Ajax->activate('qty');
			    $Ajax->activate('std_cost');
		}

    	$item_info = get_item_edit_info($_POST['stock_id']);
		$dec = $item_info['decimals'];
   		$_POST['qty'] = number_format2(0, $dec);
		//$_POST['std_cost'] = price_format($item_info["standard_cost"]);
		$_POST['std_cost'] = price_decimal_format($item_info["standard_cost"], $dec2);
		$_POST['units'] = $item_info["units"];
	}

	qty_cells(null, 'qty', $_POST['qty'], null, null, $dec);
	
	textarea_cells("","serial",$_POST['serial'],20,3);
	label_cell($_POST['units'], '', 'units');

	//amount_cells(null, 'std_cost', $_POST['std_cost']);
	amount_cells(null, 'std_cost', null, null, null, $dec2);
	label_cell("&nbsp;");

	if ($id != -1)
	{
		button_cell('UpdateItem', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
		button_cell('CancelItemChanges', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
		hidden('LineNo', $line_no);
 		set_focus('qty');
	}
	else
	{
		submit_cells('AddItem', _("Add Item"), "colspan=2",
		    _('Add new item to document'), true);
	}

	end_row();
}


//---------------------------------------------------------------------------------

function adjustment_options_controls()
{
	  echo "<br>";
	  start_table();

	  textarea_row(_("Memo"), 'memo_', null, 50, 3);

	  end_table(1);
}


//---------------------------------------------------------------------------------

?>