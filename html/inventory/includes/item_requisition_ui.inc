<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/ui/items_cart.inc");

//--------------------------------------------------------------------------------

function add_to_order(&$order, $new_item, $new_item_qty, $standard_cost)
{
    if ($order->find_cart_item($new_item))
         display_error(_("For Part :") . $new_item . " " . "This item is already on this order.  You can change the quantity ordered of the existing line if necessary.");
	else{
		// $serial_complete=1;
			// $stock_det=get_item($new_item);
			// if($stock_det['serialize']){
			// $serials=explode("\n",$_POST['serial']);
			// $serials=array_unique($serials);
			// $serial_count=0;
					// foreach($serials as $s)
					// if($s){ 
						// $serial_count++; 
						// if(!check_exist_isn($new_item, $_POST['FromStockLocation'],$s)){
								// $serial_complete = 2;
							// }
					// }
		// $serials=implode("\n",$serials);
		
		// //$_POST['serial'] = $serials;

	  	// if ($new_item_qty > $serial_count)
	  	// {
			// $serial_complete = 0;
		
	  	// }
		// }	
				
			
		// if(!$serial_complete)
			// display_error(_("Please complete the serial number entry. Duplicate values will be automatically removed."));
		// else if($serial_complete==2)
			// display_error(_("One or more entered serial number is invalid."));
		// else
         $order->add_to_cart (count($order->line_items), $new_item, $new_item_qty, $standard_cost,null,'');
	}
}

//--------------------------------------------------------------------------------

function display_order_header(&$order)
{
	global $table_style, $Refs;

	start_outer_table("width=70% $table_style");

	table_section(1);
	// label_row(_("From Location:"), get_item_location_name('CEB'));
	hidden('FromStockLocation', 'CEB');	
		
	locations_list_row(_("To Location:"), 'ToStockLocation', null, "");
	ref_row(_("Requisition Slip #:"), 'ref', '', $Refs->get_next(ST_REQUISITIONSLIP));

	table_section(2, "50%");
    date_row(_("Date:"), 'AdjDate', '', true);
    date_row(_("Required Delivery Date:"), 'delivery_date', '', true);
	
	hidden('type', '1');	

	end_outer_table(1); // outer table
}

//---------------------------------------------------------------------------------

function display_transfer_items($title, &$order)
{
	global $table_style, $path_to_root;

	display_heading($title);
    div_start('items_table');
	start_table("$table_style width=80%");
	$th = array(_("Item Code"), _("Item Description"), _("Quantity"), _("Unit"), '');
	if ( count($order->line_items)) $th[] = '';
	table_header($th);
	$subtotal = 0;
	$k = 0;  //row colour counter

	$id = find_submit('Edit');
	foreach ($order->line_items as $line_no=>$stock_item)
	{

		if ($id != $line_no)
		{
    		alt_table_row_color($k);

			view_stock_status_cell($stock_item->stock_id);
    		label_cell($stock_item->item_description);
    		qty_cell($stock_item->quantity, false, get_qty_dec($stock_item->stock_id));
    		label_cell($stock_item->units);

			edit_button_cell("Edit$line_no", _("Edit"),
				_('Edit document line'));
			delete_button_cell("Delete$line_no", _("Delete"),
				_('Remove line from document'));
    		end_row();
		}
		else
		{
			transfer_edit_item_controls($order, $line_no);
		}
	}

	if ($id == -1)
		transfer_edit_item_controls($order);

    end_table();
	div_end();
}

//---------------------------------------------------------------------------------

function transfer_edit_item_controls(&$order, $line_no=-1)
{
	global $Ajax;
	start_row();

	$id = find_submit('Edit');
	if ($line_no != -1 && $line_no == $id)
	{	
	//	$stock_det=get_item($order->line_items[$id]->stock_id);
		$_POST['stock_id'] = $order->line_items[$id]->stock_id;
		$_POST['qty'] = qty_format($order->line_items[$id]->quantity, $order->line_items[$id]->stock_id, $dec);
		$_POST['units'] = $order->line_items[$id]->units;

		hidden('stock_id', $_POST['stock_id']);
		label_cell($_POST['stock_id']);
		label_cell($order->line_items[$id]->item_description);
	    $Ajax->activate('items_table');
	}
	else
	{
		// $stock_det=get_item($_POST['stock_id']);
    	if($_SESSION["wa_current_user"]->can_access_all_item_categories == 0)
			stock_costable_items_list_cells_user(null, 'stock_id', null, false, true, $_SESSION["wa_current_user"]->default_item_categories);
		else
			stock_costable_items_list_cells(null, 'stock_id', null, false, true);
			
		if (list_updated('stock_id')) {
				$stock_det=get_item($_POST['stock_id']);
			    $Ajax->activate('units');
			    $Ajax->activate('qty');
		}

    	$item_info = get_item_edit_info($_POST['stock_id']);

		$dec = $item_info['decimals'];
   		$_POST['qty'] = number_format2(1, $dec);
		$_POST['units'] = $item_info["units"];
	//	$serial=$stock_det['serialize'];
	}

	small_qty_cells(null, 'qty', $_POST['qty'], null, null, $dec);
		
	label_cell($_POST['units'], '', 'units');

	if ($id != -1)
	{
		button_cell('UpdateItem', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
		button_cell('CancelItemChanges', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
		hidden('LineNo', $line_no);
 		set_focus('qty');
	}
	else
	{
		submit_cells('AddItem', _("Add Item"), "colspan=2",
		    _('Add new item to document'), true);
	}

	end_row();
}


//---------------------------------------------------------------------------------

function transfer_options_controls()
{
	  echo "<br>";
	  start_table();

	  textarea_row(_("Memo"), 'memo_', null, 50, 3);

	  end_table(1);
}

//---------------------------------------------------------------------------------

// function display_gl_items($title, &$order)
// {
	// global $table_style, $path_to_root;

	// $dim = get_company_pref('use_dimension');
	// $colspan = ($dim == 2 ? 4 : ($dim == 1 ? 3 : 2));
	// display_heading($title);

    // div_start('gl_table');
	// start_table("$table_style colspan=7 width=95%");

	// if ($dim == 2)
		// $th = array(_("Account Code"), _("Account Description"), _("Dimension")." 1",
			// _("Dimension")." 2", _("Amount"), _("Memo"), "");
	// else if ($dim == 1)
		// $th = array(_("Account Code"), _("Account Description"), _("Dimension"),
			// _("Amount"), _("Memo"), "");
	// else
		// $th = array(_("Account Code"), _("Account Description"),
			// _("Amount"), _("Memo"), "");

	// if (count($order->gl_items)) $th[] = '';

	// table_header($th);
	// $k = 0;  //row colour counter

	// $id = find_submit('Edit2');
	// foreach ($order->gl_items as $line => $item)
	// {
		// if ($id != $line)
		// {
    		// alt_table_row_color($k);

			// label_cell($item->code_id);
			// label_cell($item->description);
    		// if ($dim >= 1)
				// label_cell(get_dimension_string($item->dimension_id, true));
    		// if ($dim > 1)
				// label_cell(get_dimension_string($item->dimension2_id, true));
			// //amount_cell(abs($item->amount));
			// if ($order->trans_type == ST_BANKDEPOSIT)
				// amount_cell(-$item->amount);
			// else		
				// amount_cell($item->amount);
			// label_cell($item->reference);

			// edit_button_cell("Edit2$line", _("Edit2"),
				// _('Edit document line'));
			// delete_button_cell("Delete2$line", _("Delete2"),
				// _('Remove line from document'));
    		// end_row();
		// }
		// else
		// {
			// gl_edit_item_controls($order, $dim, $line);
		// }
	// }

	// if ($id == -1)
		// gl_edit_item_controls($order, $dim);

	// if ($order->count_gl_items())
		// label_row(_("Total"), number_format2(abs($order->gl_items_total()), user_price_dec()),"colspan=" . $colspan . " align=right", "align=right",3);

    // end_table();
	// div_end();
// }

// //---------------------------------------------------------------------------------

// function gl_edit_item_controls(&$order, $dim, $Index=null)
// {
	// global $Ajax;
	// $payment = $order->trans_type == ST_BANKPAYMENT;

	// start_row();
	// $id = find_submit('Edit2');
	// if ($Index != -1 && $Index == $id)
	// {
		// $item = $order->gl_items[$Index];
		// $_POST['code_id'] = $item->code_id;
		// $_POST['dimension_id'] = $item->dimension_id;
		// $_POST['dimension2_id'] = $item->dimension2_id;
		// $_POST['amount'] = price_format(abs($item->amount));
		// $_POST['description'] = $item->description;
		// $_POST['LineMemo'] = $item->reference;

		// hidden('Index', $id);
		// echo gl_all_accounts_list('code_id', null, true, true);
		// if ($dim >= 1)
			// dimensions_list_cells(null, 'dimension_id', null, true, " ", false, 1);
		// if ($dim > 1)
			// dimensions_list_cells(null, 'dimension2_id', null, true, " ", false, 2);
	    // $Ajax->activate('gl_table');
	// }
	// else
	// {
		// $_POST['amount'] = price_format(0);
		// $_POST['dimension_id'] = 0;
		// $_POST['dimension2_id'] = 0;
		// $_POST['LineMemo'] = "";
		// if(isset($_POST['_code_id_update'])) {
			    // $Ajax->activate('code_id');
		// }

		// $_POST['code_id'] = get_company_pref($payment ? 'default_cogs_act':'default_inv_sales_act');
		
		// echo gl_all_accounts_list('code_id', null, true, true);
		// if ($dim >= 1)
			// dimensions_list_cells(null, 'dimension_id', null, true, " ", false, 1);
		// if ($dim > 1)
			// dimensions_list_cells(null, 'dimension2_id', null, true, " ", false, 2);
	// }
	// if ($dim < 1)
		// hidden('dimension_id', 0);
	// if ($dim < 2)
		// hidden('dimension2_id', 0);

	// amount_cells(null, 'amount');
	// text_cells_ex(null, 'LineMemo', 35, 255);

	// if ($id != -1)
	// {
		// button_cell('UpdateItem_gl', _("Update"),
				// _('Confirm changes'), ICON_UPDATE);
		// button_cell('CancelItemChanges_gl', _("Cancel"),
				// _('Cancel changes'), ICON_CANCEL);
 		// set_focus('amount');
	// }
	// else
	// {
		// submit_cells('AddItem_gl', _("Add Item"), "colspan=2",
		    // _('Add new item to document'), true);
	// }

	// end_row();
// }


function display_gl_items($title, &$order)
{
	global $table_style, $path_to_root;

	display_heading($title);

	$dim = get_company_pref('use_dimension');

    div_start('gl_table');
	start_table("$table_style colspan=7 width=95%");
	if ($dim == 2)
		$th = array(_("Account Code"), _("Account Description"), _("Dimension")." 1",
			_("Dimension")." 2", _("Debit"), _("Credit"), _("Memo"), "");
	else if ($dim == 1)
		$th = array(_("Account Code"), _("Account Description"), _("Dimension"),
			_("Debit"), _("Credit"), _("Memo"), "");
	else
		$th = array(_("Account Code"), _("Account Description"),
			_("Debit"), _("Credit"), _("Memo"), "");

	if (count($order->gl_items)) $th[] = '';

	table_header($th);	

	$k = 0;

	$id = find_submit('Edit2');
	foreach ($order->gl_items as $line => $item) 
	{
		if ($id != $line)
		{
    		alt_table_row_color($k);

			label_cells($item->code_id, $item->description);
    		if ($dim >= 1)
   				label_cell(get_dimension_string($item->dimension_id, true));
    		if ($dim > 1)
   				label_cell(get_dimension_string($item->dimension2_id, true));
    		if ($item->amount > 0)
    		{
    			amount_cell(abs($item->amount));
    			label_cell("");
    		}	
    		else
    		{
    			label_cell("");
    			amount_cell(abs($item->amount));
    		}	
			label_cell($item->reference);

			edit_button_cell("Edit2$line", _("Edit2"),
				_('Edit journal line'));
			delete_button_cell("Delete2$line", _("Delete2"),
				_('Remove line from journal'));
    		end_row();
		} 
		else 
		{
			gl_edit_item_controls($order, $dim, $line);
		}
	}

	if ($id == -1)
		gl_edit_item_controls($order, $dim);

	if ($order->count_gl_items()) 
	{
		$colspan = ($dim == 2 ? "4" : ($dim == 1 ? "3" : "2"));
		start_row();
		label_cell(_("Total"), "align=right colspan=" . $colspan);
		amount_cell($order->gl_items_total_debit());
		amount_cell(abs($order->gl_items_total_credit()));
		label_cell('', "colspan=3");
		end_row();
	}

    end_table();
	div_end();
}

//---------------------------------------------------------------------------------

function gl_edit_item_controls(&$order, $dim, $Index=null)
{
	global $Ajax;
	start_row();

	$id = find_submit('Edit2');
	if ($Index != -1 && $Index == $id)
	{
	    // Modifying an existing row
		$item = $order->gl_items[$Index];
		$_POST['code_id'] = $item->code_id;
		$_POST['dimension_id'] = $item->dimension_id;
		$_POST['dimension2_id'] = $item->dimension2_id;
		if ($item->amount > 0)
		{
			$_POST['AmountDebit'] = price_format($item->amount);
			$_POST['AmountCredit'] = "";
		}
		else
		{
			$_POST['AmountDebit'] = "";
			$_POST['AmountCredit'] = price_format(abs($item->amount));
		}	
		$_POST['description'] = $item->description;
		$_POST['LineMemo'] = $item->reference;

		hidden('Index', $id);
		$skip_bank = !$_SESSION["wa_current_user"]->can_access('SA_BANKJOURNAL');
		echo gl_all_accounts_list('code_id', null, $skip_bank, true);
		if ($dim >= 1) 
			dimensions_list_cells(null, 'dimension_id', null, true, " ", false, 1);
		if ($dim > 1) 
			dimensions_list_cells(null, 'dimension2_id', null, true, " ", false, 2);
	    $Ajax->activate('gl_table');
	}
	else
	{
	    // Adding a new row
		$_POST['AmountDebit'] = '';  //price_format(0);
		$_POST['AmountCredit'] = ''; //price_format(0);
		$_POST['dimension_id'] = 0;
		$_POST['dimension2_id'] = 0;
		$_POST['LineMemo'] = "";
		$_POST['_code_id_edit'] = "";
		$_POST['code_id'] = "";
		if(isset($_POST['_code_id_update'])) {
			    $Ajax->activate('code_id');
		}
		
		$skip_bank = !$_SESSION["wa_current_user"]->can_access('SA_BANKJOURNAL');
		echo gl_all_accounts_list('code_id', null, $skip_bank, true);
		if ($dim >= 1)
			dimensions_list_cells(null, 'dimension_id', null, true, " ", false, 1);
		if ($dim > 1)
			dimensions_list_cells(null, 'dimension2_id', null, true, " ", false, 2);
	}
	if ($dim < 1)
		hidden('dimension_id', 0);
	if ($dim < 2)
		hidden('dimension2_id', 0);

	small_amount_cells(null, 'AmountDebit');
	small_amount_cells(null, 'AmountCredit');
	text_cells_ex(null, 'LineMemo', 35, 255);

	if ($id != -1)
	{
		button_cell('UpdateItem_gl', _("Update"),
				_('Confirm changes'), ICON_UPDATE);
		button_cell('CancelItemChanges_gl', _("Cancel"),
				_('Cancel changes'), ICON_CANCEL);
 		set_focus('amount');
	} 
	else 
		submit_cells('AddItem_gl', _("Add Item"), "colspan=2",
		    _('Add new line to journal'), true);

	end_row();
}

//---------------------------------------------------------------------------------

function gl_options_controls()
{
	  echo "<br><table align='center'>";

	  textarea_row(_("Memo"), 'memo_', null, 50, 3);

	  echo "</table>";
}

//---------------------------------------------------------------------------------

?>