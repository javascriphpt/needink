<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
function add_brand($name)
{
	$sql = "INSERT INTO ".TB_PREF."brands (name)
		VALUES (".db_escape($name).")";

	db_query($sql, "could not add brand");
}

function update_brand($type_id, $name)
{
	$sql = "UPDATE ".TB_PREF."brands SET name=".db_escape($name)."
			WHERE id=".db_escape($type_id);

	db_query($sql, "could not update brand");
}

function get_all_brand($all=false)
{
	$sql = "SELECT * FROM ".TB_PREF."brands";
	if (!$all) $sql .= " WHERE !inactive";

	return db_query($sql, "could not get all brand");
}

function get_brand($type_id)
{
	$sql = "SELECT * FROM ".TB_PREF."brands WHERE id=".db_escape($type_id);

	$result = db_query($sql, "could not get brand");

	return db_fetch($result);
}

function delete_brand($type_id)
{
	$sql="DELETE FROM ".TB_PREF."brands WHERE id=".db_escape($type_id);

	db_query($sql, "could not delete brand");
}

?>