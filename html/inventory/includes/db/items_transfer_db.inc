<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//-------------------------------------------------------------------------------------------------------------

function add_stock_transfer($Items, $location_from, $location_to, $date_, $type, $reference, $memo_)
{
	global $Refs;

	begin_transaction();

	$transfer_id = get_next_trans_no(ST_TRANSFERSLIP);
	
	foreach ($Items as $line_item)
	{
		add_stock_transfer_item($transfer_id, $line_item->stock_id, $location_from,
			$location_to, $date_, $type, $reference, $line_item->quantity);
			if($line_item->serial){
					$serials=explode("\n",$line_item->serial);
					foreach($serials as $s){
					//change_location($line_item->stock_id,$location_from,$location_to, $s);
					update_isn_status($line_item->stock_id,$location_from, $s,2);
					add_isn_trail($line_item->stock_id, $s,$location_from,$transfer_id,ST_TRANSFERSLIP,$date_,0);
					//add_isn_trail($line_item->stock_id, $s,$location_to,$transfer_id,ST_TRANSFERSLIP,$date_,1);
					}
			}
	}

	add_comments(ST_TRANSFERSLIP, $transfer_id, $date_, $memo_);

	// $Refs->save(ST_LOCTRANSFER, $transfer_id, $reference);
	// add_audit_trail(ST_LOCTRANSFER, $transfer_id, $date_);
	
	$Refs->save(ST_TRANSFERSLIP, $transfer_id, $reference);
	add_audit_trail(ST_TRANSFERSLIP, $transfer_id, $date_);

	commit_transaction();

	return $transfer_id;
}

//-------------------------------------------------------------------------------------------------------------

// add 2 stock_moves entries for a stock transfer
// $date_ is display date (not sql)
// std_cost is in HOME currency
// it seems the standard_cost field is not used at all

function add_stock_transfer_item($transfer_id, $stock_id, $location_from, $location_to,
	$date_, $type, $reference, $quantity)
{
	add_stock_move(ST_TRANSFERSLIP, $stock_id, $transfer_id, $location_from,
       	$date_, $reference, -$quantity, 0, $type);

	// add_stock_move(ST_LOCTRANSFER, $stock_id, $transfer_id, $location_to,
	   	// $date_, $reference, $quantity, 0, $type);
		
	add_stock_move_temp(ST_TRANSFERSLIP, $stock_id, $transfer_id, $location_from,
       	$date_, $reference, -$quantity, 0, $type);

	add_stock_move_temp(ST_TRANSFERSLIP, $stock_id, $transfer_id, $location_to,
	   	$date_, $reference, $quantity, 0, $type);

}

//-------------------------------------------------------------------------------------------------------------

function get_stock_transfer($trans_no)
{
	$result = get_stock_transfer_items($trans_no);
	if (db_num_rows($result) < 2)
	{
		display_db_error("transfer with less than 2 items : $trans_no", "");
	}

	// this function is very bad that it assumes that 1st record and 2nd record contain the
	// from and to locations - if get_stock_moves uses a different ordering than trans_no then
	// it will bomb
	$move1 = db_fetch($result);
	$move2 = db_fetch($result);

	// return an array of (From, To)
	if ($move1['qty'] < 0)
		return array($move1, $move2);
	else
		return array($move2, $move1);
}

function get_stock_transfer_2($trans_no)
{
	$result = get_stock_transfer_items_2($trans_no);
	if (db_num_rows($result) < 2)
	{
		display_db_error("transfer with less than 2 items : $trans_no", "");
	}

	// this function is very bad that it assumes that 1st record and 2nd record contain the
	// from and to locations - if get_stock_moves uses a different ordering than trans_no then
	// it will bomb
	$move1 = db_fetch($result);
	$move2 = db_fetch($result);

	// return an array of (From, To)
	if ($move1['qty'] < 0)
		return array($move1, $move2);
	else
		return array($move2, $move1);
}

//-------------------------------------------------------------------------------------------------------------

function get_stock_transfer_items($trans_no)
{
	$result = get_stock_moves(ST_LOCTRANSFER, $trans_no);

	if (db_num_rows($result) == 0)
	{
		return null;
	}

	return $result;
}

function get_stock_transfer_items_2($trans_no)
{
	$result = get_stock_moves_2(ST_LOCTRANSFER, $trans_no);

	if (db_num_rows($result) == 0)
	{
		return null;
	}

	return $result;
}

//-------------------------------------------------------------------------------------------------------------

function get_stock_transfer_temp($trans_no)
{
	$result = get_stock_transfer_items_temp($trans_no);
	if (db_num_rows($result) < 2)
	{
		display_db_error("transfer with less than 2 items : $trans_no", "");
	}

	// this function is very bad that it assumes that 1st record and 2nd record contain the
	// from and to locations - if get_stock_moves uses a different ordering than trans_no then
	// it will bomb
	$move1 = db_fetch($result);
	$move2 = db_fetch($result);

	// return an array of (From, To)
	if ($move1['qty'] < 0)
		return array($move1, $move2);
	else
		return array($move2, $move1);
}

function get_stock_transfer_temp_2($trans_no)
{
	$result = get_stock_transfer_items_temp_2($trans_no);
	if (db_num_rows($result) < 2)
	{
		display_db_error("transfer with less than 2 items : $trans_no", "");
	}

	// this function is very bad that it assumes that 1st record and 2nd record contain the
	// from and to locations - if get_stock_moves uses a different ordering than trans_no then
	// it will bomb
	$move1 = db_fetch($result);
	$move2 = db_fetch($result);

	// return an array of (From, To)
	if ($move1['qty'] < 0)
		return array($move1, $move2);
	else
		return array($move2, $move1);
}

//-------------------------------------------------------------------------------------------------------------

function get_stock_transfer_items_temp($trans_no)
{
	$result = get_stock_moves_temp(ST_LOCTRANSFER, $trans_no);

	if (db_num_rows($result) == 0)
	{
		return null;
	}

	return $result;
}

function get_stock_transfer_items_temp_2($trans_no)
{
	$result = get_stock_moves_temp(ST_TRANSFERSLIP, $trans_no);

	if (db_num_rows($result) == 0)
	{
		return null;
	}

	return $result;
}

//-------------------------------------------------------------------------------------------------------------

function get_transfer_slip_items($trans_no)
{
	$result = get_stock_moves(ST_TRANSFERSLIP, $trans_no);

	if (db_num_rows($result) == 0)
	{
		return null;
	}

	return $result;
}

//-------------------------------------------------------------------------------------------------------------

function void_stock_transfer($type_no)
{
	void_stock_move(ST_LOCTRANSFER, $type_no);
}

//-------------------------------------------------------------------------------------------------------------

function void_stock_transfer_slip($type_no)
{
	$sql = "DELETE FROM ".TB_PREF."stock_moves WHERE type=".db_escape(ST_TRANSFERSLIP)." AND trans_no=".db_escape($type_no);
		
	db_query($sql, "Could not void stock moves");
	
	void_stock_move_temp(ST_TRANSFERSLIP, $type_no);
}

?>