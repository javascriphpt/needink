<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
function add_item_location($loc_code, $location_name, $delivery_address, $phone, $phone2, $fax, $email, $contact)
{
	$sql = "INSERT INTO ".TB_PREF."locations (loc_code, location_name, delivery_address, phone, phone2, fax, email, contact)
		VALUES (".db_escape($loc_code).", ".db_escape($location_name).", ".db_escape($delivery_address).", "
			.db_escape($phone).", ".db_escape($phone2).", ".db_escape($fax).", ".db_escape($email).", "
			.db_escape($contact).")";

	db_query($sql,"a location could not be added");

	/* Also need to add loc_stock records for all existing items */
	$sql = "INSERT INTO ".TB_PREF."loc_stock (loc_code, stock_id, reorder_level)
		SELECT ".db_escape($loc_code).", ".TB_PREF."stock_master.stock_id, 0 FROM ".TB_PREF."stock_master";

	db_query($sql,"a location could not be added");
	
	/* Also need to add sales reference per location */
	$sql = "SELECT a.loc_code, c.type_id
		FROM ".TB_PREF."locations a, ".TB_PREF."sales_form_trans_type c
		WHERE a.loc_code=".db_escape($loc_code);
		
	$demand_result = db_query($sql,"Could not retreive demand for item");

	while($row = db_fetch($demand_result))
	{
		if($row[1] != 12)
		{
			$sql='INSERT INTO '.TB_PREF.'sales_forms (loc_code, type_id, next_ref) VALUES("'.$row[0].'", "'.$row[1].'", 1)';
			db_query($sql);
		}
		else
		{
			$sql2 = "SELECT a.loc_code, c.type_id, b.id
					FROM ".TB_PREF."locations a, ".TB_PREF."sales_form_trans_type c, ".TB_PREF."sales_form_category b
					WHERE c.type_id = 12
					AND a.loc_code=".db_escape($loc_code);
			$demand_result2 = db_query($sql2,"Could not retreive demand for item");		
			while($row2 = db_fetch($demand_result2))
			{
				$sql='INSERT INTO '.TB_PREF.'sales_forms (loc_code, type_id, next_ref, category_id) VALUES("'.$row2[0].'", "'.$row2[1].'", 1, "'.$row2[2].'")';
				db_query($sql);
			}
		}
	}
}

//------------------------------------------------------------------------------------

function update_item_location($loc_code, $location_name, $delivery_address, $phone, $phone2, $fax, $email, $contact)

{
    $sql = "UPDATE ".TB_PREF."locations SET location_name=".db_escape($location_name).",
    	delivery_address=".db_escape($delivery_address).",
    	phone=".db_escape($phone).", phone2=".db_escape($phone2).", fax=".db_escape($fax).",
    	email=".db_escape($email).", contact=".db_escape($contact)."
    	WHERE loc_code = ".db_escape($loc_code);

	db_query($sql,"a location could not be updated");
}

//------------------------------------------------------------------------------------

function delete_item_location($item_location)
{
	$sql="DELETE FROM ".TB_PREF."locations WHERE loc_code=".db_escape($item_location);
	db_query($sql,"a location could not be deleted");

	$sql = "DELETE FROM ".TB_PREF."loc_stock WHERE loc_code =".db_escape($item_location);
	db_query($sql,"a location could not be deleted");
}

//------------------------------------------------------------------------------------

function get_item_location($item_location)
{
	$sql="SELECT * FROM ".TB_PREF."locations WHERE loc_code=".db_escape($item_location);

	$result = db_query($sql,"a location could not be retrieved");

	return db_fetch($result);
}

//------------------------------------------------------------------------------------

function set_reorder_level($stock_id, $loc_code, $reorder_level)
{
	$sql = "UPDATE ".TB_PREF."loc_stock SET reorder_level = $reorder_level
		WHERE stock_id = ".db_escape($stock_id)." AND loc_code = ".db_escape($loc_code);

	db_query($sql,"an item reorder could not be set");
}

//------------------------------------------------------------------------------------

function get_loc_details($stock_id)
{
	$sql = "SELECT ".TB_PREF."loc_stock.*, ".TB_PREF."locations.location_name
		FROM ".TB_PREF."loc_stock, ".TB_PREF."locations
		WHERE ".TB_PREF."loc_stock.loc_code=".TB_PREF."locations.loc_code
		AND ".TB_PREF."loc_stock.stock_id = ".db_escape($stock_id) 
		." ORDER BY ".TB_PREF."loc_stock.loc_code";
	return db_query($sql,"an item reorder could not be retreived");
}

function get_loc_details_per_branch($stock_id, $location)
{
	$sql = "SELECT ".TB_PREF."loc_stock.*, ".TB_PREF."locations.location_name
		FROM ".TB_PREF."loc_stock, ".TB_PREF."locations
		WHERE ".TB_PREF."loc_stock.loc_code=".TB_PREF."locations.loc_code
		AND ".TB_PREF."loc_stock.stock_id = ".db_escape($stock_id)."  
		AND ".TB_PREF."loc_stock.loc_code = ".db_escape($location) ." 
		ORDER BY ".TB_PREF."loc_stock.loc_code";
	return db_query($sql,"an item reorder could not be retreived");
}

//------------------------------------------------------------------------------------

?>