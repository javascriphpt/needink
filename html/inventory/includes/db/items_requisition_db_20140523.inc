<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//-------------------------------------------------------------------------------------------------------------

function add_stock_requisition($Items, $location_from, $location_to, $date_, $delivery_date, $type, $reference, $memo_)
{
	global $Refs;

	begin_transaction();

	$requisition_id = get_next_trans_no(ST_REQUISITIONSLIP);
	
	$date = date2sql($date_);
	$del_date = date2sql($delivery_date);
	
	$sql = "INSERT INTO ".TB_PREF."items_requisition (order_no, trans_type, reference, from_stk_loc, ord_date, delivery_date)
		VALUES (" .db_escape($requisition_id) . "," .
			db_escape(ST_REQUISITIONSLIP) . "," . 
			db_escape($reference) .",". 
			db_escape($location_to) .",". 
			db_escape($date) .",". 
			db_escape($del_date) .")";

	db_query($sql, "item requisition cannot be added");
	
	foreach ($Items as $line_item)
	{
		$sql_items = "INSERT INTO ".TB_PREF."items_requisition_details (order_no, trans_type, stk_code, description, quantity) VALUES (";
		$sql_items .= db_escape($requisition_id) . ",".db_escape(ST_REQUISITIONSLIP) .
				",".db_escape($line_item->stock_id).", "
				.db_escape($line_item->item_description).",
				$line_item->quantity)";
		db_query($sql_items, "item requisition details cannot be added");
	}

	add_comments(ST_REQUISITIONSLIP, $requisition_id, $date_, $memo_);
	
	$Refs->save(ST_REQUISITIONSLIP, $requisition_id, $reference);
	add_audit_trail(ST_REQUISITIONSLIP, $requisition_id, $date_);

	commit_transaction();

	return $requisition_id;
}

//-------------------------------------------------------------------------------------------------------------

// add 2 stock_moves entries for a item requisition
// $date_ is display date (not sql)
// std_cost is in HOME currency
// it seems the standard_cost field is not used at all

function add_stock_requisition_item($requisition_id, $stock_id, $location_from, $location_to,
	$date_, $type, $reference, $quantity)
{
	add_stock_move(ST_REQUISITIONSLIP, $stock_id, $requisition_id, $location_from,
       	$date_, $reference, -$quantity, 0, $type);

	// add_stock_move(ST_LOCTRANSFER, $stock_id, $requisition_id, $location_to,
	   	// $date_, $reference, $quantity, 0, $type);
		
	add_stock_move_temp(ST_REQUISITIONSLIP, $stock_id, $requisition_id, $location_from,
       	$date_, $reference, -$quantity, 0, $type);

	add_stock_move_temp(ST_REQUISITIONSLIP, $stock_id, $requisition_id, $location_to,
	   	$date_, $reference, $quantity, 0, $type);

}

//-------------------------------------------------------------------------------------------------------------

function get_stock_requisition($trans_no)
{
	$result = get_stock_requisition_items($trans_no);
	if (db_num_rows($result) < 2)
	{
		display_db_error("requisition with less than 2 items : $trans_no", "");
	}

	// this function is very bad that it assumes that 1st record and 2nd record contain the
	// from and to locations - if get_stock_moves uses a different ordering than trans_no then
	// it will bomb
	$move1 = db_fetch($result);
	$move2 = db_fetch($result);

	// return an array of (From, To)
	if ($move1['qty'] < 0)
		return array($move1, $move2);
	else
		return array($move2, $move1);
}

function get_stock_requisition_2($trans_no)
{
	$result = get_stock_requisition_items_2($trans_no);
	if (db_num_rows($result) < 2)
	{
		display_db_error("requisition with less than 2 items : $trans_no", "");
	}

	// this function is very bad that it assumes that 1st record and 2nd record contain the
	// from and to locations - if get_stock_moves uses a different ordering than trans_no then
	// it will bomb
	$move1 = db_fetch($result);
	$move2 = db_fetch($result);

	// return an array of (From, To)
	if ($move1['qty'] < 0)
		return array($move1, $move2);
	else
		return array($move2, $move1);
}

//-------------------------------------------------------------------------------------------------------------

function get_stock_requisition_items($trans_no)
{
	$result = get_stock_moves(ST_LOCTRANSFER, $trans_no);

	if (db_num_rows($result) == 0)
	{
		return null;
	}

	return $result;
}

function get_stock_requisition_items_2($trans_no)
{
	$result = get_stock_moves_2(ST_LOCTRANSFER, $trans_no);

	if (db_num_rows($result) == 0)
	{
		return null;
	}

	return $result;
}

//-------------------------------------------------------------------------------------------------------------

function get_stock_requisition_temp($trans_no)
{
	$result = get_stock_requisition_items_temp($trans_no);
	if (db_num_rows($result) < 2)
	{
		display_db_error("requisition with less than 2 items : $trans_no", "");
	}

	// this function is very bad that it assumes that 1st record and 2nd record contain the
	// from and to locations - if get_stock_moves uses a different ordering than trans_no then
	// it will bomb
	$move1 = db_fetch($result);
	$move2 = db_fetch($result);

	// return an array of (From, To)
	if ($move1['qty'] < 0)
		return array($move1, $move2);
	else
		return array($move2, $move1);
}

function get_stock_requisition_temp_2($trans_no)
{
	$result = get_stock_requisition_items_temp_2($trans_no);
	if (db_num_rows($result) < 2)
	{
		display_db_error("requisition with less than 2 items : $trans_no", "");
	}

	// this function is very bad that it assumes that 1st record and 2nd record contain the
	// from and to locations - if get_stock_moves uses a different ordering than trans_no then
	// it will bomb
	$move1 = db_fetch($result);
	$move2 = db_fetch($result);

	// return an array of (From, To)
	if ($move1['qty'] < 0)
		return array($move1, $move2);
	else
		return array($move2, $move1);
}

//-------------------------------------------------------------------------------------------------------------

function get_stock_requisition_items_temp($trans_no)
{
	$result = get_stock_moves_temp(ST_LOCTRANSFER, $trans_no);

	if (db_num_rows($result) == 0)
	{
		return null;
	}

	return $result;
}

function get_stock_requisition_items_temp_2($trans_no)
{
	$result = get_stock_moves_temp(ST_REQUISITIONSLIP, $trans_no);

	if (db_num_rows($result) == 0)
	{
		return null;
	}

	return $result;
}

//-------------------------------------------------------------------------------------------------------------

function get_requisition_slip_items($trans_no)
{
	$result = get_stock_moves(ST_REQUISITIONSLIP, $trans_no);

	if (db_num_rows($result) == 0)
	{
		return null;
	}

	return $result;
}

//-------------------------------------------------------------------------------------------------------------

function void_stock_requisition($type_no)
{
	void_stock_move(ST_LOCTRANSFER, $type_no);
}

//-------------------------------------------------------------------------------------------------------------

function void_stock_requisition_slip($type_no)
{
	$sql = "DELETE FROM ".TB_PREF."stock_moves WHERE type=".db_escape(ST_REQUISITIONSLIP)." AND trans_no=".db_escape($type_no);
		
	db_query($sql, "Could not void stock moves");
	
	void_stock_move_temp(ST_REQUISITIONSLIP, $type_no);
}

//-------------------------------------------------------------------------------------------------------------

function get_item_requisition_details($order_no, $trans_type) {
	$sql = "SELECT   ".TB_PREF."items_requisition_details.*, ".TB_PREF."stock_master.units
			FROM     ".TB_PREF."items_requisition_details, ".TB_PREF."stock_master
			WHERE    ".TB_PREF."items_requisition_details.stk_code = ".TB_PREF."stock_master.stock_id
			AND      ".TB_PREF."items_requisition_details.trans_type = " . db_escape($trans_type) ."
			AND      ".TB_PREF."items_requisition_details.order_no = " . db_escape($order_no )." 
			ORDER BY ".TB_PREF."items_requisition_details.id";

	return db_query($sql, "Retreive order Line Items");
}

//-------------------------------------------------------------------------------------------------------------

function get_item_requisition_header($order_no, $trans_type)
{
	$sql = "SELECT ".TB_PREF."items_requisition.*, ".TB_PREF."locations.location_name, ".TB_PREF."comments.memo_
			FROM   ".TB_PREF."items_requisition, ".TB_PREF."locations, ".TB_PREF."comments
			WHERE  ".TB_PREF."locations.loc_code = ".TB_PREF."items_requisition.from_stk_loc
			AND    ".TB_PREF."items_requisition.trans_type = ".TB_PREF."comments.type
			AND    ".TB_PREF."items_requisition.order_no = ".TB_PREF."comments.id
			AND    ".TB_PREF."items_requisition.trans_type = " . db_escape($trans_type) ."
			AND    ".TB_PREF."items_requisition.order_no = " . db_escape($order_no );

	$result = db_query($sql, "order Retreival");

	$num = db_num_rows($result);
	if ($num > 1)
	{
		display_db_error("FATAL : sales order query returned a duplicate - " . db_num_rows($result), $sql, true);
	}
	else if ($num == 1)
	{
		return db_fetch($result);
	}
	else
		display_db_error("FATAL : sales order return nothing - " . db_num_rows($result), $sql, true);

}

//-------------------------------------------------------------------------------------------------------------

function read_item_requisition($order_no, &$order, $trans_type)
{
	$myrow = get_item_requisition_header($order_no, $trans_type);

	$order->trans_type = $myrow['trans_type'];
	$order->trans_no = array($order_no=> $myrow["version"]);	
	$order->set_location($myrow["from_stk_loc"], $myrow["location_name"]);
	$order->reference = $myrow["reference"];
	$order->due_date = sql2date($myrow["delivery_date"]);
	$order->document_date = sql2date($myrow["ord_date"]);
	$order->Comments = $myrow["memo_"];

	$result = get_item_requisition_details($order_no, $order->trans_type);
	if (db_num_rows($result) > 0)
	{
		$line_no=0;
		while ($myrow = db_fetch($result))
		{
			$order->add_to_cart($line_no,$myrow["stk_code"],$myrow["quantity"],
				0, 0, $myrow["qty_sent"], 0, $myrow["description"], $myrow["id"],
				0, "", $myrow["quantity_cancelled"]);
			$line_no++;
		}
	}

	return true;
}

//-------------------------------------------------------------------------------------------------------------

// Mark changes in items_requisition_details
//
function update_items_requisition_version($order)
{
  foreach ($order as $so_num => $so_ver) {
  $sql= 'UPDATE '.TB_PREF.'items_requisition SET version=version+1 WHERE order_no='. $so_num.
	' AND version='.$so_ver . " AND trans_type=167";
  db_query($sql, 'Concurrent editing conflict while sales order update');
  }
}

//-------------------------------------------------------------------------------------------------------------

function write_item_requisition(&$delivery,$bo_policy)
{
	global $Refs;

	$trans_no = $delivery->trans_no;
	if (is_array($trans_no)) $trans_no = key($trans_no);

	begin_transaction();
	
	$delivery_no = get_next_trans_no(ST_LOCTRANSFER);
	$company_data = get_company_prefs();
	$from_location = $company_data["default_item_requisition_from_location"];

	// mark item requisition for concurrency conflicts check
	update_items_requisition_version($delivery->src_docs);

	foreach ($delivery->line_items as $line_no => $delivery_line) 
	{	
		// Update isn_trans; change location of isn
		if($delivery_line->serial){
			$serials=explode("\n",$delivery_line->serial);
			foreach($serials as $s){
				change_location($delivery_line->stock_id, $from_location, $delivery->Location, $s);
				add_isn_trail($delivery_line->stock_id, $s, $from_location, $delivery_no, ST_LOCTRANSFER, $delivery->document_date, 0);
				add_isn_trail($delivery_line->stock_id, $s, $delivery->Location, $delivery_no, ST_LOCTRANSFER, $delivery->document_date, 1);
			}
		}
			
		// Now update items_requisition_details for the quantity received
		if ($delivery_line->qty_old != $delivery_line->qty_dispatched)
			update_parent_line(ST_LOCTRANSFER, $delivery_line->src_id,
				$delivery_line->qty_dispatched-$delivery_line->qty_old);

		// Insert to stock_moves
		if ($delivery_line->qty_dispatched != 0) {
			$company_data = get_company_prefs();
			$from_location = $company_data["default_item_requisition_from_location"];
			
			// from_location (-)
			add_stock_move(ST_LOCTRANSFER, $delivery_line->stock_id, $delivery_no,
				$from_location, $delivery->document_date, $delivery->reference,
				-$delivery_line->qty_dispatched, 0, 0, 1, 0, 0,
				"The customer stock movement record cannot be inserted");
			
			// to_location (-)
			add_stock_move(ST_LOCTRANSFER, $delivery_line->stock_id, $delivery_no,
				$delivery->Location, $delivery->document_date, $delivery->reference,
				$delivery_line->qty_dispatched, 0, 0, 1, 0, 0,
				"The customer stock movement record cannot be inserted");
						
		} /*quantity dispatched is more than 0 */
	} /*end of order_line loop */

	add_comments(ST_LOCTRANSFER, $delivery_no, $delivery->document_date, $delivery->Comments);
	$Refs->save(ST_LOCTRANSFER, $delivery_no, $delivery->reference);
	add_audit_trail(ST_LOCTRANSFER, $delivery_no, $delivery->document_date);
	
	commit_transaction();

	return $delivery_no;
}

//-------------------------------------------------------------------------------------------------------------

?>