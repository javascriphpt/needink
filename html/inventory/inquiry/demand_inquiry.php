<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_DEMANDINQUIRY';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/manufacturing.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/inventory/includes/inventory_db.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);
if ($use_date_picker)
	$js .= get_js_date_picker();
	
page(_("Demand Inquiry"), false, false, "", $js);

//----------------------------------------------------------------------------------------------------

start_form();

start_table("class='tablestyle_noborder'");
start_row();

if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	locations_list_cells_user(_("Deliver from Location:"), 'location', null, false, true, $_SESSION["wa_current_user"]->default_location);
else
	locations_list_cells(_("Location:"), 'location', null, false);
	
stock_categories_list_cells(_("Category:"), 'category', null, "No Category Filter", false);

submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');

end_row();

end_table();

end_form();

//----------------------------------------------------------------------------------------------------

if(isset($_POST['SearchOrders']))
	$Ajax->activate('orders_tbl');

//----------------------------------------------------------------------------------------------------

$sql = "SELECT ".TB_PREF."sales_order_details.stk_code, ".TB_PREF."sales_order_details.description, 		
				SUM(".TB_PREF."sales_order_details.quantity - " .TB_PREF."sales_order_details.qty_sent) AS QtyDemand
		FROM ".TB_PREF."sales_order_details,
			".TB_PREF."sales_orders, 
			".TB_PREF."stock_master, 
			".TB_PREF."stock_category
		WHERE ".TB_PREF."sales_order_details.order_no=" .TB_PREF."sales_orders.order_no 
		AND ".TB_PREF."sales_orders.trans_type=".ST_SALESORDER." 
		AND ".TB_PREF."sales_orders.trans_type=".TB_PREF."sales_order_details.trans_type 
		AND ".TB_PREF."sales_order_details.stk_code = ".TB_PREF."stock_master.stock_id
		AND ".TB_PREF."stock_master.category_id = ".TB_PREF."stock_category.category_id
		AND ".TB_PREF."sales_orders.is_approve = 1
		AND ".TB_PREF."sales_orders.from_stk_loc =".db_escape($_POST['location']);
	if($_POST['category'] != -1)
		$sql .= " AND ".TB_PREF."stock_category.category_id = ".db_escape($_POST['category']);
	$sql .= " GROUP BY ".TB_PREF."sales_order_details.stk_code, ".TB_PREF."sales_orders.from_stk_loc
			HAVING SUM(".TB_PREF."sales_order_details.quantity - " .TB_PREF."sales_order_details.qty_sent) >= 1
			ORDER BY ".TB_PREF."stock_category.category_id";

$result = db_query($sql,"No transactions were returned");

//----------------------------------------------------------------------------------------------------

div_start('orders_tbl');

br();

start_table($table_style);

$th = array(_("Item Code"), _("Description"), _("Demand"));

table_header($th);

$j = 1;
$k = 0; //row colour counter
start_row();
while ($myrow = db_fetch($result))
{
	$dec = get_qty_dec($myrow["stk_code"]);
	alt_table_row_color($k);
	
	view_stock_status_cell($myrow["stk_code"]);
	label_cell($myrow["description"]);
	qty_cell($myrow["QtyDemand"], false, $dec);
		
	$j++;
	If ($j == 12)
	{
		$j = 1;
		table_header($th);
	}
}
	end_row();

end_table();

div_end();

end_page();

?>
