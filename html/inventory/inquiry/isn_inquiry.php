<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_ISNINQUIRY';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/banking.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");

include_once($path_to_root . "/includes/ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "ISN Inquiry"), false, false, "", $js);

//------------------------------------------------------------------------------------------------

check_db_has_stock_items(_("There are no items defined in the system."));

if(get_post('RefreshInquiry'))
{
	$Ajax->activate('totals_tbl');
}

if (isset($_GET['stock_id']))
{
	$_POST['stock_id'] = $_GET['stock_id'];
}

if (isset($_POST['isn']) && ($_POST['isn'] != ""))
{
	$isn = $_POST['isn'];
}

if (isset($_POST['aic']) && ($_POST['aic'] != ""))
{
	$aic = $_POST['aic'];
}

//------------------------------------------------------------------------------------------------

start_form();

if (!isset($_POST['stock_id']))
	$_POST['stock_id'] = get_global_stock_item();

start_table("class='tablestyle_noborder'");

start_row();

ref_cells(_("ISN #:"), 'isn', '', null, '');
ref_cells(_("AIC #:"), 'aic', '', null, '');

// stock_items_list_cells(_("Item:"), 'stock_id', $_POST['stock_id']);

if($_SESSION["wa_current_user"]->can_access_all_item_categories == 0)
	stock_items_list_cells_user(_("Item:"), 'stock_id', $_POST['stock_id'], true, false, false, false, $_SESSION["wa_current_user"]->default_item_categories);
else
	stock_items_list_cells(_("Item:"), 'stock_id', $_POST['stock_id'], true);
	
end_row();
start_row();

if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	locations_list_cells_user(_("From Location:"), 'StockLocation', null, true, false, $_SESSION["wa_current_user"]->default_location);
else
	locations_list_cells(_("From Location:"), 'StockLocation', null, true, false);

$sql = "SELECT DISTINCT(month) as month_, month FROM ".TB_PREF."isn_gen";
$result = db_query($sql, "could not query month");

echo "<td>Month:    ";
echo "<select name=month class=combo>";
	echo "<option value=0>All Months</option>";
while ($myrow = db_fetch($result))
{
	echo "<option value=".$myrow['month_'].">".get_month_name($myrow['month'])."</option>";
}
echo "</select>";
echo "</td>";

isn_year_list_cells(_("Year:"), 'year', null, true);

//check_cells(" " . _("show unavailable:"), 'showSettled', null);

isn_status_list_cells(_(""), 'isn_status', null);

submit_cells('RefreshInquiry',_("Search"),'',_('Refresh Inquiry'), 'default');

end_row();
end_table();

set_global_stock_item($_POST['stock_id']);

//------------------------------------------------------------------------------------------------

function systype_name($dummy, $type)
{
	global $systypes_array;

	return $systypes_array[$type];
}

function show_movement($row) 
{
	return viewer_link(_("Show Movement"), "/inventory/view/view_isn.php?" . SID  . "isn=" . $row["isn"] . SID  . "&stock_id=" . $row["stock_id"]);
}

function is_available($row) 
{
	if($row['is_available'] == 1)
		return "Yes";
	else
		return "No";
}

function print_location_name($row) 
{
	return get_location_name($row['loc_code']);
}

//------------------------------------------------------------------------------------------------

$sql = "SELECT gen.isn, 
		gen.aic, 
		gen.stock_id, 
		stock.description,
		gen.aic, 
		gen.is_available,
		gen.loc_code
FROM ".TB_PREF."isn_gen as gen, ".TB_PREF."stock_master as stock
WHERE gen.stock_id = stock.stock_id 
AND gen.is_available = ".db_escape($_POST['isn_status']);
//AND gen.stock_id = ".db_escape($_POST['stock_id']);

if(isset($_POST['stock_id']) && ($_POST['stock_id'] != "") && ($_POST['stock_id'] != ALL_TEXT))
{
	$sql .= " AND gen.stock_id = ".db_escape($_POST['stock_id']);
}

if (isset($isn) && $isn != "")
{
	$sql .= " AND gen.isn LIKE ".db_escape('%'. $isn . '%');
}	

if (isset($aic) && $aic != "")
{
	$sql .= " AND gen.aic LIKE ".db_escape('%'. $aic . '%');
}	

if (isset($_POST['StockLocation']) && $_POST['StockLocation'] != ALL_TEXT)
{
	$sql .= " AND gen.loc_code = ".db_escape($_POST['StockLocation']);
}

if (isset($_POST['month']) && $_POST['month'] != 0)
{
	$sql .= " AND gen.month = ".db_escape($_POST['month']);
}

if (isset($_POST['year']) && $_POST['year'] != 0)
{
	$sql .= " AND gen.year = ".db_escape($_POST['year']);
}

// if (check_value('showSettled'))
// {
	// $sql .= " AND gen.is_available = 0";
// }
// else
// {
	// $sql .= " AND gen.is_available = 1 ";
// }

//------------------------------------------------------------------------------------------------

$cols = array(
	_("ISN") => array('ord'=>''), 
	_("AIC") => array('ord'=>''), 
	_("Item") => array('ord'=>''), 
	_("Description") => array('ord'=>''), 
	_("AIC") => array('ord'=>''), 
	_("Is Available")=> array('fun'=>'is_available', 'ord'=>''),
		array('insert'=>true, 'fun'=>'show_movement')
	);
	//_("Location") => array('fun'=>'print_location_name', 'ord'=>''),

$table =& new_db_pager('doc_tbl', $sql, $cols);

$table->width = "80%";

display_db_pager($table);

end_form();
end_page();
?>
