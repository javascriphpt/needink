<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_MEMOINQUIRY';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);
if ($use_date_picker)
	$js .= get_js_date_picker();
	
page(_("Memo Inquiry"), false, false, "", $js);

//----------------------------------------------------------------------------------------------------

if (get_post('Search'))
{
	$Ajax->activate('journal_tbl');
}

//----------------------------------------------------------------------------------------------------

start_form();

start_table("class='tablestyle_noborder'");
start_row();

date_cells(_("From:"), 'FromDate');	//, '', null, 0, -1, 0);
date_cells(_("To:"), 'ToDate');

ref_cells(_("Memo:"), 'Memo', '',null, _('Enter memo fragment or leave empty'));

submit_cells('Search', _("Search"), '', '', 'default');

end_row();

end_table();

//----------------------------------------------------------------------------------------------------

function systype_name($dummy, $type)
{
	global $systypes_array;
	
	return $systypes_array[$type];
}

function view_link($row) 
{		
	if($row["type"] == ST_CUSTDELIVERY || $row["type"] == ST_SALESINVOICE || $row["type"] == ST_CUSTCREDIT)
	{
		$ref_no = get_sales_ref_no($row["id"], $row["type"]);
		return get_trans_view_str($row["type"], $row["id"], $ref_no["location"]."-".$ref_no["form_type_no"]);
	}
	else if($row["type"] == ST_CUSTPAYMENT)
	{
		$ref_no = get_sales_ref_no_OR_PR($row["id"], $row["type"]);
		return get_trans_view_str($row["type"], $row["id"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"]));
	}
	else
		return get_trans_view_str($row["type"], $row["id"], get_reference($row["type"], $row["id"]));
}

//----------------------------------------------------------------------------------------------------

$sql = "SELECT	type, id, date_, memo_ FROM ".TB_PREF."comments
			WHERE date_ >= ".db_escape(date2sql($_POST['FromDate']))."
			AND date_ <= ".db_escape(date2sql($_POST['ToDate']))."";
if (isset($_POST['Memo']) && $_POST['Memo'] != "")
	$sql .= " AND memo_ LIKE '%". $_POST['Memo'] . "%'";

$cols = array(
	_("Type") => array('fun'=>'systype_name'), 
	_("#") => array('fun'=>'view_link'),  
	_("Date") =>array('name'=>'date_','type'=>'date','ord'=>'desc'),
	_("Memo")
);

$table =& new_db_pager('journal_tbl', $sql, $cols);

$table->width = "50%";

display_db_pager($table);

//----------------------------------------------------------------------------------------------------

end_form();
end_page();

?>
