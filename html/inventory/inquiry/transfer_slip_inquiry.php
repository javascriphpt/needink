<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_LOCATIONTRANSFER';
$path_to_root="../..";
include($path_to_root . "/includes/db_pager.inc");
include($path_to_root . "/includes/session.inc");

include($path_to_root . "/purchasing/includes/purchasing_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Transfer Slip"), false, false, "", $js);

if (isset($_GET['order_number']))
{
	$order_number = $_GET['order_number'];
}

//-----------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';

	$Ajax->addDisable(true, 'OrdersAfterDate', $disable);
	$Ajax->addDisable(true, 'OrdersToDate', $disable);

	if ($disable) {
		$Ajax->addFocus(true, 'order_number');
	} else
		$Ajax->addFocus(true, 'OrdersAfterDate');

	$Ajax->activate('orders_tbl');
}
//---------------------------------------------------------------------------------------------

start_form();

start_table("class='tablestyle_noborder'");
start_row();
ref_cells(_("#:"), 'order_number', '',null, '', true);

date_cells(_("from:"), 'OrdersAfterDate', '', null, -30);
date_cells(_("to:"), 'OrdersToDate');

submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');
end_row();
end_table();
//---------------------------------------------------------------------------------------------
if (isset($_POST['order_number']))
{
	$order_number = $_POST['order_number'];
}

//---------------------------------------------------------------------------------------------
function trans_view($trans)
{
	return get_trans_view_str(ST_TRANSFERSLIP, $trans["trans_no"], $trans["reference"]);
}

function print_location_name($row) 
{
	return get_location_name($row['loc_code']);
}

function from_location_name($trans) 
{
	$transfer_items = get_stock_transfer_temp_2($trans["trans_no"]);
	$from_trans = $transfer_items[0];
	
	return $from_trans['location_name'];
}

function receive_link($row) 
{
  return pager_link( _("Receive"), "/inventory/receive_transfer_slip.php?" . SID  . "trans_no=" . $row["trans_no"]);
}

//---------------------------------------------------------------------------------------------

$sql = "SELECT DISTINCT trans_no, tran_date, loc_code, reference
	FROM ".TB_PREF."stock_moves_temp	
	WHERE type = 166 
	AND received = 0
	AND qty > 0 ";
	
if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	$sql .= " AND loc_code = ".db_escape($_SESSION["wa_current_user"]->default_location);

if (isset($order_number) && $order_number != "")
{
	$sql .= " AND reference LIKE ".db_escape('%'. $order_number . '%');
}
else
{

	$data_after = date2sql($_POST['OrdersAfterDate']);
	$date_before = date2sql($_POST['OrdersToDate']);

	$sql .= " AND tran_date >= '$data_after'";
	$sql .= " AND tran_date <= '$date_before'";

} //end not order number selected

$cols = array(
		_("#") => array('fun'=>'trans_view', 'ord'=>''), 
		_("Date") => 'date',
		_("From Location") => array('fun'=>'from_location_name', 'ord'=>''),
		_("To Location") => array('fun'=>'print_location_name', 'ord'=>''),
		array('insert'=>true, 'fun'=>'receive_link'),
);

//---------------------------------------------------------------------------------------------------

$table =& new_db_pager('orders_tbl', $sql, $cols);

$table->width = "80%";

display_db_pager($table);

end_form();
end_page();
?>
