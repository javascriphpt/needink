<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_AICINQUIRY';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/banking.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");

include_once($path_to_root . "/includes/ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "AIC Inquiry"), false, false, "", $js);

//------------------------------------------------------------------------------------------------

check_db_has_stock_items(_("There are no items defined in the system."));

if(get_post('RefreshInquiry'))
{
	$Ajax->activate('totals_tbl');
}

if (isset($_GET['stock_id']))
{
	$_POST['stock_id'] = $_GET['stock_id'];
}

if (isset($_POST['isn']) && ($_POST['isn'] != ""))
{
	$isn = $_POST['isn'];
}

if (isset($_POST['aic']) && ($_POST['aic'] != ""))
{
	$aic = $_POST['aic'];
}

//------------------------------------------------------------------------------------------------

start_form();

if (!isset($_POST['stock_id']))
	$_POST['stock_id'] = get_global_stock_item();

start_table("class='tablestyle_noborder'");

ref_cells(_("ISN #:"), 'isn', '', null, '');

ref_cells(_("AIC #:"), 'aic', '', null, '');

// stock_items_list_cells(_("Item:"), 'stock_id', $_POST['stock_id']);

if($_SESSION["wa_current_user"]->can_access_all_item_categories == 0)
	stock_items_list_cells_user(_("Item:"), 'stock_id', $_POST['stock_id'], true, false, false, false, $_SESSION["wa_current_user"]->default_item_categories);
else
	stock_items_list_cells(_("Item:"), 'stock_id', $_POST['stock_id'], true);

check_cells(" " . _("show inactive:"), 'showSettled', null);

submit_cells('RefreshInquiry',_("Search"),'',_('Refresh Inquiry'), 'default');
end_table();

set_global_stock_item($_POST['stock_id']);

//------------------------------------------------------------------------------------------------

function update_aic($row) 
{
	return pager_link( _("Add AIC"), "/inventory/update_aic.php?" . SID  . "isn=" . $row["isn"] . "&stock_id=" . $row["stock_id"] . "&loc_code=" . $row["loc_code"]);
}

function edit_isn($row) 
{
	return pager_link( _("Edit ISN"), "/inventory/edit_isn.php?" . SID  . "isn=" . $row["isn"] . "&stock_id=" . $row["stock_id"] . "&loc_code=" . $row["loc_code"]);
}

function edit_aic($row) 
{
	return pager_link( _("Edit AIC"), "/inventory/edit_aic_2.php?New=1&" . SID  . "isn=" . $row["isn"] . "&stock_id=" . $row["stock_id"] . "&loc_code=" . $row["loc_code"]);
}

//------------------------------------------------------------------------------------------------

$sql = "SELECT gen.isn,
		gen.stock_id,
		stock.description,
		aic, gen.loc_code
FROM ".TB_PREF."isn_gen as gen, ".TB_PREF."stock_master as stock
WHERE gen.stock_id = stock.stock_id ";

if(isset($_POST['stock_id']) && ($_POST['stock_id'] != "") && ($_POST['stock_id'] != ALL_TEXT))
{
	$sql .= " AND gen.stock_id = ".db_escape($_POST['stock_id']);
}

if (isset($isn) && $isn != "")
{
	$sql .= " AND gen.isn LIKE ".db_escape('%'. $isn . '%');
}

if (isset($aic) && $aic != "")
{
	$sql .= " AND gen.aic LIKE ".db_escape('%'. $aic . '%');
}

if (check_value('showSettled'))
{
	$sql .= " AND gen.inactive = 1 ";
}
else
{
	$sql .= " AND gen.inactive = 0 ";
}

//------------------------------------------------------------------------------------------------

$cols = array(
	_("ISN") => array('ord'=>''), 
	_("Item") => array('ord'=>''), 
	_("Description") => array('ord'=>''), 
	_("AIC") => array('ord'=>''), 
		array('insert'=>true, 'fun'=>'update_aic'),
		array('insert'=>true, 'fun'=>'edit_aic'),
		array('insert'=>true, 'fun'=>'edit_isn')
	);

$table =& new_db_pager('doc_tbl', $sql, $cols);

$table->width = "80%";

display_db_pager($table);

end_form();
end_page();
?>
