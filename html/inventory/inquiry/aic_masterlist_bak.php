<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_AICMASTERLIST';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);
if ($use_date_picker)
	$js .= get_js_date_picker();
	
page(_("AIC Masterlist"), false, false, "", $js);

//----------------------------------------------------------------------------------------------------

if (get_post('Search')) 
	$Ajax->activate('orders_tbl');

//----------------------------------------------------------------------------------------------------

start_form();

if (!isset($_POST['stock_id']))
	$_POST['stock_id'] = get_global_stock_item();

start_table("class='tablestyle_noborder'");
start_row();

ref_cells(_("ISN #:"), 'isn', '', null, '');

if($_SESSION["wa_current_user"]->can_access_all_item_categories == 0)
	stock_items_list_cells_user(_("Item:"), 'stock_id', $_POST['stock_id'], false, false, false, false, $_SESSION["wa_current_user"]->default_item_categories);
else
	stock_items_list_cells(_("Item:"), 'stock_id', $_POST['stock_id'], false);

if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	locations_list_cells_user(_("Location:"), 'StockLocation', null, false, false, $_SESSION["wa_current_user"]->default_location);
else
	locations_list_cells(_("Location:"), 'StockLocation', null, false, false);
	
customer_list_cells(_("Customer: "), 'customer_id', null, true);

submit_cells('Search', _("Search"), '', '', 'default');

end_row();

end_table();

//----------------------------------------------------------------------------------------------------

function systype_name($dummy, $type)
{
	global $systypes_array;
	
	return $systypes_array[$type];
}

function view_link($row) 
{		
	if($row["type"] == ST_CUSTDELIVERY || $row["type"] == ST_SALESINVOICE || $row["type"] == ST_CUSTCREDIT)
	{
		$ref_no = get_sales_ref_no($row["id"], $row["type"]);
		return get_trans_view_str($row["type"], $row["id"], $ref_no["location"]."-".$ref_no["form_type_no"]);
	}
	else if($row["type"] == ST_CUSTPAYMENT)
	{
		$ref_no = get_sales_ref_no_OR_PR($row["id"], $row["type"]);
		return get_trans_view_str($row["type"], $row["id"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"]));
	}
	else
		return get_trans_view_str($row["type"], $row["id"], get_reference($row["type"], $row["id"]));
}

//----------------------------------------------------------------------------------------------------

function get_supplier_by_type($type, $trans_no)
{
	if($type == ST_SUPPRECEIVE)
	{
		$sql = "SELECT supp_name FROM ".TB_PREF."grn_batch a, ".TB_PREF."suppliers b 
					WHERE a.supplier_id = b.supplier_id
					AND a.id = ".db_escape($trans_no);
		$result = db_query($sql);
		$row = db_fetch_row($result);		//display_error($row[0]);
		return $row[0];
	}
	else
		return "";
}

//----------------------------------------------------------------------------------------------------

$sql = "SELECT s1.*, max(id) as max_id
		FROM 0_isn_trans as s1 , 0_stock_master s2
		WHERE s1.stock_id = s2.stock_id";
if($_POST['StockLocation'] != null)
	$sql .= " AND s1.loc_code=".db_escape($_POST['StockLocation']);
if($_POST['stock_id'] != null)
	$sql .= " AND s1.stock_id=".db_escape($_POST['stock_id']);
$sql .= " GROUP BY isn, stock_id ORDER BY stock_id";

$result = db_query($sql);

div_start('orders_tbl');
start_table($table_style);
$th = array(_("Item"), _("Description"), _("Serial Number"), _("Supplier"), _("Date of Purchase"), _("Purchase Price"), _("Latest Deployment"));

table_header($th);

$j = 1;
$k = 0; //row colour counter

while($myrow = db_fetch($result))
{
	$sql2 = "SELECT trans_type, trans_no, isn, loc_code, stock_id 
			FROM ".TB_PREF."isn_trans WHERE id=".db_escape($myrow['max_id']);
	$res = db_query($sql2);
	$row = db_fetch_row($res);
	
	$items = get_item($row[4]);
	label_cell($row[4]);
	label_cell($items['description']);
	label_cell($row[2]);
	label_cell(get_supplier_by_type($row[0], $row[1]));
	label_cell($row[3]);
	label_cell($row[4]);
	end_row();
	
	$j++;
	if ($j == 11)
	{
		$j = 1;
		table_header($th);
	}
}

//----------------------------------------------------------------------------------------------------

end_form();
end_page();

?>
