<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_AICMASTERLIST';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);
if ($use_date_picker)
	$js .= get_js_date_picker();
	
page(_("AIC Masterlist"), false, false, "", $js);

//----------------------------------------------------------------------------------------------------

if (get_post('Search')) 
	$Ajax->activate('orders_tbl');

//----------------------------------------------------------------------------------------------------

start_form();

start_table("class='tablestyle_noborder'");
start_row();

if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	locations_list_cells_user(_("Location:"), 'StockLocation', null, true, false, $_SESSION["wa_current_user"]->default_location);
else
	locations_list_cells(_("Location:"), 'StockLocation', null, true, false);
	
customer_list_cells(_("Customer: "), 'customer_id', null, true);

submit_cells('Search', _("Search"), '', '', 'default');

end_row();

end_table();

//----------------------------------------------------------------------------------------------------

function systype_name($dummy, $type)
{
	global $systypes_array;
	
	return $systypes_array[$type];
}

function view_link($row) 
{		
	if($row["type"] == ST_CUSTDELIVERY || $row["type"] == ST_SALESINVOICE || $row["type"] == ST_CUSTCREDIT)
	{
		$ref_no = get_sales_ref_no($row["id"], $row["type"]);
		return get_trans_view_str($row["type"], $row["id"], $ref_no["location"]."-".$ref_no["form_type_no"]);
	}
	else if($row["type"] == ST_CUSTPAYMENT)
	{
		$ref_no = get_sales_ref_no_OR_PR($row["id"], $row["type"]);
		return get_trans_view_str($row["type"], $row["id"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"]));
	}
	else
		return get_trans_view_str($row["type"], $row["id"], get_reference($row["type"], $row["id"]));
}

//----------------------------------------------------------------------------------------------------

function get_supplier_by_type($type, $trans_no)
{
	if($type == ST_SUPPRECEIVE)
	{
		$sql = "";
		return 1;
	}
	else
		return 0;
}

//----------------------------------------------------------------------------------------------------

$table2 = " (SELECT max(id) as max_id
		FROM 0_isn_trans" ;
if($_POST['StockLocation'] != null)
	$table2 .= " WHERE loc_code=".db_escape($_POST['StockLocation']);
$table2 .= " GROUP BY isn, stock_id )";

$sql = "SELECT isn, stock_id
			FROM 0_isn_trans a, ".$table2." b
			WHERE a.id = b.max_id";
	
display_error($sql);	
			
// $cols = array(
	// _("ISN") => array('ord'=>''), 
	// _("Item") => array('ord'=>'')
	// );

// $table =& new_db_pager('doc_tbl', $sql, $cols);

// $table->width = "80%";

// display_db_pager($table);

//----------------------------------------------------------------------------------------------------

end_form();
end_page();

?>
