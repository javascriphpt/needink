<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_ITEMSTRANSVIEW';
$path_to_root = "../..";

include($path_to_root . "/includes/session.inc");

page(_($help_context = "View Inventory Transfer"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

if (isset($_GET["trans_no"]))
{
	$trans_no = $_GET["trans_no"];
}

$transfer_items = get_stock_transfer($trans_no);
$transfer_items_to = get_stock_transfer($trans_no);

$from_trans = $transfer_items[0];
$to_trans = $transfer_items_to[1];

$sql = "SELECT ".TB_PREF."stock_moves.loc_code
		FROM ".TB_PREF."stock_moves,".TB_PREF."locations,".TB_PREF."stock_master
		WHERE ".TB_PREF."stock_moves.stock_id = ".TB_PREF."stock_master.stock_id
		AND ".TB_PREF."locations.loc_code=".TB_PREF."stock_moves.loc_code
		AND ".TB_PREF."stock_moves.qty >= 0
		AND type=16 AND trans_no=".db_escape($trans_no)." ORDER BY trans_id LIMIT 1";
$result = db_query($sql);
$row2 = db_fetch_row($result);

display_heading($systypes_array[ST_LOCTRANSFER] . " # " . get_reference(ST_LOCTRANSFER, $trans_no));

echo "<br>";
start_table("$table_style2 width=90%");

start_row();
label_cells(_("Item"), $from_trans['stock_id'] . " - " . $from_trans['description'], "class='tableheader2'");
label_cells(_("From Location"), $from_trans['location_name'], "class='tableheader2'");
label_cells(_("To Location"), get_location_name($row2 [0]) /*$to_trans['location_name']*/, "class='tableheader2'");
end_row();
start_row();
//label_cells(_("Reference"), $from_trans['reference'], "class='tableheader2'");
$adjustment_type = get_movement_type($to_trans['person_id']) ;
label_cells(_("Adjustment Type"), $adjustment_type['name'], "class='tableheader2'");
label_cells(_("Date"), sql2date($to_trans['tran_date']), "class='tableheader2'");
end_row();

$res = get_audit_trail_all(ST_LOCTRANSFER, $trans_no);
$audit_trail = "";	$x=1;
while($audit = db_fetch($res))
{
	$name = get_user($audit['user']);
	if($x == 1)
		$audit_trail .= $name['user_id']." - ".$audit['description'];
	else
		$audit_trail .= "<br>".$name['user_id']." - ".$audit['description'];
		
	$x++;
}

label_row(_("Audit Trail"), $audit_trail, "class='tableheader2'", "colspan=5");

comments_display_row(ST_LOCTRANSFER, $trans_no);

end_table(1);

echo "<br>";
start_table("$table_style width=90%");

$th = array(_("Item"), _("Description"), _("Quantity"), _("Units"), _("Serial No."), _("AIC"));
table_header($th);
$transfer_items = get_stock_moves(ST_LOCTRANSFER, $trans_no);
$k = 0;
while ($item = db_fetch($transfer_items))
{
	if ($item['loc_code'] == $to_trans['loc_code'])
	{
        alt_table_row_color($k);

        label_cell($item['stock_id']);
        label_cell($item['description']);
        qty_cell($item['qty'], false, get_qty_dec($item['stock_id']));
        label_cell($item['units']);
		
		$serial = get_serial_no_per_item($trans_no, 16, $to_trans['loc_code'], $item['stock_id']);
	
		$isn = "";
		$aic = "";
		while($row = db_fetch($serial))
		{
			$isn .= $row["isn"] ."<br>";
			$aic .= $row["aic"] ."<br>";
		}
		
		label_cell($isn);	
		label_cell($aic);	
		
        end_row();
	}
}

end_table(1);

is_voided_display(ST_LOCTRANSFER, $trans_no, _("This transfer has been voided."));

start_table("$table_style width=80%");

start_row();

label_cell("Prepared By", "class='tableheader'");
label_cell("Checked By", "class='tableheader'");
label_cell("Approved By", "class='tableheader'");
label_cell("Received By", "class='tableheader'");

end_row();

start_row();

echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";

end_row();

end_table(2);

end_page(true);
?>