<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_ISNINQUIRY';
$path_to_root = "../..";

include($path_to_root . "/includes/session.inc");

page(_($help_context = "View ISN Trans"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/inventory/includes/inventory_db.inc");

if (isset($_GET["isn"]))
{
	$isn = $_GET["isn"];
}

if (isset($_GET["stock_id"]))
{
	$stock_id = $_GET["stock_id"];
}

// $sql = "SELECT DISTINCT trans.*, 
			// stock.description,
			// moves.person_id
	// FROM ".TB_PREF."isn_trans as trans, ".TB_PREF."stock_master as stock, ".TB_PREF."stock_moves as moves
	// WHERE trans.stock_id = stock.stock_id
	// AND trans.isn = ".db_escape($isn)."
	// AND trans.trans_no = moves.trans_no
	// AND trans.trans_type = moves.type
	// AND trans.stock_id = moves.stock_id
	// AND trans.stock_id = ".db_escape($stock_id)."
	// ORDER BY trans.id, trans.date";
$sql = "SELECT DISTINCT trans.*, 
			stock.description,
			moves.person_id
	FROM ".TB_PREF."isn_trans as trans, ".TB_PREF."stock_master as stock, ".TB_PREF."stock_moves as moves
	WHERE trans.stock_id = stock.stock_id
	AND trans.isn = ".db_escape($isn)."
	AND trans.trans_no = moves.trans_no
	AND trans.trans_type = moves.type
	AND trans.stock_id = ".db_escape($stock_id)."
	ORDER BY trans.date, trans.id";
$result = db_query($sql, "Cannot retreive transaction");


div_start('doc_tbl');
start_table($table_style);
$th = array(_("Type"), _("#"), _("Location"), _("Date"), _("Detail"),
	_("Quantity In"), _("Quantity Out"));

table_header($th);

$j = 1;
$k = 0; //row colour counter

$total_in = 0;
$total_out = 0;
$qty = 0;
$after_qty = 0;
$dec = get_qty_dec($stock_id);

while ($myrow = db_fetch($result))
{

	alt_table_row_color($k);

	$trandate = sql2date($myrow["date"]);

	$type_name = $systypes_array[$myrow["trans_type"]];

	if ($myrow["movement_type"] == 1)
	{
		$qty = 1;
		$quantity_formatted = number_format2($qty, $dec);
		$total_in += $qty;
	}
	else
	{
		$qty = -1;
		$quantity_formatted = number_format2(abs($qty), $dec);
		$total_out += $qty;
	}
	$after_qty += $qty;

	label_cell($type_name);

	//label_cell(get_trans_view_str($myrow["trans_type"], $myrow["trans_no"]));

	if($myrow["trans_type"] == ST_CUSTDELIVERY || $myrow["trans_type"] == ST_SALESINVOICE || $myrow["trans_type"] == ST_CUSTCREDIT)
	{
		$ref_no = get_sales_ref_no($myrow["trans_no"], $myrow["trans_type"]);
		label_cell(get_trans_view_str($myrow["trans_type"], $myrow["trans_no"], $ref_no["location"]."-".$ref_no["form_type_no"]));
	}
	else if($myrow["trans_type"] == ST_CUSTPAYMENT)
	{
		$ref_no = get_sales_ref_no_OR_PR($myrow["trans_no"], $myrow["trans_type"]);
		label_cell(get_trans_view_str($myrow["trans_type"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"])));
	}
	else
	{
		$ref_no =  get_reference_no($myrow["trans_no"], $myrow["trans_type"]);	
		label_cell(get_trans_view_str($myrow["trans_type"], $myrow["trans_no"], $ref_no));
	}
	
	label_cell(get_location_name($myrow["loc_code"]));
	label_cell($trandate);

	$person = $myrow["person_id"];	
	$gl_posting = "";

	if (($myrow["trans_type"] == ST_CUSTDELIVERY) || ($myrow["trans_type"] == ST_CUSTCREDIT))
	{
		$cust_row = get_customer_details_from_trans($myrow["trans_type"], $myrow["trans_no"]);

		if (strlen($cust_row['name']) > 0)
			$person = $cust_row['name'] . " (" . $cust_row['br_name'] . ")";

	}
	elseif ($myrow["trans_type"] == ST_SUPPRECEIVE || $myrow['trans_type'] == ST_SUPPCREDIT)
	{
		// get the supplier name
		$sql = "SELECT supp_name FROM ".TB_PREF."suppliers WHERE supplier_id = '" . $myrow["person_id"] . "'";
		$supp_result = db_query($sql,"check failed");

		$supp_row = db_fetch($supp_result);

		if (strlen($supp_row['supp_name']) > 0)
			$person = $supp_row['supp_name'];
	}
	elseif ($myrow["trans_type"] == ST_LOCTRANSFER || $myrow["trans_type"] == ST_INVADJUST)
	{
		// get the adjustment type
		$movement_type = get_movement_type($myrow["person_id"]);
		$person = $movement_type["name"];
	}
	elseif ($myrow["trans_type"]==ST_WORKORDER || $myrow["trans_type"] == ST_MANUISSUE  ||
		$myrow["trans_type"] == ST_MANURECEIVE)
	{
		$person = "";
	}

	label_cell($person);

	label_cell((($qty >= 0) ? $quantity_formatted : ""), "nowrap align=right");
	label_cell((($qty < 0) ? $quantity_formatted : ""), "nowrap align=right");
	
	end_row();
	$j++;
	If ($j == 12)
	{
		$j = 1;
		table_header($th);
	}
//end of page full new headings if
}
//end of while loop


end_table(1);
div_end();
end_page(true);
?>