<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_ITEMSTRANSVIEW';
$path_to_root = "../..";

include($path_to_root . "/includes/session.inc");

page(_($help_context = "View Inventory Transfer"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

if (isset($_GET["trans_no"]))
{
	$trans_no = $_GET["trans_no"];
}

$transfer_items = get_stock_transfer_temp_2($trans_no);

$from_trans = $transfer_items[0];
$to_trans = $transfer_items[1];

display_heading($systypes_array[ST_TRANSFERSLIP] . " # " . get_reference(ST_TRANSFERSLIP, $trans_no));

br(1);
$adjustment_items = get_transfer_slip_items($trans_no);
$k = 0;
$header_shown = false;
while ($adjustment = db_fetch($adjustment_items))
{

	if (!$header_shown)
	{
		$adjustment_type = get_movement_type($adjustment['person_id']) ;

		start_table("$table_style2 width=90%");
		start_row();
		//label_cells(_("At Location"), $adjustment['location_name'], "class='tableheader2'");
		label_cells(_("From Location"), $from_trans['location_name'], "class='tableheader2'");
		label_cells(_("To Location"), $to_trans['location_name'], "class='tableheader2'");
    	//label_cells(_("Reference"), $adjustment['reference'], "class='tableheader2'", "colspan=6");
		label_cells(_("Date"), sql2date($adjustment['tran_date']), "class='tableheader2'");
		label_cells(_("Adjustment Type"), $adjustment_type['name'], "class='tableheader2'");
		end_row();
		
		$res = get_audit_trail_all(ST_TRANSFERSLIP, $trans_no);
		$audit_trail = "";	$x=1;
		while($audit = db_fetch($res))
		{
			$name = get_user($audit['user']);
			if($x == 1)
				$audit_trail .= $name['user_id']." - ".$audit['description'];
			else
				$audit_trail .= "<br>".$name['user_id']." - ".$audit['description'];
				
			$x++;
		}

		label_row(_("Audit Trail"), $audit_trail, "class='tableheader2'", "colspan=5");
		
		comments_display_row(ST_TRANSFERSLIP, $trans_no);

		end_table();
		$header_shown = true;

		echo "<br>";
		start_table("$table_style width=90%");

    	$th = array(_("Item"), _("Description"), _("Quantity"),
    		_("Units"), _("Serial No."), _("AIC"));
    	table_header($th);
	}

    alt_table_row_color($k);

    label_cell($adjustment['stock_id']);
    label_cell($adjustment['description']);
    qty_cell(abs($adjustment['qty']), false, get_qty_dec($adjustment['stock_id']));
    label_cell($adjustment['units']);	

	// $serial = get_serial_no($adjustment['trans_no'], 16, $adjustment['loc_code']);
	
	// $isn = "";
	// while($row = db_fetch($serial))
	// {
		// $isn .= $row["isn"] ."<br>";
	// }
	
	// label_cell($isn);	
	
	$isn=get_isn_out($trans_no, ST_TRANSFERSLIP, $adjustment["stock_id"], $adjustment['loc_code']);
	$aic=get_aic_out($trans_no, ST_TRANSFERSLIP, $adjustment["stock_id"], $adjustment['loc_code']);
	
	label_cell(str_replace("\n","<br>",$isn));
	label_cell(str_replace("\n","<br>",$aic));
	
    end_row();
}

end_table(1);

is_voided_display(ST_TRANSFERSLIP, $trans_no, _("This adjustment has been voided."));

start_table("$table_style width=80%");

start_row();

label_cell("Prepared By", "class='tableheader'");
label_cell("Checked By", "class='tableheader'");
label_cell("Approved By", "class='tableheader'");
label_cell("Received By", "class='tableheader'");

end_row();

start_row();

echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";

end_row();

end_table(2);

end_page(true);
?>