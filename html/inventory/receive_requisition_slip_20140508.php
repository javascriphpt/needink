<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_ITEMREQUISITION';
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/items_cart.inc");

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/includes/db/isn_db.inc");

include_once($path_to_root . "/inventory/includes/item_requisition_ui.inc");
include_once($path_to_root . "/inventory/includes/inventory_db.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/gl_ui.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Receive Item Requisition"), false, false, "", $js);

//-----------------------------------------------------------------------------------------------

if (isset($_GET["trans_no"]))
{
	$_POST["trans_no"] = $_GET["trans_no"];
}

//-----------------------------------------------------------------------------------------------

check_db_has_costable_items(_("There are no inventory items defined in the system (Purchased or manufactured items)."));

check_db_has_movement_types(_("There are no inventory movement types defined in the system. Please define at least one inventory adjustment type."));

//-----------------------------------------------------------------------------------------------

if (isset($_GET['AddedID'])) 
{
	$trans_no = $_GET['AddedID'];
	$trans_type = ST_LOCTRANSFER;

	display_notification_centered(_("Inventory transfer has been processed"));
	display_note(get_trans_view_str($trans_type, $trans_no, _("&View this transfer")));

	display_footer_exit();
}

//-----------------------------------------------------------------------------------------------

if (isset($_POST['Process']))
{
	global $Refs;

	$tr = &$_SESSION['transfer_items'];
	$input_error = 0;
		
	foreach($_POST as $postkey=>$postval )
    {
		if (strpos($postkey, 'qty_recd') === 0)
		{
			$id = substr($postkey, strlen('qty_recd'));
						
			$id_ = explode(',', $id);
			
			if ($_POST['qty_recd'.$id_[0]] <= 0)
			{
				display_error( _("The quantity entered must be a positive number."));
				set_focus('qty_recd'.$id_[0]);
				return false;
			}
			
			if ($_POST['qty_recd'.$id_[0]] > $_POST['qty'.$id_[0]])
			{
				display_error( _("Entered quantities cannot be greater than the quantity entered on the transfer."));
				set_focus('qty_recd'.$id_[0]);
				return false;
			}
		}
	}

	if ($input_error == 1)
		unset($_POST['Process']);
}

//-------------------------------------------------------------------------------

if (isset($_POST['Process']))
{
	begin_transaction();
	
	$transfer_id = get_next_trans_no(ST_LOCTRANSFER);
	$reference = $Refs->get_next(ST_LOCTRANSFER);
	
	foreach($_POST as $postkey=>$postval )
    {
		if (strpos($postkey, 'qty_recd') === 0)
		{
			$id = substr($postkey, strlen('qty_recd'));
						
			$id_ = explode(',', $id);
			
			
			$update = "UPDATE ".TB_PREF."stock_moves_temp
					SET qty = ".db_escape($_POST['qty_recd'.$id_[0]])."
					WHERE type = 167
					AND trans_no = ".db_escape($_POST["trans_no_2"])."
					AND trans_id = ".db_escape($id_[0])."";
			db_query($update, "could not update stock move temp");						
			
		}		
	}
	
	
	// stock moves temp
	$sql = "SELECT a.*
			FROM ".TB_PREF."stock_moves_temp a
			WHERE a.qty >= 0
			AND a.type = 167
			AND a.trans_no = ".db_escape($_POST["trans_no_2"]);

	$result = db_query($sql, "could not retieve stock moves temp");
	
	while($row = db_fetch($result))
	{
		$date_ = sql2date($row['tran_date']);
		add_stock_move(ST_LOCTRANSFER, $row['stock_id'], $transfer_id, $row['loc_code'],
			Today(), $reference, $row['qty'], 0, $row['person_id']);			
	}
	
	// isn trans
	$sql2 = "SELECT a.*
			FROM ".TB_PREF."isn_trans a
			WHERE a.trans_type = 167
			AND a.trans_no = ".db_escape($_POST["trans_no_2"]);

	$result2 = db_query($sql2, "could not retieve stock moves temp");
	
	while($row2 = db_fetch($result2))
	{
		update_isn_status($row2['stock_id'], $_POST['from_location'], $row2['isn'], 1);
		change_location($row2['stock_id'], $_POST['from_location'], $_POST['to_location'], $row2['isn']);
		add_isn_trail($row2['stock_id'], $row2['isn'], $_POST['to_location'], $transfer_id, ST_LOCTRANSFER, Today(), 1);
	}
	
	$delete = "UPDATE ".TB_PREF."stock_moves_temp
			SET received = 1
			WHERE type = 167
			AND trans_no = ".db_escape($_POST["trans_no_2"]);
	db_query($delete, "could not delete stock move temp");
	
	$delete2 = "UPDATE ".TB_PREF."stock_moves
			SET type = 16, trans_no =  ".db_escape($transfer_id)."
			WHERE type = 167
			AND trans_no = ".db_escape($_POST["trans_no_2"]);
	db_query($delete2, "could not update stock move");
	
	$delete3= "UPDATE ".TB_PREF."isn_trans
			SET trans_type = 16, trans_no = ".db_escape($transfer_id)."
			WHERE trans_type = 167
			AND trans_no = ".db_escape($_POST["trans_no_2"]);
	db_query($delete3, "could not update isn_trans");
	
	$delete3= "UPDATE ".TB_PREF."gl_trans
			SET type = 16, type_no = ".db_escape($transfer_id)."
			WHERE type = 167
			AND type_no = ".db_escape($_POST["trans_no_2"]);
	db_query($delete3, "could not update gl trans");
		
	add_comments(ST_LOCTRANSFER, $transfer_id, Today(), "Item Requisition No. ".get_reference(ST_REQUISITIONSLIP, $_POST["trans_no_2"])." - ".$_POST['memo_']);
	
	$Refs->save(ST_LOCTRANSFER, $transfer_id, $reference);
	add_audit_trail(ST_LOCTRANSFER, $transfer_id, $date_);
	
	commit_transaction();
	
	meta_forward($_SERVER['PHP_SELF'], "AddedID=".$transfer_id);
	
} /*end of process credit note */

//-----------------------------------------------------------------------------------------------
start_form();

$transfer_items = get_stock_requisition_temp_2($_POST["trans_no"]);

$from_trans = $transfer_items[0];
$to_trans = $transfer_items[1];

start_outer_table("width=70% $table_style");

table_section(1);
label_row(_("From Location:"), $from_trans['location_name'], "class='tableheader2'");
label_row(_("To Location:"), $to_trans['location_name'], "class='tableheader2'");
hidden('from_location', $from_trans['loc_code']);	
hidden('to_location', $to_trans['loc_code']);	
table_section(2, "33%");
label_row(_("Item Requisition No. :"), get_reference(ST_REQUISITIONSLIP, $_POST["trans_no"]), "class='tableheader2'");
label_row(_("Date:"), sql2date($from_trans['tran_date']), "class='tableheader2'");

hidden("transfer_date", sql2date($from_trans['tran_date']));

table_section(3, "33%");
$adjustment_type = get_movement_type($from_trans['person_id']) ;
label_row(_("Transfer Type:"), $adjustment_type['name'], "class='tableheader2'");

end_outer_table(1); // outer table

start_table("$table_style width=70%", 10);
start_row();
echo "<td>";

$transfer_items = get_stock_moves_temp(ST_REQUISITIONSLIP, $_POST["trans_no"]);

display_heading(_("Items"));
div_start('items_table');
start_table("$table_style width=80%");
$th = array(_("Item Code"), _("Item Description"), _("Quantity"), _("Serial No."), _("AIC"), _("Unit"), "Received");
table_header($th);
$subtotal = 0;
$k = 0;  //row colour counter

while ($item = db_fetch($transfer_items))
{
	if ($item['loc_code'] == $to_trans['loc_code'])
	{
		alt_table_row_color($k);

		label_cell($item['stock_id']);
		label_cell($item['description']);
		label_cell($item['qty']);
				
		$isn=get_isn_out($_POST["trans_no"], ST_REQUISITIONSLIP, $item['stock_id'], $from_trans['loc_code']);
		$aic=get_aic_out($_POST["trans_no"], ST_REQUISITIONSLIP, $item['stock_id'], $from_trans['loc_code']);
		
		label_cell(str_replace("\n","<br>",$isn));
		label_cell(str_replace("\n","<br>",$aic));
		
		label_cell($item['units']);
		label_cell($item['qty']);
		
		hidden('qty_recd'.$item['trans_id'], $item['qty']);
		hidden('qty'.$item['trans_id'], $item['qty']);
		hidden('trans_no_2', $_POST["trans_no"]);
		
		end_row();	
	}
}

end_table();
div_end();

gl_options_controls();

echo "</td>";
end_row();
end_table(1);

submit_center_first('Update', _("Update"), '', null);
submit_center_last('Process', _("Receive Transfer"), '',  'default');

end_form();
end_page();

?>
