<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//-----------------------------------------------------------------------------
//
//	Entry/Modify Delivery Note against Sales Order
//
$page_security = 'SA_ITEMREQUISITION';
$path_to_root = "..";

include_once($path_to_root . "/sales/includes/cart_class.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/db/isn_db.inc");
include_once($path_to_root . "/includes/data_checks.inc");
include_once($path_to_root . "/includes/manufacturing.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");
include_once($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
include_once($path_to_root . "/taxes/tax_calc.inc");

$js = "";
if ($use_popup_windows) {
	$js .= get_js_open_window(900, 500);
}
if ($use_date_picker) {
	$js .= get_js_date_picker();
}

if (isset($_GET['trans_no'])) {
	$_SESSION['page_title'] = _($help_context = "Receive Item Requisition");
	processing_start();
}

page($_SESSION['page_title'], false, false, "", $js);

if (isset($_GET['AddedID'])) {
	$dispatch_no = $_GET['AddedID'];
	$ref_no = get_sales_ref_no($dispatch_no, ST_LOCTRANSFER);

	display_notification_centered(_("Location Transfer # ".get_reference_no($dispatch_no, ST_LOCTRANSFER)." has been entered."));
	
	display_note(get_trans_view_str(ST_LOCTRANSFER, $dispatch_no, _("&View This Location Transfer")));
	
	display_note(get_gl_view_str(ST_LOCTRANSFER, $dispatch_no, _("View the GL Journal Entries for this Location Transfer")),1);

	hyperlink_params("$path_to_root/inventory/inquiry/requisition_slip_inquiry.php", _("Select Another Item Requisition To Receive"));

	display_footer_exit();

}
//-----------------------------------------------------------------------------

if (isset($_GET['trans_no']) && $_GET['trans_no'] > 0) {

	$ord = new Cart(ST_REQUISITIONSLIP, $_GET['trans_no'], true);

	/*read in all the selected order into the Items cart  */

	if ($ord->count_items() == 0) {
		hyperlink_params($path_to_root . "/inventory/inquiry/requisition_slip_inquiry.php",
			_("Select a different item requisition to receive"));
		die ("<br><b>" . _("This item requisition has no items. There is nothing to receive.") . "</b>");
	}

	$ord->trans_type = ST_LOCTRANSFER;
	$ord->src_docs = $ord->trans_no;
	$ord->order_no = key($ord->trans_no);
	$ord->trans_no = 0;
	$ord->reference = $Refs->get_next(ST_LOCTRANSFER);
	$ord->document_date = new_doc_date();
	$_SESSION['Items'] = $ord;
	copy_from_cart();

} elseif ( !processing_active() ) {
	/* This page can only be called with an order number for invoicing*/

	display_error(_("This page can only be opened if an item requisition has been selected. Please select it first."));

	hyperlink_params("$path_to_root/inventory/inquiry/requisition_slip_inquiry.php", _("Select an Item Requisition to Receive"));

	end_page();
	exit;

} else {
	// check_edit_conflicts();

	// if (!check_quantities()) {
		// display_error(_("Selected quantity cannot be more than quantity	not receive on item requisition."));

	// }
}

//-----------------------------------------------------------------------------

function check_data()
{
	global $Refs;

	if (!isset($_POST['DispatchDate']) || !is_date($_POST['DispatchDate']))	{
		display_error(_("The entered date of delivery is invalid."));
		set_focus('DispatchDate');
		return false;
	}

	if (!is_date_in_fiscalyear($_POST['DispatchDate'])) {
		display_error(_("The entered date of delivery is not in fiscal year."));
		set_focus('DispatchDate');
		return false;
	}

	if ($_SESSION['Items']->trans_no==0) {
		if (!$Refs->is_valid($_POST['ref'])) {
			display_error(_("You must enter a reference."));
			set_focus('ref');
			return false;
		}
	}
	
	foreach ($_SESSION['Items']->line_items as $line=>$itm)
	{
		$balance = $itm->qty_dispatched - $itm->qty_done;
		$receive = input_num('Line'.$line);
		
		// display_error($itm->qty_dispatched." => ".$itm->qty_done." => ".input_num('Line'.$line));
		if($balance > $receive)
		{
			display_error(_("Quantity received is greater than the balance."));
			set_focus('Line'.$line);
			return false;
		}
	}
	
	if (!check_quantities()) {
		return false;
	}
	$check_isn=check_isn();
	if(!$check_isn){
			display_error(_("Please complete the serial number entry. Duplicate values will be automatically removed."));
			return false;
	}
	else if($check_isn == 2){
			display_error(_("One or more entered serial number is invalid."));
			return false;
	}

	return true;
}
//------------------------------------------------------------------------------
function copy_to_cart()
{
	$cart = &$_SESSION['Items'];
	$cart->document_date = $_POST['DispatchDate'];
	$cart->due_date =  $_POST['due_date'];
	$cart->Location = $_POST['Location'];
	$cart->Comments = $_POST['Comments'];
	if ($cart->trans_no == 0)
		$cart->reference = $_POST['ref'];

}
//------------------------------------------------------------------------------

function copy_from_cart()
{
	$cart = &$_SESSION['Items'];
	$_POST['DispatchDate'] = $cart->document_date;
	$_POST['due_date'] = $cart->due_date;
	$_POST['Location'] = $cart->Location;
	$_POST['Comments'] = $cart->Comments;
	$_POST['cart_id'] = $cart->cart_id;
	$_POST['ref'] = $cart->reference;
}
//------------------------------------------------------------------------------
function check_isn(){
	$serial_complete = 1;		
	foreach ($_SESSION['Items']->line_items as $order_line)
	{
		$stock_det=get_item($order_line->stock_id);
		if($stock_det['serialize'] && $order_line->qty_dispatched>0){
		$serials=explode("\n",$order_line->serial);
		$serials=array_unique($serials);
		$serial_count=0;
		$company_data = get_company_prefs();
		$from_location = $company_data["default_item_requisition_from_location"];
		foreach($serials as $s)
			if($s){ 
				$serial_count++; 
				if(!check_exist_isn($order_line->stock_id, $from_location,$s)){
						return 2;
					}
				}
		//display_error($order_line->receive_qty+$order_line->qty_received." ".$serial_count);
		
		
		$serials=implode("\n",$serials);
		
		$order_line->serial = $serials;
		
	  	if ($order_line->qty_dispatched > $serial_count)
	  	{
			$serial_complete = 0;
			break;
	  	}
		}
	}
	
	return $serial_complete;
	

}
function check_quantities()
{
	$ok =1;
	// Update cart delivery quantities/descriptions
	foreach ($_SESSION['Items']->line_items as $line=>$itm) {
		if (isset($_POST['Line'.$line])) {
		if($_SESSION['Items']->trans_no) {
			$min = $itm->qty_done;
			$max = $itm->quantity;
		} else {
			$min = 0;
			$max = $itm->quantity - $itm->qty_done;
		}
		
			if (check_num('Line'.$line, $min, $max)) {
				$_SESSION['Items']->line_items[$line]->qty_dispatched =
				  input_num('Line'.$line);
			} else {
				set_focus('Line'.$line);
				$ok = 0;
			}

		}

		if (isset($_POST['Line'.$line.'Desc'])) {
			$line_desc = $_POST['Line'.$line.'Desc'];
			if (strlen($line_desc) > 0) {
				$_SESSION['Items']->line_items[$line]->item_description = $line_desc;
			}
		}
				$_SESSION['Items']->line_items[$line]->serial = $_POST['Line'.$line.'serial'];
		
	}
// ...
//	else
//	  $_SESSION['Items']->freight_cost = input_num('ChargeFreightCost');
	return $ok;
}
//------------------------------------------------------------------------------

function check_qoh()
{
	global $SysPrefs;

	if (!$SysPrefs->allow_negative_stock())	{
		foreach ($_SESSION['Items']->line_items as $itm) {

			if ($itm->qty_dispatched && has_stock_holding($itm->mb_flag)) {
				$company_data = get_company_prefs();
				$from_location = $company_data["default_item_requisition_from_location"];
				$qoh = get_qoh_on_date($itm->stock_id, $from_location, $_POST['DispatchDate']);

				if ($itm->qty_dispatched > $qoh) {
					display_error(_("The delivery cannot be processed because there is an insufficient quantity for item:") .
						" " . $itm->stock_id . " - " .  $itm->item_description);
					return false;
				}
			}
		}
	}
	return true;
}
//------------------------------------------------------------------------------

if (isset($_POST['process_delivery']) && check_data() && check_qoh()) {

	$dn = &$_SESSION['Items'];

	$bo_policy = 1;
	$newdelivery = ($dn->trans_no == 0);

	copy_to_cart();
	if ($newdelivery) new_doc_date($dn->document_date);
	$delivery_no = $dn->write($bo_policy);

	processing_end();
	if ($newdelivery) {
		meta_forward($_SERVER['PHP_SELF'], "AddedID=$delivery_no");
	} else {
		meta_forward($_SERVER['PHP_SELF'], "UpdatedID=$delivery_no");
	}
}

if (isset($_POST['Update']) || isset($_POST['_Location_update'])) {
	$Ajax->activate('Items');
}
//------------------------------------------------------------------------------

start_form();
hidden('cart_id');

start_table("$table_style2 width=70%", 5);
echo "<tr><td>"; // outer table

start_table("$table_style2 width=100%");
start_row();
$company_data = get_company_prefs();
$from_location = $company_data["default_item_requisition_from_location"];
label_cells(_("From Location"), get_location_name($from_location), "class='tableheader2'");
label_cells(_("Item Requisition No."), get_trans_view_str(ST_REQUISITIONSLIP, $_SESSION['Items']->order_no, get_reference_no($_SESSION['Items']->order_no, ST_REQUISITIONSLIP)), "class='tableheader2'");
end_row();

start_row();
if (!isset($_POST['Location'])) {
	$_POST['Location'] = $_SESSION['Items']->Location;
}
hidden('Location',$_POST['Location']);
label_cells(_("To Location"), get_location_name($_SESSION['Items']->Location), "class='tableheader2'");
date_cells(_("Date"), 'DispatchDate', '', $_SESSION['Items']->trans_no==0, 0, 0, 0, "class='tableheader2'");
end_row();

if (!isset($_POST['ref']))
	$_POST['ref'] = $Refs->get_next(ST_LOCTRANSFER);
hidden('ref',$_POST['ref']);

end_table();

echo "</td>";// outer table

end_table(1); // outer table

display_heading(_("Items"));
div_start('Items');
start_table("$table_style width=80%");

$new = $_SESSION['Items']->trans_no==0;
$th = array(_("Item Code"), _("Item Description"), _("Quantity"), _("Units"), _("Received"), _("This Receive"), _("Serial No."));

table_header($th);
$k = 0;
$has_marked = false;
foreach ($_SESSION['Items']->line_items as $line=>$ln_itm) {
	$item_det=get_item($ln_itm->stock_id);

	if ($ln_itm->quantity==$ln_itm->qty_done) {
		continue; //this line is fully delivered
	}
	// if it's a non-stock item (eg. service) don't show qoh
	$show_qoh = true;
	if ($SysPrefs->allow_negative_stock() || !has_stock_holding($ln_itm->mb_flag) ||
		$ln_itm->qty_dispatched == 0) {
		$show_qoh = false;
	}

	if ($show_qoh) {
		$company_data = get_company_prefs();
		$from_location = $company_data["default_item_requisition_from_location"];
		$qoh = get_qoh_on_date($ln_itm->stock_id, $from_location, $_POST['DispatchDate']);
	}

	if ($show_qoh && ($ln_itm->qty_dispatched > $qoh)) {
		// oops, we don't have enough of one of the component items
		start_row("class='stockmankobg'");
		$has_marked = true;
	} else {
		alt_table_row_color($k);
	}
	view_stock_status_cell($ln_itm->stock_id);

	label_cell($ln_itm->item_description);
	$dec = get_qty_dec($ln_itm->stock_id);
	qty_cell($ln_itm->quantity, false, $dec);
	label_cell($ln_itm->units);
	qty_cell($ln_itm->qty_done, false, $dec);

	small_qty_cells(null, 'Line'.$line, qty_format(0, $ln_itm->stock_id, $dec), null, null, $dec);

	if($item_det['serialize']){
		textarea_cells("","Line".$line."serial",$ln_itm->serial,25,3);
	}
	else label_cell("");
	
	end_row();
}

end_table(1);

if ($has_marked) {
	display_note(_("Marked items have insufficient quantities in stock as on day of delivery."), 0, 1, "class='red'");
}
start_table($table_style2);

textarea_row(_("Memo"), 'Comments', null, 50, 4);

end_table(1);
div_end();
submit_center_first('Update', _("Update"),
  _('Refresh document page'), true);
submit_center_last('process_delivery', _("Process Item Transfer"),
  _('Check entered data and save document'), 'default');

end_form();


end_page();

?>
