<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_APVVOUCHER';
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/items_cart_apv.inc");

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/ui/gl_apv_ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/gl_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

$js = '';
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

if (isset($_GET['ModifyGL'])) {
	$_SESSION['page_title'] = sprintf(_("Modifying APV # %d."), 
		$_GET['trans_no']);
	$help_context = "Modifying APV Entry";
} else
	$_SESSION['page_title'] = _($help_context = "Accounts Payable Voucher Entry");

page($_SESSION['page_title'], false, false,'', $js);
//--------------------------------------------------------------------------------------------------

function line_start_focus() {
  global 	$Ajax;

  if ($_POST['invoice_no'] != '')
  $_POST['AmountDebit'] = get_amount($_POST['invoice_no'])-$_SESSION['apv_items']->gl_items_total_debit();
  
  if ($_POST['AmountDebit'] < 0 )
		$_POST['AmountDebit'] = 0;
  
  $Ajax->activate('items_table');
  set_focus('_code_id_edit');
}
//-----------------------------------------------------------------------------------------------

// if (isset($_GET['AddedID'])) 
// {
	
	// print_hidden_script(45);
	
	// $trans_no = $_GET['AddedID'];
	// $trans_type = systypes::apv();
	
   	// display_notification_centered( _("Accounts Payable Voucher #$trans_no has been entered"));

    // display_note(get_apv_view_str($trans_no, _("View this Accounts Payable Voucher")));

   	// hyperlink_no_params($_SERVER['PHP_SELF'], _("Enter Accounts Payable Voucher"));

	// echo '<br>';
	// display_note(print_document_link($trans_no, _("Print This APV"),true,45));
	
	// display_footer_exit();
// }
if (isset($_GET['AddedID'])) 
{
	$trans_no = $_GET['AddedID'];
	$trans_type = ST_APVVOUCHER;

   	display_notification_centered( _("Accounts Payable Voucher has been entered") . " #".get_reference_no($trans_no, ST_APVVOUCHER));

    display_note(get_gl_view_str($trans_type, $trans_no, _("&View this Accounts Payable Voucher")));

	reset_focus();
	hyperlink_params($_SERVER['PHP_SELF'], _("Enter &New Accounts Payable Voucher"), "NewAPV=Yes");
	
	// echo "<br>";
	// submenu_print(_("&Print This Accounts Payable Voucher"), ST_APVVOUCHER, $trans_no, 'prtopt');

	display_footer_exit();
}
if (isset($_POST['CancelChanges'])) 
{	
   	display_notification_centered( _("Changes Canceled"));
	unset($_POST);
	unset($_SESSION['apv_items']);
	hyperlink_no_params($_SERVER['PHP_SELF'], _("Enter Accounts Payable Voucher"));
	
	display_footer_exit();
}
//--------------------------------------------------------------------------------------------------

function handle_new_order()
{
	if (isset($_SESSION['apv_items']))
	{
		$_SESSION['apv_items']->clear_items();
		unset ($_SESSION['apv_items']);
	}

    session_register("apv_items");
	
    $_SESSION['apv_items'] = new items_cart(ST_APVVOUCHER);
	
	$_POST['date_'] = Today();
	if (!is_date_in_fiscalyear($_POST['date_']))
		$_POST['date_'] = end_fiscalyear();
	$_SESSION['apv_items']->tran_date = $_POST['date_'];	
}

//-----------------------------------------------------------------------------------------------

function handle_modify_order(){

	handle_new_order();

	$apv_no=$_GET['ModifyOrderNumber'];
	$apv_header = get_apv_header($apv_no);
	
	$_POST['voucher_account']=$apv_header['voucher_account'];
	$_POST['PayType']=$apv_header['cust_supp'];
	$_POST['person_id']=$apv_header['cust_supp_id'];
	if($_POST['PayType'] == PT_SUPPLIER/*payment_person_types::supplier()*/){
		$_POST['invoice_no']=$apv_header['invoice_no'];
	}
	
	$_POST['terms']=$apv_header['terms_indicator'];
	$_POST['date_']=sql2date($apv_header['tran_date']);
	$_POST['duedate_']=sql2date($apv_header['due_date']);
	$_POST['explanation_']=$apv_header['explanation'];
	
	$sql=get_apv_details($apv_no);
	while($myrow=db_fetch($sql)){
		if($myrow['account']!=$_POST['voucher_account'])
		$_SESSION['apv_items']->add_gl_item($myrow['account'], '', $myrow['dimension_id'],
			$myrow['dimension2_id'], 0, 0, 0, $myrow['amount'], '');	
	}
}

//-----------------------------------------------------------------------------------------------

if (isset($_POST['UpdateAPV'])){
	$input_error = 0;
	if ($_SESSION['apv_items']->count_gl_items() < 1) {
		display_error(_("You must enter at least one GL line."));
		set_focus('code_id');
		$input_error = 1;
	}
	if ($_POST['person_id'] == ''  AND $_POST['PayType'] == PT_MISC/*payment_person_types::misc()*/) 
	{
		display_error( _("No Payee"));
		set_focus('cust_supp');
		$input_error = 1;
	}
	
	// if ($_POST['invoice_no'] == 0 AND $_POST['PayType'] == PT_SUPPLIER/*payment_person_types::supplier()*/) 
	// {
		// display_error( _("No Invoice number"));
		// set_focus('invoice_no');
		// $input_error = 1;
	// }
	if (!is_date($_POST['date_'])) 
	{	
		display_error(_("The entered date is invalid."));
		set_focus('date_');
		$input_error = 1;
	} 
	elseif (!is_date_in_fiscalyear($_POST['date_'])) 
	{
		display_error(_("The entered date is not in fiscal year."));
		set_focus('date_');
		$input_error = 1;
	} 
	
	if (!is_date($_POST['duedate_'])) 
	{
		display_error( _("Invalid due date"));
		set_focus('duedate_');
		$input_error = 1;
	} 
	
	if ($input_error == 1)
		unset($_POST['UpdateAPV']);
}

if(isset($_POST['UpdateAPV'])){
	$apv_no=$_GET['ModifyOrderNumber'];

	$trans_no = update_apv_entries($_SESSION['apv_items'],
		$_POST['ref'], $_POST['PayType'], $_POST['person_id'], 0 /*$_POST['po_no']*/,
		$_POST['invoice_no'], $_POST['date_'], $_POST['terms'], $_POST['duedate_'],
		$_POST['explanation_'],$_POST['voucher_account']);
	$_SESSION['apv_items']->clear_items();
	
	unset($_SESSION['apv_items']);
	meta_forward($_SERVER['PHP_SELF'], "AddedID=$trans_no");
}

//-----------------------------------------------------------------------------------------------

if (isset($_POST['Process']))
{
	$input_error = 0;

	if ($_SESSION['apv_items']->count_gl_items() < 1) {
		display_error(_("You must enter at least one GL line."));
		set_focus('code_id');
		$input_error = 1;
	}
	
	if ($_POST['person_id'] == ''  AND $_POST['PayType'] ==  PT_MISC/*payment_person_types::misc()*/) 
	{
		display_error( _("No Payee"));
		set_focus('cust_supp');
		$input_error = 1;
	}
	
	if ($_POST['invoice_no'] == 0 AND $_POST['PayType'] == PT_SUPPLIER/*payment_person_types::supplier()*/) 
	{
		display_error( _("No Invoice number"));
		set_focus('invoice_no');
		$input_error = 1;
	}
		
	if (!is_date($_POST['date_'])) 
	{	
		display_error(_("The entered date is invalid."));
		set_focus('date_');
		$input_error = 1;
	} 
	elseif (!is_date_in_fiscalyear($_POST['date_'])) 
	{
		display_error(_("The entered date is not in fiscal year."));
		set_focus('date_');
		$input_error = 1;
	} 
	
	if (!is_date($_POST['duedate_'])) 
	{
		display_error( _("Invalid due date"));
		set_focus('duedate_');
		$input_error = 1;
	} 
	
	
	if (!references::is_valid($_POST['ref'])) 
	{
		display_error( _("You must enter a reference."));
		set_focus('ref');
		$input_error = 1;
	} 
	elseif (references::exists(ST_APVVOUCHER/*systypes::apv()*/, $_POST['ref'])) 
	{
		display_error( _("The entered reference is already in use."));
		set_focus('ref');
		$input_error = 1;
	}
	
	if ($input_error == 1)
		unset($_POST['Process']);
		
}

if (isset($_POST['Process']))
{
	$trans_no = add_apv_entries($_SESSION['apv_items'],
		$_POST['ref'], $_POST['PayType'], $_POST['person_id'], 0 /*$_POST['po_no']*/,
		$_POST['invoice_no'], $_POST['date_'], $_POST['terms'], $_POST['duedate_'],
		$_POST['explanation_'],$_POST['voucher_account']);
	$_SESSION['apv_items']->clear_items();
	
	unset($_SESSION['apv_items']);
	meta_forward($_SERVER['PHP_SELF'], "AddedID=$trans_no");

} /*end of process APV*/

//-----------------------------------------------------------------------------------------------

function check_item_data()
{
	if (isset($_POST['dimension_id']) && $_POST['dimension_id'] != 0 && dimension_is_closed($_POST['dimension_id'])) 
	{
		display_error(_("Dimension is closed."));
		set_focus('dimension_id');
		return false;
	}

	if (isset($_POST['dimension2_id']) && $_POST['dimension2_id'] != 0 && dimension_is_closed($_POST['dimension2_id'])) 
	{
		display_error(_("Dimension is closed."));
		set_focus('dimension2_id');
		return false;
	}
	
	if (strlen($_POST['AmountDebit']) && !check_num('AmountDebit', 0)) 
	{
		display_error(_("The amount entered is not a valid number or is less than zero."));
		set_focus('AmountDebit');
		return false;
  	}elseif (strlen($_POST['AmountCredit']) && !check_num('AmountCredit', 0))
	{
    		display_error(_("The credit amount entered is not a valid number or is less than zero."));
		set_focus('AmountCredit');
    		return false;
  	}

	if ($_SESSION["wa_current_user"]->access != 2 && is_bank_account($_POST['code_id'])) 
	{
		display_error(_("You cannot make a GL entry for a bank account. Please use one of the banking functions for bank transactions."));
		set_focus('code_id');
		return false;
	}

   	return true;
}

//-----------------------------------------------------------------------------------------------

function handle_update_item()
{
    if($_POST['UpdateItem'] != "" && check_item_data())
    {
    	if (input_num('AmountDebit') > 0)
    		$amount = input_num('AmountDebit');
    	else
    		$amount = -input_num('AmountCredit');

    	$_SESSION['apv_items']->update_gl_item($_POST['Index'], $_POST['dimension_id'],
    		$_POST['dimension2_id'], 0, 0, 0, $amount, $_POST['doc_ref']);
    }
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------

function handle_delete_item($id)
{
	$_SESSION['apv_items']->remove_gl_item($id);
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------

function handle_new_item()
{
	if (!check_item_data())
		return;

	if (input_num('AmountDebit') > 0)
		$amount = input_num('AmountDebit');
	else if(input_num('AmountCredit') > 0)
		$amount = -(input_num('AmountCredit'));
	
	$_SESSION['apv_items']->add_gl_item($_POST['code_id'], '', $_POST['dimension_id'],
		$_POST['dimension2_id'], 0, 0, 0, $amount, '');
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------
$id = find_submit('Delete');
if ($id != -1)
	handle_delete_item($id);

if (isset($_POST['AddItem'])) 
	handle_new_item();

if (isset($_POST['UpdateItem'])) 
	handle_update_item();
	
if (isset($_POST['CancelItemChanges']))
	line_start_focus();

//-----------------------------------------------------------------------------------------------
if (isset($_GET['ModifyOrderNumber']) && $_GET['ModifyOrderNumber'] != "")
{
	handle_modify_order();
}

if (isset($_GET['NewAPV']) || !isset($_SESSION['apv_items']))
{
	handle_new_order();
}

//-----------------------------------------------------------------------------------------------

start_form();

display_order_header($_SESSION['apv_items']);

start_table("$table_style2 width=90%", 10);
start_row();
echo "<td>";
gl_options_controls();
display_gl_items(_("Accounts"), $_SESSION['apv_items']);
echo "</td>";
end_row();
end_table(1);

if (!isset($_GET['ModifyOrderNumber'])) {
submit_center('Process', _("Process APV"), true , 
	_('Process APV entry only if debits equal to credits'), true);
}else{
	submit_center_first('UpdateAPV', _("Update APV"), '', true);
		submit_center_last('CancelChanges', _("Cancel Changes")); 
}

end_form();
//------------------------------------------------------------------------------------------------

end_page();

?>
