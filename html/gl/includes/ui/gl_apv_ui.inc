<?php

include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/ui/items_cart_apv.inc");

//--------------------------------------------------------------------------------

function get_amount($trans_no)
{
	$sql = "SELECT ov_amount+ov_gst-ov_discount
				FROM ".TB_PREF."supp_trans 
				WHERE trans_no = $trans_no
				AND type = 20";
	$res = db_query($sql,"failed to get remaining amount");
	
	$row1 = db_fetch($res);
	$sql = "SELECT SUM(amount) FROM ".TB_PREF."apv_header a, ".TB_PREF."apv_details b
										WHERE invoice_no = $trans_no
										AND a.ref = b.apv_no
										AND amount >= 0";
	$res = db_query($sql,"failed to get remaining amount");
	$row2 = db_fetch($res);
	
	return $row1[0] - $row2[0];

}

function display_order_header(&$Order)
{
	global $table_style2, $Ajax;
	$Edit=isset($_GET['ModifyOrderNumber']);
	div_start('apv_header');
	start_table("$table_style2 width=90%"); //outer table
	echo "<tr><td valign=top width=50%>";

	echo "<table class=$table_style2>";
	$payment = true;
	if (!isset($_POST['PayType']))
	{
		if (isset($_GET['PayType']))
			$_POST['PayType'] = $_GET['PayType'];
		else
			$_POST['PayType'] = PT_SUPPLIER /*$payment_person_types = array (PT_SUPPLIER)*/ ;
	}
	
	if($Edit==0)
	{
		$ref_ = references::get_next(ST_APVVOUCHER);
	}
	else
	{
		$apv_no=$_GET['ModifyOrderNumber'];
		$apv_header = get_apv_header($apv_no);
		
		$sql = "SELECT ref FROM ".TB_PREF."apv_header WHERE id=".db_escape($apv_no);
		$result = db_query($sql, "could not retreive ref for $apv_no");
		$apv_header = db_fetch($result);
		
		$ref_=$apv_header['ref'];	
	}
	hidden('ref',$ref_);
		
	ref_cells(_("APV NO.: "), 'ref', '', $ref_);
	
	end_row();
	
	gl_all_accounts_list_row('Voucher Account:','voucher_account', null, false, false, false, 13);	
		
	if (isset($_POST['_PayType_update'])) {
		$_POST['person_id'] = '';
		$Ajax->activate('apv_header');
		$Ajax->activate('code_id');
	}
	if (isset($_POST['_person_id_update'])) {
		$_POST['invoice_no'] = '';
		$Ajax->activate('invoice_no');
	}
	
	if (isset($_POST['_invoice_no_update'])) {
		if ($_POST['invoice_no'] != '')
		$_POST['AmountDebit'] = get_amount($_POST['invoice_no'])-$_SESSION['apv_items']->gl_items_total_debit();
		if ($_POST['AmountDebit'] < 0 )
		$_POST['AmountDebit'] = 0;
		$Ajax->activate('AmountDebit');
	}
	
	payment_person_types_list_row2( $payment ? _("Pay To:"):_("From:"), 'PayType', $_POST['PayType'], true);
	
    switch ($_POST['PayType'])
    {
		case PT_MISC :	//payment_person_types::misc() :
    		text_row_ex($payment ?_("To the Order of:"):_("Name:"),
				 'person_id', 40, 50);
    		break;
		case PT_SUPPLIER :	//payment_person_types::supplier() :
			supplier_list_row(_("Supplier:"), 'person_id', null, false, true);
    		break;
		case PT_CUSTOMER : //payment_person_types::customer() :
    		customer_list_row(_("Customer:"), 'person_id', null, false, true);
    		break;
			
		case PT_QUICKENTRY : //payment_person_types::customer() :
    		text_row_ex($payment ?_("To the Order of:"):_("Name:"),
				 'person_id', 40, 50);
    		break;

	}
	
	if($_POST['PayType'] == PT_SUPPLIER)	//payment_person_types::supplier())
		supplier_invoice_row(_("Invoice #:"), 'invoice_no', $_POST['person_id']);

	echo "</table>";

	echo "</td><td width=50%>"; // outer table

	echo "<table class=$table_style2>";
	date_row(_("Date: "), 'date_');
	payment_terms_list_row(_("Terms: "), 'terms');
	date_row(_("Due Date: "), 'duedate_');
	
	echo "</table>";

	echo "</td></tr>"; // outer table
	end_table(1);
	div_end();
}

//---------------------------------------------------------------------------------

function display_gl_items($title, &$order)
{
	global $table_style, $path_to_root;

	display_heading($title);

	$dim = get_company_pref('use_dimension');

    div_start('items_table');
	start_table("$table_style colspan=7 width=95%");
	if ($dim == 2)
		$th = array(_("Account Code"), _("Account Title"), _("Dimension")." 1", 
			_("Dimension")." 2", _("Debit"),_("Credit"),'');
	else if ($dim == 1)
		$th = array(_("Account Code"), _("Account Title"), _("Dimension"), 
			_("Debit"), _("Credit"),'');
	else
		$th = array(_("Account Code"), _("Account Title"), 
			_("Debit"), _("Credit"),'');

	if (count($order->gl_items)) $th[] = '';

	table_header($th);	

	$k = 0;

	$id = find_submit('Edit');
	// foreach ($order->gl_items as $item) 
	foreach ($order->gl_items as $line => $item)
	{
		// if ($id != $item->index)
		if ($id != $line)
		{
    		alt_table_row_color($k);
			label_cells($item->code_id, $item->description);
			
    		if ($dim >= 1)
   				label_cell(get_dimension_string($item->dimension_id, true));
    		if ($dim > 1)
   				label_cell(get_dimension_string($item->dimension2_id, true));
				
		
    		if ($item->amount > 0)
    		{
    			amount_cell(abs($item->amount));
    			label_cell("");
    		}	
			else
    		{
    			label_cell('');
    			amount_cell(abs($item->amount));
    		}	
			// edit_button_cell("Edit$item->index", _("Edit"),
				// _('Edit journal line'));
			// edit_button_cell("Delete$item->index", _("Delete"),
				// _('Remove line from journal'));
			edit_button_cell("Edit$line", _("Edit"),
				_('Edit document line'));
			delete_button_cell("Delete$line", _("Delete"),
				_('Remove line from document'));
    		end_row();
		} 
		else 
		{
			gl_edit_item_controls($order, $dim, $item->index);
		}
	}

	if ($id == -1)
		gl_edit_item_controls($order, $dim);

	if ($order->count_gl_items()) 
	{
		$colspan = ($dim == 2 ? "3" : ($dim == 1 ? "2" : "1"));
		start_row();
		label_cell("");
		label_cell(_("Total"), "align=right colspan=" . $colspan);
		amount_cell($order->gl_items_total_debit());
		amount_cell(abs($order->gl_items_total_credit()));
		label_cell("");
		label_cell("");
		end_row();
	}

    end_table();
	div_end();
}

//---------------------------------------------------------------------------------

function gl_edit_item_controls(&$order, $dim, $Index=null)
{
	global $Ajax;
	start_row();

	$id = find_submit('Edit');
	if ($Index != -1 && $Index == $id)
	{
		$item = $order->gl_items[$Index];
		$_POST['code_id'] = $item->code_id;
		$_POST['dimension_id'] = $item->dimension_id;
		$_POST['dimension2_id'] = $item->dimension2_id;
		$_POST['AmountDebit'] = price_format($item->amount);
		$_POST['AmountCredit'] = price_format(abs($item->amount));
		$_POST['description'] = $item->description;
		$_POST['doc_ref'] = $item->reference;
		if ($_POST['AmountDebit'] < 0)
			$_POST['AmountDebit'] = 0;
		else
			$_POST['AmountCredit'] = 0;
		//$_POST['amount'] = 0;
		
		hidden('Index', $item->index);
		hidden('code_id', $item->code_id);
		label_cell($_POST['code_id']);
		label_cell($item->description);
		//label_cell();
		if ($dim >= 1) 
			dimensions_list_cells(null, 'dimension_id', null, true, " ", false, 1);
		if ($dim > 1) 
			dimensions_list_cells(null, 'dimension2_id', null, true, " ", false, 2);
	    $Ajax->activate('items_table');
	}
	else
	{
		//$_POST['amount'] = 0;
		//$_POST['AmountDebit'] = '';  //price_format(0);
		$_POST['AmountCredit'] = ''; //price_format(0);
		$_POST['dimension_id'] = 0;
		$_POST['dimension2_id'] = 0;
		//$_POST['doc_ref'] = "";
		$_POST['_code_id_edit'] = "";
		$_POST['code_id'] = "";
		if(isset($_POST['_code_id_update'])) {
			    $Ajax->activate('code_id');
		}
		
			
		$skip_bank = !$_SESSION["wa_current_user"]->can_access('SA_BANKJOURNAL');
		echo gl_all_accounts_list('code_id', null, $skip_bank, true);
		if ($dim >= 1)
			dimensions_list_cells(null, 'dimension_id', null, true, " ", false, 1);
		if ($dim > 1)
			dimensions_list_cells(null, 'dimension2_id', null, true, " ", false, 2);
	}
	if ($dim < 1)
		hidden('dimension_id', 0);
	if ($dim < 2)
		hidden('dimension2_id', 0);
	
	//ref_cells('', 'doc_ref');
	amount_cells(null, 'AmountDebit');
	amount_cells(null, 'AmountCredit');
	//text_cells_ex(null, 'doc_ref', 35, 50);
	
	if ($id != -1)
	{
		edit_button_cell('UpdateItem', _("Update"),
				_('Confirm changes'));
		edit_button_cell('CancelItemChanges', _("Cancel"),
				_('Cancel changes'));
 		//set_focus('amount');
	} 
	else 
		submit_cells('AddItem', _("Add Item"), "colspan=2",
		    _('Add new line to journal'), true);

	end_row();
}

//---------------------------------------------------------------------------------

function gl_options_controls()
{
	  echo "<br><table align='center'>";

	  textarea_row(_("Explanation"), 'explanation_', null, 70, 5);

	  echo "</table><br>";
}


//---------------------------------------------------------------------------------

?>