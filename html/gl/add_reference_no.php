<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_BANKTRANSVIEW';
$path_to_root="../..";
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/items_cart_apv.inc");

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/ui/gl_apv_ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/gl/includes/gl_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Add Reference #"), false, false, "", $js);

//----------------------------------------------------------------------------------------

if (isset($_POST['Process']))
{
	global $systypes_array;
	$error = 0;
	
	if (strlen($_POST['reference_no']) == 0) 
	{
		display_error(_("Reference # cannot be empty."));
		set_focus('reference_no');
		$error = 1;
	} 
	
	if($error != 1)
	{
		$trans_no = $_POST['trans_no'];
		$type = $_POST['type'];
		$reference_no = $_POST['reference_no'];
		$void = 0;
				
		$sql = "UPDATE ".TB_PREF."bank_trans
				SET reference_no = ".db_escape($reference_no).",
					reference_no_added = ".db_escape(date2sql(Today()))."
				WHERE trans_no = $trans_no
				AND type = $type";

		db_query($sql) or die('could not save reference #');
		
		$void = 1;
		
		$url = $path_to_root . "/gl/inquiry/bank_inquiry.php";
		
		if($void != 0)
		{			
			display_notification_centered(_("Reference # has been added."));
			unset($_POST['trans_no']);
			unset($_POST['type']);
			unset($_POST['reference_no']);
			meta_forward($url);
		}
	}
}

//----------------------------------------------------------------------------------------

if($_GET)
{
	global $systypes_array;
	
	$trans_no = $_GET['trans_no'];
	$type = $_GET['type'];
	
	start_form();

	start_table($table_style2);
	
	hidden('type', $type);
	hidden('trans_no', $trans_no);

	label_row(_("Transaction Type:"), $systypes_array[$type]);
	
	$ref_no =  get_reference_no($trans_no, $type);
	label_row(_("Transaction #:"), $ref_no);

	textarea_row(_("Reference #:"), 'reference_no', null, 30, 4);

	end_table(1);

	submit_center('Process', _("Save"), true, '', 'default');

	end_form();
}

end_page();

?>
