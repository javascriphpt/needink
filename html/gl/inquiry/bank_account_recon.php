<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_RECONCILE';
$path_to_root="../..";
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/includes/banking.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Bank Account Reconciliation"), false, false, "", $js);

check_db_has_bank_accounts(_("There are no bank accounts defined in the system."));

//-----------------------------------------------------------------------------------
// Ajax updates
//
if (get_post('Show'))
{
	$Ajax->activate('trans_tbl');
}
//------------------------------------------------------------------------------------------------

function get_chk_details($trans_no, $type)
{
	$sql = "SELECT ".TB_PREF."cheque_details.chk_number
			FROM
			".TB_PREF."cheque_details ,
			".TB_PREF."bank_trans
			WHERE
			".TB_PREF."bank_trans.trans_no = $trans_no AND 
			".TB_PREF."bank_trans.type = $type AND
			".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
			".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type ";
	
	//display_error($sql);
	$result = db_query($sql,"Failed to retrieve cheque details");
	$row = db_fetch($result);
	return $row;

}

function get_cv_chk_details($trans_no)
{
	$sql = "SELECT cheque_no
			FROM ".TB_PREF."check_voucher
			WHERE id = $trans_no";
	
	//display_error($sql);
	$result = db_query($sql,"Failed to retrieve cheque details --> cv");
	$row = db_fetch($result);
	return $row;

}

function get_chk_details_row_count($trans_no, $type)
{
	$sql = "SELECT ".TB_PREF."cheque_details.chk_number
			FROM
			".TB_PREF."cheque_details ,
			".TB_PREF."bank_trans
			WHERE
			".TB_PREF."bank_trans.trans_no = $trans_no AND 
			".TB_PREF."bank_trans.type = $type AND
			".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
			".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type ";
	
	//display_error($sql);
	$result = db_query($sql,"Failed to retrieve cheque details");
	return db_num_rows($result);

}

//------------------------------------------------------------------------------------------------

if (isset($_POST['btn_release'])) 
{
	$insert = 0;
	foreach($_POST as $postkey=>$postval )
    {
		if (strpos($postkey, 'release') === 0)
		{
			$id = substr($postkey, strlen('release'));
			
			$id_ = explode(',', $id);

			// display_error($id_[0]);
			
			$sql01 = "UPDATE ".TB_PREF."gl_trans 
					SET released=1, released_date=".db_escape(date2sql(Today()))."
					WHERE counter = ".$id_[0];

			db_query($sql01, "could not release transaction(s)");
			$insert = 1;	
		}
	}
	
	if($insert == 1)
	{
		display_notification('Updated.');
		$Ajax->activate('trans_tbl');	
	}
	
	commit_transaction();
	
}

//------------------------------------------------------------------------------------------------

start_form();
start_table("class='tablestyle_noborder'");
start_row();
bank_accounts_list_cells(_("Bank Account:"), 'bank_account', null);

date_cells(_("From:"), 'TransAfterDate', '', null, -30);
date_cells(_("To:"), 'TransToDate');

submit_cells('Show',_("Show"),'','', 'default');
end_row();
end_table();
end_form();

//------------------------------------------------------------------------------------------------

start_form();

$date_after = date2sql($_POST['TransAfterDate']);
$date_to = date2sql($_POST['TransToDate']);
if (!isset($_POST['bank_account']))
{
	$_POST['bank_account'] = "";
}
$act = get_bank_account($_POST["bank_account"]);
$sql = "SELECT ".TB_PREF."gl_trans.*, ".TB_PREF."chart_master.account_name, ".TB_PREF."bank_trans.ref 
		FROM ".TB_PREF."gl_trans, ".TB_PREF."chart_master, ".TB_PREF."bank_trans
		WHERE ".TB_PREF."chart_master.account_code=".TB_PREF."gl_trans.account
		AND ".TB_PREF."bank_trans.trans_no=".TB_PREF."gl_trans.type_no
		AND ".TB_PREF."bank_trans.type=".TB_PREF."gl_trans.type
		AND ".TB_PREF."gl_trans.amount != 0
		AND released = 0
		AND ".TB_PREF."gl_trans.account = ".db_escape($act['account_code'])."
		AND tran_date >= '$date_after'
		AND tran_date <= '$date_to'";

$result = db_query($sql,"The transactions for '" . $_POST['bank_account'] . "' could not be retrieved");

div_start('trans_tbl');
display_heading($act['bank_account_name']." - ".$act['bank_curr_code']);

start_table($table_style);

$th = array(_("Type"), _("#"), _("Check #"), _("Date"), _("Person/Item"),
	_("Debit"), _("Credit"), "");
table_header($th);

$j = 1;
$k = 0; //row colour counter
while ($myrow = db_fetch($result))
{

	alt_table_row_color($k);

	$trandate = sql2date($myrow["tran_date"]);
	label_cell($systypes_array[$myrow["type"]]);
	
	if($myrow["type"] == ST_CUSTDELIVERY || $myrow["type"] == ST_SALESINVOICE || $myrow["type"] == ST_CUSTCREDIT)
	{
		$ref_no = get_sales_ref_no($myrow["type_no"], $myrow["type"]);
		label_cell(get_trans_view_str($myrow["type"],$myrow["type_no"],$ref_no["location"]."-".$ref_no["form_type_no"]));
	}
	else if($myrow["type"] == ST_CUSTPAYMENT)
	{
		$ref_no = get_sales_ref_no_OR_PR($myrow["type_no"], $myrow["type"]);
		label_cell(get_trans_view_str($myrow["type"], $myrow["type_no"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"])));
	}
	else
		label_cell(get_trans_view_str($myrow["type"],$myrow["type_no"],$myrow['ref']));
		
	if($myrow["type"] == ST_CUSTPAYMENT || $myrow["type"] == ST_SUPPAYMENT)
	{	
		if($act["account_type"] == 1 && get_chk_details_row_count($myrow["type_no"], $myrow["type"]) == 1)	
		{
			$row = get_chk_details($myrow["type_no"], $myrow["type"]);
			label_cell($row[0]);
		}
		else
			label_cell('');
	}
	else if($myrow["type"] == ST_CVVVOUCHER)
	{
		$row2 = get_cv_chk_details($myrow["trans_no"]);
		label_cell($row2[0]);
	}
	else if($myrow["type"] == ST_BANKPAYMENT || $myrow["type"] == ST_BANKDEPOSIT)
	{
		if($act["account_type"] == 1 && get_chk_details_row_count($myrow["type_no"], $myrow["type"]) == 1)	
		{
			$row = get_chk_details($myrow["type_no"], $myrow["type"]);
			label_cell($row[0]);
		}
		else
			label_cell('');
	}
	else
		label_cell('');
	
	label_cell($trandate);
	label_cell(payment_person_name($myrow["person_type_id"],$myrow["person_id"]));
	display_debit_or_credit_cells($myrow["amount"]);
	
	check_cells("", "release".$myrow["counter"], null, true);
	
	end_row();
}
//end of while loop

hidden("from", $_POST['TransAfterDate']);
hidden("to", $_POST['TransToDate']);
hidden("account_code", $act['account_code']);

end_table(2);

submit_center_first('btn_release', _("Release"), '', 'default');
end_form();

if (db_num_rows($result) == 0)
	display_note(_("No transactions have been created for the specified criteria."), 0, 1);

div_end();
//------------------------------------------------------------------------------------------------

end_page();

?>
