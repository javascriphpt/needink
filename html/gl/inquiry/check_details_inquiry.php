<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPPCHECKDETAILSINQ';
$path_to_root="../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");
include_once($path_to_root . "/includes/banking.inc");
include($path_to_root . "/reporting/includes/reporting.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Check Details Inquiry"), false, false, "", $js);

//-----------------------------------------------------------------------------------

start_form();

start_table("class='tablestyle_noborder'");
start_row();

ref_cells(_("Check #:"), 'check_num', '',null, '', true);

date_cells(_("From:"), 'TransAfterDate', '', null, -30);
date_cells(_("To:"), 'TransToDate', '', null, 1);

submit_cells('RefreshInquiry', _("Search"),'',_('Refresh Inquiry'), 'default');
end_row();
end_table();

//------------------------------------------------------------------------------------------------

function trans_view($trans)
{
	return get_trans_view_str($trans["type"], $trans["trans_no"], $trans["ref"]);
}

function pay_to($trans)
{
	if($trans['type'] == 1)
		return payment_person_name($trans['person_type_id'], $trans['person_id']);
	else
		return get_supplier_name($trans['person_id']);
}

//------------------------------------------------------------------------------------------------
$date_after = date2sql($_POST['TransAfterDate']);
$date_to = date2sql($_POST['TransToDate']);

$sql = "SELECT
	".TB_PREF."bank_trans.trans_no,	
	".TB_PREF."bank_trans.person_id,	
	".TB_PREF."cheque_details.chk_number,
	".TB_PREF."cheque_details.chk_date,
	ABS(".TB_PREF."bank_trans.amount),
	".TB_PREF."bank_trans.type,
	".TB_PREF."bank_trans.ref,
	".TB_PREF."bank_trans.person_type_id
	FROM
	".TB_PREF."cheque_details ,
	".TB_PREF."bank_trans
	WHERE
	".TB_PREF."bank_trans.type IN (1, 22) AND
	".TB_PREF."cheque_details.type IN (1, 22) AND
	".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
	".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type AND
	".TB_PREF."cheque_details.chk_date >= '$date_after' AND	
	".TB_PREF."cheque_details.chk_date <= '$date_to' ";	
	
if (isset($_POST['check_num']) && $_POST['check_num'] != "")
	$sql .= " AND ".TB_PREF."cheque_details.chk_number LIKE ".db_escape("%".$_POST['check_num']."%");

//------------------------------------------------------------------------------------------------

$cols = array(
	_("#") => array('fun'=>'trans_view', 'ord'=>''),
	_("Pay To") => array('fun'=>'pay_to'),
	_("Check #") => array('ord'=>''),
	_("Check Date") => array('name'=>'chk_date', 'type'=>'date', 'ord'=>'desc'),
	_("Amount") => array('type'=>'amount', 'ord'=>'')
	);
	
$table =& new_db_pager('trans_tbl', $sql, $cols);

$table->width = "75%";

display_db_pager($table);

br();

$result = db_query($sql, "could not retrieve data");

$Ajax->activate('link');

div_start('link');
if(db_num_rows($result) != 0)
{
	echo "<center>";
	echo  "<a target=blank href='$path_to_root/reporting/check_details_printing.php?from=".date2sql($_POST['TransAfterDate'])."&to=".date2sql($_POST['TransToDate'])."'>" . _("Export to Excel") . "</a> ";
	echo "</center>";
}
div_end();

end_form();
end_page();

?>
