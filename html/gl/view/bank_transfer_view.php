<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_BANKTRANSVIEW';
$path_to_root="../..";

include($path_to_root . "/includes/session.inc");

page(_($help_context = "View Bank Transfer"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/gl/includes/gl_db.inc");

if (isset($_GET["trans_no"])){

	$trans_no = $_GET["trans_no"];
}

$result = get_bank_trans(ST_BANKTRANSFER, $trans_no);

if (db_num_rows($result) != 2)
	display_db_error("Bank transfer does not contain two records");

$trans1 = db_fetch($result);
$trans2 = db_fetch($result);

if ($trans1["amount"] < 0) 
{
    $from_trans = $trans1; // from trans is the negative one
    $to_trans = $trans2;
} 
else 
{
	$from_trans = $trans2;
	$to_trans = $trans1;
}

$company_currency = get_company_currency();

$show_currencies = false;
$show_both_amounts = false;

if (($from_trans['bank_curr_code'] != $company_currency) || ($to_trans['bank_curr_code'] != $company_currency))
	$show_currencies = true;

if ($from_trans['bank_curr_code'] != $to_trans['bank_curr_code']) 
{
	$show_currencies = true;
	$show_both_amounts = true;
}

display_heading($systypes_array[ST_BANKTRANSFER] . " #$trans_no");

echo "<br>";
start_table("$table_style width=80%");

start_row();
label_cells(_("From Bank Account"), $from_trans['bank_account_name'], "class='tableheader2'");
if ($show_currencies)
	label_cells(_("Currency"), $from_trans['bank_curr_code'], "class='tableheader2'");
label_cells(_("Amount"), number_format2(-$from_trans['amount'], user_price_dec()), "class='tableheader2'", "align=right");
if ($show_currencies)
{
	end_row();
	start_row();
}	
label_cells(_("To Bank Account"), $to_trans['bank_account_name'], "class='tableheader2'");
if ($show_currencies)
	label_cells(_("Currency"), $to_trans['bank_curr_code'], "class='tableheader2'");
if ($show_both_amounts)
	label_cells(_("Amount"), number_format2($to_trans['amount'], user_price_dec()), "class='tableheader2'", "align=right");
end_row();
start_row();
label_cells(_("Date"), sql2date($from_trans['trans_date']), "class='tableheader2'");
label_cells(_("Transfer Type"), $bank_transfer_types[$from_trans['account_type']],
	 "class='tableheader2'");
label_cells(_("Reference"), $from_trans['ref'], "class='tableheader2'");
end_row();

$res = get_audit_trail_all(ST_BANKTRANSFER, $trans_no);
$audit_trail = "";	$x=1;
while($audit = db_fetch($res))
{
	$name = get_user($audit['user']);
	if($x == 1)
		$audit_trail .= $name['user_id']." - ".$audit['description'];
	else
		$audit_trail .= "<br>".$name['user_id']." - ".$audit['description'];
		
	$x++;
}

label_row(_("Audit Trail"), $audit_trail, "class='tableheader2'", "colspan=5");

comments_display_row(ST_BANKTRANSFER, $trans_no);

end_table(1);

//-------------------------------------------------------------------------------------------

$sql001 = "SELECT DISTINCT alloc.trans_no_to, alloc.*, trans.ref, Trans.trans_date
		FROM ".TB_PREF."bank_trans as trans, ".TB_PREF."deposit_allocations as alloc
		WHERE trans.trans_no = alloc.trans_no_from
		AND trans.type = alloc.trans_type_from
		AND alloc.trans_type_from = ".db_escape(ST_BANKTRANSFER)."
		AND alloc.trans_no_from = ".db_escape($trans_no)."
		ORDER BY alloc.id";
		
$result001 = db_query($sql001, "could not retrieve transactions ");

if(db_num_rows($result001) >= 1)
{
	global $table_style, $systypes_array;
	
	display_heading2(_("Allocations"));

    start_table("$table_style width=80%");

    $th = array( _("Type"), _("Number"), _("Date"), _("Amount"));
	table_header($th);
    $k = $total_allocated = 0;

    while ($alloc_row = db_fetch($result001))
    {
		$ref_no = get_sales_ref_no_OR_PR($alloc_row['trans_no_to'], $alloc_row['trans_type_to']);
		
    	alt_table_row_color($k);

    	label_cell($systypes_array[$alloc_row['trans_type_to']]);
    	if($alloc_row['trans_type_to'] == 12)
			label_cell(get_trans_view_str($alloc_row['trans_type_to'],$alloc_row['trans_no_to'],$ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"])));
    	else
			label_cell(get_trans_view_str($alloc_row['trans_type_to'],$alloc_row['trans_no_to'],$alloc_row['reference']));
    	label_cell(sql2date($alloc_row['trans_date']));
    	amount_cell($alloc_row['amt']);
    	end_row();
		
		$total_allocated += $alloc_row['amt'];
    }
	start_row();
   	label_cell(_("Total Amount:"), "align=right colspan=3");
	amount_cell($total_allocated);
	end_row();
    end_table(1);
}

//-------------------------------------------------------------------------------------------

is_voided_display(ST_BANKTRANSFER, $trans_no, _("This transfer has been voided."));

start_table("$table_style width=80%");

start_row();

label_cell("Prepared By", "class='tableheader'");
label_cell("Checked By", "class='tableheader'");
label_cell("Approved By", "class='tableheader'");
label_cell("Received By", "class='tableheader'");

end_row();

start_row();

echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";

end_row();

end_table(2);

end_page(true);
?>