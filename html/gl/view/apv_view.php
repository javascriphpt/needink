<?php

$page_security = 'SA_APVVOUCHER';
$path_to_root="../..";
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
include_once($path_to_root . "/dimensions/includes/dimensions_db.inc");

page(_("Accounts Payable Voucher Details"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");


function get_supp_reference_no($trans_no)
{
	$sql = "SELECT supp_reference FROM ".TB_PREF."supp_trans WHERE type=20 AND trans_no=".db_escape($trans_no);

	$result = db_query($sql, "could not get supp reference no");

	$row = db_fetch_row($result);

	return $row[0];
}

if (!isset($_GET['trans_no'])) 
{ /*Script was not passed the correct parameters */

	echo "<p>" . _("The script must be called with a valid APV number.") . "</p>";
	exit;
}
$apvs=explode(',',$_GET['trans_no']);
$count=count($apvs);

foreach($apvs as $key=>$value){
//$trans_no = $_GET['trans_no'];
if($key>0)
	echo "<br><br><br>";
$trans_no = $value;
echo "<center>";	
display_heading(_("Account Payable Voucher") . " #".get_reference_no($trans_no, ST_APVVOUCHER));

	$myrow = get_apv_header($trans_no);
	global $table_style;

    start_table("$table_style width=95%");
    start_row();	
	label_cells(_("APV No. :"), $myrow['ref'], "class='tableheader2'", 'colspan=3');
	label_cells(_("Date :"), sql2date($myrow['tran_date']), "class='tableheader2'");
	end_row();
	start_row();	
	label_cells(_("Payee :"), payment_person_name($myrow['cust_supp'],$myrow['cust_supp_id'], true), "class='tableheader2'", 'colspan=3');
	label_cells(_("Terms :"), get_term($myrow['terms_indicator']), "class='tableheader2'");
	end_row();
	start_row();	
	//label_cells(_("P.O. No. :"), $myrow['po_no'], "class='tableheader2'");
	label_cells(_("Invoice No. :"), get_supp_reference_no($myrow['invoice_no']), "class='tableheader2' colspan=3");
	label_cells(_("Due Date :"), sql2date($myrow['due_date']), "class='tableheader2'");
	end_row();
    end_table(1);
	
	$th = array('Explanation');
	start_table("$table_style width=95%");
	table_header($th);
	label_cells('', $myrow['explanation']);
    end_table(1);
	
	
	$dim = get_company_pref('use_dimension');
	//$dim2 = get_company_pref('use_sl');
	
	start_table("$table_style width=95%");
	start_row();
	label_cell(_("Account Title"), "class='tableheader'");
	label_cell(_("Account Code"), "align=center class='tableheader'");
	
	// if($dim2==1)
		// label_cell(_("Sub-Ledger"), "class='tableheader'");
	
	for($i=1;$i<=2;$i++){
		label_cell(_("Dimension ".$i), "class='tableheader'");
	}
	// if($dim==2){
		// label_cell(_("Dimension 1"), "class='tableheader'");
		// label_cell(_("Dimension 2"), "class='tableheader'");	
	// }else if($dim==1){
		// label_cell(_("Dimension 1"), "class='tableheader'");
	// }
	//label_cell(_("Document").'<br>'. _("Ref. No."), "class='tableheader' rowspan=2");
	label_cell(_("Debit"), "class='tableheader'");
	label_cell(_("Credit"), "class='tableheader'");
	
	//label_cell(_("GL"), "class='tableheader'");
	//label_cell(_("SL"), "class='tableheader' width=8%");
	//label_cell(_("Cust/Supp"), "class='tableheader' width=16%");
	end_row();
	$result = get_apv_details($trans_no);
	
	$k = 0; //row colour counter
	while($row = db_fetch($result))
	{
		
		start_row();
			alt_table_row_color($k);	
			label_cell(get_gl_account_name($row['account']));
			label_cell($row['account']);
			//label_cell('');
			//label_cell(get_payee($myrow['cust_supp'],$myrow['cust_supp_id']));
			//label_cell($row['doc_ref']);
			// if($dim2==1)
				// label_cell($row['sl']); //SL

			for($i=1;$i<=2;$i++){
				($i==1)? $ii="":$ii=$i;
				label_cell(get_dimension_string($row['dimension'.$ii.'_id']));
			}
			// if($dim==2){
				// label_cell(get_dimension_string($row['dimension_id']));
				// label_cell(get_dimension_string($row['dimension2_id']));
			// }else if($dim==1){
				// label_cell(get_dimension_string($row['dimension_id']));
			// }
			
			if ($row['amount'] > 0)
			{
				amount_cell($row['amount']);
				label_cell('');
			}
			else
			{
				label_cell('');
				amount_cell(-1*$row['amount']);
			}
				
		end_row();
	
	}
    end_table(1);
	
	//-------------------------------------------------------------
	// display_error($count);
	if($count<=1){
		// print_hidden_script(45);
		display_note(print_document_link($trans_no, _("Reprint This APV"),true,45));
	}
	//-------------------------------------------------------------
}
end_page();

?>
