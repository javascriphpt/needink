<?php

$page_security = 'SA_APVVOUCHER';
$path_to_root="../..";
include_once($path_to_root . "/includes/session.inc");

page(_("Check Voucher Details"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

if (!isset($_GET['trans_no'])) 
{ /*Script was not passed the correct parameters */

	echo "<p>" . _("The script must be called with a valid CV number.") . "</p>";
	exit;
}

echo "<center>";	
$trans_no = $_GET['trans_no'];
$supp_reference = $_GET['supp_reference'];
display_heading(_("Check Voucher") . " #$trans_no");

	global $table_style;

    start_table("$table_style width=95%");
    start_row();	
	label_cells(_("CV No. :"), $trans_no, "class='tableheader2'");
	label_cells(_("Date :"), sql2date($cv_row['cv_date']), "class='tableheader2'");
	end_row();
	
	//$apv_row= get_apv_header_by_cv($trans_no);	
	$apv_row= get_apv_header_by_cv_2($trans_no, $supp_reference);
	$bank = get_bank_account($apv_row['bank_act']);
	start_row();	
	label_cells(_("Payee :"), payment_person_name($apv_row['cust_supp'],$apv_row['cust_supp_id'], true), "class='tableheader2'");
	label_cells(_("Bank :"), $bank['bank_account_name'], "class='tableheader2'");
	end_row();
	start_row();	

	label_cells(_("Invoice No. :"), $apv_row['invoice_no'], "class='tableheader2' ");
	label_cells(_("Check Number :"), $cv_row['cheque_no'], "class='tableheader2'");
	//label_cell('');
	end_row();
    end_table(1);
	
	$th = array('Explanation');
	start_table("$table_style width=95%");
	table_header($th);
	label_cells('', $apv_row['explanation']);
    end_table(1);
	
	start_table("$table_style width=95%");
	start_row();
	label_cell(_("Account Title"), "class='tableheader'");
	label_cell(_("Account Code"), "align=center class='tableheader'");
	//label_cell(_("Document").'<br>'. _("Ref. No."), "class='tableheader' rowspan=2");
	label_cell(_("Debit"), "class='tableheader'");
	label_cell(_("Credit"), "class='tableheader'");
	
	//label_cell(_("GL"), "class='tableheader'");
	//label_cell(_("SL"), "class='tableheader' width=8%");
	//label_cell(_("Cust/Supp"), "class='tableheader' width=16%");
	end_row();
	$result = get_apv_details($apv_row['id']/*$apv_row['ref']*/);
	
	$k = 0; //row colour counter
	while($row = db_fetch($result))
	{
		
		start_row();
			alt_table_row_color($k);	
			label_cell(get_gl_account_name($row['account']));
			label_cell($row['account']);
			//label_cell('');
			//label_cell(get_payee($myrow['cust_supp'],$myrow['cust_supp_id']));
			//label_cell($row['doc_ref']);
			
			if ($row['amount'] > 0)
			{
				amount_cell($row['amount']);
				label_cell('');
			}
			else
			{
				label_cell('');
				amount_cell(-1*$row['amount']);
			}
				
		end_row();
	
	}
    end_table(1);
end_page(true);

?>
