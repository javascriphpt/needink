<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_GLTRANSVIEW';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");

page(_($help_context = "General Ledger Transaction Details"), true);

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");

include_once($path_to_root . "/gl/includes/gl_db.inc");

include_once($path_to_root . "/reporting/includes/reporting.inc");

if (!isset($_GET['type_id']) || !isset($_GET['trans_no'])) 
{ /*Script was not passed the correct parameters */

	echo "<p>" . _("The script must be called with a valid transaction type and transaction number to review the general ledger postings for.") . "</p>";
	exit;
}

function display_gl_heading($myrow)
{
	global $table_style, $systypes_array;
	$trans_name = $systypes_array[$_GET['type_id']];
    start_table("$table_style width=95%");
    $th = array(_("General Ledger Transaction Details"), /*_("Reference"),*/
    	_("Date"), _("Person/Item"));
    table_header($th);	
    start_row();	
	if($_GET['type_id'] == ST_CUSTDELIVERY || $_GET['type_id'] == ST_SALESINVOICE || $_GET['type_id'] == ST_CUSTCREDIT)
	{
		$ref_no = get_sales_ref_no($_GET['trans_no'], $_GET['type_id']);
		label_cell("$trans_name # " . $ref_no["location"]."-".$ref_no["form_type_no"]);
		//label_cell($ref_no["location"]."-".$ref_no["form_type_no"]);
	}
	else if($_GET['type_id'] == ST_CUSTPAYMENT)
	{
		$ref_no = get_sales_ref_no_OR_PR($_GET['trans_no'], $_GET['type_id']);
		label_cell("$trans_name # " . $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"]));
	}
	else if($_GET['type_id'] == 26)
	{
		label_cell("$trans_name # " . get_wo_form_type_no_wo($_GET['trans_no']));
	}
	else
	{
		//label_cell("$trans_name #" . $_GET['trans_no']);
		label_cell("$trans_name #" . $myrow["reference"]);
	}
	label_cell(sql2date($myrow["tran_date"]));
	label_cell(payment_person_name($myrow["person_type_id"],$myrow["person_id"]));
	
	end_row();
	
	$res = get_audit_trail_all($_GET['type_id'], $_GET['trans_no']);
	$audit_trail = "";	$x=1;
	while($audit = db_fetch($res))
	{
		$name = get_user($audit['user']);
		if($x == 1)
			$audit_trail .= $name['user_id']." - ".$audit['description'];
		else
			$audit_trail .= "<br>".$name['user_id']." - ".$audit['description'];
			
		$x++;
	}

	echo "<tr><td colspan=15>Audit Trail : ".$audit_trail."</td></tr>";

	comments_display_row($_GET['type_id'], $_GET['trans_no']);

    end_table(1);
}
$sql = "SELECT gl.*, cm.account_name, IF(ISNULL(refs.reference), '', refs.reference) AS reference FROM "
	.TB_PREF."gl_trans as gl
	LEFT JOIN ".TB_PREF."chart_master as cm ON gl.account = cm.account_code
	LEFT JOIN ".TB_PREF."refs as refs ON (gl.type=refs.type AND gl.type_no=refs.id)"
	." WHERE gl.type= ".db_escape($_GET['type_id']) 
	." AND gl.type_no = ".db_escape($_GET['trans_no'])
	." ORDER BY amount DESC, counter";
$result = db_query($sql,"could not get transactions");
//alert("sql = ".$sql);

if (db_num_rows($result) == 0)
{
    echo "<p><center>" . _("No general ledger transactions have been created for") . " " .$systypes_array[$_GET['type_id']]." " . _("number") . " " . $_GET['trans_no'] . "</center></p><br><br>";
	end_page(true);
	exit;
}

/*show a table of the transactions returned by the sql */
$dim = get_company_pref('use_dimension');

if ($dim == 2)
	$th = array(_("Account Code"), _("Account Name"), _("Dimension")." 1", _("Dimension")." 2",
		_("Debit"), _("Credit"), _("Memo"));
else if ($dim == 1)
	$th = array(_("Account Code"), _("Account Name"), _("Dimension"),
		_("Debit"), _("Credit"), _("Memo"));
else		
	$th = array(_("Account Code"), _("Account Name"),
		_("Debit"), _("Credit"), _("Memo"));
$k = 0; //row colour counter
$heading_shown = false;

while ($myrow = db_fetch($result)) 
{
	if ($myrow['amount'] == 0) continue;
	if (!$heading_shown)
	{
		display_gl_heading($myrow);
		start_table("$table_style width=95%");
		table_header($th);
		$heading_shown = true;
	}	

	alt_table_row_color($k);
	
    label_cell($myrow['account']);
	label_cell($myrow['account_name']);
	if ($dim >= 1)
		label_cell(get_dimension_string($myrow['dimension_id'], true));
	if ($dim > 1)
		label_cell(get_dimension_string($myrow['dimension2_id'], true));

	display_debit_or_credit_cells($myrow['amount']);
	label_cell($myrow['memo_']);
	end_row();

}
//end of while loop
if ($heading_shown)
	end_table(1);
	
is_voided_display($_GET['type_id'], $_GET['trans_no'], _("This transaction has been voided."));

if($_GET['type_id'] == ST_SUPPINVOICE)
	display_note(print_document_link($_GET['trans_no'], _("Reprint This APV"), true, ST_SUPPINVOICE));
	
br(1);

start_table("$table_style width=80%");

start_row();

label_cell("Prepared By", "class='tableheader'");
label_cell("Checked By", "class='tableheader'");
label_cell("Approved By", "class='tableheader'");
label_cell("Received By", "class='tableheader'");

end_row();

start_row();

echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";
echo "<td>&nbsp</td>";

end_row();

end_table(2);


end_page(true);

?>
