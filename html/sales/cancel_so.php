<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root = "..";

include($path_to_root . "/includes/db_pager.inc");
include($path_to_root . "/includes/session.inc");
include($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

$page_security = 'SA_CANCELSO';

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);
if ($use_date_picker)
	$js .= get_js_date_picker();
	
page(_($help_context = "Cancel Sales Order"), false, false, "", $js);

//----------------------------------------------------------------------------------------
//simple_page_mode(true);

if (isset($_GET['trans_no']))
	$trans_no = $_GET['trans_no'];

if (isset($_GET['type']))
	$type = $_GET['type'];

//--------------------------------------------------------------------------------------------------

$sql1 = "SELECT line.*
			FROM ".TB_PREF."sales_orders as orders, ".TB_PREF."sales_order_details as line
			WHERE orders.order_no = line.order_no
			AND orders.order_no = ".db_escape($trans_no)."
			AND orders.trans_type = ".db_escape($type);
$result1 = db_query($sql1);

while($row = db_fetch($result1))
{
	$sql2 = "UPDATE ".TB_PREF."sales_order_details 
				SET quantity = ".db_escape($row['qty_sent']).",
						quantity_cancelled = ".db_escape($row['quantity'] - $row['qty_sent'])."
				WHERE id = ".db_escape($row['id']);
	db_query($sql2);
}

display_notification_centered(_("Selected Sales Order has been cancelled."));
meta_forward($path_to_root . "/sales/inquiry/sales_orders_view.php?OutstandingOnly=1");

end_page();

?>