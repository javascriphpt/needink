<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SALESTRANSVIEW';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Customer Transactions"), false, false, "", $js);


if (isset($_GET['customer_id']))
{
	$_POST['customer_id'] = $_GET['customer_id'];
}

//------------------------------------------------------------------------------------------------

start_form();

if (!isset($_POST['customer_id']))
	$_POST['customer_id'] = get_global_customer();

start_table("class='tablestyle_noborder'");
start_row();

ref_cells(_("#:"), 'OrderNumber', '',null, '', true);

customer_list_cells(_("Customer: "), 'customer_id', null, true);

if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	locations_list_cells_user(_("Deliver from Location:"), 'StockLocation', null, false, true, $_SESSION["wa_current_user"]->default_location);
else
	locations_list_cells(_("Location:"), 'StockLocation', null, true);

date_cells(_("From:"), 'TransAfterDate', '', null, -30);
date_cells(_("To:"), 'TransToDate', '', null, 1);

if (!isset($_POST['filterType']))
	$_POST['filterType'] = 0;

cust_allocations_list_cells(null, 'filterType', $_POST['filterType'], true);

submit_cells('RefreshInquiry', _("Search"),'',_('Refresh Inquiry'), 'default');
end_row();
end_table();

set_global_customer($_POST['customer_id']);

//------------------------------------------------------------------------------------------------

function display_customer_summary($customer_record)
{
	global $table_style;

	$past1 = get_company_pref('past_due_days');
	$past2 = 2 * $past1;
    if ($customer_record["dissallow_invoices"] != 0)
    {
    	echo "<center><font color=red size=4><b>" . _("CUSTOMER ACCOUNT IS ON HOLD") . "</font></b></center>";
    }

	$nowdue = "1-" . $past1 . " " . _('Days');
	$pastdue1 = $past1 + 1 . "-" . $past2 . " " . _('Days');
	$pastdue2 = _('Over') . " " . $past2 . " " . _('Days');

    start_table("width=80% $table_style");
    $th = array(_("Currency"), _("Terms"), _("Current"), $nowdue,
    	$pastdue1, $pastdue2, _("Total Balance"));
    table_header($th);

	start_row();
    label_cell($customer_record["curr_code"]);
    label_cell($customer_record["terms"]);
	amount_cell($customer_record["Balance"] - $customer_record["Due"]);
	amount_cell($customer_record["Due"] - $customer_record["Overdue1"]);
	amount_cell($customer_record["Overdue1"] - $customer_record["Overdue2"]);
	amount_cell($customer_record["Overdue2"]);
	amount_cell($customer_record["Balance"]);
	end_row();

	end_table();
}
//------------------------------------------------------------------------------------------------

div_start('totals_tbl');
if ($_POST['customer_id'] != "" && $_POST['customer_id'] != ALL_TEXT)
{
	$customer_record = get_customer_details($_POST['customer_id'], $_POST['TransToDate']);
    display_customer_summary($customer_record);
    echo "<br>";
}
div_end();

if(get_post('RefreshInquiry'))
{
	$Ajax->activate('totals_tbl');
}
//------------------------------------------------------------------------------------------------

function get_cust_po($order_no)
{
	$sql = "SELECT customer_ref FROM ".TB_PREF."sales_orders 
		WHERE trans_type = ".ST_SALESORDER."
		AND order_no = ".db_escape($order_no);

	$result = db_query($sql,"could not retreive customer po #");

	$myrow = db_fetch_row($result);	
	return $myrow[0];
}

//------------------------------------------------------------------------------------------------

function systype_name($dummy, $type)
{
	global $systypes_array;

	return $systypes_array[$type];
}

function order_view($row)
{
	// $ref_no = get_so_ref_no($row['order_'], ST_SALESORDER);
	// return $row['order_']>0 ? get_customer_trans_view_str(ST_SALESORDER, $row['order_'], $ref_no["from_stk_loc"]."-".$ref_no["so_form_type_no"]) : "";

	if($row['type'] == ST_SALESINVOICE)
	{
		$x=1;
		$dr_text = "";
		foreach(get_parent_trans(ST_SALESINVOICE,$row['trans_no']) as $id=>$val)
		{	
			$order_ = get_order_no(ST_CUSTDELIVERY,$val);
			$ref_no = get_so_ref_no($order_, ST_SALESORDER);
			
			if($x == 1)
				$dr_text .= get_customer_trans_view_str(ST_SALESORDER, $order_, $ref_no["from_stk_loc"]."-".$ref_no["so_form_type_no"]);	
			else
				$dr_text .= ", ".get_customer_trans_view_str(ST_SALESORDER, $order_, $ref_no["from_stk_loc"]."-".$ref_no["so_form_type_no"]);	
				
			$x++;
		}
	
		return $dr_text;
	}
	else
	{
		$ref_no = get_so_ref_no($row['order_'], ST_SALESORDER);
		return $row['order_']>0 ? get_customer_trans_view_str(ST_SALESORDER, $row['order_'], $ref_no["from_stk_loc"]."-".$ref_no["so_form_type_no"]) : "";
	}
}

function cust_po_view($row)
{
	if($row['type'] == ST_SALESINVOICE)
	{
		return get_cust_po($row['order_']);
	}
	else
		return "";
}

function dr_view($row)
{
	if($row['type'] == ST_SALESINVOICE)
	{
		$x=1;
		$dr_text = "";
		foreach(get_parent_trans(ST_SALESINVOICE,$row['trans_no']) as $id=>$val)
		{	
			$ref_no = get_sales_ref_no($val, ST_CUSTDELIVERY);
			if($x == 1)
				$dr_text .= get_customer_trans_view_str(ST_CUSTDELIVERY,$val, $ref_no["location"]."-".$ref_no["form_type_no"]);
			else
				$dr_text .= ", ".get_customer_trans_view_str(ST_CUSTDELIVERY,$val, $ref_no["location"]."-".$ref_no["form_type_no"]);
				
			$x++;
		}
	
		return $dr_text;
	}
	else
		return "";
}

function view_sales_person($row)
{
	$dr_text = "";
	
	if($row['type'] == ST_SALESINVOICE)
	{
		$x=1;
		// $dr_text = "";
		foreach(get_parent_trans(ST_SALESINVOICE,$row['trans_no']) as $id=>$val)
		{	
			$order_ = get_order_no(ST_CUSTDELIVERY,$val);
			
			if($x == 1)
				$dr_text .= get_salesman_name_by_order_no($order_, ST_SALESORDER);	
			else
				$dr_text .= ", ".get_salesman_name_by_order_no($order_, ST_SALESORDER);	
				
			$x++;
		}
	
		return $dr_text;
	}
	else if($row['type'] == ST_CUSTPAYMENT)
	{
		$dr_text = "";
		$x=1;
		
		$sql = "SELECT trans_no_to
				FROM ".TB_PREF."cust_allocations 
				WHERE trans_no_from=".db_escape($row['trans_no'])."
				AND trans_type_from=".db_escape($row['type'])."
				AND trans_type_to=10";
		$result = db_query($sql);
		
		while($myrow = db_fetch($result))
		{
			set_time_limit(0);
			foreach(get_parent_trans(ST_SALESINVOICE,$myrow['trans_no_to']) as $id=>$val)
			{	
				set_time_limit(0);
				$order_ = get_order_no(ST_CUSTDELIVERY,$val);
				
				if($x == 1)
					$dr_text .= get_salesman_name_by_order_no($order_, ST_SALESORDER);	
				else
					$dr_text .= ", ".get_salesman_name_by_order_no($order_, ST_SALESORDER);	
					
				$x++;
			}
		}
		
		if(db_num_rows($result) == 0)
			return "";
		else
			return $dr_text;	
	}
	else if($row['type'] == ST_CUSTDELIVERY)
	{
		$order_ = get_order_no(ST_CUSTDELIVERY,$row['trans_no']);
		$dr_text .= get_salesman_name_by_order_no($order_, ST_SALESORDER);	
		
		return $dr_text;
	}
	else
		return "";
}

function trans_view($trans)
{
	$ref_no = get_sales_ref_no($trans["trans_no"], $trans["type"]);
	//if($trans["type"] != 12)
	if($trans["type"] == ST_SALESINVOICE || $trans["type"] == ST_CUSTCREDIT || $trans["type"] == ST_CUSTDELIVERY)
		return get_trans_view_str($trans["type"], $trans["trans_no"], $ref_no["location"]."-".$ref_no["form_type_no"]);
	elseif($trans["type"] == ST_CUSTPAYMENT)
		return get_trans_view_str($trans["type"], $trans["trans_no"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($trans["form_type"]));
	else
		return get_trans_view_str($trans["type"], $trans["trans_no"], $trans["reference"]);
}

function due_date($row)
{
	return	$row["type"] == ST_SALESINVOICE	? $row["due_date"] : '';
}

function gl_view($row)
{
	return get_gl_view_str($row["type"], $row["trans_no"]);
}

function fmt_debit($row)
{
	$value =
	    $row['type']==ST_CUSTCREDIT || $row['type']==ST_CUSTPAYMENT || $row['type']==ST_BANKDEPOSIT ?
		-$row["TotalAmount"] : $row["TotalAmount"];
	return $value>=0 ? price_format($value) : '';

}

function fmt_credit($row)
{
	$value =
	    !($row['type']==ST_CUSTCREDIT || $row['type']==ST_CUSTPAYMENT || $row['type']==ST_BANKDEPOSIT) ?
		-$row["TotalAmount"] : $row["TotalAmount"];
	return $value>0 ? price_format($value) : '';
}

function balance_view($row)
{	
	if($row['type'] == ST_SALESINVOICE)
		return price_format($row["TotalAmount"] - $row["Allocated"]);
	else
		return "";
}

function credit_link($row)
{
	return $row['type'] == ST_SALESINVOICE && $row["TotalAmount"] - $row["Allocated"] > 0 ?
		pager_link(_("Credit This"),
			"/sales/customer_credit_invoice.php?InvoiceNumber=".
			$row['trans_no'], ICON_CREDIT)
			: '';
}

function edit_link($row)
{
	$str = '';

	switch($row['type']) {
	case ST_SALESINVOICE:
		if (get_voided_entry(ST_SALESINVOICE, $row["trans_no"]) === false && $row['Allocated'] == 0)
			$str = "/sales/customer_invoice.php?ModifyInvoice=".$row['trans_no'];
		break;
	case ST_CUSTCREDIT:
  		if (get_voided_entry(ST_CUSTCREDIT, $row["trans_no"]) === false && $row['Allocated'] == 0) // 2008-11-19 Joe Hunt
		{	 
			if ($row['order_']==0) // free-hand credit note
			    $str = "/sales/credit_note_entry.php?ModifyCredit=".$row['trans_no'];
			else	// credit invoice
			    $str = "/sales/customer_credit_invoice.php?ModifyCredit=".$row['trans_no'];
		}	    
		break;
	 case ST_CUSTDELIVERY:
  		if (get_voided_entry(ST_CUSTDELIVERY, $row["trans_no"]) === false)
   			$str = "/sales/customer_delivery.php?ModifyDelivery=".$row['trans_no'];
		break;
	}
	if ($str != "" && !is_closed_trans($row['type'], $row["trans_no"]))
		return pager_link(_('Edit'), $str, ICON_EDIT);
	return '';	
}

function prt_link($row)
{
  	if ($row['type'] != ST_CUSTPAYMENT && $row['type'] != ST_BANKDEPOSIT) // customer payment or bank deposit printout not defined yet.
 		return print_document_link($row['trans_no']."-".$row['type'], _("Print"), true, $row['type'], ICON_PRINT);
 	else	
		return print_document_link($row['trans_no']."-".$row['type'], _("Print Receipt"), true, ST_CUSTPAYMENT, ICON_PRINT);
}

function void_link($row)
{
	if (get_voided_entry($row['type'], $row['trans_no']) === false)
		return pager_link(_("Void"), "/sales/void.php?trans_no=". $row['trans_no']."&type=". $row['type'], ICON_REMOVE);
}

function check_overdue($row)
{
	return $row['OverDue'] == 1
		&& (abs($row["TotalAmount"]) - $row["Allocated"] != 0);
}
//------------------------------------------------------------------------------------------------
    $date_after = date2sql($_POST['TransAfterDate']);
    $date_to = date2sql($_POST['TransToDate']);

  $sql = "SELECT 
  		trans.type, 
		trans.trans_no, 
		trans.order_, 
		trans.order_ as order_no, 
		trans.trans_no as transaction_no, 
		trans.tran_date, 
		trans.due_date, 
		debtor.name, 
		branch.br_name,
		trans.trans_no as for_salesperson,
		debtor.curr_code,
		(trans.ov_amount + trans.ov_gst + trans.ov_freight 
			+ trans.ov_freight_tax + trans.ov_discount + trans.ewt)	AS TotalAmount, "; 
   	if ($_POST['filterType'] != ALL_TEXT)
		$sql .= "@bal := @bal+(trans.ov_amount + trans.ov_gst + trans.ov_freight + trans.ov_freight_tax + trans.ov_discount + trans.ewt), ";

//	else
//		$sql .= "IF(trans.type=".ST_CUSTDELIVERY.",'', IF(trans.type=".ST_SALESINVOICE." OR trans.type=".ST_BANKPAYMENT.",@bal := @bal+
//			(trans.ov_amount + trans.ov_gst + trans.ov_freight + trans.ov_freight_tax + trans.ov_discount), @bal := @bal-
//			(trans.ov_amount + trans.ov_gst + trans.ov_freight + trans.ov_freight_tax + trans.ov_discount))) , ";
		$sql .= "trans.alloc AS Allocated,
		((trans.type = ".ST_SALESINVOICE.")
			AND trans.due_date < '" . date2sql(Today()) . "') AS OverDue, trans.reference, trans.form_type
		FROM "
			.TB_PREF."debtor_trans as trans, "
			.TB_PREF."debtors_master as debtor, "
			.TB_PREF."cust_branch as branch
		WHERE debtor.debtor_no = trans.debtor_no
			AND trans.tran_date >= '$date_after'
			AND trans.tran_date <= '$date_to'
			AND trans.branch_code = branch.branch_code";
			
	// if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
		// $sql .= " AND trans.location = ".db_escape($_SESSION["wa_current_user"]->default_location);
		
	if (isset($_POST['StockLocation']) && $_POST['StockLocation'] != ALL_TEXT)
		$sql .= " AND trans.location = ".db_escape($_POST['StockLocation']);

   	if ($_POST['customer_id'] != ALL_TEXT)
   		$sql .= " AND trans.debtor_no = ".db_escape($_POST['customer_id']);
		
	if (isset($_POST['OrderNumber']) && $_POST['OrderNumber'] != "")
		$sql .= " AND trans.form_type_no LIKE ".db_escape("%".$_POST['OrderNumber']."%");

   	if ($_POST['filterType'] != ALL_TEXT)
   	{
   		if ($_POST['filterType'] == '1')
   		{
   			$sql .= " AND (trans.type = ".ST_SALESINVOICE." OR trans.type = ".ST_BANKPAYMENT.") ";
   		}
   		elseif ($_POST['filterType'] == '2')
   		{
   			$sql .= " AND (trans.type = ".ST_SALESINVOICE.") ";
   		}
   		elseif ($_POST['filterType'] == '3')
   		{
			$sql .= " AND (trans.type = " . ST_CUSTPAYMENT 
					." OR trans.type = ".ST_BANKDEPOSIT.") ";
   		}
   		elseif ($_POST['filterType'] == '4')
   		{
			$sql .= " AND trans.type = ".ST_CUSTCREDIT." ";
   		}
   		elseif ($_POST['filterType'] == '5')
   		{
			$sql .= " AND trans.type = ".ST_CUSTDELIVERY." ";
   		}

    	if ($_POST['filterType'] == '2')
    	{
    		$today =  date2sql(Today());
    		// $sql .= " AND trans.due_date < '$today'
    		$sql .= " AND trans.due_date <= '$date_to'
				AND (trans.ov_amount + trans.ov_gst + trans.ov_freight_tax + 
				trans.ov_freight + trans.ov_discount + trans.ewt - trans.alloc > 0) ";
    	}
   	}

//------------------------------------------------------------------------------------------------
db_query("set @bal:=0");

$cols = array(
	_("Type") => array('fun'=>'systype_name', 'ord'=>''),
	_("#") => array('fun'=>'trans_view', 'ord'=>''),
	_("Order") => array('fun'=>'order_view'), 
	_("Customer PO #") => array('fun'=>'cust_po_view'), 
	_("DR #") => array('fun'=>'dr_view'), 
	_("Date") => array('name'=>'tran_date', 'type'=>'date', 'ord'=>'desc'),
	_("Due Date") => array('type'=>'date', 'fun'=>'due_date'),
	_("Customer") => array('ord'=>''), 
	_("Branch") => array('ord'=>''), 
	_("Salesperson") => array('fun'=>'view_sales_person', 'ord'=>''), 
	_("Currency") => array('align'=>'center'),
	_("Debit") => array('align'=>'right', 'fun'=>'fmt_debit'), 
	_("Credit") => array('align'=>'right','insert'=>true, 'fun'=>'fmt_credit'), 
	_("Balance") => array('align'=>'right','insert'=>true, 'fun'=>'balance_view'), 
	_("RB") => array('align'=>'right', 'type'=>'amount'),
		array('insert'=>true, 'fun'=>'gl_view'),
		array('insert'=>true, 'fun'=>'edit_link'),
		array('insert'=>true, 'fun'=>'prt_link'),
		array('insert'=>true, 'fun'=>'void_link')
	);
		//array('insert'=>true, 'fun'=>'credit_link'),


if ($_POST['customer_id'] != ALL_TEXT) {
	$cols[_("Customer")] = 'skip';
	$cols[_("Currency")] = 'skip';
}
if ($_POST['filterType'] == ALL_TEXT)
	$cols[_("RB")] = 'skip';

$table =& new_db_pager('trans_tbl', $sql, $cols);
$table->set_marker('check_overdue', _("Marked items are overdue."));

$table->width = "95%";

display_db_pager($table);

if (isset($_POST['customer_id']) && $_POST['customer_id'] == ALL_TEXT) {
	$cust=0;
	$Ajax->activate('link');
} else {
	$cust = $_POST['customer_id'];
	$Ajax->activate('link');
}
	
if (isset($_POST['filterType']) && $_POST['filterType'] == ALL_TEXT) {
	$type=0;
	$Ajax->activate('link');
} else {
	$type = $_POST['filterType'];
	$Ajax->activate('link');
}
	
div_start('link');
	
echo "<center>";
echo  "<a target=blank href='$path_to_root/reporting/customer_inquiry_pdf.php?customer=".$cust."&from=".date2sql($_POST['TransAfterDate'])."&to=".date2sql($_POST['TransToDate'])."&type=".$type."&output=0'>" . _("Print") . "</a> || ";
echo  "<a target=blank href='$path_to_root/reporting/customer_inquiry_excel.php?customer=".$cust."&from=".date2sql($_POST['TransAfterDate'])."&to=".date2sql($_POST['TransToDate'])."&type=".$type."'>" . _("Export to Excel") . "</a> ";
echo "</center>";

div_end();

end_form();
end_page();

?>
