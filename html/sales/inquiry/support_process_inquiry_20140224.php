<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPINQUIRY';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Support Process Inquiry"), false, false, "", $js);


if (isset($_GET['customer_id']))
{
	$_POST['customer_id'] = $_GET['customer_id'];
}

//------------------------------------------------------------------------------------------------

function trans_view($trans)
{	
	$path_to_root = "../";
	return "<a href=".$path_to_root . "view/view_sup.php?trans_no=".$trans["id"]." target=_blank>".$trans["sup_no"]."</a>";
}

//------------------------------------------------------------------------------------------------

start_form();

if (!isset($_POST['customer_id']))
	$_POST['customer_id'] = get_global_customer();

start_table("class='tablestyle_noborder'");

start_row();
text_cells_ex('SUP No.:', 'sup_no', 20, 255);
text_cells_ex('Company Name:', 'company_name', 35, 255);
text_cells_ex('System / Unit:', 'system_unit', 35, 255);
end_row();

start_row();
text_cells_ex('Serial No.:', 'serial_no', 35, 255);
text_cells_ex('Assigned TSG:', 'assigned_tsg', 35, 255);
text_cells_ex('Conformed:', 'conformed', 35, 255);
end_row();

start_row();
date_cells(_("From:"), 'OrdersAfterDate', '', null, -30);
date_cells(_("To:"), 'OrdersToDate', '', null, 1);
sup_type_list_cells( _("Type: "), 'type', null);

submit_cells('RefreshInquiry', _("Search"),'',_('Refresh Inquiry'), 'default');

end_row();

end_table();

echo "<br>";

set_global_customer($_POST['customer_id']);

//------------------------------------------------------------------------------------------------

if(get_post('RefreshInquiry'))
{
	$Ajax->activate('totals_tbl');
	$Ajax->activate('link');
}

//------------------------------------------------------------------------------------------------

$date_after = date2sql($_POST['OrdersAfterDate']);
$date_before = date2sql($_POST['OrdersToDate']);
$Ajax->activate('link');

$sql = "SELECT sup.sup_no,
			sup.company_name,
			sup.system_unit,
			sup.serial_no,
			sup.assigned_tsg,
			sup.conformed, 
			completed_date, sup.id
		FROM ".TB_PREF."sup as sup, ".TB_PREF."sup_type as type
		WHERE sup.type = type.id 
		AND sup.type = ".db_escape($_POST['type'])."
		AND sup.completed_date >= ".db_escape($date_after)."
		AND sup.completed_date <= ".db_escape($date_before)." ";

if ($_POST['sup_no'] != '')
   	$sql .= " AND sup.sup_no LIKE ".db_escape("%".$_POST['sup_no']."%");
	
if ($_POST['company_name'] != '')
   	$sql .= " AND sup.company_name LIKE ".db_escape("%".$_POST['company_name']."%");
	
if ($_POST['system_unit'] != '')
   	$sql .= " AND sup.system_unit LIKE ".db_escape("%".$_POST['system_unit']."%");
	
if ($_POST['serial_no'] != '')
   	$sql .= " AND sup.serial_no LIKE ".db_escape("%".$_POST['serial_no']."%");
	
if ($_POST['assigned_tsg'] != '')
   	$sql .= " AND sup.assigned_tsg LIKE ".db_escape("%".$_POST['assigned_tsg']."%");
	
if ($_POST['conformed'] != '')
   	$sql .= " AND sup.conformed LIKE ".db_escape("%".$_POST['conformed']."%");
	
$sql .= " ORDER BY sup.sup_no ASC, sup.company_name";

//------------------------------------------------------------------------------------------------

db_query("set @bal:=0");

$cols = array(
	_("SUP No.") => array('fun'=>'trans_view', 'ord'=>''),
	_("Company Name") => array('ord'=>''), 
	_("System / Unit") => array('ord'=>''),
	_("Serial No") => array('ord'=>''),
	_("Assigned TSG") => array('ord'=>''),
	_("Conformed") => array('ord'=>''),
	_("Completed Date") => array('type'=>'date', 'ord'=>'')
	);

$table =& new_db_pager('trans_tbl', $sql, $cols);

$table->width = "85%";

display_db_pager($table);

br();
div_start('link');
	
echo "<center>";
echo  "<a target=blank href='$path_to_root/reporting/sup_pdf.php?sup_no=".$_POST['sup_no']."&company_name=".$_POST['company_name']."
		&system_unit=".$_POST['system_unit']."&serial_no=".$_POST['serial_no']."&assigned_tsg=".$_POST['assigned_tsg']."
		&conformed=".$_POST['conformed']."&from=".$date_after."&to=".$date_before."&type=".$_POST['type']."
		&output=0'>" . _("Print") . "</a> || ";
echo  "<a target=blank href='$path_to_root/reporting/sup_excel.php?sup_no=".$_POST['sup_no']."&company_name=".$_POST['company_name']."
		&system_unit=".$_POST['system_unit']."&serial_no=".$_POST['serial_no']."&assigned_tsg=".$_POST['assigned_tsg']."
		&conformed=".$_POST['conformed']."&from=".$date_after."&to=".$date_before."&type=".$_POST['type']."
		&output=1'>" . _("Export to Excel") . "</a>";
echo "</center>";

div_end();

end_form();
end_page();

?>
