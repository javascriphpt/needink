<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SALESTRANSVIEW';
$path_to_root = "../..";

include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");
include($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();

page(_($help_context = "Customer Balances"), false, false, "", $js);

//----------------------------------------------------------------------------------------

if(isset($_POST['print']))
{
	if($_POST['PARAM_5'] == 0)
	{
		echo "<SCRIPT LANGUAGE=\"javascript\">
				
				window.open ('".$path_to_root."/reporting/rep116.php?PARAM_0=".$_POST['PARAM_0']."&PARAM_1=".$_POST['PARAM_1']."&PARAM_2=".
				$_POST['PARAM_2']."&PARAM_3=".$_POST['PARAM_3']."&PARAM_4=".$_POST['PARAM_4']."&PARAM_5=".$_POST['PARAM_5']."&PARAM_7=".$_POST['PARAM_7']."', 
				'newwindow', config='height=600,width=900, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no');
				
				</SCRIPT>";
	}
	else
	{
		echo "<SCRIPT LANGUAGE=\"javascript\">
			
			window.open ('".$path_to_root."/reporting/rep1166.php?PARAM_0=".$_POST['PARAM_0']."&PARAM_1=".$_POST['PARAM_1']."&PARAM_2=".
			$_POST['PARAM_2']."&PARAM_3=".$_POST['PARAM_3']."&PARAM_4=".$_POST['PARAM_4']."&PARAM_5=".$_POST['PARAM_5']."&PARAM_7=".$_POST['PARAM_7']."', 
			'newwindow', config='height=600,width=900, toolbar=no, menubar=no, scrollbars=no, resizable=no,location=no, directories=no, status=no');
			
			</SCRIPT>";
	}
	
	
}

start_form();

start_table("class='tablestyle_noborder'");
start_row();

date_cells(_("Start Date:"), 'PARAM_0', '', null, -30);
date_cells(_("End Date:"), 'PARAM_1', '', null, 1);
	
currencies_list_cells2(_("Currency Filter: "), 'PARAM_4', null, false, true);

//yesno_list_cells(_("Filter: "), 'PARAM_6', null, "Unpaid", "Paid");

pdf_excel_list_cells(_("Destination: "), 'PARAM_5');

end_row();
start_row();

customer_list_cells(_("Customer: "), 'PARAM_2', null, "No Customer Filter", true);

if(isset($_POST['PARAM_2']) && $_POST['PARAM_2'] != ALL_TEXT)
{
	customer_branches_list_cells(_("Branch: "), $_POST['PARAM_2'], 'PARAM_3', null, true);
}
else
	hidden('PARAM_3', '');
	
if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	locations_list_cells_user(_("Deliver from Location:"), 'PARAM_7', null, false, true, $_SESSION["wa_current_user"]->default_location);
else
	locations_list_cells(_("Location:"), 'PARAM_7', null, true);

submit_cells('print', _("Display : Customer Balances"), true, false, false, ICON_PRINT);

end_row();
end_table(1);

end_form();

//----------------------------------------------------------------------------------------

end_page();

?>