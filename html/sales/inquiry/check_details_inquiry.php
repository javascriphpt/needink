<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTCHECKDETAILSINQ';
$path_to_root = "../..";
include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Check Details Inquiry"), false, false, "", $js);


if (isset($_GET['customer_id']))
{
	$_POST['customer_id'] = $_GET['customer_id'];
}

//------------------------------------------------------------------------------------------------

start_form();

if (!isset($_POST['customer_id']))
	$_POST['customer_id'] = get_global_customer();

start_table("class='tablestyle_noborder'");
start_row();

ref_cells(_("Check #:"), 'check_num', '',null, '', true);

customer_list_cells(_("Customer: "), 'customer_id', null, true);

if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	locations_list_cells_user(_("Deliver from Location:"), 'StockLocation', null, false, true, $_SESSION["wa_current_user"]->default_location);
else
	locations_list_cells(_("Location:"), 'StockLocation', null, true);

date_cells(_("From:"), 'TransAfterDate', '', null, -30);
date_cells(_("To:"), 'TransToDate', '', null, 1);

submit_cells('RefreshInquiry', _("Search"),'',_('Refresh Inquiry'), 'default');
end_row();
end_table();

set_global_customer($_POST['customer_id']);

//------------------------------------------------------------------------------------------------

function trans_view($trans)
{
	$ref_no = get_sales_ref_no($trans["trans_no"], $trans["type"]);
	if($trans["type"] == ST_SALESINVOICE || $trans["type"] == ST_CUSTCREDIT || $trans["type"] == ST_CUSTDELIVERY)
		return get_trans_view_str($trans["type"], $trans["trans_no"], $ref_no["location"]."-".$ref_no["form_type_no"]);
	elseif($trans["type"] == ST_CUSTPAYMENT)
	{
		$ref_no = get_sales_ref_no_OR_PR($trans["trans_no"], $trans["type"]);
		return get_trans_view_str($trans["type"], $trans["trans_no"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"]));
	}
	else
		return get_trans_view_str($trans["type"], $trans["trans_no"], $trans["reference"]);
}

//------------------------------------------------------------------------------------------------
$date_after = date2sql($_POST['TransAfterDate']);
$date_to = date2sql($_POST['TransToDate']);

$sql = "SELECT
		".TB_PREF."debtor_trans.trans_no,
		".TB_PREF."debtors_master.name,
		".TB_PREF."cheque_details.chk_number,
		".TB_PREF."cheque_details.chk_date,
		".TB_PREF."bank_trans.amount,
		".TB_PREF."debtor_trans.type
		FROM
		".TB_PREF."cheque_details ,
		".TB_PREF."debtor_trans ,
		".TB_PREF."debtors_master ,
		".TB_PREF."bank_trans
		WHERE ".TB_PREF."bank_trans.type = 12 AND ".TB_PREF."debtor_trans.type = 12 AND
		".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
		".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type AND
		".TB_PREF."debtor_trans.reference =  ".TB_PREF."bank_trans.ref AND
		".TB_PREF."debtor_trans.debtor_no =  ".TB_PREF."debtors_master.debtor_no AND 
		".TB_PREF."cheque_details.chk_date >= '$date_after' AND	
		".TB_PREF."cheque_details.chk_date <= '$date_to' ";	
		
if (isset($_POST['StockLocation']) && $_POST['StockLocation'] != ALL_TEXT)
	$sql .= " AND ".TB_PREF."debtor_trans.location = ".db_escape($_POST['StockLocation']);

if ($_POST['customer_id'] != ALL_TEXT)
	$sql .= " AND ".TB_PREF."debtor_trans.debtor_no = ".db_escape($_POST['customer_id']);
	
if (isset($_POST['check_num']) && $_POST['check_num'] != "")
	$sql .= " AND ".TB_PREF."cheque_details.chk_number LIKE ".db_escape("%".$_POST['check_num']."%");

//------------------------------------------------------------------------------------------------

$cols = array(
	_("#") => array('fun'=>'trans_view', 'ord'=>''),
	_("Customer") => array('ord'=>''),
	_("Check #") => array('ord'=>''),
	_("Check Date") => array('name'=>'chk_date', 'type'=>'date', 'ord'=>'desc'),
	_("Amount") => array('type'=>'amount', 'ord'=>'')
	);
	
$table =& new_db_pager('trans_tbl', $sql, $cols);

$table->width = "75%";

display_db_pager($table);

end_form();
end_page();

?>
