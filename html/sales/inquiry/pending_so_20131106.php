<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root = "../..";

include($path_to_root . "/includes/db_pager.inc");
include($path_to_root . "/includes/session.inc");
include($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

$page_security = 'SA_PENDINGSO';

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);
if ($use_date_picker)
	$js .= get_js_date_picker();
	
page(_($help_context = "Pending Sales Order"), false, false, "", $js);

//----------------------------------------------------------------------------------------
//simple_page_mode(true);

if (isset($_GET['order_number']))
{
	$_POST['order_number'] = $_GET['order_number'];
}

//--------------------------------------------------------------------------------------------------

function changeOrder($trans_no,$action){
	global $Ajax;
	
	$date_today = date("Y-m-d");
	
	$sql = "UPDATE ".TB_PREF."sales_orders
			SET is_approve = ".db_escape($action).",
				date_approve = ".db_escape($date_today).",
				approving_officer = ".db_escape($_SESSION["wa_current_user"]->username)."
			WHERE trans_type = 30
			AND order_no = ".db_escape($trans_no);
	db_query($sql, "could not update sales order");
		
	display_notification_centered(_("Approved"));
	
	$Ajax->activate('orders_tbl');
}

$checkaction=find_submit('approve');
if($checkaction!=-1)
changeOrder($checkaction,1);

$checkaction=find_submit('disapprove');
if($checkaction!=-1)
changeOrder($checkaction,2);	

//--------------------------------------------------------------------------------------------------


if (get_post('SearchOrders')) 
{
	$Ajax->activate('orders_tbl');
} elseif (get_post('_order_number_changed')) 
{
	$disable = get_post('order_number') !== '';

	$Ajax->addDisable(true, 'StockLocation', $disable);
	$Ajax->addDisable(true, 'OrdersAfterDate', $disable);
	$Ajax->addDisable(true, 'OrdersToDate', $disable);

	if ($disable) {
		$Ajax->addFocus(true, 'order_number');
	} else
		$Ajax->addFocus(true, 'OrdersAfterDate');

	$Ajax->activate('orders_tbl');
}

start_form();

start_table("class='tablestyle_noborder'");
start_row();

ref_cells(_("#:"), 'order_number', '',null, '', true);

// if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	// locations_list_cells_user(_("Deliver from Location:"), 'StockLocation', null, false, true, $_SESSION["wa_current_user"]->default_location);
// else
	locations_list_cells(_("Location:"), 'StockLocation', null, true);

sales_persons_list_cells(_("Sales Person:"), 'salesman', null, _("All sales person"));
date_cells(_("from:"), 'OrdersAfterDate', '', null, -30);
date_cells(_("to:"), 'OrdersToDate', '', null, 1);

submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');

end_row();

end_table(1);

end_form();

//--------------------------------------------------------------------------------------------------
//	Orders inquiry table
//
if (isset($_POST['order_number']) && ($_POST['order_number'] != ""))
{
	$order_number = $_POST['order_number'];
}

$date_after = date2sql($_POST['OrdersAfterDate']);
$date_before = date2sql($_POST['OrdersToDate']);

//figure out the sql required from the inputs available
$sql = "SELECT sorder.*, debtor.name, branch.br_name, salesman.salesman_name,
			Sum(line.unit_price*line.quantity*(1-line.discount_percent))+freight_cost AS OrderValue
	FROM ".TB_PREF."sales_orders as sorder, "
		.TB_PREF."debtors_master as debtor, "
		.TB_PREF."cust_branch as branch, "
		.TB_PREF."salesman as salesman, "
		.TB_PREF."sales_order_details as line
	WHERE sorder.order_no = line.order_no
	AND sorder.trans_type = line.trans_type
	AND sorder.debtor_no = debtor.debtor_no
	AND sorder.branch_code = branch.branch_code
	AND debtor.debtor_no = branch.debtor_no
	AND branch.salesman = salesman.salesman_code
	AND sorder.trans_type = 30 
	AND line.qty_sent < line.quantity";
	
if (isset($_POST['StockLocation']) && $_POST['StockLocation'] != ALL_TEXT)
	$sql .= " AND sorder.from_stk_loc = ".db_escape($_POST['StockLocation']);
	
if (isset($_POST['salesman']) && $_POST['salesman'] != -1)
	$sql .= " AND salesman.salesman_code = ".db_escape($_POST['salesman']);

if (isset($order_number) && $order_number != "")
{
	$sql .= " AND sorder.so_form_type_no LIKE ".db_escape('%'. $order_number . '%');
}
else
{
	$sql .= "  AND sorder.ord_date >= '$date_after'";
	$sql .= "  AND sorder.ord_date <= '$date_before'";
} //end not order number selected

$sql .= " GROUP BY sorder.order_no
		ORDER BY ord_date, sorder.from_stk_loc, sorder.so_form_type_no";	//, sorder.so_form_type, sorder.so_form_type_no";

$result = db_query($sql, "could not query sales order");

//--------------------------------------------------------------------------------------------------

start_form();

div_start('orders_tbl');
start_table($table_style);
$th = array(_("SO #"), _("Customer"), _("Branch"), _("Sales Person"), _("Order Date"), _("Amount"), _("Status"), "");

table_header($th);

$j = 1;
$k = 0; //row colour counter

while ($myrow = db_fetch($result))
{
	alt_table_row_color($k);
	
	$trandate = sql2date($myrow["ord_date"]);
	$ref_no = get_so_ref_no($myrow["order_no"], ST_SALESORDER);
	
	label_cell(get_customer_trans_view_str(ST_SALESORDER, $myrow["order_no"], $ref_no["from_stk_loc"]."-".$ref_no["so_form_type_no"]));
	label_cell($myrow["name"]);
	label_cell($myrow["br_name"]);
	label_cell($myrow["salesman_name"]);
	label_cell($trandate);
	amount_cell($myrow["OrderValue"]);

	if($myrow["is_approve"]==0)
		label_cell("For Approval");
	else if($myrow["is_approve"]==1)
		label_cell("Approved");
	else if($myrow["is_approve"]==2)
		label_cell("Rejected");
	
	$modify = ($myrow["trans_type"] == ST_SALESORDER ? "ModifyOrderNumber" : "ModifyQuotationNumber");
	
	if($myrow["is_approve"]!=1)
		label_cell(pager_link( _("Edit"),$path_to_root . "/sales_order_entry.php?$modify=" . $myrow['order_no'], ICON_EDIT));
	else
		label_cell('');
	
	end_row();
	
	$j++;
	if ($j == 11)
	{
		$j = 1;
		table_header($th);
	}
}

end_table(1);
div_end();

end_form();

end_page();

?>