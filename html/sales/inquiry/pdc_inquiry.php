<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root = "../..";

include($path_to_root . "/includes/db_pager.inc");
include($path_to_root . "/includes/session.inc");
include($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_banking.inc");

$page_security = 'SA_SALESTRANSVIEW';

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);
if ($use_date_picker)
	$js .= get_js_date_picker();
	
page(_($help_context = "PDC Inquiry"), false, false, "", $js);

//----------------------------------------------------------------------------------------

simple_page_mode(true);


//----------------------------------------------------------------------------------------
//	Orders inquiry table
//

function get_pdc($id=null){
$date_after = date2sql($_POST['OrdersAfterDate']);
$date_before = date2sql($_POST['OrdersToDate']);

$sql = "SELECT
			".TB_PREF."cheque_details.id,
			".TB_PREF."cheque_details.bank_trans_id,
			".TB_PREF."cheque_details.bank,
			".TB_PREF."cheque_details.branch,
			".TB_PREF."cheque_details.chk_number,
			".TB_PREF."cheque_details.chk_date,
			".TB_PREF."bank_trans.amount,
			".TB_PREF."debtors_master.name,
			".TB_PREF."debtor_trans.tran_date,
			".TB_PREF."debtor_trans.trans_no,
			".TB_PREF."bank_trans.bank_act
			FROM
			".TB_PREF."cheque_details ,
			".TB_PREF."debtor_trans ,
			".TB_PREF."debtors_master ,
			".TB_PREF."bank_trans
			WHERE ".TB_PREF."bank_trans.type = 12 AND ".TB_PREF."debtor_trans.type = 12 AND
			".TB_PREF."cheque_details.bank_trans_id =  ".TB_PREF."bank_trans.trans_no AND
			".TB_PREF."cheque_details.type =  ".TB_PREF."bank_trans.type AND
			".TB_PREF."debtor_trans.reference =  ".TB_PREF."bank_trans.ref AND
			".TB_PREF."debtor_trans.debtor_no =  ".TB_PREF."debtors_master.debtor_no AND 
			".TB_PREF."cheque_details.receive = 0 AND
			".TB_PREF."cheque_details.chk_date >= '$date_after' AND	
			".TB_PREF."cheque_details.chk_date <= '$date_before' ";				
if(!empty($id))
$sql.= " AND id = ".$id;

$result = db_query($sql, "could not query sales order");
return $result;
}

//----------------------------------------------------------------------------------------

if (isset($_POST['btn_deposit'])) 
{
	global $Refs;

	foreach($_POST as $postkey=>$postval )
    {
		if (strpos($postkey, 'deposit') === 0)
		{
			$id = substr($postkey, strlen('deposit'));
			
			$id_ = explode(',', $id);
			
			// display_error($id_[0]." - ".$_POST['bank_account_'.$id_[0]]." - ".$_POST['from_bank_acct'.$id_[0]]);
			
			$bank_id_ = $id_[0];
			$deposit_ = $_POST['deposit'.$id_[0]];
			$to_bank_acct_ = $_POST['bank_account_'.$id_[0]];
			$from_bank_acct_ = $_POST['from_bank_acct'.$id_[0]];
			$check_date = $_POST['check_date'.$id_[0]];
			$check_no = $_POST['check_no'.$id_[0]];
			$check_amount = $_POST['check_amount'.$id_[0]];
			$or_no = $_POST['or_no'.$id_[0]];
			
			if($from_bank_acct_ == $to_bank_acct_)
			{
				display_error(_("Same bank account. Please change."));
				set_focus('bank_account_'.$id_[0]);
				
				$insert = 0;	
			}
			
			else
			{
			
				$sql = "UPDATE ".TB_PREF."cheque_details 
						  SET receive = 1 
						  WHERE id = ".db_escape($bank_id_);
				// display_error($sql);
				db_query($sql);
				
				
				$ref = $Refs->get_next(ST_BANKTRANSFER);
				
				$trans_no = add_bank_transfer($from_bank_acct_, $to_bank_acct_,
					sql2date($check_date), $check_amount, $ref, $or_no." Check # ".$check_no, 0);
					
				$insert = 1;	
																				
			}
		}
	}
	
	if($insert == 1)
	{
		display_notification('Fund Transfer Successful.');
		$Ajax->activate('_page_body');	
	}

}
	if(isset($_GET['new']))
	$Ajax->activate('_page_body');

//----------------------------------------------------------------------------------------

start_form();

start_table("class='tablestyle_noborder'");
start_row();

date_cells(_("from:"), 'OrdersAfterDate', '', null, -30);
date_cells(_("to:"), 'OrdersToDate', '', null, 1);

submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');

end_row();

end_table(1);

end_form();

if (isset($_POST['SearchOrders'])) 
	$Ajax->activate('orders_tbl');

//----------------------------------------------------------------------------------------

start_form();

div_start('orders_tbl');
start_table($table_style);
$th = array(_("OR #"), _("Customer"), _("Amount"), _("Check #"), _("Check Date"), _("Bank"), _("Deposit"));

table_header($th);

$j = 1;
$k = 0; //row colour counter
$result = get_pdc();
while ($myrow = db_fetch($result))
{
	alt_table_row_color($k);
	
	$chk_date = sql2date($myrow["chk_date"]);
	$ref_no = get_sales_ref_no_OR_PR($myrow["trans_no"], ST_CUSTPAYMENT);
	
	label_cell(get_customer_trans_view_str(ST_CUSTPAYMENT, $myrow["trans_no"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"])));
	hidden('trans_no',$myrow['trans_no']);
	label_cell($myrow["name"]);
	amount_cell($myrow["amount"]);
	label_cell($myrow["chk_number"]);
	label_cell($chk_date);
	bank_accounts_list_cells('', 'bank_account_'.$myrow['id'], null);
	check_cells("", "deposit".$myrow["id"]);
	
	hidden("from_bank_acct".$myrow["id"], $myrow["bank_act"]);
	hidden("check_date".$myrow["id"], $myrow["chk_date"]);
	hidden("check_amount".$myrow["id"], $myrow["amount"]);
	hidden("check_no".$myrow["id"], $myrow["chk_number"]);
	hidden("or_no".$myrow["id"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"]));
	
	end_row();
	
	$j++;
	if ($j == 11)
	{
		$j = 1;
		table_header($th);
	}
}

end_table(1);

// start_table("$table_style");
// start_row();
// bank_accounts_list_cells(_("Account:"), 'bank_account', null);
// textarea_row(_("Memo:"), 'memo_', null, 40,4);

// // date_cells(_("From:"), 'TransAfterDate', '', null, -30);
// // date_cells(_("To:"), 'TransToDate');

// // submit_cells('Show',_("Show"),'','', 'default');
// end_row();
// end_table();

div_end();
submit_center('btn_deposit', _("Deposit"), true, '', 'default');

end_form();

end_page();

?>