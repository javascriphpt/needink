<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$path_to_root = "../..";

include($path_to_root . "/includes/db_pager.inc");
include($path_to_root . "/includes/session.inc");
include($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");
include_once($path_to_root . "/gl/includes/db/gl_db_banking.inc");

$page_security = 'SA_DEPOSITINQUIRY';

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);
if ($use_date_picker)
	$js .= get_js_date_picker();
	
page(_($help_context = "Deposit Inquiry"), false, false, "", $js);

//----------------------------------------------------------------------------------------

simple_page_mode(true);


//----------------------------------------------------------------------------------------
//	Orders inquiry table
//

function get_transactions(){
$date_after = date2sql($_POST['OrdersAfterDate']);
$date_before = date2sql($_POST['OrdersToDate']);

	$sql = "SELECT ".TB_PREF."bank_trans.id, ".TB_PREF."debtor_trans.trans_no, ".TB_PREF."bank_trans.type, 
				".TB_PREF."debtor_trans.debtor_no, ".TB_PREF."bank_trans.bank_act, ".TB_PREF."bank_trans.amount
			FROM ".TB_PREF."debtor_trans, ".TB_PREF."debtors_master, ".TB_PREF."bank_trans
			WHERE ".TB_PREF."debtor_trans.debtor_no =  ".TB_PREF."debtors_master.debtor_no
			AND ".TB_PREF."debtor_trans.trans_no = ".TB_PREF."bank_trans.trans_no
			AND ".TB_PREF."debtor_trans.type = ".TB_PREF."bank_trans.type
			AND ".TB_PREF."debtor_trans.type = " . ST_CUSTPAYMENT . "
			AND ".TB_PREF."debtor_trans.deposited = 0
			AND ".TB_PREF."debtor_trans.tran_date >= '$date_after' 
			AND ".TB_PREF."debtor_trans.tran_date <= '$date_before' 
			AND ".TB_PREF."debtor_trans.trans_no NOT IN ( 
				SELECT id
				FROM ".TB_PREF."voided a
				WHERE type = " . ST_CUSTPAYMENT . "
			) ";	
			
	if (isset($_POST['StockLocation']) && $_POST['StockLocation'] != ALL_TEXT)
		$sql .= " AND ".TB_PREF."debtor_trans.location = ".db_escape($_POST['StockLocation']);
	if (isset($_POST['bank_account']) && $_POST['bank_account'] != ALL_TEXT)
		$sql .= " AND ".TB_PREF."bank_trans.bank_act = ".db_escape($_POST['bank_account']);
		
	$sql .= " ORDER BY form_type_no, location, tran_date";
//display_error($sql);
$result = db_query($sql, "could not retrieve transactions ");
return $result;
}

function get_check_no($type, $trans_no)
{
	$sql = "SELECT chk_number
		FROM ".TB_PREF."cheque_details 
		WHERE ".TB_PREF."cheque_details.type = ".$type."
		AND ".TB_PREF."cheque_details.bank_trans_id = ".$trans_no;
		
	$result = db_query($sql, "could not get check_no");

	$row = db_fetch_row($result);
	
	return $row[0];
}

function get_check_date($type, $trans_no)
{
	$sql = "SELECT chk_date
		FROM ".TB_PREF."cheque_details 
		WHERE ".TB_PREF."cheque_details.type = ".$type."
		AND ".TB_PREF."cheque_details.bank_trans_id = ".$trans_no;
		
	$result = db_query($sql, "could not get check_no");

	$row = db_fetch_row($result);
	
	return $row[0];
}

//----------------------------------------------------------------------------------------

if (isset($_POST['btn_deposit'])) 
{
	global $Refs;
	
	$input_error = 0;
		
	if (!is_date($_POST['DatePaid'])) 
	{
		$input_error = 1;
		display_error(_("The entered date is invalid."));
		set_focus('DatePaid');
	}
	
	if (!is_date_in_fiscalyear($_POST['DatePaid']))
	{
		$input_error = 1;
		display_error(_("The entered date is not in fiscal year."));
		set_focus('DatePaid');
	}
	
	if (!$Refs->is_valid($_POST['ref'])) 
	{
		$input_error = 1;
		display_error(_("You must enter a reference."));
		set_focus('ref');
	}

	if (!is_new_reference($_POST['ref'], ST_BANKTRANSFER)) 
	{
		$input_error = 1;
		display_error(_("The entered reference is already in use."));
		set_focus('ref');
	}
	
	if ($_POST['FromBankAccount'] == $_POST['ToBankAccount']) 
	{
		$input_error = 1;
		display_error(_("The source and destination bank accouts cannot be the same."));
		set_focus('ToBankAccount');
	}
	
	if ($input_error == 0)
	{
		begin_transaction();

		foreach($_POST as $postkey=>$postval )
		{
			if (strpos($postkey, 'deposit') === 0)
			{
				$id = substr($postkey, strlen('deposit'));
				
				$id_ = explode(',', $id);

				$amount += $_POST['amount'.$id_[0]];
				
				$trans_no .= $_POST['trans_no'.$id_[0]].",";
				$or_no .= $_POST['or_no'.$id_[0]].",";
							
				// display_error($id_[0]." - ".$_POST['ToBankAccount']." - ".$_POST['DatePaid']." - ".$_POST['ref']." - ".$_POST['memo_']." - ".
					// $amount." - ".$_POST['or_no'.$id_[0]]." - ".$_POST['from_bank_acct'.$id_[0]]." - ".$_POST['trans_no'.$id_[0]]);
					
				
				$from_bank_acct_ = $_POST['from_bank_acct'.$id_[0]];
				
				if($from_bank_acct_ == $_POST['ToBankAccount'])
				{
					display_error(_("Same bank account. Please change."));
					set_focus('ToBankAccount');
					$insert = 0;	
					exit;
				}			
				else
					$insert = 1;
			}
		}
	}
		
	if($insert == 1)
	{
		// display_error($amount." - ".$_POST['ToBankAccount']." - ".$_POST['DatePaid']." - ".$_POST['ref']." - ".
				// $_POST['memo_']." - ".$trans_no." - ".$from_bank_acct_);
				
		$trans_no = add_bank_transfer($from_bank_acct_, $_POST['ToBankAccount'], $_POST['DatePaid'],
						$amount, $_POST['ref'], "OR# ".$or_no." - ".$_POST['memo_']);
						
		foreach($_POST as $postkey=>$postval )
		{
			if (strpos($postkey, 'deposit') === 0)
			{
				$id2 = substr($postkey, strlen('deposit'));
				
				$id_2 = explode(',', $id2);
				
				// display_error($id_2[0]." - ".$_POST['trans_no'.$id_2[0]]);
				
				$sql001 = "INSERT INTO ".TB_PREF."deposit_allocations (
					amt, date_alloc,
					trans_type_from, trans_no_from, trans_no_to, trans_type_to)
					VALUES (".db_escape($_POST['amount'.$id_2[0]]).", ".db_escape(date2sql($_POST['DatePaid'])).", 
					".db_escape(ST_BANKTRANSFER).", ".db_escape($trans_no).", ".db_escape($_POST['trans_no'.$id_2[0]]).", 
					".db_escape($_POST['type'.$id_2[0]]).")";

				db_query($sql001);
				
				
				$sql002 = "UPDATE ".TB_PREF."debtor_trans 
							SET deposited = 1, deposited_date = ".db_escape(date2sql($_POST['DatePaid']))." 
						WHERE type = 12
						AND trans_no = ".db_escape($_POST['trans_no'.$id_2[0]]);
				db_query($sql002);	
			}
			
		}
				
		display_notification('Fund Transfer Successful.');
		$Ajax->activate('_page_body');	
		echo "<meta http-equiv='Refresh' content='3; url=".$_SERVER['PHP_SELF']."'>\n";
	}
	
	commit_transaction();

}
	if(isset($_GET['new']))
	$Ajax->activate('_page_body');

//----------------------------------------------------------------------------------------

start_form();

start_table("class='tablestyle_noborder'");
start_row();

bank_accounts_list_cells(_("From Account:"), 'bank_account', null, false);

if($_SESSION["wa_current_user"]->can_access_all_locations == 0)
	locations_list_cells_user(_("Location:"), 'StockLocation', null, false, false, $_SESSION["wa_current_user"]->default_location);
else
	locations_list_cells(_("Location:"), 'StockLocation', null, true);

date_cells(_("From:"), 'OrdersAfterDate', '', null, -30);
date_cells(_("To:"), 'OrdersToDate', '', null, 1);

submit_cells('SearchOrders', _("Search"),'',_('Select documents'), 'default');

end_row();

end_table(1);

end_form();

if (isset($_POST['SearchOrders'])) 
	$Ajax->activate('orders_tbl');

//----------------------------------------------------------------------------------------

$btn_id = find_submit('_deposit');	
if ($btn_id != -1)
{
	$add_amt = 0;
	
	foreach($_POST as $postkey=>$postval )
    {
		if (strpos($postkey, 'deposit') === 0)
		{
			$id = substr($postkey, strlen('deposit'));
			
			$id_ = explode(',', $id);
			
			// display_error($id_[0]." = ".$_POST['amount'.$id_[0]]);
			
			if($id_[0] == null)
				$add_amt -= $_POST['amount'.$id_[0]];	
			else
				$add_amt += $_POST['amount'.$id_[0]];	
					
		}
	}	

	$_POST['total_amount'] = number_format($add_amt, 2);
	
	$Ajax->activate('total_amount');
}
else
	$_POST['total_amount'] = number_format2(0, 2);

//----------------------------------------------------------------------------------------

start_form();

global $table_style2, $Refs;

div_start('orders_tbl');
start_table($table_style);
$th = array(_("OR #"), _("Customer"), _("Amount"), _("Check #"), _("Check Date"), _("Deposit"));

table_header($th);

$j = 1;
$k = 0; //row colour counter
$result = get_transactions();
while ($myrow = db_fetch($result))
{
	alt_table_row_color($k);
	
	//$chk_date = sql2date($myrow["chk_date"]);
	$ref_no = get_sales_ref_no_OR_PR($myrow["trans_no"], ST_CUSTPAYMENT);
	
	label_cell(get_customer_trans_view_str(ST_CUSTPAYMENT, $myrow["trans_no"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"])));
	label_cell(get_customer_name($myrow["debtor_no"]));
	amount_cell($myrow["amount"]);
	label_cell(get_check_no(ST_CUSTPAYMENT, $myrow["trans_no"]));
	label_cell(sql2date(get_check_date(ST_CUSTPAYMENT, $myrow["trans_no"])));
	check_cells("", "deposit".$myrow["id"], null, true);
	
	hidden("from_bank_acct".$myrow["id"], $myrow["bank_act"]);
	hidden("type".$myrow["id"], $myrow["type"]);
	hidden("amount".$myrow["id"], $myrow["amount"]);
	hidden("trans_no".$myrow["id"], $myrow["trans_no"]);
	hidden("or_no".$myrow["id"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"]));
	
	end_row();
	
	$j++;
	if ($j == 11)
	{
		$j = 1;
		table_header($th);
	}
}

end_table(1);

// div_end();

//-------------------------------------------------------------------------------------------

br();

start_outer_table($table_style2);

table_section(1);

hidden('FromBankAccount', $_POST['bank_account']);

bank_accounts_list_row(_("To Account:"), 'ToBankAccount', null, false);

date_row(_("Transfer Date:"), 'DatePaid', '', null, 0, 0, 0, null, true);

ref_row(_("Reference:"), 'ref', '', $Refs->get_next(ST_BANKTRANSFER));

amount_row(_("Total Amount:"), 'total_amount');

textarea_row(_("Memo:"), 'memo_', null, 40,4);

end_outer_table(1); // outer table

submit_center('btn_deposit', _("Deposit"), true, '', 'default');
//submit_center('AddPayment',_("Enter Transfer"), true, '', 'default');

div_end();

//-------------------------------------------------------------------------------------------

end_form();

end_page();

?>