<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPINQUIRY';
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/sup_items_cart.inc");

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/sales/includes/ui/sup_ui.inc");
include_once($path_to_root . "/inventory/includes/inventory_db.inc");
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Support Process"), false, false, "", $js);

simple_page_mode(true);

//-----------------------------------------------------------------------------------------------

check_db_has_customers(_("There are no customers defined in the system. Please define a customer to add customer branches."));

check_db_has_costable_items(_("There are no inventory items defined in the system which can be adjusted (Purchased or Manufactured)."));

check_db_has_movement_types(_("There are no inventory movement types defined in the system. Please define at least one inventory adjustment type."));

//-----------------------------------------------------------------------------------------------

if (isset($_GET['UpdatedID'])) 
{
	$trans_no = $_GET['UpdatedID'];

	display_notification_centered(_("SUP has been updated."));
	display_note("<a href=".$path_to_root . "/sales/view/view_sup.php?trans_no=".$trans_no." target=_blank>View this form</a>");

	display_footer_exit();
}
//-----------------------------------------------------------------------------------------------

function can_process()
{	
	if (strlen($_POST['company_name']) == 0)
	{
		$input_error = 1;
		display_error(_("Company name cannot be empty."));
		set_focus('company_name');
		return false;
	}

	if (strlen($_POST['system_unit']) == 0)
	{
		$input_error = 1;
		display_error(_("System / Unit cannot be empty."));
		set_focus('system_unit');
		return false;
	}
	
	if (strlen($_POST['serial_no']) == 0)
	{
		$input_error = 1;
		display_error(_("Serial No cannot be empty."));
		set_focus('serial_no');
		return false;
	}
	
	if (strlen($_POST['sup_no']) == 0)
	{
		$input_error = 1;
		display_error(_("SUP No cannot be empty."));
		set_focus('sup_no');
		return false;
	}
	
	return true;
}

//-------------------------------------------------------------------------------

if (isset($_POST['Process']) && can_process()){

	if($_POST['type'] == 1) // in-house
	{
		$received_time = $_POST['received_time_h'].":".$_POST['received_time_m']." ".$_POST['received_time_t'];
		$required_time = $_POST['required_time_h'].":".$_POST['required_time_m']." ".$_POST['required_time_t'];
		$started_time = "00:00 AM";
		$completed_time = "00:00 AM";
		$_POST['started_date'] = Today();
		$_POST['completed_date'] = Today();
	}
	else
	{
		$received_time = "00:00 AM";
		$required_time = "00:00 AM";
		$_POST['received_date'] = Today();
		$_POST['required_date'] = Today();
		$started_time = $_POST['started_time_h'].":".$_POST['started_time_m']." ".$_POST['started_time_t'];
		$completed_time = $_POST['completed_time_h'].":".$_POST['completed_time_m']." ".$_POST['completed_time_t'];
	}
	
	begin_transaction();
	
	if(isset($_POST['degree_of_service_two']) && !empty($_POST['degree_of_service_two']))
		$_POST['degree_of_service_two'] = $_POST['degree_of_service_two'];
	else
		$_POST['degree_of_service_two'] = '0';
	
	$sql = "UPDATE ".TB_PREF."sup SET
				company_name = ".db_escape($_POST['company_name']) . ", 
				address = " .db_escape($_POST['address']) . ", 
				contact_person = ".db_escape($_POST['contact_person']) . ", 
				tel_no = " .db_escape($_POST['tel_no']) . ",
				system_unit = ".db_escape($_POST['system_unit']) . ", 
				location = " .db_escape($_POST['location']) . ", 
				serial_no = " .db_escape($_POST['serial_no']) . ", 
				accessories = " .db_escape($_POST['accessories']) . ", 
				customer_comment = " .db_escape($_POST['customer_comment']) . ", 
				toner_cartridge = " .db_escape($_POST['toner_cartridge']) . ", 
				tsg_evaluation = " .db_escape($_POST['tsg_evaluation']) . ", 
				tsg_recommendation = " .db_escape($_POST['tsg_recommendation']) . ", 
				sup_no = " .db_escape($_POST['sup_no']) . ", 
				received_date = " .db_escape(date2sql($_POST['received_date'])) . ", 
				received_time = " .db_escape($received_time) . ", 
				required_date = " .db_escape(date2sql($_POST['required_date'])) . ", 
				required_time = " .db_escape($required_time) . ", 
				type = " .db_escape($_POST['type']) . ", 
				assigned_tsg = " .db_escape($_POST['assigned_tsg']) . ", 
				started_date = " .db_escape(date2sql($_POST['started_date'])) . ", 
				started_time = " .db_escape($started_time) . ", 
				warranty = " .db_escape($_POST['warranty']) . ", 
				completed_date = " .db_escape(date2sql($_POST['completed_date'])) . ", 
				completed_time = " .db_escape($completed_time) . ", 
				conformed = " .db_escape($_POST['conformed']) . ", 
				status = " .db_escape($_POST['status']) . ", 
				repair_details = " .db_escape($_POST['repair_details']) . ",
				aic = " .db_escape($_POST['aic']) . ",
				degree_of_service = " .db_escape($_POST['degree_of_service']) . ",
				degree_of_service_one = " .db_escape($_POST['degree_of_service_one']) . ",
				degree_of_service_two = " .db_escape($_POST['degree_of_service_two']) . "
			WHERE id = ".db_escape($_POST['id']);

	db_query($sql,"Support process could not be inserted.");
		
	add_audit_trail(600, $_POST['id'], Today(), 'Updated');
	
	commit_transaction();
	
   	meta_forward($_SERVER['PHP_SELF'], "UpdatedID=".$_POST['id']);
} /*end of process credit note */

//-----------------------------------------------------------------------------------------------

if(isset($_GET['ModifySUPNumber']) && is_numeric($_GET['ModifySUPNumber']))
{
	$trans_id = $_GET['ModifySUPNumber'];
	
	$sql = "SELECT sup.*
			FROM ".TB_PREF."sup as sup
			WHERE sup.id = ".db_escape($trans_id);
	$result = db_query($sql, "could not retrieve transaction");
	$myrow = db_fetch($result);
	
	$_POST['id'] = $myrow["id"];
	$_POST['company_name'] = $myrow["company_name"];
	$_POST['address'] = $myrow["address"];
	$_POST['contact_person'] = $myrow["contact_person"];
	$_POST['tel_no'] = $myrow["tel_no"];
	$_POST['location'] = $myrow["location"];
	$_POST['system_unit'] = $myrow["system_unit"];
	$_POST['serial_no'] = $myrow["serial_no"];
	$_POST['accessories'] = $myrow["accessories"];
	$_POST['customer_comment'] = $myrow["customer_comment"];
	$_POST['toner_cartridge'] = $myrow["toner_cartridge"];
	$_POST['tsg_evaluation'] = $myrow["tsg_evaluation"];
	$_POST['tsg_recommendation'] = $myrow["tsg_recommendation"];
	$_POST['sup_no'] = $myrow["sup_no"];
	$_POST['assigned_tsg'] = $myrow["assigned_tsg"];
	$_POST['type'] = $myrow["type"];
	
	$_POST['received_date'] = sql2date($myrow["received_date"]);
	$_POST['received_time'] = $myrow["received_time"];
	$_POST['received_time_h'] = substr($myrow["received_time"], 0, 2);
	$_POST['received_time_m'] = substr($myrow["received_time"], 3, 2);
	$_POST['received_time_t'] = substr($myrow["received_time"], -2);
	
	$_POST['required_date'] = sql2date($myrow["required_date"]);
	$_POST['required_time'] = $myrow["required_time"];
	$_POST['required_time_h'] = substr($myrow["required_time"], 0, 2);
	$_POST['required_time_m'] = substr($myrow["required_time"], 3, 2);
	$_POST['required_time_t'] = substr($myrow["required_time"], -2);
	
	$_POST['started_date'] = sql2date($myrow["started_date"]);
	$_POST['started_time'] = $myrow["started_time"];
	$_POST['started_time_h'] = substr($myrow["started_time"], 0, 2);
	$_POST['started_time_m'] = substr($myrow["started_time"], 3, 2);
	$_POST['started_time_t'] = substr($myrow["started_time"], -2);
	
	$_POST['completed_date'] = sql2date($myrow["completed_date"]);
	$_POST['completed_time'] = $myrow["completed_time"];
	$_POST['completed_time_h'] = substr($myrow["completed_time"], 0, 2);
	$_POST['completed_time_m'] = substr($myrow["completed_time"], 3, 2);
	$_POST['completed_time_t'] = substr($myrow["completed_time"], -2);
	
	$_POST['warranty'] = $myrow["warranty"];
	$_POST['conformed'] = $myrow["conformed"];
	$_POST['status'] = $myrow["status"];
	$_POST['repair_details'] = $myrow["repair_details"];
	$_POST['aic'] = $myrow["aic"];
	$_POST['degree_of_service'] = $myrow["degree_of_service"];
	$_POST['degree_of_service_one'] = $myrow["degree_of_service_one"];
	$_POST['degree_of_service_two'] = $myrow["degree_of_service_two"];
	
	$sql2 = "SELECT part.*, stock.units
			FROM ".TB_PREF."sup as sup, ".TB_PREF."sup_part_used as part, ".TB_PREF."stock_master as stock
			WHERE sup.id = part.sup_no
			AND stock.stock_id = part.stock_id
			AND sup.id = ".db_escape($trans_id);
	$res = db_query($sql2, "could not retrieve parts used");
}

//-----------------------------------------------------------------------------------------------

function get_sup_type($id)
{
	$sql = "SELECT description FROM ".TB_PREF."sup_type WHERE id=".db_escape($id);

	$result = db_query($sql, "could not get sup type");

	$row = db_fetch_row($result);

	return $row[0];
}

//-----------------------------------------------------------------------------------------------

start_form();

div_start('link');

hidden('id', $_POST['id']);

start_outer_table($table_style2, 5);

table_section(1);

table_section_title(_("ACCOUNT NAME & DESCRIPTION"));
text_row(_("Company Name:"), 'company_name', $_POST['company_name'], 40, 50);
textarea_row(_("Address:"), 'address', $_POST['address'], 35, 4);
text_row(_("Contact Person:"), 'contact_person', $_POST['contact_person'], 40, 50);
text_row(_("Tel No.:"), 'tel_no', $_POST['tel_no'], 40, 50);
text_row(_("Location / Dept.:"), 'location', $_POST['location'], 40, 50);
text_row(_("Machine / Unit:"), 'system_unit', $_POST['system_unit'], 40, 50);
text_row(_("Serial No.:"), 'serial_no', $_POST['serial_no'], 40, 50);
text_row(_("AIC:"), 'aic', $_POST['aic'], 40, 50);
text_row(_("Accessories:"), 'accessories', $_POST['accessories'], 40, 50);

table_section_title(_("CUSTOMER COMMENT / COMPLAINT/ INSTRUCTION"));
textarea_row(_(" "), 'customer_comment', $_POST['customer_comment'], 35, 4);

table_section_title(_("TONER CARTRIDGE USED"));
toner_cartridge_list_row( _(""), 'toner_cartridge', $_POST['toner_cartridge']);

table_section_title(_("TSG EVALUATION / DIAGNOSTIC"));
textarea_row(_(" "), 'tsg_evaluation', $_POST['tsg_evaluation'], 35, 4);

table_section_title(_("TSG RECOMMENDATION"));
textarea_row(_(" "), 'tsg_recommendation', $_POST['tsg_recommendation'], 35, 4);

table_section(2);

table_section_title(_("SUPPORT PROCESS NO."));
text_row(_("SUP No.: "), 'sup_no', $_POST['sup_no'], 30, 30);
text_row(_("Assigned TSG : "), 'assigned_tsg', $_POST['assigned_tsg'], 40, 50);

// sup_type_list_row( _("Type : "), 'type', $_POST['type']);
label_row(_("Type:"), get_sup_type($_POST['type']));
hidden('type', $_POST['type']);

if($_POST['type'] == 1) // in-house
{
	date_row(_("Received (Date):"), 'received_date');

	echo "<tr>";
		echo "<td class='label'>Received (Time)</td>";
		echo "<td>";
			echo "<select name='received_time_h'>";
			
			for($x=1 ; $x<=12 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='received_time_m'>";
			
			for($x=0 ; $x<=59 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='received_time_t'>";
				echo " <option value=AM>AM</option>
					<option value=PM>PM</option>";
			echo "</select>";
			
		echo "</td>";
	echo "</tr>";

	date_row(_("Required (Date):"), 'required_date');

	echo "<tr>";
		echo "<td class='label'>Required (Time)</td>";
		echo "<td>";
			echo "<select name='required_time_h'>";
			
			for($x=1 ; $x<=12 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='required_time_m'>";
			
			for($x=0 ; $x<=59 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='required_time_t'>";
				echo " <option value=AM>AM</option>
					<option value=PM>PM</option>";
			echo "</select>";
			
		echo "</td>";
	echo "</tr>";
}
else
{
	date_row(_("Started (Date):"), 'started_date');

	echo "<tr>";
		echo "<td class='label'>Started (Time)</td>";
		echo "<td>";
			echo "<select name='started_time_h'>";
			
			for($x=1 ; $x<=12 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='started_time_m'>";
			
			for($x=0 ; $x<=59 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='started_time_t'>";
				echo " <option value=AM>AM</option>
					<option value=PM>PM</option>";
			echo "</select>";
			
		echo "</td>";
	echo "</tr>";

	date_row(_("Completed (Date):"), 'completed_date');

	echo "<tr>";
		echo "<td class='label'>Completed (Time)</td>";
		echo "<td>";
			echo "<select name='completed_time_h'>";
			
			for($x=1 ; $x<=12 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='completed_time_m'>";
			
			for($x=0 ; $x<=59 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='completed_time_t'>";
				echo " <option value=AM>AM</option>
					<option value=PM>PM</option>";
			echo "</select>";
			
		echo "</td>";
	echo "</tr>";
}

sup_warranty_list_row( _("Warranty Mode : "), 'warranty', $_POST['warranty']);
text_row(_("Conformed : "), 'conformed', $_POST['conformed'], 40, 50);

table_section_title(_("REPAIR DETAILS"));
textarea_row(_(" "), 'repair_details', $_POST['repair_details'], 35, 4);
sup_status_list_row( _("Status : "), 'status', $_POST['status']);

table_section_title(_("DEGREE OF SERVICE"));
// textarea_row(_(" "), 'degree_of_service', $_POST['degree_of_service'], 35, 4);
sup_degree_of_service_list_row( _("Level: "), 'degree_of_service', $_POST['degree_of_service'], false, true);

if(isset($_POST['degree_of_service']))
{
	$_POST['degree_of_service'] = $_POST['degree_of_service'];
	$Ajax->activate('link');
}
	
if(isset($_POST['degree_of_service_one']))
{
	$_POST['degree_of_service_one'] = $_POST['degree_of_service_one'];
	$Ajax->activate('link');
}

sup_degree_of_service_one_list_row( _("Description: "), 'degree_of_service_one', $_POST['degree_of_service_one'], false, true, $_POST['degree_of_service']);
sup_degree_of_service_two_list_row( _("Description: "), 'degree_of_service_two', $_POST['degree_of_service_two'], false, true, $_POST['degree_of_service_one']);

end_outer_table(1);

display_heading("PART(s) USED");
div_start('items_table');
start_table("$table_style width=60%");

if (db_num_rows($res) > 0)
{
	$th = array(_("ITEM"), _("QTY"), _("UNIT"), _("PRICE"), _("DESCRIPTION / PART / SERIAL NUMBER"));
	table_header($th);

	$k = 0;	//row colour counter
	$sub_total = 0;
	while ($myrow2 = db_fetch($res))
	{
		//if($myrow2['quantity']==0) continue;
		alt_table_row_color($k);
	
		label_cell($myrow2["stock_id"]." - ".$myrow2["description"]);
        qty_cell($myrow2["quantity"], false, get_qty_dec($myrow2["stock_id"]));
		label_cell($myrow2["units"]);
		amount_cell($myrow2["unit_price"]);
		label_cell(nl2br($myrow2["comment"]));
        
	end_row();
	} //end while there are line items to print out

}
else
	display_note(_("There are no line items on this dispatch."), 1, 2);

end_table(1);

echo "<br>";

submit_center_first('Process', _("Commit SUP changes"), '', 'default');

div_end();

end_form();

end_page();

?>
