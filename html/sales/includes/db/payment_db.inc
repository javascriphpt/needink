<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
/*
  Write/update customer payment.
*/
function write_customer_payment($trans_no, $customer_id, $branch_id, $bank_account,
	$date_, $ref, $amount, $discount, $memo_, $rate=0, $charge=0, $ewt=0, 
	$location, $or_form_type, $or_form_type_no)
{
	global $Refs;

	begin_transaction();

	$company_record = get_company_prefs();

	$payment_no = write_customer_trans(ST_CUSTPAYMENT, $trans_no, $customer_id, $branch_id, 
		$date_, $ref, $amount, $discount, 0, 0, 0, 0, 0, 0, 0, "", 0, $rate, 0, 0, $location, 
		$or_form_type_no, $ewt, $or_form_type);

	$bank_gl_account = get_bank_gl_account($bank_account);

	if ($trans_no != 0) {
	  delete_comments(ST_CUSTPAYMENT, $trans_no);
	  void_bank_trans(ST_CUSTPAYMENT, $trans_no, true);
	  void_gl_trans(ST_CUSTPAYMENT, $trans_no, true);
	  void_cust_allocations(ST_CUSTPAYMENT, $trans_no, $date_);
	}
	$total = 0;
	/* Bank account entry first */
	$total += add_gl_trans_customer(ST_CUSTPAYMENT, $payment_no, $date_,
		$bank_gl_account, 0, 0, $amount - $charge,  $customer_id,
		"Cannot insert a GL transaction for the bank account debit", $rate);

	if ($branch_id != ANY_NUMERIC) {

		$branch_data = get_branch_accounts($branch_id);

		$debtors_account = $branch_data["receivables_account"];
		$discount_account = $branch_data["payment_discount_account"];

	} else {
		$debtors_account = $company_record["debtors_act"];
		$discount_account = $company_record["default_prompt_payment_act"];
	}

	if (($ewt + $discount + $amount) != 0)	{
	/* Now Credit Debtors account with receipts + discounts */
	$total += add_gl_trans_customer(ST_CUSTPAYMENT, $payment_no, $date_,
		$debtors_account, 0, 0, -($ewt + $discount + $amount), $customer_id,
		"Cannot insert a GL transaction for the debtors account credit", $rate);
	}
	if ($discount != 0)	{
		/* Now Debit discount account with discounts allowed*/
		$total += add_gl_trans_customer(ST_CUSTPAYMENT, $payment_no, $date_,
			$discount_account, 0, 0, $discount, $customer_id,
			"Cannot insert a GL transaction for the payment discount debit", $rate);
	}
	
	if ($ewt != 0)	{
		/* Now Debit discount account with discounts allowed*/
		$ewt_act = get_company_pref('default_sales_ewt_act');
		$total += add_gl_trans_customer(ST_CUSTPAYMENT, $payment_no, $date_,
			$ewt_act, 0, 0, $ewt, $customer_id,
			"Cannot insert a GL transaction for the ewt debit", $rate);
	}

	if ($charge != 0)	{
		/* Now Debit bank charge account with charges */
		$charge_act = get_company_pref('bank_charge_act');
		$total += add_gl_trans_customer(ST_CUSTPAYMENT, $payment_no, $date_,
			$charge_act, 0, 0, $charge, $customer_id,
			"Cannot insert a GL transaction for the payment bank charge debit", $rate);
	}
	/*Post a balance post if $total != 0 */
	add_gl_balance(ST_CUSTPAYMENT, $payment_no, $date_, -$total, PT_CUSTOMER, $customer_id);	

	/*now enter the bank_trans entry */
	add_bank_trans(ST_CUSTPAYMENT, $payment_no, $bank_account, $ref,
		$date_, $amount - $charge, PT_CUSTOMER, $customer_id,
		get_customer_currency($customer_id), "", $rate);
		
	if(get_bank_trans_type($bank_account) == 1 && $_POST['type_'] == 2)
	{
		add_check($payment_no, ST_CUSTPAYMENT, $_POST['Bank'], $_POST['Branch'], $_POST['ChkNo'], $_POST['Cheque_Date']);		
	}

	add_comments(ST_CUSTPAYMENT, $payment_no, $date_, $memo_);

	$Refs->save(ST_CUSTPAYMENT, $payment_no, $ref);
	
	update_so_form_type_no_OR_PR($location, ST_CUSTPAYMENT, $or_form_type_no, $or_form_type);
	add_so_form_type_no_OR_PR(ST_CUSTPAYMENT, $payment_no, $or_form_type_no, $location, $or_form_type);
	
	commit_transaction();

	return $payment_no;
}

//-------------------------------------------------------------------------------------------------

function add_check($trans_id,$type,$bank,$bank_branch,$chk_number,$date)
{
	$sql = "INSERT  INTO ".TB_PREF."cheque_details(bank_trans_id, bank, branch, chk_number, chk_date, type)
			VALUES($trans_id, ".db_escape($bank).", ".db_escape($bank_branch).", ".db_escape($chk_number).", '".date2sql($date)."', $type)";
			
	db_query($sql,'unable to add check details');
}

//-------------------------------------------------------------------------------------------------

function void_customer_payment($type, $type_no)
{
	begin_transaction();

	void_bank_trans($type, $type_no, true);
	void_gl_trans($type, $type_no, true);
	void_cust_allocations($type, $type_no);
	void_customer_trans($type, $type_no);

	commit_transaction();
}


?>