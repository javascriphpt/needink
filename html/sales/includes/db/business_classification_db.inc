<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/

function get_business_classification($id)
{
	$sql = "SELECT * FROM ".TB_PREF."business_classification";
	
	if(isset($id))
	$sql.= " WHERE id=".db_escape($id);
	$result = db_query($sql, "could not get business type");

	return $result;
}

function get_business_classification_name($id)
{
	$sql = "SELECT name FROM ".TB_PREF."business_classification WHERE id=".db_escape($id);

	$result = db_query($sql, "could not get business type");

	$row = db_fetch_row($result);

	return $row[0];
}
?>