<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPINQUIRY';
$path_to_root = "..";
include_once($path_to_root . "/includes/ui/sup_items_cart.inc");

include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/sales/includes/ui/sup_ui.inc");
include_once($path_to_root . "/inventory/includes/inventory_db.inc");
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
if ($use_date_picker)
	$js .= get_js_date_picker();
page(_($help_context = "Support Process"), false, false, "", $js);

simple_page_mode(true);

//-----------------------------------------------------------------------------------------------

check_db_has_customers(_("There are no customers defined in the system. Please define a customer to add customer branches."));

check_db_has_costable_items(_("There are no inventory items defined in the system which can be adjusted (Purchased or Manufactured)."));

check_db_has_movement_types(_("There are no inventory movement types defined in the system. Please define at least one inventory adjustment type."));

//-----------------------------------------------------------------------------------------------

if (isset($_GET['AddedID'])) 
{
	$trans_no = $_GET['AddedID'];

	display_notification_centered(_("Support Process has been added"));
	display_note("<a href=".$path_to_root . "/sales/view/view_sup.php?trans_no=".$trans_no." target=_blank>View this form</a>");

	hyperlink_no_params($_SERVER['PHP_SELF'], _("Enter &Another Support Process"));

	display_footer_exit();
}
//--------------------------------------------------------------------------------------------------

function line_start_focus() {
  global 	$Ajax;

  $Ajax->activate('items_table');
  set_focus('_stock_id_edit');
}
//-----------------------------------------------------------------------------------------------

function handle_new_order()
{
	if (isset($_SESSION['adj_items']))
	{
		$_SESSION['adj_items']->clear_items();
		unset ($_SESSION['adj_items']);
	}

    //session_register("adj_items");

    $_SESSION['adj_items'] = new items_cart(ST_INVADJUST);
	$_POST['AdjDate'] = new_doc_date();
	if (!is_date_in_fiscalyear($_POST['AdjDate']))
		$_POST['AdjDate'] = end_fiscalyear();
	$_SESSION['adj_items']->tran_date = $_POST['AdjDate'];	
}

//-----------------------------------------------------------------------------------------------

function can_process()
{	
	if (strlen($_POST['sup_no']) != 0)
	{
		$sql = "SELECT sup_no FROM ".TB_PREF."sup WHERE sup_no = '".$_POST['sup_no']."'";
		$result = db_query($sql, "sup_no"); 
				
		if (db_num_rows($result) != 0)
		{
			$input_error = 1;
			display_error(_("The entered SUP No is already in use."));
			set_focus('sup_no');
			return false;
		}
	}
	
	if (strlen($_POST['company_name']) == 0)
	{
		$input_error = 1;
		display_error(_("Company name cannot be empty."));
		set_focus('company_name');
		return false;
	}

	if (strlen($_POST['system_unit']) == 0)
	{
		$input_error = 1;
		display_error(_("System / Unit cannot be empty."));
		set_focus('system_unit');
		return false;
	}
	
	if (strlen($_POST['serial_no']) == 0)
	{
		$input_error = 1;
		display_error(_("Serial No cannot be empty."));
		set_focus('serial_no');
		return false;
	}
	
	if (strlen($_POST['sup_no']) == 0)
	{
		$input_error = 1;
		display_error(_("SUP No cannot be empty."));
		set_focus('sup_no');
		return false;
	}
	
	$adj = &$_SESSION['adj_items'];

	if (count($adj->line_items) == 0)	{
		display_error(_("You must enter at least one non empty item line."));
		set_focus('stock_id');
		return false;
	}
	
	return true;
}

//-------------------------------------------------------------------------------

if (isset($_POST['Process']) && can_process()){

	if($_POST['type'] == 1) // in-house
	{
		$received_time = $_POST['received_time_h'].":".$_POST['received_time_m']." ".$_POST['received_time_t'];
		$required_time = $_POST['required_time_h'].":".$_POST['required_time_m']." ".$_POST['required_time_t'];
		$started_time = "00:00 AM";
		$completed_time = "00:00 AM";
		$_POST['started_date'] = Today();
		$_POST['completed_date'] = Today();
	}
	else
	{
		$received_time = "00:00 AM";
		$required_time = "00:00 AM";
		$_POST['received_date'] = Today();
		$_POST['required_date'] = Today();
		$started_time = $_POST['started_time_h'].":".$_POST['started_time_m']." ".$_POST['started_time_t'];
		$completed_time = $_POST['completed_time_h'].":".$_POST['completed_time_m']." ".$_POST['completed_time_t'];
	}
	
	begin_transaction();
	
	if(isset($_POST['degree_of_service_two']) && !empty($_POST['degree_of_service_two']))
		$_POST['degree_of_service_two'] = $_POST['degree_of_service_two'];
	else
		$_POST['degree_of_service_two'] = '0';
	
	$sql = "INSERT INTO ".TB_PREF."sup (debtor_no, branch_code, company_name, address, contact_person,
			tel_no, system_unit, location, serial_no, accessories, 
			customer_comment, toner_cartridge, tsg_evaluation, tsg_recommendation, sup_no,
			received_date, received_time, required_date, required_time, type, 
			assigned_tsg, started_date, started_time, warranty, completed_date,
			completed_time, conformed, status, repair_details, aic, degree_of_service, 
			degree_of_service_one, degree_of_service_two)
			VALUES (".db_escape($_POST['debtor_no']). ",".db_escape($_POST['branch_code']) . ", "
				.db_escape($_POST['company_name']) . ", " .db_escape($_POST['address']) . ", "
				.db_escape($_POST['contact_person']) . ", " .db_escape($_POST['tel_no']) . ", "
				.db_escape($_POST['system_unit']) . ", " .db_escape($_POST['location']) . ", "
				.db_escape($_POST['serial_no']) . ", " .db_escape($_POST['accessories']) . ", "
				.db_escape($_POST['customer_comment']) . ", " .db_escape($_POST['toner_cartridge']) . ", "
				.db_escape($_POST['tsg_evaluation']) . ", " .db_escape($_POST['tsg_recommendation']) . ", "
				.db_escape($_POST['sup_no']) . ", " .db_escape(date2sql($_POST['received_date'])) . ", "
				.db_escape($received_time) . ", " .db_escape(date2sql($_POST['required_date'])) . ", "
				.db_escape($required_time) . ", " .db_escape($_POST['type']) . ", "
				.db_escape($_POST['assigned_tsg']) . ", " .db_escape(date2sql($_POST['started_date'])) . ", "
				.db_escape($started_time) . ", " .db_escape($_POST['warranty']) . ", "
				.db_escape(date2sql($_POST['completed_date'])) . ", " .db_escape($completed_time) . ", "
				.db_escape($_POST['conformed']) . ", " .db_escape($_POST['status']) . ", "
				.db_escape($_POST['repair_details']) . ", " .db_escape($_POST['aic']) . ", " .db_escape($_POST['degree_of_service']) . ","
				.db_escape($_POST['degree_of_service_one']) . ", " .db_escape($_POST['degree_of_service_two']) . ")";

	db_query($sql,"Support process could not be inserted.");
	
	$sup_no = db_insert_id();
		
	foreach ($_SESSION['adj_items']->line_items as $line_item)
	{
		$sql = "INSERT INTO ".TB_PREF."sup_part_used (sup_no, stock_id, description, unit_price, quantity, comment)
				VALUES (".db_escape($sup_no). ",".db_escape($line_item->stock_id) . ", "
					.db_escape($line_item->item_description) . ", " .db_escape($line_item->price) . ", "
					.db_escape($line_item->quantity) . ", " .db_escape($line_item->comment) . ")";
					
		db_query($sql,"Part Used could not be inserted.");		
	}
	
	add_audit_trail(600, $sup_no, Today());
	
	commit_transaction();
	
	$_SESSION['adj_items']->clear_items();
	unset($_SESSION['adj_items']);

   	meta_forward($_SERVER['PHP_SELF'], "AddedID=$sup_no");
} /*end of process credit note */

//-----------------------------------------------------------------------------------------------

function check_item_data()
{
	if (!check_num('qty',0))
	{
		display_error(_("The quantity entered is negative or invalid."));
		set_focus('qty');
		return false;
	}

   	return true;
}

//-----------------------------------------------------------------------------------------------

function handle_update_item()
{
    if($_POST['UpdateItem'] != "" && check_item_data())
    {
		$id = $_POST['LineNo'];
    	$_SESSION['adj_items']->update_cart_item($id, input_num('qty'), 
			0, $_POST['comment'], input_num('price'));
    }
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------

function handle_delete_item($id)
{
	$_SESSION['adj_items']->remove_from_cart($id);
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------

function handle_new_item()
{
	if (!check_item_data())
		return;

	add_to_order($_SESSION['adj_items'], $_POST['stock_id'], 
	  input_num('qty'), 0, $_POST['comment'], input_num('price'));
	line_start_focus();
}

//-----------------------------------------------------------------------------------------------
$id = find_submit('Delete');
if ($id != -1)
	handle_delete_item($id);

if (isset($_POST['AddItem']))
	handle_new_item();

if (isset($_POST['UpdateItem']))
	handle_update_item();

if (isset($_POST['CancelItemChanges'])) {
	line_start_focus();
}
//-----------------------------------------------------------------------------------------------

if (isset($_GET['NewSUP']) || !isset($_SESSION['adj_items']))
{
	handle_new_order();
}

//-----------------------------------------------------------------------------------------------

start_form();

echo "<center>" . _("Select a customer: ") . "&nbsp;&nbsp;";
echo customer_list('customer_id', null, false, true);
customer_branches_list_row(_("Branch:"),  $_POST['customer_id'], 'branch_id', null, false, true, true, true);
echo "</center><br>";

$num_branches = db_customer_has_branches($_POST['customer_id']);

$sql = "SELECT c.name, c.address, b.*
		FROM ".TB_PREF."cust_branch b, "
			.TB_PREF."debtors_master c
		WHERE b.debtor_no=c.debtor_no
		AND b.debtor_no = ".db_escape($_POST['customer_id'])."
		AND b.branch_code = ".db_escape($_POST['branch_id']);
$result = db_query($sql,"check failed");
$myrow = db_fetch($result);

$_POST['debtor_name'] = $myrow["name"];
$_POST['address'] = $myrow["address"];
$_POST['tel_no'] = $myrow["phone"];
$_POST['contact_name'] = $myrow["contact_name"];

if(isset($_POST['customer_id']) || isset($_POST['branch_id']))
	$Ajax->activate('_page_body');

start_outer_table($table_style2, 5);

table_section(1);

hidden('debtor_no', $myrow["debtor_no"]);
hidden('branch_code', $myrow["branch_code"]);
hidden('selected_id', $selected_id);
hidden('popup', @$_REQUEST['popup']);

table_section_title(_("ACCOUNT NAME & DESCRIPTION"));
text_row(_("Company Name:"), 'company_name', $_POST['debtor_name'], 40, 50);
textarea_row(_("Address:"), 'address', $_POST['address'], 35, 4);
text_row(_("Contact Person:"), 'contact_person', $_POST['contact_name'], 40, 50);
text_row(_("Tel No.:"), 'tel_no', $_POST['tel_no'], 40, 50);
text_row(_("Location / Dept.:"), 'location', null, 40, 50);
text_row(_("Machine / Unit:"), 'system_unit', null, 40, 50);
text_row(_("Serial No.:"), 'serial_no', null, 40, 50);
text_row(_("AIC:"), 'aic', null, 40, 50);
text_row(_("Accessories:"), 'accessories', null, 40, 50);

table_section_title(_("CUSTOMER COMMENT / COMPLAINT/ INSTRUCTION"));
textarea_row(_(" "), 'customer_comment', null, 35, 4);

table_section_title(_("TONER CARTRIDGE USED"));
toner_cartridge_list_row( _(""), 'toner_cartridge', null);

table_section_title(_("TSG EVALUATION / DIAGNOSTIC"));
textarea_row(_(" "), 'tsg_evaluation', null, 35, 4);

table_section_title(_("TSG RECOMMENDATION"));
textarea_row(_(" "), 'tsg_recommendation', null, 35, 4);

table_section(2);

table_section_title(_("SUPPORT PROCESS NO."));
text_row(_("SUP No.: "), 'sup_no', null, 30, 30);
text_row(_("Assigned TSG : "), 'assigned_tsg', null, 40, 50);

sup_type_list_row( _("Type: "), 'type', null, false, true);

if($_POST['type'] == 1) // in-house
{
	date_row(_("Received (Date):"), 'received_date');

	echo "<tr>";
		echo "<td class='label'>Received (Time)</td>";
		echo "<td>";
			echo "<select name='received_time_h'>";
			
			for($x=1 ; $x<=12 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='received_time_m'>";
			
			for($x=0 ; $x<=59 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='received_time_t'>";
				echo " <option value=AM>AM</option>
					<option value=PM>PM</option>";
			echo "</select>";
			
		echo "</td>";
	echo "</tr>";

	date_row(_("Required (Date):"), 'required_date');

	echo "<tr>";
		echo "<td class='label'>Required (Time)</td>";
		echo "<td>";
			echo "<select name='required_time_h'>";
			
			for($x=1 ; $x<=12 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='required_time_m'>";
			
			for($x=0 ; $x<=59 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='required_time_t'>";
				echo " <option value=AM>AM</option>
					<option value=PM>PM</option>";
			echo "</select>";
			
		echo "</td>";
	echo "</tr>";
}
else
{
	date_row(_("Started (Date):"), 'started_date');

	echo "<tr>";
		echo "<td class='label'>Started (Time)</td>";
		echo "<td>";
			echo "<select name='started_time_h'>";
			
			for($x=1 ; $x<=12 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='started_time_m'>";
			
			for($x=0 ; $x<=59 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='started_time_t'>";
				echo " <option value=AM>AM</option>
					<option value=PM>PM</option>";
			echo "</select>";
			
		echo "</td>";
	echo "</tr>";

	date_row(_("Completed (Date):"), 'completed_date');

	echo "<tr>";
		echo "<td class='label'>Completed (Time)</td>";
		echo "<td>";
			echo "<select name='completed_time_h'>";
			
			for($x=1 ; $x<=12 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='completed_time_m'>";
			
			for($x=0 ; $x<=59 ; $x++){
				if(strlen($x) < 2){
					$value = '0'.$x;
				}else{
					$value = $x;
				}
				echo "<option value='$value'>$value</option>";
			}
			
			echo "</select>";
			
			echo "<select name='completed_time_t'>";
				echo " <option value=AM>AM</option>
					<option value=PM>PM</option>";
			echo "</select>";
			
		echo "</td>";
	echo "</tr>";
}
sup_warranty_list_row( _("Warranty Mode: "), 'warranty', null);
text_row(_("Conformed: "), 'conformed', null, 40, 50);

table_section_title(_("REPAIR DETAILS"));
textarea_row(_(" "), 'repair_details', null, 35, 4);
sup_status_list_row( _("Status: "), 'status', null);

table_section_title(_("DEGREE OF SERVICE"));
// textarea_row(_(" "), 'degree_of_service', null, 35, 4);
sup_degree_of_service_list_row( _("Level: "), 'degree_of_service', null, false, true);
sup_degree_of_service_one_list_row( _("Description: "), 'degree_of_service_one', null, false, true, $_POST['degree_of_service']);

sup_degree_of_service_two_list_row( _("Description: "), 'degree_of_service_two', null, false, true, $_POST['degree_of_service_one']);

end_outer_table(1);

display_adjustment_items(_("PART(s) USED"), $_SESSION['adj_items']);

//submit_add_or_update_center($selected_id == -1, '', 'both');

echo "<br>";

submit_center_first('Process', _("Add Support Process"), '', 'default');

end_form();

end_page();

?>
