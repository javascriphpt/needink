<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
//-----------------------------------------------------------------------------

$path_to_root = "..";
$page_security = 'SA_EDITREFPO';

include_once($path_to_root . "/sales/includes/cart_class.inc");
include_once($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/sales/includes/sales_ui.inc");
include_once($path_to_root . "/sales/includes/ui/sales_order_ui.inc");
include_once($path_to_root . "/sales/includes/sales_db.inc");
include_once($path_to_root . "/sales/includes/db/sales_types_db.inc");
include_once($path_to_root . "/reporting/includes/reporting.inc");

$js = '';

if ($use_popup_windows) {
	$js .= get_js_open_window(900, 500);
}

if ($use_date_picker) {
	$js .= get_js_date_picker();
}

if (isset($_GET['ModifyOrderNumber']) && is_numeric($_GET['ModifyOrderNumber'])) {

	$ref_no = get_so_ref_no($_GET['ModifyOrderNumber'], ST_SALESORDER);	
	$help_context = 'Modifying Sales Order';
	//$_SESSION['page_title'] = sprintf( _("Modifying Sales Order # %d"), $_GET['ModifyOrderNumber']);
	$_SESSION['page_title'] = "Modifying Sales Order # ".$ref_no["from_stk_loc"]."-".$ref_no["so_form_type_no"];
	create_cart(ST_SALESORDER, $_GET['ModifyOrderNumber']);

}

page($_SESSION['page_title'], false, false, "", $js);
//-----------------------------------------------------------------------------

if (list_updated('branch_id')) {
	// when branch is selected via external editor also customer can change
	$br = get_branch(get_post('branch_id'));
	$_POST['customer_id'] = $br['debtor_no'];
	$Ajax->activate('customer_id');
}

if (isset($_GET['UpdatedID'])) {
	$order_no = $_GET['UpdatedID'];
	$ref_no = get_so_ref_no($order_no, ST_SALESORDER);

	//display_notification_centered(sprintf( _("Order # %d has been updated."),$order_no));
	display_notification_centered(_("Order # ".$ref_no["from_stk_loc"]."-".$ref_no["so_form_type_no"]." has been updated."));

	submenu_view(_("&View This Order"), ST_SALESORDER, $order_no);

	submenu_print(_("&Print This Order"), ST_SALESORDER, $order_no, 'prtopt');
	submenu_print(_("&Email This Order"), ST_SALESORDER, $order_no, null, 1);
	set_focus('prtopt');

	// submenu_option(_("Confirm Order Quantities and Make &Delivery"),
		// "/sales/customer_delivery.php?OrderNumber=$order_no");

	submenu_option(_("Select A Different &Order"),
		"/sales/inquiry/sales_orders_view.php?OutstandingOnly=1");

	display_footer_exit();

} 
else
	check_edit_conflicts();
//-----------------------------------------------------------------------------

function copy_to_cart()
{
	$cart = &$_SESSION['Items'];

	$cart->reference = $_POST['ref'];

	$cart->Comments =  $_POST['Comments'];
	$cart->so_form_type_no = $_POST['so_form_type_no'];

	$cart->document_date = $_POST['OrderDate'];
	if ($cart->trans_type == ST_SALESINVOICE)
		$cart->cash = $_POST['cash']; 
	if ($cart->cash) {
		$cart->due_date = $cart->document_date;
		$cart->phone = $cart->cust_ref = $cart->delivery_address = '';
		$cart->freight_cost = input_num('freight_cost');
		$cart->ship_via = 1;
		$cart->deliver_to = '';//$_POST['deliver_to'];
	} else {
		$cart->due_date = $_POST['delivery_date'];
		$cart->cust_ref = $_POST['cust_ref'];
		$cart->freight_cost = input_num('freight_cost');
		$cart->deliver_to = $_POST['deliver_to'];
		$cart->delivery_address = $_POST['delivery_address'];
		$cart->phone = $_POST['phone'];
		$cart->Location = $_POST['Location'];
		$cart->ship_via = $_POST['ship_via'];
	}
	if (isset($_POST['email']))
		$cart->email =$_POST['email'];
	else
		$cart->email = '';
	$cart->customer_id	= $_POST['customer_id'];
	$cart->Branch = $_POST['branch_id'];
	$cart->sales_type = $_POST['sales_type'];
	// POS
	if ($cart->trans_type!=ST_SALESORDER && $cart->trans_type!=ST_SALESQUOTE) { // 2008-11-12 Joe Hunt
		$cart->dimension_id = $_POST['dimension_id'];
		$cart->dimension2_id = $_POST['dimension2_id'];
	}	
}

//-----------------------------------------------------------------------------

function copy_from_cart()
{
	$cart = &$_SESSION['Items'];
	$_POST['ref'] = $cart->reference;
	$_POST['Comments'] = $cart->Comments;

	$_POST['OrderDate'] = $cart->document_date;
	$_POST['delivery_date'] = $cart->due_date;
	$_POST['cust_ref'] = $cart->cust_ref;
	$_POST['freight_cost'] = price_format($cart->freight_cost);

	$_POST['deliver_to'] = $cart->deliver_to;
	$_POST['delivery_address'] = $cart->delivery_address;
	$_POST['phone'] = $cart->phone;
	$_POST['Location'] = $cart->Location;
	$_POST['ship_via'] = $cart->ship_via;

	$_POST['customer_id'] = $cart->customer_id;

	$_POST['branch_id'] = $cart->Branch;
	$_POST['sales_type'] = $cart->sales_type;
	
	$_POST['so_form_type_no'] = $cart->so_form_type_no;
	
	// POS 
	if ($cart->trans_type == ST_SALESINVOICE)
		$_POST['cash'] = $cart->cash;
	if ($cart->trans_type!=ST_SALESORDER && $cart->trans_type!=ST_SALESQUOTE) { // 2008-11-12 Joe Hunt
		$_POST['dimension_id'] = $cart->dimension_id;
		$_POST['dimension2_id'] = $cart->dimension2_id;
	}	
	$_POST['cart_id'] = $cart->cart_id;
		
}
//--------------------------------------------------------------------------------

function line_start_focus() {
  global 	$Ajax;

  $Ajax->activate('items_table');
  set_focus('_stock_id_edit');
}

//-----------------------------------------------------------------------------

if (isset($_POST['ProcessOrder']) ) {

	$trans_no = key($_SESSION['Items']->trans_no);
	
	db_query("UPDATE ".TB_PREF."sales_orders SET customer_ref = ".db_escape($_POST['cust_ref'])." WHERE order_no = ".db_escape($trans_no)." AND trans_type = ".db_escape(ST_SALESORDER));
	
	meta_forward($_SERVER['PHP_SELF'], "UpdatedID=$trans_no");

}

if (isset($_POST['update'])) {
	$Ajax->activate('items_table');
}

//--------------------------------------------------------------------------------

function check_item_data()
{
	global $SysPrefs;

	if (!check_num('qty', 0) || !check_num('Disc', 0, 100)) {
		display_error( _("The item could not be updated because you are attempting to set the quantity ordered to less than 0, or the discount percent to more than 100."));
		set_focus('qty');
		return false;
	} elseif (!check_num('price', 0)) {
		display_error( _("Price for item must be entered and can not be less than 0"));
		set_focus('price');
		return false;
	} elseif (isset($_POST['LineNo']) && isset($_SESSION['Items']->line_items[$_POST['LineNo']])
	    && !check_num('qty', $_SESSION['Items']->line_items[$_POST['LineNo']]->qty_done)) {

		set_focus('qty');
		display_error(_("You attempting to make the quantity ordered a quantity less than has already been delivered. The quantity delivered cannot be modified retrospectively."));
		return false;
	} // Joe Hunt added 2008-09-22 -------------------------
	elseif ($_SESSION['Items']->trans_type!=ST_SALESORDER && $_SESSION['Items']->trans_type!=ST_SALESQUOTE && !$SysPrefs->allow_negative_stock() &&
		is_inventory_item($_POST['stock_id']))
	{
		$qoh = get_qoh_on_date($_POST['stock_id'], $_POST['Location'], $_POST['OrderDate']);
		if (input_num('qty') > $qoh)
		{
			$stock = get_item($_POST['stock_id']);
			display_error(_("The delivery cannot be processed because there is an insufficient quantity for item:") .
				" " . $stock['stock_id'] . " - " . $stock['description'] . " - " .
				_("Quantity On Hand") . " = " . number_format2($qoh, get_qty_dec($_POST['stock_id'])));
			return false;
		}
		return true;
	}
	return true;
}

//--------------------------------------------------------------------------------

function handle_update_item()
{
	if ($_POST['UpdateItem'] != '' && check_item_data()) {
		$_SESSION['Items']->update_cart_item($_POST['LineNo'],
		 input_num('qty'), input_num('price'),
		 input_num('Disc') / 100, $_POST['item_description'] );
	}
  line_start_focus();
}

//--------------------------------------------------------------------------------

function handle_delete_item($line_no)
{
    if ($_SESSION['Items']->some_already_delivered($line_no) == 0) {
	    $_SESSION['Items']->remove_from_cart($line_no);
    } else {
	display_error(_("This item cannot be deleted because some of it has already been delivered."));
    }
    line_start_focus();
}

//--------------------------------------------------------------------------------

function handle_new_item()
{

	if (!check_item_data()) {
			return;
	}
	add_to_order($_SESSION['Items'], $_POST['stock_id'], input_num('qty'),
		input_num('price'), input_num('Disc') / 100);
	$_POST['_stock_id_edit'] = $_POST['stock_id']	= "";
	line_start_focus();
}

//--------------------------------------------------------------------------------

function  handle_cancel_order()
{
	global $path_to_root, $Ajax;


	if ($_SESSION['Items']->trans_type == ST_CUSTDELIVERY) {
		display_notification(_("Direct delivery entry has been cancelled as requested."), 1);
		submenu_option(_("Enter a New Sales Delivery"),	"/sales/sales_order_entry.php?NewDelivery=1");

	} elseif ($_SESSION['Items']->trans_type == ST_SALESINVOICE) {
		display_notification(_("Direct invoice entry has been cancelled as requested."), 1);
		submenu_option(_("Enter a New Sales Invoice"),	"/sales/sales_order_entry.php?NewInvoice=1");
	} else {
		if ($_SESSION['Items']->trans_no != 0) {
			if ($_SESSION['Items']->trans_type == ST_SALESORDER && 
				sales_order_has_deliveries(key($_SESSION['Items']->trans_no)))
				display_error(_("This order cannot be cancelled because some of it has already been invoiced or dispatched. However, the line item quantities may be modified."));
			else {
				delete_sales_order(key($_SESSION['Items']->trans_no), $_SESSION['Items']->trans_type);
				if ($_SESSION['Items']->trans_type == ST_SALESQUOTE)
				{
					display_notification(_("This sales quotation has been cancelled as requested."), 1);
					submenu_option(_("Enter a New Sales Quotation"), "/sales/sales_order_entry.php?NewQuotation=Yes");
				}
				else
				{
					display_notification(_("This sales order has been cancelled as requested."), 1);
					submenu_option(_("Enter a New Sales Order"), "/sales/sales_order_entry.php?NewOrder=Yes");
				}
			}	
		} else {
			processing_end();
			meta_forward($path_to_root.'/index.php','application=orders');
		}
	}
	$Ajax->activate('_page_body');
	processing_end();
	display_footer_exit();
}

//--------------------------------------------------------------------------------

function create_cart($type, $trans_no)
{ 
	global $Refs;

	processing_start();
	$doc_type = $type;

	if (isset($_GET['NewQuoteToSalesOrder']))
	{
		$trans_no = $_GET['NewQuoteToSalesOrder'];
		$doc = new Cart(ST_SALESQUOTE, $trans_no);
		$doc->trans_no = 0;
		$doc->trans_type = ST_SALESORDER;
		$doc->reference = $Refs->get_next($doc->trans_type);
		$doc->document_date = $doc->due_date = new_doc_date();
		$doc->Comments = _("Sales Quotation") . " # " . $trans_no;
		$_SESSION['Items'] = $doc;
	}	
	elseif($type != ST_SALESORDER && $type != ST_SALESQUOTE && $trans_no != 0) { // this is template
		$doc_type = ST_SALESORDER;

		$doc = new Cart(ST_SALESORDER, array($trans_no));
		$doc->trans_type = $type;
		$doc->trans_no = 0;
		$doc->document_date = new_doc_date();
		if ($type == ST_SALESINVOICE) {
			$doc->due_date = get_invoice_duedate($doc->customer_id, $doc->document_date);
			$doc->pos = user_pos();
			$pos = get_sales_point($doc->pos);
			$doc->cash = $pos['cash_sale'];
			if (!$pos['cash_sale'] || !$pos['credit_sale']) 
				$doc->pos = -1; // mark not editable payment type
			else
				$doc->cash = date_diff2($doc->due_date, Today(), 'd')<2;
		} else
			$doc->due_date = $doc->document_date;
		$doc->reference = $Refs->get_next($doc->trans_type);
		// if ($type == ST_SALESORDER) {
			// $doc->reference = get_so_form_type_no($order->Location, ST_SALESORDER);
		// }
		//$doc->Comments='';
		foreach($doc->line_items as $line_no => $line) {
			$doc->line_items[$line_no]->qty_done = 0;
		}
		$_SESSION['Items'] = $doc;
	} else
		$_SESSION['Items'] = new Cart($type,array($trans_no));
	copy_from_cart();
}

//--------------------------------------------------------------------------------

if (isset($_POST['CancelOrder']))
	handle_cancel_order();

$id = find_submit('Delete');
if ($id!=-1)
	handle_delete_item($id);

if (isset($_POST['UpdateItem']))
	handle_update_item();

if (isset($_POST['AddItem']))
	handle_new_item();

if (isset($_POST['CancelItemChanges'])) {
	line_start_focus();
}

//--------------------------------------------------------------------------------
check_db_has_stock_items(_("There are no inventory items defined in the system."));

check_db_has_customer_branches(_("There are no customers, or there are no customers with branches. Please define customers and customer branches."));

if ($_SESSION['Items']->trans_type == ST_SALESINVOICE) {
	$idate = _("Invoice Date:");
	$orderitems = _("Sales Invoice Items");
	$deliverydetails = _("Enter Delivery Details and Confirm Invoice");
	$cancelorder = _("Cancel Invoice");
	$porder = _("Place Invoice");
} elseif ($_SESSION['Items']->trans_type == ST_CUSTDELIVERY) {
	$idate = _("Delivery Date:");
	$orderitems = _("Delivery Note Items");
	$deliverydetails = _("Enter Delivery Details and Confirm Dispatch");
	$cancelorder = _("Cancel Delivery");
	$porder = _("Place Delivery");
} elseif ($_SESSION['Items']->trans_type == ST_SALESQUOTE) {
	$idate = _("Quotation Date:");
	$orderitems = _("Sales Quotation Items");
	$deliverydetails = _("Enter Delivery Details and Confirm Quotation");
	$cancelorder = _("Cancel Quotation");
	$porder = _("Place Quotation");
	$corder = _("Commit Quotations Changes");
} else {
	$idate = _("Order Date:");
	$orderitems = _("Sales Order Items");
	$deliverydetails = _("Enter Delivery Details and Confirm Order");
	$cancelorder = _("Cancel Order");
	$porder = _("Place Order");
	$corder = _("Commit Order Changes");
}
start_form();

hidden('cart_id');
if (isset($_GET['ModifyOrderNumber']) && is_numeric($_GET['ModifyOrderNumber']))
	$customer_error = display_order_header($_SESSION['Items'], 0, $idate);
else
	$customer_error = display_order_header($_SESSION['Items'], ($_SESSION['Items']->any_already_delivered() == 0), $idate);

if ($customer_error == "") {
	start_table("$table_style width=80%", 10);
	echo "<tr><td>";
	display_order_summary_not_editable($orderitems, $_SESSION['Items'], false);
	echo "</td></tr>";
	echo "<tr><td>";
	display_delivery_details_not_editable($_SESSION['Items']);
	echo "</td></tr>";
	end_table(1);

	if ($_SESSION['Items']->trans_no == 0) {

		submit_center_first('ProcessOrder', $porder,
		    _('Check entered data and save document'), 'default');
		// submit_js_confirm('CancelOrder', _('You are about to void this Document.\nDo you want to continue?'));
	} else {
		submit_center_first('ProcessOrder', $corder,
		    _('Validate changes and update document'), 'default');
	}

	// submit_center_last('CancelOrder', $cancelorder,
	   // _('Cancels document entry or removes sales order when editing an old document'));
} else {
	display_error($customer_error);
}
end_form();
end_page();

?>
