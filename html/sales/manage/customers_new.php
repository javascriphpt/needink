<?
$page_security = 'SA_CUSTOMER';
$path_to_root="../..";
include($path_to_root . "/includes/session.inc");
include_once($path_to_root . "/includes/ui.inc");
page(_("Customer"));
echo "<link type='text/css' href='css/smoothness/jquery-ui-1.7.1.custom.css' rel='stylesheet' />	
		<script type='text/javascript' src='js/jquery-1.3.2.min.js'></script>
		<script type='text/javascript' src='js/jquery-ui-1.7.1.custom.min.js'></script>";
	// if($_GET['x']==1) display_notification_centered("New Supplier has been added.");
	// else if($_GET['x']==2) display_notification_centered("Changes has been saved.");
	// $country="";
	
$new_customer = get_post('customer_id') == ''; 
global $Ajax;
if(list_updated('customer_id')){
	$new_customer = false; 
	// display_error($new_supplier);

	$Ajax->activate('tab-1');
	$Ajax->activate('tab-2');
	$Ajax->activate('tab-3');
}	

// $bank_accnt = get_post('bank_account') == ''; 
// if(list_updated('bank_account')){
	// $Ajax->activate('accnt_no');
	// $Ajax->activate('b_address');
// }

if(isset($_POST['bank_account']))
{
	$query = "SELECT *  FROM ".TB_PREF."bank_accounts WHERE id = ".db_escape($_POST["bank_account"]);
	
	$bank_accnt = db_query($query);
	$data = db_fetch($bank_accnt);
	
	$_POST['accnt_no'] = $data['bank_account_number'];
	$_POST['b_address'] = $data['bank_address'];
	
	$Ajax->activate('accnt_no');
	$Ajax->activate('b_address');
}


// $_POST['accnt_name'] = $data['bank_account_name'];
// $_POST['yr_operation'] = $data

if(isset($_POST['submit'])){

	//initialise no input errors assumed initially before we test
	$input_error = 0;

	/*******************************************************
	Validation of input fields...************************************
	*******************************************************/

	if (strlen($_POST['supp_name']) == 0 || $_POST['supp_name'] == "") 
	{
		$input_error = 1;
		display_error(_("The supplier name must be entered."));
		set_focus('supp_name');
	}
	
	if (strlen($_POST['supp_ref']) == 0 || $_POST['supp_ref'] == "") 
	{
		$input_error = 1;
		display_error(_("The supplier short name must be entered."));
		set_focus('supp_ref');
	}
	
	/*******************************************************
	*******************************************************/
	
	$comp = "SELECT if(count (*)=0,0,1) 
				  FROM ".TB_PREF."suppliers 
				  WHERE supp_account_no = ".db_escape($_POST['supp_account_no'])." 
				  OR supp_name = ".db_escape($_POST['supp_name'])." 
				  OR supp_ref = ".db_escape($_POST['supp_ref']);
	
	// display_error($comp);
	$comp= db_query($comp);
	$not_exist = db_fetch($comp);
	if (!$not_exist[0]) 
	{
	display_error('qweqweqwe');
		$input_error = 1;
		display_error(_("The supplier name must be entered."));
		set_focus('supp_name');
	}
	// die();
	
	
	if ($input_error !=1 ){
		if($new_supplier){
		$sql = "INSERT INTO ".TB_PREF."suppliers (
						supp_account_no,
						supp_name,
						supp_ref,
						office_address,
						warehouse_address,
						plant_address,
						website,
						dti_registration_no,
						tin_no,
						btype_id,
						bclass_id,
						no_of_operation_yr,
						capitalization,
						no_of_business_yr,
						total_sales,
						no_of_employees,
						bank_account,
						credit_limit,
						curr_code,
						tax_group_id,
						payment_terms,
						payable_account,
						purchase_account,
						payment_discount_account,
						inactive
					) VALUES (";
					
			$sql.= db_escape($_POST['supp_account_no']).",";
			$sql.= db_escape($_POST['supp_name']).",";
			$sql.= db_escape($_POST['supp_ref']).",";
			$sql.= db_escape($_POST['o_address']).",";
			$sql.= db_escape($_POST['w_address']).",";
			$sql.= db_escape($_POST['p_address']).",";
			$sql.= db_escape($_POST['website']).",";
			$sql.= db_escape($_POST['dti_no']).",";
			$sql.= db_escape($_POST['tin_no']).",";
			$sql.= db_escape($_POST['btype_id']).",";
			$sql.= db_escape($_POST['bclass_id']).",";
			$sql.= input_num($_POST['yr_operation']).",";
			$sql.= input_num($_POST['capitalization']).",";
			$sql.= input_num($_POST['yr_business']).",";
			$sql.= input_num($_POST['total_sales']).",";
			$sql.= input_num($_POST['employee_no']).",";
			$sql.= db_escape($_POST['bank_account']).",";
			$sql.= input_num($_POST['credit_limit']).",";
			$sql.= db_escape($_POST['curr_code']). ", ";
			$sql.= db_escape($_POST['tax_group_id']). ", ";
			$sql.= db_escape($_POST['payment_terms']). ", ";
			$sql.= db_escape($_POST['payable_account']). ", ";
			$sql.= db_escape($_POST['purchase_account']). ", ";
			$sql.= db_escape($_POST['payment_discount_account']).", ";
			$sql.= db_escape($_POST['inactive']);
			$sql.=")";
			display_error($sql);
			db_query($sql);
		}
	
	}
	
	// display_error($sql);
	// die();
}

start_form();

// if(isset($_GET['supp_id']) || $_GET['supp_id']!='' || $_POST['supplier_id']!=$_GET['supp_id']){
 // $_GET['supp_id'] = $_POST['supplier_id'];
// // exit();
// }
echo "<center>";
// supplier_list_cells($label, $name, $selected_id=null, $all_option=false, 
	// $submit_on_change=false, $all=false, $editkey = false,$async=false)
	customer_list_cells(_("Select a customer: "), 'customer_id', null,
		_('New customer'), true, check_value('show_inactive'));
	check_cells(_("Show inactive:"), 'show_inactive', null, true);
echo "</center>";
	if (get_post('_show_inactive_update')) {
		$Ajax->activate('customer_id');
		set_focus('customer_id');
	}
if (isset($_GET['customer_id'])) 
{
	$_POST['customer_id'] = $_GET['customer_id'];
}
if($new_supplier){

	//Default value for fields...
	empty_fields();
	
}else{
	$sql = "SELECT * 
			  FROM ".TB_PREF."suppliers ";
	$sql.= " WHERE supplier_id = ".db_escape($_POST['supplier_id']);
	$sql = db_query($sql);
	$data = db_fetch($sql);
	
	$_POST['supp_account_no'] = $data['supp_account_no'];
	$_POST['supp_name'] = $data['supp_name'];
	$_POST['supp_ref'] = $data['supp_ref'];
	$_POST['o_address'] = $data['office_address'];
	$_POST['w_address'] = $data['warehouse_address'];
	$_POST['p_address'] = $data['plant_address'];
	$_POST['website'] = $data['website'];
	$_POST['dti_no'] = $data['dti_registration_no'];
	$_POST['tin_no'] = $data['tin_no'];
	$_POST['btype_id'] = $data['btype_id'];
	$_POST['bclass_id'] = $data['bclass_id'];
	$_POST['yr_operation'] = $data['no_of_operation_yr'];
	$_POST['capitalization'] = price_format($data['capitalization']);
	$_POST['yr_business'] = $data['no_of_business_yr'];
	$_POST['total_sales'] = price_format($data['total_sales']);
	$_POST['employee_no'] = $data['no_of_employees'];
		
	
	// if($_POST['supplier_id']!=$_GET['supp_id']){
	
		// $Ajax->activate('_page_body');
		// if($data['supplier_id'])
		// meta_forward($_SERVER['PHP_SELF'], 'supp_id='.$data['supplier_id']);
		// $_POST['supplier_id'] = 2;
		// $_POST['supplier_id']!=$_GET['supp_id'];
	// }
	// exit;
	
}
// end_form();
	
// echo "<br><form method=POST><div id='tabs' style='width:90%; margin:auto'>
// echo "<table width='90%'>";
div_start('tabs');
// echo "<div id='tabs' style='width:90%; margin:auto'>";
// div_end();
echo "<br>
        <ul>
            <li><a href='#tab-1'>General Information</a></li>
            <li><a href='#tab-2' onclick=addcontact($new_supplier)>Contact Person</a></li>
			<li><a href='#tab-3' onclick=addbranch($_GET[deb_id])>Credit Reference</a></li>
	   </ul>";
// echo "</table>";
	//		
/****************General Tab********************/		


div_start('tab-1');
echo "
<table>
	<tr>
		<td rowspan=2 valign=top>";

echo "
				<fieldset>
					<legend>General Information</legend>
					<table align=center width=100% bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=0>";
		
						// text_row(_("Our Customer No:"), 'supp_account_no', null, 20, 30);
						// text_row(_("Supplier Name:"), 'supp_name', null, 42, 40);
						// text_row(_("Supplier Short Name:"), 'supp_ref', null, 30, 30);
						// textarea_row(_("Office Address:"), 'o_address', null, 45, 5);
						// textarea_row(_("Warehouse(s) Address:"), 'w_address', null, 45, 5);
						// textarea_row(_("Plant(s) Address:"), 'p_address', null, 45, 5);
						// link_row(_("Website:"), 'website', null, 40, 55);
						// text_row(_("SEC / DTI Registration No.:"), 'dti_no', null, 35, 40);
						// text_row(_("TIN No:"), 'tin_no', null, 35, 40);
						// record_status_list_row(_("Supplier status:"), 'inactive');
						
						textarea_row(_("Company Address:"), 'c_address', null, 45, 5);
						textarea_row(_("Office Address:"), 'o_address', null, 45, 5);
						text_row(_("SEC / DTI Registration No.:"), 'dti_no', null, 35, 40);
						text_row(_("TIN No:"), 'tin_no', null, 35, 40);
					echo "</table>";
					echo "</fieldset>";

echo "</td>";
echo "<td valign=top width=70%>
				<fieldset>
					<legend>Other Information</legend>
					<table width=100% align=center bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=30>";

						business_types_list_row(_("Business Type:"), 'btype_id', $_POST['btype_id']);
						echo "<td>";
						hyperlink_params_td($path_to_root . "/sales/manage/business_type.php",
							'<b>'. (@$_REQUEST['popup'] ?  _("Select or &Add") : _("&Add or Edit ")).'</b>', 
							"".(@$_REQUEST['popup'] ? '&popup=1':''));
						end_row();

						business_class_types_list_row(_("Classification:"), 'bclass_id', $_POST['bclass_id']);
						echo "<td>";
						hyperlink_params_td($path_to_root . "/sales/manage/business_classification.php",
							'<b>'. (@$_REQUEST['popup'] ?  _("Select or &Add") : _("&Add or Edit ")).'</b>', 
							"".(@$_REQUEST['popup'] ? '&popup=1':''));
						end_row();
					
						amount_row(_("No. of Years in Operation"),'yr_operation', null,null,null,0);
						// amount_row(_("Capitalization:"), 'capitalization', null, 42, 40);
						// amount_row(_("Capitalization:"), 'capitalization', null);
						// text_row(_("No. of Years in Business:"), 'b_operation', null, 42, 40);
						// amount_row(_("No. of Years in Business:"), 'yr_business', null,null,null,0);
						// text_row(_("Total Sales (Previous Fiscal Year):"), 'total_sales', null, 42, 40);
						// amount_row(_("Total Sales (Previous Fiscal Year):"), 'total_sales', null);
						amount_row(_("No. of Employees:"), 'employee_no', null,null,null,0);
		
					echo "</table>";
				echo "</fieldset>";

echo "</tr>";
echo "<tr>
	<td valign=top width=50%>
		<fieldset>
			<legend>Purchasing</legend>
			<table width=100% align=center bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=30>";

				text_row(_("Bank Name/Account:"), 'bank_account', null, 42, 40);
				amount_row(_("Credit Limit:"), 'credit_limit', null);
				if (!$new_supplier) 
				{
					label_row(_("Supplier's Currency:"), $_POST['curr_code']);
					hidden('curr_code', $_POST['curr_code']);
				} 
				else 
				{
					currencies_list_row(_("Supplier's Currency:"), 'curr_code', null);
				}

				tax_groups_list_row(_("Tax Group:"), 'tax_group_id', null);

				payment_terms_list_row(_("Payment Terms:"), 'payment_terms', null);

			echo "</table>";
		echo "</fieldset>";
echo "<tr>
	<td valign=top width=50%>
		<fieldset>
			<legend>Check Payments</legend>";
echo "			<table width=100% bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' >";

				// gl_all_accounts_list_row(_("Accounts Payable Account:"), 'payable_account', $_POST['payable_account']);

				// gl_all_accounts_list_row(_("Purchase Account:"), 'purchase_account', $_POST['purchase_account']);

				// gl_all_accounts_list_row(_("Purchase Discount Account:"), 'payment_discount_account', $_POST['payment_discount_account']);
				bank_accounts_list_cells(_("Account:"), 'bank_account', null,true);
				
				text_row(_("Account No."), 'accnt_no', null, 35, 40);
				// text_row(_("Account Name"), 'accnt_name', null, 35, 40);
				// text_row(_("Bank Name"), 'bank_name', null, 35, 40);
				textarea_row(_("Bank Address:"), 'b_address', null, 45, 5);
				
				$dim = get_company_pref('use_dimension');
				if ($dim >= 1)
				{
					table_section_title(_("Dimension"));

					dimensions_list_row(_("Dimension")." 1:", 'dimension_id', null, true, " ", false, 1);
					if ($dim > 1)
						dimensions_list_row(_("Dimension")." 2:", 'dimension2_id', null, true, " ", false, 2);
				}
				if ($dim < 1)
					hidden('dimension_id', 0);
				if ($dim < 2)
					hidden('dimension2_id', 0);

			echo "</table>";
		echo "</fieldset>";
echo "</table>";



div_end();
		// if($_GET['action']=='edit'){
			// echo "<input type=hidden name=deb_id value=$_GET[deb_id]>";
		// }
/****************Contact Person********************/		
// div_start('tab-2');
// echo  "<br>
// <div id=contact></div><br><br>
		// <center>	
		// <fieldset align=center style='width:40%'><legend>Add Contact</legend><table width=100% align=center bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=30>
// ";
	// text_row(_("Contact Name:"), 'cname', null, 40, 40);
	// text_row(_("Phone No.(s):"), 'cphone_no', null, 20, 20);
	// text_row(_(""), 'cphone2_no', null, 20, 20);
	// text_row(_("Fax No.(s):"), 'cfax_no', null, 20, 20);
	// text_row(_(""), 'cfax2_no', null, 20, 20);
	// email_row(_("Email Address:"), 'email', null, 35, 55);
	// // foreach($_POST as $key=>$value)
		// // display_error($key."=>".$value);
// echo "</table>";
// echo "<br>
	// <div align=center>";

// echo "			<input type=button value=Add class=x onclick=addcontact('','insert')>";
// echo "	</div>
		// </fieldset>
		// </center>
		// ";
// div_end();
		
		
/****************Credit Reference Tab********************/			
div_start('tab-3');
// echo  "<br>
		
				// <div id=branch></div>
		
		// <br><center>
			// <fieldset style='width:35%;' align=center>
				// <legend>Add Credit Reference</legend>
				// <table width=100% align=center bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' align='center' style='border-collapse: collapse;' cellspacing=30>";
					
					// text_row(_("Name:"), 'ref_name', null, 40, 40);
					// text_row(_("Contact Person:"), 'ref_person', null, 40, 40);
					// text_row(_("Telephone No."), 'ref_person_tel', null, 40, 40);
					// text_row(_("Account No."), 'ref_accnt_no', null, 40, 40);
					
// echo "					
				// </table>
				// <br>
				// <input type=button value=Add class=x id='_add_' onclick=addbranch('','insert')>
			// </fieldset>
		// </center>
		// ";
		
		
div_end();
if (!$new_customer) 
{
	submit_center_first('submit', _("Update Supplier"), 
	  _('Update supplier data'), @$_REQUEST['popup'] ? true : 'default');
	submit_return('select', get_post('supplier_id'), _("Select this supplier and return to document entry."));
	submit_center_last('delete', _("Delete Supplier"), 
	  _('Delete supplier data if have been never used'), true);
}
else 
{
	submit_center('submit', _("Add New Customer Details"), true, '', 'default');
}
/****************Accounting History********************/		
// echo  "<div id='tab-5' align=left style='padding-left:10%'>";
	// <br><br>
					// <table cellspacing=4 cellpadding='5' bordercolor='#cccccc' border='0' bgcolor='#f9f9f9' style='border-collapse: collapse;'>
						// <tr>
							// <td><div id=tin>TIN Number:
							// <td><div id=tin2><input type=text name=tin size=21 style='width:98%' value=".$tin.">
						// </tr>";
						// //	percent_row(_("Discount Percent:"), 'discount', $discount);
						// //	amount_row("Credit Limit:",'credit_limit',$credit_limit);
						// echo"<tr>
							// <td>Currency:
							// <td>".currency($currency)."
						// </tr><tr>
							// <td>Payment Terms:
							// <td>".terms($terms)."
						// </tr>
					// <tr>	
						// <td><div id=tax_witheld>Tax Withheld: <td><div id=tax_witheld2><input type=checkbox name=witheld";if($witheld=='on')echo" checked";echo"></div>
					// </table>";
			
					// ECHO "</div>";
			 // echo "</div>";		
	 // echo "<br><br><center><input type=button class=x value=Save onclick=checkContent('$_GET[action]');>&nbsp;&nbsp;<input type=button class=x value=Cancel onclick=document.location='supplier_list.php'></form>";

// $new_supplier = $_POST['supplier_id'];
	 

	 
end_page();
?>
<style>
	.x{
	font-family:verdana,arial,sans-serif;
	width:100px;
	border: 1px solid rgb(0, 102, 204); 
	padding: 1px 4px; 
	background-color: rgb(226, 226, 226); 
	font-size: 12px;
	}
	select
	{
		width:50%;
	}
	legend{
		font-size:14px;
		font-family:calibri;
		color:orange;
		font-weight:bold;
		}
</style>

<script>

var xmlHttp = createXmlHttpRequestObject();
function checkType(){
	var type=document.forms[0].type;
//	alert(type.value);
	if(type.value=='foreign'){
		document.getElementById("tin").style.visibility="hidden";
		document.getElementById("tin2").style.visibility="hidden";
		document.getElementById("tax_witheld").style.visibility="hidden";
		document.getElementById("tax_witheld2").style.visibility="hidden";
	}
	else{
		document.getElementById("tin").style.visibility="visible";
		document.getElementById("tin2").style.visibility="visible";
		document.getElementById("tax_witheld").style.visibility="visible";
		document.getElementById("tax_witheld2").style.visibility="visible";
	}
}
function createXmlHttpRequestObject()
{
		var xmlHttp;
		if(window.ActiveXObject)
		{
		try
		{
		xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch (e)
		{
		xmlHttp = false;
		}
		}
		else
		{
		try
		{
		xmlHttp = new XMLHttpRequest();
		}
		catch (e)
		{
		xmlHttp = false;
		}
		}
		if (!xmlHttp)
		alert("Error creating the XMLHttpRequest object.");
		else
		return xmlHttp;
}
function handleServerResponse5()
{
		if (xmlHttp.readyState == 4)
		{
				helloMessage = xmlHttp.responseText;
			
				document.getElementById("contact").innerHTML = helloMessage;
		}
}
function newCust(){
	var left = (screen.width/2)-(450);
	var top = (screen.height/2)-(200);
			window.open("../../sales/manage/Add_supp_customer.php","_blank","toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes,resizable=no,center=yes, copyhistory=yes, width=1000, height=600,top="+top+", left="+left);

	}

function addcontact(id,action,line_no)
{
	if (xmlHttp.readyState == 4 || xmlHttp.readyState == 0){
			var supp_id=document.forms[0].supplier_id.value;
			// alert('!@#!@# '+supp_id);
			var cname=document.forms[0].cname.value;
			var cphone_no=document.forms[0].cphone_no.value;
			var cphone2_no=document.forms[0].cphone2_no.value;
			var cfax_no=document.forms[0].cfax_no.value;
			var cfax2_no=document.forms[0].cfax2_no.value;
			var email=document.forms[0].email.value;
		if(action=='insert'){
			if(supp_id!='')
			var url1 = 'contact.php?id='+supp_id+
											'&cname='+cname+
											'&cphone='+cphone_no+
											'&cphone2='+cphone2_no+
											'&cfax='+cfax_no+
											'&cfax2='+cfax2_no+
											'email='+email;
			else
			var url1 = 'contact.php?action='+action+
											'&cname='+cname+
											'&cphone='+cphone_no+
											'&cphone2='+cphone2_no+
											'&cfax='+cfax_no+
											'&cfax2='+cfax2_no+
											'&email='+email;
			document.forms[0].supplier_id.value="";
			document.forms[0].cname.value="";
			document.forms[0].cphone_no.value="";
			document.forms[0].cphone2_no.value="";
			document.forms[0].cfax_no.value="";
			document.forms[0].cfax2_no.value="";
			document.forms[0].email.value="";
		}
		else if(action=='delete')
		
			var url1='contact.php?id='+supp_id+"&action="+action+"&line_no="+line_no;
		else	var url1='contact.php?id='+supp_id;
			// alert(url1);
		xmlHttp.open("GET", url1, true);
		xmlHttp.onreadystatechange = handleServerResponse5;
		xmlHttp.send(null);

	} 
}

function checkContent(action)
	{
//		alert(action);
		if(action!='edit')
		{
			var companyname=document.forms[0].supp_name.value;
			//alert(companyname);
			if(companyname=="") alert("Supplier name is required");
			else{
			document.forms[0].action='supplierAdd.php';
			document.forms[0].submit();
			}
		}
		else{
			var companyname=document.forms[0].supp_name.value;
			//alert(companyname);
			if(companyname=="") alert("Supplier name is required");
			else{
			document.forms[0].action='supplierEdit.php';
			document.forms[0].submit();
			}
		}
	}
	$(function(){
	// Tabs
				$('#tabs').tabs();
				
				//hover states on the static widgets
				$('#dialog_link, ul#icons li').hover(
					function() { $(this).addClass('ui-state-hover'); }, 
					function() { $(this).removeClass('ui-state-hover'); }
				);
				
			});
function handleServerResponse4()
{
		if (xmlHttp.readyState == 4)
		{
				helloMessage = xmlHttp.responseText;
			
				document.getElementById("branch").innerHTML = helloMessage;
		}
}			
function addbranch(id,action,id2)
{
		if (xmlHttp.readyState == 4 || xmlHttp.readyState == 0){
			if(action=='insert'){
				
				var supplier = document.forms[0].supplier_id.value;
				var ref_name = document.forms[0].ref_name.value;
				var ref_person = document.forms[0].ref_person.value;
				var ref_person_tel = document.forms[0].ref_person_tel.value;
				var ref_accnt_no = document.forms[0].ref_accnt_no.value;
				
				// alert(ref_name);
				// var url1='credit_reference.php?id='+id+"&action="+action+"&name="+name+"&address="+address+"&email="+email+"&tel_no="+tel_no+"&fax_no="+fax_no+"&province="+province+"&city="+city;
				if(supplier!='')
				var url1='credit_reference.php?supplier_id='+supplier+
														'&action='+action+
														'&ref_name='+ref_name+
														'&ref_person='+ref_person+
														'&ref_person_tel='+ref_person_tel+
														'&ref_accnt_no='+ref_accnt_no;
				// else
				// var url1='credit_reference.php?action='+action+
														// '&ref_name='+ref_name+
														// '&ref_person='+ref_person+
														// '&ref_person_tel='+ref_person_tel+
														// '&ref_accnt_no='+ref_accnt_no;
				alert(url1);
			}
			else if(action=='delete')
				var url1='credit_reference.php?id='+id+"&action="+action+"&id2="+id2;
			else	var url1='credit_reference.php?id='+id;
			xmlHttp.open("GET", url1, true);
		xmlHttp.onreadystatechange = handleServerResponse4;
			xmlHttp.send(null);
	
		} 
}

// function addbranch(id,action,id2)
// {
	// if (xmlHttp.readyState == 4 || xmlHttp.readyState == 0){
			// var ref_name = document.forms[0].ref_name.value;
			// var ref_person = document.forms[0].ref_person.value;
			// var ref_person_tel = document.forms[0].ref_person_tel.value;
			// var ref_accnt_no = document.forms[0].ref_accnt_no.value;
		// if(action=='insert'){
			// if(supp_id!='')
			 // var url1='credit_reference.php?id='+id+"&action="+action+"&name="+name+"&address="+address+"&email="+email+"&tel_no="+tel_no+"&fax_no="+fax_no+"&province="+province+"&city="+city;
				// // //alert(url1);
			// else
			// var url1='credit_reference.php?id='+id+"&action="+action+"&id2="+id2;
			// document.forms[0].supplier_id.value="";
			// document.forms[0].cname.value="";
			// document.forms[0].cphone_no.value="";
			// document.forms[0].cphone2_no.value="";
			// document.forms[0].cfax_no.value="";
			// document.forms[0].cfax2_no.value="";
			// document.forms[0].email.value="";
		// }
		// else if(action=='delete')
		
			// var url1='credit_reference.php?id='+supp_id+"&action="+action+"&line_no="+line_no;
		// else	var url1='contact.php?id='+supp_id;
			// // alert(url1);
		// xmlHttp.open("GET", url1, true);
		// xmlHttp.onreadystatechange = handleServerResponse5;
		// xmlHttp.send(null);

	// } 
// }

</script>

