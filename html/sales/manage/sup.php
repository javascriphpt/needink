<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_CUSTOMER';
//$page_security = 3;
$path_to_root="../..";
include($path_to_root . "/includes/db_pager.inc");
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Support Process"), @$_REQUEST['popup']);

include($path_to_root . "/includes/ui.inc");
simple_page_mode(true);

//-----------------------------------------------------------------------------------------------

if ($Mode=='ADD_ITEM')
{	
	//initialise no input errors assumed initially before we test
	$input_error = 0;

	//first off validate inputs sensible

	if (strlen($_POST['sup_no']) != 0)
	{
		$sql = "SELECT sup_no FROM ".TB_PREF."sup WHERE sup_no = '".$_POST['sup_no']."'";
		$result = db_query($sql, "sup_no"); 
				
		if (db_num_rows($result) != 0)
		{
			$input_error = 1;
			display_error(_("The entered SUP No is already in use."));
			set_focus('sup_no');
		}
	}
	
	if (strlen($_POST['company_name']) == 0)
	{
		$input_error = 1;
		display_error(_("Company name cannot be empty."));
		set_focus('company_name');
	}

	if (strlen($_POST['system_unit']) == 0)
	{
		$input_error = 1;
		display_error(_("System / Unit cannot be empty."));
		set_focus('system_unit');
	}
	
	if (strlen($_POST['serial_no']) == 0)
	{
		$input_error = 1;
		display_error(_("Serial No cannot be empty."));
		set_focus('serial_no');
	}
	
	if (strlen($_POST['sup_no']) == 0)
	{
		$input_error = 1;
		display_error(_("SUP No cannot be empty."));
		set_focus('sup_no');
	}

	if ($input_error != 1)
	{
		$received_time = $_POST['received_time_h'].":".$_POST['received_time_m']." ".$_POST['received_time_t'];
		$required_time = $_POST['required_time_h'].":".$_POST['required_time_m']." ".$_POST['required_time_t'];
		$started_time = $_POST['started_time_h'].":".$_POST['started_time_m']." ".$_POST['started_time_t'];
		$completed_time = $_POST['completed_time_h'].":".$_POST['completed_time_m']." ".$_POST['completed_time_t'];
		
    	if ($selected_id == -1)
		{
			/*Selected branch is null cos no item selected on first time round so must be adding a	record must be submitting new entries in the new Customer Branches form */
			$sql = "INSERT INTO ".TB_PREF."sup (debtor_no, branch_code, company_name, address, contact_person,
				tel_no, system_unit, location, serial_no, accessories, 
				customer_comment, toner_cartridge, tsg_evaluation, tsg_recommendation, sup_no,
				received_date, received_time, required_date, required_time, type, 
				assigned_tsg, started_date, started_time, warranty, completed_date,
				completed_time, conformed, repair_details)
				VALUES (".db_escape($_POST['debtor_no']). ",".db_escape($_POST['branch_code']) . ", "
					.db_escape($_POST['company_name']) . ", " .db_escape($_POST['address']) . ", "
					.db_escape($_POST['contact_person']) . ", " .db_escape($_POST['tel_no']) . ", "
					.db_escape($_POST['system_unit']) . ", " .db_escape($_POST['location']) . ", "
					.db_escape($_POST['serial_no']) . ", " .db_escape($_POST['accessories']) . ", "
					.db_escape($_POST['customer_comment']) . ", " .db_escape($_POST['toner_cartridge']) . ", "
					.db_escape($_POST['tsg_evaluation']) . ", " .db_escape($_POST['tsg_recommendation']) . ", "
					.db_escape($_POST['sup_no']) . ", " .db_escape(date2sql($_POST['received_date'])) . ", "
					.db_escape($received_time) . ", " .db_escape(date2sql($_POST['required_date'])) . ", "
					.db_escape($required_time) . ", " .db_escape($_POST['type']) . ", "
					.db_escape($_POST['assigned_tsg']) . ", " .db_escape(date2sql($_POST['started_date'])) . ", "
					.db_escape($started_time) . ", " .db_escape($_POST['warranty']) . ", "
					.db_escape(date2sql($_POST['completed_date'])) . ", " .db_escape($completed_time) . ", "
					.db_escape($_POST['conformed']) . ", " .db_escape($_POST['repair_details']) . ")";

			$note = _('Support Process has been added');
		}
		//run the sql from either of the above possibilites
		db_query($sql,"Support process could not be inserted.");
		display_notification($note);
		$Mode = 'RESET';
		if (@$_REQUEST['popup']) {
			set_focus("Select".($_POST['branch_code'] == -1 
				? db_insert_id(): $_POST['branch_code']));
		}
	}

}

if ($Mode == 'RESET')
{
	$selected_id = -1;
	$cust_id = $_POST['customer_id'];
	unset($_POST);
	$_POST['customer_id'] = $cust_id;
	$Ajax->activate('_page_body');
}

//-----------------------------------------------------------------------------------------------

check_db_has_customers(_("There are no customers defined in the system. Please define a customer to add customer branches."));

simple_page_mode(true);
//-----------------------------------------------------------------------------------------------

start_form();

echo "<center>" . _("Select a customer: ") . "&nbsp;&nbsp;";
echo customer_list('customer_id', null, false, true);
customer_branches_list_row(_("Branch:"),  $_POST['customer_id'], 'branch_id', null, false, true, true, true);
echo "</center><br>";

$num_branches = db_customer_has_branches($_POST['customer_id']);

$sql = "SELECT c.name, c.address, b.*
		FROM ".TB_PREF."cust_branch b, "
			.TB_PREF."debtors_master c
		WHERE b.debtor_no=c.debtor_no
		AND b.debtor_no = ".db_escape($_POST['customer_id'])."
		AND b.branch_code = ".db_escape($_POST['branch_id']);
$result = db_query($sql,"check failed");
$myrow = db_fetch($result);

$_POST['debtor_name'] = $myrow["name"];
$_POST['address'] = $myrow["address"];
$_POST['tel_no'] = $myrow["phone"];
$_POST['contact_name'] = $myrow["contact_name"];

if(isset($_POST['customer_id']) || isset($_POST['branch_id']))
	$Ajax->activate('_page_body');

start_outer_table($table_style2, 5);

table_section(1);

hidden('debtor_no', $myrow["debtor_no"]);
hidden('branch_code', $myrow["branch_code"]);
hidden('selected_id', $selected_id);
hidden('popup', @$_REQUEST['popup']);

table_section_title(_("ACCOUNT NAME & DESCRIPTION"));
text_row(_("Company Name:"), 'company_name', $_POST['debtor_name'], 40, 50);
textarea_row(_("Address:"), 'address', $_POST['address'], 35, 4);
text_row(_("Contact Person:"), 'contact_person', $_POST['contact_name'], 40, 50);
text_row(_("Tel No.:"), 'tel_no', $_POST['tel_no'], 40, 50);
text_row(_("System / Unit:"), 'system_unit', null, 40, 50);
text_row(_("Location:"), 'location', null, 40, 50);
text_row(_("Serial No.:"), 'serial_no', null, 40, 50);
text_row(_("Accessories:"), 'accessories', null, 40, 50);

table_section_title(_("CUSTOMER COMMENT / COMPLAINT/ INSTRUCTION"));
textarea_row(_(" "), 'customer_comment', null, 35, 4);

table_section_title(_("TONER CARTRIDGE USED"));
toner_cartridge_list_row( _(""), 'toner_cartridge', null);

table_section_title(_("TSG EVALUATION / DIAGNOSTIC"));
textarea_row(_(" "), 'tsg_evaluation', null, 35, 4);

table_section_title(_("TSG RECOMMENDATION"));
textarea_row(_(" "), 'tsg_recommendation', null, 35, 4);

table_section(2);

table_section_title(_("SUPPORT PROCESS NO."));
text_row(_("SUP - "), 'sup_no', null, 30, 30);
sup_type_list_row( _("Type : "), 'type', null);
date_row(_("Received (Date):"), 'received_date');

echo "<tr>";
	echo "<td class='label'>Received (Time)</td>";
	echo "<td>";
		echo "<select name='received_time_h'>";
		
		for($x=1 ; $x<=12 ; $x++){
			if(strlen($x) < 2){
				$value = '0'.$x;
			}else{
				$value = $x;
			}
			echo "<option value='$value'>$value</option>";
		}
		
		echo "</select>";
		
		echo "<select name='received_time_m'>";
		
		for($x=0 ; $x<=59 ; $x++){
			if(strlen($x) < 2){
				$value = '0'.$x;
			}else{
				$value = $x;
			}
			echo "<option value='$value'>$value</option>";
		}
		
		echo "</select>";
		
		echo "<select name='received_time_t'>";
			echo " <option value=AM>AM</option>
				<option value=PM>PM</option>";
		echo "</select>";
		
	echo "</td>";
echo "</tr>";

date_row(_("Required (Date):"), 'required_date');

echo "<tr>";
	echo "<td class='label'>Required (Time)</td>";
	echo "<td>";
		echo "<select name='required_time_h'>";
		
		for($x=1 ; $x<=12 ; $x++){
			if(strlen($x) < 2){
				$value = '0'.$x;
			}else{
				$value = $x;
			}
			echo "<option value='$value'>$value</option>";
		}
		
		echo "</select>";
		
		echo "<select name='required_time_m'>";
		
		for($x=0 ; $x<=59 ; $x++){
			if(strlen($x) < 2){
				$value = '0'.$x;
			}else{
				$value = $x;
			}
			echo "<option value='$value'>$value</option>";
		}
		
		echo "</select>";
		
		echo "<select name='required_time_t'>";
			echo " <option value=AM>AM</option>
				<option value=PM>PM</option>";
		echo "</select>";
		
	echo "</td>";
echo "</tr>";

sup_warranty_list_row( _("Warranty Type : "), 'warranty', null);
text_row(_("Assigned TSG : "), 'assigned_tsg', null, 40, 50);

date_row(_("Started (Date):"), 'started_date');

echo "<tr>";
	echo "<td class='label'>Started (Time)</td>";
	echo "<td>";
		echo "<select name='started_time_h'>";
		
		for($x=1 ; $x<=12 ; $x++){
			if(strlen($x) < 2){
				$value = '0'.$x;
			}else{
				$value = $x;
			}
			echo "<option value='$value'>$value</option>";
		}
		
		echo "</select>";
		
		echo "<select name='started_time_m'>";
		
		for($x=0 ; $x<=59 ; $x++){
			if(strlen($x) < 2){
				$value = '0'.$x;
			}else{
				$value = $x;
			}
			echo "<option value='$value'>$value</option>";
		}
		
		echo "</select>";
		
		echo "<select name='started_time_t'>";
			echo " <option value=AM>AM</option>
				<option value=PM>PM</option>";
		echo "</select>";
		
	echo "</td>";
echo "</tr>";

date_row(_("Completed (Date):"), 'completed_date');

echo "<tr>";
	echo "<td class='label'>Started (Time)</td>";
	echo "<td>";
		echo "<select name='completed_time_h'>";
		
		for($x=1 ; $x<=12 ; $x++){
			if(strlen($x) < 2){
				$value = '0'.$x;
			}else{
				$value = $x;
			}
			echo "<option value='$value'>$value</option>";
		}
		
		echo "</select>";
		
		echo "<select name='completed_time_m'>";
		
		for($x=0 ; $x<=59 ; $x++){
			if(strlen($x) < 2){
				$value = '0'.$x;
			}else{
				$value = $x;
			}
			echo "<option value='$value'>$value</option>";
		}
		
		echo "</select>";
		
		echo "<select name='completed_time_t'>";
			echo " <option value=AM>AM</option>
				<option value=PM>PM</option>";
		echo "</select>";
		
	echo "</td>";
echo "</tr>";

text_row(_("Conformed : "), 'conformed', null, 40, 50);

table_section_title(_("REPAIR DETAILS"));
textarea_row(_(" "), 'repair_details', null, 35, 4);

end_outer_table(1);

submit_add_or_update_center($selected_id == -1, '', 'both');

end_form();

end_page();

?>
