<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SUPINQUIRY';
$path_to_root = "../..";
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/sales/includes/sales_ui.inc");

include_once($path_to_root . "/sales/includes/sales_db.inc");

$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(900, 600);
page(_($help_context = "View Support Process"), true, false, "", $js);


if (isset($_GET["trans_no"]))
{
	$trans_id = $_GET["trans_no"];
}
elseif (isset($_POST["trans_no"]))
{
	$trans_id = $_POST["trans_no"];
}

function get_toner_cartridge($id)
{
	$sql = "SELECT description FROM ".TB_PREF."toner_cartridge WHERE id=".db_escape($id);

	$result = db_query($sql, "could not get toner cartridge");

	$row = db_fetch_row($result);

	return $row[0];
}

function get_sup_type($id)
{
	$sql = "SELECT description FROM ".TB_PREF."sup_type WHERE id=".db_escape($id);

	$result = db_query($sql, "could not get sup type");

	$row = db_fetch_row($result);

	return $row[0];
}

function get_sup_warranty($id)
{
	$sql = "SELECT description FROM ".TB_PREF."sup_warranty WHERE id=".db_escape($id);

	$result = db_query($sql, "could not get sup warranty");

	$row = db_fetch_row($result);

	return $row[0];
}

function get_sup_status($id)
{
	$sql = "SELECT description FROM ".TB_PREF."sup_status WHERE id=".db_escape($id);

	$result = db_query($sql, "could not get sup status");

	$row = db_fetch_row($result);

	return $row[0];
}
//------------------------------------------------------------------------------------------------

$sql = "SELECT sup.*
		FROM ".TB_PREF."sup as sup
		WHERE sup.id = ".db_escape($trans_id);
$result = db_query($sql, "could not retrieve transaction");
$myrow = db_fetch($result);

$sql2 = "SELECT part.*, stock.units
		FROM ".TB_PREF."sup as sup, ".TB_PREF."sup_part_used as part, ".TB_PREF."stock_master as stock
		WHERE sup.id = part.sup_no
		AND stock.stock_id = part.stock_id
		AND sup.id = ".db_escape($trans_id);
$res = db_query($sql2, "could not retrieve parts used");

//------------------------------------------------------------------------------------------------

start_outer_table($table_style2, 5);

table_section(1);

table_section_title(_("ACCOUNT NAME & DESCRIPTION"));
label_row(_("Company Name:"), $myrow['company_name']);
label_row(_("Address:"), nl2br($myrow['address']));
label_row(_("Contact Person:"), $myrow['contact_person']);
label_row(_("Tel No.:"), $myrow['tel_no']);
label_row(_("System / Unit:"), $myrow['system_unit']);
label_row(_("Location:"), $myrow['location']);
label_row(_("Serial No.:"), $myrow['serial_no']);
label_row(_("Accessories:"), $myrow['accessories']);

table_section_title(_("CUSTOMER COMMENT / COMPLAINT/ INSTRUCTION"));
label_row(_(" "), nl2br($myrow['customer_comment']));

table_section_title(_("TONER CARTRIDGE USED"));
label_row(_(""), get_toner_cartridge($myrow['toner_cartridge']));

table_section_title(_("TSG EVALUATION / DIAGNOSTIC"));
label_row(_(" "), nl2br($myrow['tsg_evaluation']));

table_section_title(_("TSG RECOMMENDATION"));
label_row(_(" "), nl2br($myrow['tsg_recommendation']));

table_section(2);

table_section_title(_("SUPPORT PROCESS NO."));
label_row(_("SUP - "), $myrow['sup_no']);
label_row(_("Type:"), get_sup_type($myrow['type']));
label_row(_("Received (Date / Time):"), $myrow['received_date']." ".$myrow['received_time']);
label_row(_("Required (Date / Time):"), $myrow['required_date']." ".$myrow['required_time']);
label_row(_("Warranty Type:"), get_sup_warranty($myrow['warranty']));
label_row(_("Assigned TSG : "), $myrow['assigned_tsg']);
label_row(_("Started (Date / Time):"), $myrow['started_date']." ".$myrow['started_time']);
label_row(_("Completed (Date / Time):"), $myrow['completed_date']." ".$myrow['completed_time']);
label_row(_("Conformed : "), $myrow['conformed']);
label_row(_("Status : "), get_sup_status($myrow['status']));

table_section_title(_("REPAIR DETAILS"));
label_row(_(" "), nl2br($myrow['repair_details']));

$res1 = get_audit_trail_all(600, $trans_id);
$audit_trail = "";	$x=1;
while($audit = db_fetch($res1))
{
	$name = get_user($audit['user']);
	if($x == 1)
		$audit_trail .= $name['user_id']." - ".$audit['description'];
	else
		$audit_trail .= "<br>".$name['user_id']." - ".$audit['description'];
		
	$x++;
}

label_row(_("Audit Trail"), $audit_trail);

end_outer_table(1);

display_heading("PART(s) USED");
div_start('items_table');
start_table("$table_style width=60%");

if (db_num_rows($res) > 0)
{
	$th = array(_("ITEM"), _("QTY"), _("UNIT"), _("PRICE"), _("DESCRIPTION / PART / SERIAL NUMBER"));
	table_header($th);

	$k = 0;	//row colour counter
	$sub_total = 0;
	while ($myrow2 = db_fetch($res))
	{
		//if($myrow2['quantity']==0) continue;
		alt_table_row_color($k);
	
		label_cell($myrow2["stock_id"]." - ".$myrow2["description"]);
        qty_cell($myrow2["quantity"], false, get_qty_dec($myrow2["stock_id"]));
		label_cell($myrow2["units"]);
		amount_cell($myrow2["unit_price"]);
		label_cell(nl2br($myrow2["comment"]));
        
	end_row();
	} //end while there are line items to print out

}
else
	display_note(_("There are no line items on this dispatch."), 1, 2);

end_table(1);

//------------------------------------------------------------------------------------------------

end_page(true);

?>