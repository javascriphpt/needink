<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SALESTRANSVIEW';
$path_to_root = "..";
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/admin/db/voiding_db.inc");
include_once($path_to_root . "/purchasing/includes/purchasing_ui.inc");

$js = "";
if ($use_date_picker)
	$js .= get_js_date_picker();
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
	
page(_($help_context = "Receive DR/ SI"), false, false, "", $js);

//----------------------------------------------------------------------------------------

if (isset($_POST['Process']))
{
	global $systypes_array;
	$error = 0;
	
	if (strlen($_POST['received_by']) == 0) 
	{
		display_error(_("Received_by cannot be empty."));
		set_focus('received_by');
		$error = 1;
	}
	
	if (!is_date($_POST['received_date']))
	{
		display_error(_("The entered date is invalid."));
		set_focus('received_date');		
		$error = 1;
	}
	
	if (!is_date_in_fiscalyear($_POST['received_date']))
	{
		display_error(_("The entered date is not in fiscal year."));
		set_focus('received_date');
		$error = 1;
	}
	
	if($error != 1)
	{
		$trans_no = $_POST['trans_no'];
		$type = $_POST['type'];
		$received_date = $_POST['received_date'];
		$received_by = $_POST['received_by'];
		$void = 0;
				
		$sql = "UPDATE ".TB_PREF."debtor_trans
				SET received_by = ".db_escape($received_by).",
					received_date = ".db_escape(date2sql($received_date))."
				WHERE trans_no = $trans_no
				AND type = $type";

		db_query($sql) or die('could not save received data');
		
		$void = 1;
		
		$url = $path_to_root . "/sales/inquiry/customer_inquiry.php";
		
		if($void != 0)
		{			
			display_notification_centered(_("Success"));
			unset($_POST['trans_no']);
			unset($_POST['type']);
			unset($_POST['received_date']);
			unset($_POST['received_by']);
			meta_forward($url);
		}
	}
}

//----------------------------------------------------------------------------------------

if($_GET)
{
	global $systypes_array;
	
	$trans_no = $_GET['trans_no'];
	$type = $_GET['type'];
	
	start_form();

	start_table($table_style2);
	
	hidden('type', $type);
	hidden('trans_no', $trans_no);

	label_row(_("Transaction Type:"), $systypes_array[$type]);
	
	$ref_no =  get_reference_no($trans_no, $type);
	label_row(_("Transaction #:"), $ref_no);
	
	date_row(_("Received Date:"), 'received_date');

	textarea_row(_("Received By:"), 'received_by', null, 30, 4);

	end_table(1);

	submit_center('Process', _("Save"), true, '', 'default');

	end_form();
}

end_page();

?>