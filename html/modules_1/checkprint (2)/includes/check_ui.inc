<?php


//-----------------------------------------------------------------------------------------------

function unassigned_check_list($name, $selected_id=null, $submit_on_change=false)
{
	$sql = "SELECT account_code, bank_account_name, bank_account_number, bank_curr_code FROM ".TB_PREF."bank_accounts WHERE account_code NOT IN (SELECT bank_ref FROM ".TB_PREF."check_account) AND account_type=1";

	return combo_input($name, $selected_id, $sql, 'account_code', 'bank_account_name',
		array(
			'format' => '_format_check_account',
			'order' => 'account_code',
			'select_submit'=> $submit_on_change,
			'async' => false		
		) );
}

function unassigned_check_list_cells($label, $name, $selected_id=null, $submit_on_change=false)
{
	if ($label != null)
		echo "<td>$label</td>\n";
	echo "<td>";
	echo unassigned_check_list($name, $selected_id, $submit_on_change);
	echo "</td>\n";
	return $str;
}

function unassigned_check_list_row($label, $name, $selected_id=null, $submit_on_change=false)
{
	echo "<tr>\n";
	unassigned_check_list_cells($label, $name, $selected_id, $submit_on_change);
	echo "</tr>\n";
}


function _format_check_account($row)
{
		return $row[0] .  "&nbsp;&nbsp;&nbsp;&nbsp;" . $row[1].  "&nbsp;&nbsp;(" . $row[2] .  ")&nbsp;&nbsp;-&nbsp;&nbsp;" . $row[3];
}



//---------------------------------------------------------------------------------------------------
?>