<?php

/*
	Do not edit this file manually. This copy of global file is overwritten
	by extensions editor.
*/

$installed_extensions = array (
	12 => array ( 'tab' => 'GL',
			'name' => 'Checking Accounts',
			'path' => 'checkprint',
			'title' => 'Checking Accounts',
			'active' => '1',
			'type' => 'plugin',
			'filename' => 'check_accounts.php',
			'acc_file' => 'acc_levels.php',
			'access' => 'SA_CHECKPRINTSETUP',
		),
	13 => array ( 'tab' => 'GL',
			'name' => 'Check Printing',
			'path' => 'checkprint',
			'title' => 'Check Printing',
			'active' => '1',
			'type' => 'plugin',
			'filename' => 'check_list.php',
			'acc_file' => 'acc_levels.php',
			'access' => 'SA_CHECKPRINT',
		),
	);
?>