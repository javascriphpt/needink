<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_PCCODE';
$path_to_root = "..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "PC Code"));

include($path_to_root . "/includes/ui.inc");

if (!isset($_POST['StockLocation']))
	$_POST['StockLocation'] = get_global_loc_code();
	
//-------------------------------------------------------------------------------------------------

if(isset($_POST['UpdateData']))
{
	$insert = 1;
	foreach($_POST as $postkey=>$postval )
    {
		if (strpos($postkey, 'ref') === 0)
		{
			$id = substr($postkey, strlen('ref'));
			
			$id_ = explode(',', $id);
			// display_error($id_[0]." = ".$_POST['prefix'.$id_[0]]." - ".$_POST['ref'.$id_[0]]);
						
			// if (strlen($_POST['prefix'.$id_[0]]) == 0 || trim($_POST['prefix'.$id_[0]]) == '')
			// {
				// display_error( _("Prefix should not be empty."));
				// set_focus($_POST['prefix'.$id_[0]]);
				// $insert = 0;
			// }
			
			if (strlen($_POST['ref'.$id_[0]]) == 0 || trim($_POST['ref'.$id_[0]]) == '')
			{
				display_error( _("Reference should not be empty."));
				set_focus($_POST['ref'.$id_[0]]);
				$insert = 0;
			}			
				
			if ($insert == 1)
			{
				$sql = "UPDATE ".TB_PREF."pc_code SET next_ref = ".db_escape($_POST['ref'.$id_[0]])."
					WHERE loc_code = ".db_escape($_POST['StockLocation']);
				db_query($sql,"reference could not be set");
			}
			
		}
	}
	
	if ($insert == 1)
		display_notification_centered(_("Updated"));
}

//-------------------------------------------------------------------------------------------------

start_form();

echo "<center>" . _("Location:"). "&nbsp;";
echo locations_list('StockLocation', null, false, true);

echo "<hr></center>";

set_global_loc_code($_POST['StockLocation']);

//display_error("StockLocation ".$_POST['StockLocation']);

if(isset($_POST['StockLocation']))
	$Ajax->activate('reorders');


//-------------------------------------------------------------------------------------------------

div_start('reorders');
start_table($table_style2);

$j = 1;
$k=0; //row colour counter

$th = array(/*_("Prefix"),*/ _("Next Ref"));
table_header($th);

$sql = "SELECT *
		FROM ".TB_PREF."pc_code 
		WHERE loc_code=".db_escape($_POST['StockLocation']);
$result = db_query($sql,"check failed");

while ($myrow = db_fetch($result))
{
	alt_table_row_color($k);
		
	//text_cells('', 'prefix'.$myrow["loc_code"], $myrow["prefix"]);
	text_cells('', 'ref'.$myrow["loc_code"], $myrow["next_ref"]);
	
	end_row();
}

end_table(1);

div_end();

submit_center('UpdateData', _("Update"), true, false, 'default');


//-------------------------------------------------------------------------------------------------

end_form();

//-------------------------------------------------------------------------------------------------

end_page();

?>