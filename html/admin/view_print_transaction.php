<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_VIEWPRINTTRANSACTION';
$path_to_root = "..";

include($path_to_root . "/includes/db_pager.inc");
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/reporting/includes/reporting.inc");
$js = "";
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
page(_($help_context = "View, Print or Void Transactions"), false, false, "", $js);

//----------------------------------------------------------------------------------------
function view_link($myrow)
{
	//return get_trans_view_str($trans["type"], $trans["trans_no"]);
	
	if($myrow["type"] == ST_CUSTDELIVERY || $myrow["type"] == ST_SALESINVOICE || $myrow["type"] == ST_CUSTCREDIT)
	{
		$ref_no = get_sales_ref_no($myrow["trans_no"], $myrow["type"]);
		return get_trans_view_str($myrow["type"], $myrow["trans_no"], $ref_no["location"]."-".$ref_no["form_type_no"]);
	}
	else if($myrow["type"] == ST_CUSTPAYMENT)
	{
		$ref_no = get_sales_ref_no_OR_PR($myrow["trans_no"], $myrow["type"]);
		return get_trans_view_str($myrow["type"], $myrow["trans_no"], $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"]));
	}
	else if($myrow["type"] == ST_WORKORDER)
	{
		$ref_no = get_wo_ref_no($myrow["trans_no"]);
		return get_trans_view_str($myrow["type"], $myrow["trans_no"], $ref_no["form_type_no"]);
	}
	else if($myrow["type"] == ST_MANURECEIVE)
	{
		$ref_no = get_wo_produce_ref_no($myrow["trans_no"]);
		return get_trans_view_str($myrow["type"], $myrow["trans_no"], $ref_no["form_type_no"]);
	}
	else if($myrow["type"] == ST_MANUISSUE)
	{
		$ref_no = get_wo_issues_ref_no($myrow["trans_no"]);
		return get_trans_view_str($myrow["type"], $myrow["trans_no"], $ref_no["form_type_no"]);
	}
	else
	{
		$ref_no =  get_reference_no($myrow["trans_no"], $myrow["type"]);
		return get_trans_view_str($myrow["type"], $myrow["trans_no"], $ref_no);
	}
}

function prt_link($row)
{
  	if ($row['type'] != ST_CUSTPAYMENT && $row['type'] != ST_BANKDEPOSIT) // customer payment or bank deposit printout not defined yet.
 		return print_document_link($row['trans_no'], _("Print"), true, $row['type'], ICON_PRINT);
}

function gl_view($row)
{
	return get_gl_view_str($row["type"], $row["trans_no"]);
}

function void_link($row)
{
	if (get_voided_entry($row['type'], $row['trans_no']) === false)
		return pager_link(_("Void"), "/admin/void_transaction_new.php?trans_no=". $row['trans_no']."&type=". $row['type'], ICON_REMOVE);
}

if (get_post('ProcessSearch')) 
{
	if (!check_valid_entries())
		unset($_POST['ProcessSearch']);
	$Ajax->activate('transactions');
} elseif (get_post('_OrderNumber_changed')) 
{
	$disable = get_post('OrderNumber') !== '';

	$Ajax->addDisable(true, 'filterType', $disable);
	$Ajax->addDisable(true, 'FromTransNo', $disable);
	$Ajax->addDisable(true, 'ToTransNo', $disable);

	if ($disable) {
		set_focus('OrderNumber');
	} else
		set_focus('filterType');

	$Ajax->activate('transactions');
}

function viewing_controls()
{
	display_note(_("Only documents can be printed."));

    start_table("class='tablestyle_noborder'");
	start_row();
	
	ref_cells(_("#:"), 'OrderNumber', '',null, '', true);

	systypes_list_cells(_("Type:"), 'filterType', null, true);

	if (!isset($_POST['FromTransNo']))
		$_POST['FromTransNo'] = "1";
	if (!isset($_POST['ToTransNo']))
		$_POST['ToTransNo'] = "999999";

    ref_cells(_("from #:"), 'FromTransNo');

    ref_cells(_("to #:"), 'ToTransNo');

    submit_cells('ProcessSearch', _("Search"), '', '', 'default');

	end_row();
    end_table(1);

}

//----------------------------------------------------------------------------------------

function check_valid_entries()
{
	if (!is_numeric($_POST['FromTransNo']) OR $_POST['FromTransNo'] <= 0)
	{
		display_error(_("The starting transaction number is expected to be numeric and greater than zero."));
		return false;
	}

	if (!is_numeric($_POST['ToTransNo']) OR $_POST['ToTransNo'] <= 0)
	{
		display_error(_("The ending transaction number is expected to be numeric and greater than zero."));
		return false;
	}

	return true;
}

//----------------------------------------------------------------------------------------

function handle_search()
{
	global $table_style;
	if (check_valid_entries()==true)
	{
		$db_info = get_systype_db_info($_POST['filterType']);

		if ($db_info == null)
			return;

		$table_name = $db_info[0];
		$type_name = $db_info[1];
		$trans_no_name = $db_info[2];
		$trans_ref = $db_info[3];

		$sql = "SELECT DISTINCT $trans_no_name as trans_no";

		if ($trans_ref)
			$sql .= " ,$trans_ref ";

		$sql .= ", ".$_POST['filterType']." as type FROM $table_name
			WHERE $trans_no_name >= ".db_escape($_POST['FromTransNo']). "
			AND  $trans_no_name <= ".db_escape($_POST['ToTransNo']);

		if ($type_name != null)
			$sql .= " AND `$type_name` = ".db_escape($_POST['filterType']);
			
		if (isset($_POST['OrderNumber']) && $_POST['OrderNumber'] != "")
		{
			$sql .= " AND $trans_no_name LIKE ".db_escape('%'.$_POST['OrderNumber'].'%');
		}

		$sql .= " ORDER BY $trans_no_name";


		$print_type = $_POST['filterType'];
		$print_out = ($print_type == ST_SALESINVOICE || $print_type == ST_CUSTCREDIT || $print_type == ST_CUSTDELIVERY ||
			$print_type == ST_PURCHORDER || $print_type == ST_SALESORDER || $print_type == ST_SALESQUOTE);

		$cols = array(
			_("System #"), 
			//_("Reference"), 
			_("#") => array('insert'=>true, 'fun'=>'view_link'),
			_("Print") => array('insert'=>true, 'fun'=>'prt_link'), 
			_("GL") => array('insert'=>true, 'fun'=>'gl_view'),
			_("Void") => array('insert'=>true, 'fun'=>'void_link')
		);
		if(!$print_out) {
			array_remove($cols, 2);
		}
		// if(!$trans_ref) {
			// array_remove($cols, 1);
		// }

		$table =& new_db_pager('transactions', $sql, $cols);
		$table->width = "40%";
		display_db_pager($table);
	}

}

//----------------------------------------------------------------------------------------

// if (isset($_POST['ProcessSearch']))
// {
	// if (!check_valid_entries())
		// unset($_POST['ProcessSearch']);
	// $Ajax->activate('transactions');
// }

//----------------------------------------------------------------------------------------

start_form(false);
	viewing_controls();
	handle_search();
end_form(2);

end_page();

?>
