<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_WOFORMSETUP';
$path_to_root = "..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Sales Forms Setup"));

include($path_to_root . "/includes/ui.inc");

if (!isset($_POST['type']))
	$_POST['type'] = get_global_loc_code();
	
//-------------------------------------------------------------------------------------------------

if(isset($_POST['UpdateData']))
{
	foreach($_POST as $postkey=>$postval )
    {
		if (strpos($postkey, 'wo') === 0)
		{
			$id = substr($postkey, strlen('wo'));
			
			$id_ = explode(',', $id);
			// display_error($id_[0]." = ".$_POST['wo'.$id_[0]]." - ".$_POST['type']);

			$sql = "UPDATE ".TB_PREF."wo_forms SET next_ref = ".db_escape($_POST['wo'.$id_[0]])."
				WHERE category_id = ".db_escape($_POST['type'])." 
				AND type_id = ".db_escape($id_[0]);
			
			db_query($sql,"reference could not be set");
			
		}
	}
	
	display_notification_centered(_("Updated"));
}

//-------------------------------------------------------------------------------------------

function get_wo_form_type_name($type_id)
{
	$sql = "SELECT name FROM ".TB_PREF."wo_form_trans_type WHERE type_id=".db_escape($type_id);

	$result = db_query($sql, "could not get type name");

	$row = db_fetch_row($result);

	return $row[0];
}

//-------------------------------------------------------------------------------------------------

start_form();

//-------------------------------------------------------------------------------------------------

echo "<center>";

wo_types_list_row(_("Type:"), 'type', null);

echo "<hr></center>";

set_global_loc_code($_POST['type']);

if(isset($_POST['type']))
	$Ajax->activate('reorders');

//-------------------------------------------------------------------------------------------------

div_start('reorders');
start_table($table_style2);

$j = 1;
$k=0; //row colour counter

$th = array(_("Transaction Type"), _("Next Ref"));
table_header($th);

$sql = "SELECT *
		FROM ".TB_PREF."wo_forms 
		WHERE category_id=".db_escape($_POST['type'])."
		ORDER BY type_id";
$result = db_query($sql,"check failed");

while ($myrow = db_fetch($result))
{
	alt_table_row_color($k);
		
		
	label_cell(get_wo_form_type_name($myrow["type_id"]));
	text_cells('', 'wo'.$myrow["type_id"], $myrow["next_ref"]);
	
	end_row();
}

end_table(1);

div_end();

submit_center('UpdateData', _("Update"), true, false, 'default');


//-------------------------------------------------------------------------------------------------

end_form();

//-------------------------------------------------------------------------------------------------

end_page();

?>