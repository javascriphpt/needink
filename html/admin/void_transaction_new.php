<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_VOIDTRANSACTION';
$path_to_root = "..";
include_once($path_to_root . "/includes/session.inc");

include_once($path_to_root . "/includes/date_functions.inc");
include_once($path_to_root . "/includes/ui.inc");
include_once($path_to_root . "/includes/data_checks.inc");

include_once($path_to_root . "/admin/db/voiding_db.inc");
include_once($path_to_root . "/purchasing/includes/purchasing_ui.inc");

$js = "";
if ($use_date_picker)
	$js .= get_js_date_picker();
if ($use_popup_windows)
	$js .= get_js_open_window(800, 500);
	
page(_($help_context = "Void a Transaction"), false, false, "", $js);

//----------------------------------------------------------------------------------------

if (isset($_POST['ProcessVoiding']))
{
	global $systypes_array;
	$error = 0;
	
	if (!is_date($_POST['date_']))
	{
		display_error(_("The entered date is invalid."));
		set_focus('date_');		
		$error = 1;
	}
	
	if (!is_date_in_fiscalyear($_POST['date_']))
	{
		display_error(_("The entered date is not in fiscal year."));
		set_focus('date_');
		$error = 1;
	}
	
	$void_entry = get_voided_entry($_POST['type'], $_POST['trans_no']);
	if ($void_entry != null) 
	{
		display_error(_("The selected transaction has already been voided."), true);
		unset($_POST['trans_no']);
		unset($_POST['type']);
		unset($_POST['memo_']);
		unset($_POST['date_']);
		$error = 1;
	}
	
	if($error != 1)
	{
		$trans_no = $_POST['trans_no'];
		$type = $_POST['type'];
		$date_ = $_POST['date_'];
		$memo_ = $_POST['memo_'];
		$void = 0;
		
		switch ($type) 
		{
			case ST_PURCHORDER : // it's a PO
			
				if(po_received($trans_no))
				{
					display_error(_("This order cannot be cancelled because some of it has already been invoiced or received."));
					$void = 0;
				}
				else
				{
					$sql = "UPDATE ".TB_PREF."purch_orders
								SET comments = '$memo_'
								WHERE order_no = $trans_no";
					db_query($sql) or die('could not void purchase order');
					
					$sql = "UPDATE ".TB_PREF."purch_order_details
								SET qty_invoiced = 0, unit_price=0, act_price=0, std_cost_unit=0, quantity_ordered=0, quantity_received=0
								WHERE order_no = $trans_no";
					db_query($sql) or die('could not void purchase order details');
					
					$void = 1;
				}
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
			
				break;
				
			case ST_SUPPRECEIVE : // it's a RR
			
				if(po_invoiced($trans_no))
				{
					display_error(_("The system cannot void this received note for the invoice for this transaction has not been voided."));
					$void = 0;
				}
				else
				{	
					post_void_supp_trans($type, $trans_no);
					
					$sql = "SELECT b.isn
							FROM ".TB_PREF."stock_moves a, ".TB_PREF."isn_trans b
							WHERE a.trans_no = b.trans_no
							AND a.type = b.trans_type
							AND a.trans_no = $trans_no
							AND a.type = $type";
					$result = db_query($sql, "could not get isn");
										
					while ($row = db_fetch($result))
					{
						$sql002 = "SELECT is_available FROM ".TB_PREF."isn_gen WHERE isn = ".db_escape($row[0]);
						$result002 = db_query($sql002, "sql002");
						$row002 = db_fetch_row($result002);
						
						if($row002[0] == 1)
						{
							// available
							
							$delete = "DELETE FROM ".TB_PREF."isn_trans WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
							db_query($delete);
							
							$update = "DELETE FROM ".TB_PREF."isn_gen WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
							db_query($update);
						}
						else
						{
							display_error(_("One or more entered serial number is not available."));
							$void = 0;
						}						
					}	
										
					$void = 1;					
				}
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
			
				break;
				
			case ST_SUPPINVOICE : // it's a suppler invoice
			
				if (!exists_supp_trans($type, $trans_no))
					display_error($systypes_array[$type]." # ".get_reference_no($trans_no, $type)." does not exist.");
				
				post_void_supp_trans($type, $trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
			
				break;				
			
			case ST_SUPPAYMENT : // it's a supplier payment
			
				if (!exists_supp_trans($type, $trans_no))
					display_error($systypes_array[$type]." # ".get_reference_no($trans_no, $type)." does not exist.");
				
				post_void_supp_trans($type, $trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
			
				break;
				
			case ST_BANKPAYMENT : // it's a payment
			
				if (!exists_bank_trans($type, $trans_no))
					display_error($systypes_array[$type]." # ".get_reference_no($trans_no, $type)." does not exist.");					
					
				void_bank_trans($type, $trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
			
				break;
				
			case ST_SALESORDER: // it's a sales order
		
				if(sales_order_has_deliveries_so($trans_no) == 1)
				{
					display_error(_("This order cannot be cancelled because some of it has already been invoiced or dispatched."));
				}
				else
				{
					$sql = "UPDATE ".TB_PREF."sales_orders
								SET comments = '$memo_',freight_cost=0
								WHERE order_no = $trans_no
								AND trans_type = $type";
					db_query($sql) or die('could not void sales order');
					
					$sql = "UPDATE ".TB_PREF."sales_order_details
								SET qty_sent = 0, unit_price = 0, quantity = 0, discount_percent = 0
								WHERE order_no = $trans_no
								AND trans_type = $type";
					db_query($sql) or die('could not void sales order details');
					
					$void = 1;
				
					$url = $path_to_root . "/admin/view_print_transaction.php";
				}
			
				break;
				
			case ST_SALESQUOTE: // it's a sales order
			
				$sql = "UPDATE ".TB_PREF."sales_orders
							SET comments = '$memo_',freight_cost=0
							WHERE order_no = $trans_no
							AND trans_type = $type";
				db_query($sql) or die('could not void sales order');
				
				$sql = "UPDATE ".TB_PREF."sales_order_details
							SET qty_sent = 0, unit_price = 0, quantity = 0, discount_percent = 0
							WHERE order_no = $trans_no
							AND trans_type = $type";
				db_query($sql) or die('could not void sales order details');
				
				$void = 1;
			
				$url = $path_to_root . "/admin/view_print_transaction.php";
							
				break;
				
			case ST_CUSTDELIVERY : // it's a customer dispatch
				
				if (!exists_customer_trans($type, $trans_no))
				{
					display_error("This ".$systypes_array[$type]." does not exist.");
					$void = 0;
				}
				
				if ($type == 13)	// added 04 Oct 2008 by Joe Hunt. If delivery note has a not voided invoice, then NO.
				{
					$delivery = get_customer_trans($trans_no, $type);
					if ($delivery['trans_link'] != 0)
					{
						if (get_voided_entry(10, $delivery['trans_link']) === false)
						{
							display_error(_("This Delivery Note has a not voided invoice."));
							$void = 0;
						}
						else
						{
							post_void_customer_trans($type, $trans_no);
				
							$sql = "SELECT b.isn
									FROM ".TB_PREF."stock_moves a, ".TB_PREF."isn_trans b
									WHERE a.trans_no = b.trans_no
									AND a.type = b.trans_type
									AND a.trans_no = $trans_no
									AND a.type = $type";
							$result = db_query($sql, "could not get isn");
							//$row = db_fetch_row($result);
							
							while ($row = db_fetch($result))
							{
								$delete = "DELETE FROM ".TB_PREF."isn_trans WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
								db_query($delete);
								
								$update = "UPDATE ".TB_PREF."isn_gen SET is_available = 1 WHERE isn = '".$row[0]."'";
								db_query($update);
							}				
							$void = 1;
							
							$url = $path_to_root . "/admin/view_print_transaction.php";
						}
					}
					else
					{
						post_void_customer_trans($type, $trans_no);
				
						$sql = "SELECT b.isn
								FROM ".TB_PREF."stock_moves a, ".TB_PREF."isn_trans b
								WHERE a.trans_no = b.trans_no
								AND a.type = b.trans_type
								AND a.trans_no = $trans_no
								AND a.type = $type";
						$result = db_query($sql, "could not get isn");
						//$row = db_fetch_row($result);
						
						while ($row = db_fetch($result))
						{
							$delete = "DELETE FROM ".TB_PREF."isn_trans WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
							db_query($delete);
							
							$update = "UPDATE ".TB_PREF."isn_gen SET is_available = 1 WHERE isn = '".$row[0]."'";
							db_query($update);
						}				
						$void = 1;
						
						$url = $path_to_root . "/admin/view_print_transaction.php";
					}
				}	
				
				break;
				
			case ST_SALESINVOICE : // it's a customer invoice
			
				if (!exists_customer_trans($type, $trans_no))
				{
					display_error("This ".$systypes_array[$type]." does not exist.");
					$void = 0;
				}
				
				post_void_customer_trans($type, $trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
			
				break;
				
			case ST_CUSTCREDIT : // it's a customer credit note
			
				if (!exists_customer_trans($type, $trans_no))
				{
					display_error("This ".$systypes_array[$type]." does not exist.");
					$void = 0;
				}
				
				// check isn if there's transaction after
				$sql1 = "SELECT b.id, b.isn, b.loc_code FROM ".TB_PREF."isn_trans b 
						WHERE b.trans_no = ".db_escape($trans_no)." AND b.trans_type = ".db_escape($type)."
						ORDER BY id DESC LIMIT 1";
				$res1 = db_query($sql1);
				$row1 = db_fetch_row($res1);				
				
				if(db_num_rows($res1) == 0)
				{
					post_void_customer_trans($type, $trans_no);
						
					$void = 1;
				
					$url = $path_to_root . "/sales/inquiry/customer_inquiry.php";
				}
				else
				{
					$sql2 = "SELECT b.id FROM ".TB_PREF."isn_trans b WHERE b.id > ".$row1[0]." AND b.isn IN (".db_escape($row1[1]).") AND b.loc_code = ".db_escape($row1[2]);
					$res2 = db_query($sql2);	
					if(db_num_rows($res2) == 0)
					{
						$sql = "SELECT b.isn
								FROM ".TB_PREF."stock_moves a, ".TB_PREF."isn_trans b
								WHERE a.trans_no = b.trans_no
								AND a.type = b.trans_type
								AND a.trans_no = $trans_no
								AND a.type = $type";
						$result = db_query($sql, "could not get isn");
						
						while($row = db_fetch($result))
						{
						
							$delete = "DELETE FROM ".TB_PREF."isn_trans WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
							db_query($delete);
							
							$update = "UPDATE ".TB_PREF."isn_gen SET is_available = 0 WHERE isn = '".$row[0]."'";
							db_query($update);
							
						}
						
						post_void_customer_trans($type, $trans_no);
						
						$void = 1;
					
						$url = $path_to_root . "/sales/inquiry/customer_inquiry.php";
					}
					else
					{
						display_error(_("The entered transaction cannot be voided."));
						$void = 0;
					}				
				}
				
				break;
				
			case ST_CUSTPAYMENT : // it's a customer payment
			
				if (!exists_customer_trans($type, $trans_no))
				{
					display_error("This ".$systypes_array[$type]." does not exist.");
					$void = 0;
				}
				
				post_void_customer_trans($type, $trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
				
				break;
				
			case ST_JOURNAL : // it's a journal entry
				
				if (!exists_gl_trans($type, $trans_no))
					display_error(_("The entered transaction does not exist or cannot be voided."));
				
				void_journal_trans($type, $trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
				
				break;
				
			case ST_BANKDEPOSIT : // it's a deposit
			
				if (!exists_bank_trans($type, $trans_no))
					display_error($systypes_array[$type]." # ".get_reference_no($trans_no, $type)." does not exist.");					
					
				void_bank_trans($type, $trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
			
				break;
				
			case ST_BANKTRANSFER : // it's a transfer
			
				if (!exists_bank_trans($type, $trans_no))
					display_error(_("The entered transaction does not exist or cannot be voided."));
				
				void_bank_trans($type, $trans_no);
				
				/////////// deposit allocation ///////////
				
				// clear any allocations for this transaction
				$sql = "SELECT * FROM ".TB_PREF."deposit_allocations
					WHERE (trans_type_from=".db_escape($type)." AND trans_no_from=".db_escape($trans_no).")";
				$result = db_query($sql, "could not void debtor transactions for type=$type and trans_no=$trans_no");

				while ($row = db_fetch($result))
				{
					$sql = "UPDATE ".TB_PREF."debtor_trans SET deposited = 0
						WHERE (type= " . $row['trans_type_from'] . " AND trans_no=" . $row['trans_no_from'] . ")
						OR (type=" . $row['trans_type_to'] . " AND trans_no=" . $row['trans_no_to'] . ")";
					db_query($sql, "could not clear allocation");											
				}

				// remove any allocations for this transaction
				$sql = "DELETE FROM ".TB_PREF."deposit_allocations
					WHERE (trans_type_from=".db_escape($type)." AND trans_no_from=".db_escape($trans_no).")";
				
				db_query($sql, "could not void debtor transactions for type=$type and trans_no=$trans_no");
				
				/////////// deposit allocation ///////////
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
				
				break;
				
			case ST_COSTUPDATE : // it's a journal entry
				
				if (!exists_gl_trans($type, $trans_no))
					display_error(_("The entered transaction does not exist or cannot be voided."));
				
				void_journal_trans($type, $trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
				
				break;
				
			case ST_DIMENSION : // it's a dimension
			
				break;
				
			case ST_MANURECEIVE : // it's a work order production
				
				if (!exists_work_order_produce($trans_no))
					display_error(_("The entered transaction does not exist or cannot be voided."));
				
				void_work_order_produce($trans_no);
				
				$sql = "SELECT b.isn
						FROM ".TB_PREF."stock_moves a, ".TB_PREF."isn_trans b
						WHERE a.trans_no = b.trans_no
						AND a.type = b.trans_type
						AND a.trans_no = $trans_no
						AND a.type = $type";
				$result = db_query($sql, "could not get isn");
									
				while ($row = db_fetch($result))
				{
					$sql002 = "SELECT is_available FROM ".TB_PREF."isn_gen WHERE isn = ".db_escape($row[0]);
					$result002 = db_query($sql002, "sql002");
					$row002 = db_fetch_row($result002);
					
					if($row002[0] == 1)
					{
						// available
						
						$delete = "DELETE FROM ".TB_PREF."isn_trans WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
						db_query($delete);
						
						$update = "DELETE FROM ".TB_PREF."isn_gen WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
						db_query($update);
					}
					else
					{
						display_error(_("One or more entered serial number is not available."));
						$void = 0;
					}						
				}	
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
				
				break;
				
			case ST_MANUISSUE : // it's a work order issue
			
				if (!exists_work_order_issue($trans_no))
					display_error(_("The entered transaction does not exist or cannot be voided."));
					
				void_work_order_issue($trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
				
				break;
				
			case ST_WORKORDER : // it's a work order
				
				if (!get_work_order($trans_no, true))
					display_error(_("The entered transaction does not exist or cannot be voided."));
				
				void_work_order($trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";

				break;
				
			case ST_INVADJUST : // it's a stock adjustment
				
				if (get_stock_adjustment_items($trans_no) == null)
					display_error(_("The entered transaction does not exist or cannot be voided."));
				
				void_stock_adjustment($trans_no);
				
				$sql = "SELECT b.isn
						FROM ".TB_PREF."stock_moves a, ".TB_PREF."isn_trans b
						WHERE a.trans_no = b.trans_no
						AND a.type = b.trans_type
						AND a.trans_no = $trans_no
						AND a.type = $type";
				$result = db_query($sql, "could not get isn");
									
				while ($row = db_fetch($result))
				{
					$sql002 = "SELECT is_available FROM ".TB_PREF."isn_gen WHERE isn = ".db_escape($row[0]);
					$result002 = db_query($sql002, "sql002");
					$row002 = db_fetch_row($result002);
					
					if($row002[0] == 1)
					{
						// available
						
						$delete = "DELETE FROM ".TB_PREF."isn_trans WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
						db_query($delete);
						
						$update = "DELETE FROM ".TB_PREF."isn_gen WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
						db_query($update);
					}
					else
					{
						display_error(_("One or more entered serial number is not available."));
						$void = 0;
					}						
				}

				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";				
				
				break;
			
			case ST_LOCTRANSFER : // it's a stock transfer
				
				if (get_stock_transfer_items($trans_no) == null)
					display_error(_("The entered transaction does not exist or cannot be voided."));
				
				$sql000 = "SELECT loc_code FROM ".TB_PREF."stock_moves WHERE qty < 0 AND type=".db_escape($type)." AND trans_no=".db_escape($trans_no)." LIMIT 1";
				$result000 = db_query($sql000, "sql000");	
				$row000 = db_fetch_row($result000);
				
				$sql001 = "UPDATE ".TB_PREF."gl_trans SET amount=0 WHERE type=".db_escape($type)." AND type_no=".db_escape($trans_no);
				db_query($sql001);
				
				$sql = "SELECT b.isn
						FROM ".TB_PREF."stock_moves a, ".TB_PREF."isn_trans b
						WHERE a.trans_no = b.trans_no
						AND a.type = b.trans_type
						AND a.trans_no = $trans_no
						AND a.type = $type";
				$result = db_query($sql, "could not get isn");
									
				while ($row = db_fetch($result))
				{
					$sql002 = "SELECT is_available FROM ".TB_PREF."isn_gen WHERE isn = ".db_escape($row[0]);
					$result002 = db_query($sql002, "sql002");
					$row002 = db_fetch_row($result002);
					
					if($row002[0] == 1)
					{
						// available
						
						$delete = "DELETE FROM ".TB_PREF."isn_trans WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
						db_query($delete);
						
						$update = "DELETE FROM ".TB_PREF."isn_gen WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
						db_query($update);
						
						$update000 = "UPDATE ".TB_PREF."isn_gen SET loc_code = ".db_escape($row000[0])." WHERE isn = '".$row[0]."'";
						db_query($update000);
					}						
				}

				void_stock_transfer($trans_no);

				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";	
				
				break;
				
			case ST_TRANSFERSLIP : // it's a transfer slip
				
				if (get_stock_transfer_items_temp_2($trans_no) == null)
					display_error(_("The entered transaction does not exist or cannot be voided."));
				
				$sql000 = "SELECT loc_code FROM ".TB_PREF."stock_moves WHERE type=".db_escape($type)." AND trans_no=".db_escape($trans_no);
				$result000 = db_query($sql000, "sql000");
				$row000 = db_fetch_row($result000);
								
				$sql001 = "DELETE FROM ".TB_PREF."gl_trans WHERE type=".db_escape($type)." AND type_no=".db_escape($trans_no);
				db_query($sql001);
				
				$sql = "SELECT b.isn
						FROM ".TB_PREF."stock_moves a, ".TB_PREF."isn_trans b
						WHERE a.trans_no = b.trans_no
						AND a.type = b.trans_type
						AND a.trans_no = $trans_no
						AND a.type = $type";
				$result = db_query($sql, "could not get isn");
								
				while ($row = db_fetch($result))
				{
					$sql002 = "SELECT is_available FROM ".TB_PREF."isn_gen WHERE isn = ".db_escape($row[0]);
					$result002 = db_query($sql002, "sql002");
					$row002 = db_fetch_row($result002);
					
					if($row002[0] == 1)
					{
						// available
						
						$delete = "DELETE FROM ".TB_PREF."isn_trans WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
						db_query($delete);
						
						$update = "DELETE FROM ".TB_PREF."isn_gen WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
						db_query($update);
						
						$update000 = "UPDATE ".TB_PREF."isn_gen SET loc_code = ".db_escape($row000[0])." WHERE isn = '".$row[0]."'";
						db_query($update000);
					}
					if($row002[0] == 2)
					{
						// for receiving
						
						$delete = "DELETE FROM ".TB_PREF."isn_trans WHERE trans_type = $type AND trans_no = $trans_no AND isn = '".$row[0]."'";
						db_query($delete);
						
						$update000 = "UPDATE ".TB_PREF."isn_gen SET is_available = 1 WHERE isn = '".$row[0]."' AND loc_code = ".db_escape($row000[0]);
						db_query($update000);
					}
				}

				void_stock_transfer_slip($trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";	
				
				break;
				
			case ST_APVVOUCHER : // it's APV
				
				if (!exists_apv($type, $trans_no))
					display_error(_("The entered transaction does not exist or cannot be voided."));
				
				void_apv($type, $trans_no);
				
				void_gl_trans($type, $trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
				
				break;
				
			case ST_CVVVOUCHER : // it's CV
				
				if (!exists_cv($type, $trans_no))
					display_error(_("The entered transaction does not exist or cannot be voided."));
				
				void_cv($type, $trans_no);
				
				$void = 1;
				
				$url = $path_to_root . "/admin/view_print_transaction.php";
				
				break;
		}
		
		if($void != 0)
		{
			// only add an entry if it's actually been voided
			add_audit_trail($type, $trans_no, $date_, _("Voided.")."\n".$memo_);
			add_voided_entry($type, $trans_no, $date_, $memo_);
			
			display_notification_centered(_("Selected transaction has been voided."));
			unset($_POST['trans_no']);
			unset($_POST['type']);
			unset($_POST['memo_']);
			unset($_POST['date_']);
			meta_forward($url);
		}
	}
}

//----------------------------------------------------------------------------------------
if($_GET)
{
	global $systypes_array;
	
	$trans_no = $_GET['trans_no'];
	$type = $_GET['type'];

	start_form();

	start_table($table_style2);
	
	hidden('type', $type);
	hidden('trans_no', $trans_no);

	label_row(_("Transaction Type:"), $systypes_array[$type]);
	
	if($type == ST_CUSTDELIVERY || $type == ST_SALESINVOICE || $type == ST_CUSTCREDIT)
	{
		$ref_no = get_sales_ref_no($trans_no, $type);
		label_row(_("Transaction #:"), $ref_no["location"]."-".$ref_no["form_type_no"]);
	}
	else if($type == ST_CUSTPAYMENT)
	{
		$ref_no = get_sales_ref_no_OR_PR($trans_no, $type);
		label_row(_("Transaction #:"), $ref_no["location"]."-".$ref_no["form_type_no"]."-".get_so_form_cat_name($ref_no["form_type"]));
	}
	else if($type == ST_SALESORDER || $type == ST_SALESQUOTE)
	{
		$ref_no = get_so_ref_no($trans_no, $type);
		label_row(_("Transaction #:"), $ref_no["from_stk_loc"]."-".$ref_no["so_form_type_no"]);
	}
	else if($type == ST_WORKORDER)
	{
		$ref_no = get_wo_ref_no($trans_no);
		label_row(_("Transaction #:"), $ref_no["form_type_no"]);
	}
	else if($type == ST_MANURECEIVE)
	{
		$ref_no = get_wo_produce_ref_no($trans_no);
		label_row(_("Transaction #:"), $ref_no["form_type_no"]);
	}
	else if($type == ST_MANUISSUE)
	{
		$ref_no = get_wo_issues_ref_no($trans_no);
		label_row(_("Transaction #:"), $ref_no["form_type_no"]);
	}
	else
	{
		$ref_no =  get_reference_no($trans_no, $type);
		label_row(_("Transaction #:"), $ref_no);
	}
		
	date_row(_("Voiding Date:"), 'date_');

	textarea_row(_("Memo:"), 'memo_', null, 30, 4);

	end_table(1);

	submit_center('ProcessVoiding', _("Void Transaction"), true, '', 'default');

	end_form();
}
//----------------------------------------------------------------------------------------

end_page();

?>