<?php
/**********************************************************************
    Copyright (C) FrontAccounting, LLC.
	Released under the terms of the GNU General Public License, GPL, 
	as published by the Free Software Foundation, either version 3 
	of the License, or (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  
    See the License here <http://www.gnu.org/licenses/gpl-3.0.html>.
***********************************************************************/
$page_security = 'SA_SALESFORMSETUP';
$path_to_root = "..";
include($path_to_root . "/includes/session.inc");

page(_($help_context = "Sales Forms Setup"));

include($path_to_root . "/includes/ui.inc");

if (!isset($_POST['StockLocation']))
	$_POST['StockLocation'] = get_global_loc_code();
	
//-------------------------------------------------------------------------------------------------

if(isset($_POST['UpdateData']))
{
	foreach($_POST as $postkey=>$postval )
    {
		if (strpos($postkey, 'so') === 0)
		{
			$id = substr($postkey, strlen('so'));
			
			$id_ = explode(',', $id);
			// display_error($id_[0]." = ".$_POST['so'.$id_[0]]." - ".$_POST['StockLocation']);
			
			if($id_[0] == '30' || $id_[0] == '13' || $id_[0] == '10' || $id_[0] == '11' || $id_[0] == '32')
			{
				$sql = "UPDATE ".TB_PREF."sales_forms SET next_ref = ".db_escape($_POST['so'.$id_[0]])."
					WHERE loc_code = ".db_escape($_POST['StockLocation'])." 
					AND type_id = ".db_escape($id_[0]);
				
				db_query($sql,"reference could not be set");
			}
			else
			{
				$cat_id = substr($id_[0], 5);
				// display_error($cat_id);
				
				$sql = "UPDATE ".TB_PREF."sales_forms SET next_ref = ".db_escape($_POST['so'.$id_[0]])."
					WHERE loc_code = ".db_escape($_POST['StockLocation'])." 
					AND type_id = 12
					AND category_id = ".db_escape($cat_id);
				
				db_query($sql,"reference could not be set");
			}
		}
	}
	
	display_notification_centered(_("Updated"));
}

//-------------------------------------------------------------------------------------------

function get_sales_form_type_name($type_id)
{
	$sql = "SELECT name FROM ".TB_PREF."sales_form_trans_type WHERE type_id=".db_escape($type_id);

	$result = db_query($sql, "could not get type name");

	$row = db_fetch_row($result);

	return $row[0];
}
//-------------------------------------------------------------------------------------------------

start_form();



echo "<center>" . _("Location:"). "&nbsp;";
echo locations_list('StockLocation', null, false, true);

echo "<hr></center>";

set_global_loc_code($_POST['StockLocation']);


//display_error("StockLocation ".$_POST['StockLocation']);
//display_error("category_id ".$_POST['category_id']);

if(isset($_POST['StockLocation']))
	$Ajax->activate('reorders');


//-------------------------------------------------------------------------------------------------

div_start('reorders');
start_table($table_style2);

$j = 1;
$k=0; //row colour counter

$th = array(_("Type"), _("Next Ref"));
table_header($th);

$sql = "SELECT *
		FROM ".TB_PREF."sales_forms 
		WHERE loc_code=".db_escape($_POST['StockLocation'])."
		AND type_id != 12
		ORDER BY type_id";
$result = db_query($sql,"check failed");

while ($myrow = db_fetch($result))
{
	alt_table_row_color($k);
		
		
	label_cell(get_sales_form_type_name($myrow["type_id"]));
	text_cells('', 'so'.$myrow["type_id"], $myrow["next_ref"]);
	
	end_row();
}

alt_table_row_color($k);


	label_cell("Customer Payment", "colspan=2");
	//text_cells('', 'so'.$myrow["type_id"], $myrow["next_ref"]);
				
	$th = array(_("Type"), _("Next Ref"));
	table_header($th);
	
	$sql2 = "SELECT b.name, a.next_ref, a.type_id, b.id
			FROM ".TB_PREF."sales_forms a, ".TB_PREF."sales_form_category b
			WHERE a.category_id = b.id
			AND a.loc_code=".db_escape($_POST['StockLocation'])."
			AND a.type_id = 12";
	$res = db_query($sql2,"check failed");
	
	while ($row = db_fetch($res))
	{	
		alt_table_row_color($k);
		label_cell($row["name"]);
		text_cells('', 'so'.$row["type_id"].'cat'.$row["id"], $row["next_ref"]);
		end_row();
	}
		

end_row();

end_table(1);

div_end();

submit_center('UpdateData', _("Update"), true, false, 'default');


//-------------------------------------------------------------------------------------------------

end_form();

//-------------------------------------------------------------------------------------------------

end_page();

?>